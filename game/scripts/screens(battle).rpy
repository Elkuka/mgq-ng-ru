init python:
    class Cmd():
        def __init__(self):
            self.clear()

        def clear(self):
            for idx in xrange(1, 9):
                setattr(self, "button%d_text" % idx, "")
                setattr(self, "button%d_action" % idx, None)

            self.switch_buttons = None
            self.switch_buttons_action = None

        def attack(self):
            self.clear()

            self.button1_text = "Атака"
            self.button1_action = 1 if store.cmd1 == 1 else None

            self.button2_text = "Навыки"
            self.button2_action = 7 if store.cmd7 == 1 and store.kousoku==0 else None

            self.button3_text = "Вырываться"
            self.button3_action = 2 if store.cmd2 == 1 and store.kousoku > 0 else None

            self.button4_text = "Защита"
            self.button4_action = 3 if store.cmd3 == 1 and store.kousoku==0 else None

            self.button5_text = "Подождать"
            self.button5_action = 4 if store.cmd4 == 1 else None

            self.button6_text = "Сдаться"
            self.button6_action = 5 if store.cmd5 == 1 else None

            self.button7_text = "Попросить об атаке" if persistent.NGMODE == False else ""
            self.button7_action = 6 if store.cmd6 == 1 and persistent.NGMODE == False else None

            self.button8_text = "Алиса" if store.alice_skill == 1 and store.kousoku == 0 else ""
            self.button8_action = 8 if store.alice_skill == 1 and store.kousoku == 0 else None

            self.button8_text = "Компаньоны" if store.support == 1 and store.support_turn < 1 else ""
            self.button8_action = 8 if store.support == 1 and store.support_turn < 1 else None


        def hanyo(self):
            self.clear()
            self.button1_text = store.hanlist1
            self.button1_action = 1 if store.hanlist1 else None

            self.button2_text = store.hanlist2
            self.button2_action = 2 if store.hanlist2 else None

            self.button3_text = store.hanlist3
            self.button3_action = 3 if store.hanlist3 else None

            self.button4_text = store.hanlist4
            self.button4_action = 4 if store.hanlist4 else None

        def onedari_clear(self):
            for idx in xrange(1, 17):
                setattr(store, "list%d" % idx, "")
                setattr(store, "list%d_unlock" % idx, 0)

        def onedari(self):
            self.clear()

            if store.list9:
                self.switch_buttons = 1
                self.switch_buttons_action = self.onedari2

            for idx in xrange(1, 9):
                setattr(self, "button%d_text" % idx, getattr(store, "list%d" % idx))
                if getattr(store, "list%d_unlock" % idx):
                    setattr(self, "button%d_action" % idx, idx)

        def onedari2(self):
            self.clear()
            self.switch_buttons = 1
            self.switch_buttons_action = self.onedari

            for idx in xrange(1, 9):
                setattr(self, "button%d_text" % idx, getattr(store, "list%d" % (idx+8)))
                if getattr(store, "list%d_unlock" % idx):
                    setattr(self, "button%d_action" % (idx+8), (idx+8))

        def skill_sylph(self):
            self.clear()

            self.button1_text = "Повторить призыв - 1"
            self.button1_action = 11 if store.mp > 0 else None

            if store.skill_whitewind:
                self.button5_text = "Белые ветра - 2"
                self.button5_action = 31 if store.mp > 1 and store.wind > 0 else None

            if store.skill_chipapa > 0:
                self.button2_text = "Чи-па-па - 1"
                self.button2_action = 32 if store.mp > 0 and store.wind > 0 else None

            if store.skill_earthcancel > 0:
                self.button3_text = "Отмена земли - 6"
                self.button3_action = 33 if store.mp > 5 and store.wind > 0 else None

        def skill_gnome(self):
            self.clear()

            self.button1_text = "Повторить призыв - 1"
            self.button1_action = 12 if store.mp > 0 else None

            if store.skill_brace:
                self.button5_text = "Укоренение - 4"
                self.button5_action = 36 if store.mp > 3 and store.earth > 0 else None

            if store.skill_strength > 0:
                self.button2_text = "Мощь земли - 3" # Доктор Попов, залогиньтесь x2
                self.button2_action = 37 if store.mp > 2 and store.earth > 0 else None

            if store.skill_windcancel > 0:
                self.button3_text = "Отмена ветра - 6"
                self.button3_action = 38 if store.mp > 5 and store.earth > 0 else None

        def skill_undine(self):
            self.clear()

            self.button1_text = "Уровень 2 - ВСЕ"
            self.button1_action = 13 if store.mp > 3 else None

            if store.aqua == 2:
                self.button5_text = "Уровень 3 - 0"
                self.button5_action = 16 if store.aqua == 2 else None
            elif store.aqua != 2:
                self.button5_text = "Уровень 3 - 2"
                self.button5_action = 16 if store.mp > 1 else None

            if store.skill_refresh > 0:
                self.button2_text = "Очищение - 2"
                self.button2_action = 41 if store.mp > 1 else None

            if store.skill_unpentagram > 0:
                self.button3_text = "Аква-пентаграмма - 3"
                self.button3_action = 42 if store.mp > 2 else None

            if store.skill_firecancel > 0:
                self.button4_text = "Фаер-резист - 6"
                self.button4_action = 43 if store.mp > 5 else None

        def skill_salamander(self):
            self.clear()

            self.button1_text = "Повторить призыв - 4"
            self.button1_action = None

            if store.skill_enrage:
                self.button5_text = "Берсерк - 5"
                self.button5_action = 46 if store.mp > 4 and store.fire > 0 else None

            if store.skill_burn > 0:
                self.button2_text = "Сгори! - 4"
                self.button2_action = 47 if store.mp > 3 and store.fire > 0 else None

            if store.skill_watercancel > 0:
                self.button3_text = "Отмена воды - 6"
                self.button3_action = 48 if store.mp > 5 and store.fire > 0 else None


        def luka_skill1(self):
            self.clear()

            if store.skill01 > 18 and store.skillset > 1:
                self.luka_skillx1()
                return

            if store.skill01 > 25:
                self.switch_buttons = 1
                self.switch_buttons_action = self.luka_skill2

            if store.skill01 > 13 or store.skillset == 1:
                self.button1_text = "Безмятежный Демонический Клинок - 2"
                self.button1_action = 8 if store.mp > 1 else None
            elif skill01 > 0:
                self.button1_text = "Демоническое Обезглавливание - 2"
                self.button1_action = 1 if store.mp > 1 else None

            if store.skill01 > 10 or store.skillset == 1:
                self.button2_text = "Молниеносный Удар Клинка - 2"
                self.button2_action = 6 if store.mp > 1 else None
            elif skill01 > 2:
                self.button2_text = "Громовой Выпад - 2"
                self.button2_action = 3 if store.mp > 1 else None

            if store.skill01 > 11 or store.skillset == 1:
                self.button3_text = "Сотрясающее Землю Обезглавливание 3"
                self.button3_action = 7 if store.mp > 2 else None
            elif skill01 > 4:
                self.button3_text = "Демонический Крушитель Черепов - 3"
                self.button3_action = 5 if store.mp > 2 and tikei > 0 else None

            if store.skill01 > 1 or store.skillset == 1:
                self.button4_text = "Неудержимый Испепеляющий Клинок - 4"
                self.button4_action = 10 if store.mp > 3 else None
            elif skill01 > 5:
                self.button4_text = "Гибельный Клинок Звезды Хаоса - 4"
                self.button4_action = 2 if store.mp > 3 else None
            elif skill01 > 1:
                self.button4_text = "Беспорядочные удары - 2"
                self.button4_action = 2 if store.mp > 1 else None

            if store.skill01 > 3:
                if guard_on != 0:
                    self.button5_text = "{color=#FF8000}Медитация - 3{/color}"
                else:
                    self.button5_text = "Медитация - 3"
                self.button5_action = 4 if store.mp > 2 and (store.fire == 0 or store.fire > 2) else None

            if store.skill01 > 25:
                if guard_on != 0:
                    self.button6_text = "{color=#FF8000}Разгон - 0{/color}"
                else:
                    self.button6_text = "Разгон - 0"
                self.button6_action = 26 if store.mylife > 0 and store.fire == 0 and store.aqua != 2 else None
            else:
                self.button6_text = "Крайность - 0"
                self.button6_action = 15 if store.mylife > 0 else None

            if store.skill01 > 20 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
                self.button8_text = "Четырёхкратный Гига-удар - 1"
                self.button8_action = 9 if store.mp > 0 else None
            elif store.skill04 < 1:
                if aqua == 4:
                    self.button8_text = "{color=#00BFFF}Безмятежный разум - 2{/color}"
                else:
                    self.button8_text = "Безмятежный разум - 2"
                self.button8_action = 16 if store.mp > 1 else None


        def luka_skill2(self):
            self.clear()

            if store.skill01 > 18 and store.skillset == 2:
                self.luka_skillx2()
                return

            # if 6 < skill01 < 17:
            self.switch_buttons = 1
            self.switch_buttons_action = self.luka_skill1

            if 0 < store.skill02 < 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if store.mp > 1 and store.wind == 0 and store.aqua < 2 else None
            elif store.skill02 == 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if store.mp > 1 and store.wind == 0 and store.aqua < 2 and store.fire == 0 else None
            elif store.skill02 == 3:
                if wind > 0 and wind != 4:
                    self.button1_text = "{color=00C957}Сильфа - 1{/color}"
                    if store.aqua != 2:
                        self.button1_action = 28 if store.mp > 0 and store.fire != 1 and store.fire != 2 else None
                    else:
                        self.button1_action = 28 if store.mp > 0 and (store.aqua == 0 or store.aqua == 3) and (store.fire == 0 or store.fire == 3) else None
                else:
                    self.button1_text = "Сильфа - 1"
                    self.button1_action = 11 if store.mp > 0 and ((store.aqua == 0 or store.aqua > 2) or (store.aqua == 2 and store.skill04 > 5)) and (store.fire == 0 or store.fire == 3) else None

            if 0 < store.skill03 < 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if store.mp > 1 and store.earth == 0 and store.aqua < 2 else None
            elif store.skill03 == 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if store.mp > 1 and store.earth == 0 and store.aqua < 2 and store.fire == 0 else None
            elif store.skill03 == 3:
                if earth > 0:
                    self.button2_text = "{color=F4A460}Гнома - 1{/color}"
                    if store.aqua != 2:
                        self.button2_action = 29 if store.mp > 0 and store.fire != 1 and store.fire != 2 else None
                    else:
                        self.button1_action = 29 if store.mp > 0 and (store.aqua == 0 or store.aqua == 3) and (store.fire == 0 or store.fire == 3) else None
                else:
                    self.button2_text = "Гнома - 1"
                    self.button2_action = 12 if store.mp > 0 and ((store.aqua == 0 or store.aqua > 2) and (store.fire == 0 or store.fire == 3)) or store.aqua != 2 else None

            if store.skill04 > 1:
                if aqua > 0:
                    self.button3_text = "{color=00BFFF}Ундина{/color}"
                else:
                    self.button3_text = "Ундина"

                self.button3_action = 27 if store.mp > 1 else None

            if 0 < store.skill05 < 2:
                self.button4_text = "{color=FF6347}Саламандра - 2{/color}"
                self.button4_action = 14 if store.mp > 1 and store.fire == 0 and store.aqua < 2 else None
            elif store.skill05 == 2:
                self.button4_text = "{color=FF6347}Саламандра - 2{/color}"
                self.button4_action = 14 if store.mp > 1 and store.fire == 0 and store.aqua < 2 else None
            elif store.skill05 == 3:
                if fire > 0:
                    self.button4_text = "{color=FF6347}Саламандра - 4{/color}"
                    self.button4_action = 30 if store.mp > 3 and store.aqua != 2 else None
                else:
                    self.button4_text = "Саламандра - 4"
                    self.button4_action = 14 if store.mp > 3 and (store.aqua > 2 or store.aqua < 2) else None

            if store.skill01 > 21 and store.skill02 > 0 and store.skill03 > 0 and store.skill04 > 0 and store.skill05 > 0:
                self.button5_text = "Вызов Четырёх Духов - 6"
                self.button5_action = 23 if store.mp > 5 else None

            if store.skill02 > 0 and store.skill03 > 0 and store.skill04 > 0 and store.skill05 > 0:
                self.button6_text = "Элементальная Спика - 10"
                self.button6_action = 22 if store.mp > 9 and store.wind > 0 and store.earth > 0 and store.aqua > 0 and store.fire > 0 else None
            if store.skill01 > 20 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0 and store.skillset == 2:
                self.button7_text = "Четырёхкратный Гига-удар - 1"
                self.button7_action = 9 if store.mp > 0 else None
            if store.skill01 > 25:
                self.button8_text = "Крайность - 0"
                self.button8_action = 15 if store.mylife > 0 else None

        def luka_skillx1(self):
            self.clear()

            if store.skill01 > 25:
                self.switch_buttons = 1
                self.switch_buttons_action = self.luka_skillx2

            self.button1_text = "Мгновенное Убийство - 2"
            self.button1_action = 17 if store.mp > 1 else None

            self.button2_text = "Возрождение Небесного Демона - 4"
            self.button2_action = 19 if store.mp > 3 else None

            self.button3_text = "Девятиликий Ракшаса - 6"
            self.button3_action = 18 if store.mp > 5 else None

            if daten < 1:
                self.button4_text = "Денница - 8"
            elif daten > 0:
                self.button4_text = "{color=#00FFFF}Денница (заряжена){/color}"
            self.button4_action = 24 if store.mp > 7 and daten < 1 else None

            if guard_on != 0:
                self.button5_text = "{color=#FF8000}Медитация - 3{/color}"
            else:
                self.button5_text = "Медитация - 3"
            self.button5_action = 4 if store.mp > 2 and (store.fire == 0 or store.fire > 2) else None

            if store.skill01 > 25:
                if guard_on != 0:
                    self.button6_text = "{color=#FF8000}Разгон - 0{/color}"
                else:
                    self.button6_text = "Разгон - 0"
                self.button6_action = 26 if store.mylife > 0 and store.fire == 0 else None
            else:
                self.button6_text = "Крайность - 0"
                self.button6_action = 15 if store.mylife > 0 else None

            if wind == 4:
                self.button7_text = "{color=#006400}Танец Падшего Ангела - 2{/color}"
            else:
                self.button7_text = "Танец Падшего Ангела - 2"
            self.button7_action = 25 if store.mp > 1 else None

            if skill04 > 0:
                self.button8_text = "Безмятежный Демонический Клинок - 2"
                self.button8_action = 8 if store.mp > 1 else None
            elif skill04 < 1:
                if aqua == 4:
                    self.button8_text = "{color=#00BFFF}Безмятежный разум - 2{/color}"
                else:
                    self.button8_text = "Безмятежный разум - 2"
                self.button8_action = 16 if store.mp > 1 else None


        def luka_skillx2(self):
            self.clear()

            self.switch_buttons = 1
            self.switch_buttons_action = self.luka_skillx1

            if 0 < store.skill02 < 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if store.mp > 1 and store.wind == 0 and store.aqua < 2 else None
            elif store.skill02 == 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if store.mp > 1 and store.wind == 0 and store.aqua < 2 and store.fire == 0 else None
            elif store.skill02 == 3:
                if wind > 0 and wind != 4:
                    self.button1_text = "{color=00C957}Сильфа - 1{/color}"
                    if store.aqua != 2:
                        self.button1_action = 28 if store.mp > 0 and store.fire != 1 and store.fire != 2 else None
                    else:
                        self.button1_action = 28 if store.mp > 0 and (store.aqua == 0 or store.aqua == 3) and (store.fire == 0 or store.fire == 3) else None
                else:
                    self.button1_text = "Сильфа - 1"
                    self.button1_action = 11 if store.mp > 0 and ((store.aqua == 0 or store.aqua > 2) or (store.aqua == 2 and store.skill04 > 5)) and (store.fire == 0 or store.fire == 3) else None

            if 0 < store.skill03 < 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if store.mp > 1 and store.earth == 0 and store.aqua < 2 else None
            elif store.skill03 == 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if store.mp > 1 and store.earth == 0 and store.aqua < 2 and store.fire == 0 else None
            elif store.skill03 == 3:
                if earth > 0:
                    self.button2_text = "{color=F4A460}Гнома - 1{/color}"
                    if store.aqua != 2:
                        self.button2_action = 29 if store.mp > 0 and store.fire != 1 and store.fire != 2 else None
                    else:
                        self.button1_action = 29 if store.mp > 0 and (store.aqua == 0 or store.aqua == 3) and (store.fire == 0 or store.fire == 3) else None
                else:
                    self.button2_text = "Гнома - 1"
                    self.button2_action = 12 if store.mp > 0 and ((store.aqua == 0 or store.aqua > 2) and (store.fire == 0 or store.fire == 3)) or store.aqua != 2 else None

            if store.skill04 > 1:
                if aqua > 0:
                    self.button3_text = "{color=00BFFF}Ундина{/color}"
                else:
                    self.button3_text = "Ундина"

                self.button3_action = 27 if store.mp > 1 else None

            if 0 < store.skill05 < 2:
                self.button4_text = "{color=FF6347}Саламандра - 2{/color}"
                self.button4_action = 14 if store.mp > 1 and store.fire == 0 and store.aqua < 2 else None
            elif store.skill05 == 2:
                self.button4_text = "{color=FF6347}Саламандра - 2{/color}"
                self.button4_action = 14 if store.mp > 1 and store.fire == 0 and store.aqua < 2 else None
            elif store.skill05 == 3:
                if fire > 0:
                    self.button4_text = "{color=FF6347}Саламандра - 4{/color}"
                    self.button4_action = 30 if store.mp > 3 and store.aqua != 2 else None
                else:
                    self.button4_text = "Саламандра - 4"
                    self.button4_action = 14 if store.mp > 3 and (store.aqua > 2 or store.aqua < 2) else None

            if store.skill01 > 21 and store.skill02 > 0 and store.skill03 > 0 and store.skill04 > 0 and store.skill05 > 0:
                self.button5_text = "Вызов Четырёх Духов - 6"
                self.button5_action = 23 if store.mp > 5 else None

            if store.skill02 > 0 and store.skill03 > 0 and store.skill04 > 0 and store.skill05 > 0:
                self.button6_text = "Элементальная Спика - 10"
                self.button6_action = 22 if store.mp > 9 and store.wind > 0 and store.earth > 0 and store.aqua > 0 and store.fire > 0 else None
            if store.skill01 > 20 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0 and store.skillset == 2:
                self.button7_text = "Четырёхкратный Гига-удар - 1"
                self.button7_action = 9 if store.mp > 0 else None
            if store.skill01 > 25:
                self.button8_text = "Крайность - 0"
                self.button8_action = 15 if store.mylife > 0 else None

        def alice_skill0(self):
            self.clear()

            self.button1_text = "Omega Blaze - 1"
            self.button1_action = 1

            self.button2_text = "Frost Ozma - 1"
            self.button2_action = 2

            self.button3_text = "Monster Lord's Cruelty - 2"
            self.button3_action = 3

        def alice_skill(self):
            self.clear()

            self.button1_text = "Omega Blaze"
            self.button1_action = 1

            self.button2_text = "Frost Ozma"
            self.button2_action = 2

            self.button3_text = "Monster Lord's Cruelty"
            self.button3_action = 3

            self.button4_text = "Eye of Recovery"
            self.button4_action = 4

        def alma_skill(self):
            self.clear()

            self.button1_text = _("Flying Knee - 2")
            self.button1_action = 1 if store.mp > 1 else None

            self.button2_text = _("Roundhouse Kick - 3")
            self.button2_action = 2 if store.mp > 2 else None

            self.button3_text = _("Melty Hand - 2")
            self.button3_action = 3 if store.mp > 1 else None

            self.button4_text = _("Shamshir - 5")
            self.button4_action = 4 if store.mp > 4 else None

        def erubetie_skill(self):
            self.clear()

            self.button1_text = _("Arcadia Spread - 2")
            self.button1_action = 1 if store.mp > 1 else None

            self.button2_text = _("Melty Storm - 4")
            self.button2_action = 2 if store.mp > 3 else None

            if store.sinkou == 3:
                self.button3_text = _("Colony Fusion - 0")
                self.button3_action = 3

        def tamamo_skill(self):
            self.clear()

            self.button1_text = _("Tail Attack - 2")
            self.button1_action = 1 if store.mp > 1 else None

            self.button2_text = _("Tamamo Punch - 3")
            self.button2_action = 2 if store.mp > 2 else None

            self.button3_text = _("Nine Moons - 5")
            self.button3_action = 3 if store.mp > 4 else None

            self.button4_text = _("Moonlight Cannon - 8")
            self.button4_action = 4 if store.mp > 7 else None

            if store.sinkou == 2:
                self.button5_text = _("Word of Dispel - 0")
                self.button5_action = 5

        def granberia_skill(self):
            self.clear()

            self.button1_text = _("Dragon Butcher Attack - 2")
            self.button1_action = 1 if store.mp > 1 else None

            self.button2_text = _("Bloody Fissure Thunder Thrust * Gale - 2")
            self.button2_action = 2 if store.mp > 1 else None

            self.button3_text = _("Death Sword Chaos Star - 3")
            self.button3_action = 3 if store.mp > 2 else None

            self.button4_text = _("Vaporizing Rebellion Sword - 4")
            self.button4_action = 4 if store.mp > 3 else None

            if store.sinkou == 4:
                self.button5_text = _("Quadruple Giga - 1")
                self.button5_action = 5 if store.mp > 0 else None

    cmd = Cmd()

screen cmd():
    predict False
    $ LM = 5
    $ RM = 5
    default var=getattr(cmd, act)()

    if turn_scorer > 0:
        use turn_scorer

    if wind or aqua or fire or earth:
        $ LM = 25
        use elm

    if enemy_wind or enemy_aqua or enemy_fire or enemy_earth:
        $ RM = 25
        use enemy_elm

    vbox:
        yalign 1.0
        yoffset -75

        if renpy.variant("touch"):

            if act != "attack":
                imagebutton:
                    at xyzoom(.9)
                    auto "back_%s"
                    margin (5, 3)
                    xalign 1.0

                    action Return(-1)

            if act == "attack":
                imagebutton:
                    at xyzoom(.9)
                    auto "menu_%s"
                    margin (5, 3)
                    xalign 1.0
                    action ShowMenu()

            if cmd.switch_buttons:
                hbox:
                    xfill True

                    # imagebutton:
                    #     xalign 0
                    #     at xyzoom(.9)
                    #     auto "left_%s_n"
                    #     margin (5, 3)
                    #     action Function(cmd.switch_buttons_action)

                    imagebutton:
                        xalign 1.0
                        at xyzoom(.9)
                        auto "right_%s_n"
                        margin (5, 3)
                        action Function(cmd.switch_buttons_action)

        window:
            background Frame(im.MatrixColor(persistent.box,
                                            im.matrix.colorize(persistent.box_color, persistent.box_color)),
                                            Borders(15, 15, 15, 15),
                                            tile=True)
            style_group "attack"

            left_margin LM
            right_margin RM
            side "l c r":

                # if not renpy.variant("touch") and cmd.switch_buttons:
                #     imagebutton:
                #         xmargin 2
                #         align (0, 1.0)
                #         auto "images/System/left_%s.webp"
                #         action Function(cmd.switch_buttons_action)

                frame:
                    xmargin 0
                    xfill True
                    background None
                    padding (0, 0)
                    align (.5, 1.0)
                    has grid 2 4:
                        transpose True
                        xfill True

                    for idx in xrange(1, 9):

                        textbutton getattr(cmd, "button%d_text" % idx):
                            if getattr(cmd, "button%d_action" % idx):
                                action Return(getattr(cmd, "button%d_action" % idx))

                if not renpy.variant("touch") and cmd.switch_buttons:
                    imagebutton:
                        xmargin 2
                        align (1.0, 1.0)
                        auto "images/System/right_%s.webp"

                        action Function(cmd.switch_buttons_action)

    if act != "attack":
        key "game_menu" action Return(-1)

screen hp():
    $ hero_nm = {
        0: "Лука",
        1: "Alice",
        2: "Alma Elma",
        3: "Erubetie",
        4: "Tamamo",
        5: "Granberia",
        99: "[ori_name!t]"
        }

    if not renpy.get_screen("choice"):
        window:
            id 'window'
            background Frame(im.MatrixColor(persistent.box,
                                            im.matrix.colorize(persistent.box_color, persistent.box_color)),
                                            Borders(15, 15, 15, 15),
                                            tile=True)
            ysize 70
            ypadding 0
            margin (5, 0, 5, 5)

            has hbox

            add "face" xoffset 5 yalign .5

            grid 2 1:
                yalign 1.0
                xfill True
                spacing 5
                style_group "hp"

                vbox:
                    xfill True
                    spacing 0

                    text "%s" % hero_nm[player] align (0, 0)

                    frame:
                        bar:
                            align (1.0, 1.0)
                            value AnimatedValue(range=max_mylife, value=mylife, delay=.2)
                            right_bar Frame('images/System/bar_full.webp', Borders(5, 5, 5, 5))
                            left_bar Frame('images/System/bar_empty.webp', Borders(5, 5, 5, 5))
                            bar_invert True

                        text "[mylife]/[max_mylife]":
                            align (.52, 0.5)
                            size 16
                            outlines [(2, "#000")]

                    hbox:
                        xoffset 20

                        text _('SP: [mp]')

                vbox:
                    xfill True
                    spacing 0

                    if appear_name:
                        text '[appear_name!t]' align (1.0, 0)
                    else:
                        text '[monster_name!t]' align (1.0, 0)

                    frame:
                        bar:
                            yalign .49
                            value AnimatedValue(enemylife, range=max_enemylife, delay=.2)

                            left_bar Frame('images/System/bar_full.webp', Borders(5, 5, 5, 5))
                            right_bar Frame('images/System/bar_empty.webp', Borders(5, 5, 5, 5))

                        # text "[enemylife]/[max_enemylife]":
                        #     align (.52, 0.5)
                        #     size 16
                        #     outlines [(2, "#000")]


style hp_text:
    is default
    size 14
    font "fonts/taurus.ttf"
    outlines [(1, "#000")]
    bold True

style hp_bar:
    is bar
    right_gutter 0
    left_gutter 0
    top_gutter 0
    bottom_gutter 0
    thumb None
    thumb_offset 0
    xsize 330
    ysize 20

style hp_frame:
    is frame
    background None
    padding (0, 0)
    margin (0, 0)
    ysize 20

style skillname:
    font "fonts/anime_ace.ttf"
    size 28
    italic True
    bold True
    color '#fff000'
    outlines [(2, "#00a", 2, 2),(2, "#000")]

style continents:
    font "fonts/papyrus.ttf"
    size 23
    color '#fff'
    outlines [(2, "#000")]

############################################################################
# Attack screen
#
#

style attack_window:
    yminimum 115
    xpadding 4

style attack_grid:
    is default
    align (.5, 5)
    xfill True

style attack_button:
    is default
    xalign .5
    padding (0, 0)
    margin (0, 0)
    background None

style attack_button_text:
    is text
    font "fonts/blogger.ttf"
    size 21
    kerning -0.5
    line_leading 1
    line_spacing 2
    idle_color "#fff"
    hover_color "#f00"
    insensitive_color "#8888"
    idle_outlines [(2, "#000c")]
    hover_outlines [(2, "#000c")]
    insensitive_outlines [(2, "#303030cc")]

style attack_button_text:
    variant "touch"
    line_leading 5
    line_spacing 5

screen turn_scorer():
    #add renpy.get_screen("bg", layer='master')
    #add "gm_bg"
    #add "gm char"

    style_group "gm"

    frame:
        pos (4, 280)
        #spacing 4

        vbox:

            if wind > 0 and wind != 4:
                text _("Ветер: [wind_turn]") size 14 color "#008000"
            elif wind > 0 and wind == 4:
                text _("Свет: [wind_turn]") size 14 color "#f0f0f0"
            else:
                text _("Ветер: 0") size 14 color "#008000"

            if earth > 0:
                text _("Земля: [earth_turn]") size 14 color "#A0522D"
            else:
                text _("Земля: 0") size 14 color "#A0522D"

            if aqua > 0:
                text _("Вода: [aqua_turn]") size 14 color "#00FFFF"
            else:
                text _("Вода: 0") size 14 color "#00FFFF"

            if fire > 0:
                text _("Огонь: [fire_turn]") size 14 color "#FF0000"
            else:
                text _("Огонь: 0") size 14 color "#FF0000"

screen elm():
    vbox:
        pos (2, 427)
        spacing 4

        if 0 < wind < 4 or wind > 4:
            add 'elm_wind'
        elif wind == 4 and skill02 == 0:
            add 'elm_wind2'
        elif (wind == 4 and skill02 > 0):
            add 'elm_wind3'
        else:
            null height 20

        if earth:
            add 'elm_earth'
        else:
            null height 20

        if aqua == 4 and skill04 == 0:
            add 'elm_aqua2'
        elif aqua:
            add 'elm_aqua'
        else:
            null height 20

        if fire:
            add 'elm_fire'
        else:
            null height 20


screen enemy_elm():
    if skill01 > 6:
        vbox:
            pos (778, 427)
            spacing 4

            if enemy_wind:
                add 'elm_wind'
            else:
                null height 20

            if enemy_earth:
                add 'elm_earth'
            else:
                null height 20

            if poison > 0:
                add 'elm_poison'
            elif enemy_aqua:
                add 'elm_aqua'
            else:
                null height 20

            if enemy_fire:
                add 'elm_fire'
            else:
                null height 20


screen badend(img="", zmonster_x=0,  bad1="", bad2=""):
    add img yalign 0.5 xpos zmonster_x

    style_group "bad_end"

    frame:
        align (0.8, 0.5)

        vbox:
            text _("Проигрышей: [persistent.count_end]") size 22 color "#f00"
            null height 20

            text "[bad1!t]"
            text "[bad2!t]"


style bad_end_frame:
    background Frame('images/System/win_bg_0.webp', 5, 5)
    xsize 420
    padding (20, 20)

style bad_end_text:
    size 19
    color "#ffff00"
    outlines [(2, "#000")]
    font "fonts/archiform.otf"
    xoffset 10


screen demo():
    vbox:
        yoffset -240
        yalign 1.0

        window:
            id "window"
            background Frame(im.MatrixColor(persistent.box,
                                            im.matrix.colorize(persistent.box_color, persistent.box_color)),
                                            Borders(10, 10, 10, 10),
                                            tile=True)
            style_group "attack_demo"
            ysize 115
            xmargin 5
            xpadding 30

            grid 2 4:
                xalign 0
                xfill True
                transpose True

                textbutton _('Attack{#at}'):
                    if demo == "attack":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Skill{#at}'):
                    if demo == "skill":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Struggle{#at}'):
                    if demo == "struggle":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Guard{#at}'):
                    if demo == "guard":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Wait{#at}'):
                    if demo == "wait":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Surrender{#at}'):
                    if demo == "surrender":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                textbutton _('Request{#at}'):
                    if demo == "request":
                        text_color "#f00"
                        at flash_but
                    action NullAction()

                null

style attack_demo_button:
    is attack_button

style attack_demo_button_text:
    is attack_button_text
    hover_color "#fff"
