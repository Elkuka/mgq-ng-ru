#############################################################
#
#   Меню
#
#############################################################
screen choice_spinoff(enter="spinoff"):
    tag menu
    modal True

    frame:
        style_group "spinoff"

        has vpgrid:
            rows MOD_ROWS
            spacing 15
            xfill True
            if MOD_ROWS > 9:
                scrollbars "vertical"
                mousewheel True
                draggable True

        for i in xrange(len(mod_tags)):
            $ mod_name_loc = mod_name[i]
            textbutton "[mod_name_loc]":
                action Return(mod_start_lab[i])

    use title(_("Побочные истории"))
    use return_but("spinoff_menu")

style spinoff_frame:
    xfill True
    yfill True
    margin (10, 60)
    padding (10, 10)
    background Frame('images/System/win_bg_0.webp', 5, 5)

style spinoff_button:
    background None
    # xpadding 0
    # xmargin 0
    xalign .5

style spinoff_button_text:
    is text
    xalign 0
    size 26
    font "fonts/archiform.otf"
    idle_color "#fff000"
    idle_outlines [(2, '#000c')]
    hover_color "#000fff"
    hover_outlines [(2, '#000c')]
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008")]

style spinoff_vscrollbar:
    is pref_vscrollbar

#############################################################
#
# Инициализация
#
#############################################################

label spinoff_start:
    $ _game_menu_screen = None
    $ addon_on = 1
    $ mon_labo_on = 1
    python:
        MOD_ROWS = 0
        for i in xrange(len(mod_tags)):
            MOD_ROWS += 1
    $ renpy.scene(layer="screens")
    stop music
    scene expression "#000" as bg
    scene bg 046
    show gm_bg
    with ImageDissolve('images/System/blinds.webp', 2.0)
    show screen choice_spinoff
    $ result = ui.interact()
    hide screen choice_spinoff
    scene bg black
    window auto
    $ renpy.jump(result)