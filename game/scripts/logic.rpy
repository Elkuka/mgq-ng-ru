# Открывает экран битвы и передаёт переменную act
label cmd(act):
    $ renpy.start_predict_screen("cmd")
    window hide
    $ result = renpy.call_screen("cmd")
    window show
    window auto
    $ renpy.stop_predict_screen("cmd")
    return

# Открывает экран города
label cmd_city:
    $ renpy.start_predict_screen("city")
    window hide
    $ renpy.scene()
    $ renpy.show(city_bg)
    with dissolve
    $ result = renpy.call_screen("city")
    window show
    window auto
    $ renpy.stop_predict_screen("city")
    return

# Счётчик проигрышей
label count_defeat(m_name, d_tag):
    $ persistent.defeat[d_tag] += 1

    if persistent.defeat[d_tag] == 1:
        $ persistent.count_koenemy += 1

    if persistent.defeat[d_tag] > persistent.count_most:
        $ persistent.count_most = persistent.defeat[d_tag]
        $ persistent.count_mostname = m_name

    return

# Обнуление интерфейса города
label city_null():
    call hanyo_null
    python:
        for idx in xrange(1, 20):
            idx = ("0%d" % idx) if idx < 10 else ("%d" % idx)
            setattr(store, "city_button%s" % idx, "")
            setattr(store, "city_act%s" % idx, 1)

    return

# Обнуление интерфейса запроса атак врага
label onedari_null():
    python:
        for idx in xrange(1, 17):
            setattr(store, "list%d" % idx, "")
            setattr(store, "list%d_unlock" % idx, 0)
    return

# Обработка хода битвы
label common_main:
    if sinkou == 2 and poison > 0 and skill_refresh < 1:
        call learn_refresh

    if not kousoku > 0:
        if enemy_wind > 0 and enemy_windturn < 1:
            call enemy_element_sel

        if enemy_earth > 0 and enemy_earthturn < 1:
            call enemy_element_sel

        if enemy_aqua > 0 and enemy_aquaturn < 1:
            call enemy_element_sel

        if enemy_fire > 0 and enemy_fireturn < 1:
            call enemy_element_sel

    $ enemy_spiritcount = 0
    $ saveback = 0
    $ previous_menu = 1
    $ gnome_buff = gnome_rep / 2
    $ undine_buff = undine_rep / 2
    $ store._skipping = False
    $ store._rollback = False
    $ previous_menu = 1
    $ battle = 1
    $ guard_on = 0
    $ support_turn = 0
    $ is_attack = 0
    $ is_damaged = 0
    if before_action != 91:
        $ mp_charge = 0

    if bougyo == 1:
        $ guard_on = 1
        $ bougyo = 0
    elif bougyo == 2 and (before_action == 42 or before_action == 91) and skill01 > 26:
        $ guard_on = 1

    if guard_on == 0:
        $ bougyo = 0

    if luka_turn > luka_maxturn:
        $ luka_turn = 0
    $ turn += 1
    $ owaza_count1a += 1
    $ owaza_count1b += 1
    $ owaza_count1c += 1
    $ owaza_count1d += 1

    if Difficulty == 3:
        $ owaza_count1a = 10
        $ owaza_count1b = 10
        $ owaza_count1c = 10
        $ owaza_count1d = 10

    $ owaza_count2a += 1
    $ owaza_count2b += 1
    $ owaza_count2c += 1
    $ owaza_count2d += 1

    if Difficulty == 3:
        $ owaza_count2a = 10
        $ owaza_count2b = 10
        $ owaza_count2c = 10
        $ owaza_count2d = 10

    $ owaza_count3a += 1
    $ owaza_count3b += 1
    $ owaza_count3c += 1
    $ owaza_count3d += 1
    $ owaza_count3e += 1
    $ owaza_count3f += 1
    $ owaza_count3g += 1
    $ owaza_count3h += 1

    if Difficulty == 3:
        $ owaza_count3a = 10
        $ owaza_count3b = 10
        $ owaza_count3c = 10
        $ owaza_count3d = 10
        $ owaza_count3e = 10
        $ owaza_count3f = 10
        $ owaza_count3g = 10
        $ owaza_count3h = 10

    $ tuika_owaza_count1a += 1
    $ tuika_owaza_count1b += 1
    $ tuika_owaza_count1c += 1
    $ tuika_owaza_count1d += 1
    $ tuika_owaza_count1e += 1
    $ tuika_owaza_count1f += 1
    $ tuika_owaza_count1g += 1
    $ tuika_owaza_count1h += 1
    $ tuika_owaza_count2a += 1
    $ tuika_owaza_count2b += 1
    $ tuika_owaza_count2c += 1
    $ tuika_owaza_count2d += 1
    $ tuika_owaza_count3a += 1
    $ tuika_owaza_count3b += 1
    $ tuika_owaza_count3c += 1
    $ tuika_owaza_count3d += 1
    $ tuika_owaza_count4a += 1
    $ tuika_owaza_count4b += 1
    $ skill_sub = 0
    $ skill_num = 0
    $ persistent.skill_num = 0

    if max_mylife != mylife:
        $ nodamage = 1

    if max_mylife > mylife*5 and kousoku == 0 and sel_kiki == 1:
        call face(param=2)

    if not kousoku > 0:
        $ wind_turn -= 1

        if wind > 0 and wind_turn < 0:
            call element_sel

        $ earth_turn -= 1

        if earth > 0 and earth_turn < 0:
            call element_sel

    if aqua > 0 and aqua != 2:
        $ aqua_turn -= 1

        if aqua_turn < 0:
            call element_sel

    if aqua == 2 and aqua_end == 1:
        call aqua_sel

    if aqua == 2 and wind != 5:
        $ mp -= 1

        if mp < 0:
            call aqua_sel

    elif aqua == 2 and wind == 5:
        $ mp -= 2

        if mp < 0:
            call aqua_sel

    if aqua == 2 and mp < 1:
        call aqua_sel

    if aqua == 2:
        $ aqua_turn = mp

    if aqua == 2 and wind == 5:
        $ wind_turn = mp

    $ aqua_end = 0

    if not kousoku > 0:
        $ fire_turn -= 1

        if fire > 0 and fire_turn < 0:
            call element_sel

    if poison > 0:
        $ poison -= 1
    if poison == 0:
        call common_poisonfade
    if poison > 0:
        call common_poison

    call count_alive

    if freeze_turn1 > 0 and enemy1life < 1:
        $ freeze_turn1 = -1
    if freeze_turn2 > 0 and enemy2life < 1:
        $ freeze_turn2 = -1
    if freeze_turn3 > 0 and enemy3life < 1:
        $ freeze_turn3 = -1
    if freeze_turn4 > 0 and enemy4life < 1:
        $ freeze_turn4 = -1
    if freeze_turn5 > 0 and enemy5life < 1:
        $ freeze_turn5 = -1
    if freeze_turn6 > 0 and enemy6life < 1:
        $ freeze_turn6 = -1
    if freeze_turn7 > 0 and enemy7life < 1:
        $ freeze_turn7 = -1
    if freeze_turn8 > 0 and enemy8life < 1:
        $ freeze_turn8 = -1
    if freeze_turn9 > 0 and enemy9life < 1:
        $ freeze_turn9 = -1

    if burn_turn1 > 0 and enemy1life < 1:
        $ burn_turn1 = -1
    if burn_turn2 > 0 and enemy2life < 1:
        $ burn_turn2 = -1
    if burn_turn3 > 0 and enemy3life < 1:
        $ burn_turn3 = -1
    if burn_turn4 > 0 and enemy4life < 1:
        $ burn_turn4 = -1
    if burn_turn5 > 0 and enemy5life < 1:
        $ burn_turn5 = -1
    if burn_turn6 > 0 and enemy6life < 1:
        $ burn_turn6 = -1
    if burn_turn7 > 0 and enemy7life < 1:
        $ burn_turn7 = -1
    if burn_turn8 > 0 and enemy8life < 1:
        $ burn_turn8 = -1
    if burn_turn9 > 0 and enemy9life < 1:
        $ burn_turn9 = -1

    if burn_turn == 0:
        call common_burnfade
    if burn_turn1 == 0:
        $ enemy_target = 1
        call common_burnfade
    if burn_turn2 == 0:
        $ enemy_target = 2
        call common_burnfade
    if burn_turn3 == 0:
        $ enemy_target = 3
        call common_burnfade
    if burn_turn4 == 0:
        $ enemy_target = 4
        call common_burnfade
    if burn_turn5 == 0:
        $ enemy_target = 5
        call common_burnfade
    if burn_turn6 == 0:
        $ enemy_target = 6
        call common_burnfade
    if burn_turn7 == 0:
        $ enemy_target = 7
        call common_burnfade
    if burn_turn8 == 0:
        $ enemy_target = 8
        call common_burnfade
    if burn_turn9 == 0:
        $ enemy_target = 9
        call common_burnfade

    if freeze_turn == 0:
        call common_freezefade
    if freeze_turn1 == 0:
        $ enemy_target = 1
        call common_freezefade
    if freeze_turn2 == 0:
        $ enemy_target = 2
        call common_freezefade
    if freeze_turn3 == 0:
        $ enemy_target = 3
        call common_freezefade
    if freeze_turn4 == 0:
        $ enemy_target = 4
        call common_freezefade
    if freeze_turn5 == 0:
        $ enemy_target = 5
        call common_freezefade
    if freeze_turn6 == 0:
        $ enemy_target = 6
        call common_freezefade
    if freeze_turn7 == 0:
        $ enemy_target = 7
        call common_freezefade
    if freeze_turn8 == 0:
        $ enemy_target = 8
        call common_freezefade
    if freeze_turn9 == 0:
        $ enemy_target = 9
        call common_freezefade

    if burn_turn > 0:
        call common_burn
        $ burn_turn -= 1
    if burn_turn1 > 0:
        $ enemy_target = 1
        call common_burn
        $ burn_turn1 -= 1
    if burn_turn2 > 0:
        $ enemy_target = 2
        call common_burn
        $ burn_turn2 -= 1
    if burn_turn3 > 0:
        $ enemy_target = 3
        call common_burn
        $ burn_turn3 -= 1
    if burn_turn4 > 0:
        $ enemy_target = 4
        call common_burn
        $ burn_turn4 -= 1
    if burn_turn5 > 0:
        $ enemy_target = 5
        call common_burn
        $ burn_turn5 -= 1
    if burn_turn6 > 0:
        $ enemy_target = 6
        call common_burn
        $ burn_turn6 -= 1
    if burn_turn7 > 0:
        $ enemy_target = 7
        call common_burn
        $ burn_turn7 -= 1
    if burn_turn8 > 0:
        $ enemy_target = 8
        call common_burn
        $ burn_turn8 -= 1
    if burn_turn9 > 0:
        $ enemy_target = 9
        call common_burn
        $ burn_turn9 -= 1

    if freeze_turn > 0:
        call common_burn
        $ freeze_turn -= 1
    if freeze_turn1 > 0:
        $ enemy_target = 1
        call common_burn
        $ freeze_turn1 -= 1
    if freeze_turn2 > 0:
        $ enemy_target = 2
        call common_burn
        $ freeze_turn2 -= 1
    if freeze_turn3 > 0:
        $ enemy_target = 3
        call common_burn
        $ freeze_turn3 -= 1
    if freeze_turn4 > 0:
        $ enemy_target = 4
        call common_burn
        $ freeze_turn4 -= 1
    if freeze_turn5 > 0:
        $ enemy_target = 5
        call common_burn
        $ freeze_turn5 -= 1
    if freeze_turn6 > 0:
        $ enemy_target = 6
        call common_burn
        $ freeze_turn6 -= 1
    if freeze_turn7 > 0:
        $ enemy_target = 7
        call common_burn
        $ freeze_turn7 -= 1
    if freeze_turn8 > 0:
        $ enemy_target = 8
        call common_burn
        $ freeze_turn8 -= 1
    if freeze_turn9 > 0:
        $ enemy_target = 9
        call common_burn
        $ freeze_turn9 -= 1

    if whitewind_turn == 0:
        call whitewind_end
    if brace_turn == 0:
        call brace_end
    if strength_turn == 0:
        call strength_end
    if unpentagram_turn == 0:
        call unpentagram_end
    if erpentagram_turn == 0:
        call erpentagram_end
    if enrage_turn == 0:
        call enrage_end
    if burn_turn == 0:
        call burn_end
    if blackwind_turn == 0:
        call blackwind_end
    if waterwall_turn == 0:
        call waterwall_end
    if titanguard_turn == 0:
        call titanguard_end
    if potion > 0 and potion_turn == 0:
        call potion_end
    # if zylphe_turn == 0:
    #     call zylphe_end
    # if gnomaren_turn == 0:
    #     call gnomaren_end
    # if grandine_turn == 0:
    #     call grandine_end
    # if gigamander_turn == 0:
    #     call gigamander_end

    if seduce_turn > 0:
        $ renpy.call(lavel + "_seduce")
    if seduce_turn == 0:
        $ seduce_turn = -1
        $ renpy.call(lavel + "_endseduce")
    if seduce_turn1 > 0:
        $ renpy.call(lavel + "_seduce1")
    if seduce_turn1 == 0:
        $ seduce_turn1 = -1
        $ renpy.call(lavel + "_endseduce1")
    if seduce_turn2 > 0:
        $ renpy.call(lavel + "_seduce2")
    if seduce_turn2 == 0:
        $ seduce_turn2 = -1
        $ renpy.call(lavel + "_endseduce2")
    if seduce_turn3 > 0:
        $ renpy.call(lavel + "_seduce3")
    if seduce_turn3 == 0:
        $ seduce_turn3 = -1
        $ renpy.call(lavel + "_endseduce3")
    if seduce_turn4 > 0:
        $ renpy.call(lavel + "_seduce4")
    if seduce_turn4 == 0:
        $ seduce_turn4 = -1
        $ renpy.call(lavel + "_endseduce4")
    if seduce_turn5 > 0:
        $ renpy.call(lavel + "_seduce5")
    if seduce_turn5 == 0:
        $ seduce_turn5 = -1
        $ renpy.call(lavel + "_endseduce5")
    if seduce_turn6 > 0:
        $ renpy.call(lavel + "_seduce6")
    if seduce_turn6 == 0:
        $ seduce_turn6 = -1
        $ renpy.call(lavel + "_endseduce6")
    if seduce_turn7 > 0:
        $ renpy.call(lavel + "_seduce7")
    if seduce_turn7 == 0:
        $ seduce_turn7 = -1
        $ renpy.call(lavel + "_endseduce7")
    if seduce_turn8 > 0:
        $ renpy.call(lavel + "_seduce8")
    if seduce_turn8 == 0:
        $ seduce_turn8 = -1
        $ renpy.call(lavel + "_endseduce8")
    if seduce_turn9 > 0:
        $ renpy.call(lavel + "_seduce9")
    if seduce_turn9 == 0:
        $ seduce_turn9 = -1
        $ renpy.call(lavel + "_endseduce9")
    if erbind_turn > 0:
        $ renpy.call(lavel + "_bind")
    if erbind_turn == 0:
        $ erbind_turn = -1
        $ renpy.call(lavel + "_endbind")
    if erbind_turn1 > 0:
        $ renpy.call(lavel + "_bind1")
    if erbind_turn1 == 0:
        $ erbind_turn1 = -1
        $ renpy.call(lavel + "_endbind1")
    if erbind_turn2 > 0:
        $ renpy.call(lavel + "_bind2")
    if erbind_turn2 == 0:
        $ erbind_turn2 = -1
        $ renpy.call(lavel + "_endbind2")
    if erbind_turn3 > 0:
        $ renpy.call(lavel + "_bind3")
    if erbind_turn3 == 0:
        $ erbind_turn3 = -1
        $ renpy.call(lavel + "_endbind3")
    if erbind_turn4 > 0:
        $ renpy.call(lavel + "_bind4")
    if erbind_turn4 == 0:
        $ erbind_turn4 = -1
        $ renpy.call(lavel + "_endbind4")
    if erbind_turn5 > 0:
        $ renpy.call(lavel + "_bind5")
    if erbind_turn5 == 0:
        $ erbind_turn5 = -1
        $ renpy.call(lavel + "_endbind5")
    if erbind_turn6 > 0:
        $ renpy.call(lavel + "_bind6")
    if erbind_turn6 == 0:
        $ erbind_turn6 = -1
        $ renpy.call(lavel + "_endbind6")
    if erbind_turn7 > 0:
        $ renpy.call(lavel + "_bind7")
    if erbind_turn7 == 0:
        $ erbind_turn7 = -1
        $ renpy.call(lavel + "_endbind7")
    if erbind_turn8 > 0:
        $ renpy.call(lavel + "_bind8")
    if erbind_turn8 == 0:
        $ erbind_turn8 = -1
        $ renpy.call(lavel + "_endbind8")
    if erbind_turn9 > 0:
        $ renpy.call(lavel + "_bind9")
    if erbind_turn9 == 0:
        $ erbind_turn9 = -1
        $ renpy.call(lavel + "_endbind9")

    if whitewind_turn > 0:
        call whitewind_effect
    if enrage_turn > 0:
        call enrage_effect

    if whitewind_turn > 0:
        $ whitewind_turn -= 1
    if brace_turn > 0:
        $ brace_turn -= 1
    if strength_turn > 0:
        $ strength_turn -= 1
    if unpentagram_turn > 0:
        $ unpentagram_turn -= 1
    if enrage_turn > 0:
        $ enrage_turn -= 1
    if burn_turn > 0:
        $ burn_turn -= 1
    if blackwind_turn > 0:
        $ blackwind_turn -= 1
    if titanguard_turn > 0:
        $ titanguard_turn -= 1
    if seduce_turn > 0:
        $ seduce_turn -= 1
    if seduce_turn1 > 0:
        $ seduce_turn1 -= 1
    if seduce_turn2 > 0:
        $ seduce_turn2 -= 1
    if seduce_turn3 > 0:
        $ seduce_turn3 -= 1
    if seduce_turn4 > 0:
        $ seduce_turn4 -= 1
    if seduce_turn5 > 0:
        $ seduce_turn5 -= 1
    if seduce_turn6 > 0:
        $ seduce_turn6 -= 1
    if seduce_turn7 > 0:
        $ seduce_turn7 -= 1
    if seduce_turn8 > 0:
        $ seduce_turn8 -= 1
    if seduce_turn9 > 0:
        $ seduce_turn9 -= 1
    if erpentagram_turn > 0:
        $ erpentagram_turn -= 1
    if erbind_turn > 0:
        $ erbind_turn -= 1
    if erbind_turn1 > 0:
        $ erbind_turn1 -= 1
    if erbind_turn2 > 0:
        $ erbind_turn2 -= 1
    if erbind_turn3 > 0:
        $ erbind_turn3 -= 1
    if erbind_turn4 > 0:
        $ erbind_turn4 -= 1
    if erbind_turn5 > 0:
        $ erbind_turn5 -= 1
    if erbind_turn6 > 0:
        $ erbind_turn6 -= 1
    if erbind_turn7 > 0:
        $ erbind_turn7 -= 1
    if erbind_turn8 > 0:
        $ erbind_turn8 -= 1
    if erbind_turn9 > 0:
        $ erbind_turn9 -= 1
    if zylphe_turn > 0:
        $ zylphe_turn -= 1
    if gnomaren_turn > 0:
        $ gnomaren_turn -= 1
    if grandine_turn > 0:
        $ grandine_turn -= 1
    if gigamander_turn > 0:
        $ gigamander_turn -= 1

    if enemy_wind > 0 and enemy_windturn < 1:
        call enemy_element_sel
    if enemy_fire > 0 and enemy_fireturn < 1:
        call enemy_element_sel
    if enemy_aqua > 0 and enemy_aquaturn < 1:
        call enemy_element_sel
    if enemy_earth > 0 and enemy_earthturn < 1:
        call enemy_element_sel
    if enemy1_wind > 0 and enemy1_windturn < 1:
        call enemy_element_sel
    if enemy1_fire > 0 and enemy1_fireturn < 1:
        call enemy_element_sel
    if enemy1_aqua > 0 and enemy1_aquaturn < 1:
        call enemy_element_sel
    if enemy1_earth > 0 and enemy1_earthturn < 1:
        call enemy_element_sel
    if enemy2_wind > 0 and enemy2_windturn < 1:
        call enemy_element_sel
    if enemy2_fire > 0 and enemy2_fireturn < 1:
        call enemy_element_sel
    if enemy2_aqua > 0 and enemy2_aquaturn < 1:
        call enemy_element_sel
    if enemy2_earth > 0 and enemy2_earthturn < 1:
        call enemy_element_sel
    if enemy3_wind > 0 and enemy3_windturn < 1:
        call enemy_element_sel
    if enemy3_fire > 0 and enemy3_fireturn < 1:
        call enemy_element_sel
    if enemy3_aqua > 0 and enemy3_aquaturn < 1:
        call enemy_element_sel
    if enemy3_earth > 0 and enemy3_earthturn < 1:
        call enemy_element_sel
    if enemy4_wind > 0 and enemy4_windturn < 1:
        call enemy_element_sel
    if enemy4_fire > 0 and enemy4_fireturn < 1:
        call enemy_element_sel
    if enemy4_aqua > 0 and enemy4_aquaturn < 1:
        call enemy_element_sel
    if enemy4_earth > 0 and enemy4_earthturn < 1:
        call enemy_element_sel
    if enemy5_wind > 0 and enemy5_windturn < 1:
        call enemy_element_sel
    if enemy5_fire > 0 and enemy5_fireturn < 1:
        call enemy_element_sel
    if enemy5_aqua > 0 and enemy5_aquaturn < 1:
        call enemy_element_sel
    if enemy5_earth > 0 and enemy5_earthturn < 1:
        call enemy_element_sel
    if enemy6_wind > 0 and enemy6_windturn < 1:
        call enemy_element_sel
    if enemy6_fire > 0 and enemy6_fireturn < 1:
        call enemy_element_sel
    if enemy6_aqua > 0 and enemy6_aquaturn < 1:
        call enemy_element_sel
    if enemy6_earth > 0 and enemy6_earthturn < 1:
        call enemy_element_sel
    if enemy7_wind > 0 and enemy7_windturn < 1:
        call enemy_element_sel
    if enemy7_fire > 0 and enemy7_fireturn < 1:
        call enemy_element_sel
    if enemy7_aqua > 0 and enemy7_aquaturn < 1:
        call enemy_element_sel
    if enemy7_earth > 0 and enemy7_earthturn < 1:
        call enemy_element_sel
    if enemy8_wind > 0 and enemy8_windturn < 1:
        call enemy_element_sel
    if enemy8_fire > 0 and enemy8_fireturn < 1:
        call enemy_element_sel
    if enemy8_aqua > 0 and enemy8_aquaturn < 1:
        call enemy_element_sel
    if enemy8_earth > 0 and enemy8_earthturn < 1:
        call enemy_element_sel
    if enemy9_wind > 0 and enemy9_windturn < 1:
        call enemy_element_sel
    if enemy9_fire > 0 and enemy9_fireturn < 1:
        call enemy_element_sel
    if enemy9_aqua > 0 and enemy9_aquaturn < 1:
        call enemy_element_sel
    if enemy9_earth > 0 and enemy9_earthturn < 1:
        call enemy_element_sel

    $ enemy_spiritcount = 0
    $ enemy1_spiritcount = 0
    $ enemy2_spiritcount = 0
    $ enemy3_spiritcount = 0
    $ enemy4_spiritcount = 0
    $ enemy5_spiritcount = 0
    $ enemy6_spiritcount = 0
    $ enemy7_spiritcount = 0
    $ enemy8_spiritcount = 0
    $ enemy9_spiritcount = 0

    if enemy_wind > 0:
        $ enemy_spiritcount += 1
        $ enemy_windturn -= 1
    if enemy_fire > 0:
        $ enemy_spiritcount += 1
        $ enemy_fireturn -= 1
    if enemy_aqua > 0:
        $ enemy_spiritcount += 1
        $ enemy_aquaturn -= 1
    if enemy_earth > 0:
        $ enemy_spiritcount += 1
        $ enemy_earthturn -= 1
    if enemy1_wind > 0:
        $ enemy1_spiritcount += 1
        $ enemy1_windturn -= 1
    if enemy1_fire > 0:
        $ enemy1_spiritcount += 1
        $ enemy1_fireturn -= 1
    if enemy1_aqua > 0:
        $ enemy1_spiritcount += 1
        $ enemy1_aquaturn -= 1
    if enemy1_earth > 0:
        $ enemy1_spiritcount += 1
        $ enemy1_earthturn -= 1
    if enemy2_wind > 0:
        $ enemy2_spiritcount += 1
        $ enemy2_windturn -= 1
    if enemy2_fire > 0:
        $ enemy2_spiritcount += 1
        $ enemy2_fireturn -= 1
    if enemy2_aqua > 0:
        $ enemy2_spiritcount += 1
        $ enemy2_aquaturn -= 1
    if enemy2_earth > 0:
        $ enemy2_spiritcount += 1
        $ enemy2_earthturn -= 1
    if enemy3_wind > 0:
        $ enemy3_spiritcount += 1
        $ enemy3_windturn -= 1
    if enemy3_fire > 0:
        $ enemy3_spiritcount += 1
        $ enemy3_fireturn -= 1
    if enemy3_aqua > 0:
        $ enemy3_spiritcount += 1
        $ enemy3_aquaturn -= 1
    if enemy3_earth > 0:
        $ enemy3_spiritcount += 1
        $ enemy3_earthturn -= 1
    if enemy4_wind > 0:
        $ enemy4_spiritcount += 1
        $ enemy4_windturn -= 1
    if enemy4_fire > 0:
        $ enemy4_spiritcount += 1
        $ enemy4_fireturn -= 1
    if enemy4_aqua > 0:
        $ enemy4_spiritcount += 1
        $ enemy4_aquaturn -= 1
    if enemy4_earth > 0:
        $ enemy4_spiritcount += 1
        $ enemy4_earthturn -= 1
    if enemy5_wind > 0:
        $ enemy5_spiritcount += 1
        $ enemy5_windturn -= 1
    if enemy5_fire > 0:
        $ enemy5_spiritcount += 1
        $ enemy5_fireturn -= 1
    if enemy5_aqua > 0:
        $ enemy5_spiritcount += 1
        $ enemy5_aquaturn -= 1
    if enemy5_earth > 0:
        $ enemy5_spiritcount += 1
        $ enemy5_earthturn -= 1
    if enemy6_wind > 0:
        $ enemy6_spiritcount += 1
        $ enemy6_windturn -= 1
    if enemy6_fire > 0:
        $ enemy6_spiritcount += 1
        $ enemy6_fireturn -= 1
    if enemy6_aqua > 0:
        $ enemy6_spiritcount += 1
        $ enemy6_aquaturn -= 1
    if enemy6_earth > 0:
        $ enemy6_spiritcount += 1
        $ enemy6_earthturn -= 1
    if enemy7_wind > 0:
        $ enemy7_spiritcount += 1
        $ enemy7_windturn -= 1
    if enemy7_fire > 0:
        $ enemy7_spiritcount += 1
        $ enemy7_fireturn -= 1
    if enemy7_aqua > 0:
        $ enemy7_spiritcount += 1
        $ enemy7_aquaturn -= 1
    if enemy7_earth > 0:
        $ enemy7_spiritcount += 1
        $ enemy7_earthturn -= 1
    if enemy8_wind > 0:
        $ enemy8_spiritcount += 1
        $ enemy8_windturn -= 1
    if enemy8_fire > 0:
        $ enemy8_spiritcount += 1
        $ enemy8_fireturn -= 1
    if enemy8_aqua > 0:
        $ enemy8_spiritcount += 1
        $ enemy8_aquaturn -= 1
    if enemy8_earth > 0:
        $ enemy8_spiritcount += 1
        $ enemy8_earthturn -= 1
    if enemy9_wind > 0:
        $ enemy9_spiritcount += 1
        $ enemy9_windturn -= 1
    if enemy9_fire > 0:
        $ enemy9_spiritcount += 1
        $ enemy9_fireturn -= 1
    if enemy9_aqua > 0:
        $ enemy9_spiritcount += 1
        $ enemy9_aquaturn -= 1
    if enemy9_earth > 0:
        $ enemy9_spiritcount += 1
        $ enemy9_earthturn -= 1

    if status != 0:
        $ status_turn -= 1

        if (status in (1, 2, 4, 5, 7) and status_turn > -1) or status == 8:
            jump status_sel1
        elif status in (1, 2, 4, 5, 7) and status_turn < 0:
            jump status_sel2
        elif status == 6:
            jump status_sel3

    if kadora != 0:

        if kadora_damage != 0 or kousoku != 0 or status != 0:
            $ kadora_damage = 0
            $ kadora = 0

        "Сила, наполняющая меч Луки, исчезает!"

        if kadora == 1:
            jump skill15a
        elif kadora == 2:
            jump skill15b
        elif kadora == 3:
            jump skill15c
        elif kadora == 4:
            jump skill15d

    if kousan > 0:
        $ renpy.jump(lavel + "_a")

    $ renpy.jump(lavel + "_main")

# Проигрывает музыку
label musicplay():
    $ mus_dic = {
        1: "battle",
        2: "boss1",
        3: "boss2",
        4: "sitenno",
        5: "maou",
        6: "comi1",
        7: "ruka",
        8: "boss3",
        9: "boss4",
        10: "yonseirei",
        11: "yonseirei2",
        12: "tamamo",
        13: "boss0",
        14: "eden",
        15: "stein2",
        16: "maou3",
        17: "maou4",
        18: "irias2",
        19: "irias3",
        20: "granberia",
        21: "tamamo",
        24: "granberia",
        25: "granberia2",
        26: "tamamo",
        27: "spirits",
        28: "alma",
        29: "erubetie",
        30: "boss",
        31: "boss2",
        32: "boss3",
        33: "boss4",
        34: "boss_adra",
        35: "spirits2",
        36: "boss5",
        37: "ruka4"
    }

    if mus > 19:
        $ renpy.music.play("NGDATA/bgm/%s.ogg" % mus_dic[mus], channel="music")
    elif mus < 20:
        $ renpy.music.play("audio/bgm/%s.ogg" % mus_dic[mus], channel="music")

    return

# Отравление ядом
label common_poison:
    $ poison_damage = 1
    if poison < 1:
        return
    play sound "audio/se/aqua3.ogg"
    "Яд медленно отравляет тело Луки!"
    if lavel == "erubetie_ng":
        call damage((150,200))
    elif lavel == "lilith_ng":
        call damage((280,350))
    else:
        call damage(((max_mylife/15),(max_mylife/9)))

    if mylife < 1:
        $ renpy.jump(lavel + "_hpoison")

    $ poison_damage = 0

    return

# Спад яда
label common_poisonfade:
    $ poison = 0
    if status == 9:
        $ status = 0

    "Яд выходит из тела Луки!"

    call status_print
    return

# Горение противника
label common_burn:
    play sound "audio/se/fire2.ogg"
    if enemy_num == 1:
        "Постепенно сгорая, [enemy] получает урон!"
    elif enemy_num > 1:
        "Постепенно сгорая, [target_name] получает урон!"

    $ ori_name = "Алиса"
    call enemylife
    $ ori_name = "Лука"

    return

# Спад горения
label common_burnfade:
    if enemy_num == 1:
        $ burn_turn -=1
        "Огонь на теле [enemy] наконец затухает!"
    elif enemy_num > 1:
        call target
        $ burn_turn -=1
        "Огонь на теле [target_name] наконец затухает!"

    return

# Заморозка противника
label common_freeze:
    play sound "NGDATA/se/iceeffect_frozen.ogg"
    if enemy_num == 1:
        "Тело [enemy] заморожено!"
    elif enemy_num > 1:
        "Тело [target_name] заморожено!"

    $ ori_name = "Алиса"
    call enemylife
    $ ori_name = "Лука"

    return

# Спад заморозки
label common_freezefade:
    play sound "NGDATA/se/iceeffect_break.ogg"
    if enemy_num == 1:
        $ freeze_turn -=1
        "Оболочка льда вокруг [enemy] растаяла!"
    elif enemy_num > 1:
        call target
        $ burn_turn -=1
        "Оболочка льда вокруг [target_name] растаяла!"

    return

# Эффект исцеляющего ветра каждый ход
label whitewind_effect:
    call show_skillname("Белый Ветер")
    play sound "audio/se/wind2.ogg"
    "Потоки святого ветра обволакивают Луку..."
    play sound "audio/se/kaihuku.ogg"
    "Лука восстанавливает немного здоровья"
    $ mylife += max_mylife * 10/100
    if mylife > max_mylife:
        $ mylife = max_mylife

    call hide_skillname
    return

# Спад исцеляющего ветра
label whitewind_end:
    $ whitewind_turn -= 1
    "Святой ветер, окружающий Луку, развеивается!"

    return

# Наносит урон сам себе каждый ход
label enrage_effect:
    call show_skillname("Берсерк")
    play sound "audio/se/fire2.ogg"
    "Огонь ярости сжигает тело Луки!"
    $ damage = max_mylife/5
    $ mylife -= damage
    if mylife < 1:
        $ mylife = 1
    "Лука получает [damage] урона!"
    call hide_skillname
    if max_mylife < mylife * 5:
        call face(param=1)

    return

# Спад берсерка
label enrage_end:
    $ enrage_turn -= 1
    "Пламенная ярость в сердце Луки угасает!"
    return

# Обнуление временных переменных
label hanyo_null():
    $ hanyo = [0 for x in hanyo]
    return

# Обнуление переменных после начала и конца битвы
label syokika:
    call hanyo_null()
    $ used_serenegale = 0
    $ kousoku = 0
    $ status = 0
    $ sinkou = 0
    $ sel_hphalf = 0
    $ sel_kiki = 0
    $ bougyo = 0
    $ kousan = 0
    $ cmd1 = 1
    $ cmd2 = 1
    $ cmd3 = 1
    $ cmd4 = 1
    $ cmd5 = 1
    $ cmd6 = 1
    $ cmd7 = 1
    $ counter = 0
    $ owaza_count1a = 1
    $ owaza_count1b = 1
    $ owaza_count1c = 1
    $ owaza_count1d = 1
    $ owaza_count2a = 1
    $ owaza_count2b = 1
    $ owaza_count2c = 1
    $ owaza_count2d = 1
    $ owaza_count3a = 1
    $ owaza_count3b = 1
    $ owaza_count3c = 1
    $ owaza_count3d = 1
    $ owaza_count3e = 1
    $ owaza_count3f = 1
    $ owaza_count3g = 1
    $ owaza_count3h = 1
    $ ren = 0
    $ ren2 = 0
    $ list_num1 = 0
    $ list_num2 = 0
    $ list_num3 = 0
    $ list_num4 = 0
    $ list_num5 = 0
    $ list_num6 = 0
    $ list_num7 = 0
    $ list_num8 = 0
    $ list_num9 = 0
    $ list_num10 = 0
    $ list_num11 = 0
    $ list_num12 = 0
    $ list_num13 = 0
    $ list_num14 = 0
    $ list_num15 = 0
    $ list_num16 = 0
    $ turn = 0
    $ first_sel1 = 0
    $ first_sel2 = 0
    $ first_sel3 = 0
    $ first_sel4 = 0
    $ wind = 0
    $ wind_turn = 0
    $ fire = 0
    $ fire_turn = 0
    $ earth = 0
    $ earth_turn = 0
    $ aqua = 0
    $ aqua_turn = 0
    $ cont = 0
    $ kousoku_hp = 0
    $ list1 = ""
    $ list2 = ""
    $ list3 = ""
    $ list4 = ""
    $ list5 = ""
    $ list6 = ""
    $ list7 = ""
    $ list8 = ""
    $ list9 = ""
    $ list10 = ""
    $ list11 = ""
    $ list12 = ""
    $ list13 = ""
    $ list14 = ""
    $ list15 = ""
    $ list16 = ""
    $ hanlist1 = ""
    $ hanlist2 = ""
    $ hanlist3 = ""
    $ hanlist4 = ""
    $ hanlist5 = ""
    $ hanlist6 = ""
    $ hanlist7 = ""
    $ hanlist8 = ""
    $ wind_kakuritu = 0
    $ wind_guard_on = 0
    $ nodamage = 0
    $ enemylv = 0
    $ owaza_num1 = 0
    $ owaza_num2 = 0
    $ before_action = 0
    $ skill_sub = 0
    $ nowmusic = 0
    $ persistent.hsean = 0
    stop hseanwave fadeout 1
    stop hseanvoice fadeout 1
    $ mogaku_anno1 = ""
    $ mogaku_anno2 = ""
    $ mogaku_anno3 = ""
    $ mogaku_anno4 = ""
    $ mogaku_sel1 = ""
    $ mogaku_sel2 = ""
    $ mogaku_sel3 = ""
    $ mogaku_sel4 = ""
    $ mogaku_sel5 = ""
    $ mogaku_dassyutu1 = ""
    $ mogaku_dassyutu2 = ""
    $ mogaku_earth_dassyutu1 = ""
    $ mogaku_earth_dassyutu2 = ""
    $ mogaku_kaisin1 = ""
    $ mogaku_kaisin2 = ""
    $ half_s1 = ""
    $ half_s2 = ""
    $ kiki_s1 = ""
    $ kiki_s2 = ""
    $ tukix = 320
    $ tukiy = 50
    $ tekikazu = 1
    $ kadora = 0
    $ earth_keigen_el = 0
    $ kaihi_el = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ enemy_holy = 0
    $ tuika_owaza_count1a = 1
    $ tuika_owaza_count1b = 1
    $ tuika_owaza_count1c = 1
    $ tuika_owaza_count1d = 1
    $ tuika_owaza_count1e = 1
    $ tuika_owaza_count1f = 1
    $ tuika_owaza_count1g = 1
    $ tuika_owaza_count1h = 1
    $ tuika_owaza_count2a = 1
    $ tuika_owaza_count2b = 1
    $ tuika_owaza_count2c = 1
    $ tuika_owaza_count2d = 1
    $ tuika_owaza_count3a = 1
    $ tuika_owaza_count3b = 1
    $ tuika_owaza_count3c = 1
    $ tuika_owaza_count3d = 1
    $ tuika_owaza_count4a = 1
    $ tuika_owaza_count4b = 1
    $ bougyo_attack = 0
    return

# Инициализация NGPlus-битв
label battleinit:
    $ count = 0
    $ result = 0
    $ turn_scorer = 1
    call spirit_buff

# Усиление духов от отношения
label spirit_buff:
    if skill03 < 1:
        $ gnome_buff = 0
    else:
        $ gnome_buff = gnome_rep / 2
    if skill04 < 1:
        $ undine_buff = 0
    else:
        $ undine_buff = undine_rep / 2

    if skill02 < 1:
        $ sylph_buff = 0
    else:
        $ sylph_buff = sylph_rep / 2
    if skill05 < 1:
        $ salamander_buff = 0
    else:
        $ salamander_buff = salamander_rep / 2

    if enviro_wind == 0 and enviro_earth == 0 and enviro_aqua == 0 and enviro_fire == 0:
        return
    call enviro_buff
    return

# Усиление духов от окружения
label enviro_buff:
    if skill03 != 0:
        if enviro_earth == 1:
            $ gnome_buff = gnome_buff*(110/100)
        elif enviro_earth == 2:
            $ gnome_buff = gnome_buff*(130/100)
        elif enviro_earth == 3:
            $ gnome_buff = gnome_buff*(150/100)

    if skill04 != 0:
        if enviro_aqua == 1:
            $ undine_buff = undine_buff+10
        elif enviro_aqua == 2:
            $ undine_buff = undine_buff+20
        elif enviro_aqua == 3:
            $ undine_buff = undine_buff+30

        if enviro_fire == 1:
            $ undine_buff = undine_buff-10
        elif enviro_fire == 2:
            $ undine_buff = undine_buff-20
        elif enviro_fire == 3:
            $ undine_buff = undine_buff-30

    if skill02 != 0:
        if enviro_wind == 1:
            $ sylph_buff = sylph_buff+15
        elif enviro_wind == 2:
            $ sylph_buff = sylph_buff+25
        elif enviro_wind == 3:
            $ sylph_buff = sylph_buff+35

    if skill05 != 0:
        if enviro_fire == 1:
            $ salamander_buff = salamander_buff*(125/100)
        elif enviro_fire == 2:
            $ salamander_buff = salamander_buff*(150/100)
        elif enviro_fire == 3:
            $ salamander_buff = salamander_buff*(175/100)

        if enviro_aqua == 1:
            $ salamander_buff = salamander_buff-10
        elif enviro_aqua == 2:
            $ salamander_buff = salamander_buff-20
        elif enviro_aqua == 3:
            $ salamander_buff = salamander_buff-30

# Инициализация битвы
label syutugen:
    $ enemy_num = 1
    $ target_all = 0
    $ no_hit = 0

    call status_reset
    if ng == 1:
        $ enemy_num = 1
    $ luka_maxturn = 1
    $ enemy_spiritcount = 0
    $ used_daystar = 0
    $ enemy_wind = 0
    $ enemy_windturn = 0
    $ enemy_earth = 0
    $ enemy_earthturn = 0
    $ enemy_aqua = 0
    $ enemy_aquaturn = 0
    $ enemy_fire = 0
    $ enemy_fireturn = 0

    $ renpy.start_predict(tatie1)
    $ renpy.start_predict(tatie2)
    $ renpy.start_predict(tatie3)
    $ renpy.start_predict(tatie4)
    $ renpy.start_predict(haikei)
    $ renpy.start_predict_screen("hp")

    stop music fadeout 1
    play music "audio/bgm/battlestart.ogg" noloop

    if not addon_on:
        $ ori_name = "Лука"
        $ player = 0
    elif addon_on and not ori_name:
        $ ori_name = "Лука"
        $ player = 0
    elif addon_on and ori_name:
        $ player = 99

    $ renpy.show(haikei)
    $ renpy.show(tatie1, at_list=[monster_pos], zorder=5)
    $ renpy.with_statement(dissolve)

    $ mylv_sin = mylv

    if player in (0, 99):
        call maxhp
    elif player == 1:
        $ max_mylife = 32000
    elif player == 2:
        $ max_mylife = 15000
    elif player == 3:
        $ max_mylife = 22000
    elif player == 4:
        $ max_mylife = 22000
    elif player == 5:
        $ max_mylife = 24000

    $ enemylife = max_enemylife
    $ mylife = max_mylife

    call syokika
    call battleinit
    show screen hp

    if plural_name == 1:
        ap "[appear_name!t] появляются!{w=4.5}{nw}"
    else:
        if appear_name:
            ap "[appear_name!t] появляется!{w=4.5}{nw}"
        else:
            ap "[monster_name!t] появляется!{w=4.5}{nw}"

    if mus != nowmusic:
        call musicplay

    return

# Инициализация битвы с неск. противниками
label syutugen_plural:
    $ target_all = 0
    $ no_hit = 0
    call status_reset
    if ng == 1:
        $ enemy_num = 1
    $ luka_maxturn = 1
    $ enemy_spiritcount = 0
    $ used_daystar = 0
    $ enemy_wind = 0
    $ enemy_windturn = 0
    $ enemy_earth = 0
    $ enemy_earthturn = 0
    $ enemy_aqua = 0
    $ enemy_aquaturn = 0
    $ enemy_fire = 0
    $ enemy_fireturn = 0

    $ renpy.start_predict(tatie1)
    $ renpy.start_predict(tatie2)
    $ renpy.start_predict(tatie3)
    $ renpy.start_predict(tatie4)
    $ renpy.start_predict(haikei)
    $ renpy.start_predict_screen("hp")
    stop music fadeout 1
    play music "audio/bgm/battlestart.ogg" noloop

    if not addon_on:
        $ ori_name = "Лука"
        $ player = 0
    elif addon_on and not ori_name:
        $ ori_name = "Лука"
        $ player = 0
    elif addon_on and ori_name:
        $ player = 99

    $ renpy.show(haikei)
    $ renpy.show(tatie1, at_list=[monster_pos], zorder=5)
    with dissolve

    $ mylv_sin = mylv

    if player == 0 or player == 99:
        call maxhp
    elif player == 1:
        $ max_mylife = 32000

    $ mylife = max_mylife
    $ enemylife = max_enemylife
    call syokika
    call battleinit
    show screen hp

    if appear_name:
        ap "[appear_name!t] появляются!{w=4.5}{nw}"
    else:
        ap "[monster_name!t] появляются!{w=4.5}{nw}"

    pause 4.5

    if mus != nowmusic:
        call musicplay

    return

# Инициализация битвы Алисы
label syutugen2_alice:
    $ renpy.start_predict(tatie1)
    $ renpy.start_predict(tatie2)
    $ renpy.start_predict(tatie3)
    $ renpy.start_predict(tatie4)
    $ renpy.start_predict(haikei)
    $ renpy.start_predict_screen("hp")

    if mus != nowmusic:
        call musicplay

    $ player = 1
    $ renpy.show(haikei)
    $ renpy.show(tatie1, at_list=[monster_pos], zorder=5)
    with dissolve

    $ mylv_sin = mylv

    if player == 0:
        call maxhp
    elif player == 1:
        $ max_mylife = 32000
    elif player == 99:
        call maxhp

    $ mylife = max_mylife
    $ enemylife = max_enemylife
    call syokika
    call battleinit
    show screen hp
    return

# Инициализация битвы без оповещения
label syutugen2:
    $ enemy_num = 1
    $ surrender = 0
    $ target_all = 0
    $ no_hit = 0
    call status_reset
    if ng == 1:
        $ enemy_num = 1
    $ luka_maxturn = 1
    $ enemy_spiritcount = 0
    $ used_daystar = 0
    $ daten = 0
    $ enemy_wind = 0
    $ enemy_windturn = 0
    $ enemy_earth = 0
    $ enemy_earthturn = 0
    $ enemy_aqua = 0
    $ enemy_aquaturn = 0
    $ enemy_fire = 0
    $ enemy_fireturn = 0

    $ renpy.start_predict(tatie1)
    $ renpy.start_predict(tatie2)
    $ renpy.start_predict(tatie3)
    $ renpy.start_predict(tatie4)
    $ renpy.start_predict(haikei)
    $ renpy.start_predict_screen("hp")

    if mus != nowmusic:
        call musicplay

    if player == 0:
        $ ori_name = "Лука"
    elif player == 2:
        $ ori_name = "Alma　Elma"
    elif player == 3:
        $ ori_name = "Erubetie"
    elif player == 4:
        $ ori_name = "Tamamo"
    elif player == 5:
        $ ori_name = "Granberia"

    $ renpy.show(haikei)
    $ renpy.show(tatie1, at_list=[monster_pos], zorder=5)

    with dissolve

    $ mylv_sin = mylv

    if player == 0:
        call maxhp
    elif player == 1:
        $ max_mylife = 32000
    elif player == 2:
        $ max_mylife = 15000
    elif player == 3:
        $ max_mylife = 22000
    elif player == 4:
        $ max_mylife = 22000
    elif player == 5:
        $ max_mylife = 24000
    elif player == 99:
        call maxhp

    $ mylife = max_mylife
    $ enemylife = max_enemylife
    call syokika
    call battleinit
    show screen hp
    return

# Зависимость SP от уровня
label maxmp:
    if player == 2:
        $ max_mp = 13
    elif player == 3:
        $ max_mp = 10
    elif player == 4:
        $ max_mp = 15
    elif player == 5:
        $ max_mp = 13
    elif skill01 == 0:
        $ max_mp = 0
    elif mylv < 8:
        $ max_mp = 2
    elif mylv < 15:
        $ max_mp = 3
    elif mylv < 23:
        $ max_mp = 4
    elif mylv < 30:
        $ max_mp = 5
    elif mylv < 37:
        $ max_mp = 6
    elif mylv < 44:
        $ max_mp = 7
    elif mylv < 51:
        $ max_mp = 8
    elif mylv < 58:
        $ max_mp = 9
    elif mylv < 65:
        $ max_mp = 10
    elif mylv < 72:
        $ max_mp = 11
    elif mylv < 79:
        $ max_mp = 12
    elif mylv < 81:
        $ max_mp = 13
    elif mylv < 84:
        $ max_mp = 14
    elif mylv < 86:
        $ max_mp = 15
    elif mylv < 88:
        $ max_mp = 16
    elif mylv < 90:
        $ max_mp = 17
    elif mylv < 92:
        $ max_mp = 18
    elif mylv < 94:
        $ max_mp = 19
    elif mylv < 96:
        $ max_mp = 20
    elif mylv < 98:
        $ max_mp = 21
    elif mylv < 98:
        $ max_mp = 22
    elif mylv == 100:
        $ max_mp = 23
    # Расчёт SP для >100 уровней
    else:
        $ max_mp = 23+3/5*(mylv-100)

    return

# Проверка опыта для повышения уровня
label lvup_check:
    if exp < 10:
        $ mylv = 1
    elif exp < 20:
        $ mylv = 2
    elif exp < 40:
        $ mylv = 3
    elif exp < 80:
        $ mylv = 4
    elif exp < 160:
        $ mylv = 5
    elif exp < 320:
        $ mylv = 6
    elif exp < 530:
        $ mylv = 7
    elif exp < 800:
        $ mylv = 8
    elif exp < 1200:
        $ mylv = 9
    elif exp < 1800:
        $ mylv = 10
    elif exp < 2800:
        $ mylv = 11
    elif exp < 4000:
        $ mylv = 12
    elif exp < 6000:
        $ mylv = 13
    elif exp < 9000:
        $ mylv = 14
    elif exp < 12000:
        $ mylv = 15
    elif exp < 15000:
        $ mylv = 16
    elif exp < 19000:
        $ mylv = 17
    elif exp < 24000:
        $ mylv = 18
    elif exp < 29000:
        $ mylv = 19
    elif exp < 35000:
        $ mylv = 20
    elif exp < 41000:
        $ mylv = 21
    elif exp < 47000:
        $ mylv = 22
    elif exp < 53000:
        $ mylv = 23
    elif exp < 61000:
        $ mylv = 24
    elif exp < 70000:
        $ mylv = 25
    elif exp < 81000:
        $ mylv = 26
    elif exp < 93000:
        $ mylv = 27
    elif exp < 105000:
        $ mylv = 28
    elif exp < 117000:
        $ mylv = 29
    elif exp < 130000:
        $ mylv = 30
    elif exp < 145000:
        $ mylv = 31
    elif exp < 160000:
        $ mylv = 32
    elif exp < 180000:
        $ mylv = 33
    elif exp < 210000:
        $ mylv = 34
    elif exp < 240000:
        $ mylv = 35
    elif exp < 270000:
        $ mylv = 36
    elif exp < 300000:
        $ mylv = 37
    elif exp < 340000:
        $ mylv = 38
    elif exp < 380000:
        $ mylv = 39
    elif exp < 430000:
        $ mylv = 40
    elif exp < 480000:
        $ mylv = 41
    elif exp < 540000:
        $ mylv = 42
    elif exp < 600000:
        $ mylv = 43
    elif exp < 670000:
        $ mylv = 44
    elif exp < 740000:
        $ mylv = 45
    elif exp < 810000:
        $ mylv = 46
    elif exp < 880000:
        $ mylv = 47
    elif exp < 960000:
        $ mylv = 48
    elif exp < 1040000:
        $ mylv = 49
    elif exp < 1120000:
        $ mylv = 50
    elif exp < 1200000:
        $ mylv = 51
    elif exp < 1300000:
        $ mylv = 52
    elif exp < 1400000:
        $ mylv = 53
    elif exp < 1500000:
        $ mylv = 54
    elif exp < 1650000:
        $ mylv = 55
    elif exp < 1800000:
        $ mylv = 56
    elif exp < 1950000:
        $ mylv = 57
    elif exp < 2100000:
        $ mylv = 58
    elif exp < 2300000:
        $ mylv = 59
    elif exp < 2500000:
        $ mylv = 60
    elif exp < 2700000:
        $ mylv = 61
    elif exp < 3000000:
        $ mylv = 62
    elif exp < 3400000:
        $ mylv = 63
    elif exp < 3800000:
        $ mylv = 64
    elif exp < 4200000:
        $ mylv = 65
    elif exp < 4600000:
        $ mylv = 66
    elif exp < 5000000:
        $ mylv = 67
    elif exp < 5500000:
        $ mylv = 68
    elif exp < 6100000:
        $ mylv = 69
    elif exp < 6800000:
        $ mylv = 70
    elif exp < 7600000:
        $ mylv = 71
    elif exp < 8500000:
        $ mylv = 72
    elif exp < 9500000:
        $ mylv = 73
    elif exp < 11000000:
        $ mylv = 74
    elif exp < 12500000:
        $ mylv = 75
    elif exp < 14000000:
        $ mylv = 76
    elif exp < 16000000:
        $ mylv = 77
    elif exp < 18000000:
        $ mylv = 78
    elif exp < 20000000:
        $ mylv = 79
    elif exp < 22000000:
        $ mylv = 80
    elif exp < 24000000:
        $ mylv = 81
    elif exp < 26000000:
        $ mylv = 82
    elif exp < 28000000:
        $ mylv = 83
    elif exp < 30000000:
        $ mylv = 84
    elif exp < 32000000:
        $ mylv = 85
    elif exp < 34000000:
        $ mylv = 86
    elif exp < 36000000:
        $ mylv = 87
    elif exp < 38000000:
        $ mylv = 88
    elif exp < 40000000:
        $ mylv = 89
    elif exp < 43000000:
        $ mylv = 90
    elif exp < 46000000:
        $ mylv = 91
    elif exp < 49000000:
        $ mylv = 92
    elif exp < 53000000:
        $ mylv = 93
    elif exp < 56000000:
        $ mylv = 94
    elif exp < 59000000:
        $ mylv = 95
    elif exp < 63000000:
        $ mylv = 96
    elif exp < 67000000:
        $ mylv = 97
    elif exp < 71000000:
        $ mylv = 98
    elif exp < 75000000:
        $ mylv = 99
    elif exp < 80000000:
        $ mylv = 100
        $ exp_sin = 75000000
    # Расчёт опыта для >100 уровней
    elif exp > exp_sin + (mylv ** 3 * 10 / 2):
        $ exp_sin += (mylv ** 3 * 10 / 2)
        $ mylv += 1

    call maxhp
    call maxmp
    call spirit_buff
    return


label maxhp:
    if mylv == 1:
        $ max_mylife = 30
    elif mylv == 2:
        $ max_mylife = 40
    elif mylv == 3:
        $ max_mylife = 50
    elif mylv == 4:
        $ max_mylife = 60
    elif mylv == 5:
        $ max_mylife = 70
    elif mylv == 6:
        $ max_mylife = 80
    elif mylv == 7:
        $ max_mylife = 90
    elif mylv == 8:
        $ max_mylife = 100
    elif mylv == 9:
        $ max_mylife = 110
    elif mylv == 10:
        $ max_mylife = 120
    elif mylv == 11:
        $ max_mylife = 130
    elif mylv == 12:
        $ max_mylife = 140
    elif mylv == 13:
        $ max_mylife = 150
    elif mylv == 14:
        $ max_mylife = 160
    elif mylv == 15:
        $ max_mylife = 170
    elif mylv == 16:
        $ max_mylife = 180
    elif mylv == 17:
        $ max_mylife = 190
    elif mylv == 18:
        $ max_mylife = 200
    elif mylv == 19:
        $ max_mylife = 215
    elif mylv == 20:
        $ max_mylife = 230
    elif mylv == 21:
        $ max_mylife = 245
    elif mylv == 22:
        $ max_mylife = 260
    elif mylv == 23:
        $ max_mylife = 275
    elif mylv == 24:
        $ max_mylife = 290
    elif mylv == 25:
        $ max_mylife = 305
    elif mylv == 26:
        $ max_mylife = 320
    elif mylv == 27:
        $ max_mylife = 335
    elif mylv == 28:
        $ max_mylife = 350
    elif mylv == 29:
        $ max_mylife = 370
    elif mylv == 30:
        $ max_mylife = 390
    elif mylv == 31:
        $ max_mylife = 410
    elif mylv == 32:
        $ max_mylife = 430
    elif mylv == 33:
        $ max_mylife = 450
    elif mylv == 34:
        $ max_mylife = 470
    elif mylv == 35:
        $ max_mylife = 490
    elif mylv == 36:
        $ max_mylife = 510
    elif mylv == 37:
        $ max_mylife = 530
    elif mylv == 38:
        $ max_mylife = 550
    elif mylv == 39:
        $ max_mylife = 580
    elif mylv == 40:
        $ max_mylife = 610
    elif mylv == 41:
        $ max_mylife = 640
    elif mylv == 42:
        $ max_mylife = 670
    elif mylv == 43:
        $ max_mylife = 700
    elif mylv == 44:
        $ max_mylife = 730
    elif mylv == 45:
        $ max_mylife = 760
    elif mylv == 46:
        $ max_mylife = 790
    elif mylv == 47:
        $ max_mylife = 820
    elif mylv == 48:
        $ max_mylife = 860
    elif mylv == 49:
        $ max_mylife = 900
    elif mylv == 50:
        $ max_mylife = 940
    elif mylv == 51:
        $ max_mylife = 980
    elif mylv == 52:
        $ max_mylife = 1020
    elif mylv == 53:
        $ max_mylife = 1060
    elif mylv == 54:
        $ max_mylife = 1100
    elif mylv == 55:
        $ max_mylife = 1150
    elif mylv == 56:
        $ max_mylife = 1200
    elif mylv == 57:
        $ max_mylife = 1250
    elif mylv == 58:
        $ max_mylife = 1300
    elif mylv == 59:
        $ max_mylife = 1350
    elif mylv == 60:
        $ max_mylife = 1400
    elif mylv == 61:
        $ max_mylife = 1450
    elif mylv == 62:
        $ max_mylife = 1500
    elif mylv == 63:
        $ max_mylife = 1550
    elif mylv == 64:
        $ max_mylife = 1600
    elif mylv == 65:
        $ max_mylife = 1650
    elif mylv == 66:
        $ max_mylife = 1700
    elif mylv == 67:
        $ max_mylife = 1750
    elif mylv == 68:
        $ max_mylife = 1800
    elif mylv == 69:
        $ max_mylife = 1850
    elif mylv == 70:
        $ max_mylife = 1900
    elif mylv == 71:
        $ max_mylife = 1950
    elif mylv == 72:
        $ max_mylife = 2000
    elif mylv == 73:
        $ max_mylife = 2050
    elif mylv == 74:
        $ max_mylife = 2100
    elif mylv == 75:
        $ max_mylife = 2150
    elif mylv == 76:
        $ max_mylife = 2200
    elif mylv == 77:
        $ max_mylife = 2250
    elif mylv == 78:
        $ max_mylife = 2300
    elif mylv == 79:
        $ max_mylife = 2350
    elif mylv == 80:
        $ max_mylife = 2400
    elif mylv == 81:
        $ max_mylife = 2450
    elif mylv == 82:
        $ max_mylife = 2500
    elif mylv == 83:
        $ max_mylife = 2550
    elif mylv == 84:
        $ max_mylife = 2600
    elif mylv == 85:
        $ max_mylife = 2650
    elif mylv == 86:
        $ max_mylife = 2700
    elif mylv == 87:
        $ max_mylife = 2750
    elif mylv == 88:
        $ max_mylife = 2800
    elif mylv == 89:
        $ max_mylife = 2900
    elif mylv == 90:
        $ max_mylife = 3000
    elif mylv == 91:
        $ max_mylife = 3100
    elif mylv == 92:
        $ max_mylife = 3200
    elif mylv == 93:
        $ max_mylife = 3300
    elif mylv == 94:
        $ max_mylife = 3400
    elif mylv == 95:
        $ max_mylife = 3500
    elif mylv == 96:
        $ max_mylife = 3600
    elif mylv == 97:
        $ max_mylife = 3700
    elif mylv == 98:
        $ max_mylife = 3800
    elif mylv == 99:
        $ max_mylife = 3900
    elif mylv == 100:
        $ max_mylife = 4000
    # Расчёт HP для >100 уровней
    else:
        $ hp_mult += 1
        $ max_mylife = mylv * hp_mult

    return

# Подсчёт непотребств в дневник
label count(finish1, finish2):
    $ persistent.count_end += 1

    if finish1 ==  1:
        $ persistent.count_nakadashi += 1
    elif finish1 ==  2:
        $ persistent.count_sakuseikikan += 1
    elif finish1 ==  3:
        $ persistent.count_sumata += 1
    elif finish1 ==  4:
        $ persistent.count_fera += 1
    elif finish1 ==  5:
        $ persistent.count_tekoki += 1
    elif finish1 ==  6:
        $ persistent.count_ashikoki += 1
    elif finish1 ==  7:
        $ persistent.count_paizuri += 1
    elif finish1 ==  8:
        $ persistent.count_analsex += 1
    elif finish1 ==  9:
        $ persistent.count_analseme += 1
    elif finish1 == 10:
        $ persistent.count_syokusyu += 1
    elif finish1 == 11:
        $ persistent.count_slime += 1
    elif finish1 == 12:
        $ persistent.count_plant += 1
    elif finish1 == 13:
        $ persistent.count_wing += 1
    elif finish1 == 14:
        $ persistent.count_buturi += 1
    elif finish1 == 15:
        $ persistent.count_nenmaku += 1
    elif finish1 == 16:
        $ persistent.count_role += 1
    elif finish1 == 17:
        $ persistent.count_ganmenkizyou += 1
    elif finish1 == 18:
        $ persistent.count_kiss += 1
    elif finish1 == 19:
        $ persistent.count_sipppo += 1
    elif finish1 == 20:
        $ persistent.count_ito += 1
    elif finish1 == 21:
        $ persistent.count_onani += 1
    elif finish1 == 22:
        $ persistent.count_kami += 1
    elif finish1 == 23:
        $ persistent.count_tikubi += 1
    elif finish1 == 24:
        $ persistent.count_zenshin += 1
    elif finish1 == 25:
        $ persistent.count_kusuguri += 1
    elif finish1 == 26:
        $ persistent.count_kaze += 1
    elif finish1 == 27:
        $ persistent.count_ihuku += 1
    elif finish1 == 28:
        $ persistent.count_zensin += 1
    elif finish1 == 29:
        $ persistent.count_houyou += 1
    elif finish1 == 30:
        $ persistent.count_tissoku += 1
    elif finish1 == 31:
        $ persistent.count_yuuwaku += 1
    elif finish1 == 32:
        $ persistent.count_sonota += 1
    elif finish1 == 33:
        $ persistent.count_marunomi += 1
    elif finish1 == 34:
        $ persistent.count_sirikoki += 1

    if kousoku > 0:
        $ persistent.count_kousoku += 1

    if status == 1:
        $ persistent.count_koukotu += 1
    elif status == 2:
        $ persistent.count_mahi += 1
    elif status == 4:
        $ persistent.count_yuwaku += 1
    elif status == 5:
        $ persistent.count_kuppuku += 1
    elif status == 6:
        $ persistent.count_sekika += 1
    elif status == 7:
        $ persistent.count_konran += 1

    if finish2 == 1:
        $ persistent.count_b001 += 1
    elif finish2 == 2:
        $ persistent.count_b002 += 1
    elif finish2 == 3:
        $ persistent.count_b003 += 1
    elif finish2 == 4:
        $ persistent.count_b004 += 1
    elif finish2 == 5:
        $ persistent.count_b005 += 1
    elif finish2 == 6:
        $ persistent.count_b006 += 1
    elif finish2 == 7:
        $ persistent.count_b007 += 1
    elif finish2 == 8:
        $ persistent.count_b008 += 1
    elif finish2 == 9:
        $ persistent.count_b009 += 1
    elif finish2 == 10:
        $ persistent.count_b010 += 1
    elif finish2 == 11:
        $ persistent.count_b011 += 1
    elif finish2 == 12:
        $ persistent.count_b012 += 1
    elif finish2 == 13:
        $ persistent.count_b013 += 1
    elif finish2 == 14:
        $ persistent.count_b014 += 1
    elif finish2 == 15:
        $ persistent.count_b015 += 1
    elif finish2 == 16:
        $ persistent.count_b016 += 1
    elif finish2 == 17:
        $ persistent.count_b017 += 1

    if persistent.skill_num == 0:
        return
    else:
        $ persistent.skills[persistent.skill_num][1] += 1

    return

# Функция вычисления урона для персонажа
label damage(d, line="single"):
    $ DELAY = 0.5

    if player == 0:
        $ ori_name = "Лука"
    elif player == 2:
        $ ori_name = "Alma Elma"
    elif player == 3:
        $ ori_name = "Erubetie"
    elif player == 4:
        $ ori_name = "Tamamo"
    elif player == 5:
        $ ori_name = "Granberia"

    # Исключает уворот или контратаку против яда
    if poison_damage == 0:
        if ng == 1 and aqua == 2 and wind == 5 and kousoku < 1:
            call serenegale_guard
            jump common_main
        if ng == 1 and aqua > 0 and kousoku < 1:
            call aqua_guard
            jump common_main
        if ng == 1 and wind > 0 and kousoku < 1:
            call wind_guard
            jump common_main
        if daten > 0 and kousoku < 1 and (status == 0 or status == 9):
            call skill22x
            jump common_main
        if aqua == 2:
            $ aqua_end = 1

    # Если в аргумент функции поступил массив из двух чисел (d[0] и d[1]), вычисляет случайное число между ними.
    if len(d) == 2:
        $ damage = rnd2(d[0], d[1])
    # В противном случае передаёт аргумент d переменной damage без изменений.
    else:
        $ damage = d[0]

    # Прорыв защиты
    if guard_break > 0 and bougyo > 0:
        call guard_break

    # Нормальный эффект удара
    if a_effect1 == 0:
        play sound "audio/se/damage.ogg"
        $ renpy.transition(hpunch, "master")
        $ renpy.transition(hpunch, "screens")
        $ DELAY = 0.5
    # Эффект сильной тряски
    elif a_effect1 == 1 or a_effect2 == 1:
        play sound "audio/se/damage2.ogg"
        $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "master")
        $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "screens")
        $ DELAY = 2.5
    # Эффект слабой тряски
    elif a_effect1 == 2 or a_effect2 == 2:
        play sound "audio/se/ikazuti.ogg"
        $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "master")
        $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "screens")
        $ DELAY = 1.5

    $ a_effect1 = 0

    # Если эта атака ядом или атака damage_kotei, урон не будет увеличиваться или ослабляться
    if damage_kotei == 1 or poison_damage > 0:
        $ damage_kotei = 0
    else:
        if enemylv > 0:
            $ damage += enemylv*3
        if enemy_fire == 1:
            $ damage = damage * 15/10
        if enemy_fire == 2:
            $ damage = damage * 18/10
        if enemy_fire == 3:
            $ damage = damage * 22/10
        if bougyo == 1:
            $ damage = damage / 2

        if bougyo == 2:
            $ damage = damage * 2/3

        if earth > 1:
            $ damage = damage * (100 - earth_keigen) / 100

        if aqua == 1:
            $ damage = damage * 4/5

        if enemy_wind > 0 and earth > 0:
            $ damage = damage * (100 - earth_keigen_el) / 100
            $ earth_keigen_el = 0

        # Уменьшение урона в зависимости от отношений с Тамамо. :3
        if party_member[1] == 3 and tamamo_rep > 9:
            $ damage = damage * (100 - (tamamo_rep*2/3))/100

        if blackwind_turn > 0:
            $ damage = damage * 2/3

        if erpentagram_turn > 0 or unpentagram_turn > 0:
            $ damage = damage * 20/100

        if titanguard_turn > 0 or gnomaren > 0:
            $ damage = damage * 1/100

        if potion == 2:
            $ damage = damage * 65/100
        if potion == 5:
            $ damamge = 0

    if damage == 0:
        $ damage = 1

    # Если удар единичный, вывод одну строку.
    if line == 'single':
        pause DELAY
        "[ori_name!t] получает [damage] урона!"

    # В противном случае выводит от 1 до 3 строк.
    elif line == 'first':
        pause DELAY
        "[ori_name!t] получает [damage] урона!{w}{nw}"

    elif line == 'second':
        extend "{w=[DELAY]}\n[ori_name!t] получает [damage] урона!{w}{nw}"

    elif line == 'last':
        extend "{w=[DELAY]}\n[ori_name!t] получает [damage] урона!"

    # Если урон больше текущего HP, не позволяет здоровью уйти в минус
    if mylife - damage < 0:
        $ mylife = 0
    else:
        $ mylife -= damage

    $ kadora_damage = 1
    $ aqua_counter = 0
    if damage_nobr > 0:
        $ damage_nobr = 0
    return

# Изменяет спрайт противника в соответствии с состоянием
label status_print(face=0):
    $ bougyo = 0

    if kousoku == 0:
        $ hanyo[4] = 0
        hide ct
    elif kousoku != 0 and face == 0:
        $ renpy.show(tatie4, at_list=[monster_pos], zorder=5)

    $ renpy.transition(dissolve, layer="master")

    return

# Спад вражеских элементов
label enemy_element_sel:
    if enemy_wind > 0 and enemy_windturn < 2:
        call enemy_element_sel1
    if enemy_earth > 0 and enemy_earthturn < 2:
        call enemy_element_sel2
    if enemy_aqua > 0 and enemy_aquaturn < 2:
        call enemy_element_sel3
    if enemy_fire > 0 and enemy_fireturn < 2:
        call enemy_element_sel4

    return

label enemy_element_sel1:
    $ enemy_wind = 0
    $ enemy_windturn = 0
    $ enemy_spiritcount -= 1
    "Сила ветра пропадает!"

    return

label enemy_element_sel2:
    $ enemy_earth = 0
    $ enemy_earthturn = 0
    $ enemy_spiritcount -= 1
    "Сила земли пропадает!"

    return

label enemy_element_sel3:
    $ enemy_aqua = 0
    $ enemy_aquaturn = 0
    $ enemy_spiritcount -= 1
    "Сила воды пропадает!"

    return

label enemy_element_sel4:
    $ enemy_fire = 0
    $ enemy_fireturn = 0
    $ enemy_spiritcount -= 1
    "Сила огня пропадает!"

    return

# Изменение лица противника по переданному аргументу
label face(param=None):
    if param in (1, 2):
        if kousoku > 0:
            return

        if param == 1:
            if kousan > 0 or (max_mylife > mylife*5 and sel_kiki == 1):
                $ param = 2

    elif param == 3:
        hide ct

    $ renpy.show(getattr(store, "tatie%d" % param), at_list=[monster_pos], zorder=5)

    if tekikazu > 1:
        $ renpy.show(getattr(store, "tatie%db" % param), at_list=[monster_pos_b], zorder=4)

    if tekikazu > 2:
        $ renpy.show(getattr(store, "tatie%dc" % param), at_list=[monster_pos_c], zorder=3)

    if tekikazu > 3:
        $ renpy.show(getattr(store, "tatie%dd" % param), at_list=[monster_pos_d], zorder=2)

    $ renpy.transition(dissolve, layer="master")
    return

# Плохая концовка
label badend:
    $ persistent.hsean = 0
    $ store._rollback = True
    $ store._skipping = True
    stop music fadeout 1
    scene expression "#000" as bg with Dissolve(1.0)
    pause 1.0

    if _in_replay and replay_h:
        $ renpy.end_replay()

    show screen badend(tatie2, zmonster_x, bad1, bad2)
    pause
    hide screen badend
    scene expression "#000" as bg with Dissolve(1.0)

    if _in_replay:
        $ end_n = 100

    if mylv_sin > mylv:
        $ mylv = mylv_sin
        call maxhp
        call maxmp

    call syokika

# Опции после концовки
label badend_b:
    $ nobtn = 0
    scene bg black

    menu:
        "Переиграть{#menu_badend_b}" if end_n < 2: #553
            jump afterend_return

        "Переиграть{#menu_badend_b}" if end_n >= 2: #553
            jump afterend_return

        "Попросить совет{#menu_badend_b}" if end_n >= 2 and ng > 1: #555
            call hansei
            jump afterend_return

        "Изменить сложность{#menu_badend_b}" if end_n >= 2: #561
            jump difficulty_henkou

        "В главное меню{#menu_badend_b}" if not _in_replay: #559
            $ renpy.full_restart()

        "Назад в Монстропедию{#menu_badend_b}" if _in_replay: #563
            $ renpy.end_replay()

# плохая концовка с одной строчкой и без восстановления уровня
label badend2:
    stop music fadeout 1
    scene expression "#000" as bg with Dissolve(1.0)
    call syokika
    $ end_n = 0
    jump badend_b


label afterend_return:
    $ cont = 1

    if end_n < 2:
        $ renpy.jump(end_go)

    $ renpy.jump(lavel + "_start")


label afterend_goback:
    $ exp -= exp_minus
    call lvup_check

    if ivent04 == 0:
        $ item04 = 0

    if end_n == 7 and item15 == 1:
        $ item11 = 1
        $ item15 = 0
    elif end_n == 8 and item08 == 0:
        $ item04 = 0
        $ item07 = 0

    if end_n == 3:
        $ skill01 = 2
        $ _bg = "bg 019"
        play music "audio/bgm/field1.ogg"
        jump map1
    elif end_n == 4:
        $ skill01 = 5
        $ item12 = 0
        $ _bg = "bg 041"
        play music "audio/bgm/field3.ogg"
        jump map2
    elif end_n == 5:
        $ _bg = "bg 062"
        play music "audio/bgm/sabaku.ogg"
        jump map3
    elif end_n == 6:
        $ _bg = "bg 019"
        play music "audio/bgm/field3.ogg"
        jump map4
    elif end_n == 7:
        $ _bg = "bg 041"
        play music "audio/bgm/field3.ogg"
        jump map5
    elif end_n == 8:
        $ _bg = "bg 099"
        play music "audio/bgm/field3.ogg"
        jump map6
    elif end_n == 9:
        $ skill01 = 14
        $ skill02 = 2
        $ skill03 = 2
        $ skill04 = 2
        $ skill05 = 2
        $ _bg = "bg 142"
        play music "audio/bgm/manotairiku.ogg"
        jump map7
    elif end_n == 10:
        play music "audio/bgm/field4.ogg"
        jump map8
    elif end_n == 11:
        play music "audio/bgm/field4.ogg"
        jump map9
    elif end_n == 12:
        play music "audio/bgm/field4.ogg"
        jump map10
    elif end_n == 13:
        play music "audio/bgm/field4.ogg"
        jump map11
    elif end_n == 14:
        play music "audio/bgm/field4.ogg"
        jump map12
    elif end_n == 15:
        play music "audio/bgm/field4.ogg"
        jump map13

# Изменение сложности в игре
label difficulty_henkou:
    scene bg black
    pause 0.5
    show expression Text(_("Сложность?"), size=24, style="ui_text"):
        pos (172, 143) #700

    menu:
        "Нормальный{#Difficulty_henkou}":
            $ Difficulty = 1

        "Тяжёлый{#Difficulty_henkou}":
            $ Difficulty = 2

        "Парадоксальный{#Difficulty_henkou}":
            $ Difficulty = 3

    scene expression "#000" as bg
    $ cont = 1

    if end_n < 2:
        $ renpy.jump(end_go)

    $ renpy.jump(lavel + "_start")

# Высасывание уровня
label lvdrain:
    $ mylv -= drain

    if mylv < 1:
        $ mylv = 1

    if mylv == 1:
        $ persistent.count_mylv0 = 1

    call maxhp
    call maxmp

    if max_mylife < mylife:
        $ mylife = max_mylife

    call maxmp
    play sound "audio/se/lvdown.ogg"

    "Уровень [ori_name!t] уменьшился на [drain] единиц!"

    $ enemylv += drain
    $ persistent.count_drainlv += drain

    "Уровень [monster_name!t] увеличился на [drain] единиц!"

    return

# Функция вычисления урона для противника
label enemylife(line="single"):
    if not renpy.get_skipping():
        $ renpy.show(img_tag, at_list=[slash, alpha_off])

    if enemy_num > 1:
        call enemylife_multiple
        return
    if no_hit > 0:
        call no_hit
        return
    if enemy_wind == 4 and aqua != 2:
        call enemy_windguard
        return
    if skill04 > 0 and (wind != 3 or wind < 4) and knight_attack < 1 and enemy_aqua > 1 and result != 17:
        $ ransu = rnd2(1,100)
        if enemy_aqua == 2 and ransu < 76:
            return_to enemy_aquaguard
        elif enemy_aqua == 3 and ransu < 38:
            return_to enemy_aquaguard
        elif enemy_aqua == 4 and ransu < 101:
            return_to enemy_aquaguard

        if enemy_wind == 2 and ransu < 51:
            call enemy_windguard
            return_to common_attack_end
        elif enemy_wind == 3 and ransu < 76:
            call enemy_windguard
            return_to common_attack_end

        if enemy_holy == 1:
            return_to enemy_holyguard

    $ damage = abairitu * mylv * damage_keigen / 2000
    if enrage_turn > 0:
        $ damage = damage * 2
    $ damage = rnd2(damage * 9 / 10, damage * 11 / 10)
    if party_member[1] == 2 and granberia_rep > 9:
        call enemylife_gran
    if fire > 2:
        call enemylife_sala

    if line == 'single':
        pause .5

        "[ori_name!t] наносит [damage] урона!"

    elif line == 'first' or line == 1:
        $ s2 = renpy.substitute("[ori_name!t] наносит [damage] урона!")
        pause .5

        "[s2!t]{w}{nw}"
    elif line == 'second' or line == 2:
        $ s3 = renpy.substitute("[ori_name!t] наносит [damage] урона!")

        extend "{w=.5}\n[s3!t]{w}{nw}"
    elif line == 'last' or line == 3:
        $ s4 = renpy.substitute("[ori_name!t] наносит [damage] урона!")

        extend "{w=.5}\n[s4!t]"

    if skill_sub != 2:

        if enemylife - damage < 0:
            $ enemylife = 0
        else:
            $ enemylife -= damage

    $ is_damaged = 1
    if damage_nobr > 0:
        $ damage_nobr = 0

    return


label enemylife2(line="single"):
    if not renpy.get_skipping():
        $ renpy.show(img_tag, at_list=[slash, alpha_off])

    if isinstance(line, str) == False:
        if line == 1:
            $ line = "first"
        elif line == 2:
            $ line = "second"
        elif line == 3:
            $ line = "last"

    if line == 'single':
        pause .5

        "[ori_name!t] наносит [damage] урона!"

    elif line == 'first':
        $ s2 = renpy.substitute("[ori_name!t] наносит [damage] урона!")
        pause .5

        "[s2!t]{w}{nw}"
    elif line == 'second':
        $ s3 = renpy.substitute("[ori_name!t] наносит [damage] урона!")

        extend "{w=.5}\n[s3!t]{w}{nw}"
    elif line == 'last':
        $ s4 = renpy.substitute("[ori_name!t] наносит [damage] урона!")

        extend "{w=.5}\n[s4!t]"

    if enemylife - damage < 0:
        $ enemylife = 0
    else:
        $ enemylife -= damage

    return

label enemylife3(nm="", line=""):
    if not renpy.get_skipping():
        $ renpy.show(img_tag, at_list=[slash, alpha_off])

    "[nm!t] наносит [damage] урона!"

    if enemylife - damage < 0:
        $ enemylife = 0
    else:
        $ enemylife -= damage

    return

# Усиление урона от Саламандры
label enemylife_sala:
    if salamander_rep < -19:
        $ damage = damage * 75/100
    elif -20 < salamander_rep < -14:
        $ damage = damage * 80/100
    elif -15 < salamander_rep < -9:
        $ damage = damage * 85/100
    elif -10 < salamander_rep < -4:
        $ damage = damage * 90/100
    elif -5 < salamander_rep < 0:
        $ damage = damage * 95/100
    elif 9 < salamander_rep < 20:
        $ damage = damage * 110/100
    elif 19 < salamander_rep < 30:
        $ damage = damage * 115/100
    elif 29 < salamander_rep < 40:
        $ damage = damage * 120/100
    elif 39 < salamander_rep < 50:
        $ damage = damage * 125/100
    elif 49 < salamander_rep:
        $ damage = damage * 130/100

    if enviro_fire == 1:
        $ damage = damage * 120/100
    elif enviro_fire == 2:
        $ damage = damage * 135/100
    elif enviro_fire == 3:
        $ damage = damage * 150/100
    if enviro_aqua == 1:
        $ damage = damage * 90/100
    elif enviro_aqua == 2:
        $ damage = damage * 85/100
    elif enviro_aqua == 3:
        $ damage = damage * 80/100

    return

# Исцеление противника
label enemylife_kaihuku:
    play sound "audio/se/kaihuku.ogg"

    "[monster_name!t] восстанавливает [damage] HP!"

    if enemylife + damage > max_enemylife:
        $ enemylife = max_enemylife
    else:
        $ enemylife += damage

    return

# Уворот противника
label kaihi:
    if not renpy.get_skipping():
        $ renpy.show(img_tag, at_list=[miss])
    pause 1.0
    return


label counter:
    play sound "audio/se/counter.ogg"
    if not renpy.get_skipping():
        show expression Text(_("КОНТРАТАКА!!!"), size=90, bold=True, italic="True", color="#cc0000", outlines=[(4, "#000", 0, 0)], font="fonts/taurus.ttf") zorder 30 as txt onlayer overlay:
            align (0.5, 0.5)
            zoom 0.2
            alpha 0.0
            subpixel True
            easeout 0.75 rotate 360 zoom 1.0 alpha 1.0
            easeout 1.5 alpha 0.0
            #easein 1.0 yalign 0.20
            #easeout 1.4 yalign 1.3 alpha 0.0
    else:
        show expression Text(_("КОНТРАТАКА!!!"), size=90, bold=True, italic="True", color="#cc0000", outlines=[(4, "#000", 0, 0)], font="fonts/taurus.ttf") zorder 30 as txt onlayer overlay:
            align (.5, .5)
    $ renpy.pause(3.2, hard=True)
    hide txt

    return

label guard_break:
    play sound "audio/se/counter.ogg"
    if not renpy.get_skipping():
        show expression Text(_("ПРОРЫВ ЗАЩИТЫ!!!"), size=90, bold=True, italic="True", color="#cc0000", outlines=[(4, "#000", 0, 0)], font="fonts/taurus.ttf") zorder 30 as txt onlayer overlay:
            align (0.5, 0.5)
            zoom 0.2
            alpha 0.0
            subpixel True
            easeout 0.75 rotate 360 zoom 1.0 alpha 1.0
            easeout 1.5 alpha 0.0
            #easein 1.0 yalign 0.20
            #easeout 1.4 yalign 1.3 alpha 0.0
    else:
        show expression Text(_("ПРОРЫВ ЗАЩИТЫ!!!"), size=90, bold=True, italic="True", color="#cc0000", outlines=[(4, "#000", 0, 0)], font="fonts/taurus.ttf") zorder 30 as txt onlayer overlay:
            align (.5, .5)
    $ renpy.pause(3.2, hard=True)
    hide txt
    $ bougyo = 0
    $ print damage
    $ damage = damage * 15/10
    $ print damage
    $ a_effect2 = 1
    $ guard_break = 0
    "[enemy] прорывается через защитную стойку Луки!"

    return

# Капитуляция персонажа
label kousan_syori:
    $ before_action = 53
    $ kousan = 1
    $ persistent.count_kousan += 1

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.transition(Dissolve(1.5), layer="master")
        call musicplay

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    return

# Капитуляция персонажа запрошенным навыком
label onedari_syori:
    $ before_action = 54
    $ kousan = 2
    $ persistent.count_onedari += 1

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.transition(Dissolve(1.5), layer="master")
        call musicplay

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ kousoku = 0
    call status_print

    $ renpy.show(tatie2, at_list=[monster_pos], zorder = 5)

    if tekikazu > 1:
        $ renpy.show(tatie2b, at_list=[monster_pos_b], zorder = 4)

    if tekikazu > 2:
        $ renpy.show(tatie2c, at_list=[monster_pos_c], zorder = 3)

    if tekikazu > 3:
        $ renpy.show(tatie2d, at_list=[monster_pos_d], zorder = 3)

    $ renpy.transition(dissolve, layer="master")
    return


label onedari_syori_k:
    $ before_action = 54
    $ kousan = 2
    $ persistent.count_onedari += 1

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.with_statement(Dissolve(1.5))
        call musicplay

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0

    if kousoku > 0:
        return

    hide ct
    hide bk
    hide bk2

    $ renpy.show(tatie2, at_list=[monster_pos], zorder = 5)

    if tekikazu > 1:
        $ renpy.show(tatie2b, at_list=[monster_pos_b], zorder = 4)

    if tekikazu > 2:
        $ renpy.show(tatie2c, at_list=[monster_pos_c], zorder = 3)

    if tekikazu > 3:
        $ renpy.show(tatie2d, at_list=[monster_pos_d], zorder = 3)

    $ renpy.transition(dissolve, layer="master")
    return


# Стоны
label ikigoe:
    $ ransu = rnd2(1,4)

    if ikigoe != 4 and status == 1:
        if ransu == 1:
            ori "Фуаа..."
        elif ransu == 2:
            ori "Ааа..."
        elif ransu == 3:
            ori "Хааа..."
        elif ransu == 4:
            ori "Слишком... слишком хорошо..."
    elif ikigoe < 2:
        if ransu == 1:
            ori "Ааа... кончаю!"
        elif ransu == 2:
            ori "Фуаааа!"
        elif ransu == 3:
            ori "Аааааа!"
        elif ransu == 4:
            ori "Уууууу!"
    elif ikigoe == 2:
        if ransu == 1:
            ori "Аааахх!"
        elif ransu == 2:
            ori "Ааа!"
        elif ransu == 3:
            ori "Нет... я больше этого не выдержу!"
        elif ransu == 4:
            ori "Ах... слишком хорошо..."
    elif ikigoe == 3:
        if ransu == 1:
            ori "Аргх! Я сейчас кончу!"
        elif ransu == 2:
            ori "К-кончаю!"
        elif ransu == 3:
            ori "Угх! Х-хватит!"
        elif ransu == 4:
            ori "Я... больше не выдержу!"
    elif ikigoe == 4:
        if ransu == 1:
            ori "Ургх...!"
        elif ransu == 2:
            ori "Аргх..."
        elif ransu == 3:
            ori "Ах... ахх..."
        elif ransu == 4:
            ori "Угх... ахх..."

    return


label victory(viclose=0):
    $ battle = 0
    $ saveback = 1
    $ turn_scorer = 0
    $ a_effect = 0

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.transition(Dissolve(1.5), layer="master")

    play music "audio/bgm/fanfale.ogg" noloop
    hide screen hp

    if plural_name == 1:
        "[monster_name!t] убегают прочь!"
    else:
        if appear_name:
            if viclose == 1:
                "[appear_name!t] побеждена!"
            elif viclose == 2:
                "[appear_name!t] убегает прочь!"
        else:
            if viclose == 1:
                "[monster_name!t] побеждена!"
            elif viclose == 2:
                "[monster_name!t] убегает прочь!"

    $ ng = 1
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ daten = 0
    $ store._skipping = True
    $ store._rollback = True
    $ mp_charge = 0
    $ nocharge = 0
    $ plural_name = 0

    $ persistent.count_ko += 1

    if Difficulty != 0:
        if mylife == max_mylife and nodamage == 0:
            $ persistent.count_hpfull = 1

        if Difficulty == 3:
            $ persistent.count_hellvic = 1

        if status == 7:
            $ persistent.count_vickonran = 1

        if counter > 0:
            $ persistent.counterk = 1

        if alice_skill == 1:
            $ persistent.alicek = 1

    $ renpy.end_replay()

    if status == 9:
        call common_poisonfade

    if mylv_sin != mylv:
        $ mylv = mylv_sin

        "После победы над ней, уровень Луки возвращается!"

    $ renpy.stop_predict(tatie1)
    $ renpy.stop_predict(tatie2)
    $ renpy.stop_predict(tatie3)
    $ renpy.stop_predict(tatie4)
    $ renpy.stop_predict(haikei)
    $ renpy.stop_predict_screen("hp")
    $ renpy.save_persistent()
    $ renpy.free_memory()
    return


label victory_plural(viclose=0):
    $ battle = 0
    $ saveback = 1
    $ turn_scorer = 0
    $ a_effect = 0

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.transition(Dissolve(1.5), layer="master")

    $ renpy.music.play("audio/bgm/fanfale.ogg", channel="music", loop=None)
    hide screen hp
    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ store._skipping = True
    $ store._rollback = True

    if appear_name:
        if viclose == 1:
            "[appear_name!t] побеждена!"
        elif viclose == 2:
            "[appear_name!t] убегает прочь!"
    else:
        if viclose == 1:
            "[monster_name!t] побеждена!"
        elif viclose == 2:
            "[monster_name!t] убегает прочь!"

    $ persistent.count_ko += 1

    if mylife == max_mylife and nodamage == 0:
        $ persistent.count_hpfull = 1

    if Difficulty == 3:
        $ persistent.count_hellvic = 1

    if status == 7:
        $ persistent.count_vickonran = 1

    $ renpy.end_replay()

    if mylv_sin != mylv:
        $ mylv = mylv_sin

        "[ori_name!t] получает новый уровень!"

    $ renpy.stop_predict(tatie1)
    $ renpy.stop_predict(tatie2)
    $ renpy.stop_predict(tatie3)
    $ renpy.stop_predict(tatie4)
    $ renpy.stop_predict(haikei)
    $ renpy.stop_predict_screen("hp")
    $ renpy.save_persistent()
    $ renpy.free_memory()
    return


label lose(viclose=0):
    $ battle = 0
    $ saveback = 1
    $ turn_scorer = 0
    $ a_effect = 0

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.with_statement(Dissolve(1.5))

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    hide screen hp

    if viclose == 1:
        "[ori_name!t] проигрывает..."
    elif viclose == 2:
        "[ori_name!t] сдался..."

    play music "audio/bgm/ero1.ogg" fadeout 1

    if turn == 1 and kousan == 0:
        $ persistent.count_onekill = 1

    python:
        for idx in ("wind", "earth", "aqua", "fire"):
            if getattr(store, "enemy_%s" % idx):
                setattr(persistent, "count_enemy_%s" % idx, 1)

    # if enemy_wind > 0:
    #     $ persistent.count_enemy_wind = 1

    # if enemy_earth > 0:
    #     $ persistent.count_enemy_earth = 1

    # if enemy_aqua > 0:
    #     $ persistent.count_enemy_aqua = 1

    # if enemy_fire > 0:
    #     $ persistent.count_enemy_fire = 1

    $ persistent.hsean = 1
    return


label lose2(viclose=0):

    if aqua == 2:
        $ renpy.show(haikei)
        $ renpy.with_statement(Dissolve(1.5))

    $ wind = 0
    $ earth = 0
    $ aqua = 0
    $ fire = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ bougyo = 0

    if viclose == 1:
        "[ori_name!t] проигрывает..."
    elif viclose == 2:
        "[ori_name!t] сдаётся..."

    $ kousan = 3
    $ sel_hphalf = 1
    $ sel_kiki = 1
    $ mylife = 0
    play music "audio/bgm/ero1.ogg" fadeout 1

    if turn == 1 and kousan == 0:
        $ persistent.count_onekill = 1

    python:
        for idx in ("wind", "earth", "aqua", "fire"):
            if getattr(store, "enemy_%s" % idx):
                setattr(persistent, "count_enemy_%s" % idx, 1)

    # if enemy_wind > 0:
    #     $ persistent.count_enemy_wind = 1

    # if enemy_earth > 0:
    #     $ persistent.count_enemy_earth = 1

    # if enemy_aqua > 0:
    #     $ persistent.count_enemy_aqua = 1

    # if enemy_fire > 0:
    #     $ persistent.count_enemy_fire = 1

    return


label lvup:
    $ exp += getexp
    $ mylv_before = mylv

    call lvup_check

    "Лука получает [getexp] очков опыта!"

    if mylv != mylv_before:
        play sound "audio/se/lvup.ogg"

        "[ori_name!t] теперь [mylv] уровня!"

    return


label syasei1:
    with flash
    return


label syasei2:
    play sound "audio/se/syasei.ogg"
    with flash
    with flash
    with flash
    return


label hsean_cut:
    if _in_replay:
        return

    menu:
        "Смотреть H-сцену{#hsean_cut}":
            jump hsean_cut1

        "Пропустить H-сцену{#hsean_cut}":
            jump hsean_cut2


label hsean_cut1:
    return


label hsean_cut2:
    if _in_replay:
        return

    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)
    show expression Text(_("Сцена изнасилования была пропущена."), size=50, pos=(75, 200)) with dissolve #200

    "......"

    return_to badend


label hosyoku_cut:
    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)
    show expression Text(_("Сцена пожирания была пропущена..."), size=50, pos=(50, 200)) with dissolve #200

    "Будучи переваренным монстром, Лука закончил свои приключения..."
    "...."

    jump badend


label hosyoku_cut2:
    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)
    show expression Text(_("Сцена пожирания была пропущена..."), size=50, pos=(50, 200)) as txt with dissolve #200

    "Будучи переваренным монстром, Лука закончил свои приключения..."
    "...."

    $ renpy.end_replay()
    jump badend2


label element_sel:
    if 0 < wind < 4 and wind_turn < 0:
        $ wind = 0
        "Сила ветра исчезает!"

    elif wind == 4 and wind_turn < 0:
        $ wind = 0
        "Танец Падшего Англеа заканчивается!"

    if earth > 0 and earth_turn < 0:
        $ earth = 0
        "Сила земли исчезает!"

    if aqua > 0 and aqua_turn < 0 and skill04 > 0:
        $ aqua = 0
        "Сила воды исчезает!"

    elif aqua > 0 and aqua_turn < 0 and skill04 == 0:
        $ aqua = 0
        "Состояние безмятежности проходит!"

    if fire == 1 and fire_turn < 0:
        $ fire = 0
        "Сила огня исчезает!"

    elif fire == 2 and fire_turn < 0:
        $ fire = 0
        "Сила огня исчезает!"
        $ mp = 0

    elif fire == 3 and fire_turn < 0:
        $ fire = 0
        "Сила огня исчезает!"

    return


label aqua_sel:
    if mp < 0:
        $ mp = 0

    stop music fadeout 1
    $ renpy.show(haikei)
    $ renpy.transition(Dissolve(1.5), layer="master")
    $ aqua = 0
    $ aqua_end = 0
    call musicplay

    "Сила воды исчезает!"

    pause 1.0
    return


label status_sel1:
    if status == 1:
        "Лука находится в трансе..."
    elif status == 2:
        "Тело Луки парализованно..."
    elif status == 4:
        "Лука не может сопротивляться воле монстра..."
    elif status == 5:
        "Лука загипнотизирован... "

        $ renpy.jump(lavel + "_yuwaku_a")
    elif status == 7:
        "Лука в замешательстве..."

        $ renpy.jump(lavel + "_main")
    elif status == 8:
        "Часть тела Луки окаменела..."

        if kousan == 0:
            $ renpy.jump(lavel + "_main")
        elif kousan > 0:
            $ renpy.jump(lavel + "_a")

    elif status == 9:
        "Тело Луки отравлено..."
        $ renpy.jump(lavel + "_main")

    $ renpy.jump(lavel + "_a")


label status_sel2:
    if kousan > 0:
        jump status_sel1

    if Difficulty == 3:
        $ ransu = rnd2(1,2)

        if ransu == 2:
            jump status_sel1

    if status == 1:
        "[ori_name!t] освобождается из транса!"
    elif status == 2:
        "Паралич Луки проходит!"
    elif status == 4:
        "[ori_name!t] приходит в себя!"
    elif status == 5:
        "[ori_name!t] вырывается из гипноза!"
    elif status == 7:
        "[ori_name!t] больше не в замешательстве!"

    $ status = 0
    call status_print

    if owaza_num2 < 2:
        $ owaza_count2a = 0
    elif owaza_num2 == 2:
        $ owaza_count2b = 0
    elif owaza_num2 == 3:
        $ owaza_count2c = 0
    elif owaza_num2 == 4:
        $ owaza_count2d = 0

    $ owaza_num2 = 0
    $ tuika_owaza_count1a = 0
    $ tuika_owaza_count1b = 0
    $ tuika_owaza_count1c = 0
    $ tuika_owaza_count1d = 0
    $ tuika_owaza_count1e = 0
    $ tuika_owaza_count1f = 0
    $ tuika_owaza_count1g = 0
    $ tuika_owaza_count1h = 0
    $ tuika_owaza_count2a = 0
    $ tuika_owaza_count2b = 0
    $ tuika_owaza_count2c = 0
    $ tuika_owaza_count2d = 0
    $ tuika_owaza_count3a = 0
    $ tuika_owaza_count3b = 0
    $ tuika_owaza_count3c = 0
    $ tuika_owaza_count3d = 0
    $ renpy.jump(lavel + "_main")


label status_sel3:
    $ sel1 = status_turn+1

    "[sel1] turns until complete petrification!"

    if status_turn == 0:
        "Only one turn until complete petrification!"

    if kousan > 0:
        $ renpy.jump(lavel + "_a")

    $ renpy.jump(lavel + "_main")


label common_attack:
    $ nikaime = 0

    if aqua > 1:
        call attack_aqua
        jump common_attack_end

    if wind == 3 and earth == 0:
        $ ransu = rnd2(1,100)

        if ransu == 1:
            call attack_kaisin
            $ nikaime = 1
            call attack_kaisin
            $ nikaime = 0
            jump common_attack_end
        elif ransu < 11:
            call attack
            $ nikaime = 1
            call attack_kaisin
            $ nikaime = 0
            jump common_attack_end
        elif ransu < 20:
            call attack_kaisin
            $ nikaime = 1
            call attack
            $ nikaime = 0
            jump common_attack_end
        else:
            call attack
            $ nikaime = 1
            call attack
            $ nikaime = 0
            jump common_attack_end
    elif wind == 3 and earth == 2:
        $ ransu = rnd2(1,25)

        if ransu == 1:
            call attack_kaisin
            $ nikaime = 1
            call attack_kaisin
            $ nikaime = 0
            jump common_attack_end
        elif ransu < 6:
            call attack
            $ nikaime = 1
            call attack_kaisin
            $ nikaime = 0
            jump common_attack_end
        elif ransu < 11:
            call attack_kaisin
            $ nikaime = 1
            call attack
            $ nikaime = 0
            jump common_attack_end
        else:
            call attack
            $ nikaime = 1
            call attack
            $ nikaime = 0
            jump common_attack_end
    elif wind == 3 and earth == 3:
        call attack_kaisin
        $ nikaime = 1
        call attack_kaisin
        $ nikaime = 0
        jump common_attack_end

    $ ransu = rnd2(1,100)

    if ransu > kaihi and wind < 2:
        call attack_miss
        jump common_attack_end
    elif ransu > kaihi and wind == 4 and skill02 == 0:
        call attack_miss
        jump common_attack_end
    elif ransu > kaihi_el and enemy_wind > 0:
        call attack_miss
        jump common_attack_end
    elif earth == 3:
        call attack_kaisin
        jump common_attack_end

    $ ransu = rnd2(1,10)

    if ransu == 9 and earth == 2:
        call attack_kaisin
        jump common_attack_end
    elif ransu > 7 and max_mylife < mylife*5:
        call attack_kaisin
        jump common_attack_end
    elif ransu == 10:
        call attack_kaisin
        jump common_attack_end

    call attack
    jump common_attack_end


label attack:
    $ before_action = 1
    $ ransu = rnd2(1,3)

    if nikaime != 1:
        if ransu == 1:
            ori "Хаа!"
        elif ransu == 2:
            ori "Ораа!"
        elif ransu == 3:
            ori "Получай!"

    "[ori_name!t] атакует!"

    if nikaime == 0:
        call mp_kaihuku

    play sound "audio/se/slash.ogg"

    if fire == 0:
        $ abairitu = 100 + mylv
    elif fire == 1:
        $ abairitu = 120 + mylv * 6 / 5
    elif fire == 2:
        $ abairitu = 170 + mylv * 17 / 10
    elif fire == 3:
        $ abairitu = 200 + mylv * 2

    call enemylife
    return


label attack_kaisin:
    $ before_action = 2
    $ ransu = rnd2(1,4)

    if nikaime != 1:
        if ransu == 1:
            ori "Открылась!"
        elif ransu == 2:
            ori "СУУУПЕР АТАКААА!"
        elif ransu == 3:
            ori "Сейчас ты у меня получишь!"

    "Критическое попадание!"

    if nikaime == 0:
        call mp_kaihuku

    play sound "audio/se/slash3.ogg"

    if fire == 0:
        $ abairitu = 170 + mylv * 17 / 10
    elif fire == 1:
        $ abairitu = 200 + mylv * 2
    elif fire == 2:
        $ abairitu = 250 + mylv * 5 / 2
    elif fire == 3:
        $ abairitu = 300 + mylv * 3

    call enemylife
    return


label attack_miss:
    $ before_action = 3
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Чёрт!"
    elif ransu == 2:
        ori "Ух-ох!"
    elif ransu == 3:
        ori "Дерьмо!"

    "[ori_name!t] атакует!"

    call mp_kaihuku
    play sound "audio/se/miss.ogg"

    "Промах!"

    return


label attack_aqua:
    $ before_action = 4
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori ".............."
    elif ransu == 2:
        ori "Вижу!"
    elif ransu == 3:
        ori "Вверяя свой меч потоку..."

    call show_skillname("Клинок Стоячей Воды")

    "Меч Луки, сверкая, направляется к противнику!"

    if aqua > 2:
        call mp_kaihuku

    if aqua > 2:
        $ abairitu = 140 + mylv * 7 / 5

    if aqua == 2:
        $ abairitu = 440 + mylv * 4

    if earth == 3:
        $ abairitu = abairitu * 3 / 2

    if fire == 3:
        $ abairitu = abairitu * 3 / 2

    play sound "audio/se/slash4.ogg"

    call enemylife
    call hide_skillname

    if not wind == 3:
        return

    call show_skillname("Клинок Стоячей Воды")

    "Сверкая подобно молнии, меч Луки возвращается в ножны!"

    if aqua > 2:
        $ abairitu = 140 + mylv * 7 / 5

    if aqua == 2:
        $ abairitu = 440 + mylv * 4

    if earth == 3:
        $ abairitu = abairitu * 3 / 2

    if fire == 3:
        $ abairitu = abairitu * 3 / 2

    play sound "audio/se/slash4.ogg"

    call enemylife
    call hide_skillname
    return


label common_attack_end:
    call hide_skillname

    if enemy_aqua > 1 and enemy_aqua != 3 and enemy_aqua != 4 and result == 37:
        $ enemy_aquaturn = 0
        call enemy_element_sel

    $ guard_on = 0

    if bougyo == 1:
        $ guard_on = 1
    elif bougyo == 2 and before_action == 42 and skill01 > 26:
        $ guard_on = 1
    elif bougyo == 2 and before_action == 91 and skill01 > 26:
        $ guard_on = 1

    if guard_on == 0:
        $ bougyo = 0

    if enemylife == 0:
        $ renpy.jump(lavel + "_v")

    if before_action != 3 or before_action != 59:
        if counter > 0:
            call counter
            $ renpy.call(lavel + "_counter")
            jump common_main

    $ luka_turn += 1

    if luka_turn < luka_maxturn:
        $ renpy.jump(lavel + "_main")

    $ renpy.jump(lavel + "_a")


label common_bougyo:
    if player == 0:
        $ ori_name = "Лука"
    elif player == 2:
        $ ori_name = "Alma Elma"
    elif player == 3:
        $ ori_name = "Erubetie"
    elif player == 4:
        $ ori_name = "Tamamo"
    elif player == 5:
        $ ori_name = "Granberia"

    $ before_action = 51
    $ bougyo = 1
    $ mp_charge = 0

    "[ori_name!t] принимает защитную стойку!"

    $ renpy.jump(lavel + "_a")


label common_nasugamama:
    if player == 0:
        $ ori_name = "Лука"
    elif player == 2:
        $ ori_name = "Alma Elma"
    elif player == 3:
        $ ori_name = "Erubetie"
    elif player == 4:
        $ ori_name = "Tamamo"
    elif player == 5:
        $ ori_name = "Granberia"

    $ before_action = 52
    $ mp_charge = 0

    "[ori_name!t] остается неподвижным."

    $ renpy.jump(lavel + "_a")


label common_mogaku:
    $ before_action = 7
    $ ransu = rnd2(1,3)

    if result == 1 and ransu == 1:
        ori "Угх..."
    elif result == 1 and ransu == 2:
        ori "Грр... Чёрт возьми!"
    elif result == 1 and ransu == 3:
        ori "Отпусти меня!"
    elif result == 2 and ransu == 1:
        ori "Гх..."
    elif result == 2 and ransu == 2:
        ori "Гх... отстань!"
    elif result == 2 and ransu == 3:
        ori "Грр... отпусти!"

    if result == 1:
        "[ori_name!t] пытается атаковать!"

        call mp_kaihuku
    elif result == 2:
        "[ori_name!t] начинает вырываться!"

    if genmogaku != 99:
        if result == 2:
            $ genmogaku -= 1

            if genmogaku < 0:
                $ genmogaku = 0

    if result == 1:
        "[mogaku_anno3!t]"
    elif result == 2:
        "[mogaku_anno1!t]"

    $ ransu = rnd2(1,5)

    if ransu == 1:
        $ sel1 = mogaku_sel1
    elif ransu == 2:
        $ sel1 = mogaku_sel2
    elif ransu == 3:
        $ sel1 = mogaku_sel3
    elif ransu == 4:
        $ sel1 = mogaku_sel4
    elif ransu == 5:
        $ sel1 = mogaku_sel5

    if sel1:
       enemy "[sel1!t]"

    $ renpy.jump(lavel + "_a")


label common_mogaku2:
    $ before_action = 8

    if Difficulty == 2:
        $ ransu = rnd2(1,10)

        if ransu == 5:
            jump common_mogaku
    elif Difficulty == 3:
        $ ransu = rnd2(1,2)

        if ransu == 2:
            jump common_mogaku

    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Грр....!"
    elif ransu == 2:
        ori "Чёрт тебя побери...!"
    elif ransu == 3:
        ori "О-отпусти!"

    play sound "audio/se/dassyutu.ogg"

    $ renpy.show(tatie3, at_list=[monster_pos], zorder=5)

    if tekikazu > 1:
        $ renpy.show(tatie3b, at_list=[monster_pos_b], zorder=4)

    if tekikazu > 2:
        $ renpy.show(tatie3c, at_list=[monster_pos_c], zorder=3)

    if tekikazu > 3:
        $ renpy.show(tatie3d, at_list=[monster_pos_d], zorder=2)

    $ kousoku = 0
    call status_print

    "[ori_name!t] борется интенсивнее и вырывается на свободу!"

    if owaza_num1 < 2:
        $ owaza_count1a = 0
    elif owaza_num1 == 2:
        $ owaza_count1b = 0
    elif owaza_num1 == 3:
        $ owaza_count1c = 0
    elif owaza_num1 == 4:
        $ owaza_count1d = 0

    $ owaza_num1 = 0
    $ tuika_owaza_count1a = 0
    $ tuika_owaza_count1b = 0
    $ tuika_owaza_count1c = 0
    $ tuika_owaza_count1d = 0
    $ tuika_owaza_count1e = 0
    $ tuika_owaza_count1f = 0
    $ tuika_owaza_count1g = 0
    $ tuika_owaza_count1h = 0
    $ tuika_owaza_count2a = 0
    $ tuika_owaza_count2b = 0
    $ tuika_owaza_count2c = 0
    $ tuika_owaza_count2d = 0
    $ tuika_owaza_count3a = 0
    $ tuika_owaza_count3b = 0
    $ tuika_owaza_count3c = 0
    $ tuika_owaza_count3d = 0
    $ sel1 = mogaku_dassyutu1
    $ sel2 = mogaku_dassyutu2

    if sel1:
        enemy "[sel1!t]"

    if sel2:
        enemy "[sel2!t]"

    call face(param=1)
    jump common_main


label mogaku_earth1:
    $ before_action = 7
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Гх...!"
    elif ransu == 2:
        ori "О-отпусти!"
    elif ransu == 3:
        ori "Чёрт возьми!"

    if genmogaku != 99:
        $ genmogaku -= 1

    if genmogaku < 0:
        $ genmogaku = 0

    play sound "audio/se/power.ogg"

    "[ori_name!t]'s body is filled with the power of the Earth, giving him incredible strength!"
    "[mogaku_anno1!t]"

    $ ransu = rnd2(1,5)

    if ransu == 1:
        $ sel1 = mogaku_sel1
    elif ransu == 2:
        $ sel1 = mogaku_sel2
    elif ransu == 3:
        $ sel1 = mogaku_sel3
    elif ransu == 4:
        $ sel1 = mogaku_sel4
    elif ransu == 5:
        $ sel1 = mogaku_sel5

    if sel1:
        "[sel1!t]"

    $ renpy.jump(lavel + "_a")


label mogaku_earth2:
    $ before_action = 8

    if Difficulty == 3:
        $ ransu = rnd2(1,2)

        if ransu == 2:
            jump mogaku_earth1

    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "С этой силой...!"
    elif ransu == 2:
        ori "С силой Гномы...!"
    elif ransu == 3:
        ori "С силой земли...!"

    play sound "audio/se/power.ogg"

    "Тело [ori_name!t] наполняется силой земли, дающая ему небывалую мощь!"

    play sound "audio/se/dassyutu.ogg"

    $ renpy.show(tatie3, at_list=[monster_pos], zorder=5)

    if tekikazu > 1:
        $ renpy.show(tatie3b, at_list=[monster_pos_b], zorder=4)

    if tekikazu > 2:
        $ renpy.show(tatie3c, at_list=[monster_pos_c], zorder=3)

    if tekikazu > 3:
        $ renpy.show(tatie3d, at_list=[monster_pos_d], zorder=2)

    $ kousoku = 0
    call status_print

    "[mogaku_anno2!t]"

    if owaza_num1 < 2:
        $ owaza_count1a = 0
    elif owaza_num1 == 2:
        $ owaza_count1b = 0
    elif owaza_num1 == 3:
        $ owaza_count1c = 0
    elif owaza_num1 == 4:
        $ owaza_count1d = 0

    $ owaza_num1 = 0
    $ tuika_owaza_count1a = 0
    $ tuika_owaza_count1b = 0
    $ tuika_owaza_count1c = 0
    $ tuika_owaza_count1d = 0
    $ tuika_owaza_count1e = 0
    $ tuika_owaza_count1f = 0
    $ tuika_owaza_count1g = 0
    $ tuika_owaza_count1h = 0
    $ tuika_owaza_count2a = 0
    $ tuika_owaza_count2b = 0
    $ tuika_owaza_count2c = 0
    $ tuika_owaza_count2d = 0
    $ tuika_owaza_count3a = 0
    $ tuika_owaza_count3b = 0
    $ tuika_owaza_count3c = 0
    $ tuika_owaza_count3d = 0

    $ sel1 = mogaku_earth_dassyutu1
    $ sel2 = mogaku_earth_dassyutu2

    if sel1:
        enemy "[sel1!t]"

    if sel2:
        enemy "[sel2!t]"

    call face(param=1)
    jump common_main


label common_miss2:
    $ before_action = 9
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Haa!"
    elif ransu == 2:
        ori "Ora!"
    elif ransu == 3:
        ori "Take this!"

    "[ori_name!t] attacks!"

    if mogaku_anno4:
        "[mogaku_anno4!t]"

    call mp_kaihuku
    play sound "audio/se/slash.ogg"
    call enemylife
    $ kousoku_hp -= damage

    if enemylife == 0:
        $ kousoku = 0
        call status_print
        $ renpy.jump(lavel + "_v")

    jump common_attack_end


label common_miss3:
    $ before_action = 10
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Лука ATTAAACK!"
    elif ransu == 2:
        ori "An opening!"
    elif ransu == 3:
        ori "Get... off... of... me!"

    "[ori_name!t] attacks!"
    "[mogaku_anno3!t]"

    call mp_kaihuku
    play sound "audio/se/slash3.ogg"
    $ abairitu = 200
    call enemylife

    if enemylife == 0:
        $ renpy.jump(lavel + "_v")

    play sound "audio/se/dassyutu.ogg"
    call face(param=3)

    $ sel1 = mogaku_kaisin1
    $ sel2 = mogaku_kaisin2

    if sel1:
        enemy "[sel1!t]"

    if sel2:
        enemy "[sel2!t]"

    $ kousoku = 0
    call status_print

    "[ori_name!t] breaks free!"

    $ owaza_count1a = 0
    $ owaza_count1b = 0
    $ owaza_count1c = 0
    $ owaza_count1d = 0
    $ tuika_owaza_count1a = 0
    $ tuika_owaza_count1b = 0
    $ tuika_owaza_count1c = 0
    $ tuika_owaza_count1d = 0
    $ tuika_owaza_count1e = 0
    $ tuika_owaza_count1f = 0
    $ tuika_owaza_count1g = 0
    $ tuika_owaza_count1h = 0
    $ tuika_owaza_count2a = 0
    $ tuika_owaza_count2b = 0
    $ tuika_owaza_count2c = 0
    $ tuika_owaza_count2d = 0
    $ tuika_owaza_count3a = 0
    $ tuika_owaza_count3b = 0
    $ tuika_owaza_count3c = 0
    $ tuika_owaza_count3d = 0
    call face(param=1)
    jump common_attack_end


# Вычисляет вероятность активировать защиту ветром на основании wind_kakuritu
label wind_guard_kakuritu:
    if wind == 0:
        $ wind_guard_on = 0
        return

    if status in (1, 2, 4, 5, 7):
        $ wind_guard_on = 0
        return

    $ ransu = rnd2(1,100)

    $ wind_kakuritu = wind_kakuritu * (100+(sylph_rep*2/5))/100

    if blackwind_turn > 0:
        $ wind_kakuritu += 30

    if zylphe > 0:
        $ wind_kakuritu += 50

    if party_member[1] == 4:
        $ wind_kakuritu += alma_rep

    if ransu < wind_kakuritu+1:
        $ wind_guard_on = 1
        return

    $ ransu = rnd2(1,100)

    if party_member[1] == 4 and alma_rep > 9 and ransu < alma_rep(100-(alma_rep*2/3))/100:
        $ wind_guard = 1
        return

    $ wind_guard_on = 0
    return

# Уклонение разными типами ветра
label wind_guard():
    if wind == 1:
        call show_skillname("Ветрянная Защита")
        play sound "audio/se/wind2.ogg"
    elif wind == 2:
        call show_skillname("Танец Ветра")
        play sound "audio/se/miss.ogg"
    elif wind == 3:
        call show_skillname("Ураган")
        play sound "audio/se/miss_wind.ogg"
        if ng < 2:
            $ sel1 = "Но Лука движется словно торнадо и уклоняется!"
    elif wind == 4 and skill02 == 0:
        call show_skillname("Танец Падшего Ангела")
        play sound "audio/se/bun.ogg"
        $ renpy.show(haikei, at_list=[lightwave])
        $ renpy.show(tatie1, at_list=[lightwave, monster_pos])
        $ sel1 = "Но Лука, подхваченный священной волной, уходит от атаки!"
    elif wind == 4 and skill02 > 0:
        call show_skillname("Танец Падшего Ангела: Буря")
        play sound "audio/se/bun.ogg"
        $ renpy.show(haikei, at_list=[lightwave])
        $ renpy.show(tatie1, at_list=[lightwave, monster_pos])
        $ sel1 = "Но Лука, подхваченный святой волной и ветром, уходит от атаки!"

    hide ct
    if wind > 3:
        $ renpy.show(haikei) #,99,700 # with wave

    if sel1[:1] == "\n":
        extend "[sel1!t]"
    else:
        "[sel1!t]"

    call hide_skillname
    return

# Уклонение безмятежным разумом
label aqua_guard:
    $ print ransu
    hide ct with dissolve
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    if aqua != 2:
        $ renpy.show(haikei, at_list=[miss2])

    "Но [ori_name!t] уклоняется, словно поток воды, обегающий препятствие!"

    call hide_skillname
    return

label enemy_windguard:
    call hide_skillname
    play sound "audio/se/wind2.ogg"
    $ renpy.hide(img_tag)
    pause 0.01
    call face(param=2)
    "Но [monster_name] двигается подобно ветру и уклоняется от атаки!"
    call face(param=1)

    return

label enemy_aquaguard:
    pause .5
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.hide(img_tag)
    pause 0.01
    call face(param=2)
    "Но [monster_name!t] уклоняется, словно поток воды, обегающий препятствие!"
    call face(param=1)

    return

# Фразы при достижении 1/2 HP
label common_s1:
    if max_mylife > mylife*5:
        $ sel_hphalf = 1
        jump common_s2

    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Гх..."
    elif ransu == 2:
        ori "Ох..."
    elif ransu == 3:
        ori "Угх..."

    call face(param=2)

    $ sel1 = half_s1
    $ sel2 = half_s2

    if sel1:
        enemy "[sel1!t]"

    if sel2:
        enemy "[sel2!t]"

    call face(param=1)
    $ sel_hphalf = 1
    return

# Фразы при достижении 1/5 HP
label common_s2:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Ахх..."
    elif ransu == 2:
        ori "Аххх..."
    elif ransu == 3:
        ori "Ууу..."

    call face(param=2)
    $ sel1 = kiki_s1
    $ sel2 = kiki_s2

    if sel1:
        enemy "[sel1!t]"

    if sel2:
        enemy "[sel2!t]"

    $ sel_kiki = 1
    return


label selprint(c_name="   "):
    if sel3:
        if rnd_ev == 1:
            $ rnd_ev = 0
        else:
            python:
                while True:
                    ransu = rnd2(1,3)

                    if ransu != ren:
                        ren = ransu
                        break

        if ransu == 1:
            name "[sel1!t]"
        elif ransu == 2:
            name "[sel2!t]"
        elif ransu == 3:
            name "[sel3!t]"

    elif sel2:
        name "[sel1!t]"
        name "[sel2!t]"

    elif sel1:
        name "[sel1!t]"

    return


label selprint1:
    enemy "[sel1!t]"

    return


label selprint2:
    enemy "[sel1!t]"
    "[sel2!t]"

    return


label selprintn(name="   "):
    if sel3:
        if rnd_ev == 1:
            $ rnd_ev = 0
        else:
            python:
                while True:
                    ransu = rnd2(1,3)

                    if ransu != ren:
                        ren = ransu
                        break

        if ransu == 1:
            name "[sel1!t]"
        elif ransu == 2:
            name "[sel2!t]"
        elif ransu == 3:
            name "[sel3!t]"

    elif sel2:
        name "[sel1!t]"
        name "[sel2!t]"

    elif sel1:
        name "[sel1!t]"

    return


label selprint1n:
    "[sel1!t]"

    return


label selprint2n:
    "[sel1!t]"
    "[sel2!t]"

    return

# Регенерация SP
label mp_kaihuku:
    if skill01 == 0:
        return

    $ mp += 1

    if mp > max_mp:
        $ mp = max_mp

    return

# Подсчёт типов навыков в дневник
label skillcount1(skill_num):
    if persistent.skills[skill_num][0] == 0:
        $ persistent.count_oskill1 += 1

    $ persistent.skills[skill_num][0] += 1
    $ persistent.skill_num = skill_num
    return


label skillcount2(skill_num):
    if persistent.skills[skill_num][0] == 0:
        $ persistent.count_oskill2 += 1

    $ persistent.skills[skill_num][0] += 1
    $ persistent.skill_num = skill_num
    return


label skillcount3(skill_num):
    if persistent.skills[skill_num][0] == 0:
        $ persistent.count_oskill3 += 1

    $ persistent.skills[skill_num][0] += 1
    $ persistent.skill_num = skill_num
    return


label show_skillname(skn=""):
    show expression Text(skn, style="skillname") at sk zorder 50 as skillname
    pause .5
    return


label hide_skillname:
    hide skillname at sk
    pause .5
    return

# Присоединение персонажей
label join(chr_name=0, sw=0):
    if chr_name == 1:
        $ party_member[0] = chr_name
        $ chr_name = "Алиса"

    if chr_name in xrange(1, 6):
        if party_member[1] != 0:
            $ knight_previous = party_member[1]
        $ party_member[1] = chr_name
        if chr_name == 2:
            $ knight_name = "Гранберия"
            $ knight_name1 = "Гранберией"
            $ knight_name2 = "Гранберию"
        elif chr_name == 3:
            $ knight_name = "Тамамо"
            $ knight_name1 = knight_name
            $ knight_name2 = knight_name
        elif chr_name == 4:
            $ knight_name = "Альма Эльма"
            $ knight_name1 = "Альмой Эльмой"
            $ knight_name2 = "Альму Эльму"
        elif chr_name == 5:
            $ knight_name = "Эрубети"
            $ knight_name1 = knight_name
            $ knight_name2 = knight_name

        $ chr_name = knight_name

    if chr_name in xrange(6, 8) and party_member[2] == 0:
        $ party_member[2] = chr_name
        $ pm = party_member[2:4]
        if chr_name == 6:
            $ chr_name = "Кицунэ"
        elif chr_name == 7:
            $ chr_name = "Гарпия"
        elif chr_name == 8:
            $ chr_name = "Куруму"
        elif chr_name == 9:
            $ chr_name = "Сара"
    elif chr_name in xrange(6, 8) and party_member[3] == 0:
        $ party_member[3] = chr_name
        $ pm = party_member[2:4]
        if chr_name == 6:
            $ chr_name = "Кицунэ"
        elif chr_name == 7:
            $ chr_name = "Гарпия"
        elif chr_name == 8:
            $ chr_name = "Куруму"
        elif chr_name == 9:
            $ chr_name = "Сара"

    if sw == 1:
        play sound "audio/se/lvup.ogg"
        "[chr_name] получена!"
    else:
        play sound "audio/se/fanfale2.ogg"
        "[chr_name] присоединилась к группе!"

    return

# Обнуление состояний
label status_reset:
    $ poison = -1
    $ burn_turn = -1
    $ burn_turn1 = -1
    $ burn_turn2 = -1
    $ burn_turn3 = -1
    $ burn_turn4 = -1
    $ burn_turn5 = -1
    $ burn_turn6 = -1
    $ burn_turn7 = -1
    $ burn_turn8 = -1
    $ burn_turn9 = -1
    $ freeze_turn = -1
    $ freeze_turn1 = -1
    $ freeze_turn2 = -1
    $ freeze_turn3 = -1
    $ freeze_turn4 = -1
    $ freeze_turn5 = -1
    $ freeze_turn6 = -1
    $ freeze_turn7 = -1
    $ freeze_turn8 = -1
    $ freeze_turn9 = -1
    $ zylphe = -1
    $ zylphe_turn = -1
    $ gnomaren = -1
    $ gnomaren_turn = -1
    $ whitewind_turn = -1
    $ brace_turn = -1
    $ strength_turn = -1
    $ unpentagram_turn = -1
    $ erpentagram_turn = -1
    $ potion_turn = -1
    $ potion = 0
    $ immune = 0
    $ mylife_boost = 0
    $ luka_maxturn = 1
    $ lift_turn = -1
    $ harpy_windbuff = 0
    $ windbuff_turn = -1
    $ enrage_turn = -1
    $ burn_turn = -1
    $ blackwind_turn = -1
    $ waterwall_turn = -1
    $ titanguard_turn = -1
    $ seduce_turn = -1
    $ seduce_turn1 = -1
    $ seduce_turn2 = -1
    $ seduce_turn3 = -1
    $ seduce_turn4 = -1
    $ seduce_turn5 = -1
    $ seduce_turn6 = -1
    $ seduce_turn7 = -1
    $ seduce_turn8 = -1
    $ seduce_turn9 = -1
    $ erbind_turn = -1
    $ erbind_turn1 = -1
    $ erbind_turn2 = -1
    $ erbind_turn3 = -1
    $ erbind_turn4 = -1
    $ erbind_turn5 = -1
    $ erbind_turn6 = -1
    $ erbind_turn7 = -1
    $ erbind_turn8 = -1
    $ erbind_turn9 = -1
    $ lubind_turn = -1
    $ lubind_turn1 = -1
    $ lubind_turn2 = -1
    $ lubind_turn3 = -1
    $ tabind_turn = -1
    $ tabind_turn1 = -1
    $ tabind_turn2 = -1
    $ tabind_turn3 = -1
    $ enemy_barrier = 0
    $ enemy1_barrier = 0
    $ enemy2_barrier = 0
    $ enemy3_barrier = 0
    $ enemy4_barrier = 0
    $ enemy5_barrier = 0
    $ enemy6_barrier = 0
    $ enemy7_barrier = 0
    $ enemy8_barrier = 0
    $ enemy9_barrier = 0
    $ paralyze_turn = -1
    $ paralyze_turn1 = -1
    $ paralyze_turn2 = -1
    $ paralyze_turn3 = -1
    $ paralyze_turn4 = -1
    $ paralyze_turn5 = -1
    $ paralyze_turn6 = -1
    $ paralyze_turn7 = -1
    $ paralyze_turn8 = -1
    $ paralyze_turn9 = -1
    $ enemydisable = 0
    $ enemy1disable = 0
    $ enemy2disable = 0
    $ enemy3disable = 0
    return

label count_alive:
    $ alive_num = 0
    if enemy_num == 1 and enemylife > 0:
        $ alive_num = alive_num+1
    if enemy_num > 1 and enemy1life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 1 and enemy2life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 2 and enemy3life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 3 and enemy4life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 4 and enemy5life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 5 and enemy6life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 6 and enemy7life > 0:
        $ alive_num = alive_num+1
    if enemy_num > 7 and enemy8life > 0:
        $ alive_num = alive_num+1
    return

label zylphe_end:
    $ zylphe_turn -= 1
    "Сила Зильфи прекращает влиять на тело Луки!"
    $ zylphe = 0
    return

label gnomaren_end:
    $ gnomaren_turn -= 1
    "Сила Гномарен прекращает влиять на тело Луки!"
    $ gnomaren = 0
    return

label grandine_end:
    $ grandine_turn -= 1
    "Сила Грандины прекращает влиять на тело Луки!"
    $ grandine = 0
    return

label gigamander_end:
    $ gigamander_turn -= 1
    "Сила Гигамандер прекращает влиять на тело Луки!"
    $ gigamander = 0
    return

label skillset(s):
    $ skillset = s
    if s == 2:
        play sound2 "audio/se/fire4.ogg"
        pause 0.6
        play sound2 "audio/se/laser2.ogg"
        pause 0.6
        play sound2 "audio/se/bom3.ogg"
    return

label badend_h:
    play sound "audio/se/down.ogg"

    translator "А ты ожидал здесь увидеть хентай, да?{w}\nХентайные сцены я буду переводить в последнюю очередь!"

    $ monster_x = -200
    $ monster_y = 0
    $ end_n = 2
    $ bad1 = "sample text"
    $ bad2 = "sample text"
    jump badend

label sex_h:
    play sound "audio/se/down.ogg"

    translator "А ты ожидал здесь увидеть хентай, да?{w}\nХентайные сцены я буду переводить в последнюю очередь!"
    return

label part_achi:
    if persistent.granberia_hell > 0 and persistent.queenhapy_hell > 0 and persistent.tamamo_hell > 0 and persistent.granberia_maxrep > 0 and persistent.granberia_minrep > 0 and persistent.tamamo_maxrep > 0 and persistent.starblade and persistent.nine_moons_won and persistent.tamamo_knows:
        $ persistent.ng1_achi

    return

# Счётчик вайфу
label waifu_count:
    $ waifu_count = 0
    if salamander_rep > 20:
        $ waifu_count += 1
    if sylph_rep > 20:
        $ waifu_count += 1
    if undine_rep > 20:
        $ waifu_count += 1
    if gnome_rep > 20:
        $ waifu_count += 1
    if granberia_rep > 20:
        $ waifu_count += 1
    if alma_rep > 20:
        $ waifu_count += 1
    if erubetie_rep > 20:
        $ waifu_count += 1
    if tamamo_rep > 20:
        $ waifu_count += 1
    if alice_rep > 20:
        $ waifu_count += 1
    if harpy_rep > 20:
        $ waifu_count += 1
    if kitsune_rep > 20:
        $ waifu_count += 1
    if chrome_rep > 20:
        $ waifu_count += 1
    if kuria_rep > 20:
        $ waifu_count += 1

    return

# Подставляет нужные переменные в имена переменных
label spirit_order(name, order):
    python:
        exec('%s_order = %i' % (name, order))
        if order == 1:
            exec('%s_rep += 2' % name)
        elif order == 2:
            exec('%s_rep += 1' % name)

        exec('spirit%i = %s' % (order, name))
        
    return

label campspecial:
    if tamamo_rep > 2 and party_member[1] == 3 and tamamo_scene < 1:
        $ city_button02 = "{color=#FF8000}[knight_name]{/color}" #602
        $ city_act02 = 1
    elif tamamo_rep > 2 and party_member[1] == 3 and tamamo_scene < 2:
        $ city_button02 = "[knight_name]" #602
        $ city_act02 = 0 

    call cmd_city

    if result == 2:
        call tamamo_scene0
    elif result == 19:
        $ renpy.jump(camp_name + "_sleep")
