init -10 python:
    def slime_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slime st01", (x, 80)])
        elif persistent.face == 2:
            imgs.append(["slime st02", (x, 80)])
        elif persistent.face == 3:
            imgs.append(["slime st03", (x, 80)])

        if persistent.bk1:
            imgs.append(["slime bk01", (x, 80)])
        if persistent.bk2:
            imgs.append(["slime bk02", (x, 80)])
        if persistent.bk3:
            imgs.append(["slime bk03", (x, 80)])
        if persistent.bk4:
            imgs.append(["slime bk04", (x, 80)])
        if persistent.bk5:
            imgs.append(["slime bk05", (x, 80)])

        return imgs


    def alice1_pose(x):
        imgs = []

        if persistent.game_clear == 0 and persistent.pose > 2:
            persistent.pose = 1
        elif persistent.game_clear > 0 and persistent.pose > 8:
            persistent.pose = 1

        if persistent.game_clear == 0 and persistent.pose < 3 and persistent.face > 4:
            persistent.face = 1
        elif persistent.game_clear > 1 and persistent.pose < 3 and persistent.face > 9:
            persistent.face = 1

        if persistent.pose == 3 and persistent.face > 5:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 5:
            persistent.face = 1
        elif persistent.pose == 5 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 6 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 7 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 8:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice st01b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice st02b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice st03b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["alice st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["alice st04b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["alice st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["alice st05b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["alice st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["alice st06b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["alice st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["alice st07b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["alice st08", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["alice st08b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["alice st09", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 9:
            imgs.append(["alice st09b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alice st11", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alice st11b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alice st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["alice st12b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alice st13", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["alice st13b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["alice st14", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["alice st14b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["alice st15", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["alice st15b", (x-80, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["alice st21", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 1:
            imgs.append(["alice st21b", (x-80, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["alice st22", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 2:
            imgs.append(["alice st22b", (x-80, 0)])
        elif persistent.pose == 7 and persistent.face == 1:
            imgs.append(["alice st23", (x, 0)])
        elif persistent.pose == 7 and persistent.face == 2:
            imgs.append(["alice st24", (x, 0)])
        elif persistent.pose == 8 and persistent.face == 1:
            imgs.append(["alice st31", (x+80, 125)])

        return imgs


    def slug_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face == 1 and persistent.pose == 1:
            imgs.append(["slug st01", (x, 0)])
        elif persistent.face == 2 and persistent.pose == 1:
            imgs.append(["slug st02", (x, 0)])
        elif persistent.face == 1 and persistent.pose == 2:
            imgs.append(["slug st11", (x, 0)])
        elif persistent.face == 2 and persistent.pose == 2:
            imgs.append(["slug st12", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["slug bk01", (x, 0)])

            if persistent.bk2:
                imgs.append(["slug bk02", (x, 0)])

            if persistent.bk3:
                imgs.append(["slug bk03", (x, 0)])


        return imgs


    def mdg_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mdg st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mdg st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mdg st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["mdg st04", (x, 0)])

        if persistent.bk1:
            imgs.append(["mdg bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mdg bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mdg bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["mdg bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["mdg bk05", (x, 0)])

        return imgs


    def granberia1_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs


    def mimizu_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["mimizu st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["mimizu st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["mimizu st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["mimizu st11", (x, 0)])

        if persistent.bk1:
            imgs.append(["mimizu bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mimizu bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mimizu bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["mimizu bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["mimizu bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["mimizu bk06", (x, 0)])

        return imgs


    def gob_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 5:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["gob st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["gob st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["gob st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["gob st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["gob st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["gob st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["gob st22", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["gob st23", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["gob bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["gob bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["gob bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["gob bk04", (x, 0)])

        return imgs


    def pramia_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["pramia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["pramia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["pramia st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["pramia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["pramia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["pramia st13", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["pramia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["pramia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["pramia bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["pramia bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["pramia bk05", (x, 0)])

        return imgs


    def vgirl_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["vgirl st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["vgirl st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["vgirl st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["vgirl st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["vgirl st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["vgirl st13", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["vgirl bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["vgirl bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["vgirl bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["vgirl bk04", (x, 0)])

        return imgs


    def dragonp_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["dragonp st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["dragonp st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["dragonp st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["dragonp st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["dragonp st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["dragonp st13", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["dragonp bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["dragonp bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["dragonp bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["dragonp bk04", (x, 0)])

        return imgs


    def mitubati_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mitubati st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mitubati st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mitubati st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["mitubati bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mitubati bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mitubati bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["mitubati bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["mitubati bk05", (x, 0)])

        return imgs


    def hapy_a_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["hapy_a st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["hapy_a st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["hapy_a st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["hapy_a bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["hapy_a bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["hapy_a bk03", (x, 0)])

        return imgs


    def hapy_bc_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 3:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose > 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["hapy_bc st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["hapy_bc st01a", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["hapy_bc st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["hapy_bc st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["hapy_bc st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["hapy_bc st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["hapy_bc st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["hapy_bc st31", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["hapy_bc st32", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["hapy_bc bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["hapy_bc bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["hapy_bc bk03", (x, 0)])

        return imgs


    def queenharpy_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["queenharpy st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["queenharpy st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["queenharpy st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["queenharpy st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["queenharpy st01_2", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["queenharpy st02_2", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["queenharpy st03_2", (x, 0)])

        if persistent.bk1:
            imgs.append(["queenharpy bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["queenharpy bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["queenharpy bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["queenharpy bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["queenharpy bk05", (x, 0)])

        return imgs


    def delf_a_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["delf_a st01", (x, -100)])
        elif persistent.face == 2:
            imgs.append(["delf_a st02", (x, -100)])
        elif persistent.face == 3:
            imgs.append(["delf_a st03", (x, -100)])

        if persistent.bk1:
            imgs.append(["elf bk01", (x, -100)])
        if persistent.bk2:
            imgs.append(["elf bk02", (x, -100)])
        if persistent.bk3:
            imgs.append(["elf bk03", (x, -100)])
        if persistent.bk4:
            imgs.append(["delf_a bk01", (x, -100)])

        return imgs


    def delf_b_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["delf_b st01", (x, 30)])
        elif persistent.face == 2:
            imgs.append(["delf_b st02", (x, 30)])
        elif persistent.face == 3:
            imgs.append(["delf_b st03", (x, 30)])

        if persistent.bk1:
            imgs.append(["delf_b bk01", (x, 30)])
        if persistent.bk2:
            imgs.append(["delf_b bk02", (x, 30)])
        if persistent.bk3:
            imgs.append(["delf_b bk03", (x, 30)])
        if persistent.bk4:
            imgs.append(["delf_b bk04", (x, 30)])

        return imgs


    def hiru_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["hiru st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["hiru st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["hiru st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["hiru bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["hiru bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["hiru bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["hiru bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["hiru bk05", (x, 0)])

        return imgs


    def rahure_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["rahure st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["rahure st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["rahure st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["rahure bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["rahure bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["rahure bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["rahure bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["rahure bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["rahure bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["rahure bk07", (x, 0)])

        return imgs


    def ropa_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["ropa st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["ropa st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["ropa st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["ropa bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["ropa bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["ropa bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["ropa bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["ropa bk05", (x, 0)])

        return imgs


    def youko_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["youko st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["youko st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["youko st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["youko bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["youko bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["youko bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["youko bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["youko bk05", (x, 0)])

        return imgs


    def meda_pose(x):
        imgs = []

        imgs.append(["meda st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["meda bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["meda bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["meda bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["meda bk04", (x, 0)])

        return imgs


    def kumo_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kumo st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kumo st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kumo st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kumo bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kumo bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kumo bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["kumo bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["kumo bk05", (x, 0)])

        return imgs


    def mimic_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mimic st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mimic st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["mimic bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mimic bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mimic bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["mimic bk04", (x, 0)])

        return imgs


    def nanabi_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["nanabi st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["nanabi st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["nanabi st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["nanabi st04", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["nanabi st11", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["nanabi st21", (x, 0)])

        if persistent.pose != 3:
            if persistent.bk1:
                imgs.append(["nanabi bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["nanabi bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["nanabi bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["nanabi bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["nanabi bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["nanabi bk06", (x, 0)])
            if persistent.bk7:
                imgs.append(["nanabi bk07", (x, 0)])

        return imgs


    def tamamo1_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["tamamo st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["tamamo st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["tamamo st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["tamamo st04", (x, 0)])

        if persistent.bk1:
            imgs.append(["tamamo bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["tamamo bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["tamamo bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["tamamo bk04", (x, 0)])

        return imgs


    def alma_elma1_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1

        if persistent.pose < 4 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 4 and persistent.face == 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alma_elma st41", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alma_elma st42", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alma_elma st43", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alma_elma st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alma_elma st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alma_elma st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alma_elma st31", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alma_elma st32", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alma_elma st33", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alma_elma st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["alma_elma st22", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["alma_elma bk01", (x, 0)])
        if persistent.pose == 3 and persistent.bk1 == 1 and persistent.face == 1:
            imgs.append(["alma_elma st31b", (x, 0)])
        if persistent.pose == 3 and persistent.bk1 == 1 and persistent.face == 2:
            imgs.append(["alma_elma st32b", (x, 0)])
        if persistent.pose == 3 and persistent.bk1 == 1 and persistent.face == 3:
            imgs.append(["alma_elma st33b", (x, 0)])

        return imgs

    def namako_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["namako st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["namako st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["namako st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["namako bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["namako bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["namako bk03", (x, 0)])

        return imgs


    def kai_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["kai st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["kai st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["kai st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["kai st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["kai st12", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["kai bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["kai bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["kai bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["kai bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["kai bk05", (x, 0)])

        return imgs


    def lamia_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["lamia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["lamia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["lamia st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["lamia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["lamia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["lamia st13", (x, 0)])

        if persistent.bk1:
            imgs.append(["lamia bk01", (x, 0)])
        
        if persistent.bk2 and persistent.pose == 1:
            imgs.append(["lamia bk02a", (x, 0)])
        elif persistent.bk2 and persistent.pose == 2:
            imgs.append(["lamia bk02b", (x, 0)])
        
        if persistent.bk3 and persistent.pose == 1:
            imgs.append(["lamia bk03a", (x, 0)])
        elif persistent.bk3 and persistent.pose == 2:
            imgs.append(["lamia bk03b", (x, 0)])
        
        if persistent.bk4 and persistent.pose == 2:
            imgs.append(["lamia bk04", (x, 0)])
        
        if persistent.bk5:
            imgs.append(["lamia bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["lamia bk06", (x, 0)])

        return imgs


    def granberia2_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        if persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        if persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.bk1:
            imgs.append(["granberia bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["granberia bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["granberia bk03", (x, 0)])

        return imgs


    def page17_pose(x):
        imgs = []

        imgs.append(["page17 st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["page17 bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["page17 bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["page17 bk03", (x, 0)])

        return imgs


    def page257_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["page257 st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["page257 st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["page257 st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["page257 bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["page257 bk02", (x, 0)])

        return imgs


    def page65537_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        if persistent.pose < 3 and persistent.face > 2:
            persistent.face = 1
        if persistent.pose == 3 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["page65537 st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["page65537 st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["page65537 st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["page65537 st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["page65537 st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["page65537 st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["page65537 st23", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["page65537 bk01a", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["page65537 bk01b", (x, 0)])
        elif persistent.pose == 3 and persistent.bk1 == 1:
            imgs.append(["page65537 bk01c", (x, 0)])
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["page65537 bk02a", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["page65537 bk02b", (x, 0)])
        elif persistent.pose == 3 and persistent.bk2 == 1:
            imgs.append(["page65537 bk02c", (x, 0)])
        if persistent.pose == 1 and persistent.bk3:
            imgs.append(["page65537 bk03a", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3:
            imgs.append(["page65537 bk03b", (x, 0)])
        elif persistent.pose == 3 and persistent.bk3:
            imgs.append(["page65537 bk03c", (x, 0)])
        if persistent.bk4:
            imgs.append(["page65537 bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["page65537 bk05", (x, 0)])

        return imgs


    def kani_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        # if persistent.pose == 2:
        #     persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["kani st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["kani st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["kani st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["kani st00", (x, 0)])

        if persistent.bk1:
            imgs.append(["kani bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kani bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kani bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["kani bk04", (x, 0)])

        return imgs


    def kurage_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kurage st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kurage st02", (x, 0)])

        if persistent.bk1:
            imgs.append(["kurage bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kurage bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kurage bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["kurage bk04", (x, 0)])

        return imgs


    def iso_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["iso st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["iso st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["iso st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["iso bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["iso bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["iso bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["iso bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["iso bk05", (x, 0)])

        return imgs


    def ankou_pose(x):
        imgs = []

        imgs.append(["ankou st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["ankou bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["ankou bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["ankou bk03", (x, 0)])

        return imgs


    def kraken_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kraken st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kraken st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kraken st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kraken bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kraken bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kraken bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["kraken bk04", (x, 0)])

        return imgs


    def meia_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["meia st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["meia st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["meia st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["meia st04", (x, 0)])

        return imgs


    def ghost_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["ghost st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["ghost st02", (x, 0)])

        if persistent.bk1:
            imgs.append(["ghost bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["ghost bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["ghost bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["ghost bk04", (x, 0)])

        return imgs


    def doll_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["doll st01", (x, 50)])
        elif persistent.face == 2:
            imgs.append(["doll st02", (x, 50)])
        elif persistent.face == 3:
            imgs.append(["doll st03", (x, 50)])

        if persistent.bk1:
            imgs.append(["doll bk01", (x, 50)])
        if persistent.bk2:
            imgs.append(["doll bk02", (x, 50)])
        if persistent.bk3:
            imgs.append(["doll bk03", (x, 50)])

        return imgs


    def zonbe_pose(x):
        imgs = []

        imgs.append(["zonbe st11", (x, 82)])

        if persistent.bk1:
            imgs.append(["zonbe bk11", (x, 82)])
        if persistent.bk2:
            imgs.append(["zonbe bk12", (x, 82)])
        if persistent.bk3:
            imgs.append(["zonbe bk13", (x, 82)])

        return imgs


    def zonbes_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["zonbe st21", (x, 96)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["zonbe st22", (x, 96)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["zonbe st31", (x-35, 31)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["zonbe st32", (x-35, 31)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["zonbe st41", (x-61, 12)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["zonbe st42", (x-61, 12)])

        if persistent.bk1 and persistent.pose == 1:
            imgs.append(["zonbe bk21", (x, 96)])
        elif persistent.bk1 and persistent.pose == 2:
            imgs.append(["zonbe bk31", (x-35, 31)])
        elif persistent.bk1 and persistent.pose == 3:
            imgs.append(["zonbe bk41", (x-61, 12)])
        if persistent.bk2 and persistent.pose == 1:
            imgs.append(["zonbe bk22", (x, 96)])
        elif persistent.bk2 and persistent.pose == 2:
            imgs.append(["zonbe bk32", (x-35, 31)])
        elif persistent.bk2 and persistent.pose == 3:
            imgs.append(["zonbe bk42", (x-61, 12)])
        if persistent.bk3 and persistent.pose == 1:
            imgs.append(["zonbe bk23", (x, 96)])
        elif persistent.bk3 and persistent.pose == 2:
            imgs.append(["zonbe bk33", (x-35, 31)])
        elif persistent.bk3 and persistent.pose == 3:
            imgs.append(["zonbe bk43", (x-61, 12)])

        return imgs


    def frederika_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["frederika st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["frederika st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["frederika st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["frederika st11", (x+150, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["frederika st12", (x+150, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["frederika st13", (x+150, 0)])
        elif persistent.pose == 3:
            imgs.append(["frederika st21", (x+102, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["frederika bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["frederika bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["frederika bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["frederika bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["frederika bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["frederika bk06", (x, 0)])
            if persistent.bk7:
                imgs.append(["frederika bk07", (x, 0)])

        return imgs


    def chrom_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["chrom st11", (x, 60)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["chrom st12", (x, 60)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["chrom st13", (x, 60)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["chrom st01", (x+77, 150)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["chrom st02", (x+77, 150)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["chrom st03", (x+77, 150)])

        if persistent.bk1:
            imgs.append(["chrom bk01", (x, 60)])
        if persistent.bk2:
            imgs.append(["chrom bk02", (x, 60)])
        if persistent.bk3:
            imgs.append(["chrom bk03", (x, 60)])
        if persistent.bk4:
            imgs.append(["chrom bk04", (x, 60)])
        if persistent.bk5:
            imgs.append(["chrom bk05", (x, 60)])

        return imgs


    def fairy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["fairy st01", (x, 130)])
        elif persistent.face == 2:
            imgs.append(["fairy st02", (x, 130)])
        elif persistent.face == 3:
            imgs.append(["fairy st03", (x, 130)])

        if persistent.bk1:
            imgs.append(["fairy bk01", (x, 130)])
        if persistent.bk2:
            imgs.append(["fairy bk02", (x, 130)])
        if persistent.bk3:
            imgs.append(["fairy bk03", (x, 130)])

        return imgs


    def elf_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        imgs.append(["elf st01", (x, -100)])

        if persistent.bk1:
            imgs.append(["elf bk01", (x, -100)])
        if persistent.bk2:
            imgs.append(["elf bk02", (x, -100)])
        if persistent.bk3:
            imgs.append(["elf bk03", (x, -100)])
        if persistent.bk4:
            imgs.append(["elf bk04", (x, -100)])

        return imgs


    def tfairy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["tfairy st01", (x, 130)])
        elif persistent.face == 2:
            imgs.append(["tfairy st02", (x, 130)])
        elif persistent.face == 3:
            imgs.append(["tfairy st03", (x, 130)])

        if persistent.bk1:
            imgs.append(["tfairy bk01", (x, 130)])
        
        if persistent.bk2:
            imgs.append(["tfairy bk02", (x, 130)])
        
        if persistent.bk3:
            imgs.append(["tfairy bk03", (x, 130)])
        
        if persistent.bk4:
            imgs.append(["tfairy bk04", (x, 130)])
        
        if persistent.bk5:
            imgs.append(["tfairy bk05", (x, 130)])

        return imgs


    def fairys_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face == 1 and persistent.pose == 1:
            imgs.append(["fairys_a st01", (x, 100)])
        elif persistent.face == 2 and persistent.pose == 1:
            imgs.append(["fairys_a st02", (x, 100)])
        elif persistent.face == 3 and persistent.pose == 1:
            imgs.append(["fairys_a st03", (x, 100)])
        elif persistent.face == 1 and persistent.pose == 2:
            imgs.append(["fairys_b st11", (x+10, 100)])
        elif persistent.face == 2 and persistent.pose == 2:
            imgs.append(["fairys_b st12", (x+10, 100)])
        elif persistent.face == 3 and persistent.pose == 2:
            imgs.append(["fairys_b st13", (x+10, 100)])
        elif persistent.face == 1 and persistent.pose == 3:
            imgs.append(["fairys_c st21", (x-20, 100)])
        elif persistent.face == 2 and persistent.pose == 3:
            imgs.append(["fairys_c st22", (x-20, 100)])
        elif persistent.face == 3 and persistent.pose == 3:
            imgs.append(["fairys_c st23", (x-20, 100)])

        if persistent.bk1 and persistent.pose == 1:
            imgs.append(["fairys bk01", (x, 100)])
        elif persistent.bk1 and persistent.pose == 2:
            imgs.append(["fairys bk11", (x+10, 100)])
        elif persistent.bk1 and persistent.pose == 3:
            imgs.append(["fairys bk21", (x-20, 100)])
        
        if persistent.bk2 and persistent.pose == 1:
            imgs.append(["fairys bk02", (x, 100)])
        elif persistent.bk2 and persistent.pose == 2:
            imgs.append(["fairys bk12", (x+10, 100)])
        elif persistent.bk2 and persistent.pose == 3:
            imgs.append(["fairys bk22", (x-20, 100)])
        
        if persistent.bk3 and persistent.pose == 1:
            imgs.append(["fairys bk03", (x, 100)])
        elif persistent.bk3 and persistent.pose == 2:
            imgs.append(["fairys bk13", (x+10, 100)])
        elif persistent.bk3 and persistent.pose == 3:
            imgs.append(["fairys bk23", (x-20, 100)])

        return imgs


    def sylph_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 7:
            persistent.face = 1

        if persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1


        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["sylph st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["sylph st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["sylph st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["sylph st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["sylph st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["sylph st06", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["sylph st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["sylph st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["sylph st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["sylph st21", (x, 100)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["sylph st22", (x+155, 100)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["sylph bk01", (x, 0)])
            elif persistent.bk2:
                imgs.append(["sylph bk02", (x, 0)])
            elif persistent.bk3:
                imgs.append(["sylph bk03", (x, 0)])

        return imgs


    def c_dryad_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1:
            imgs.append(["c_dryad st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["c_dryad st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["c_dryad st22", (x, 0)])

        if persistent.bk1:
            imgs.append(["c_dryad bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["c_dryad bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["c_dryad bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["c_dryad bk04", (x, 0)])
        
        if persistent.bk5:
            imgs.append(["c_dryad bk05", (x, 0)])
        
        if persistent.bk6:
            imgs.append(["c_dryad bk06", (x, 0)])

        return imgs


    def taran_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["taran st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["taran st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["taran st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["taran bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["taran bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["taran bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["taran bk04", (x, 0)])
        
        if persistent.bk5:
            imgs.append(["taran bk05", (x, 0)])

        return imgs


    def mino_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face == 1 and persistent.pose == 1:
            imgs.append(["mino st01", (x, 0)])
        elif persistent.face == 2 and persistent.pose == 1:
            imgs.append(["mino st02", (x, 0)])
        elif persistent.face == 3 and persistent.pose == 1:
            imgs.append(["mino st03", (x, 0)])
        elif persistent.face == 4 and persistent.pose == 1:
            imgs.append(["mino st04", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["mino st11", (x, 0)])

        if persistent.bk1:
            imgs.append(["mino bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["mino bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["mino bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["mino bk04", (x, 0)])

        return imgs


    def sasori_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["sasori st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["sasori st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["sasori st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["sasori st11", (x, 0)])

        if persistent.bk1:
            imgs.append(["sasori bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["sasori bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["sasori bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["sasori bk04", (x, 0)])
        
        if persistent.pose == 2 and persistent.bk5 == 1:
            imgs.append(["sasori bk05", (x, 0)])

        return imgs


    def lamp_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose > 4:
            persistent.pose = 1

        if persistent.pose == 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["lamp st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["lamp st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["lamp st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["lamp st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["lamp st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["lamp st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["lamp st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["lamp st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["lamp st23", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["lamp st31", (x, 0)])

        if persistent.bk1:
            imgs.append(["lamp bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["lamp bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["lamp bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["lamp bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["lamp bk05", (x, 0)])

        return imgs


    def mummy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mummy st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mummy st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mummy st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["mummy bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mummy bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mummy bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["mummy bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["mummy bk05", (x, 0)])

        return imgs


    def kobura_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kobura st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kobura st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kobura st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kobura bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kobura bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kobura bk03", (x, 0)])

        return imgs


    def lamias_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["lamias st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["lamias st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["lamias st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["lamias bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["lamias bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["lamias bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["lamias bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["lamias bk05", (x, 0)])

        return imgs


    def sphinx_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["sphinx st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["sphinx st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["sphinx st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["sphinx bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["sphinx bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["sphinx bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["sphinx bk04", (x, 0)])

        return imgs


    def sara_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["sara st01r", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["sara st02r", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["sara st03r", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["sara st04r", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["sara st11r", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["sara st12r", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["sara st13r", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["sara st14r", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["sara st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["sara st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["sara st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["sara st24", (x, 0)])

        return imgs


    def suckvore_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if persistent.face > 2:
            persistent.face = 1
        if persistent.pose == 1:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["suckvore st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["suckvore st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["suckvore st12", (x, 0)])

        if persistent.bk1:
            imgs.append(["suckvore bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["suckvore bk02", (x, 0)])

        return imgs


    def wormv_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["wormv st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["wormv st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["wormv st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["wormv st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["wormv st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["wormv st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["wormv st14", (x, 0)])

        if persistent.bk1:
            imgs.append(["wormv bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["wormv bk02", (x, 0)])

        return imgs


    def ironmaiden_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1:
            imgs.append(["ironmaiden st01", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["ironmaiden st11", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["ironmaiden st21", (x, 0)])

        return imgs

    def lily_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["lily st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["lily st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["lily st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["lily st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["lily st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["lily st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["lily st13", (x, 0)])

        if persistent.bk1:
            imgs.append(["lily bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["lily bk02", (x, 0)])

        return imgs


    def arizigoku_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["arizigoku st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["arizigoku st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["arizigoku st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["arizigoku bk01", (x, 0)])

        return imgs


    def sandw_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["sandw st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["sandw st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["sandw st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["sandw bk01", (x, 0)])

        return imgs


    def gnome_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1
        if persistent.pose < 3 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 3:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["gnome st11", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["gnome st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["gnome st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["gnome st02", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["gnome st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["gnome st31", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["gnome st32", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["gnome st33", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["gnome st34", (x-40, 0)])

        if persistent.bk1:
            imgs.append(["gnome bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["gnome bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["gnome bk03", (x, 0)])

        return imgs


    def ilias1_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1
        if persistent.pose == 2:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["ilias st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["ilias st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["ilias st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["ilias st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["ilias st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["ilias st06", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["ilias st14", (x, 0)])

        if persistent.bk1:
            imgs.append(["ilias bk01", (x, 0)])

        return imgs

    def alice2_pose(x):
        imgs = []

        if persistent.pose > 5:
            persistent.pose = 1
        elif persistent.pose < 5 and persistent.face == 11:
            persistent.face = 1
        elif persistent.pose == 5:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice st01b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alice st11", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alice st11b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice st02b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alice st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["alice st12b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice st03b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alice st13", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["alice st13b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["alice st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["alice st04b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["alice st14", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["alice st14b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["alice st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["alice st05b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["alice st15", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["alice st15b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["alice st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["alice st06b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["alice st16", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["alice st16bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["alice st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["alice st07b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["alice st17", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["alice st17bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["alice st08", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["alice st08b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["alice st18", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 8:
            imgs.append(["alice st18bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["alice st09", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 9:
            imgs.append(["alice st09b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["alice st19", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 9:
            imgs.append(["alice st19bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 10:
            imgs.append(["alice st51", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 10:
            imgs.append(["alice st51b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 10:
            imgs.append(["alice st61", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 10:
            imgs.append(["alice st61b", (x-80, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["alice st31", (x+80, 125)])

        return imgs


    def centa_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["centa st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["centa st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["centa st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["centa bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["centa bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["centa bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["centa bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["centa bk05", (x, 0)])

        return imgs


    def kaeru_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1
        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["kaerua st01", (x, 50)])
            elif persistent.face == 2:
                imgs.append(["kaerua st02", (x, 50)])
            elif persistent.face == 3:
                imgs.append(["kaerua st03", (x, 50)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["kaerub st01", (x-4, 50)])
            if persistent.face == 2:
                imgs.append(["kaerub st02", (x-4, 50)])
            if persistent.face == 3:
                imgs.append(["kaerub st03", (x-4, 50)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["kaerua bk01", (x, 50)])
            if persistent.bk2:
                imgs.append(["kaerua bk02", (x, 50)])
            if persistent.bk3:
                imgs.append(["kaerua bk03", (x, 50)])
            if persistent.bk4:
                imgs.append(["kaerua bk04", (x, 50)])
        elif persistent.pose == 2:
            if persistent.bk1:
                imgs.append(["kaerub bk01", (x-4, 50)])
            if persistent.bk2:
                imgs.append(["kaerub bk02", (x-4, 50)])
            if persistent.bk3:
                imgs.append(["kaerub bk03", (x-4, 50)])
            if persistent.bk4:
                imgs.append(["kaerub bk04", (x-4, 50)])

        return imgs


    def alraune_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["alraune st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["alraune st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["alraune st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["alraune bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["alraune bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["alraune bk03", (x, 0)])

        return imgs


    def dullahan_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["dullahan st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["dullahan st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["dullahan st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["dullahan bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["dullahan bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["dullahan bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["dullahan bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["dullahan bk05", (x, 0)])

        return imgs


    def cerberus_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["cerberus st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["cerberus st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["cerberus st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["cerberus bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["cerberus bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["cerberus bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["cerberus bk04", (x, 0)])

        return imgs


    def alma_elma2_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alma_elma st21", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alma_elma st22", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alma_elma st23", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alma_elma st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alma_elma st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alma_elma st13", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1 == 1:
                imgs.append(["alma_elma bk01", (x, 0)])
            if persistent.bk2 == 1:
                imgs.append(["alma_elma bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["alma_elma bk03", (x, 0)])
            if persistent.bk4 == 1:
                imgs.append(["alma_elma bk04", (x, 0)])
            if persistent.bk5 == 1:
                imgs.append(["alma_elma bk13", (x, 0)])
            if persistent.bk6:
                imgs.append(["alma_elma bk16", (x, 0)])

        return imgs


    def yukionna_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["yukionna st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["yukionna st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["yukionna st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["yukionna bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["yukionna bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["yukionna bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["yukionna bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["yukionna bk05", (x, 0)])

        return imgs


    def nekomata_pose(x):
        imgs = []

        if persistent.face > 7:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["nekomata st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["nekomata st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["nekomata st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["nekomata st04", (x, 0)])
        elif persistent.face == 5:
            imgs.append(["nekomata st05", (x, 0)])
        elif persistent.face == 6:
            imgs.append(["nekomata st06", (x, 0)])
        elif persistent.face == 7:
            imgs.append(["nekomata st07", (x, 0)])

        if persistent.bk1:
            imgs.append(["nekomata bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["nekomata bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["nekomata bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["nekomata bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["nekomata bk05", (x, 0)])

        return imgs


    def samuraielf_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["samuraielf st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["samuraielf st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["samuraielf st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["samuraielf bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["samuraielf bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["samuraielf bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["samuraielf bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["samuraielf bk05", (x, 0)])

        return imgs


    def kunoitielf_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kunoitielf st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kunoitielf st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kunoitielf st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kunoitielf bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kunoitielf bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kunoitielf bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["kunoitielf bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["kunoitielf bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["kunoitielf bk06", (x, 0)])

        return imgs


    def yamatanooroti_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["yamatanooroti st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["yamatanooroti st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["yamatanooroti st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["yamatanooroti bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["yamatanooroti bk02", (x, 0)])

        return imgs


    def moss_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["moss st01", (x, 60)])
        elif persistent.face == 2:
            imgs.append(["moss st02", (x, 60)])
        elif persistent.face == 3:
            imgs.append(["moss st03", (x, 60)])

        if persistent.bk1:
            imgs.append(["moss bk01", (x, 60)])
        if persistent.bk2:
            imgs.append(["moss bk02", (x, 60)])
        if persistent.bk3:
            imgs.append(["moss bk03", (x, 60)])
        if persistent.bk4:
            imgs.append(["moss bk04", (x, 60)])
        if persistent.bk5:
            imgs.append(["moss bk05", (x, 60)])

        return imgs


    def mosquito_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mosquito st01", (x, 50)])
        elif persistent.face == 2:
            imgs.append(["mosquito st02", (x, 50)])
        elif persistent.face == 3:
            imgs.append(["mosquito st03", (x, 50)])

        if persistent.bk1:
            if persistent.face == 1:
                imgs.append(["mosquito bk01a", (x, 50)])
            elif persistent.face == 2:
                imgs.append(["mosquito bk01b", (x, 50)])
            elif persistent.face == 3:
                imgs.append(["mosquito bk01c", (x, 50)])

        if persistent.bk2:
            imgs.append(["mosquito bk02", (x, 50)])
        if persistent.bk3:
            imgs.append(["mosquito bk03", (x, 50)])
        if persistent.bk4:
            imgs.append(["mosquito bk04", (x, 50)])
        if persistent.bk5:
            imgs.append(["mosquito bk05", (x, 50)])

        return imgs


    def imomusi_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["imomusi st01", (x, 140)])
        elif persistent.face == 2:
            imgs.append(["imomusi st02", (x, 140)])
        elif persistent.face == 3:
            imgs.append(["imomusi st03", (x, 140)])

        if persistent.bk1:
            imgs.append(["imomusi bk01", (x, 140)])
        if persistent.bk2:
            imgs.append(["imomusi bk02", (x, 140)])

        return imgs


    def mukade_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mukade st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mukade st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mukade st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["mukade bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["mukade bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["mukade bk03", (x, 0)])

        return imgs


    def kaiko_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kaiko st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kaiko st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kaiko st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kaiko bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kaiko bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kaiko bk03", (x, 0)])

        return imgs


    def suzumebati_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["suzumebati st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["suzumebati st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["suzumebati bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["suzumebati bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["suzumebati bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["suzumebati bk04", (x, 0)])

        return imgs


    def queenbee_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["queenbee st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["queenbee st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["queenbee st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["queenbee bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["queenbee bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["queenbee bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["queenbee bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["queenbee bk05", (x, 0)])

        return imgs


    def a_alm_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_alm st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["a_alm st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["a_alm bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["a_alm bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["a_alm bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["a_alm bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["a_alm bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["a_alm bk06", (x, 0)])

        return imgs


    def a_looty_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_looty st01", (x, 130)])
        elif persistent.face == 2:
            imgs.append(["a_looty st02", (x, 130)])
        elif persistent.face == 3:
            imgs.append(["a_looty st03", (x, 130)])

        if persistent.bk1:
            imgs.append(["a_looty bk01", (x, 130)])
        if persistent.bk2:
            imgs.append(["a_looty bk02", (x, 130)])
        if persistent.bk3:
            imgs.append(["a_looty bk03", (x, 130)])
        if persistent.bk4:
            imgs.append(["a_looty bk04", (x, 130)])
        if persistent.bk5:
            imgs.append(["a_looty bk05", (x, 130)])

        return imgs


    def a_vore_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_vore st01", (x, 90)])
        elif persistent.face == 2:
            imgs.append(["a_vore st02", (x, 90)])
        elif persistent.face == 3:
            imgs.append(["a_vore st03", (x, 90)])

        if persistent.bk1:
            imgs.append(["a_vore bk01", (x, 90)])
        if persistent.bk2:
            imgs.append(["a_vore bk02", (x, 90)])
        if persistent.bk3:
            imgs.append(["a_vore bk03", (x, 90)])
        if persistent.bk4:
            imgs.append(["a_vore bk04", (x, 90)])
        if persistent.bk5:
            imgs.append(["a_vore bk05", (x, 90)])

        return imgs


    def a_parasol_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_parasol st01", (x, 40)])
        elif persistent.face == 2:
            imgs.append(["a_parasol st02", (x, 40)])
        elif persistent.face == 3:
            imgs.append(["a_parasol st03", (x, 40)])

        if persistent.bk1:
            imgs.append(["a_parasol bk01", (x, 40)])
        if persistent.bk2:
            imgs.append(["a_parasol bk02", (x, 40)])
        if persistent.bk3:
            imgs.append(["a_parasol bk03", (x, 40)])
        if persistent.bk4:
            imgs.append(["a_parasol bk04", (x, 40)])
        if persistent.bk5:
            imgs.append(["a_parasol bk05", (x, 40)])
        if persistent.bk6:
            imgs.append(["a_parasol bk06", (x, 40)])

        return imgs


    def a_prison_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_prison st01", (x, 70)])
        elif persistent.face == 2:
            imgs.append(["a_prison st02", (x, 70)])
        elif persistent.face == 3:
            imgs.append(["a_prison st03", (x, 70)])

        if persistent.bk1:
            imgs.append(["a_prison bk01", (x, 70)])
        if persistent.bk2:
            imgs.append(["a_prison bk02", (x, 70)])
        if persistent.bk3:
            imgs.append(["a_prison bk03", (x, 70)])
        if persistent.bk4:
            imgs.append(["a_prison bk04", (x, 70)])
        if persistent.bk5:
            imgs.append(["a_prison bk05", (x, 70)])
        if persistent.bk6:
            imgs.append(["a_prison bk06", (x, 70)])
        if persistent.bk7:
            imgs.append(["a_prison bk07", (x, 70)])

        return imgs


    def a_emp_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["a_emp st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["a_emp st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["a_emp st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["a_emp bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["a_emp bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["a_emp bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["a_emp bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["a_emp bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["a_emp bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["a_emp bk07", (x, 0)])
        if persistent.bk8:
            imgs.append(["a_emp bk08", (x, 0)])

        return imgs


    def dorothy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["dorothy st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["dorothy st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["dorothy st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["dorothy bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["dorothy bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["dorothy bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["dorothy bk04", (x, 0)])

        return imgs


    def rafi_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["rafi st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["rafi st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["rafi st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["rafi bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["rafi bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["rafi bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["rafi bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["rafi bk05", (x, 0)])

        return imgs


    def dina_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["dina st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["dina st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["dina st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["dina bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["dina bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["dina bk03", (x, 0)])

        return imgs


    def jelly_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["jelly st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["jelly st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["jelly st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["jelly bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["jelly bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["jelly bk03", (x, 0)])

        return imgs


    def blob_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["blob st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["blob st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["blob st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["blob bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["blob bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["blob bk03", (x, 0)])

        return imgs


    def slime_green_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slime_green st01", (x, 100)])
        elif persistent.face == 2:
            imgs.append(["slime_green st02", (x, 100)])
        elif persistent.face == 3:
            imgs.append(["slime_green st03", (x, 100)])

        if persistent.bk1:
            imgs.append(["slime_green bk01", (x, 100)])
        if persistent.bk2:
            imgs.append(["slime_green bk02", (x, 100)])
        if persistent.bk3:
            imgs.append(["slime_green bk03", (x, 100)])

        return imgs


    def slime_blue_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slime_blue st01", (x, 20)])
        elif persistent.face == 2:
            imgs.append(["slime_blue st02", (x, 20)])
        elif persistent.face == 3:
            imgs.append(["slime_blue st03", (x, 20)])

        if persistent.bk1:
            imgs.append(["slime_blue bk01", (x, 20)])
        if persistent.bk2:
            imgs.append(["slime_blue bk02", (x, 20)])
        if persistent.bk3:
            imgs.append(["slime_blue bk03", (x, 20)])

        return imgs


    def slime_red_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slime_red st01", (x, 11)])
        elif persistent.face == 2:
            imgs.append(["slime_red st02", (x, 11)])
        elif persistent.face == 3:
            imgs.append(["slime_red st03", (x, 11)])

        if persistent.bk1:
            imgs.append(["slime_red bk01", (x, 11)])
        if persistent.bk2:
            imgs.append(["slime_red bk02", (x, 11)])
        if persistent.bk3:
            imgs.append(["slime_red bk03", (x, 11)])

        return imgs


    def slime_purple_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slime_purple st01", (x, 40)])
        elif persistent.face == 2:
            imgs.append(["slime_purple st02", (x, 40)])
        elif persistent.face == 3:
            imgs.append(["slime_purple st03", (x, 40)])

        if persistent.bk1:
            imgs.append(["slime_purple bk01", (x, 40)])
        if persistent.bk2:
            imgs.append(["slime_purple bk02", (x, 40)])
        if persistent.bk3:
            imgs.append(["slime_purple bk03", (x, 40)])

        return imgs


    def erubetie1_pose(x):
        imgs = []

        imgs.append(["erubetie st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["erubetie bk07", (x, 0)])

        return imgs


    def undine_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose == 2:
            persistent.face = 1
        if persistent.pose == 3 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["undine st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["undine st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["undine st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["undine st11", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["undine st21", (x+226, 144)])
            elif persistent.face == 2:
                imgs.append(["undine st22", (x+226, 144)])
            elif persistent.face == 3:
                imgs.append(["undine st23", (x+226, 144)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["undine bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["undine bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["undine bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["undine bk04", (x, 0)])

        return imgs


    def kamakiri_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kamakiri st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kamakiri st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kamakiri st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kamakiri bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kamakiri bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kamakiri bk03", (x, 0)])

        return imgs


    def scylla_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["scylla st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["scylla st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["scylla st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["scylla bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["scylla bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["scylla bk03", (x, 0)])

        return imgs


    def medusa_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["medusa st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["medusa st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["medusa st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["medusa bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["medusa bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["medusa bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["medusa bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["medusa bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["medusa bk06", (x, 0)])

        return imgs


    def golem_pose(x):
        imgs = []

        imgs.append(["golem st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["golem bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["golem bk02", (x, 0)])

        return imgs


    def madgolem_pose(x):
        imgs = []

        imgs.append(["madgolem st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["madgolem bk01", (x, 0)])

        return imgs


    def artm_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["artm st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["artm st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["artm bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["artm bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["artm bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["artm bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["artm bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["artm bk06", (x, 0)])

        return imgs


    def ant_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["ant st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["ant st13", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["ant st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["ant st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["ant bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["ant bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["ant bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["ant bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["ant bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["ant bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["ant bk07", (x, 0)])

        return imgs


    def queenant_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["queenant st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["queenant st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["queenant bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["queenant bk02", (x, 0)])

        return imgs


    def maccubus_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose < 3 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 3:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["maccubus st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["maccubus st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["maccubus st03", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["maccubus st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["maccubus st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["maccubus st13", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["maccubus st21", (x, 0)])

        if persistent.pose != 3:
            if persistent.bk1:
                imgs.append(["maccubus bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["maccubus bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["maccubus bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["maccubus bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["maccubus bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["maccubus bk06", (x, 0)])

        return imgs


    def minccubus_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose == 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["minccubus st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["minccubus st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["minccubus st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["minccubus st11", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["minccubus bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["minccubus bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["minccubus bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["minccubus bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["minccubus bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["minccubus bk06", (x, 0)])
            if persistent.bk7:
                imgs.append(["minccubus bk07", (x, 0)])

        return imgs


    def renccubus_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        if persistent.pose == 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["renccubus st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["renccubus st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["renccubus st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["renccubus st04", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["renccubus st11", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["renccubus bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["renccubus bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["renccubus bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["renccubus bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["renccubus bk05", (x, 0)])

        return imgs


    def succubus_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["succubus st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["succubus st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["succubus st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["succubus st04", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["succubus st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["succubus st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["succubus st13", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["succubus st14", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["succubus bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["succubus bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["succubus bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["succubus bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["succubus bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["succubus bk06", (x, 0)])
            if persistent.bk7:
                imgs.append(["succubus bk07", (x, 0)])

        return imgs


    def witchs_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["witchs st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["witchs st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["witchs st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["witchs st04", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["witchs st11", (x, 0)])
            elif persistent.pose == 2 and persistent.face == 2:
                imgs.append(["witchs st12", (x, 0)])
            elif persistent.pose == 2 and persistent.face == 3:
                imgs.append(["witchs st13", (x, 0)])
            elif persistent.pose == 2 and persistent.face == 4:
                imgs.append(["witchs st14", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["witchs st21", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["witchs st22", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["witchs st23", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["witchs st24", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["witchs bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["witchs bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["witchs bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["witchs bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["witchs bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["witchs bk06", (x, 0)])

        return imgs


    def lilith_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["lilith st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["lilith st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["lilith st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["lilith bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["lilith bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["lilith bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["lilith bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["lilith bk05", (x, 0)])

        return imgs


    def madaminsect_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        # if persistent.pose == 2:
        #     persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["madaminsect st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["madaminsect st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["madaminsect st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["madaminsect st11", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["madaminsect bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["madaminsect bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["madaminsect bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["madaminsect bk04", (x, 0)])

        return imgs


    def madamumbrella_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if (persistent.pose == 1 and persistent.face > 3) or (persistent.pose == 2 and persistent.face > 2):
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["madamumbrella st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["madamumbrella st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["madamumbrella st03", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["madamumbrella st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["madamumbrella st12", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["madamumbrella bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["madamumbrella bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["madamumbrella bk03", (x, 0)])

        return imgs


    def maidscyulla_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if (persistent.pose == 1 and persistent.face > 3) or persistent.pose == 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["maidscyulla st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["maidscyulla st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["maidscyulla st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["maidscyulla st11", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["maidscyulla bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["maidscyulla bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["maidscyulla bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["maidscyulla bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["maidscyulla bk05", (x, 0)])
            if persistent.bk6:
                imgs.append(["maidscyulla bk06", (x, 0)])

        return imgs


    def emily_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1

        if (persistent.pose == 1 or persistent.pose == 3) and persistent.face > 4:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        if persistent.pose == 4:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["emily st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["emily st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["emily st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["emily st04", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["emily st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["emily st12", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["emily st21", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["emily st22", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["emily st23", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["emily st24", (x, 0)])
        elif persistent.pose == 4:
            imgs.append(["emily st31", (x, 0)])

        if persistent.pose < 3:
            if persistent.bk1:
                imgs.append(["emily bk01", (x, 0)])
            if persistent.pose == 1:
                if persistent.bk2:
                    imgs.append(["emily bk02", (x, 0)])
                if persistent.bk3:
                    imgs.append(["emily bk03", (x, 0)])
            elif persistent.pose == 2:
                if persistent.bk4:
                    imgs.append(["emily bk04", (x, 0)])

        return imgs


    def cassandra_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if persistent.pose == 1 and persistent.face > 5:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["cassandra st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["cassandra st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["cassandra st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["cassandra st04", (x, 0)])
            elif persistent.face == 5:
                imgs.append(["cassandra st05", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["cassandra st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["cassandra st12", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["cassandra bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["cassandra bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["cassandra bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["cassandra bk04", (x, 0)])

        return imgs


    def yougan_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["yougan st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["yougan st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["yougan st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["yougan bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["yougan bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["yougan bk03", (x, 0)])

        return imgs


    def basilisk_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["basilisk st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["basilisk st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["basilisk st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["basilisk bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["basilisk bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["basilisk bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["basilisk bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["basilisk bk05", (x, 0)])

        return imgs


    def dragon_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["dragon st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["dragon st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["dragon st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["dragon bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["dragon bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["dragon bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["dragon bk04", (x, 0)])

        return imgs


    def salamander_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if (persistent.pose == 1 or persistent.pose == 3) and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["salamander st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["salamander st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["salamander st03", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["salamander st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["salamander st12", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["salamander st21", (x+235, 85)])
            elif persistent.face == 2:
                imgs.append(["salamander st22", (x+235, 85)])
            elif persistent.face == 3:
                imgs.append(["salamander st23", (x+235, 85)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["salamander bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["salamander bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["salamander bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["salamander bk04", (x, 0)])

        return imgs


    def granberia3_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1
        if persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        if persistent.pose == 3 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["granberia st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["granberia st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["granberia st04", (x, 0)])
            elif persistent.face == 5:
                imgs.append(["granberia st05", (x, 0)])
            elif persistent.face == 6:
                imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["granberia st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["granberia st41", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs


    def kani2_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        if (persistent.pose == 1 and persistent.face > 3) or persistent.pose == 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["kani st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["kani st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["kani st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["kani st00", (x, 0)])

        if persistent.bk1:
            imgs.append(["kani bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["kani bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["kani bk03", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk4:
                imgs.append(["kani bk04", (x, 0)])

        return imgs


    def dagon_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["dagon st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["dagon st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["dagon st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["dagon st11", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["dagon bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["dagon bk02", (x, 0)])

        return imgs


    def poseidones_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["poseidones st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["poseidones st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["poseidones st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["poseidones bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["poseidones bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["poseidones bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["poseidones bk04", (x, 0)])

        return imgs


    def seiren_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["seiren st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["seiren st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["seiren st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["seiren bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["seiren bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["seiren bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["seiren bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["seiren bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["seiren bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["seiren bk07", (x, 0)])

        return imgs


    def hitode_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["hitode st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["hitode st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["hitode st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["hitode bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["hitode bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["hitode bk03", (x, 0)])

        return imgs


    def beelzebub_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["beelzebub_a st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["beelzebub_a st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["beelzebub_a st03", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["beelzebub_b st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["beelzebub_b st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["beelzebub_b st13", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["beelzebub_c st21", (x-200, 0)])
            elif persistent.face == 2:
                imgs.append(["beelzebub_c st22", (x-200, 0)])
            elif persistent.face == 3:
                imgs.append(["beelzebub_c st23", (x-200, 0)])

        if persistent.bk1:
            if persistent.pose == 1:
                imgs.append(["beelzebub bk01", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["beelzebub bk11", (x, 0)])
            elif persistent.pose == 3:
                imgs.append(["beelzebub bk21", (x-200, 0)])
        
        if persistent.bk2:
            if persistent.pose == 1:
                imgs.append(["beelzebub bk02", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["beelzebub bk12", (x, 0)])
            elif persistent.pose == 3:
                imgs.append(["beelzebub bk22", (x-200, 0)])
        
        if persistent.bk3:
            if persistent.pose == 1:
                imgs.append(["beelzebub bk03", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["beelzebub bk13", (x, 0)])
            elif persistent.pose == 3:
                imgs.append(["beelzebub bk23", (x-200, 0)])
        
        if persistent.bk4:
            if persistent.pose == 1:
                imgs.append(["beelzebub bk04", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["beelzebub bk14", (x, 0)])
            elif persistent.pose == 3:
                imgs.append(["beelzebub bk24", (x-200, 0)])

        return imgs


    def trickfairy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["trickfairy st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["trickfairy st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["trickfairy st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["trickfairy bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["trickfairy bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["trickfairy bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["trickfairy bk04", (x, 0)])
        
        if persistent.bk5:
            imgs.append(["trickfairy bk05", (x, 0)])

        return imgs


    def queenfairy_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["queenfairy st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["queenfairy st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["queenfairy st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["queenfairy bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["queenfairy bk02", (x, 0)])

        return imgs


    def queenelf_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["queenelf st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["queenelf st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["queenelf st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["queenelf bk01", (x, 0)])
        
        if persistent.bk2:
            imgs.append(["queenelf bk02", (x, 0)])
        
        if persistent.bk3:
            imgs.append(["queenelf bk03", (x, 0)])
        
        if persistent.bk4:
            imgs.append(["queenelf bk04", (x, 0)])
        
        if persistent.bk5:
            imgs.append(["queenelf bk05", (x, 0)])

        return imgs


    def saraevil_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["saraevil st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["saraevil st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["saraevil st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["saraevil bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["saraevil bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["saraevil bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["saraevil bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["saraevil bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["saraevil bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["saraevil bk07", (x, 0)])
        if persistent.bk8:
            imgs.append(["saraevil bk08", (x, 0)])

        return imgs


    def wyvern_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["wyvern st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["wyvern st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["wyvern st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["wyvern bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["wyvern bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["wyvern bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["wyvern bk04", (x, 0)])

        return imgs


    def kyoryuu_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kyoryuu st01", (x, 0)])
        if persistent.face == 2:
            imgs.append(["kyoryuu st02", (x, 0)])
        if persistent.face == 3:
            imgs.append(["kyoryuu st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["kyoryuu bk01", (x, 0)])

        return imgs


    def c_beast_pose(x):
        imgs = []

        if persistent.face > 2:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_beast st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_beast st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["c_beast bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["c_beast bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["c_beast bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["c_beast bk04", (x, 0)])

        return imgs


    def c_dryad_vore_pose(x):
        imgs = []

        imgs.append(["c_dryad_vore st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["c_dryad_vore bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["c_dryad_vore bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["c_dryad_vore bk03", (x, 0)])

        return imgs


    def vampire_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["vampire st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["vampire st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["vampire st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["vampire bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["vampire bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["vampire bk03", (x, 0)])

        return imgs


    def behemoth_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["behemoth st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["behemoth st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["behemoth st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["behemoth bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["behemoth bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["behemoth bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["behemoth bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["behemoth bk05", (x, 0)])

        return imgs


    def esuccubus_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["esuccubus st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["esuccubus st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["esuccubus st03", (x, 0)])

        if persistent.bk1:
            imgs.append(["esuccubus bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["esuccubus bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["esuccubus bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["esuccubus bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["esuccubus bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["esuccubus bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["esuccubus bk07", (x, 0)])

        return imgs


    def hatibi_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["hatibi st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["hatibi st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["hatibi st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["hatibi st04", (x, 0)])

        if persistent.bk1:
            imgs.append(["hatibi bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["hatibi bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["hatibi bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["hatibi bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["hatibi bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["hatibi bk06", (x, 0)])

        return imgs


    def inp_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["inp_a st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["inp_a st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["inp_a st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["inp_a st04", (x, 0)])

        if persistent.bk1:
            imgs.append(["inp bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["inp bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["inp bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["inp bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["inp bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["inp bk06", (x, 0)])

        return imgs


    def gigantweapon_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["gigantweapon st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["gigantweapon st02", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["gigantweapon st11", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["gigantweapon st21", (x, 0)])

        if persistent.bk1:
            imgs.append(["gigantweapon bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["gigantweapon bk02", (x, 0)])
        if persistent.bk3:
            if persistent.pose == 1:
                imgs.append(["gigantweapon bk05", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["gigantweapon bk04", (x, 0)])
            elif persistent.pose == 3:
                imgs.append(["gigantweapon bk03", (x, 0)])

        return imgs


    def alma_elma3_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["alma_elma st51", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["alma_elma st52", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["alma_elma st53", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["alma_elma st21", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["alma_elma st22", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["alma_elma st23", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["alma_elma st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["alma_elma st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["alma_elma st13", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["alma_elma bk11", (x, 0)])
            if persistent.bk2:
                imgs.append(["alma_elma bk12", (x, 0)])
            if persistent.bk3:
                imgs.append(["alma_elma bk13", (x, 0)])
            if persistent.bk4:
                imgs.append(["alma_elma bk14", (x, 0)])
            if persistent.bk5:
                imgs.append(["alma_elma bk03", (x, 0)])
            if persistent.bk6:
                imgs.append(["alma_elma bk21", (x, 0)])
        if persistent.pose == 2:
            if persistent.bk1:
                imgs.append(["alma_elma bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["alma_elma bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["alma_elma bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["alma_elma bk04", (x, 0)])
            if persistent.bk5:
                imgs.append(["alma_elma bk13", (x, 0)])
            if persistent.bk6:
                imgs.append(["alma_elma bk16", (x, 0)])

        return imgs


    def tamamo2_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose > 5:
            persistent.pose = 1

        # if persistent.pose > 2:
        #     persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["tamamo st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["tamamo st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["tamamo st13", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["tamamo st14", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["tamamo st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["tamamo st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["tamamo st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["tamamo st04", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["tamamo st41", (x, 0)])
        elif persistent.pose == 4:
            imgs.append(["tamamo st21", (x, 0)])
        elif persistent.pose == 5:
            imgs.append(["tamamo st31", (x, 0)])

        if persistent.pose < 3:
            if persistent.bk1:
                imgs.append(["tamamo bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["tamamo bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["tamamo bk03", (x, 0)])
            if persistent.bk4:
                imgs.append(["tamamo bk04", (x, 0)])

            if persistent.pose == 1:
                if persistent.bk5:
                    imgs.append(["tamamo bk05", (x, 0)])
                if persistent.bk6:
                    imgs.append(["tamamo bk06", (x, 0)])
                if persistent.bk7:
                    imgs.append(["tamamo bk07", (x, 0)])

        return imgs


    def erubetie2_pose(x):
        imgs = []

        imgs.append(["erubetie st01", (x, 0)])

        if persistent.bk1:
            imgs.append(["erubetie bk01", (x, 0)])
        if persistent.bk2:
            imgs.append(["erubetie bk02", (x, 0)])
        if persistent.bk3:
            imgs.append(["erubetie bk03", (x, 0)])
        if persistent.bk4:
            imgs.append(["erubetie bk04", (x, 0)])
        if persistent.bk5:
            imgs.append(["erubetie bk05", (x, 0)])
        if persistent.bk6:
            imgs.append(["erubetie bk06", (x, 0)])
        if persistent.bk7:
            imgs.append(["erubetie bk07", (x, 0)])

        return imgs


    def granberia4_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 5:
            persistent.pose = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["granberia st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["granberia st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["granberia st04", (x, 0)])
            elif persistent.face == 5:
                imgs.append(["granberia st05", (x, 0)])
            elif persistent.face == 6:
                imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["granberia st11", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st12", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["granberia st41", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st42", (x, 0)])
        elif persistent.pose == 4:
            if persistent.face == 1:
                imgs.append(["granberia st51", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["granberia st52", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["granberia st53", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs


    def alice3_pose(x):
        imgs = []

        if persistent.pose > 7:
            persistent.pose = 1
        if persistent.pose < 5 and persistent.face > 10:
            persistent.face = 1
        # if persistent.pose < 5 and persistent.face == 0:
        #     persistent.face = 10
        # if persistent.pose > 4:
        #     persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice st01b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alice st11", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alice st11b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice st02b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alice st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["alice st12b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice st03b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alice st13", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["alice st13b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["alice st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["alice st04b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["alice st14", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["alice st14b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["alice st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["alice st05b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["alice st15", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["alice st15b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["alice st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["alice st06b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["alice st16", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["alice st16bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["alice st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["alice st07b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["alice st17", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["alice st17bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["alice st08", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["alice st08b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["alice st18", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 8:
            imgs.append(["alice st18bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["alice st09", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 9:
            imgs.append(["alice st09b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["alice st19", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 9:
            imgs.append(["alice st19bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 10:
            imgs.append(["alice st51", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 10:
            imgs.append(["alice st51b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 10:
            imgs.append(["alice st61", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 10:
            imgs.append(["alice st61b", (x-80, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["alice st21", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 1:
            imgs.append(["alice st23", (x, 0)])
        elif persistent.pose == 7 and persistent.face == 1:
            imgs.append(["alice st31", (x+80, 125)])

        if persistent.bk1:
            if persistent.pose == 1:
                imgs.append(["alice bk01", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk01b", (x-80, 0)])
            elif persistent.pose == 3:
                imgs.append(["alice bk01", (x, 0)])
            elif persistent.pose == 4:
                imgs.append(["alice bk01b", (x-80, 0)])
            elif persistent.pose == 5:
                imgs.append(["alice bk11", (x, 0)])
            elif persistent.pose == 6:
                imgs.append(["alice bk12", (x, 0)])
        if persistent.bk2:
            if persistent.pose == 1:
                imgs.append(["alice bk02", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk02b", (x-80, 0)])
            elif persistent.pose == 3:
                imgs.append(["alice bk02", (x, 0)])
            elif persistent.pose == 4:
                imgs.append(["alice bk02b", (x-80, 0)])
        if persistent.bk3:
            if persistent.pose == 1:
                imgs.append(["alice bk03", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk03b", (x-80, 0)])
            elif persistent.pose == 3:
                imgs.append(["alice bk03", (x, 0)])
            elif persistent.pose == 4:
                imgs.append(["alice bk03b", (x-80, 0)])
        if persistent.bk4:
            if persistent.pose == 1:
                imgs.append(["alice bk04", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk04b", (x-80, 0)])
            elif persistent.pose == 3:
                imgs.append(["alice bk04", (x, 0)])
            elif persistent.pose == 4:
                imgs.append(["alice bk04b", (x-80, 0)])
        if persistent.bk5:
            if persistent.pose == 1:
                imgs.append(["alice bk05", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk05b", (x-80, 0)])
            elif persistent.pose == 3:
                imgs.append(["alice bk05", (x, 0)])
            elif persistent.pose == 4:
                imgs.append(["alice bk05b", (x-80, 0)])
        if persistent.bk6:
            if persistent.pose == 1:
                imgs.append(["alice bk06", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk06b", (x-80, 0)])
        if persistent.bk7:
            if persistent.pose == 1:
                imgs.append(["alice bk07", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk07b", (x-80, 0)])
        if persistent.bk8:
            if persistent.pose == 1:
                imgs.append(["alice bk08", (x, 0)])
            elif persistent.pose == 2:
                imgs.append(["alice bk08b", (x-80, 0)])

        return imgs


    def ilias2_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1
        elif persistent.pose > 1 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1:
            if persistent.face == 1:
                imgs.append(["ilias st01", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["ilias st02", (x, 0)])
            elif persistent.face == 3:
                imgs.append(["ilias st03", (x, 0)])
            elif persistent.face == 4:
                imgs.append(["ilias st04", (x, 0)])
            elif persistent.face == 5:
                imgs.append(["ilias st05", (x, 0)])
            elif persistent.face == 6:
                imgs.append(["ilias st06", (x, 0)])
        elif persistent.pose == 2:
            if persistent.face == 1:
                imgs.append(["ilias st13", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["ilias st12", (x, 0)])
        elif persistent.pose == 3:
            if persistent.face == 1:
                imgs.append(["ilias st14", (x, 0)])
            elif persistent.face == 2:
                imgs.append(["ilias st16", (x, 0)])

        return imgs


    def cupid_pose(x):
        imgs = []

        if persistent.face > 6:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["cupid st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["cupid st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["cupid st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["cupid st04", (x, 0)])
        elif persistent.face == 5:
            imgs.append(["cupid st05", (x, 0)])
        elif persistent.face == 6:
            imgs.append(["cupid st06", (x, 0)])

        if persistent.bk1:
            imgs.append(["cupid bk01", (x, 0)])

        if persistent.bk2:
            imgs.append(["cupid bk02", (x, 0)])

        if persistent.bk3:
            imgs.append(["cupid bk03", (x, 0)])

        if persistent.bk4:
            imgs.append(["cupid bk04", (x, 0)])

        return imgs


    def valkyrie_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["valkyrie st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["valkyrie st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["valkyrie st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["valkyrie st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["valkyrie st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["valkyrie st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["valkyrie st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["valkyrie st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["valkyrie st23", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["valkyrie st31", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["valkyrie st32", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["valkyrie st33", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["valkyrie bk01", (x, 0)])
        elif persistent.pose > 1 and persistent.bk1 == 1:
            imgs.append(["valkyrie bk11", (x, 0)])

        if persistent.pose < 4 and persistent.bk2 == 1:
            imgs.append(["valkyrie bk02", (x, 0)])
        elif persistent.pose == 4 and persistent.bk2 == 1:
            imgs.append(["valkyrie bk32", (x, 0)])

        if persistent.pose < 4 and persistent.bk3 == 1:
            imgs.append(["valkyrie bk03", (x, 0)])
        elif persistent.pose == 4 and persistent.bk3 == 1:
            imgs.append(["valkyrie bk33", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["valkyrie bk34", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["valkyrie bk35", (x, 0)])

        return imgs


    def ariel_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["ariel st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["ariel st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["ariel st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["ariel bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["ariel bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["ariel bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["ariel bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["ariel bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["ariel bk06", (x, 0)])

        return imgs


    def c_s2_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_s2 st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_s2 st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_s2 st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["c_s2 bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["c_s2 bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["c_s2 bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["c_s2 bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["c_s2 bk05", (x, 0)])

        return imgs


    def c_a3_pose(x):
        imgs = []

        imgs.append(["c_a3 st01", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["c_a3 bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["c_a3 bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["c_a3 bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["c_a3 bk04", (x, 0)])

        return imgs


    def stein1_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        
        if persistent.pose < 3 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 3:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["stein st21", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["stein st22", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["stein st23", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["stein st01r", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["stein st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["stein st03", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["stein st11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["stein bk11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["stein bk12", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["stein bk13", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["stein bk14", (x, 0)])

        return imgs


    def ranael_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1
        
        if (persistent.pose == 1 or persistent.pose == 4) and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["ranael st31", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["ranael st32", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["ranael st33", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["ranael st01", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["ranael st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["ranael st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["ranael st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["ranael st22", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["ranael st23", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["ranael bk31", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1 and persistent.bk1 == 1:
            imgs.append(["ranael bk11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2 and persistent.bk1 == 1:
            imgs.append(["ranael bk12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2 and persistent.bk1 == 1:
            imgs.append(["ranael bk22", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3 and persistent.bk1 == 1:
            imgs.append(["ranael bk23", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["ranael bk32", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["ranael bk33", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["ranael bk34", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["ranael bk35", (x, 0)])

        return imgs


    def c_tangh_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_tangh st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_tangh st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_tangh st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["c_tangh bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["c_tangh bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["c_tangh bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["c_tangh bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["c_tangh bk05", (x, 0)])

        return imgs


    def angels_pose(x):
        imgs = []

        if persistent.pose > 5:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["angels_a st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["angels_a st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["angels_a st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["angels_b st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["angels_b st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["angels_b st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["angels_c st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["angels_c st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["angels_c st23", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["angels_d st31", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["angels_d st32", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["angels_d st33", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["angels_e st41", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["angels_e st42", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 3:
            imgs.append(["angels_e st43", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["angels bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["angels bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["angels bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["angels bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["angels bk05", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["angels bk06", (x, 0)])
        elif persistent.pose == 3 and persistent.bk6 == 1:
            imgs.append(["angels bk26", (x, 0)])
        elif persistent.pose == 4 and persistent.bk6 == 1:
            imgs.append(["angels bk36", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk7 == 1:
            imgs.append(["angels bk07", (x, 0)])
        elif persistent.pose == 3 and persistent.bk7 == 1:
            imgs.append(["angels bk27", (x, 0)])
        elif persistent.pose == 4 and persistent.bk7 == 1:
            imgs.append(["angels bk37", (x, 0)])

        return imgs


    def nagael_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["nagael st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["nagael st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["nagael st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["nagael bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["nagael bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["nagael bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["nagael bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["nagael bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["nagael bk06", (x, 0)])
        
        if persistent.bk7 == 1:
            imgs.append(["nagael bk07", (x, 0)])

        return imgs


    def c_slag_pose(x):
        imgs = []
        imgs.append(["c_slag st01", (x, 0)])

        return imgs


    def c_tentacle_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_tentacle st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_tentacle st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_tentacle st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["c_tentacle bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["c_tentacle bk02", (x, 0)])

        return imgs


    def mariel_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mariel st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mariel st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mariel st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["mariel st04", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["mariel bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["mariel bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["mariel bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["mariel bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["mariel bk05", (x, 0)])

        return imgs


    def c_prison_pose(x):
        imgs = []
        imgs.append(["c_prison st01", (x, 0)])

        return imgs


    def c_medulahan_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_medulahan st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_medulahan st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_medulahan st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["c_medulahan bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["c_medulahan bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["c_medulahan bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["c_medulahan bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["c_medulahan bk05", (x, 0)])

        return imgs


    def trinity_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["trinity_a st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["trinity_a st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["trinity_a st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["trinity_a st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["trinity_b st11", (x+265, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["trinity_b st12", (x+265, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["trinity_b st13", (x+265, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["trinity_b st14", (x+265, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["trinity_c st21", (x-260, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["trinity_c st22", (x-260, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["trinity_c st23", (x-260, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["trinity_c st24", (x-260, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["trinity bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["trinity bk11", (x+265, 0)])
        elif persistent.pose == 3 and persistent.bk1 == 1:
            imgs.append(["trinity bk21", (x-260, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["trinity bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["trinity bk12", (x+265, 0)])
        elif persistent.pose == 3 and persistent.bk2 == 1:
            imgs.append(["trinity bk22", (x-260, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["trinity bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["trinity bk13", (x+265, 0)])
        elif persistent.pose == 3 and persistent.bk3 == 1:
            imgs.append(["trinity bk23", (x-260, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["trinity bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["trinity bk14", (x+265, 0)])
        elif persistent.pose == 3 and persistent.bk4 == 1:
            imgs.append(["trinity bk24", (x-260, 0)])

        return imgs


    def c_bug_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_bug st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_bug st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_bug st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["c_bug bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["c_bug bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["c_bug bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["c_bug bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["c_bug bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["c_bug bk06", (x, 0)])

        return imgs


    def sisterlamia_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 5:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["sisterlamia st01r", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["sisterlamia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["sisterlamia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["sisterlamia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["sisterlamia st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["sisterlamia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["sisterlamia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["sisterlamia st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["sisterlamia st14", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["sisterlamia st15", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["sisterlamia st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["sisterlamia st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["sisterlamia st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["sisterlamia st24", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["sisterlamia st25", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["sisterlamia bk01", (x, 0)])

        if persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["sisterlamia bk12", (x, 0)])
        elif persistent.pose == 3 and persistent.bk2 == 1:
            imgs.append(["sisterlamia bk22", (x, 0)])

        if persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["sisterlamia bk13", (x, 0)])
        elif persistent.pose == 3 and persistent.bk3 == 1:
            imgs.append(["sisterlamia bk13", (x, 0)])

        if persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["sisterlamia bk14", (x, 0)])
        elif persistent.pose == 3 and persistent.bk4 == 1:
            imgs.append(["sisterlamia bk14", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["sisterlamia bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["sisterlamia bk06", (x, 0)])

        if persistent.bk7 == 1:
            imgs.append(["sisterlamia bk07", (x, 0)])

        return imgs


    def catoblepas_pose(x):
        imgs = []

        imgs.append(["catoblepas st01", (x, 0)])

        return imgs


    def muzukiel_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["muzukiel st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["muzukiel st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["muzukiel st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["muzukiel st11", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["muzukiel bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["muzukiel bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["muzukiel bk03", (x, 0)])
        
        if persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["muzukiel bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["muzukiel bk05", (x, 0)])

        return imgs


    def mermaid_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        
        if persistent.face == 1:
            imgs.append(["mermaid st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mermaid st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mermaid st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["mermaid bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["mermaid bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["mermaid bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["mermaid bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["mermaid bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["mermaid bk06", (x, 0)])
        
        if persistent.bk7 == 1:
            imgs.append(["mermaid bk07", (x, 0)])

        return imgs


    def g_mermaid_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1
        
        if persistent.face == 1:
            imgs.append(["g_mermaid st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["g_mermaid st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["g_mermaid st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["g_mermaid st04", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["g_mermaid bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["g_mermaid bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["g_mermaid bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["g_mermaid bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["g_mermaid bk05", (x, 0)])

        return imgs


    def ningyohime_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 9:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["ningyohime st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["ningyohime st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["ningyohime st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["ningyohime st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["ningyohime st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["ningyohime st06", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["ningyohime st07", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["ningyohime st08", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["ningyohime st09", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["ningyohime st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["ningyohime st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["ningyohime st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["ningyohime st14", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["ningyohime st15", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["ningyohime st16", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["ningyohime st17", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["ningyohime st18", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 9:
            imgs.append(["ningyohime st19", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["ningyohime st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["ningyohime st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["ningyohime st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["ningyohime st24", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["ningyohime st25", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["ningyohime st26", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["ningyohime st27", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["ningyohime st28", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["ningyohime st29", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["ningyohime bk01", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["ningyohime bk02", (x, 0)])
        elif persistent.pose > 1 and persistent.bk2 == 1:
            imgs.append(["ningyohime bk12", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["ningyohime bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["ningyohime bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["ningyohime bk05", (x, 0)])

        return imgs


    def queenmermaid_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["queenmermaid st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["queenmermaid st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["queenmermaid st11", (x+60, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["queenmermaid st13", (x+60, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["queenmermaid bk01", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["queenmermaid bk02", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["queenmermaid bk03", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["queenmermaid bk04", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["queenmermaid bk05", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["queenmermaid bk06", (x, 0)])

        return imgs


    def xx7_pose(x):
        imgs = []
        imgs.append(["xx_7 st01", (x, 0)])
        return imgs


    def shadow_pose(x):
        imgs = []

        imgs.append(["shadow st01", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["shadow bk01", (x, 0)])
        elif persistent.bk2 == 1:
            imgs.append(["shadow bk02", (x, 0)])
        elif persistent.bk3 == 1:
            imgs.append(["shadow bk03", (x, 0)])
        elif persistent.bk4 == 1:
            imgs.append(["shadow bk04", (x, 0)])

        return imgs


    def gaistvine_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 8:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["gaistvine st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["gaistvine st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["gaistvine st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["gaistvine bk11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["gaistvine bk12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["gaistvine bk13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["gaistvine bk14", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["gaistvine bk15", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["gaistvine bk16", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["gaistvine bk17", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["gaistvine bk01", (x, 0)])

        return imgs


    def chrom2_pose(x):
        imgs = []

        if persistent.pose > 8:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 9:
            persistent.face = 1
        elif 3 < persistent.pose < 8 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 8 and persistent.face > 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["chrom st41", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["chrom st42", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["chrom st43", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["chrom st31", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["chrom st32", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["chrom st21", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["chrom st22", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["chrom st23", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["chrom st24", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["chrom st25", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["chrom st26", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["chrom st27", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["chrom st28", (x+20, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["chrom st29", (x+20, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["chrom st51", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["chrom st52", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["chrom st53", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["chrom st54", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["chrom st61", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["chrom st62", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 3:
            imgs.append(["chrom st63", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 4:
            imgs.append(["chrom st64", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 1:
            imgs.append(["chrom st71", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 2:
            imgs.append(["chrom st72", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 3:
            imgs.append(["chrom st73", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 4:
            imgs.append(["chrom st74", (x, 0)])
        elif persistent.pose == 7 and persistent.face == 1:
            imgs.append(["chromg st01", (x+10, 0)])
        elif persistent.pose == 7 and persistent.face == 2:
            imgs.append(["chromg st02", (x+10, 0)])
        elif persistent.pose == 7 and persistent.face == 3:
            imgs.append(["chromg st03", (x+10, 0)])
        elif persistent.pose == 8 and persistent.face == 1:
            imgs.append(["chromg st11", (x+10, 0)])
        elif persistent.pose == 8 and persistent.face == 2:
            imgs.append(["chromg st12", (x+10, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["chrom bk41", (x, 0)])
        elif persistent.pose == 7 and persistent.bk1 == 1:
            imgs.append(["chromg bk01", (x+10, 0)])
        elif persistent.pose == 8 and persistent.bk1 == 1:
            imgs.append(["chromg bk11", (x+10, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["chrom bk42", (x, 0)])
        elif persistent.pose == 7 and persistent.bk2 == 1:
            imgs.append(["chromg bk02", (x+10, 0)])
        elif persistent.pose == 8 and persistent.bk2 == 1:
            imgs.append(["chromg bk12", (x+10, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["chrom bk43", (x, 0)])
        elif persistent.pose == 7 and persistent.bk3 == 1:
            imgs.append(["chromg bk03", (x+10, 0)])
        elif persistent.pose == 8 and persistent.bk3 == 1:
            imgs.append(["chromg bk13", (x+10, 0)])

        return imgs


    def berryel_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["berryel st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["berryel st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["berryel st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["berryel bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["berryel bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["berryel bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["berryel bk04", (x, 0)])

        return imgs


    def revel_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1:
            imgs.append(["revel st01", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["revel st11", (x, 0)])

        return imgs


    def gnome2_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1
        
        if persistent.pose < 3 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 3:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 9:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["gnome st11", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["gnome st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["gnome st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["gnome st02", (x, 0)])
        elif persistent.pose == 3:
            imgs.append(["gnome st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["gnome st31", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["gnome st32", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["gnome st33", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["gnome st34", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["gnome st35", (x+256,95)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["gnome st36", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["gnome st37", (x-40, 0)])
        elif persistent.pose == 4 and persistent.face == 8:
            imgs.append(["gnome st38", (x+256,18)])
        elif persistent.pose == 4 and persistent.face == 9:
            imgs.append(["gnome st39", (x+296,155)])

        return imgs


    def c_chariot_pose(x):
        imgs = []

        imgs.append(["c_chariot st01", (x, 0)])

        return imgs


    def carmilla_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["carmilla st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["carmilla st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["carmilla st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["carmilla bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["carmilla bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["carmilla bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["carmilla bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["carmilla bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["carmilla bk06", (x, 0)])

        return imgs


    def elisabeth_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["elisabeth st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["elisabeth st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["elisabeth st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["elisabeth bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["elisabeth bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["elisabeth bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["elisabeth bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["elisabeth bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["elisabeth bk06", (x, 0)])
        
        if persistent.bk7 == 1:
            imgs.append(["elisabeth bk07", (x, 0)])

        return imgs


    def queenvanpire_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["queenvanpire st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["queenvanpire st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["queenvanpire st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["queenvanpire st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["queenvanpire st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["queenvanpire st13", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["queenvanpire bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["queenvanpire bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["queenvanpire bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["queenvanpire bk04", (x, 0)])
        
        if persistent.pose == 2 and persistent.bk5 == 1:
            imgs.append(["queenvanpire bk05", (x, 0)])

        return imgs


    def c_homunculus_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["c_homunculus st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["c_homunculus st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["c_homunculus st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["c_homunculus bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["c_homunculus bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["c_homunculus bk03", (x, 0)])

        return imgs


    def ironmaiden_k_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["ironmaiden_k st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["ironmaiden_k st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["ironmaiden_k st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["ironmaiden_k bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["ironmaiden_k bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["ironmaiden_k bk03", (x, 0)])

        return imgs


    def lusia_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["lusia st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["lusia st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["lusia st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["lusia st04", (x, 0)])
        
        if persistent.face != 4 and persistent.bk1 == 1:
            imgs.append(["lusia bk01", (x, 0)])
        elif persistent.face == 4 and persistent.bk1 == 1:
            imgs.append(["lusia bk01b", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["lusia bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["lusia bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["lusia bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["lusia bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["lusia bk06", (x, 0)])
        
        if persistent.bk7 == 1:
            imgs.append(["lusia bk07", (x, 0)])

        return imgs


    def silkiel_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["silkiel st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["silkiel st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["silkiel st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["silkiel st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["silkiel st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["silkiel st13", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["silkiel bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["silkiel bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["silkiel bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["silkiel bk04", (x, 0)])

        return imgs


    def endiel_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["endiel st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["endiel st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["endiel st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["endiel st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["endiel st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["endiel st13", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["endiel bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["endiel bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["endiel bk03", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["endiel bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["endiel bk14", (x, 0)])
        
        if persistent.pose == 2 and persistent.bk5 == 1:
            imgs.append(["endiel bk05", (x, 0)])

        return imgs


    def sylph2_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 7:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 4 and persistent.face > 11:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["sylph st41", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["sylph st42", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["sylph st43", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["sylph st44", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["sylph st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["sylph st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["sylph st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["sylph st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["sylph st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["sylph st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["sylph st07", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["sylph st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["sylph st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["sylph st23", (x+155,100)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["sylph st24", (x+155,100)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["sylph st25", (x+155,100)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["sylph st26", (x+321,241)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["sylph st27", (x+321,241)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["sylph st31", (x, 100)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["sylph st32", (x, 100)])
        elif persistent.pose == 4 and persistent.face == 8:
            imgs.append(["sylph st33", (x+310,200)])
        elif persistent.pose == 4 and persistent.face == 9:
            imgs.append(["sylph st34", (x+323,245)])
        elif persistent.pose == 4 and persistent.face == 10:
            imgs.append(["sylph st35", (x+323,200)])
        elif persistent.pose == 4 and persistent.face == 11:
            imgs.append(["sylph st36", (x+323,88)])

        return imgs


    def typhon_pose(x):
        imgs = []

        imgs.append(["typhon st01", (x, 0)])

        return imgs


    def lamianloid_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["lamianloid st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["lamianloid st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["lamianloid st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["lamianloid st04", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["lamianloid st11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["lamianloid bk01", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["lamianloid bk02", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["lamianloid bk03", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["lamianloid bk04", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["lamianloid bk05", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["lamianloid bk06", (x, 0)])

        return imgs


    def knightloid_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1

        if persistent.face > 5:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["knightloid st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["knightloid st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["knightloid st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["knightloid st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["knightloid st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["knightloid st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["knightloid st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["knightloid st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["knightloid st14", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["knightloid st15", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["knightloid st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["knightloid st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["knightloid st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["knightloid st24", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["knightloid st25", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["knightloid st31", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["knightloid st32", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["knightloid st33", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["knightloid st34", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["knightloid st35", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["knightloid bk01", (x, 0)])

        if persistent.bk2 == 1 and (persistent.pose == 2 or persistent.pose == 4):
            imgs.append(["knightloid bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["knightloid bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["knightloid bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["knightloid bk05", (x, 0)])

        return imgs


    def akaname_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["akaname st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["akaname st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["akaname st03", (x, 0)])

        if persistent.face == 2 and persistent.bk1 == 1:
            imgs.append(["akaname bk01", (x, 0)])

        return imgs


    def mikolamia_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["mikolamia st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["mikolamia st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["mikolamia st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["mikolamia bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["mikolamia bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["mikolamia bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["mikolamia bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["mikolamia bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["mikolamia bk06", (x, 0)])

        if persistent.bk7 == 1:
            imgs.append(["mikolamia bk07", (x, 0)])

        return imgs


    def kezyorou_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["kezyorou st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["kezyorou st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["kezyorou st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["kezyorou bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["kezyorou bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["kezyorou bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["kezyorou bk04", (x, 0)])

        return imgs


    def sirohebisama_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["sirohebisama st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["sirohebisama st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["sirohebisama st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["sirohebisama bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["sirohebisama bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["sirohebisama bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["sirohebisama bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["sirohebisama bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["sirohebisama bk06", (x, 0)])

        return imgs


    def walraune_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["walraune st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["walraune st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["walraune st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["walraune bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["walraune bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["walraune bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["walraune bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["walraune bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["walraune bk06", (x, 0)])

        return imgs


    def dryad_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["dryad st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["dryad st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["dryad st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["dryad st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["dryad st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["dryad st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["dryad st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["dryad st14", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["dryad bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["dryad bk11", (x, 0)])

        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["dryad bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["dryad bk12", (x, 0)])

        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["dryad bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["dryad bk13", (x, 0)])

        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["dryad bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["dryad bk14", (x, 0)])

        return imgs


    def queenalraune_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["queenalraune st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["queenalraune st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["queenalraune st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["queenalraune st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["queenalraune st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["queenalraune st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["queenalraune st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["queenalraune st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["queenalraune st23", (x, 0)])

        if persistent.pose != 3:
            if persistent.bk1 == 1:
                imgs.append(["queenalraune bk01", (x, 0)])

            if persistent.bk2 == 1:
                imgs.append(["queenalraune bk02", (x, 0)])

            if persistent.bk3 == 1:
                imgs.append(["queenalraune bk03", (x, 0)])

            if persistent.bk4 == 1:
                imgs.append(["queenalraune bk04", (x, 0)])

            if persistent.pose == 2 and persistent.bk5 == 1:
                imgs.append(["queenalraune bk05", (x, 0)])

        return imgs


    def slimelord_pose(x):
        imgs = []

        if persistent.face > 5:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["slimelord st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["slimelord st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["slimelord st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["slimelord st04", (x, 0)])
        elif persistent.face == 5:
            imgs.append(["slimelord st05", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["slimelord bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["slimelord bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["slimelord bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["slimelord bk04", (x, 0)])

        return imgs


    def eggel_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["eggel st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["eggel st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["eggel st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["eggel bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["eggel bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["eggel bk03", (x, 0)])

        return imgs


    def salamander2_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 4:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["salamander st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["salamander st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["salamander st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["salamander st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["salamander st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["salamander st21", (x+235,85)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["salamander st22", (x+235,85)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["salamander st23", (x+235,85)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["salamander st24", (x+240,85)])

        return imgs


    def tutigumo_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["tutigumo st11", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["tutigumo st12", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["tutigumo st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["tutigumo st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["tutigumo st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["tutigumo st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["tutigumo bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["tutigumo bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["tutigumo bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["tutigumo bk04", (x, 0)])

        return imgs


    def alakneload_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["alakneload st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["alakneload st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["alakneload st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["alakneload bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["alakneload bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["alakneload bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["alakneload bk04", (x, 0)])

        return imgs


    def kumonomiko_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["kumonomiko st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["kumonomiko st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["kumonomiko st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["kumonomiko st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["kumonomiko st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["kumonomiko st13", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["kumonomiko bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["kumonomiko bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["kumonomiko bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["kumonomiko bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["kumonomiko bk05", (x, 0)])

        return imgs


    def narcubus_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["narcubus st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["narcubus st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["narcubus st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["narcubus st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["narcubus st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["narcubus st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["narcubus st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["narcubus st14", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["narcubus bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["narcubus bk11", (x, 0)])

        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["narcubus bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["narcubus bk12", (x, 0)])

        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["narcubus bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["narcubus bk13", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["narcubus bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["narcubus bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["narcubus bk06", (x, 0)])

        return imgs


    def inps_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        elif persistent.pose > 1 and persistent.face > 3:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["inp_a st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["inp_a st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["inp_a st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["inp_a st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["inp st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["inp st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["inp st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["inp st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["inp st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["inp st23", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["inp bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["inp bk11", (x, 0)])
        elif persistent.pose == 3 and persistent.bk1 == 1:
            imgs.append(["inp bk21", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["inp bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["inp bk12", (x, 0)])
        elif persistent.pose == 3 and persistent.bk2 == 1:
            imgs.append(["inp bk22", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["inp bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["inp bk13", (x, 0)])
        elif persistent.pose == 3 and persistent.bk3 == 1:
            imgs.append(["inp bk23", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["inp bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["inp bk14", (x, 0)])
        elif persistent.pose == 3 and persistent.bk4 == 1:
            imgs.append(["inp bk24", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["inp bk05", (x, 0)])
        elif persistent.pose == 2 and persistent.bk5 == 1:
            imgs.append(["inp bk15", (x, 0)])
        elif persistent.pose == 3 and persistent.bk5 == 1:
            imgs.append(["inp bk25", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["inp bk06", (x, 0)])

        return imgs


    def eva_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["eva st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["eva st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["eva st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["eva st11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["eva bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["eva bk11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["eva bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["eva bk12", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["eva bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["eva bk13", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["eva bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["eva bk14", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["eva bk05", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["eva bk06", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk7 == 1:
            imgs.append(["eva bk07", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk8 == 1:
            imgs.append(["eva bk08", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk9 == 1:
            imgs.append(["eva bk09", (x, 0)])

        return imgs


    def trooperloid_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 5:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 4:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["trooperloid st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["trooperloid st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["trooperloid st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["trooperloid st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["trooperloid st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["trooperloid st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["trooperloid st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["trooperloid st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["trooperloid st14", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["trooperloid bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["trooperloid bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["trooperloid bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["trooperloid bk04", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["trooperloid bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["trooperloid bk06", (x, 0)])
        
        if persistent.bk7 == 1:
            imgs.append(["trooperloid bk07", (x, 0)])
        
        if persistent.bk8 == 1:
            imgs.append(["trooperloid bk08", (x, 0)])

        return imgs


    def assassinloid_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose < 3 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 5:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["assassinloid st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["assassinloid st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["assassinloid st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["assassinloid st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["assassinloid st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["assassinloid st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["assassinloid st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["assassinloid st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["assassinloid st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["assassinloid st24", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["assassinloid st25", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["assassinloid bk01", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["assassinloid bk11", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["assassinloid bk02", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["assassinloid bk12", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["assassinloid bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["assassinloid bk13", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["assassinloid bk04", (x, 0)])
        elif persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["assassinloid bk14", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk5 == 1:
            imgs.append(["assassinloid bk05", (x, 0)])
        elif persistent.pose == 1 and persistent.bk6 == 1:
            imgs.append(["assassinloid bk06", (x, 0)])

        return imgs


    def yomotu_pose(x):
        imgs = []

        imgs.append(["yomotu st01", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["yomotu bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["yomotu bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["yomotu bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["yomotu bk04", (x, 0)])

        return imgs


    def wormiel_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["wormiel st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["wormiel st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["wormiel st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["wormiel bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["wormiel bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["wormiel bk03", (x, 0)])

        return imgs


    def undine2_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["undine st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["undine st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["undine st03", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["undine st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["undine st21", (x+226, 144)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["undine st22", (x+226, 144)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["undine st23", (x+226, 144)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["undine st24", (x+227, 145)])

        return imgs


    def alice4_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 10:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice st71", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice st72", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice st73", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["alice st74", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["alice st75", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["alice st76", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["alice st77", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["alice st79", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["alice st81", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 10:
            imgs.append(["alice st82", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice st92", (x+250, 150)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice st93", (x+250, 115)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice st94", (x+225, 200)])

        return imgs


    def drainplant_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["drainplant st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["drainplant st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["drainplant st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["drainplant bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["drainplant bk02", (x, 0)])

        return imgs


    def drainloid_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["drainloid st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["drainloid st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["drainloid st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["drainloid bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["drainloid bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["drainloid bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["drainloid bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["drainloid bk05", (x, 0)])

        return imgs


    def replicant_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["replicant st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["replicant st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["replicant st11", (x, 0)])
        
        if persistent.face == 1 and persistent.bk1 == 1:
            imgs.append(["replicant bk01", (x, 0)])
        elif persistent.face == 2 and persistent.bk1 == 1:
            imgs.append(["replicant bk11", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["replicant bk11", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["replicant bk02", (x, 0)])
        
        if persistent.bk3 == 1:
            imgs.append(["replicant bk03", (x, 0)])
        
        if persistent.bk4 == 1:
            imgs.append(["replicant bk04", (x, 0)])

        return imgs


    def laplace_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["laplace st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["laplace st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["laplace st03", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["laplace bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["laplace bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["laplace bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["laplace bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["laplace bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["laplace bk06", (x, 0)])

        if persistent.bk7 == 1:
            imgs.append(["laplace bk07", (x, 0)])

        if persistent.bk8 == 1:
            imgs.append(["laplace bk08", (x, 0)])

        return imgs


    def ad5_pose(x):
        imgs = []

        imgs.append(["ad_5 st01", (x, 0)])

        return imgs


    def fermesara_pose(x):
        imgs = []

        imgs.append(["fermesara st01", (x, 0)])


        if persistent.bk1 == 1:
            imgs.append(["fermesara bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["fermesara bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["fermesara bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["fermesara bk04", (x, 0)])

        return imgs


    def angelghoul_pose(x):
        imgs = []

        imgs.append(["angelghoul st01", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["angelghoul bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["angelghoul bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["angelghoul bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["angelghoul bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["angelghoul bk05", (x, 0)])

        if persistent.bk6 == 1:
            imgs.append(["angelghoul bk06", (x, 0)])

        return imgs


    def dragonzonbe_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        
        if (persistent.pose == 1 or persistent.pose == 3) and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["dragonzonbe st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["dragonzonbe st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["dragonzonbe st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["dragonzonbe st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["dragonzonbe st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["dragonzonbe st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["dragonzonbe st23", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["dragonzonbe bk01", (x, 0)])
        
        if persistent.bk2 == 1:
            imgs.append(["dragonzonbe bk02", (x, 0)])
        
        if persistent.pose < 3 and persistent.bk3 == 1:
            imgs.append(["dragonzonbe bk03", (x, 0)])
        
        if persistent.pose == 3 and persistent.bk4 == 1:
            imgs.append(["dragonzonbe bk04", (x, 0)])
        
        if persistent.pose == 3 and persistent.bk5 == 1:
            imgs.append(["dragonzonbe bk05", (x, 0)])

        return imgs


    def cirque1_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["cirque st41", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["cirque st42", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["cirque st43", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["cirque st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["cirque st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["cirque st03", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["cirque bk41", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["cirque bk01", (x, 0)])

        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["cirque bk42", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["cirque bk02", (x, 0)])

        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["cirque bk43", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["cirque bk03", (x, 0)])

        if persistent.pose == 2 and persistent.bk4 == 1:
            imgs.append(["cirque bk04", (x, 0)])

        return imgs


    def cirque2_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["cirque st51", (x-30, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["cirque st52", (x-30, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["cirque st53", (x-30, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["cirque st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["cirque st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["cirque st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["cirque st14", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["cirque bk51", (x-30, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["cirque bk11", (x, 0)])

        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["cirque bk52", (x-30, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["cirque bk12", (x, 0)])

        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["cirque bk53", (x-30, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["cirque bk13", (x, 0)])

        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["cirque bk54", (x-30, 0)])

        return imgs


    def cirque3_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["cirque st31", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["cirque st32", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["cirque st33", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["cirque st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["cirque st22", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["cirque st23", (x, 0)])

        if persistent.pose == 1 and persistent.bk1 == 1:
            imgs.append(["cirque bk31", (x, 0)])
        elif persistent.pose == 2 and persistent.bk1 == 1:
            imgs.append(["cirque bk21", (x, 0)])

        if persistent.pose == 1 and persistent.bk2 == 1:
            imgs.append(["cirque bk32", (x, 0)])
        elif persistent.pose == 2 and persistent.bk2 == 1:
            imgs.append(["cirque bk22", (x, 0)])

        if persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["cirque bk33", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["cirque bk23", (x, 0)])

        if persistent.pose == 1 and persistent.bk4 == 1:
            imgs.append(["cirque bk34", (x, 0)])

        return imgs


    def alice15th_pose(x):
        imgs = []

        if persistent.pose > 5:
            persistent.pose = 1

        if (persistent.pose < 4 or persistent.pose == 5) and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice15th st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice15th st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice15th st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice15th st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice15th st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice15th st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alice15th st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alice15th st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alice15th st23", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alice15th st31b", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["alice15th st41", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["alice15th st42", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 3:
            imgs.append(["alice15th st43", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk1 == 1:
            imgs.append(["alice15th bk01", (x, 0)])
        elif persistent.pose == 4 and persistent.bk1 == 1:
            imgs.append(["alice15th bk31", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk2 == 1:
            imgs.append(["alice15th bk02", (x, 0)])
        elif persistent.pose == 4 and persistent.bk2 == 1:
            imgs.append(["alice15th bk32", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk3 == 1:
            imgs.append(["alice15th bk03", (x, 0)])
        elif persistent.pose == 4 and persistent.bk3 == 1:
            imgs.append(["alice15th bk33", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk4 == 1:
            imgs.append(["alice15th bk04", (x, 0)])
        elif persistent.pose == 4 and persistent.bk4 == 1:
            imgs.append(["alice15th bk34", (x, 0)])
        
        if persistent.pose == 3 and persistent.bk5 == 1:
            imgs.append(["alice15th bk05", (x, 0)])
        elif persistent.pose == 4 and persistent.bk5 == 1:
            imgs.append(["alice15th bk35", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk6 == 1:
            imgs.append(["alice15th bk06", (x, 0)])
        elif persistent.pose == 4 and persistent.bk6 == 1:
            imgs.append(["alice15th bk36", (x, 0)])

        return imgs


    def shirom_pose(x):
        imgs = []

        if persistent.pose > 8:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 4:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 4:
            persistent.face = 1
        elif 2 < persistent.pose < 7 and persistent.face > 2:
            persistent.face = 1
        elif persistent.pose == 7:
            persistent.face = 1
        elif persistent.pose == 8 and persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["shirom st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["shirom st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["shirom st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["shirom st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["shirom st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["shirom st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["shirom st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["shirom st08", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["shirom st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["shirom st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["shirom st21", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["shirom st22", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["shirom st31", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["shirom st32", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 1:
            imgs.append(["shirom st41", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 2:
            imgs.append(["shirom st42", (x, 0)])
        elif persistent.pose == 7 and persistent.face == 1:
            imgs.append(["shirom st51", (x, 0)])
        elif persistent.pose == 8 and persistent.face == 1:
            imgs.append(["shirom st71", (x, 0)])
        elif persistent.pose == 8 and persistent.face == 2:
            imgs.append(["shirom st72", (x, 0)])
        elif persistent.pose == 8 and persistent.face == 3:
            imgs.append(["shirom st73", (x, 0)])

        if persistent.pose < 3 or persistent.pose == 8:
            if persistent.bk1 == 1:
                imgs.append(["shirom bk01", (x, 0)])

            if persistent.bk2 == 1:
                imgs.append(["shirom bk02", (x, 0)])

            if persistent.bk3 == 1:
                imgs.append(["shirom bk03", (x, 0)])

        return imgs


    def alice8th1_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["alice8th st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["alice8th st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["alice8th st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["alice8th st04", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["alice8th bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["alice8th bk02", (x, 0)])

        if persistent.bk3 == 1:
            imgs.append(["alice8th bk03", (x, 0)])

        if persistent.bk4 == 1:
            imgs.append(["alice8th bk04", (x, 0)])

        if persistent.bk5 == 1:
            imgs.append(["alice8th bk05", (x, 0)])

        return imgs


    def traptemis_pose(x):
        imgs = []
        imgs.append(["traptemis st01", (x, 0)])

        return imgs


    def gargoyle_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1
        
        if persistent.pose < 3 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face > 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["gargoyle st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["gargoyle st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["gargoyle st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["gargoyle st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["gargoyle st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["gargoyle st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["gargoyle st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["gargoyle st22", (x, 0)])
        
        if persistent.pose < 3 and persistent.bk1 == 1:
            imgs.append(["gargoyle bk01", (x, 0)])
        elif persistent.pose == 3 and persistent.bk1 == 1:
            imgs.append(["gargoyle bk21", (x, 0)])
        
        if persistent.pose < 3 and persistent.bk2 == 1:
            imgs.append(["gargoyle bk02", (x, 0)])

        return imgs


    def doppele_pose(x):
        imgs = []

        if persistent.pose > 6:
            persistent.pose = 1

        if persistent.face > 4:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["doppele st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["doppele st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["doppele st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["doppele st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["doppele st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["doppele st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["doppele st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["doppele st14", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["doppele st21", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["doppele st22", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["doppele st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["doppele st24", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["doppele st31", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["doppele st32", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["doppele st33", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["doppele st34", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 1:
            imgs.append(["doppele st41", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 2:
            imgs.append(["doppele st42", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 3:
            imgs.append(["doppele st43", (x, 0)])
        elif persistent.pose == 5 and persistent.face == 4:
            imgs.append(["doppele st44", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 1:
            imgs.append(["doppele st51", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 2:
            imgs.append(["doppele st52", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 3:
            imgs.append(["doppele st53", (x, 0)])
        elif persistent.pose == 6 and persistent.face == 4:
            imgs.append(["doppele st54", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["doppele bk01", (x, 0)])
        
        if persistent.pose < 3 and persistent.bk2 == 1:
            imgs.append(["doppele bk02", (x, 0)])
        elif persistent.pose == 3 and persistent.bk2 == 1:
            imgs.append(["doppele bk32", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk3 == 1:
            imgs.append(["doppele bk03", (x, 0)])
        elif persistent.pose == 6 and persistent.face > 2 and persistent.bk3 == 1:
            imgs.append(["doppele bk03", (x, 0)])
        
        if persistent.pose < 4 and persistent.bk4 == 1:
            imgs.append(["doppele bk04", (x, 0)])
        elif persistent.pose == 4 and persistent.bk4 == 1:
            imgs.append(["doppele bk34", (x, 0)])
        elif persistent.pose == 5 and persistent.bk4 == 1:
            imgs.append(["doppele bk34", (x, 0)])
        
        if persistent.bk5 == 1:
            imgs.append(["doppele bk05", (x, 0)])
        
        if persistent.bk6 == 1:
            imgs.append(["doppele bk06", (x, 0)])
        
        if persistent.pose == 1 and persistent.bk7 == 1:
            imgs.append(["doppele bk07", (x, 0)])
        elif persistent.pose == 5 and persistent.bk7 == 1:
            imgs.append(["doppele bk07", (x, 0)])

        return imgs


    def hainu_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["hainu st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["hainu st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["hainu st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["hainu st11", (x-320, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["hainu st12", (x-320, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["hainu st13", (x-320, 0)])

        return imgs


    def amphis_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["amphis st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["amphis st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["amphis st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["amphis st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["amphis st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["amphis st13", (x, 0)])

        return imgs


    def tukuyomi_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["tukuyomi st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["tukuyomi st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["tukuyomi st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["tukuyomi st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["tukuyomi st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["tukuyomi st13", (x, 0)])

        return imgs


    def arcen_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["arcen st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["arcen st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["arcen st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["arcen st11", (x-320, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["arcen st12", (x-320, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["arcen st13", (x-320, 0)])

        return imgs


    def rapun_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["rapun st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["rapun st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["rapun st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["rapun st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["rapun st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["rapun st13", (x, 0)])

        return imgs


    def heavensgate_pose(x):
        imgs = []

        if persistent.face > 3:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["heavensgate st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["heavensgate st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["heavensgate st03", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["heavensgate bk01", (x, 0)])
        elif persistent.bk2 == 1:
            imgs.append(["heavensgate bk02", (x, 0)])
        elif persistent.bk3 == 1:
            imgs.append(["heavensgate bk03", (x, 0)])
        elif persistent.bk4 == 1:
            imgs.append(["heavensgate bk04", (x, 0)])

        return imgs


    def eden_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1:
            imgs.append(["eden st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["eden st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["eden st22", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["eden st23", (x, 0)])

        return imgs


    def stein2_pose(x):
        imgs = []

        imgs.append(["stein2 st01", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["stein2 bk01", (x, 0)])
        elif persistent.bk2 == 1:
            imgs.append(["stein2 bk02", (x, 0)])

        return imgs


    def alice8th2_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 2:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice8th2 st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice8th2 st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice8th2 st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice8th2 st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice8th2 st12", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["alice8th2 bk01", (x, 0)])
        elif persistent.bk2 == 1:
            imgs.append(["alice8th2 bk02", (x, 0)])
        elif persistent.pose == 1 and persistent.bk3 == 1:
            imgs.append(["alice8th2 bk03", (x, 0)])
        elif persistent.pose == 2 and persistent.bk3 == 1:
            imgs.append(["alice8th2 bk13", (x, 0)])
        elif persistent.bk4 == 1:
            imgs.append(["alice8th2 bk04", (x, 0)])

        return imgs


    def alice8th3_pose(x):
        imgs = []

        if persistent.face > 4:
            persistent.face = 1

        if persistent.face == 1:
            imgs.append(["alice8th3 st01", (x, 0)])
        elif persistent.face == 2:
            imgs.append(["alice8th3 st02", (x, 0)])
        elif persistent.face == 3:
            imgs.append(["alice8th3 st03", (x, 0)])
        elif persistent.face == 4:
            imgs.append(["alice8th3 st04", (x, 0)])
        
        if persistent.bk1 == 1:
            imgs.append(["alice8th3 bk01", (x, 0)])
        elif persistent.bk2 == 1:
            imgs.append(["alice8th3 bk02", (x, 0)])
        elif persistent.bk3 == 1:
            imgs.append(["alice8th3 bk03", (x, 0)])
        elif persistent.bk4 == 1:
            imgs.append(["alice8th3 bk04", (x, 0)])

        return imgs


    def alice8th4_pose(x):
        imgs = []
        imgs.append(["alice8th4 st01", (x, 0)])

        return imgs


    def ilias3_pose(x):
        imgs = []

        if persistent.pose > 3:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1

        if persistent.pose > 1 and persistent.face > 2:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["ilias st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["ilias st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["ilias st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["ilias st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["ilias st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["ilias st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["ilias st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["ilias st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["ilias st14", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["ilias st16", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1 == 1:
                imgs.append(["ilias bk01", (x, 0)])
            elif persistent.bk2 == 1:
                imgs.append(["ilias bk02", (x, 0)])
            elif persistent.bk3 == 1:
                imgs.append(["ilias bk03", (x, 0)])
            elif persistent.bk4 == 1:
                imgs.append(["ilias bk04", (x, 0)])
            elif persistent.bk5 == 1:
                imgs.append(["ilias bk05", (x, 0)])
            elif persistent.bk6 == 1:
                imgs.append(["ilias bk06", (x, 0)])
            elif persistent.bk7 == 1:
                imgs.append(["ilias bk07", (x, 0)])

        return imgs


    def ilias4_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.pose == 1:
            imgs.append(["ilias2 st01", (x, 0)])
        elif persistent.pose == 2:
            imgs.append(["ilias2 st11", (x, 0)])

        if persistent.bk1 == 1:
            imgs.append(["ilias2 bk01", (x, 0)])

        if persistent.bk2 == 1:
            imgs.append(["ilias2 bk02", (x, 0)])

        return imgs


    def end_pose(x):
        imgs = []

        return imgs


    def granberia5_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 and persistent.face > 6:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 3:
            persistent.face = 1
        
        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])

        return imgs


    def alma_elma4_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1

        if persistent.face > 3:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alma_elma st11", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alma_elma st12", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alma_elma st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alma_elma st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alma_elma st22", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alma_elma st23", (x, 0)])

        return imgs


    def tamamo3_pose(x):
        imgs = []

        if persistent.pose > 2:
            persistent.pose = 1
        
        if persistent.pose == 1 > 3:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face > 5:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["tamamo st51", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["tamamo st52", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["tamamo st53", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["tamamo st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["tamamo st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["tamamo st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["tamamo st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["tamamo st05", (x+100, 20)])

        return imgs


    def erubetie3_pose(x):
        imgs = []

        imgs.append(["erubetie st01", (x, 0)])

        return imgs


    def alice5_pose(x):
        imgs = []

        if persistent.pose > 4:
            persistent.pose = 1

        if persistent.face > 10:
            persistent.face = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alice st01", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alice st01b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alice st11", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["alice st11b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alice st02", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alice st02b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alice st12", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["alice st12b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alice st03", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alice st03b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alice st13", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["alice st13b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["alice st04", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["alice st04b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["alice st14", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["alice st14b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["alice st05", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 5:
            imgs.append(["alice st05b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["alice st15", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["alice st15b", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["alice st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 6:
            imgs.append(["alice st06b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["alice st16", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["alice st16bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 7:
            imgs.append(["alice st07", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 7:
            imgs.append(["alice st07b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["alice st17", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["alice st17bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 8:
            imgs.append(["alice st08", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 8:
            imgs.append(["alice st08b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["alice st18", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 8:
            imgs.append(["alice st18bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 9:
            imgs.append(["alice st09", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 9:
            imgs.append(["alice st09b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["alice st19", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 9:
            imgs.append(["alice st19bre", (x-80, 0)])
        elif persistent.pose == 1 and persistent.face == 10:
            imgs.append(["alice st51", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 10:
            imgs.append(["alice st51b", (x-80, 0)])
        elif persistent.pose == 3 and persistent.face == 10:
            imgs.append(["alice st61", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 10:
            imgs.append(["alice st61b", (x-80, 0)])

        return imgs


