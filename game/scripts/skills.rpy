label common_skill:
    call cmd("luka_skill1")

label cmd_result:
    if result == 1:
        jump skill1                                         # Демоническое обезглавливание
    elif result == 2 and skill01 > 1 and skill01 < 6:
        jump skill2                                         # Беспорядочные взмахи
    elif result == 2 and skill01 > 5:
        jump skill6                                         # Гибельный Клинок Звезды Хаоса
    elif result == 3:
        jump skill3                                         # Грозовой выпад
    elif result == 4:
        jump skill4                                         # Медитация
    elif result == 5:
        jump skill5                                         # Небесный Крушитель Черепов
    elif result == 6:
        jump skill11                                        # Молниеносный Мгновенный Клинок
    elif result == 7:
        jump skill12                                        # Сокрушающее Землю Обезглавливание
    elif result == 8:
        jump skill13                                        # Безмятежный Демонический Клинок
    elif result == 9:
        jump skill15                                        # Четырёхкратный Гига-удар
    elif result == 10:
        jump skill16                                        # Неудержимый Испепеляющий Клинок
    elif result == 11 and skill02 < 2:
        jump skill7                                         # Сильфа 1 лвла
    elif result == 11 and skill02 == 2:
        jump skill7b                                        # Сильфа 2 лвла
    elif result == 11 and skill02 == 3:
        jump skill7c                                        # Сильфа 3 лвла
    elif result == 11 and skill02 == 4:
        jump skill7d                                        # Сильфа 2 лвла (Танец Падшего Ангела)
    elif result == 11 and skill02 == 5:
        jump skill7e                                        # Сильфа 3 лвла (Танец Падшего Ангела: Буря)
    elif result == 12 and skill03 < 2:
        jump skill8                                         # Гнома 1 лвла
    elif result == 12 and skill03 == 2:
        jump skill8b                                        # Гнома 2 лвла
    elif result == 12 and skill03 == 3:
        jump skill8c                                        # Гнома 3 лвла
    elif result == 12 and skill03 == 4 and ng > 0:
        jump skill8_ng4                                     # Гнома 4 лвла NG+
    elif result == 13 and skill04 < 2:
        jump skill10                                        # Дерьмовая Ундина
    elif result == 13 and (skill04 == 2 or (skill04 > 0 and ng > 0)):
        jump skill10b                                       # Хорошая Ундина
    elif result == 13 and skill04 > 0:
        jump skill10c                                       # Фиговая Ундина
    elif result == 13 and skill04 == 4:
        jump skill18                                        # Дерьмовенькая Ундина
    elif result == 14 and skill05 < 2:
        jump skill14                                        # Саламандра лвл 1
    elif result == 14 and skill05 == 2:
        jump skill14b                                       # Саламандра лвл 2
    elif result == 14 and skill05 == 3:
        jump skill14c                                       # Саламандра лвл 3
    elif result == 15:
        jump skill17                                        # Крайность
    elif result == 16:
        jump skill18                                        # Безмятежный разум
    elif result == 17:
        jump skill19                                        # Мгновенное убийство
    elif result == 18:
        jump skill20                                        # Девятиликий Ракшаса (какого хуя это делает перед HDR?)
    elif result == 19:
        jump skill21                                        # Возрождение Небесного Демона
    elif result == 24:
        jump skill22                                        # Денница
    elif result == 25 and skill02 < 1:
        jump skill23                                        # Танец Падшего Ангела
    elif result == 25 and skill02 > 0:
        jump skill7e                                        # Танец Падшего Ангела с Сильфой
    elif result == 22:
        jump skill24                                        # Элементальная Спика
    elif result == 23:
        jump skill25                                        # Вызов Четырёх Духов
    elif result == 26:
        jump skill26                                        # Разгон

    elif result == 31:
        jump syskill1                                       # Белые ветра
    elif result == 32:
        jump syskill2                                       # Чи-па-па!
    elif result == 33:
        jump syskill3                                       # Отмена земли
    elif result == 34:
        jump syskill4                                       # Зарезервированно
    elif result == 35:
        jump syskill5                                       # Зарезервированно

    elif result == 36:
        jump gnskill1                                       # Захват
    elif result == 37:
        jump gnskill2                                       # Сила земли
    elif result == 38:
        jump gnskill3                                       # Отмена ветра
    elif result == 39:
        jump gnskill4                                       # Зарезервированно
    elif result == 40:
        jump gnskill5                                       # Зарезервированно

    elif result == 41:
        jump unskill1                                       # Очищение
    elif result == 42:
        jump unskill2                                       # Аква-пентаграмма
    elif result == 43:
        jump unskill3                                       # Фаер-резист
    elif result == 44:
        jump unskill4                                       # Зарезервированно
    elif result == 45:
        jump unskill5                                       # Зарезервированно

    elif result == 46:
        jump saskill1                                       # Берсерк
    elif result == 47:
        jump saskill2                                       # Сгори!
    elif result == 48:
        jump saskill3                                       # Отмена воды
    elif result == 49:
        jump saskill4                                       # Зарезервированно
    elif result == 50:
        jump saskill5                                       # Зарезервированно

    elif result == 27:
        call cmd("skill_undine")                            # Дисплей Ундины
        jump cmd_result
    elif result == 28:
        call cmd("skill_sylph")                             # Дисплей Сильфы
        jump cmd_result
    elif result == 29:
        call cmd("skill_gnome")                             # Дисплей Гномы
        jump cmd_result
    elif result == 30:
        call cmd("skill_salamander")                        # Дисплей Саламандры
        jump cmd_result
    elif result == -1:
        $ renpy.jump(lavel + "_main")

label skill1:
    $ before_action = 21
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Нападаю! Демоническое Обезглавливание!"
    elif ransu == 2:
        ori "Ну всё, попалась! Демоническое Обезглавливание!"
    elif ransu == 3:
        ori "Получи! Демоническое Обезглавливание!"

    $ mp -= 2
    call show_skillname("Демоническое обезглавливание")

    "[ori_name!t] прыгает к ней и бьёт мечом по её шее!"

    if skill_sub == 1:
        return

    play sound "audio/se/tuki.ogg"
    show demon decapitation at xy(X=tukix, Y=tukiy) zorder 30
    pause .26
    hide demon
    $ abairitu = 150 + mylv * 3 / 2
    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill2:
    $ before_action = 41
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Получи, и ещё, и ещё!"
    elif ransu == 2:
        ori "Один точно попадёт!"
    elif ransu == 3:
        ori "Аааах!!!"

    $ mp -= 2
    call show_skillname("Flail Wildly")

    "[ori_name!t] начинает крутиться вокруг своей оси, размахивая мечом во все стороны!"

    play sound "audio/se/miss.ogg"

    "Промах!"

    pause 0.5
    play sound "audio/se/miss.ogg"

    "Промах!"

    pause 0.5
    play sound "audio/se/miss.ogg"

    "Промах!"

    pause 0.5
    play sound "audio/se/miss.ogg"

    "Промах!"
    ".... Похоже это не очень эффективно..."

    call hide_skillname

    if skill_sub == 1:
        return

    jump common_attack_end


label skill3:
    $ before_action = 22
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Получай! Громовой Выпад!"
    elif ransu == 2:
        ori "Ха! Громовой Выпад!"
    elif ransu == 3:
        ori "Нападаю! Громовой Выпад!"

    $ mp -= 2
    call show_skillname("Громовой Выпад")

    "[ori_name!t] ступает вперёд, словно гром, и делает резкий взмах!"

    if skill_sub == 1:
        return

    if turn > 1:
        call skill3a
    elif turn == 1:
        call skill3b

    pause 0.26
    hide thunder
    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill3a:
    play sound "audio/se/tuki.ogg"
    $ abairitu = 120 + mylv * 6 / 5
    show thunder thrust a at xy(X=tukix, Y=tukiy) zorder 30
    return


label skill3b:
    "Ранняя атака наносит повышенный урон!"

    play sound "audio/se/slash3.ogg"
    $ abairitu = 200 + mylv * 2
    show thunder thrust b at xy(X=tukix, Y=tukiy) zorder 30
    return


label skill4:
    $ before_action = 42

    ori "............."

    $ mp -= 3
    call show_skillname("Медитация")

    "[ori_name!t] спокойно медитирует.{w}{nw}"

    play sound "audio/se/kaihuku.ogg"
    $ damage = max_mylife / 2

    extend "\n[ori_name!t] восстанавливает половину своего здоровья!"

    if mylife + damage > max_mylife:
        $ mylife = max_mylife
    else:
        $ mylife += damage

    if guard_on == 1 and skill01 > 26 and fire < 1:
        $ bougyo = 2

    call hide_skillname

    if max_mylife < mylife*5:
        call face(param=1)

    $ renpy.jump(lavel + "_a")


label skill5:
    $ before_action = 23
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Уооо! Небесный Крушитель Черепов!"
    elif ransu == 2:
        ori "Как насчёт этого!? Небесный Крушитель Черепов!"
    elif ransu == 3:
        ori "Попалась! Небесный Крушитель Черепов!"

    $ mp -= 3
    call show_skillname("Небесный Крушитель Черепов")

    if tikei == 0:
        "[ori_name!t] залезает на возвышенность и прыгает оттуда!"
    elif tikei == 1:
        "[ori_name!t] залезает на мачту и прыгает оттуда!"
    elif tikei == 2:
        "[ori_name!t] залезает на дерево и прыгает оттуда!"
    elif tikei == 3:
        "[ori_name!t] залезает на стену и прыгает оттуда!"
    elif tikei == 4:
        "[ori_name!t] залезает на колонну и прыгает оттуда!"
    elif tikei == 5:
        "[ori_name!t] взбирается на здание и прыгает сверху вниз!"

    window hide
    hide screen hp
    play sound "audio/se/karaburi.ogg"
    show effect 003 zorder 30 with dissolve
    pause 0.3
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    show screen hp
    window show
    play sound "audio/se/bom2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=25)
    window auto

    "[ori_name!t] наносит сокрушительный удар по её голове!"

    if skill_sub == 1:
        return

    play sound "audio/se/damage2.ogg"
    $ abairitu = 300 + mylv * 3
    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill6a:
    window hide
    hide screen hp
    show death sword chaos star zorder 30
    $ renpy.pause(1.5, hard=True)
    hide death
    show screen hp
    window show
    window auto

    return

label skill6:
    $ before_action = 24
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Гибельный Клинок Звезды Хаоса!"
    elif ransu == 2:
        ori "Съешь это! Гибельный Клинок Звезды Хаоса!"
    elif ransu == 3:
        ori "Нападаю! Гибельный Клинок Звезды Хаоса!"

    $ mp -= 4
    call show_skillname("Гибельный Клинок Звезды Хаоса")

    "Меч Луки сверкает и блестит, как хаотичная падающая звезда!"

    call skill6a

    if skill_sub == 1:
        return

    while j < 5:
        play sound "audio/se/slash.ogg"

        if fire < 2:
            $ abairitu = rnd2(50+mylv*1/2, 150+mylv*3/2)
        elif fire == 2:
            $ abairitu = rnd2(100+mylv, 225+mylv *9/4)

        call enemylife(count+1)
        $ count += 1
        if count > 2:
            $ count = 0
        $ j += 1

    $ j = 0
    $ count = 0
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill7:
    $ before_action = 43
    $ persistent.count_wind += 1

    ori "Сильфа, одолжи мне свою силу!"

    $ mp -= 2
    call show_skillname("Ветряная защита")
    $ wind = 1
    $ wind_turn = 8

    if skill01 < 9:
        $ earth = 0
        $ earth_turn = 0

    $ aqua = 0
    $ aqua_turn = 0
    play sound "audio/se/wind2.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan sylph st12
    elif ransu == 100:
        syoukan sylph st07
        $ persistent.g_sylph = 1

    "Могучий ветер обвивает Луку!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill7b:
    $ before_action = 43
    $ persistent.count_wind += 1

    ori "Отдаться ветру...!"

    $ mp -= 2
    call show_skillname("Игривый ветер")
    $ wind = 2
    $ wind_turn = 12

    if skill01 < 9 or skill02 == 4:
        $ earth = 0
        $ earth_turn = 0

    $ aqua = 0
    $ aqua_turn = 0
    play sound "audio/se/wind2.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan sylph st12
    elif ransu == 100:
        syoukan sylph st07
        $ persistent.g_sylph = 1

    "Мощные ветра кружат вокруг тела Луки, резко увеличивая его скорость!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill7c:
    $ before_action = 43
    $ persistent.count_wind += 1

    ori "Сильфа, бушуй и свирепствуй, как тебе захочется!"

    $ mp -= 1
    call show_skillname("Опустошающий Ураган")
    $ wind = 3
    $ wind_turn = 16
    play sound "audio/se/wind3.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan sylph st12
    elif ransu == 100:
        syoukan sylph st07
        $ persistent.g_sylph = 1

    "Движения Луки становятся так же быстры и мощны, как опустошительный ураган!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill8:
    $ before_action = 44
    $ persistent.count_earth += 1

    ori "Гнома, одолжи мне свою силу!"

    $ mp -= 2
    call show_skillname("Сила Земли") # Попов, перелогинься
    $ earth = 1
    $ earth_turn = 8

    if skill01 < 9:
        $ wind = 0
        $ wind_turn = 0

    $ aqua = 0
    $ aqua_turn = 0
    play sound "audio/se/earth4.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan gnome st21
    elif ransu == 100:
        syoukan gnome st12
        $ persistent.z_gnome = 1

    "Сила Земли растекается по телу Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill8b:
    $ before_action = 44
    $ persistent.count_earth += 1

    ori "Наполнить дыханием земли само моё тело...!"

    $ mp -= 2
    call show_skillname("Дыхание Земли")
    $ earth = 2
    $ earth_turn = 12

    if skill01 < 9 or skill03 == 4:
        $ wind = 0
        $ wind_turn = 0

    $ aqua = 0
    $ aqua_turn = 0
    play sound "audio/se/earth1.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan gnome st21
    elif ransu == 100:
        syoukan gnome st12
        $ persistent.z_gnome = 1

    "Дыхание земли укрепляется в теле Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill8c:
    $ before_action = 44
    $ persistent.count_earth += 1

    ori "Гнома, наполни меня непоколебимой силой земли!"

    $ mp -= 1
    call show_skillname("Дикие Земли")
    $ earth = 3
    $ earth_turn = 16
    play sound "audio/se/earth2.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan gnome st21
    elif ransu == 100:
        syoukan gnome st12
        $ persistent.z_gnome = 1

    "Непоколебимая и несокрушимая сила Земли укрепляется в теле Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill10:
    $ before_action = 45
    $ persistent.count_aqua += 1

    ori "Ундина, одолжи мне свою силу!"

    $ mp -= 2
    call show_skillname("Водный барьер")
    $ aqua = 1
    $ aqua_turn = 8
    $ wind = 0
    $ wind_turn = 0
    $ earth = 0
    $ earth_turn = 0
    $ fire = 0
    $ fire_turn = 0
    play sound "audio/se/aqua2.ogg"
    syoukan undine st11

    "Тонкая стенка воды окружает Луку!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill10b:
    $ before_action = 45
    $ persistent.count_aqua += 1

    ori "Принять течение в сердце...!"

    call show_skillname("Безмятежный разум")
    $ aqua = 2
    $ wind = 0
    $ wind_turn = 0
    $ earth = 0
    $ earth_turn = 0
    $ fire = 0
    $ fire_turn = 0
    play sound "audio/se/aqua.ogg"
    syoukan undine st11

    stop music fadeout 1
    show bg black with Dissolve(1.5)
    play music "audio/bgm/aqua.ogg"

    "[ori_name!t] доверяет своё сердце течению воды!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill10c:
    $ before_action = 45
    $ persistent.count_aqua += 1

    ori "Успокоить своё сердце и разум..."

    call show_skillname("Безмятежный разум")

    if skill01 < 17:
        $ mp -= 4
    elif skill01 > 17:
        $ mp -= 2

    $ aqua = 3
    $ aqua_turn = 16
    play sound "audio/se/aqua.ogg"
    syoukan undine st11

    "[ori_name!t] очищает свой разум и объединяется с мировым течением!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill11:
    $ before_action = 25
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "С ветром, направляющим мой меч... Молниеносный Удар Клинка!"
    elif ransu == 2:
        if skill02 < 1:
            jump skill11
        ori "С силой Сильфы...! Молниеносный Удар Клинка!"
    elif ransu == 3:
        ori "Нападаю! Молниеносный Удар Клинка!"

    $ mp -= 2
    call show_skillname("Молниеносный Удар Клинка{#skill11_skillname}")
    play sound "audio/se/wind2.ogg"

    if wind != 0:
        show cutin_ruka_wind zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause 0.7
        show cutin_wind zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause 0.7
        hide cutin_ruka_wind
        pause .1
        hide cutin_wind
        pause .1
        with dissolve

    "Двигаясь, словно мощный порыв ветра, [ori_name!t] делает быстрый, словно молния, выпад вперёд!"

    if turn > 1 and wind == 0:
        call skill11a
    elif turn == 1:
        call skill11b
    elif wind > 0:
        call skill11c

    pause 0.1
    hide thunder

    if skill_sub == 1:
        return

    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill11a:
    $ abairitu = 220 + mylv * 11 / 5

    if fire > 1:
        $ abairitu = abairitu * 3 / 2

    play sound "audio/se/tuki.ogg"
    show effect wind zorder 30 #1
    pause 0.05
    show thunder thrust a at xy(tukix, tukiy) zorder 30
    pause 0.25
    hide effect
    hide thunder
    return


label skill11b:
    "Ранняя атака наносит повышенный урон!"

    $ abairitu = 350 + mylv * 7 / 2
    play sound "audio/se/slash3.ogg"
    show effect wind zorder 30 #1
    pause 0.05
    show thunder thrust b at xy(tukix, tukiy) zorder 30
    pause 0.25
    hide effect
    hide thunder
    return


label skill11c:
    $ abairitu = 350 + mylv * 7 / 2

    if fire > 1:
        $ abairitu = abairitu * 3 / 2

    play sound "audio/se/slash3.ogg"
    show effect wind zorder 30 #1
    pause 0.05
    show thunder thrust b at xy(tukix, tukiy) zorder 30
    pause 0.25
    hide effect
    hide thunder
    return

label skill12a:

    play sound "audio/se/karaburi.ogg"
    show effect earth zorder 30
    $ renpy.pause(0.3, hard=True)
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    play sound "audio/se/bom2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=30)

    return

label skill12:
    $ before_action = 26
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "О Земля, наполни же мой меч своей силой! Сотрясающее Землю Обезглавливание!"
    elif ransu == 2:
        if skill03 < 1:
            jump skill12
        ori "Гнома, придай мне своей силы! Сотрясающее Землю Обезглавливание!"
    elif ransu == 3:
        ori "Ощути всю тяжесть самой Земли! Сотрясающее Землю Обезглавливание!"

    $ mp -= 3
    if fire < 1 and earth < 1:
        call show_skillname("Сотрясающее Землю Обезглавливание")
        play sound "audio/se/earth1.ogg"

        if earth != 0:
            show cutin_ruka_earth zorder 30:
                    pos (500, -200)
                    alpha 0
                    linear .7 ypos -50 alpha 1.0
            pause .7
            show cutin_earth zorder 30:
                    pos (100, -200)
                    alpha 0
                    linear .7 ypos -50 alpha 1.0
            pause .7
            hide cutin_ruka_earth
            pause .1
            hide cutin_earth
            pause .1
    elif fire > 0 and earth > 0:
        call show_skillname("Сотрясающее Землю Обезглавливание: Магма")
        play sound "audio/se/earth1.ogg"

        show cutin_ruka_earth zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_earth zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_earth
        pause .1
        hide cutin_earth
        pause .1
        show cutin_ruka_fire zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_fire zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_fire
        pause .1
        hide cutin_fire
        pause .1

    elif wind == 5 and aqua == 2:
        call show_skillname("Сотрясающее Землю Обезглавливание: Грозовое обрушение")
        play sound "audio/se/earth1.ogg"

        show cutin_ruka_wind zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_wind zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_wind
        pause .1
        hide cutin_wind
        pause .1
        show cutin_ruka_aqua zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_aqua zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_aqua
        pause .1
        hide cutin_aqua
        pause .1

    call skill12a

    "Сила Земли разливается и наполняет всё тело Луки когда он прыгает в воздух!"
    "[ori_name!t] обрушивается вниз, сотрясая саму Землю при приземлении!"

    if skill_sub == 1:
        return

    if aqua < 1 and enemy_wind == 1:
        if enemy_wind > 0 and enemy_holy < 1 and wind < 1:
            call enemy_windguard
            jump common_attack_end

    play sound "audio/se/damage2.ogg"
    $ abairitu = 450 + mylv * 9 / 2

    if earth > 0:
        $ abairitu = abairitu*4/3

    if fire > 1 and earth > 0:
        $ abairitu = abairitu*125/100

    if fire > 1 and earth < 1:
        $ abairitu = abairitu*125/100

    call enemylife
    if wind == 5:
        "Ведомый порывом ветра, остаточный образ Луки обрушивается на противника!"
        if earth > 0:
            $ abairitu = abairitu*4/3

        if fire > 1 and earth > 0:
            $ abairitu = abairitu*125/100

        if fire > 1 and earth < 1:
            $ abairitu = abairitu*125/100

        call enemylife

    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end

label skill13a:

    play sound "audio/se/karaburi.ogg"
    show effect 006a zorder 30
    $ renpy.pause(0.3, hard=True)
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)

    return

label skill13:
    $ before_action = 27
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Вместе с потоком... Безмятежный Демонический Клинок!"
    elif ransu == 2:
        if skill04 < 1:
            jump skill13
        ori "Ундина, одолжи мне свою силу... Безмятежный Демонический Клинок!"
    elif ransu == 3:
        ori "Я рассеку всё на своём пути... Безмятежный Демонический Клинок!"

    $ aqua_end = 1
    $ mp -= 2
    call show_skillname("Безмятежный Демонический Клинок{#skill13_skillname}")
    play sound "audio/se/aqua2.ogg"

    if aqua > 0 and aqua != 4:
        show cutin_ruka_aqua zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_aqua zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_aqua
        pause .1
        hide cutin_aqua
        pause .1

    call skill13a

    "[ori_name!t] вкладывает меч в ножны, и безмятежно держит рукой его рукоять!"
    "Вынимая его в молниеносном, гладком, как течение воды, взмахе, он прорубается сквозь всё на своём пути!"

    if skill_sub == 1:
        return

    play sound "audio/se/slash4.ogg"
    $ abairitu = 300+mylv*3

    if aqua == 2:
        $ abairitu = abairitu*2

    if aqua > 0 and aqua != 2:
        $ abairitu = abairitu*3/2

    if fire > 1:
        $ abairitu = abairitu*3/2

    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill14:
    $ before_action = 46
    $ persistent.count_fire += 1
    $ ransu = rnd2(1,3)

    ori "Саламандра, одолжи мне свою силу...!"

    $ mp -= 2
    call show_skillname("Клинок Пламени")
    $ fire = 1
    $ fire_turn = 5
    play sound "audio/se/fire1.ogg"
    syoukan salamander st02

    "Языки огня пробегают вверх и вниз по мечу Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill14b:
    $ before_action = 46
    $ persistent.count_fire += 1

    ori "Наполни мой меч пламенем самого ада!"

    $ mp = max_mp
    call show_skillname("Адский Испепелящий Клинок")
    $ wind = 0
    $ wind_turn = 0
    $ earth = 0
    $ earth_turn = 0
    $ aqua = 0
    $ aqua_turn = 0
    $ fire = 2
    $ fire_turn = 5
    play sound "audio/se/fire2.ogg"
    syoukan salamander st02

    "Языки огня пробегают вверх и вниз по мечу Луки, готовые воспламениться с его умениями меча!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill14c:
    $ before_action = 46
    $ persistent.count_fire += 1

    ori "Саламандра, очисти всё в искупляющем пламени!"

    $ mp = max_mp
    call show_skillname("Очищающее Пламя")
    $ fire = 3
    $ fire_turn = 5
    play sound "audio/se/fire4.ogg"
    syoukan salamander st02

    "Языки огня пробегают вверх и вниз по мечу Луки готовые воспламениться с его умениями меча!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill15:
    $ before_action = 47
    $ kadora = 1

    if aqua == 2:
        $ renpy.show(haikei)
        with Dissolve(1.5)
        call musicplay

    $ wind = 0
    $ wind_turn = 0
    $ earth = 0
    $ earth_turn = 0
    $ aqua = 0
    $ aqua_turn = 0
    $ fire = 0
    $ fire_turn = 0
    $ mp -= 1

    ori "Приди, Сильфа"

    call show_skillname("Сильфа")
    play sound "audio/se/wind2.ogg"
    syoukan sylph st12

    "Сила ветра вливается в меч Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill15a:
    $ before_action = 47
    $ kadora = 2

    ori "Приди, Гнома!"

    call show_skillname("Гнома")
    play sound "audio/se/earth1.ogg"
    syoukan gnome st21

    "Сила земли вливается в меч Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill15b:
    $ before_action = 47
    $ kadora = 3

    ori "Приди, Ундина!"

    call show_skillname("Ундина")
    play sound "audio/se/aqua2.ogg"
    syoukan undine st11

    "Сила воды вливается в меч Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill15c:
    $ before_action = 47
    $ kadora = 4
    ori "Приди, Саламандра!"

    call show_skillname("Salamander")
    play sound "audio/se/fire2.ogg"
    syoukan salamander st02

    "Сила огня вливается в меч Луки!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")

label skill15e:

    play sound "audio/se/wind2.ogg"
    show cutin_wind zorder 30:
        pos (0, -200)
        alpha 0.0
        linear 0.5 ypos -50 alpha 1.0
    pause .5
    play sound "audio/se/earth1.ogg"
    show cutin_earth zorder 30:
        pos (200, -200)
        alpha 0.0
        linear 0.5 ypos -50 alpha 1.0
    pause .5
    play sound "audio/se/aqua2.ogg"
    show cutin_aqua zorder 30:
        pos (400, -200)
        alpha 0.0
        linear 0.5 ypos -50 alpha 1.0
    pause .5
    play sound "audio/se/fire2.ogg"
    show cutin_fire zorder 30:
        pos (600, -200)
        alpha 0.0
        linear 0.5 ypos -50 alpha 1.0
    pause .5
    hide cutin_wind
    hide cutin_fire
    hide cutin_earth
    hide cutin_aqua
    with Dissolve(1.0)

    return

label skill15d:
    $ before_action = 28
    $ kadora = 0
    $ persistent.count_kadora = 1

    ori "Тебе пришёл конец! Четырёхкратный Гига-удар!"

    call show_skillname("Четырёхкратный Гига-удар{#skill15d_skillname}")

    call skill15e

    "Силы четырёх духов перекрывают и усиливают друг друга, взрываясь в силовом потоке!"

    pause 0.5
    play sound "audio/se/karaburi.ogg"
    show effect 003 zorder 30 #2
    pause 0.3
    play sound "audio/se/earth1.ogg"
    show effect earth zorder 30
    pause 0.1 #2
    play sound "audio/se/fire2.ogg"
    show effect fire zorder 30
    pause 0.1 #2
    play sound "audio/se/aqua2.ogg"
    show effect aqua zorder 30
    pause 0.1 #2
    play sound "audio/se/wind2.ogg"
    show effect wind zorder 30
    pause 0.1 #2
    hide effect with ImageDissolve("Images/System/mask01.webp", 0.1)
    play sound "audio/se/bom3.ogg"
    with Quake((0, 0, 0, 0), 2.0, dist=40)

    "Всесокрушающая сила высвобождается из меча Луки!"

    if skill_sub == 1:
        return

    play sound "audio/se/damage2.ogg"
    $ damage = rnd2(40000,50000)
    call enemylife2
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill16:
    $ before_action = 29
    $ ransu = rnd2(1,2)

    if ransu == 1:
        ori "Я очищу тебя в искупляющем пламени... Неудержимый Испепеляющий Клинок!"
    elif ransu == 2:
        if skill05 < 1:
            ori "Сгори в этом адском пламени! Неудержимый Испепеляющий Клинок!"
        else:
            ori "Одолжи мне свою силу, Саламандра! Неудержимый Испепеляющий Клинок!"

    $ mp -= 4
    if ng > 0 and wind > 0 and wind != 4 and fire > 0:
        call show_skillname("Неудержимый Испепеляющий Клинок: Тайфун{#skill16_skillname}")

        play sound "audio/se/fire2.ogg"

        show cutin_ruka_fire zorder 30:
            pos (500, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_fire zorder 30:
            pos (100, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_fire
        pause .1
        hide cutin_fire
        pause .1

        play sound "audio/se/wind2.ogg"

        show cutin_ruka_wind zorder 30:
            pos (500, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_wind zorder 30:
            pos (100, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_wind
        pause .1
        hide cutin_wind
        pause .1

        call skill16c

    else:
        call show_skillname("Неудержимый Испепеляющий Клинок{#skill16_skillname}")

        play sound "audio/se/fire2.ogg"

        if fire != 0:
            show cutin_ruka_fire zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
            pause .7
            show cutin_fire zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
            pause .7
            hide cutin_ruka_fire
            pause .1
            hide cutin_fire
            pause .1

        call skill16a

    "Воспламеняясь очищающим огнём, [ori_name!t] атакует в вихре пламени!"

    if skill_sub == 1:
        return

    if enemy_wind > 0 and enemy_holy < 1 and wind < 2:
        call enemy_windguard
        jump common_attack_end

    if ng > 0 and wind > 0 and fire > 0 and wind != 4:
        while j < 8:
            play sound "audio/se/slash.ogg"

            if fire < 2:
                $ abairitu = rnd2(150+mylv*3/2, 200+mylv*2)
            elif fire > 1:
                $ abairitu = rnd2(200+mylv*2, 300+mylv*3)

            $ abairitu = abairitu * 12 / 10

            call enemylife(count+1)
            $ count += 1
            if count > 2:
                $ count = 0
            $ j += 1

    else:
        while j < 5:
            play sound "audio/se/slash.ogg"

            if fire < 2:
                $ abairitu = rnd2(150+mylv*3/2, 200+mylv*2)
            elif fire > 1:
                $ abairitu = rnd2(200+mylv*2, 300+mylv*3)

            if ng > 0 and wind > 0 and wind != 4:
                $ abairitu = abairitu * 12 / 10

            call enemylife(count+1)
            $ count += 1
            if count > 2:
                $ count = 0
            $ j += 1

    $ j = 0
    $ count = 0
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill16a:
    show vaporizing rebellion sword zorder 30
    $ renpy.pause(3.0, hard=True)
    hide vaporizing
    return

label skill16c:
    show effect wind zorder 30
    show vaporizing rebellion sword typhoon zorder 30
    $ renpy.pause(3.0, hard=True)
    hide vaporizing
    hide effect wind
    return

label skill17:
    $ before_action = 55
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori "Загнать себя в угол...!"
    elif ransu == 2:
        ori "Таков мой способ ведения боя...!"
    elif ransu == 3:
        ori "Даже находясь на грани, я не дрогну!"

    call show_skillname("Крайность")

    "[ori_name!t] спокойно сосредотачивается...{w}{nw}"

    play sound "audio/se/down2.ogg"
    $ mylife = 1

    extend "\nЖизнеспособность Луки резко падает!"

    call hide_skillname
    call face(param=2)
    $ renpy.jump(lavel + "_main")


label skill18:
    $ before_action = 45

    ori "Успокоить своё сердце и разум..."

    call show_skillname("Безмятежный разум")
    $ mp -= 2
    $ aqua = 4
    play sound "audio/se/aqua.ogg"

    if skill04 != 0:
        $ persistent.count_aqua += 1
        syoukan undine st11
        $ aqua_turn = 16
    else:
        $ aqua_turn = 12

    if skill02 < 1:
        $ wind = 0
        $ wind_turn = 0

    "[ori_name!t] очищает свой разум и объединяется с мировым течением!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")

label skill19a:
    if wind > 0 and skill02 > 0:
        play sound "audio/se/wind2.ogg"
        show cutin_ruka_wind zorder 30:
            pos (500, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_wind zorder 30:
            pos (100, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_wind
        hide cutin_ruka_wind

    play sound "audio/se/tuki.ogg"
    show effect 007 zorder 30
    $ renpy.pause(0.1, hard=True) #2
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)

    return

label skill19:
    $ before_action = 30
    $ ransu = rnd2(1,2)

    if ransu == 1:
        ori "Пришёл за тобой судьбоносный клинок правосудия!"
    elif ransu == 2:
        ori "Насквозь прорезая пространство, время и здравый смысл!"

    $ mp -= 2

    if wind > 0 and wind != 4:
        call show_skillname("Мгновенное Убийство: Опустошающее Торнадо")
    else:
        call show_skillname("Мгновенное Убийство")

    call skill19a

    "Клинок Луки прорывается сквозь само время и пространство!"

    if skill_sub == 1:
        return

    play sound "audio/se/slash4.ogg"

    if wind != 3 and fire == 0:
        $ abairitu = 250+mylv*5/2
    elif wind == 3 and fire == 0:
        $ abairitu = 400+mylv*7/2
    elif wind != 3 and fire > 0:
        $ abairitu = 300+mylv*11/4
    elif wind == 3 and fire > 0:
        $ abairitu = 450+mylv*9/2

    if wind > 0 and wind != 4 and skill02 > 0:
        $ abairitu = abairitu*11/10

    call enemylife

    if skill_sub > 1:
        return

    if wind > 0 and wind != 4:
            "Бушуя, подобно торнаду, меч Луки возвращается в ножны!"

            play sound "audio/se/slash4.ogg"

            if wind != 3 and fire == 0:
                $ abairitu = 250+mylv*5/2
            elif wind == 3 and fire == 0:
                $ abairitu = 400+mylv*7/2
            elif wind != 3 and fire > 0:
                $ abairitu = 300+mylv*11/4
            elif wind == 3 and fire > 0:
                $ abairitu = 450+mylv*9/2

            if wind > 0 and wind != 4 and skill02 > 0:
                $ abairitu = abairitu*11/10

            call enemylife

    call hide_skillname
    jump common_attack_end

label skill20a:
    window hide
    hide screen hp

    if not fire < 3:
        play sound "audio/se/fire2.ogg"
        show cutin_ruka_fire zorder 30:
            pos (500, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_fire zorder 30:
            pos (100, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_fire
        pause 0.1
        hide cutin_fire
        pause .1

    show rakshasa zorder 30
    $ renpy.pause(1.5, hard=True)
    hide rakshasa
    show screen hp
    window show
    window auto

    return

label skill20:
    $ before_action = 31
    $ ransu = rnd2(1,3)

    ori "Челюсти Ракшасы да приведут грешников к расплате!"

    $ mp -= 6

    if fire == 3:
        call show_skillname("Девятиликий Ракшаса: Асура")
    elif fire < 3:
        call show_skillname("Девятиликий Ракшаса")

    call skill20a

    "[ori_name!t] атакует в смертельном танце!"

    if skill_sub == 1:
        return

    while j < 9:
        play sound "audio/se/slash.ogg"

        if fire == 0:
            $ abairitu = rnd2(100+mylv,130+mylv*13/10)
        elif fire == 2:
            $ abairitu = rnd2(120+mylv*6/5,150+mylv*3/2)
        elif fire == 3:
            $ abairitu = rnd2(140+mylv*7/5,170+mylv*17/10)

        call enemylife(count+1)
        $ count += 1
        if count > 2:
            $ count = 0
        $ j += 1

    $ j = 0
    $ count = 0
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end

label skill21a:
    window hide
    hide screen hp
    if not earth < 3:
        play sound "audio/se/earth1.ogg"
        show cutin_ruka_earth zorder 30:
                pos (500, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_earth zorder 30:
                pos (100, -200)
                alpha 0
                linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_earth
        pause .1
        hide cutin_earth
        pause .1

    $ renpy.pause(0.3, hard=True)
    play sound "audio/se/bom2.ogg"
    show effect 001 zorder 30 #2
    $ renpy.pause(0.3, hard=True)
    hide effect with ImageDissolve("images/System/mask02.webp", 0.5, ramplen=256, hard=True)
    show screen hp
    window show
    window auto

    return

label skill21:
    $ before_action = 32
    $ ransu = rnd2(1,3)
    ori "Вся жизнь возвращается к своей первоначальной форме!"

    $ mp -= 4

    if earth == 3:
        call show_skillname("Возрождение Небесного Демона: Гайя")
    elif earth < 3:
        call show_skillname("Возрождение Небесного Демона")

    call skill21a

    "[ori_name!t] концентрирует вокруг себя сгусток мощной магической энергии!"

    if skill_sub == 1:
        return

    play sound "audio/se/damage2.ogg"

    if earth < 3 and fire == 0:
        $ abairitu = 600 + mylv * 6
    elif earth == 3 and fire == 0:
        $ abairitu = 800 + mylv * 8
    elif earth < 3 and fire > 0:
        $ abairitu = 600 + mylv * 8
    elif earth == 3 and fire > 0:
        $ abairitu = 1000 + mylv * 10

    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end


label skill22:
    $ before_action = 59

    ori "..........."

    $ mp -= 4
    call show_skillname("Концентрация магии")
    play sound "audio/se/power.ogg"

    "[ori_name!t] закрывает глаза, и атмосфера вокруг него наполняется магической энергией ошеломляющей мощи!"

    call hide_skillname

    if skill_sub > 1:
        return

    $ daten = 1
    jump common_attack_end

label skill22a:
    pause 0.3
    window hide
    hide screen hp
    play sound "audio/se/bom3.ogg"
    show effect 002 zorder 30 with ImageDissolve("images/System/mask01.webp", 1.5, hard=True)
    show effect 002 zorder 30:
        align (0.5, 0.5)
        pause 1.0
        linear 1.5 zoom 3.0
    $ renpy.pause(1.6, hard=True)
    show color white
    hide effect with Dissolve(1.5)
    hide color with Dissolve(2.0)

    return

label skill22x:
    if enemy_num > 1:
        $ target_all = 1
    $ is_attack = 1
    $ guard_break = 0
    $ used_daystar += 1
    $ daten = 0
    hide ct
    call face(param=3)
    call counter

    if aqua != 3:
        call show_skillname("Денница")
    elif aqua > 1 and skill04 > 0:
        call show_skillname("Непогрешимая Денница")

    $ ransu = rnd2(1,3)

    ori "Моя мать - Утренняя Звезда, дитя зари.{w}{nw}"
    extend "\nЗвезда, падшая на Землю, да обретёт победу!"

    if mp > 3:
        $ daten_less = 1
        $ mp -= 4
    elif mp < 4:
        $ daten_less = mp / 4.0
        $ mp = 0

    if aqua == 3:
        window hide
        hide screen hp
        play sound "audio/se/aqua2.ogg"
        show cutin_ruka_aqua zorder 30:
            pos (500, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        show cutin_aqua zorder 30:
            pos (100, -200)
            alpha 0
            linear .7 ypos -50 alpha 1.0
        pause .7
        hide cutin_ruka_aqua
        pause .1
        hide cutin_aqua
        pause .1

        window show
        show screen hp
        window auto

    "Ослепительная звезда обрушивается в ад!"

    call skill22a

    if nostar > 0:
        if nostar == 1:
            $ renpy.jump(lavel + "_nostar")
        $ nostar -= 1

    while j < 3:
        play sound "audio/se/damage2.ogg"

        if aqua == 0 and fire == 0:
            $ abairitu = 450 + mylv * 9 / 2
        elif aqua > 0 and fire == 0:
            $ abairitu = 600 + mylv * 6
        elif aqua == 0 and fire > 0:
            $ abairitu = 600 + mylv * 6
        elif aqua > 0 and fire > 0:
            $ abairitu = 700 + mylv * 7
        elif aqua > 0 and skill04 > 0 and fire == 0:
            $ abairitu = 750 + mylv * 6
        elif aqua > 0 and skill04 > 0 and fire > 0:
            $ abairitu = 850 + mylv * 8

        $ abairitu = int(abairitu * daten_less)

        window show
        show screen hp
        window auto
        call enemylife(count+1)
        $ count += 1
        if count > 2:
            $ count = 0
        $ j += 1

    $ j = 0
    $ count = 0
    call hide_skillname

    if enemylife == 0:
        $ renpy.pop_return()
        $ renpy.jump(lavel + "_v")

    call face(param=1)
    $ target_all = 0
    $ result = 44
    return


label skill23:
    $ before_action = 56

    ori "Непогрешимый и неизменный... Танец Падшего Ангела!"

    call show_skillname("Танец Падшего Ангела")
    $ mp -= 2
    $ wind = 4
    $ wind_turn = 12
    if skill04 < 2:
        $ aqua = 0
        $ aqua_turn = 0
    play sound "audio/se/bun.ogg"

    $ renpy.show(haikei, at_list=[lightwave]) #700 # with wave
    $ renpy.show(tatie1, at_list=[lightwave, monster_pos])


    "Лука объединяется со святым течением!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill7d:
    $ before_action = 43
    $ persistent.count_wind += 1

    l "Непогрешимый и неизменный... Танец Падшего Ангела!"

    $ mp -= 2
    call show_skillname("Танец Падшего Ангела")
    $ wind = 4
    $ wind_turn = 12
    play sound "audio/se/wind2.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan sylph st12
    elif ransu == 100:
        syoukan sylph st07
        $ persistent.g_sylph = 1

    $ renpy.show(haikei) #700 # with wave

    "[ori_name!t] объединяется со святым течением!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill7e:
    $ before_action = 43
    $ persistent.count_wind += 1

    ori "Непогрешимый и неизменный... Танец Падшего Ангела!"

    $ mp -= 1
    call show_skillname("Танец Падшего Ангела: Буря")
    $ wind = 3
    $ wind_turn = 16
    play sound "audio/se/wind3.ogg"
    $ ransu = rnd2(1,100)

    if ransu != 100:
        syoukan sylph st12
    elif ransu == 100:
        syoukan sylph st07
        $ persistent.g_sylph = 1

    $ renpy.show(haikei) #700 # with wave

    "[ori_name!t] объединяется со святым течением и ветром!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill24:
    $ before_action = 34

    ori "Вся сила четырёх духов в моей руке!"

    $ mp -= 10
    window hide
    hide screen hp
    call show_skillname("Элементальная Спика")
    play sound "audio/se/wind2.ogg"
    show cutin_wind zorder 30:
        pos (0, -200)
        alpha .0
        linear .3 ypos -50 alpha 1.0
    pause .3
    play sound "audio/se/earth1.ogg"
    show cutin_earth zorder 30:
        pos (200, -200)
        alpha .0
        linear .3 ypos -50 alpha 1.0
    pause .3
    play sound "audio/se/aqua2.ogg"
    show cutin_aqua zorder 30:
        pos (400, -200)
        alpha .0
        linear .3 ypos -50 alpha 1.0
    pause .3
    play sound "audio/se/fire2.ogg"
    show cutin_fire zorder 30:
        pos (600, -200)
        alpha .0
        linear .3 ypos -50 alpha 1.0
    pause .3
    hide cutin_wind
    hide cutin_fire
    hide cutin_aqua
    hide cutin_earth
    with Dissolve(1.0)
    show screen hp
    window auto

    "Сила Четырёх Духов вливается в правую руку Луки, после чего он взмахивает своим мечом с разрушительной силой!"

    pause 0.5
    play sound "audio/se/karaburi.ogg"
    window hide
    hide screen hp
    show spica zorder 30 #2
    pause 1.2
    hide spica
    show screen hp
    window show
    window auto

    if skill_sub == 1:
        return

    if enemy_earth > 0 or enemy_wind > 0 or enemy_aqua > 0 or enemy_fire > 0:
        "Но [monster_name] имеет устойчивость к одному из элементов!"

    play sound "audio/se/damage2.ogg"
    $ abairitu = 3000 + mylv * 20
    if enemy_wind > 0:
        $ abairitu = abairitu / 2
    if enemy_earth > 0:
        $ abairitu = abairitu / 2
    if enemy_aqua > 0:
        $ abairitu = abairitu / 2
    if enemy_fire > 0:
        $ abairitu = abairitu / 2

    call enemylife
    call hide_skillname

    if skill_sub > 1:
        return

    jump common_attack_end

label skill25:
    if skillset == 1:
        $ sentaku= 1
        jump skill25b

    pause .5

    sylph "Лука!{w}\nТебе нужен мой ветер или ты хочешь его скомбинировать со своей святой силой?"

    menu:
        "Опустошающий Ураган":
            $ sentaku = 1
            jump skill25b

        "Танец Падшего Ангела":
            $ sentaku = 2
            jump skill25b


label skill25b:
    $ before_action = 57
    $ persistent.count_wind += 1
    $ persistent.count_earth += 1
    $ persistent.count_aqua += 1
    $ persistent.count_fire += 1
    $ ransu = rnd2(1,3)

    ori "Вперёд, все вместе!"

    $ mp -= 6
    call show_skillname("Вызов Четырёх Духов")
    play sound "audio/se/wind2.ogg"
    window hide
    hide screen hp
    show cutin_wind zorder 30:
        pos (0, -200)
        alpha 0.0
        linear 1.0 ypos -50 alpha 1.0
    pause 1.0
    play sound "audio/se/earth1.ogg"
    show cutin_earth zorder 30:
        pos (200, -200)
        alpha 0.0
        linear 1.0 ypos -50 alpha 1.0
    pause 1.0
    play sound "audio/se/aqua2.ogg"
    show cutin_aqua zorder 30:
        pos (400, -200)
        alpha 0.0
        linear 1.0 ypos -50 alpha 1.0
    pause 1.0
    play sound "audio/se/fire2.ogg"
    show cutin_fire zorder 30:
        pos (600, -200)
        alpha 0.0
        linear 1.0 ypos -50 alpha 1.0
    pause 1.0
    hide cutin_wind
    hide cutin_fire
    hide cutin_aqua
    hide cutin_earth
    with Dissolve(1.0)
    show screen hp
    window auto

    if sentaku == 1:
        $ wind = 3
        $ wind_turn = 16
    elif sentaku == 2:
        $ wind = 4
        $ wind_turn = 12
    $ earth = 3
    $ earth_turn = 16
    $ aqua = 3
    $ aqua_turn = 16
    $ fire = 3
    $ fire_turn = 5

    "Лука призывает всех духов разом!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label common_alice_skill:
    call cmd("alice_skill")

    if result == -1:
        $ renpy.jump(lavel + "_main")
    else:
        jump expression "skill_al%d" % result

    jump common_alice_skill


label skill_al1:
    $ alice_skill = 0
    $ before_action = 35
    call show_skillname("Word of Dispel")
    show alice st74 at xy(-240, 50) zorder 16 with dissolve #690

    a "I ask thee, eternal time..."
    a "Reveal my body, yoked in twilight..."

    play sound "audio/se/flash.ogg"
    show alice st03b at xy(X=-163) with flash

    a "Return my true body to me!"

    call hide_skillname
    hide alice with dissolve
    call show_skillname("Omega Blaze")

    "Alice unleashes a storm of fire!"

    play sound "audio/se/fire1.ogg"
    $ renpy.movie_cutscene("video/movie01.webm")

    if alice_skill1 == 0:
        jump skill_al_miss

    $ damage = rnd2(40*alice_skill1, 45*alice_skill1)
    play sound "audio/se/damage2.ogg"
    call enemylife3(nm="Alice")
    call hide_skillname

    if enemylife == 0:
        $ persistent.count_alice1 = 1
        $ renpy.jump(lavel + "_v")

    $ renpy.jump(lavel + "_a")


label skill_al2:
    $ alice_skill = 0
    $ before_action = 36
    call show_skillname("Word of Dispel")
    show alice st74 at xy(-240, 50) zorder 16 with dissolve #690

    a "I ask thee, eternal time..."
    a "Reveal my body, yoked in twilight..."

    play sound "audio/se/flash.ogg"
    show alice st03b at xy(X=-163) with flash

    a "Return my true body to me!"

    hide alice with dissolve
    call show_skillname("Frost Ozma")

    "Pure magic flows off of Alice, freezing the air around her!"

    play sound "audio/se/wind3.ogg"
    $ renpy.movie_cutscene("video/movie02.webm")

    if alice_skill1 == 0:
        jump skill_al_miss

    $ damage = rnd2(40*alice_skill2, 45*alice_skill2)
    play sound "audio/se/damage2.ogg"
    call enemylife3(nm="Alice")
    call hide_skillname

    if enemylife == 0:
        $ persistent.count_alice1 = 1
        $ renpy.jump(lavel + "_v")

    $ renpy.jump(lavel + "_a")


label skill_al3:
    $ alice_skill = 0
    $ before_action = 37
    call show_skillname("Word of Dispel")
    show alice st74 at xy(-240, 50) zorder 16 with dissolve #690

    a "I ask thee, eternal time..."
    a "Reveal my body, yoked in twilight..."

    play sound "audio/se/flash.ogg"
    show alice st03b at xy(X=-163) with flash

    a "Return my true body to me!"

    hide alice with dissolve
    call show_skillname("Monster Lord's Cruelty")
    play sound "audio/se/bom1.ogg"

    "The power of darkness radiates from Alice!"

    pause 0.3
    show layer master at maou_cruelty
    pause 3.0
    show color white zorder 30

    if alice_skill3 == 0:
        jump skill_al_miss

    $ damage =  rnd2(10*alice_skill3, 12*alice_skill3)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    $ damage =  rnd2(10*alice_skill3, 12*alice_skill3)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    $ damage =  rnd2(10*alice_skill3, 12*alice_skill3)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    $ damage =  rnd2((10*alice_skill3, 12*alice_skill3))
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    call hide_skillname
    show layer master
    hide color with Dissolve(1.0)

    if enemylife == 0:
        $ persistent.count_alice1 = 1
        $ renpy.jump(lavel + "_v")

    $ renpy.jump(lavel + "_a")


label skill_al_miss:
    show layer master
    hide color with Dissolve(1.0)

    "But Alice's attack had no effect!"

    call hide_skillname
    $ renpy.jump(lavel + "_a")


label skill_al4:
    $ alice_skill = 0
    $ before_action = 58
    call show_skillname("Word of Dispel")
    show alice st74 zorder 16:
        pos (-240, 50)
    with dissolve #690

    a "I ask thee, eternal time..."
    a "Reveal my body, yoked in twilight..."

    play sound "audio/se/flash.ogg"
    show alice st03b at xy(X=-163) with flash #690

    a "Return my true body to me!"

    hide alice with dissolve
    call show_skillname("Eye of Recovery")
    pause 0.2

    "The power of restoration fills Alice's eyes!{w}{nw}"

    play sound "audio/se/kaihuku.ogg"
    $ damage = max_mylife

    extend "{w=.7}\nLuka has been completely healed!"

    if mylife + damage > max_mylife:
        $ mylife = max_mylife
    else:
        $ mylife += damage

    call hide_skillname

    if max_mylife < mylife * 5:
        call face(param=1)

    $ renpy.jump(lavel + "_a")


label alice_skillx:
    while True:
        $ ransu = rnd2(1,4)

        if ransu == 1:
            jump alice_skillx1
        elif ransu == 2:
            jump alice_skillx2
        elif ransu == 3:
            jump alice_skillx3
        elif ransu == 4 and owaza_count3h > 3:
            jump alice_skillx4


label alice_skillx1:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        a "Haa!"
    elif ransu == 2:
        a "Eat this!"
    elif ransu == 3:
        a "Hya!"

    "Alice attacks!"

    play sound "audio/se/dageki.ogg"
    $ ori_name = "Alice"
    $ damage = rnd2(1000,1200)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname

    if enemylife == 0:
        $ persistent.count_alice2 = 1
        $ renpy.pop_return()
        $ renpy.jump(lavel + "_v")

    return


label alice_skillx2:
    python:
        while True:
            ransu = rnd2(1,6)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        a "I call forth the fires of hell!"
    elif ransu == 2:
        a "Can you endure this scorching heat!?"
    elif ransu == 3:
        a "Eat the flames of the Monster Lord!"
    elif ransu == 4:
        a "Burn in my flames!"
    elif ransu == 5:
        a "Taste the hellfire of a Monster Lord!"
    elif ransu == 6:
        a "Burn to ashes!"

    call show_skillname("Omega Blaze")

    "Alice creates a storm of fire!"

    play sound "audio/se/fire1.ogg"
    $ renpy.movie_cutscene("video/movie01.webm")
    play sound "audio/se/damage2.ogg"
    $ renpy.show(img_tag, at_list=[slash, alpha_off])
    $ ori_name = "Alice"
    $ damage = rnd2(3000,3500)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname

    if enemylife == 0:
        $ persistent.count_alice2 = 1
        $ renpy.pop_return()
        $ renpy.jump(lavel + "_v")

    return


label alice_skillx3:
    python:
        while True:
            ransu = rnd2(1,6)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        a "I call forth the icy touch of death!"
    elif ransu == 2:
        a "Go numb to your very bones!"
    elif ransu == 3:
        a "Freeze in a block of ice!"
    elif ransu == 4:
        a "Can you endure the chill of death!?"
    elif ransu == 5:
        a "I'll freeze even the air itself!"
    elif ransu == 6:
        a "Freeze!"

    call show_skillname("Frost Ozma")

    "Alice's magical power freezes the air!"

    play sound "audio/se/wind3.ogg"
    $ renpy.movie_cutscene("video/movie02.webm")
    play sound "audio/se/damage2.ogg"
    $ renpy.show(img_tag, at_list=[slash, alpha_off])
    $ ori_name = "Alice"
    $ damage = rnd2(3000,3500)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname

    if enemylife == 0:
        $ persistent.count_alice2 = 1
        $ renpy.pop_return()
        $ renpy.jump(lavel + "_v")

    return


label alice_skillx4:
    $ owaza_count3h = 0

    python:
        while True:
            ransu = rnd2(1,6)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        a "This is the power of a Monster Lord!"
    elif ransu == 2:
        a "Taste my power!"
    elif ransu == 3:
        a "I'll show you the power of darkness!"
    elif ransu == 4:
        a "Taste the secret art of my family!"
    elif ransu == 5:
        a "The power of darkness shall destroy everything!"
    elif ransu == 6:
        a "Perish in the darkness!"

    call show_skillname("Monster Lord's Cruelty")
    play sound "audio/se/bom1.ogg"

    "Alice lets loose the power of darkness!"

    pause 0.3
    show layer master at maou_cruelty
    pause 3.0
    show color white zorder 30
    $ damage = rnd2(1000,1200)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    $ damage = rnd2(1000,1200)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    with dissolve
    $ damage = rnd2(1000,1200)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    $ damage = rnd2(1000,1200)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.6, dist=30)

    "Alice deals [damage] damage!"

    $ enemylife -= damage

    if enemylife < 0:
        $ enemylife = 0

    call hide_skillname
    show layer master
    hide color with Dissolve(2.0)

    if enemylife == 0:
        $ persistent.count_alice2 = 1
        $ renpy.pop_return()
        $ renpy.jump(lavel + "_v")

    return


label aruma_skillx:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        jump aruma_skillx1
    elif ransu == 2:
        jump aruma_skillx2
    elif ransu == 3:
        jump aruma_skillx3
    elif ransu == 4:
        jump aruma_skillx4


label aruma_skillx1:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        alma "Haa!"
    elif ransu == 2:
        alma "Ha!"
    elif ransu == 3:
        alma "Hya!"

    "Alma Elma attacks!"

    play sound "audio/se/dageki.ogg"
    $ ori_name = "Alma Elma"
    $ damage = rnd2(800,1000)
    call enemylife2
    $ ori_name = "Luka"
    return


label aruma_skillx2:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        alma "Take this!"
    elif ransu == 2:
        alma "Prepare for violence!"
    elif ransu == 3:
        alma "My fist... Taste it!"
    elif ransu == 4:
        alma "I'll pound you to dust!"

    call show_skillname("Diamond Plated Fist")
    play sound "audio/se/dageki2.ogg"

    show effect 017 at xy(X=180, Y=80) zorder 30
    pause .75
    hide effect

    "Alma Elma's fist slams into Ilias!"

    play sound "audio/se/damage2.ogg"
    $ ori_name = "Alma Elma"
    $ damage = rnd2(4000,4500)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname
    return


label aruma_skillx3:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        alma "Eat this!"
    elif ransu == 2:
        alma "Break!"
    elif ransu == 3:
        alma "Hiiiya!"
    elif ransu == 4:
        alma "This knee can even destroy a Dragon!"

    call show_skillname("Dragon Knee")
    play sound "audio/se/dageki2.ogg"
    show effect 016 at xy(180, 80) zorder 30
    pause .25
    hide effect with Dissolve(0.01)

    "Alma Elma jumps forward as fast as lightning, kneeing Ilias!"

    play sound "audio/se/damage2.ogg"
    $ ori_name = "Alma Elma"
    $ damage = rnd2(2000,2500)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname
    return


label aruma_skillx4:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        alma "With my very heart and soul!"
    elif ransu == 2:
        alma "My final attack!"
    elif ransu == 3:
        alma "With all of my fighting spirit...!"
    elif ransu == 4:
        alma "With everything I've got...!"

    call show_skillname("Palm of Pure Violence")
    play sound "audio/se/dageki2.ogg"
    show effect 025 zorder 30 #2
    pause .8
    hide effect

    "With all of her strength and spirit, Alma Elma slams her palm into Ilias!"

    play sound "audio/se/damage2.ogg"
    $ ori_name = "Alma Elma"
    $ damage = rnd2(5000,5500)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname
    return


label tamamo_skillx:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        jump tamamo_skillx1
    elif ransu == 2:
        jump tamamo_skillx2
    elif ransu == 3:
        jump tamamo_skillx3


label tamamo_skillx1:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        tamamo_no_mae "Ha!"
    elif ransu == 2:
        tamamo_no_mae "Hya!"
    elif ransu == 3:
        tamamo_no_mae "Haa!"

    "Tamamo attacks!"

    play sound "audio/se/dageki.ogg"
    $ ori_name = "Tamamo"
    $ damage = rnd2(2000,2500)
    call enemylife2
    $ ori_name = "Luka"
    return


label tamamo_skillx2:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        tamamo_no_mae "Taste my power!"
    elif ransu == 2:
        tamamo_no_mae "It has been a while since the last time I punched you!"
    elif ransu == 3:
        tamamo_no_mae "It has been a thousand years... But taste the power of my tails!"
    elif ransu == 4:
        tamamo_no_mae "I know you haven't forgotten the power of my tails!"

    call show_skillname("Nine Moons")

    "All nine of Tamamo's tails slam into Ilias!"

    pause 0.1
    play sound "audio/se/tuki.ogg"
    show effect 023 zorder 30  #2
    pause .2
    play sound "audio/se/tuki.ogg"
    show effect 023 at xy(200, 100) zorder 30
    pause .2
    play sound "audio/se/tuki.ogg"
    show effect 023 at xy(50, -50) zorder 30
    pause .2
    play sound "audio/se/tuki.ogg"
    show effect 023 at xy(-200, 100) zorder 30
    pause .2
    play sound "audio/se/tuki.ogg"
    show effect 023 at xy(-50, -50) zorder 30
    pause .2
    hide effect
    $ ori_name = "Tamamo"
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("first")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("second")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("last")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("first")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("second")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("last")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("first")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("second")
    play sound "audio/se/damage.ogg"
    $ damage = rnd2(900,1100)
    call enemylife2("last")
    call hide_skillname
    $ ori_name = "Luka"
    return


label tamamo_skillx3:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        tamamo_no_mae "I'll remind you of the pain I can bring."
    elif ransu == 2:
        tamamo_no_mae "You shall never forget the pain of my claws again!"
    elif ransu == 3:
        tamamo_no_mae "I'll slice you to pieces!"
    elif ransu == 4:
        tamamo_no_mae "A dance of death!"

    call show_skillname("Dance of Death")

    "Tamamo begins a dance of death, her claws slicing everything around her!"

    pause 0.1
    show effect 024 zorder 30
    pause 2.4
    hide effect
    $ ori_name = "Tamamo"
    play sound "audio/se/damage2.ogg"
    $ damage = rnd2(2500,3000)
    call enemylife2("first")
    play sound "audio/se/damage2.ogg"
    $ damage = rnd2(2500,3000)
    call enemylife2("second")
    play sound "audio/se/damage2.ogg"
    $ damage = rnd2(2500,3000)
    call enemylife2("last")
    call hide_skillname
    $ ori_name = "Luka"
    return


label gran_skillx:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        jump gran_skillx1
    elif ransu == 2:
        jump gran_skillx2
    elif ransu == 3:
        jump gran_skillx3
    elif ransu == 4:
        jump gran_skillx4


label gran_skillx1:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        g "Here I come!"
    elif ransu == 2:
        g "Haa!"
    elif ransu == 3:
        g "Hya!"

    "Granberia attacks!"

    play sound "audio/se/slash.ogg"
    $ ori_name = "Granberia"
    $ damage = rnd2(1000,1100)
    call enemylife2
    $ ori_name = "Luka"
    return


label gran_skillx2:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        g "Taste the power of a Dragon!"
    elif ransu == 2:
        g "How about this!?"
    elif ransu == 3:
        g "Break!"
    elif ransu == 4:
        g "Cut into pieces!"

    call show_skillname("Dragon Butcher Attack")

    "Granberia swings down her huge sword!"

    play sound "audio/se/karaburi.ogg"
    show effect 010 at xy(X=150, Y=30) zorder 30
    pause .25
    hide effect

    play sound "audio/se/damage2.ogg"
    $ ori_name = "Granberia"
    $ damage = rnd2(2500,2800)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname
    return


label gran_skillx3:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        g "I'll pierce you through!"
    elif ransu == 2:
        g "Can you keep up!?"
    elif ransu == 3:
        g "I'll cut you through!"
    elif ransu == 4:
        g "A flurry of pierces!"

    call show_skillname("Bloody Fissure Thunder Thrust * Gale")

    "Granberia steps forward like lightning, her sword creating a thunderous boom!"

    play sound "audio/se/slash3.ogg"
    show thunder thrust b at xy(200, 80) zorder 30
    pause .2
    hide thunder
    play sound "audio/se/damage.ogg"
    $ ori_name = "Granberia"
    $ damage = rnd2(2500,2800)
    call enemylife2
    $ ori_name = "Luka"
    call hide_skillname
    return


label gran_skillx4:
    $ ransu = rnd2(1,4)

    if ransu == 1:
        g "A speed greater than the Gods!"
    elif ransu == 2:
        g "I'll cut you to bits!"
    elif ransu == 3:
        g "I'll attack faster than you can defend!"
    elif ransu == 4:
        g "Haaaaaaaaaaaaaaa!!!"

    call show_skillname("Death Sword Chaos Star")

    "Granberia slashes out with countless blows, too many to see!"

    pause 0.5
    show death sword chaos star zorder 30
    pause 1.5
    hide death
    play sound "audio/se/slash.ogg"
    $ ori_name = "Granberia"
    $ damage = rnd2(600,700)
    call enemylife2("first")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(600,700)
    call enemylife2("second")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(600,700)
    call enemylife2("last")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(600,700)
    call enemylife2("first")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(600,700)
    call enemylife2("last")
    $ ori_name = "Luka"
    call hide_skillname
    return


label gran_skillx5:
    call show_skillname("Неудержимый Испепеляющий Клинок")
    call skill16a

    "Wreathed in flames, Granberia lets loose an endless flurry of attacks!"

    pause 0.5
    $ ori_name = "Granberia"
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(1200,1500)
    call enemylife2("first")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(1200,1500)
    call enemylife2("second")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(1200,1500)
    call enemylife2("last")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(1200,1500)
    call enemylife2("first")
    play sound "audio/se/slash.ogg"
    $ damage = rnd2(1200,1500)
    call enemylife2("last")
    call hide_skillname
    $ ori_name = "Luka"
    return

label skill26:
    $ before_action = 91

    ori "............."

    call show_skillname("Разгон")
    play sound "audio/se/power.ogg"
    if enemy_wind < 2:

        "[ori_name!t] заряжает свою магию!{w}{nw}"
    elif enemy_wind > 1:

        "[ori_name!t] заряжает свою магию!{w}{nw}"
        if enemy_holy != 1 and enemy_holy != 2 and bougyo < 1:
            $ renpy.jump(lavel + "_chargecancel")
        elif enemy_holy != 1 and skill01 < 27:
            $ renpy.jump(lavel + "_chargecancel")
    if nocharge > 0:
        $ renpy.jump(lavel + "_chargecancel")

    $ damage = 2 + mp_charge
    $ mp += damage
    $ mp_charge += 1
    if mp_charge < 2:
        $ mp_charge = 1
    if mp > max_mp:
        $ mp = max_mp

    extend "\n[ori_name!t] восстанавливает [damage] SP!"

    if guard_on == 1 and skill01 > 26 and fire < 1:
        $ bougyo = 2

    call hide_skillname

    $ renpy.jump(lavel + "_a")
