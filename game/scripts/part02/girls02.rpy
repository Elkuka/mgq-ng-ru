init python:
########################################
# ALICE2
########################################
    def get_alice2():
        girl = Chr()
        girl.unlock = persistent.alice2_unlock == 1
        girl.name = "Alice"
        girl.pedia_name = "Alice (2)"
        girl.info = _("""The 16th Monster Lord, outside of the founding Monster Lord, she is the most powerful of them all. Even though she's very young in the monster world, she holds the loyalty of the Four Heavenly Knights.
Though she has a policy for monsters to only attack humans for self-defense, there are many who are seemingly going against it. But in fear of her overwhelming power as the Monster Lord, there are none who dare revolt against her.
She has been traveling with Luka for a long time. With Alice absent from the castle, Tamamo seems to be in charge of governmental affairs.""")
    #   girl.label = "alice2"
        girl.label_cg = "alice2_cg"
        girl.info_rus_img = "alice2_pedia"
        girl.m_pose = alice2_pose
        girl.zukan_ko = None
        girl.facenum = 10
        girl.posenum = 5
        girl.x0 = -20
        girl.x1 = 150
        girl.lv = 145
        girl.hp = 32000
        girl.echi = ["lb_0163a", "lb_0203a", "lb_0279a"]
        girl.scope = {"ruka_tati": 1,
        "mylv": 25,
        "skill01": 0,
        "item01": 1
        }
        return girl

########################################
# CENTA
########################################
    def get_centa():
        girl = Chr()
        girl.unlock = persistent.centa_unlock == 1
        girl.name = "Centaur Girl"
        girl.pedia_name = "Centaur Girl"
        girl.info = _("""A very famous, half-horse half-human monster. Proud of their heritage, they are quite aloof from other monsters. But once a year, they go into a breeding frenzy, and will attack any man they see in order to satisfy their urges. Once she captures a mate, she will rape them without rest. Despite her horse like pussy being loose, she can control it with such skill that no man can resist it.After a man is raped by her, the Centaur Girl will continue to use him as her personal stallion.
Furthermore, Centaur Girls generally take one man per breeding season. But if they particularly like the man they captured, they are known to keep them as their personal stallion for life."""
)
        girl.zukan_ko = "centa"
        girl.label = "centa"
        girl.label_cg = "centa_cg"
        girl.info_rus_img = "centa_pedia"
        girl.m_pose = centa_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 25
        girl.hp = 1500
        girl.skills = (
            (3263, "Centaur Kiss"),
            (3264, "Centaur Handjob"),
            (3265, "Centaur Blowjob"),
            (3266, "Centaur Rape")
            )
        girl.scope = {"ruka_tati": 1,
        "mylv": 25,
        "skill01": 7,
        "item01": 9
        }
        return girl

########################################
# KAERU
########################################
    def get_kaeru():
        girl = Chr()
        girl.unlock = persistent.kaeru_unlock == 1
        girl.name = "Frog Girls"
        girl.pedia_name = "Frog Girls"
        girl.info = _("""An amphibious monster, they infest streams and wetlands. They prefer feeding on humans, and will generally attack them in swarms. Once the man is caught, they will use their tongues to lick them to ejaculation. Their skill full use of their long tongues can easily lick their prey dry.Since there are usually many of them, their feeding never stops with a single ejaculation.
If they find a particularly good male, they will sometimes mate with him as a group. The Frog Girls will take turns raping him, and will switch after every orgasm.Easily overpowered by their sheer numbers, the man will have no choice but to mate with every single one of them. After the mating is finished, the man will surely be completely exhausted. Exploiting that weakness, they will usually be taken to the Frog Girls' home and kept as a shared mate."""
)
        girl.zukan_ko = "kaeru"
        girl.label = "kaeru"
        girl.label_cg = "kaeru_cg"
        girl.info_rus_img = "kaeru_pedia"
        girl.m_pose = kaeru_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -42
        girl.x1 = 158
        girl.lv = 26
        girl.hp = 1600
        girl.echi = ["kaeru_ha", "kaeru_hb"]
        girl.skills = ((3267, "Frog Handjob"),
                        (3268, "Frog Footjob"),
                        (3269, "Frog Tit Fuck"),
                        (3270, "Full-Body Licking Attack"),
                        (3271, "Groin Licking Attack"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 26,
        "skill01": 8,
        "item01": 9
        }
        return girl

########################################
# ALRAUNE
########################################
    def get_alraune():
        girl = Chr()
        girl.unlock = persistent.alraune_unlock == 1
        girl.name = "Alraune"
        girl.pedia_name = "Alraune"
        girl.info = _("""A famous plant type monster. There are many different kinds of Alraunes, and their personality and appearance vary wildly depending on the type.This Alraune is of a very pure type, and has an upper body very much similar to a human's.
They can live off photosynthesis alone, but they prefer male semen. Their bodies all contain an intake for semen, and will carefully extract it from any capture male.Since it's designed to feed, the hole is quite capable of forcing any man to come quickly.
Alraunes generally have a gentle personality, and will not suck a man dry. Always optimistic, they live a mostly carefree life."""
)
        girl.zukan_ko = "alraune"
        girl.label = "alraune"
        girl.label_cg = "alraune_cg"
        girl.info_rus_img = "alraune_pedia"
        girl.m_pose = alraune_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -53
        girl.x1 = 147
        girl.lv = 26
        girl.hp = 1700
        girl.skills = ((3272, "Alraune's Blowjob"),
                          (3273, "Alraune's Tit Fuck"),
                          (3274, "Alraune's Fragrance"),
                          (3275, "Succubus Flower"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 26,
        "skill01": 8,
        "item01": 9
        }
        return girl

########################################
# DULLAHAN
########################################
    def get_dullahan():
        girl = Chr()
        girl.unlock = persistent.dullahan_unlock == 1
        girl.name = "Dullahan"
        girl.pedia_name = "Dullahan"
        girl.info = _("""Filled with powerful magic, she is able to separate her head from her body. Many Dark Elves train to acquire the power needed to become a Dullahan. Boasting powerful physical skills, their fighting ability is extremely high. With the ability for the body and head to operate independently of each other, the head on her hair can take advantage of any openings the body creates.She's easily a match for any warrior who dares to challenge her.
Once the Dullahan defeats her challenger, she will force them to surrender fully to her. Using her magical hair and flexible tongue, no man can resist her.
Many of them are interested in fencing and chivalry. Some of them even enjoy participating in the Coliseum, testing their strength against worthy challengers.""")
        girl.zukan_ko = "dullahan"
        girl.label = "dullahan"
        girl.label_cg = "dullahan_cg"
        girl.info_rus_img = "durahan_pedia"
        girl.m_pose = dullahan_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 28
        girl.x1 = 228
        girl.lv = 27
        girl.hp = 2000
        girl.skills = ((3276, "Degeneration: Hand"),
                          (3277, "Degeneration: Breast"),
                          (3278, "Degeneration: Hair"),
                          (3279, "Degeneration: Mouth"),
                          (3280, "Restraint: Hand"),
                          (3281, "Restraint: Breast"),
                          (3282, "Restraint: Hair"),
                          (3283, "Restraint: Mouth"),
                          (3284, "Kiss of Ecstasy"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 26,
        "skill01": 8,
        "item01": 9
        }
        return girl

########################################
# CERBERUS
########################################
    def get_cerberus():
        girl = Chr()
        girl.unlock = persistent.cerberus_unlock == 1
        girl.name = "Cerberus"
        girl.pedia_name = "Cerberus"
        girl.info = _("""A beast famous as the "Hellhound". Three heads attached to one body, each able to think for itself. Her agility is very high, and it's difficult to get an opening on her due to her three sets of eyes. A very dangerous monster to cross swords with.
A sexually greedy beast, they will take any chance to rape a man given to them. Once she mounts her mate, there is nothing they can do. Giving in to her beast instincts, she will thrash and wildly pump her mate, quickly forcing them to orgasm. Raping her catch without mercy, they are even known to go until he dies.""")
        girl.zukan_ko = "cerberus"
        girl.label = "cerberus"
        girl.label_cg = "cerberus_cg"
        girl.info_rus_img = "kerberos_pedia"
        girl.m_pose = cerberus_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 27
        girl.hp = 2000
        girl.echi = ["cerberus_ha", "cerberus_hb"]
        girl.skills = ((3285, "Shake Job"),
                           (3286, "Triple Tit Fuck"),
                           (3287, "Triple Blowjob"),
                           (3288, "Evil Dog's Adultery"),
                           (3289, "Wild Shake"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 27,
        "skill01": 8,
        "item01": 9
        }
        return girl

########################################
# ALMA_ELMA2
########################################
    def get_alma_elma2():
        girl = Chr()
        girl.unlock = persistent.alma_elma2_unlock == 1
        girl.name = "Alma Elma"
        girl.pedia_name = "Alma Elma (2)"
        girl.info = _("""One of the Four Heavenly Knights, she's the Succubus Queen, and commands the power of wind. Also surprisingly skilled in martial arts, she's a force to be reckoned with in hand to hand combat. Despite being called the Queen Succubus, it doesn't look like she leads them. Believing in a carefree life of anything goes, she doesn't have any subordinates.
A martial arts enthusiast, she fights in the Grand Noah Coliseum under the name "Kyuba". Each time she appears, she has won. Known as a "Strong Mysterious Girl", she has many fans among the spectators. None of them know she is one of the Four Heavenly Knights.""")
        girl.zukan_ko = "alma_elma2"
        girl.label = "alma_elma2"
        girl.label_cg = "alma_elma2_cg"
        girl.info_rus_img = "alma_elma2_pedia"
        girl.m_pose = alma_elma2_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 125
        girl.hp = 15000
        girl.echi = ["alma_elma2_ha", "alma_elma2_hb"]
        girl.skills = ((3290, "Melty Hand"),
                              (3291, "Sweet Blow"),
                              (3292, "Succubatic Footwork"),
                              (3602, "Succubus Arts"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 28,
        "skill01": 8,
        "item01": 9
        }
        return girl

########################################
# YUKIONNA
########################################
    def get_yukionna():
        girl = Chr()
        girl.unlock = persistent.yukionna_unlock == 1
        girl.name = "Yuki-onna"
        girl.pedia_name = "Yuki-onna"
        girl.info = _("""An extremely rare monster that only lives in the mountains in the Yamatai Region. Able to chill the air at will, it's said she is able to create blizzards.
When a man of her liking enters her territory, she creates a snowstorm to weaken him. Embracing the weakened man, she steals his energy and body heat. Her chilling embrace brings ecstasy to the man as he breathes his last breath in her chest. In some cases, she may even have sex with the man. With their body heat and semen both sucked out by her cold vagina, the man dies while shivering in ecstasy.
In the extremely rare chance that she falls in love with a man, it's said she will take the form of a human woman and press him into marriage. This marriage belief is mostly handed down through stories in the Yamatai Region.""")
        girl.zukan_ko = "yukionna"
        girl.label = "yukionna"
        girl.label_cg = "yukionna_cg"
        girl.info_rus_img = "yukionna_pedia"
        girl.m_pose = yukionna_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 28
        girl.hp = 1800
        girl.echi = ["yukionna_ha", "yukionna_hb"]
        girl.skills = ((3293, "Icicle Hair"),
                       (3294, "Ice Handjob"),
                       (3295, "Freezing Mouth"),
                       (3296, "Frozen Kiss"),
                       (3297, "Chilling Embrace")
                       )
        girl.scope = {"ruka_tati": 1,
        "mylv": 28,
        "skill01": 8,
        "skill02": 2,
        "item01": 9
        }
        return girl

########################################
# NEKOMATA
########################################
    def get_nekomata():
        girl = Chr()
        girl.unlock = persistent.nekomata_unlock == 1
        girl.name = "Nekomata"
        girl.pedia_name = "Nekomata"
        girl.info = _("""A friendly monster towards humans. They rarely attack humans, but may if they are starving. At times they may forcefully mate with a human, so care is still required. They are surprisingly agile and quick-footed, so they should not be underestimated despite their friendly appearance.
Unique to her race of monster, the Nekomata's vagina has a rough sensation to it. Easily capable of overpowering her partner with pleasure, men will ejaculate quickly after being inserted in their high quality genitals.
Other than human semen, they love dried bonito. Like a normal cat, they love walking around, basking in the sun and living selfish lives.""")
        girl.zukan_ko = "nekomata"
        girl.label = "nekomata"
        girl.label_cg = "nekomata_cg"
        girl.info_rus_img = "nekomata_pedia"
        girl.m_pose = nekomata_pose
        girl.facenum = 7
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 28
        girl.hp = 1800
        girl.skills = ((3298, "Bushy Tailjob"),
                           (3299, "Squishy Pawjob"),
                           (3300, "Hairy Tit Fuck"),
                           (3301, "Rough Tongue Blowjob"),
                           (3302, "Cat in Heat"),
                           (3303, "Perverted Cat"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 28,
        "skill01": 8,
        "skill02": 2,
        "item01": 9
        }
        return girl

########################################
# SAMURAIELF
########################################
    def get_samuraielf():
        girl = Chr()
        girl.unlock = persistent.samuraielf_unlock == 1
        girl.name = "Samurai Elf"
        girl.pedia_name = "Samurai Elf"
        girl.info = _("""An elf native to the Yamatai Region, she follows the bushido code. Originating in Yamatai Village, the bushido code had a profound influence on the nearby elven village. However, the bushido code has long been a thing of the past in Yamatai Village, and only lives on in the elven village.
The Samurai Elf is on a mission to preserve the peace and order in the area, removing outsiders by force. Since she is both an elf and a trained warrior, a human warrior would struggle to match her in combat.
When her opponent is a man, she prefers using pinning techniques. After defeating him, she will rape him. Despite the bushido code, this act is triggered by her monster instincts.
She gets along well with the Kunoichi Elf, and pairs of them are frequently seem patrolling the area.""")
        girl.zukan_ko = "samuraielf"
        girl.label = "waelf"
        girl.label_cg = "samuraielf_cg"
        girl.info_rus_img = "samuraielf_pedia"
        girl.m_pose = samuraielf_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 29
        girl.hp = 1200
        girl.echi = ["waelf_ha"]
        girl.skills = ((3304, "Double Handjob"),
                             (3305, "Double Breast Pressure"),
                             (3306, "Double Mouth Attack"),
                             (3307, "Choking Breast Pin"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 29,
        "skill01": 8,
        "skill02": 2,
        "item01": 9
        }
        return girl

########################################
# KUNOITIELF
########################################
    def get_kunoitielf():
        girl = Chr()
        girl.unlock = persistent.kunoitielf_unlock == 1
        girl.name = "Kunoichi Elf"
        girl.pedia_name = "Kunoichi Elf"
        girl.info = _("""An elf native to the Yamatai Region that has been trained in the arts of ninjutsu. Ninjutsu was developed in Yamatai Village, but now only lives on in the nearby elven village.
In addition to ninjutsu, she is also trained in sexual skills, capable of showing men either heaven or hell through pleasure. If caught by a Kunoichi Elf, the man can easily be squeezed dry.
Wanting to defend her home area, she faithfully defends Yamatai Village. If an outsider enters without permission, she will prey on them with her Kunoichi skills. Due to her duty, she gets along well with the Samurai Elf.""")
        girl.zukan_ko = "kunoitielf"
        girl.label = "waelf"
        girl.label_cg = "kunoitielf_cg"
        girl.info_rus_img = "kunoitielf_pedia"
        girl.m_pose = kunoitielf_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 29
        girl.hp = 1400
        girl.echi = ["waelf_hb"]
        girl.skills = ((3664, "Leg Skill: Trample"),
                             (3308, "Hand Skill: Spider"),
                             (3309, "Breast Skill: Pressure"),
                             (3310, "Mouth Skill: Devour"),
                             (3311, "Ultm. Skill: Pussy Exhaustion"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 29,
        "skill01": 8,
        "skill02": 2,
        "item01": 9
        }
        return girl

########################################
# YAMATANOOROTI
########################################
    def get_yamatanooroti():
        girl = Chr()
        girl.unlock = persistent.yamatanooroti_unlock == 1
        girl.name = "Yamata no Orochi"
        girl.pedia_name = "Yamata no Orochi"
        girl.info = _("""An ancient monster that has eight independent heads. She was sealed for 100 years, but seems to have awoken recently. Boasting extremely high magical power, she seems to be the most powerful monster in the region.
Though she has sexual organs, she is unable to have children. Though she can't reproduce, she is very lustful. Located in the Yamatai Region, she requires a sacrifice every year. As the sacrifices will be licked dry by her eight heads for a full year, the sacrifices happily volunteer for the role.
After the year is up, the sacrifice will be sent back to the village and a new one will take his place.
The man that returns comes back disabled and exhausted, but is not killed.""")
        girl.zukan_ko = "yamatanooroti"
        girl.label = "yamatanooroti"
        girl.label_cg = "yamatanooroti_cg"
        girl.info_rus_img = "yamatanooroti_pedia"
        girl.m_pose = yamatanooroti_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 65
        girl.hp = 9500
        girl.skills = ((3312, "Sacrifice Tasting"),
                                (3313, "Eight Head Assault"),
                                (3314, "Unending Licking"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 29,
        "skill01": 8,
        "skill02": 2,
        "item01": 9
        }
        return girl

########################################
# MOSS
########################################
    def get_moss():
        girl = Chr()
        girl.unlock = persistent.moss_unlock == 1
        girl.name = "Moth Girl"
        girl.pedia_name = "Moth Girl"
        girl.info = _("""A monster living in Plansect village. Though they are mostly nocturnal, they are sometimes active as early as noon. However most of them attack men at night to suck their semen. Holding powerful pheromones in her scales, once a man breathes them in, he gladly becomes the Moth Girl's captive. Once entranced, the Moth Girl will insert the man's penis into her lower mouth and suck out his semen.
Like other monsters of the insect types, the Moth Girl's vaginas are very unique in structure. Tiny hairs line her warm vagina, along with unique creases and bends, capable of forcing men to orgasm nearly instantly. She generally uses the semen as food, but will use the semen for reproductive purposes in breeding season.""")
        girl.zukan_ko = "moss"
        girl.label = "insects"
        girl.label_cg = "moss_cg"
        girl.info_rus_img = "moss_pedia"
        girl.m_pose = moss_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 30
        girl.x1 = 230
        girl.lv = 30
        girl.hp = 1200
        girl.echi = ["insects_ha"]
        girl.skills = ((3316, "Six-Legged Caress"),
                       (3318, "Double Bug Tit Fuck"),
                       (3319, "Double Bug Blowjob"),
                       (3315, "Pheromone Scales"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 30,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MOSQUITO
########################################
    def get_mosquito():
        girl = Chr()
        girl.unlock = persistent.mosquito_unlock == 1
        girl.name = "Mosquito Girl"
        girl.pedia_name = "Mosquito Girl"
        girl.info = _("""An insect monster that lives in Plansect Village. She sucks human blood, but her favorite food is human semen.
Her straw-like tongue can slip into a man's urethra and directly suck out his semen. In addition, she can also use the vagina at the end of her abdomen to mate with the man normally. Her unique sucking abilities give the man an unparalleled sense of release.
Considering men to be nothing but simple prey, they are known to suck men to death. Due to her cold-blooded nature, she is considered a very dangerous monster.""")
        girl.zukan_ko = "mosquito"
        girl.label = "insects"
        girl.label_cg = "mosquito_cg"
        girl.info_rus_img = "mosquito_pedia"
        girl.m_pose = mosquito_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 30
        girl.x1 = 180
        girl.lv = 30
        girl.hp = 1200
        girl.echi = ["insects_hb"]
        girl.skills = ((3317, "Mosquito Handjob"),
                           (3318, "Double Bug Tit Fuck"),
                           (3319, "Double Bug Blowjob"),
                           (3320, "Mosquito Tit Fuck"),
                           (3321, "Double Insect Blowjob"),
                           (3585, "Mosquito Drain"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 30,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# IMOMUSI
########################################
    def get_imomusi():
        girl = Chr()
        girl.unlock = persistent.imomusi_unlock == 1
        girl.name = "Caterpillar Girl"
        girl.pedia_name = "Caterpillar Girl"
        girl.info = _("""An insect monster that resembles a green caterpillar. Her primary method if feeding is to immobilize her prey with her sticky silk, and then sucking out his semen. She can absorb the semen through her silk, or use her mouth to suck out his semen.
Once she has enough nourishment, she can go into the pupa stage. Since she sucks out semen for the purpose of storing nourishment for this stage, it's rare for her to suck a man to his death. It is unknown what kind of insect she will become until she grows bigger...""")
        girl.zukan_ko = "imomusi"
        girl.label = "insects"
        girl.label_cg = "imomusi_cg"
        girl.info_rus_img = "imomusi_pedia"
        girl.m_pose = imomusi_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -20
        girl.x1 = 180
        girl.lv = 22
        girl.hp = 800
        girl.echi = ["insects_hc"]
        girl.skills = ((3321, "Double Insect Blowjob"),
                          (3322, "Sticky Silk"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 30,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MUKADE
########################################
    def get_mukade():
        girl = Chr()
        girl.unlock = persistent.mukade_unlock == 1
        girl.name = "Centipede Girl"
        girl.pedia_name = "Centipede Girl"
        girl.info = _("""An insect monster that's closely related to a centipede. Boasting a durable carapace, her combat ability is high.
Feeding on human bodily fluids, her preferred feed is human semen. After restraining her catch, she will wrap her legs around him and use her many feet to stimulate him. Using those many feet to force him to ejaculate, she will squeeze him dry. In most cases, she will continue until her prey dies.
In some cases, she will mate with her catch through a gap in her carapace. Tiny projections that are capable of moving like feet provide unbearable stimulation to the man. Restrained, the man is forced to ejaculate dozens of times. Here, too, the man is usually forced to mate until his death of exhaustion. She's an extremely dangerous monster, as most encounters lead to death.""")
        girl.zukan_ko = "mukade"
        girl.label = "mukade"
        girl.label_cg = "mukade_cg"
        girl.info_rus_img = "mukade_pedia"
        girl.m_pose = mukade_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 30
        girl.hp = 1800
        girl.skills = ((3323, "Sweet Mouth"),
                         (3324, "Centipede Attack"),
                         (3325, "Centipede Trample"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 30,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# KAIKO
########################################
    def get_kaiko():
        girl = Chr()
        girl.unlock = persistent.kaiko_unlock == 1
        girl.name = "Silkworm Girl"
        girl.pedia_name = "Silkworm Girl"
        girl.info = _("""An insect monster with the characteristics of a silkworm.
Capable of spraying sticky silk from her abdomen, she rolls up her prey in a cocoon. As her silk is able to infuse the prey with sexual pleasure by mere contact, they will be brought to climax over and over. Capable of absorbing semen through the silk itself, the Silkworm Girl is able to obtain nourishment.
Wrapped in the cocoon and in a trance of ecstasy, the man is made to be both incontinent and forced to ejaculate over and over. Every fluid forced out by the cocoon is able to be turned into nutrients by the Silkworm Girl. In addition, the cocoon is capable of keeping the prey inside alive, making it an effective, long lasting source of nourishment.""")
        girl.zukan_ko = "kaiko"
        girl.label = "kaiko"
        girl.label_cg = "kaiko_cg"
        girl.info_rus_img = "kaiko_pedia"
        girl.m_pose = kaiko_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -230
        girl.lv = 31
        girl.hp = 2000
        girl.skills = ((3326, "Silky Blowjob"),
                        (3327, "Silky Tit Fuck"),
                        (3328, "Silky Pleasure"),
                        (3329, "Intoxicating Cocoon"),
                        (3330, "Semen Sucking Cocoon"))
        girl.scope = {
        "ruka_tati": 1,
        "mylv": 31,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SUZUMEBATI
########################################
    def get_suzumebati():
        girl = Chr()
        girl.unlock = persistent.suzumebati_unlock == 1
        girl.name = "Hornet Girl"
        girl.pedia_name = "Hornet Girl"
        girl.info = _("""An insect monster of the bee-type. With the Queen at the top of their social structure, she is part of the lowly soldier class. With her body made for combat, her durable exoskeleton and powerful muscles make her a formidable foe. Though she's primarily a meat-eater, she is also able to live off honey.
As the Queen is the only fertile member of the group, the worker and soldier types cannot breed. Because of that, all captured men are handed over to the Queen. If the man does not satisfy the Queen, he is made into a sex slave of the rest of the hive. Though she's barren, she still has a powerful sexual drive. Any captured man will be gladly raped to satisfy their desire. Used as an outlet for their sexual desire, the man will be kept as a sex slave for the hive until his death.""")
        girl.zukan_ko = "suzumebati"
        girl.label = "suzumebati"
        girl.label_cg = "suzumebati_cg"
        girl.info_rus_img = "suzumebati_pedia"
        girl.m_pose = suzumebati_pose
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -180
        girl.lv = 31
        girl.hp = 2000
        girl.skills = ((3331, "Multiple Handjob"),
                             (3332, "Honey Blowjob"),
                             (3333, "Honey Rich Tit Fuck"),
                             (3334, "Bee Dance"))
        girl.scope = {
        "ruka_tati": 1,
        "mylv": 31,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# QUEENBEE
########################################
    def get_queenbee():
        girl = Chr()
        girl.unlock = persistent.queenbee_unlock == 1
        girl.name = "Queen Bee"
        girl.pedia_name = "Queen Bee"
        girl.info = _("""Queen of the hive, she is also the leader of the insect race in Sectforest. Entrusting all other tasks to the other bees in the hive, she focuses mainly on breeding. If the hive was a single organism, she would be equivalent to the genitals.
All men captured by any of the worker or soldier bees are handed over to the Queen. If she likes him, then he becomes a tool used in her breeding. Forced to continually mate with her, the exquisite genitals held by the Queen force him to repeated ejaculations. Treated as little more than a tool to breed with, the forced mating can continue for days without rest. Like this, the Queen can be fully inseminated.
After the forced mating has finished, the man is not freed. Forcibly fed special honey produced by the bees, he is kept alive for many future breeding sessions.""")
        girl.zukan_ko = "queenbee"
        girl.label = "queenbee"
        girl.label_cg = "queenbee_cg"
        girl.info_rus_img = "queenbee_pedia"
        girl.m_pose = queenbee_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 45
        girl.hp = 3200
        girl.skills = ((3335, "Queen's Hand"),
                           (3336, "Queen's Bust"),
                           (3337, "Queen's Mouth"),
                           (3338, "Pheromone Kiss"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 31,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_ALM
########################################
    def get_a_alm():
        girl = Chr()
        girl.unlock = persistent.a_alm_unlock == 1
        girl.name = "Alra Arum"
        girl.pedia_name = "Alra Arum"
        girl.info = _("""A plant monster of the Alraune species.
Though capable of living through photosynthesis, she prefers to consume male semen. Using her soft petals to embrace the man, she is able to suck out his energy. As their energy is sucked, most men also ejaculate. In that way, they can suck out semen as nourishment. Kept in place by both her embrace and her ivy, the man is forced to orgasm over and over.
Though she appears kind, many often suck men to death. A particularly greedy monster, they are dangerous due to the chance of death.""")
        girl.zukan_ko = "a_alm"
        girl.label = "plants1"
        girl.label_cg = "a_alm_cg"
        girl.info_rus_img = "a_alm_pedia"
        girl.m_pose = a_alm_pose
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 6
        girl.x1 = 200
        girl.lv = 32
        girl.hp = 1600
        girl.echi = ["plants1_hb"]
        girl.skills = ((3343, "Arum Tit Fuck"),
                        (3344, "Arum Blowjob"),
                        (3345, "Lewd Flowers"),
                        (3346, "Monster Flower Suck"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 32,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_LOOTY
########################################
    def get_a_looty():
        girl = Chr()
        girl.unlock = persistent.a_looty_unlock == 1
        girl.name = "Alra Rooty"
        girl.pedia_name = "Alra Rooty"
        girl.info = _("""A plant monster of the Alraune species. With a small amount of dryad blood in her, she has taken a unique appearance.
She obtains her nourishment through the roots on the plant part of her body. As the roots wrap around a man's genitals, the stimulation forces them to ejaculate. In this method, many men are sucked completely dry.
In addition, if they particularly like their prey, she is known to forcibly assimilate with him. Leaving the man's will and consciousness in tact, their bodies unify as one. The body is completely under Alra Rooty's control, and is used as little more than a source of semen.""")
        girl.zukan_ko = "a_looty"
        girl.label = "plants1"
        girl.label_cg = "a_looty_cg"
        girl.info_rus_img = "a_looty_pedia"
        girl.m_pose = a_looty_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 20
        girl.x1 = 220
        girl.lv = 32
        girl.hp = 1400
        girl.echi = ["plants1_ha"]
        girl.skills = ((3339, "Double Tit Fuck"),
                          (3340, "Double Blowjob"),
                          (3341, "Rooty Caress"),
                          (3342, "Rooty Draw"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 32,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_VORE
########################################
    def get_a_vore():
        girl = Chr()
        girl.unlock = persistent.a_vore_unlock == 1
        girl.name = "Alra Vore"
        girl.pedia_name = "Alra Vore"
        girl.info = _("""A plant monster of the Alraune species.
An unusual plant that only eats meat, she uses the mouth's on the end of her ivy to devour her prey. She is capable of photosynthesis, but the energy obtained is inefficient.
If her prey is a man, she doesn't neglect to milk his semen first. Her mouth is capable of stimulating the man's penis, forcing him to come inside her predation organ. While forcing him to ejaculate, she will swallow him alive.
A cold-blooded monster, she enjoys watching her prey's agonized face as she eats him alive. She will sometimes very slowly swallow the man, harshly playing with her food more than is necessary.""")
        girl.zukan_ko = "a_vore"
        girl.label = "plants2"
        girl.label_cg = "a_vore_cg"
        girl.info_rus_img = "a_vore_pedia"
        girl.m_pose = a_vore_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -30
        girl.x1 = 170
        girl.lv = 32
        girl.hp = 1300
        girl.echi = ["plants2_ha"]
        girl.skills = ((3347, "Plant Suck"),
                         (3348, "Plant Vore"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 32,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_PARASOL
########################################
    def get_a_parasol():
        girl = Chr()
        girl.unlock = persistent.a_parasol_unlock == 1
        girl.name = "Alra Parasol"
        girl.pedia_name = "Alra Parasol"
        girl.info = _("""A plant monster that has many characteristics similar to a mushroom. Though it appears as though she's wearing clothes, it is in fact just her fungal body. Her characteristic parasol is used to shield her from direct sunlight.
Since her staple food is semen, she will use her fungi to cover the man's penis to extract it. If she particularly likes the man, she will draw his body into her own, and add him to her seedbed. The man is forced to ejaculate over and over, fertilizing her fungal body. Since his body is made into part of Alra Parasol's, he won't die until the host body dies.
In addition, Alra Parasol's body is constantly changing due to the mushrooms grown from her seedbed. This makes her method of reproduction different from other monsters.""")
        girl.zukan_ko = "a_parasol"
        girl.label = "plants2"
        girl.label_cg = "a_parasol_cg"
        girl.info_rus_img = "a_parasol_pedia"
        girl.m_pose = a_parasol_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = 50
        girl.x1 = 250
        girl.lv = 32
        girl.hp = 1000
        girl.echi = ["plants2_hb"]
        girl.skills = ((3349, "Double Blowjob"),
                            (3350, "Double Tit Fuck"),
                            (3351, "Sweet Hand"),
                            (3352, "Fungal Skirt"),
                            (3353, "Seedbed Embrace"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 32,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_PRISON
########################################
    def get_a_prison():
        girl = Chr()
        girl.unlock = persistent.a_prison_unlock == 1
        girl.name = "Alra Prison"
        girl.pedia_name = "Alra Prison"
        girl.info = _("""A plant monster that has a huge flower as part of her body.
She can freely control the flower attached to her body, and uses it as a cage to keep her prey locked inside, thus gaining her name. Locked inside her prison, the man will be attacked by countless branches of ivy. With all four limbs bound, the man is helplessly milked inside her prison.
Like this, she can trap the man inside of her forever, using him to supply her with semen. Essentially becoming a part of her body, he is kept inside of her until his natural lifespan is over. Until then, he will be bound and squeezed for his semen by the ivy continuously.""")
        girl.zukan_ko = "a_prison"
        girl.label = "plants2"
        girl.label_cg = "a_prison_cg"
        girl.info_rus_img = "a_prison_pedia"
        girl.m_pose = a_prison_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -40
        girl.x1 = 160
        girl.lv = 32
        girl.hp = 1000
        girl.echi = ["plants2_hc"]
        girl.skills = ((3354, "Flower Footjob"),
                           (3355, "Flower Intercrural"),
                           (3356, "Flower Prison"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 32,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# A_EMP
########################################
    def get_a_emp():
        girl = Chr()
        girl.unlock = persistent.a_emp_unlock == 1
        girl.name = "Alra Priestess"
        girl.pedia_name = "Alra Priestess"
        girl.info = _("""A plant monster that's also the leader of the Planforest plant race. Though she is powerful enough to be called leader, she is not as strong as the mercenary Canaan Sisters.
Though she speaks politely, she is extremely prideful, and considers herself above touching men. When squeezing men for semen, she prefers using either her feet or ivy. so as not to dirty herself.
In addition, she is heartless towards those she considers enemies. If the enemy is a human male, she will squeeze out his semen until he dies. Despite her appearance as a little girl, she is quite brutal.""")
        girl.zukan_ko = "a_emp"
        girl.label = "a_emp"
        girl.label_cg = "a_emp_cg"
        girl.info_rus_img = "a_emp_pedia"
        girl.m_pose = a_emp_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 8
        girl.x0 = -50
        girl.x1 = 150
        girl.lv = 35
        girl.hp = 2600
        girl.skills = ((3357, "Priestess Hair"),
                        (3358, "Priestess Foot"),
                        (3359, "Priestess Bust"),
                        (3360, "Priestess Mouth"),
                        (3361, "Queen's Foot Hell"),
                        (3362, "Queen's Semen Sucking"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 33,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# DOROTHY
########################################
    def get_dorothy():
        girl = Chr()
        girl.unlock = persistent.dorothy_unlock == 1
        girl.name = "Dorothy"
        girl.pedia_name = "Dorothy"
        girl.info = _("""A plant monster, she is also the youngest Canaan Sister. Taking after a venus flytrap, she is quite partial to the taste of meat. She particularly enjoys playing with human males as she slowly digests them, taunting and forcing them to ejaculate all the while. The sticky juices around her body allow her to start digesting her prey before she even swallows him. The pleasure received when dissolved by her juices is unbearable, forcing men to numerous orgasms as they begin to be digested. Due to her cruelty, she has eaten many men this way.
In addition to humans, she also eats insect monsters in the same way. During the war in Plansect Village, she is feared by the insects due to the amount of their brethren she has eaten."""
)
        girl.zukan_ko = "dorothy"
        girl.label = "dorothy"
        girl.label_cg = "dorothy_cg"
        girl.info_rus_img = "dorothy_pedia"
        girl.m_pose = dorothy_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 38
        girl.hp = 3300
        girl.skills = ((3363, "Sticky Hairs"),
                          (3364, "Trichome Penis Torture"),
                          (3365, "Venus Trap"),
                          (3366, "Philia Melt"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 33,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# RAFI
########################################
    def get_rafi():
        girl = Chr()
        girl.unlock = persistent.rafi_unlock == 1
        girl.name = "Raffia"
        girl.pedia_name = "Raffia"
        girl.info = _("""A plant monster resembling a pitcher plant, she is also the middle Canaan Sister.
She drops her prey into her large pitcher, filled with digestive juices, and slowly dissolves them. The lukewarm juices overpower the prey with ecstasy, forcing them to even begin to desire being dissolved inside her pitcher. Most men are also forced to orgasm inside her pitcher, providing her with even more nutrients. Sometimes Rafi will even use a small pitcher to squeeze semen from the prey currently being dissolved.
Eating both man and insect alike, she is a very dangerous monster.""")
        girl.zukan_ko = "rafi"
        girl.label = "rafi"
        girl.label_cg = "rafi_cg"
        girl.info_rus_img = "rafi_pedia"
        girl.m_pose = rafi_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 39
        girl.hp = 3400
        girl.skills = ((3367, "Ivy Caress"),
                       (3368, "Mini Pitcher"),
                       (3369, "Intoxicating Pitcher"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 33,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# DINA
########################################
    def get_dina():
        girl = Chr()
        girl.unlock = persistent.dina_unlock == 1
        girl.name = "Deana"
        girl.pedia_name = "Deana"
        girl.info = _("""A plant monster, she is also the oldest Canaan Sister. Her hair is a predation organ capable of dissolving both humans and insect monsters. She eats her prey in an almost businesslike manner, quickly eating them so she can return to more important tasks. Looking at the prey with bored eyes, she will watch them slowly dissolve in her organ.
The Canaan Sisters constantly roam about, looking for fights to take part in so that they can feed. Due to their mercenary lifestyle, they are more powerful than normal plant monsters. Traveling to Plansect Village, they volunteered to assist the plant monsters in the war. Due to their cruelty, the war intensified.""")
        girl.zukan_ko = "dina"
        girl.label = "dina"
        girl.label_cg = "dina_cg"
        girl.info_rus_img = "dina_pedia"
        girl.m_pose = dina_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 40
        girl.hp = 3500
        girl.skills = ((3665, "Venus Bend"),
                       (3666, "Venus Trap"),
                       (3370, "Philia Melt"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 33,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# JELLY
########################################
    def get_jelly():
        girl = Chr()
        girl.unlock = persistent.jelly_unlock == 1
        girl.name = "Jelly Girl"
        girl.pedia_name = "Jelly Girl"
        girl.info = _("""A sticky monster of the slime race. But due to her high elasticity, her body is almost more jelly like than slime like. She has a calm personality, and is simple-minded. But due to her sticky body, a common warrior would be unable to escape her grasp if caught. If that happens, he has no choice but to helplessly let her prey on him.
She enjoys human semen as much as other slimes. Able to control her sticky body at will, no man can resist an orgasm if she wants him to. Unexpectedly greedy, she will never allow her prey to escape once caught. Due to the ease of being caught in her sticky body until death, great care should be taken around her.""")
        girl.zukan_ko = "jelly"
        girl.label = "jelly"
        girl.label_cg = "jelly_cg"
        girl.info_rus_img = "jelly_pedia"
        girl.m_pose = jelly_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 34
        girl.hp = 2500
        girl.skills = ((3371, "Jelly Press"),
                        (3372, "Jelly Draw"),
                        (3373, "Jelly Heaven"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# BLOB
########################################
    def get_blob():
        girl = Chr()
        girl.unlock = persistent.blob_unlock == 1
        girl.name = "Blob Girl"
        girl.pedia_name = "Blob Girl"
        girl.info = _("""A carnivorous monster of the slime race. The Blob Girl herself is made up of digestive juices. Any prey who's taken into her body will never escape. As the man is stuck inside of her body, his semen is squeezed out while he's slowly digested.
Her tone is light and friendly, but she's extremely brutal. She seems to get sadistic pleasure from dissolving and feeding on men. She rarely wanders outside of her habitat, but is extremely dangerous.""")
        girl.zukan_ko = "blob"
        girl.label = "blob"
        girl.label_cg = "blob_cg"
        girl.info_rus_img = "blob_pedia"
        girl.m_pose = blob_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 34
        girl.hp = 2700
        girl.skills = ((3374, "Blob Spread"),
                       (3375, "Blob Draw"),
                       (3376, "Blob Heaven"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SLIME_GREEN
########################################
    def get_slime_green():
        girl = Chr()
        girl.unlock = persistent.slime_green_unlock == 1
        girl.name = "Green Slime"
        girl.pedia_name = "Green Slime"
        girl.info = _("""A slime monster composed of a green liquid. Though she has no special characteristics, her high magic power allows her to spread out her body to encompass a huge area. Though her body is stickier than other slimes, her personality is similar.
Feeding on the same staple food as other slimes, semen, she will wrap up the man in her sticky slime, and forcing him to ejaculate to feed on it. She will not squeeze him to his death, but will keep the man as her possession for exclusive use.
In the slime race, there are those who wish to remain alone, and those who prefer to be in groups. The Green Slime is of the latter group.
Due to this, she is a troublesome foe due to her likelihood of calling friends to assist her if things aren't going her way.""")
        girl.zukan_ko = "slime_green"
        girl.label = "slime_green"
        girl.label_cg = "slime_green_cg"
        girl.info_rus_img = "slime_green_pedia"
        girl.m_pose = slime_green_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 45
        girl.x1 = 245
        girl.lv = 34
        girl.hp = 1200
        girl.skills = ((3377, "Green Spread"),
                              (3378, "Green Draw"),
                              (3379, "Green Ecstasy"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SLIME_BLUE
########################################
    def get_slime_blue():
        girl = Chr()
        girl.unlock = persistent.slime_blue_unlock == 1
        girl.name = "Blue Slime"
        girl.pedia_name = "Blue Slime"
        girl.info = _("""A slime monster with a quiet personality. Very elastic and sticky, her blue body has a unique feel to it.
She prefers to attack men in groups instead of being alone. The Blue Slime is the type to join in when other slimes are attacking a man, unwilling to be the one to initiate. But that's just a quirk of her personality, and not due to being any less powerful than other slimes.
While other slimes prefer to focus on the man's groin area, she seems oddly fixated on nipples.""")
        girl.zukan_ko = "slime_blue"
        girl.label = "slimes"
        girl.label_cg = "slime_blue_cg"
        girl.info_rus_img = "slime_blue_pedia"
        girl.m_pose = slime_blue_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 15
        girl.x1 = 218
        girl.lv = 34
        girl.hp = 1300
        girl.skills = ((3381, "Blue Flow"),
                             (3384, "Double Draw"),
                             (3385, "Triple Draw"),
                             (3386, "Special Draw"),
                             (3387, "Double Spread"),
                             (3388, "Triple Spread"),
                             (3389, "Special Spread"),
                             (3390, "Special Heaven"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SLIME_RED
########################################
    def get_slime_red():
        girl = Chr()
        girl.unlock = persistent.slime_red_unlock == 1
        girl.name = "Red Slime"
        girl.pedia_name = "Red Slime"
        girl.info = _("""A slime monster with a red body. She has a lively personality, and hates to lose. She loves everything to do with anal sex, but is quite timid when it comes to more simple things.
Her body is much warmer than other slimes, bringing men an incredibly relaxing sensation when inside her. Due to her overconfident personality, she is known to squeeze semen from men until they're almost dead from exhaustion.""")
        girl.zukan_ko = "slime_green"
        girl.label = "slimes"
        girl.label_cg = "slime_red_cg"
        girl.info_rus_img = "slime_red_pedia"
        girl.m_pose = slime_red_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 27
        girl.x1 = 227
        girl.lv = 34
        girl.hp = 1300
        girl.skills = ((3382, "Red Gale"),
                            (3384, "Double Draw"),
                            (3385, "Triple Draw"),
                            (3386, "Special Draw"),
                            (3387, "Double Spread"),
                            (3388, "Triple Spread"),
                            (3389, "Special Spread"),
                            (3390, "Special Heaven"),)
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SLIME_PURPLE
########################################
    def get_slime_purple():
        girl = Chr()
        girl.unlock = persistent.slime_purple_unlock == 1
        girl.name = "Purple Slime"
        girl.pedia_name = "Purple Slime"
        girl.info = _("""Formed out of purple slime, she's a leader-like slime that lives in Undine's Spring. With the ability to communicate with Erubetie, she leads the slime girls when Erubetie is absent.
Her staple food is male semen, and she prefers to attack in a group. In addition to her body being extremely sticky, she is able to jiggle her body with great intensity. Using those two properties, she is able to skillfully use her body to force men to ejaculate.
Prey caught by her will not be allowed to die. She, along with her group, will keep them captive, squeezing out his semen until the end of his natural life.""")
        girl.zukan_ko = "slime_green"
        girl.label = "slimes"
        girl.label_cg = "slime_purple_cg"
        girl.info_rus_img = "slime_purple_pedia"
        girl.m_pose = slime_purple_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 13
        girl.x1 = 213
        girl.lv = 35
        girl.hp = 1500
        girl.skills = ((3380, "Purple Kiss"),
                               (3384, "Double Draw"),
                               (3385, "Triple Draw"),
                               (3386, "Special Draw"),
                               (3387, "Double Spread"),
                               (3388, "Triple Spread"),
                               (3389, "Special Spread"),
                               (3390, "Special Heaven"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 34,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# ERUBETIE1
########################################
    def get_erubetie1():
        girl = Chr()
        girl.unlock = persistent.erubetie1_unlock == 1
        girl.name = "Erubetie"
        girl.pedia_name = "Erubetie (1)"
        girl.info = _("""One of the Four Heavenly Knights, she is a slime monster. Called the Queen Slime, she is the most powerful of all the slime monsters. Able to divide and expand her body at will, her attacks are very unpredictable. She also seems capable of controlling water at will, using it to inflate her own body. In this way, she can even create a massive tsunami of slime.
Hating humans that pollute the water, she rarely appears before them. If any human sets foot in her home, she will show them no mercy. Erubetie will use her body itself to wrap them up and dissolve them inside her slimy body.
Not very interested in her duties as one of the Four Heavenly Knights, she rarely involves herself in their troubles. She has a calm personality, and is very fond of her fellow slime brethren. Due to that, her hatred of humans is deep-rooted.""")
        girl.zukan_ko = "erubetie1"
        girl.label = "erubetie1"
        girl.label_cg = "erubetie1_cg"
        girl.info_rus_img = "erubetie1_pedia"
        girl.m_pose = erubetie1_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = 25
        girl.x1 = 225
        girl.lv = 125
        girl.hp = 22000
        girl.skills = ((3391, "Melt Storm"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 35,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# UNDINE
########################################
    def get_undine():
        girl = Chr()
        girl.unlock = persistent.undine_unlock == 1
        girl.name = "Undine"
        girl.pedia_name = "Undine"
        girl.info = _("""A spirit who resides in a cave under ""Undine's Spring"", out of view of the rest of the world. A long time ago it seems as though she appeared before humans, but now she's a shut-in. Hating the humans who pollute the waters of the world, she mercilessly preys on any who dare enter her terrain. Absorbing them into her body, she assimilates their body to become one with hers. Though she dislikes humans, she seems to hold regrets that humans and monsters aren't existing in peace as they did in the past. A close friend of the Queen Slime, Erubetie, they treat each other as equals. They also seem not to interfere with each other's dealings.""")
        girl.zukan_ko = "undine"
        girl.label = "undine"
        girl.label_cg = "undine_cg"
        girl.info_rus_img = "undine_pedia"
        girl.m_pose = undine_pose
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 65
        girl.hp = 12000
        girl.skills = ((3392, "Aqua Shake"),
                     (3393, "Aqua Stroke"),
                     (3394, "Aqua Melty"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 35,
        "skill01": 8,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# KAMAKIRI
########################################
    def get_kamakiri():
        girl = Chr()
        girl.unlock = persistent.kamakiri_unlock == 1
        girl.name = "Mantis Girl"
        girl.pedia_name = "Mantis Girl"
        girl.info = _("""An insect monster strongly resembling a praying mantis. A carnivorous monster that eats not only other monsters, but human beings as well. Their favorite food however is male semen, and will both use them for reproduction and for feeding. Due to their unique insect based vagina, any male caught by them will be enraptured in the unique sensations. After forcing her prey to multiple ejaculations and weakening them, they will then usually eat their mate.
However when the mantis chooses a mate for reproductive purposes, she won't eat them. After she has been inseminated, she will lay her eggs on the male's body. The eggs will in turn stimulate the man, forcing him to multiple ejaculations, providing nourishment for the newborns. Like this, the male is used as a tool for reproduction all the way until their birth. Once captured, the male will be kept as a reproductive tool for the rest of his life.""")
        girl.zukan_ko = "kamakiri"
        girl.label = "kamakiri"
        girl.label_cg = "kamakiri_cg"
        girl.info_rus_img = "kamakiri_pedia"
        girl.m_pose = kamakiri_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 36
        girl.hp = 2800
        girl.skills = ((3395, "Mantis Blowjob"),
                       (3396, "Mantis Tit Fuck"),
                       (3397, "Mantis Rape"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 36,
        "skill01": 9,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SCYLLA
########################################
    def get_scylla():
        girl = Chr()
        girl.unlock = persistent.scylla_unlock == 1
        girl.name = "Scylla"
        girl.pedia_name = "Scylla"
        girl.info = _("""A very famous monster whose lower body is comprised of multiple tentacles. Various types exist, but this specific one's tentacles resemble an octopus's. With her multiple tentacles, she can both bind the male's body and squeeze his semen at the same time.
She generally only uses her tentacles to force men to come. Able to skillfully coil her tentacles around his penis, the Scylla can easily force any man to ejaculate. Due to the soft, elastic sensations of her tentacles, there has been no case of a man being able to resist her once captured.
If a male with high quality genes is found, they are known to force them to mate. Using her tentacles, she can manipulate the man's body to insert himself into her, and keep him from escaping. With additional tiny tentacles in her vagina designed for mating, the intense pleasure allows her to easily breed with her captured male.""")
        girl.zukan_ko = "scylla"
        girl.label = "scylla"
        girl.label_cg = "scylla_cg"
        girl.info_rus_img = "scylla_pedia"
        girl.m_pose = scylla_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -60
        girl.x1 = 140
        girl.lv = 37
        girl.hp = 3000
        girl.skills = ((3398, "Tentacle Body Torture"),
                     (3399, "Tentacle Nipple Torture"),
                     (3400, "Tentacle Groin Torture"),
                     (3401, "Tentacle Anal Torture"),
                     (3402, "Tentacle Tighten"),
                     (3403, "Elysion - De - Scylla"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 37,
        "skill01": 10,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MEDUSA
########################################
    def get_medusa():
        girl = Chr()
        girl.unlock = persistent.medusa_unlock == 1
        girl.name = "Medusa"
        girl.pedia_name = "Medusa"
        girl.info = _("""A famous monster with snakes for hair. A gaze from her magic eyes can also turn anyone to stone. Using this power, they are known for playing with men as they slowly turn them to stone. Since they have to both feed their snake hair and maintain their own high magic power, they require lots of semen. Due to that, they are always hungry for more men to play with. Utilizing both her own body and her snake hair, she will do everything she can to force the male to feed her.
In addition, if she finds a man she likes, she may just rape him for fun. With tiny feelers inside her vagina that are similar to her snake hair, no man is able to resist her for long.""")
        girl.zukan_ko = "medusa"
        girl.label = "medusa"
        girl.label_cg = "medusa_cg"
        girl.info_rus_img = "medusa_pedia"
        girl.m_pose = medusa_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = 28
        girl.x1 = 218
        girl.lv = 37
        girl.hp = 3200
        girl.skills = ((3404, "Medusa's Hand"),
                     (3405, "Medusa's Mouth"),
                     (3406, "Medusa's Thighs"),
                     (3407, "Lewd Snake's Hair"),
                     (3408, "Lewd Snake's Vagina"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 37,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# GOLEM
########################################
    def get_golem():
        girl = Chr()
        girl.unlock = persistent.golem_unlock == 1
        girl.name = "Golem Girl"
        girl.pedia_name = "Golem Girl"
        girl.info = _("""An artificial monster created by magic science. Having no will of it's own, it simply guards the castle like it was instructed to by it's creators.
Those with permission to enter the castle need to blow on a special whistle in order for her to allow them to pass. In addition, she is instructed not to chase after anyone who retreats.
Where her genitals would be, she is equipped with a special hole designed to process male semen. Any male inserted into her to be used as a power source will be quickly drained and left weak. As the males are usually condemned prisoners, they are used to power the Golem Girl until their death. This inhuman act appears to be carried out without either the King nor the citizens knowledge.""")
        girl.zukan_ko = "golem"
        girl.label = "golem"
        girl.label_cg = "golem_cg"
        girl.info_rus_img = "golem_pedia"
        girl.m_pose = golem_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 44
        girl.hp = 6000
        girl.skills = ((3409, "Play"),
                    (3410, "Power Supply"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 38,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MADGOLEM
########################################
    def get_madgolem():
        girl = Chr()
        girl.unlock = persistent.madgolem_unlock == 1
        girl.name = "Mud Golem Girl"
        girl.pedia_name = "Mud Golem Girl"
        girl.info = _("""An artificial monster created by magic science with no will of it's own. It appears to assist mainly with engineering projects inside Grangold Castle.
Using semen for energy, this artificial monster will return to a simple puddle of mud when it runs out. Resupplying is performed by wrapping a male's body in her mud, and stimulating him until his orgasm. As the man is never killed in process, energy is resupplied to the Mud Golems on a volunteer basis.
A while ago an error was discovered in her thought circuit that caused her to go berserk, squeezing random men's semen to the brink of death. However she was redesigned, and a similar incident hasn't happened since. But the repairs to her thought circuit seemed to have left her vulnerable to large spikes of magical energy...""")
        girl.zukan_ko = "madgolem"
        girl.label = "madgolem"
        girl.label_cg = "madgolem_cg"
        girl.info_rus_img = "madgolem_pedia"
        girl.m_pose = madgolem_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 38
        girl.hp = 3200
        girl.skills = ((3411, "Mud Spread"),
                       (3412, "Mud Draw"),
                       (3413, "Mud Shake"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 38,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# ARTM
########################################
    def get_artm():
        girl = Chr()
        girl.unlock = persistent.artm_unlock == 1
        girl.name = "Automata Girl"
        girl.pedia_name = "Automata Girl"
        girl.info = _("""An artificial monster created by magic science with no will of it's own, used for sexual play. Her though circuits are vastly superior to other artificially created monsters, but she still has no individual will. Programmed with expert level sexual techniques, her sexual skills far surpass any prostitute's. However due to the high cost of production, there exist only a few located in luxury hotels.
With her vagina specially created by the most advanced magic science available, the texture and sensations are at a level impossible for a normal human to compare to. In addition, the special material will never stretch or be damaged no matter how much it's used. In addition, both her hand and mouth are able to bring any man to a quick and powerful orgasm.
Of course, her source of power is semen. Due to her role, energy is never in short supply.""")
        girl.zukan_ko = "artm"
        girl.label = "artm"
        girl.label_cg = "artm_cg"
        girl.info_rus_img = "artm_pedia"
        girl.m_pose = artm_pose
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -33
        girl.x1 = 167
        girl.lv = 39
        girl.hp = 3000
        girl.skills = ((3414, "Mechanical Handjob"),
                   (3415, "Mechanical Blowjob"),
                   (3416, "Mechanical Breast Service"),
                   (3417, "Bubble Service Trance"),
                   (3418, "Thigh Service"),
                   (3419, "Anal Service"),
                   (3420, "Mechanical Electro-Footjob"),
                   (3421, "Service Program 666"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 38,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# ANT
########################################
    def get_ant():
        girl = Chr()
        girl.unlock = persistent.ant_unlock == 1
        girl.name = "Ant Girl"
        girl.pedia_name = "Ant Girl"
        girl.info = _("""An insect monster taking after an ant. Very closely related to the Hornet Girl, they share many characteristics. Part of a class based society, they are servants to their Queen. All men they capture are offered to their Queen first. Any man rejected by her is handed over to be used as the Ant Girls group sex slave. Of course since they are not fertile, they only rape the man for their own pleasure.
In addition, the Ant Girls working in Grangold are under the impression that their Queen's order is to "Obey all the humans". With that order in mind, they obediently work for the humans.""")
        girl.zukan_ko = "ant"
        girl.label = "ant"
        girl.label_cg = "ant_cg"
        girl.info_rus_img = "ant_pedia"
        girl.m_pose = ant_pose
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 39
        girl.hp = 3900
        girl.skills = ((3422, "Ant Kiss"),
                  (3423, "Double Multi-Arm Handjob"),
                  (3424, "Triple Multi-Arm Handjob"),
                  (3425, "Double Ant Tit Fuck"),
                  (3426, "Triple Ant Tit Fuck"),
                  (3427, "Double Ant Blowjob"),
                  (3428, "Triple Ant Blowjob"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 39,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# QUEENANT
########################################
    def get_queenant():
        girl = Chr()
        girl.unlock = persistent.queenant_unlock == 1
        girl.name = "Queen Ant"
        girl.pedia_name = "Queen Ant"
        girl.info = _("""A powerful monster that is Queen of the Ant Girls, she also boasts the most powerful magic of the insect monsters. She usually orders her Ant Girls to do her bidding, and remains in one place breeding. After finding a male with high quality genes, she will continue to reproduce with him for a very long time. With her body specialized in reproduction, the Queen Ant spends three fourths of her day copulating with the man. The other fourth is used for sleeping and eating. Since breeding is a biological imperative for her, she never wants to stop.
Due to her specialization in breeding, her sexual organs are highly developed and efficient at extracting semen from her mate. In addition, she is able to become pregnant using the semen of any male creature to ensure the continuity of her line. But breeding with other creatures is used only in an emergency, as humans produce the highest quality offspring.""")
        girl.zukan_ko = "queenant"
        girl.label = "queenant"
        girl.label_cg = "queenant_cg"
        girl.info_rus_img = "queenant_pedia"
        girl.m_pose = queenant_pose
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 50
        girl.hp = 5000
        girl.skills = ((3429, "Queen's Comfortable Mouth"),
                       (3430, "Queen's Soft Breasts"),
                       (3431, "Queen's Unmatched Skill"),
                       (3432, "Eyes of Seduction"),
                       (3433, "Queen's Stud"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 40,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MACCUBUS
########################################
    def get_maccubus():
        girl = Chr()
        girl.unlock = persistent.maccubus_unlock == 1
        girl.name = "Maccubus"
        girl.pedia_name = "Maccubus"
        girl.info = _("""A prostitute that lived in the Succubus Village before being turned into a Succubus. Succubus is the name given to monsters who have all of the traits and qualities befitting one of their family of monster. Maccubus is a special name given to Succubi who already have expert level sexual skills before their transformation.
Already quite skilled in pleasing men, her Succubus transformation drastically increased the quality of the pleasure she is able to bring. In addition, she is now able to employ the famous "Energy Drain" move to suck the energy from men. She's one who is already more powerful than many Succubi, even though just being transformed into one herself.""")
        girl.zukan_ko = "maccubus"
        girl.label = "maccubus"
        girl.label_cg = "maccubus_cg"
        girl.info_rus_img = "maccubus_pedia"
        girl.m_pose = maccubus_pose
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 6
        girl.x0 = -25
        girl.x1 = 175
        girl.lv = 40
        girl.hp = 3500
        girl.skills = ((3434, "Succubus Handjob"),
                       (3435, "Succubus Blowjob"),
                       (3436, "Succubus Tit Fuck"),
                       (3437, "Succubus Pussy"),
                       (3438, "Succubus Grind"),
                       (3439, "Heavenly Piston"),
                       (3440, "Energy Drain"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 40,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MINCCUBUS
########################################
    def get_minccubus():
        girl = Chr()
        girl.unlock = persistent.minccubus_unlock == 1
        girl.name = "Mincubus"
        girl.pedia_name = "Mincubus"
        girl.info = _("""A young girl that lived in the Succubus Village before being turned into a Succubus. She is given the name Minccubus due to her young age.
Although her sexual techniques are lacking, her childlike curiosity in how she plays with men makes her irresistible. Many men willingly sacrifice themselves to her, drained of all energy as she satisfies her sexual curiosity. Though she is a young girl, it would be dangerous to underestimate her.""")
        girl.zukan_ko = "minccubus"
        girl.label = "minccubus"
        girl.label_cg = "minccubus_cg"
        girl.info_rus_img = "minccubus_pedia"
        girl.m_pose = minccubus_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 7
        girl.x0 = -25
        girl.x1 = 175
        girl.lv = 38
        girl.hp = 2600
        girl.skills = ((3441, "Soft Handjob"),
                        (3442, "Wet Blowjob"),
                        (3443, "Silky Hairjob"),
                        (3444, "Face Riding"),
                        (3445, "Panty Stroke"),
                        (3446, "Sock Stroke"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 40,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# RENCCUBUS
########################################
    def get_renccubus():
        girl = Chr()
        girl.unlock = persistent.renccubus_unlock == 1
        girl.name = "Lencubus"
        girl.pedia_name = "Lencubus"
        girl.info = _("""A giri who lives in the {b}Succubus Village{/b}. Due to the erotic power, she was turned into a Succubus. Why is she called LEncubus? Because she can use the unusual move {b}Level Drain{/b}.
Though her normal abilities are pretty poor, her innate talent for {b}Level Drain{/b} is formidable. It's a powerful technique that even few mature Succubi are able to master. It takes the power (level) of her prey, and makes it her own. She does this by converting the prey's power (level) into energy form, and sucking it in so that it combines with her own. Though it may look like an ejaculation when the prey's power is being sucked, it is not semen. The cloudy liquid is actually a very condensed form of biological energy. Though it may feel like an ejaculation, the exhaustion or weakness that would usually accompany it is not present when afflicted by this technique. However, when the prey becomes weaker and the Lencubus becomes stronger, it can quickly become a dangerous situation.
There is a limit to the levels that can be absorbed by {b}Level Drain{/b}, so the user cannot become infinitely strong.""")
        girl.zukan_ko = "renccubus"
        girl.label = "renccubus"
        girl.label_cg = "renccubus_cg"
        girl.info_rus_img = "renccubus_pedia"
        girl.m_pose = renccubus_pose
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -25
        girl.x1 = 175
        girl.lv = 22
        girl.hp = 1200
        girl.skills = ((3447, "Lust Powered Handjob"),
                        (3448, "Lust Powered Blowjob"),
                        (3449, "Lust Powered Tit Fuck"),
                        (3450, "Level Drain"),
                        (3451, "Energy Drain"),
                        (3452, "Life Killing Vagina"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 41,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SUCCUBUS
########################################
    def get_succubus():
        girl = Chr()
        girl.unlock = persistent.succubus_unlock == 1
        girl.name = "Succubus"
        girl.pedia_name = "Succubus"
        girl.info = _("""A mature Succubus, she appears to be quite high ranking. Proud of her sexual prowess and control of her body, she is fond of showing off her abilities in front of the newly transformed Succubi. Many adventurers faced against her are likely to be squeezed to death.
Living most of her life as a wanderer, she seems to have settled down in Succubus Village disguised as a human. As the Nias faith spread, more and more humans became reluctant to willingly have sex with Succubi. Due to that, she was forced into hiding, awaiting the day when Succubi are able to roam freely and have fun with men whenever they want.""")
        girl.zukan_ko = "succubus"
        girl.label = "succubus"
        girl.label_cg = "succubus_cg"
        girl.info_rus_img = "succubus_pedia"
        girl.m_pose = succubus_pose
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 43
        girl.hp = 4500
        girl.skills = ((3453, "Delightful Handjob"),
                       (3454, "Hecto-Deep Blowjob"),
                       (3455, "Breast Squeeze"),
                       (3456, "Tail Drain"),
                       (3457, "Melting Kiss"),
                       (3458, "Energy Drain"),
                       (3459, "Infinite Pussy"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 41,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# WITCHS
########################################
    def get_witchs():
        girl = Chr()
        girl.unlock = persistent.witchs_unlock == 1
        girl.name = "Succubus Witch"
        girl.pedia_name = "Succubus Witch"
        girl.info = _("""A powerful Succubi who, while not holding too much erotic power, boasts very high magical power. Due to her powerful magic, she was put in charge of reviving the Legendary Succubus Lilith.
A rather troublesome opponent, she infuses her gloves and hat with her magical energy, and lets them operate as if they had their own will. Because of her high magical potential, she is a voracious eater when she catches a man. Along with her seemingly bottomless appetite is her greed of knowledge, which causes her to experiment with the man as she feasts. She is not cold-blooded, but is considered very dangerous.""")
        girl.zukan_ko = "witchs"
        girl.label = "witchs"
        girl.label_cg = "witchs_cg"
        girl.info_rus_img = "witchs_pedia"
        girl.m_pose = witchs_pose
        girl.facenum = 4
        girl.posenum = 3
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 41
        girl.hp = 4200
        girl.echi = ["witchs_ha", "witchs_hb"]
        girl.skills = ((3460, "Milking Gloves"),
                         (3461, "Hat of Pleasure"),
                         (3462, "Breast Milking"),
                         (3463, "HP Tail Drain"),
                         (3464, "Lvl Tail Drain"),
                         (3465, "Melting Kiss"),
                         (3466, "Eyes of Seduction"),
                         (3467, "Energy Drain"),
                         (3598, "Minimum Phantasm: Heaven"),
                         (3599, "Minimum Phantasm: Hell"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 41,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# LILITH
########################################
    def get_lilith():
        girl = Chr()
        girl.unlock = persistent.lilith_unlock == 1
        girl.name = "Lilith & Lilim"
        girl.pedia_name = "Lilith & Lilim"
        girl.info = _("""Legendary Succubus Sisters that were sealed 500 years ago by a Hero. Incredibly strong Succubi, they are treated by other Succubi as their savior.
Top-class Succubi, they work together to indulge a man in ecstasy. Entangling their bodies together with the man's, they take turns mating with him all while rubbing their bodies against his.
To tell them apart, the older sister with smaller breasts is Lilith, and the younger sister with larger breasts is Lilim. The two consider themselves of the same flesh, and cannot bear to part with the other for longer than a few moments.""")
        girl.zukan_ko = "lilith"
        girl.label = "lilith"
        girl.label_cg = "lilith_cg"
        girl.info_rus_img = "lilith_pedia"
        girl.m_pose = lilith_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -62
        girl.x1 = 138
        girl.lv = 55
        girl.hp = 6000
        girl.skills = ((3468, "Laughing Gend"),
                         (3469, "Tin Cyrus"),
                         (3470, "Melo Sophia"),
                         (3471, "Ritis Ani"),
                         (3472, "Twin Tail Drain"),
                         (3473, "Twin Energy Drain"),
                         (3474, "Chaos Pleasure"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 42,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MADAMINSECT
########################################
    def get_madaminsect():
        girl = Chr()
        girl.unlock = persistent.madaminsect_unlock == 1
        girl.name = "Madame Insect"
        girl.pedia_name = "Madame Insect"
        girl.info = _("""An insect monster that usually takes the form of a human noblewoman. Living in Lady's Village, she attacks male travelers and sucks out their bodily fluids until they die.
When preying on a male, she forces their penis into a mouth located in her lower body.
Stimulating it, she forces them to ejaculate directly into her, feeding her. Sucking out all of their body fluids, she continues this until the male is dead.
An extremely sadistic monster, she derives great pleasure from watching the struggling male slowly weaken as he ejaculates into her. Contrary to her refined look, she is extremely dangerous.""")
        girl.zukan_ko = "madaminsect"
        girl.label = "madaminsect"
        girl.label_cg = "madaminsect_cg"
        girl.info_rus_img = "madaminsect_pedia"
        girl.m_pose = madaminsect_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 42
        girl.hp = 3200
        girl.skills = ((3475, "Lady's Masturbation"),
                              (3476, "Lady's Blowjob"),
                              (3477, "Lady's Tit Fuck"),
                              (3478, "Fluid Sucking"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 42,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MADAMUMBRELLA
########################################
    def get_madamumbrella():
        girl = Chr()
        girl.unlock = persistent.madamumbrella_unlock == 1
        girl.name = "Madame Umbrella"
        girl.pedia_name = "Madame Umbrella"
        girl.info = _("""A monster with a soft body of the Scylla type, she usually keeps herself disguised as a human. A mix between jellyfish and squid, she uses her tentacles to squeeze her prey of semen.
When transformed, her Umbrella is actually part of her body. Any man that strays too close is grabbed by tentacles that sprout from there.
Proud of the control she can exert over her catch, she enjoys forcing men to ejaculate from simulations other than just their penis. After toying with her catch for a while, she will continue on with every available stimulation source until they are squeezed to death.""")
        girl.zukan_ko = "madamumbrella"
        girl.label = "madamumbrella"
        girl.label_cg = "madamumbrella_cg"
        girl.info_rus_img = "madamumbrella_pedia"
        girl.m_pose = madamumbrella_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 43
        girl.hp = 3400
        girl.skills = ((3479, "Tentacle Play"),
                                (3480, "Sucker Play"),
                                (3481, "Sensual Anal Spin"),
                                (3482, "Lady's Amusement"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 43,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# MAIDSCYULLA
########################################
    def get_maidscyulla():
        girl = Chr()
        girl.unlock = persistent.maidscyulla_unlock == 1
        girl.name = "Scylla Maid"
        girl.pedia_name = "Scylla Maid"
        girl.info = _("""A type of Scylla, she is the acting maid at Cassandra's mansion. She has multiple tentacles that sprout from her lower body, and has a giant mouth located underneath her that she uses for predatory purposes.
An extremely sadistic monster that disguises her cruel treatment as "service" for the male. Ignoring anything her "guest" tells her, she uses her tentacles to torture them as she looks on in joy. After breaking her catch's spirit, she will force him to orgasm a couple times before finally preying on them. At times she is known to force the catch to ejaculate inside her mouth, just to get a final laugh at their pathetic display before eating them whole.""")
        girl.zukan_ko = "maidscyulla"
        girl.label = "maidscyulla"
        girl.label_cg = "maidscyulla_cg"
        girl.info_rus_img = "maidscyulla_pedia"
        girl.m_pose = maidscyulla_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 43
        girl.hp = 4000
        girl.skills = ((3483, "Service: Hand"),
                              (3484, "Service: Mouth"),
                              (3485, "Service: Tentacle"),
                              (3486, "Service: Holding"),
                              (3487, "Service: Sucking"),
                              (3488, "Service: Feeding"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 43,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# EMILY
########################################
    def get_emily():
        girl = Chr()
        girl.unlock = persistent.emily_unlock == 1
        girl.name = "Emily"
        girl.pedia_name = "Emily"
        girl.info = _("""Daughter to Cassandra, she's a monster from the Scylla line. Though she is still a young girl, she boasts both powerful magical and physical ability.
Despite her carefree manner, she is surprisingly cruel. She loves to use her tentacles to play with men until they die. It's unknown how many have been killed by Emily. Using her tentacles to bind the male, she enjoys playing with them as she watches their agonized expressions. This will continue until Emily grows bored of them, where she will then quickly finish them off.
Her mother, Cassandra, has the genes of many different families of monsters within her. The Scylla genes took dominant control in Emily during Cassandra's pregnancy."""
)
        girl.zukan_ko = "emily"
        girl.label = "emily"
        girl.label_cg = "emily_cg"
        girl.info_rus_img = "emily_pedia"
        girl.m_pose = emily_pose
        girl.facenum = 4
        girl.posenum = 4
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 52
        girl.hp = 5000
        girl.skills = ((3489, "Emily's Handjob"),
                        (3490, "Emily's Blowjob"),
                        (3491, "Emily's Toy"),
                        (3492, "Twin Tail Rondo"),
                        (3493, "Tentacle Waltz"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 43,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# CASSANDRA
########################################
    def get_cassandra():
        girl = Chr()
        girl.unlock = persistent.cassandra_unlock == 1
        girl.name = "Cassandra"
        girl.pedia_name = "Cassandra"
        girl.info = _("""The Lord of Lady's Village, she also competed against Alipheese the 15th for the Monster Lord's throne. Extremely strong, she is one of the most powerful monsters in the world. It appears as though she can also trace her lineage far down the line to the Fateburn family.
An extremely cold-blooded monster, she preys on any man to come into her village. Her victims are believed to be in the many hundreds.
Holding the genes of many different monster lines in her, it seems as though she can produce offspring of many different types. In addition, she is able to control her flesh freely. Using that, she can form her digestive organ outside of her own body, and watch her prey be digested with her own eyes. Wrapping up her prey in her organ, Cassandra usually forces them to multiple orgasms, until they have no semen left. After that, she enjoys watching them as they slowly dissolve away.""")
        girl.zukan_ko = "cassandra"
        girl.label = "cassandra"
        girl.label_cg = "cassandra_cg"
        girl.info_rus_img = "cassandra_pedia"
        girl.m_pose = cassandra_pose
        girl.facenum = 5
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 110
        girl.hp = 18500
        girl.echi = ["cassandra_ha", "cassandra_hb"]
        girl.skills = ((3494, "Corrosive Prelude"),
                            (3495, "Flesh Waltz"),
                            (3496, "Melting Rondo"),
                            (3497, "Digestive Tango"),
                            (3498, "Sticky Symphony"),
                            (3499, "Black and White Concerto"),
                            (3500, "Dissolving Sonata"),
                            (3501, "Prey's Requiem"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 44,
        "skill01": 11,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# YOUGAN
########################################
    def get_yougan():
        girl = Chr()
        girl.unlock = persistent.yougan_unlock == 1
        girl.name = "Lava Girl"
        girl.pedia_name = "Lava Girl"
        girl.info = _("""A monster who lives in a volcano, with her body comprised mainly of magma. A subspecies of the Slime family, she is able to freely manipulate her body.
With semen as her energy source, she wraps the males body in her magma when she locates them. Naturally, she lowers her body temperature so as not to kill the male. The preferred temperature is the same as a human female's vagina. Holding her prey like that, she will spin her magma around his penis, forcing him to multiple orgasms.
There are very few adventurers who travel near the volcano, so prey is extremely precious. Due to that, the Lava Girl is very careful not to let any precious prey die. She will hold him inside her body in an area equivalent to her womb, and ensure he's perfectly safe until his natural lifespan runs out.""")
        girl.zukan_ko = "yougan"
        girl.label = "yougan"
        girl.label_cg = "yougan_cg"
        girl.info_rus_img = "yougan_pedia"
        girl.m_pose = yougan_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 44
        girl.hp = 4800
        girl.skills = ((3502, "Magma Shaft"),
                         (3503, "Magma Draw"),
                         (3504, "Magma Bath"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 44,
        "skill01": 12,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# BASILISK
########################################
    def get_basilisk():
        girl = Chr()
        girl.unlock = persistent.basilisk_unlock == 1
        girl.name = "Basilisk"
        girl.pedia_name = "Basilisk"
        girl.info = _("""A powerful monster that is able to petrify, with a mix of reptile and bird characteristics. They are almost never seen around human towns, and prefer volcanoes or mountains. In addition, they are unable to fly in the sky.
They are able to not only petrify humans with their magical eyes, but through their saliva. It is very dangerous to let them either lick you or have sex with you. They are known to be very cruel, and sometimes turn humans to stone for fun. Notably, they are known to petrify the male's penis and rape him, enjoying the permanent rock hard sensations.
They use the semen they squeeze out for both feeding and reproductive purposes. However their main purpose in raping males seems to be simply for their own enjoyment.""")
        girl.zukan_ko = "basilisk"
        girl.label = "basilisk"
        girl.label_cg = "basilisk_cg"
        girl.info_rus_img = "basilisk_pedia"
        girl.m_pose = basilisk_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 45
        girl.hp = 5200
        girl.skills = ((3505, "Bird Handjob"),
                           (3506, "Ample Tit Fuck"),
                           (3507, "Petrifying Sucking"),
                           (3508, "Stone Rape"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 45,
        "skill01": 12,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# DRAGON
########################################
    def get_dragon():
        girl = Chr()
        girl.unlock = persistent.dragon_unlock == 1
        girl.name = "Dragon Girl"
        girl.pedia_name = "Dragon Girl"
        girl.info = _("""One of the most famous monsters in the world, Dragons are top of the list when it comes to being inherently powerful. However very few adventurers ever meet one, due to their preference for very secluded areas. There are many different types of Dragon monsters. This specific one has her human part protected inside her large mouth. As a family of Dragons breeds more with humans, their children slowly begin to look more humanoid. Those more human-like Dragons are known as Dragonkin.
Their scales are tough, and their fangs and horns are sharp. Their base offensive ability is far higher than any other monster type. In addition, their intelligence seems to be higher than average, too.
Any prey they catch will be used to satisfy both their sexual appetite and their hunger. Due to their seclusion, they prefer to savor the prey as they slowly eat them.""")
        girl.zukan_ko = "dragon"
        girl.label = "dragon"
        girl.label_cg = "dragon_cg"
        girl.info_rus_img = "dragon_pedia"
        girl.m_pose = dragon_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 50
        girl.hp = 6000
        girl.skills = ((3509, "Dragon Hand"),
                         (3510, "Dragon Mouth"),
                         (3511, "Dragon Breasts"),
                         (3512, "Dragon Tongue"),
                         (3513, "Oral Fixation"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 45,
        "skill01": 12,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# SALAMANDER
########################################
    def get_salamander():
        girl = Chr()
        girl.unlock = persistent.salamander_unlock == 1
        girl.name = "Salamander"
        girl.pedia_name = "Salamander"
        girl.info = _("""The spirit of flame has a very prideful and wild disposition. Living quietly in the Gold Region volcano, she doesn't interact much with the outside world. However she's famous in the monster world, and many come to her hoping to be made into her apprentice. Salamander seems to mostly decline, though.
Anyone who dares challenge her is relentlessly attacked without a shred of mercy. If the challenger is a man, she also enjoys humiliating them afterwards. Able to control her body heat at will, she creates a relaxing warmth forcing the man to ejaculate whether he wants to or not. Not fond of killing, she usually leaves the men on the brink of death outside the volcano after she finishes humiliating them.
She seems willing to promise her undying loyalty to someone worthy. But getting the prideful Salamander to acknowledge you is no easy task.""")
        girl.zukan_ko = "salamander"
        girl.label = "salamander"
        girl.label_cg = "salamander_cg"
        girl.info_rus_img = "salamander_pedia"
        girl.m_pose = salamander_pose
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 85
        girl.hp = 16000
        girl.skills = ((3514, "Anti Fire Fist"),
                             (3515, "Fire Spirit's Foot"),
                             (3516, "Fire Spirit's Embrace"),
                             (3517, "Flame Steiner"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 45,
        "skill01": 12,
        "skill02": 2,
        "skill03": 2,
        "item01": 9
        }
        return girl

########################################
# GRANBERIA3
########################################
    def get_granberia3():
        girl = Chr()
        girl.unlock = persistent.granberia3_unlock == 1
        girl.name = "Granberia"
        girl.pedia_name = "Granberia (3)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = "granberia3"
        girl.label = "granberia3"
        girl.label_cg = "granberia3_cg"
        girl.info_rus_img = "granberia3_pedia"
        girl.m_pose = granberia3_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 24000
        girl.scope = {"ruka_tati": 1,
        "mylv": 46,
        "skill01": 12,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# KANI2
########################################
    def get_kani2():
        girl = Chr()
        girl.unlock = persistent.kani2_unlock == 1
        girl.name = "Crab Girl"
        girl.pedia_name = "Crab Girl (2)"
        girl.info = _("""A monster that roams near the sandy beaches close to Port Natalia. With her fighting spirit ignited by the loss against Luka, she trained herself nonstop to challenge him to a rematch. Due to her strict training, her combat abilities were drastically improved since her last met her.
In addition, it seems as though she has been regularly catching every single man to appear in her territory, and giving them a thorough penis washing. As reports of these washes spread among the local populace, the sandy beaches of Natalia have become a popular spot for men to travel alone to."""
)
        girl.zukan_ko = "kani2"
        girl.label = "kani2"
        girl.label_cg = "kani2_cg"
        girl.info_rus_img = "kani2_pedia"
        girl.m_pose = kani2_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 46
        girl.hp = 2200
        girl.skills = ((3518, "Bubble Bubble"),
                        (3519, "Bubble Wash Heaven"),
                        (3520, "Bubble Shake Hell"),
                        (3521, "Special Bubble Handjob"),
                        (3522, "Full Body Bubble Hell"),
                        (3523, "Hellish Penis Washing"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 46,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# DAGON
########################################
    def get_dagon():
        girl = Chr()
        girl.unlock = persistent.dagon_unlock == 1
        girl.name = "Dagon"
        girl.pedia_name = "Dagon"
        girl.info = _("""A monster that lives in the Northern Sea. Posing as a mermaid, she catches careless men who dare to get too close to her. Her mimicry isn't that great, but due to the obfuscation of the sea water, some men have been known to be tricked. In addition, due to living so close to the northern continent, she is quite a powerful monster.
Once she catches a man, she uses her lower body to wrap around him, squeezing out his semen. An overly happy, innocent monster, she has been known to squeeze men to death with a carefree smile on her face."""
)
        girl.zukan_ko = "dagon"
        girl.label = "dagon"
        girl.label_cg = "dagon_cg"
        girl.info_rus_img = "dagon_pedia"
        girl.m_pose = dagon_pose
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 46
        girl.hp = 4600
        girl.skills = ((3528, "Squirm Squirm"),
                        (3529, "Suck Suck"),
                        (3530, "Squish Squish"),
                        (3531, "Forcible Semen Squeeze"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 46,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }

        return girl

########################################
# POSEIDONES
########################################
    def get_poseidones():
        girl = Chr()
        girl.unlock = persistent.poseidones_unlock == 1
        girl.name = "Poseidoness"
        girl.pedia_name = "Poseidoness"
        girl.info = _("""The Queen over the Northern Sea. Proud of her physical and magical abilities, she is even stronger than the Kraken Queen of the Southern Seas. Though she is strict, she holds no cruelty or malice. Ruling over the northern half of the seas, she metes out suitable punishments to those that disturb the peace.
Though she's able to live on fish and seaweed, she enjoys the taste of human semen. On the occasions when she can enjoy some, she skillfully uses her tentacles to squeeze it out. She does not attack men, however. Her milking is reserved for criminals of the sea, and those who challenge her and lose.""")
        girl.zukan_ko = "poseidones"
        girl.label = "poseidones"
        girl.label_cg = "poseidones_cg"
        girl.info_rus_img = "poseidones_pedia"
        girl.m_pose = poseidones_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 65
        girl.hp = 7500
        girl.skills = ((3535, "Tentacle Caress"),
                             (3536, "Tentacle Raid"),
                             (3537, "Tentacle Smash"),
                             (3538, "Bound Caress"),
                             (3539, "Bound Raid"),
                             (3540, "Abyss Drain"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 47,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# SEIREN
########################################
    def get_seiren():
        girl = Chr()
        girl.unlock = persistent.seiren_unlock == 1
        girl.name = "Siren"
        girl.pedia_name = "Siren"
        girl.info = _("""A powerful bird monster that roams the Northern Sea. Using her seductive voice, she tempts sailors to her, and then extracts all of their semen. Due to the potential for loss of life, experienced sailors usually travel with earplugs.
Though they usually act alone, Sirens have been known to wait in groups on rocks to attack gigantic ships. With her high quality vagina and seductive voice, the Siren can easily overpower a man in ecstasy in mere moments. In trade for this pleasure, the man is milked until his death.""")
        girl.zukan_ko = "seiren"
        girl.label = "seiren"
        girl.label_cg = "seiren_cg"
        girl.info_rus_img = "seiren_pedia"
        girl.m_pose = seiren_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 48
        girl.hp = 5200
        girl.skills = ((3541, "Feather Caress"),
                         (3542, "Feather Wingjob"),
                         (3543, "Chest of Pleasure"),
                         (3544, "Mouth of Fascination"),
                         (3545, "Armpit Squeeze"),
                         (3546, "Thirsting Vagina"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 48,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }

        return girl

########################################
# HITODE
########################################
    def get_hitode():
        girl = Chr()
        girl.unlock = persistent.hitode_unlock == 1
        girl.name = "Starfish Girl"
        girl.pedia_name = "Starfish Girl"
        girl.info = _("""A monster that resides in the Northern Sea. Due to living so close to the northern continent, they are quite powerful. Due to this particular Starfish Girl being trapped in a barrier for so many years, her solo training has increased her power even more.
Her staple food is male semen and flesh. Once caught in her body, there are very few men capable of escaping. With her back foot, she tickles the man's body to stimulate him as she forces his penis into her. After sucking up his semen, she brings her stomach out of her own body to begin digesting her catch's body. As he weakens from the digestion process, the Starfish Girl slowly brings him inside her.""")
        girl.zukan_ko = "hitode"
        girl.label = "hitode"
        girl.label_cg = "hitode_cg"
        girl.info_rus_img = "hitode_pedia"
        girl.m_pose = hitode_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 48
        girl.hp = 4800
        girl.skills = ((3524, "Star Blowjob"),
                         (3525, "Star Caress"),
                         (3526, "Full-Body Foot Caress"),
                         (3527, "Penis Foot Caress"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 48,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# BEELZEBUB
########################################
    def get_beelzebub():
        girl = Chr()
        girl.unlock = persistent.beelzebub_unlock == 1
        girl.name = "Beelzebub"
        girl.pedia_name = "Beelzebub"
        girl.info = _("""An insect type monster that was sealed in ancient times by the first Monster Lord. An ancient monster that belongs to a different evolutionary tree of monsters, different from the current generation. An incredibly powerful monster, she was revered as a God in ancient times. Many were sealed at first, but the vast majority have died over the years. Only three of the strongest have managed to survive to the present day.
Their strength is formidable, but it's their reproductive ability that is truly terrifying. Mating with a single man for a year straight, they can give birth to 10,000 daughters. Keeping the man as a reproductive tool, she keeps his penis in her abdomen, keeping them in a trance of pleasure to ensure continual insemination. Inserting the eggs into the man's rectum after fertilization, he is treated as nothing but a reproductive tool.""")
        girl.zukan_ko = "beelzebub"
        girl.label = "beelzebub"
        girl.label_cg = "beelzebub_cg"
        girl.info_rus_img = "beelzebub_pedia"
        girl.m_pose = beelzebub_pose
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 4
        girl.x0 = -100
        girl.x1 = 100
        girl.lv = 65
        girl.hp = 8000
        girl.skills = ((3554, "Flies of Purgatory"),
                            (3555, "Triple Breast Crush"),
                            (3556, "Monster Fly Suck"),
                            (3557, "Black Sabbath"),
                            (3603, "Monster Fly Handjob"),
                            (3661, "Monster Fly Breast Squeeze"),
                            (3662, "Monster Fly Blowjob"),
                            (3558, "Breast Jail"),
                            (3587, "Breast Prison"),
                            (3588, "Breast Confinement"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 48,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# TRICKFAIRY
########################################
    def get_trickfairy():
        girl = Chr()
        girl.unlock = persistent.trickfairy_unlock == 1
        girl.name = "Trick Fairy"
        girl.pedia_name = "Trick Fairy"
        girl.info = _("""A powerful, high ranking fairy. However, like other fairies, she still retains her mischievous, innocent nature.
Very curious about sex, she wants to try having sex with a human, despite her small size. Though her body is barely larger than a penis, her stretchy vagina is able to take and clamp down around a man. After shaking her body as hard as she can to make him come, the semen largely spills out of her due to her tiny body.
In addition, she's very possessive, making the possibility of the man being kept very high. Though she appears innocent and friendly, do not be underestimate this dangerous monster.""")
        girl.zukan_ko = "trickfairy"
        girl.label = "trickfairy"
        girl.label_cg = "trickfairy_cg"
        girl.info_rus_img = "trickfairy_pedia"
        girl.m_pose = trickfairy_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -58
        girl.x1 = 142
        girl.lv = 49
        girl.hp = 5400
        girl.skills = ((3559, "Trick Hand"),
                             (3560, "Trick Chest"),
                             (3561, "Trick Mouth"),
                             (3562, "Soft Butt"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 49,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# QUEENFAIRY
########################################
    def get_queenfairy():
        girl = Chr()
        girl.unlock = persistent.queenfairy_unlock == 1
        girl.name = "Queen Fairy"
        girl.pedia_name = "Queen Fairy"
        girl.info = _("""The most powerful Fairy in the world, both her magical power and synergy with nature is very high. Able to use plants as if they were her own hands, the Queen Fairy uses the forest as her own sexual organ. Originally she did not attack humans, but her anger has brought her to the point where she's now antagonistic. Unlike her carefree Fairy brethren, she appears to have a strong sense of responsibility as their Queen.
Any human captured by her is forced to experience the anger of the forest through her. Any semen squeezed out from a man is treated as nourishment for the forest. In addition, she seems to be good friends with the Queen Elf.""")
        girl.zukan_ko = "queenfairy"
        girl.label = "queenfairy"
        girl.label_cg = "queenfairy_cg"
        girl.info_rus_img = "queenfairy_pedia"
        girl.m_pose = queenfairy_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 60
        girl.hp = 9500
        girl.skills = ((3570, "Ivy Wand"),
                             (3571, "Ivy Draw"),
                             (3572, "Sucking Ivy"),
                             (3573, "Fanatic Dahlia"),
                             (3574, "Heaven's Dance"),
                             (3575, "Ecstasy Incense"),
                             (3563, "Ivy Tighten"),
                             (3564, "Ivy Caress"),
                             (3576, "Mellow Rafflesia"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 50,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# QUEENELF
########################################
    def get_queenelf():
        girl = Chr()
        girl.unlock = persistent.queenelf_unlock == 1
        girl.name = "Queen Elf"
        girl.pedia_name = "Queen Elf"
        girl.info = _("""The Queen of all the world's Elves. Though she doesn't enjoy fighting, her physical and magical abilities are top class. She originally avoided fighting, but has started to deeply hate humans. Due to that, she fights against humans without any mercy.
Her sexual skills are extremely powerful, and are believed to be on the same level as a high-class Succubus's. With her extensive knowledge and abilities, she is widely known as an expert in vaginal skills among the monster races. Any man caught by her will have their semen squeezed without exception.""")
        girl.zukan_ko = "queenelf"
        girl.label = "queenelf"
        girl.label_cg = "queenelf_cg"
        girl.info_rus_img = "queenelf_pedia"
        girl.m_pose = queenelf_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 60
        girl.hp = 10000
        girl.skills = ((3577, "Elven Hand Squeeze"),
                           (3578, "Elven Mouth Melt"),
                           (3579, "Elven Breast Drowning"),
                           (3581, "Kiss of Ecstasy"),
                           (3582, "Elven Queen's Vagina"),
                           (3580, "Heavenly Thrusting"),
                           (3600, "Hellishly Tight Vagina"),
                           (3601, "Infinite Sucking"),
                           (3663, "Energy Drain"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 51,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# SARAEVIL
########################################
    def get_saraevil():
        girl = Chr()
        girl.unlock = persistent.saraevil_unlock == 1
        girl.name = "Sara (Succubization)"
        girl.pedia_name = "Sara (Succubization)"
        girl.info = _("""The Princess of Sabasa, after being corrupted by the Queen Elfs magic power. In addition to the power of a Succubus, she also awoke the Sphinx's blood that still ran through her veins.
Due to that combination, her power even surpasses the Queen Elfs.
Her human memory has all remained, but her thoughts and desires have become confused due to the sudden drastic increase in power. Overcome by her own desires, she has turned into an incredibly dangerous monster. At the mere sight of a man, the desire to drain him dry takes over her human thoughts.
After enough time has passed, it's expected that her mind would have slowly returned to normal as she got used to controlling her power. But before that could happen, the amount of men drained dry would reach enormous numbers.""")
        girl.zukan_ko = "saraevil"
        girl.label = "saraevil"
        girl.label_cg = "saraevil_cg"
        girl.info_rus_img = "saraevil_pedia"
        girl.m_pose = saraevil_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 8
        girl.x0 = -200
        girl.lv = 70
        girl.hp = 12000
        girl.skills = ((3565, "Earth Mother's Handjob"),
                           (3566, "Isis's Lovemaking"),
                           (3567, "Hellish Blowjob"),
                           (3568, "Ecstasy Press"),
                           (3569, "Tail Drain"),
                           (3583, "Penis Choking Vagina"),
                           (3584, "Overwhelming Grind"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 52,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# WYVERN
########################################
    def get_wyvern():
        girl = Chr()
        girl.unlock = persistent.wyvern_unlock == 1
        girl.name = "Wyvern"
        girl.pedia_name = "Wyvern"
        girl.info = _("""A subspecies of the Dragon race, she is almost unmatched in aerial combat. Due to her known superiority in the sky, she was enlisted as an aerial defense soldier, to take down anyone attempting to enter the northern continent from the sky.
A ferocious monster, she will squeeze the semen out of her prey without any mercy. Raping the man as she flies through the air, her catch is left completely at her mercy due to the inevitable death from the fall. In addition, it appears as though she prefers to mate for reproductive purposes while flying through the clear sky. In most cases, the man is raped until his death.""")
        girl.zukan_ko = "wyvern"
        girl.label = "wyvern"
        girl.label_cg = "wyvern_cg"
        girl.info_rus_img = "wyvern_pedia"
        girl.m_pose = wyvern_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 55
        girl.hp = 5500
        girl.skills = ((3589, "Aerial Blowjob"),
                         (3590, "Violent Breast Rape"),
                         (3591, "Air Rape"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 52,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "item01": 9
        }
        return girl

########################################
# KYORYUU
########################################
    def get_kyoryuu():
        girl = Chr()
        girl.unlock = persistent.kyoryuu_unlock == 1
        girl.name = "Kyoryuu"
        girl.pedia_name = "Kyoryuu"
        girl.info = _("""A subspecies of the Dragon race with a gigantic body. Her movements are slow, and her intelligence is low, but her power is incredible. A common human being cannot hope to fight against her.
When fortunate enough to find a human male, she will suck on him in her giant mouth, extracting his semen. Though her tongue is gigantic, she can skillfully control it to easily make the man ejaculate inside her mouth.
Though she is surprisingly gentle and quiet, she is still a carnivorous monster. She's a dangerous monster, but is only known to inhabit the northern continent.""")
        girl.zukan_ko = "kyoryuu"
        girl.label = "kyoryuu"
        girl.label_cg = "kyoryuu_cg"
        girl.info_rus_img = "kyoryuu_pedia"
        girl.m_pose = kyoryuu_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 53
        girl.hp = 10000
        girl.skills = ((3667, "Sticky Saliva"),
                          (3668, "Wet Lick"),
                          (3592, "Full-Body Lick"),
                          (3593, "Full-Body Suck"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 53,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# C_BEAST
########################################
    def get_c_beast():
        girl = Chr()
        girl.unlock = persistent.c_beast_unlock == 1
        girl.name = "Chimera Beast"
        girl.pedia_name = "Chimera Beast"
        girl.info = _("""A strange beast type monster, thought to be a mutation. Where she came from, and whether she has any emotions, is still as of yet unclear.
Thought she appears to have no thoughts or feelings, the Chimera Beast is still just as agile and instinctual as a hunting beast may be. Though it speaks in an intelligible language, her speech pattern is unnatural.
After catching her prey, she uses her long tongue and sexual skills to extract his semen. In addition, she is able to force him to the ground and forcibly rape him. Not much else is known about this mysterious monster.""")
        girl.zukan_ko = "c_beast"
        girl.label = "c_beast"
        girl.label_cg = "c_beast_cg"
        girl.info_rus_img = "c_beast_pedia"
        girl.m_pose = c_beast_pose
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 54
        girl.hp = 8500
        girl.skills = ((3594, "Double Breast Rape"),
                          (3595, "Tongue Splash"),
                          (3596, "Vacuum Tail"),
                          (3597, "Piston Rush"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 54,
        "skill01": 14,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# C_DRYAD_VORE
########################################
    def get_c_dryad_vore():
        girl = Chr()
        girl.unlock = persistent.c_dryad_vore_unlock == 1
        girl.name = "Chimera Dryad Vore"
        girl.pedia_name = "Chimera Dryad Vore"
        girl.info = _("""A strange plant type monster that appeared in the ruins of Remina. With the core of a human female, it appears as though parasitic plants have taken over most of her body. The only thing stranger than her appearance is the questions around her origin.
Using her parasitic plants, she emotionlessly feeds on her prey. She eats both humans and monsters, but will extract a male's semen before digesting them. It is not possible to escape once trapped by her. All that awaits is a horrible fate of being devoured alive by plants.""")
        girl.zukan_ko = "c_dryad_vore"
        girl.label = "c_dryad_vore"
        girl.label_cg = "c_dryad_vore_cg"
        girl.info_rus_img = "c_dryad_vore_pedia"
        girl.m_pose = c_dryad_vore_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 80
        girl.hp = 16000
        girl.echi = ["c_dryad_vore_ha", "c_dryad_vore_hb", "c_dryad_vore_hc"]
        girl.skills = ((3604, "Ivy Digestion"),
                       (3605, "Penis Devouring Flytrap"),
                       (3606, "Semen Slurping Pitcher"),
                       (3607, "Intoxicating Aroma"),
                       (3608, "Melty Ivy"),
                       (3609, "Venus Trap of Pleasure"),
                       (3610, "Digestive Pitcher"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 55,
            "skill01": 14,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# VAMPIRE
########################################
    def get_vampire():
        girl = Chr()
        girl.unlock = persistent.vampire_unlock == 1
        girl.name = "Vampire"
        girl.pedia_name = "Vampire"
        girl.info = _("""A high-level Vampire who wields powerful magic who resupplies her own energy through human semen and blood. She usually resides in the northern continent, but is known to travel south and attack the Gold Region when she gets hungry. When she goes south, nearly 100 humans end up attacked by her. If she attacks a female, she sucks their blood and makes them her servant. If it's a man, she rapes him as she sucks out his blood. In addition, her mantle is an extension of her body, able to squeeze out and absorb semen.
With no human soldier able to stand against her, she's treated almost like a natural disaster. When the Vampire appears, the nearby humans can only pray to Nias until she's finished.""")
        girl.zukan_ko = "vampire"
        girl.label = "vampire"
        girl.label_cg = "vampire_cg"
        girl.info_rus_img = "vampire_pedia"
        girl.m_pose = vampire_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -25
        girl.x1 = 175
        girl.lv = 56
        girl.hp = 9000
        girl.skills = ((3617, "Vampire Bust"),
                          (3618, "Semen Drain"),
                          (3619, "Bloodsucking"),
                          (3620, "Dress Drain"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 56,
            "skill01": 14,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# BEHEMOTH
########################################
    def get_behemoth():
        girl = Chr()
        girl.unlock = persistent.behemoth_unlock == 1
        girl.name = "Behemoth"
        girl.pedia_name = "Behemoth"
        girl.info = _("""A huge, famous monster. Though she's famous, there are almost no humans who have actually seen her due to her reclusive habitat on the northern continent. An incredibly powerful monster, even the most experienced of soldiers can't stand against her in a match of physical prowess. Besides her incredibly physical strength, she can also bind her catch with her tail, and slowly violate them as she pleases.
When catching a man, she will rape him without mercy. If she likes him, she will even mate with him for reproductive purposes. In that case, she will bring him back to her den and keep him as her personal sex slave.""")
        girl.zukan_ko = "behemoth"
        girl.label = "behemoth"
        girl.label_cg = "behemoth_cg"
        girl.info_rus_img = "behemoth_pedia"
        girl.m_pose = behemoth_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -230
        girl.x1 = -30
        girl.lv = 56
        girl.hp = 10000
        girl.skills = ((3611, "Behemoth Shake"),
                           (3612, "Behemoth Suck"),
                           (3613, "Behemoth Breasts"),
                           (3614, "Behemoth Torture"),
                           (3615, "Behemoth Draw"),
                           (3616, "Animal Rape"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 56,
            "skill01": 14,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# ESUCCUBUS
########################################
    def get_esuccubus():
        girl = Chr()
        girl.unlock = persistent.esuccubus_unlock == 1
        girl.name = "Elder Succubus"
        girl.pedia_name = "Elder Succubus"
        girl.info = _("""Considered powerful even among the most powerful of Succubi, she is at the peak of her race in ability. With her intense magical power and seductive skills, normal humans are unable to remain sane when in her presence. Losing their reason, they quickly descend into complete carnal desire, desiring only to be raped by her.
The Elder Succubus uses her entire body as a weapon to squeeze her prey completely dry. Her hands, mouth, feet, vagina, thighs... Even her bellybutton can easily force a man to come. Her bellybutton in particular is able to give a unique, unbearable pleasure second only to her vagina.
Incredibly prideful, she is coldhearted when it comes to her prey. Once she has forced a man to orgasm once, there is no future left for him but to become her prey.""")
        girl.zukan_ko = "esuccubus"
        girl.label = "esuccubus"
        girl.label_cg = "esuccubus_cg"
        girl.info_rus_img = "esuccubus_pedia"
        girl.m_pose = esuccubus_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -12
        girl.x1 = 188
        girl.lv = 57
        girl.hp = 9500
        girl.skills = ((3621, "Semen Squeezecraft"),
                            (3622, "Male Dominating Footcraft"),
                            (3623, "Draining Blowjob"),
                            (3624, "Male Squeezing Titcraft"),
                            (3625, "Kiss of Ecstasy"),
                            (3626, "Energy Drain"),
                            (3627, "Voracious Monster Bellybutton"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 57,
            "skill01": 15,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# HATIBI
########################################
    def get_hatibi():
        girl = Chr()
        girl.unlock = persistent.hatibi_unlock == 1
        girl.name = "Yao"
        girl.pedia_name = "Yao"
        girl.info = _("""An incredibly powerful monster, Yao is near the top of all monsters in physical and magical abilities. Together with Nanabi, she serves as the right hand of Tamamo.
Yao enjoys playing with men, and will use all eight of her tails to that end. In addition, she can use her soft giant hand and elastic tongue to delight any man. Incredibly prideful, she doesn't use her vagina to mate with men. When raping a man, she always uses her ass.
Though she is sexually sadistic, she is not so cruel as to kill her catch. That said, there's no escape from her if she chooses to play with the man until his natural death.""")
        girl.zukan_ko = "hatibi"
        girl.label = "hatibi"
        girl.label_cg = "hatibi_cg"
        girl.info_rus_img = "hatibi_pedia"
        girl.m_pose = hatibi_pose
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 58
        girl.hp = 12000
        girl.echi = ["hatibi_ha", "hatibi_hb"]
        girl.skills = ((3548, "Tail Comfort"),
                         (3549, "Playful Hand"),
                         (3550, "Dancing Tongue"),
                         (3551, "Breast Valley"),
                         (3552, "Eight Moons"),
                         (3553, "Gate of Defilement"),
                         (3586, "Eyes of Temptation"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 58,
            "skill01": 15,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# INP
########################################
    def get_inp():
        girl = Chr()
        girl.unlock = persistent.inp_unlock == 1
        girl.name = "Imp"
        girl.pedia_name = "Imp"
        girl.info = _("""One of the weak Imps living in the Monster Lord's Castle. Far too weak to fight, she is merely a normal resident. Naturally, her magical and physical abilities are pitifully low. Any soldier capable of making it to the Monster Lord's Castle could defeat her with a single tap. The only choice left to such a weak monster when faced with a human is to try to seduce them. But due to her extreme fragility, it's easy to make light of her seduction and play around with her. Though she is weak, it's dangerous to allow her to play with you sexually. If she manages to make the man come, he will quickly become defenseless. In addition, if she manages to get the man inside her, he will be left powerless. If that were to happen, the powerful human would be toyed with by this Imp, far weaker than him. Though she's an incredibly low-grade monster, you must never forget that she is still a monster.""")
        girl.zukan_ko = "inp"
        girl.label = "inp"
        girl.label_cg = "inp_cg"
        girl.info_rus_img = "inp_pedia"
        girl.m_pose = inp_pose
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 3
        girl.hp = 50
        girl.echi = ["inp_ha", "inp_hb"]
        girl.skills = ((3532, "Imp Handjob"),
                      (3533, "Imp Fellatio"),
                      (3534, "Imp Tit Fuck"),
                      (3547, "Imp Hip Shake"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 59,
            "skill01": 15,
            "skill02": 2,
            "skill03": 2,
            "skill04": 2,
            "skill05": 2,
            "item01": 9
            }
        return girl

########################################
# GIGANTWEAPON
########################################
    def get_gigantweapon():
        girl = Chr()
        girl.unlock = persistent.gigantweapon_unlock == 1
        girl.name = "Giganto Weapon"
        girl.pedia_name = "Giganto Weapon"
        girl.info = _("""An artificial monster created at the time of the Great Monster Wars. Containing the superior aspects of many different monsters, she's an incredibly powerful Chimeric monster. Discovered unmoving in an unused portion portion of the castle her creator is unknown. Using the knowledge remaining from the Great Monster Wars, she was able to be repaired to the point of being operational again. Despite that, she is still far weaker than she was during the Great Monster Wars.
Though she follows the orders of the one she registers as her master, her destruction impulse is so strong she sometimes goes berserk. Binding any animal with her tail, she will mechanically squeeze out their semen to use as her power source. Of course, the most energy dense semen is found in human males.""")
        girl.zukan_ko = "gigantweapon"
        girl.label = "gigantweapon"
        girl.label_cg = "gigantweapon_cg"
        girl.info_rus_img = "gigantweapon_pedia"
        girl.m_pose = gigantweapon_pose
        girl.facenum = 2
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -100
        girl.lv = 90
        girl.hp = 18000
        girl.skills = ((3628, "Tentacle Dance"),
                               (3629, "Semen Squeezing Program"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 58,
        "skill01": 15,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# alma_elma3
########################################
    def get_alma_elma3():
        girl = Chr()
        girl.unlock = persistent.alma_elma3_unlock == 1
        girl.name = "Alma Elma"
        girl.pedia_name = "Alma Elma (3)"
        girl.info = _("""The Queen Succubus who commands the power of wind. Out of all the Succubi throughout the ages, she is believed to be the most formidable in battle.
Despite her genitals, every part of her body is a lethal sexual weapon that can bring any man to their knees in mere moments. Of course her abilities aren't limited to just men. Alma Elma can even bring females to mind breaking ecstasy in moments.
Though she participates in the Colosseum, she doesn't truly fight seriously. Though true to her nature as a Succubus, she enjoys making the loser drown in pleasure and submission to her. Despite seeming as though she cares about nothing, she does take great pride in herself as a Succubus. Due to her complicated personality, she became isolated from other Succubi.""")
        girl.zukan_ko = "alma_elma3"
        girl.label = "alma_elma3"
        girl.label_cg = "alma_elma3_cg"
        girl.info_rus_img = "alma_elma3_pedia"
        girl.m_pose = alma_elma3_pose
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 125
        girl.hp = 15000
        girl.skills = ((3630, "Melty Hand"),
                              (3631, "Sweet Fellatio"),
                              (3632, "Tail Caress"),
                              (3633, "Ancient Succubus Way: Snake"),
                              (3634, "Romanus Teri Spirit"),
                              (3635, "Melo Sophie Tellus"),
                              (3636, "Succubus Ceremony"),
                              (3637, "Meltic Kiss"),
                              (3638, "Tail Drain: Reterra"),
                              (3639, "A Queen's Drain"),
                              (3640, "Five Step Vaginal Hell"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 60,
        "skill01": 16,
        "skill02": 2,
        "skill03": 2,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# TAMAMO2
########################################
    def get_tamamo2():
        girl = Chr()
        girl.unlock = persistent.tamamo2_unlock == 1
        girl.name = "Tamamo"
        girl.pedia_name = "Tamamo (2)"
        girl.info = _("""The strongest of the beast type monsters, and the leader of the Kitsune race. One of the Four Heavenly Knights, her physical and defense strength are unmatched among all monsters despite her tiny body. In addition, her magical power is extremely high, enabling her to use many types of magic.
With a calm disposition, she won't attack humans unless challenged first. It seems she also acts to prevent trouble between humans and monsters.
With her true age unknown, she has also acted as the teacher for the last few generations of Monster Lords. At the very least, she has been in this role since Alipheese the Fifteenth was a child. Acting as Alipheese the Sixteenth's mentor since childhood, she has a close bond with the current Monster Lord. Due to her strict education, Alipheese quickly gets moody when Tamamo starts to nag at her. Though in truth Alipheese seems to care for her, with their relationship closer to that of a mother and daughter than of a teacher and pupil.""")
        girl.zukan_ko = "tamamo2"
        girl.label = "tamamo2"
        girl.label_cg = "tamamo2_cg"
        girl.info_rus_img = "tamamo2_pedia"
        girl.m_pose = tamamo2_pose
        girl.facenum = 4
        girl.posenum = 5
        girl.bk_num = 7
        girl.x0 = -180
        girl.lv = 128
        girl.hp = 22000
        girl.skills = ((3641, "Kitsune Handling"),
                          (3642, "Kitsune Mouth Handling"),
                          (3643, "Flat Breast Rub"),
                          (3644, "Moonwar Footpound"),
                          (3645, "Emasculating Amusement"),
                          (3646, "Moontail Entanglement"),
                          (3647, "Nine Moons"),
                          (3648, "Moonlight Rendezvous"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 61,
        "skill01": 16,
        "skill02": 3,
        "skill03": 2,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# ERuBETIE2
########################################
    def get_erubetie2():
        girl = Chr()
        girl.unlock = persistent.erubetie2_unlock == 1
        girl.name = "Erubetie"
        girl.pedia_name = "Erubetie (2)"
        girl.info = _("""The Queen Slime and one of the Four Heavenly Knights, Erubetie boasts unmatched vitality among monsters. In actuality, she is a colony of over 10,000 slimes spirits and other monsters that is controlled by a single will. Most willingly fused with Erubetie, but there are some who were attacked and taken by force. She cannot technically be called a Slime monster any longer. Erubetie keeps control over the colony inside her with her iron will.
That said, the only ones taken inside her colony are females. Males are apparently incompatible with her, and are instead dissolved and eaten.
When facing humans, she is filled with both anger and despair. At one point, she desired to live with humans in peace... But due to the thousands of beings inside her, it may be that her personality has become quite confused.""")
        girl.zukan_ko = "erubetie2"
        girl.label = "erubetie2"
        girl.label_cg = "erubetie2_cg"
        girl.info_rus_img = "erubetie2_pedia"
        girl.m_pose = erubetie2_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = 25
        girl.x1 = 225
        girl.lv = 125
        girl.hp = 22000
        girl.skills = ((3649, "Agartha Draw"),
                           (3650, "Arcadia Spread"),
                           (3651, "Xanadu Stroke"),
                           (3652, "Shangri-La Screw"),
                           (3653, "Melt Storm"),
                           (3654, "Heaven's Prison"),
                           (3655, "Neverland Frontier"),
                           (3656, "Divine Destiny"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 62,
        "skill01": 16,
        "skill02": 3,
        "skill03": 3,
        "skill04": 2,
        "skill05": 2,
        "item01": 9
        }
        return girl

########################################
# GRANBERIA4
########################################
    def get_granberia4():
        girl = Chr()
        girl.unlock = persistent.granberia4_unlock == 1
        girl.name = "Granberia"
        girl.pedia_name = "Granberia (4)"
        girl.info = _("""The most powerful Cursed Swordswoman Dragonkin and one of the Heavenly Knights. Unmatched in the control of flame, even above her teacher Salamander, Granberia has no equal when it comes to fire based sword skills. Though it appears she has difficulty battling against Alma Elma for some reason.
Though she's incredibly powerful in her physical abilities, she seems to have an inferiority complex regarding her sexual skills. Due to that uneasiness, she appears to become a little too aggressive when humiliating men after their defeat. In truth, her sexual skills are top-notch, and her natural instinct as a Dragonkin along with her body itself could leave any man gasping in ecstasy.""")
        girl.zukan_ko = "granberia4"
        girl.label = "granberia4"
        girl.label_cg = "granberia4_cg"
        girl.info_rus_img = "granberia4_pedia"
        girl.m_pose = granberia4_pose
        girl.facenum = 5
        girl.posenum = 4
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 24000
        girl.scope = {"ruka_tati": 1,
        "mylv": 63,
        "skill01": 16,
        "skill02": 3,
        "skill03": 3,
        "skill04": 2,
        "skill05": 3,
        "item01": 9
        }
        return girl

########################################
# ALICE3
########################################
    def get_alice3():
        girl = Chr()
        girl.unlock = persistent.alice3_unlock == 1
        girl.name = "Alice"
        girl.pedia_name = "Alice (3)"
        girl.info = _("""The 16th Monster Lord, she is known to be the most powerful one since the first generation. She has inherited the will of her mother, and strives for peace with humanity. However it's rumored that many monsters are grouping in rebellion, conspiring against her.
After fighting Nias once and losing, she realized that she cannot simply defeat a Goddess. Traveling around the world Nias controls, she looked for a way to resolve the issue with humanity. But due to her trauma at destroying her mother's dream as a young child, she can't quite look at everything with a rational mind...
She herself couldn't escape from the belief that if she, as the Monster Lord, were to be killed by a Hero, that everything would end happily. Finally freed from the temptation of self-sacrifice, she is walking on her own path for the first time.""")
        girl.zukan_ko = "alice3"
        girl.label = "alice2"
        girl.label_cg = "alice2_cg"
        girl.info_rus_img = "alice3_pedia"
        girl.m_pose = alice3_pose
        girl.facenum = 10
        girl.posenum = 7
        girl.bk_num = 8
        girl.x0 = -20
        girl.x1 = 150
        girl.lv = 145
        girl.hp = 32000
        girl.skills = ((3657, "Satanic Pressure"),
                         (3658, "Pleasure Drain"),
                         (3659, "Eyes of Temptation"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 64,
        "skill01": 16,
        "skill02": 3,
        "skill03": 3,
        "skill04": 3,
        "skill05": 3,
        "item01": 9
        }
        return girl

########################################
# ilias2
########################################
    def get_ilias2():
        girl = Chr()
        girl.unlock = persistent.hsean_ilias1 == 1 and persistent.game_clear > 1
        girl.name = "Ilias"
        girl.pedia_name = "Goddess Ilias (2)"
        girl.info = _("""As the Goddess who created the world, she resides in the heavens. Though she's incredibly powerful, she rarely extends that power to the world below. But by extending her will to the humans below, she leads them.
The equal of the Founding Monster Lord, there are many doubts surrounding her such as what happened to the Hero Heinrich, the appearance of a strange monster called "Black Alice", the strange female scientist with the Chimeric Monsters, and the Slaughter of Remina... It may be that llias has had a hand in all of these issues""")
        girl.zukan_ko = "ilias2"
        girl.label = "ilias2"
        girl.label_cg = "ilias2_cg"
        girl.info_rus_img = "ilias2_pedia"
        girl.m_pose = ilias2_pose
        girl.zukan_ko = None
        girl.facenum = 6
        girl.posenum = 3
        girl.x0 = -200
        girl.lv = "????"
        girl.hp = "????"
        girl.echi = ["lb_0301a"]
        girl.scope = {"ruka_tati": 1,
        "mylv": 64,
        "skill01": 16,
        "skill02": 3,
        "skill03": 3,
        "skill04": 3,
        "skill05": 3,
        "item01": 9
        }
        return girl
