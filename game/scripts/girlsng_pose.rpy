init -10 python:

    def granberia_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def queenharpy_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["queenharpy st21", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["queenharpy st22", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["queenharpy st23", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["queenharpy st24", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["queenharpy st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["queenharpy st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["queenharpy st13", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 4:
            imgs.append(["queenharpy st14", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["queenharpy bk02", (x, 0)])
            if persistent.bk2:
                imgs.append(["queenharpy bk03", (x, 0)])
            if persistent.bk3:
                imgs.append(["queenharpy bk04", (x, 0)])
            if persistent.bk4:
                imgs.append(["queenharpy bk05", (x, 0)])

        return imgs

    def tamamo_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["tamamo st62", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["tamamo st61", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["tamamo st63", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["tamamo st51", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["tamamo st52", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["tamamo st53", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["tamamo st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["tamamo st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["tamamo st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["tamamo st14", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["tamamo st15", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["tamamo st16", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 1:
            imgs.append(["tamamo st01", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 2:
            imgs.append(["tamamo st02", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 3:
            imgs.append(["tamamo st03", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 4:
            imgs.append(["tamamo st04", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 5:
            imgs.append(["tamamo st05a", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 6:
            imgs.append(["tamamo st05a", (x, 0)])
        elif persistent.pose == 4 and persistent.face == 7:
            imgs.append(["tamamo st09", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def alma_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["alma_elma st51", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["alma_elma st52", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["alma_elma st53", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["alma_elma st21", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["alma_elma st22", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["alma_elma st23", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["alma_elma st11", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["alma_elma st12", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 3:
            imgs.append(["alma_elma st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 4:
            imgs.append(["alma_elma st14", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 5:
            imgs.append(["alma_elma st15", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 6:
            imgs.append(["alma_elma st16", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 7:
            imgs.append(["alma_elma st17", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 8:
            imgs.append(["alma_elma st18", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 9:
            imgs.append(["alma_elma st19", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def sylph_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def gnome_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def erubetie_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def undine_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs

    def salamander_ng_pose(x):
        imgs = []

        if persistent.pose == 1 and persistent.face == 7:
            persistent.face = 1
        elif persistent.pose == 2 and persistent.face == 4:
            persistent.face = 1
        elif persistent.pose == 3 and persistent.face == 3:
            persistent.face = 1
        elif persistent.pose == 4:
            persistent.pose = 1

        if persistent.pose == 1 and persistent.face == 1:
            imgs.append(["granberia st01", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 2:
            imgs.append(["granberia st02", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 3:
            imgs.append(["granberia st03", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 4:
            imgs.append(["granberia st04", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 5:
            imgs.append(["granberia st05", (x, 0)])
        elif persistent.pose == 1 and persistent.face == 6:
            imgs.append(["granberia st06", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 1:
            imgs.append(["granberia st11", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 2:
            imgs.append(["granberia st12", (x, 0)])
        elif persistent.pose == 2 and persistent.face == 3:
            imgs.append(["granberia st13", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 1:
            imgs.append(["granberia st41", (x, 0)])
        elif persistent.pose == 3 and persistent.face == 2:
            imgs.append(["granberia st42", (x, 0)])

        if persistent.pose == 1:
            if persistent.bk1:
                imgs.append(["granberia bk01", (x, 0)])
            if persistent.bk2:
                imgs.append(["granberia bk02", (x, 0)])
            if persistent.bk3:
                imgs.append(["granberia bk03", (x, 0)])

        return imgs
