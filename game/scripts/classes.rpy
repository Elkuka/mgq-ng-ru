init -8 python:

    import math

    class Chr(object):
        def __init__(self):
            self.facenum = 1
            self.posenum = 1
            self.bk_num = 0
            self.x0 = 0
            self.x1 = 0
            self.y0 = 0
            self.scope = None
            self.info = ""
            self.hp = 0
            self.max_hp = 0
            self.lv = 0
            self.skills = None
            self.echi = [""]

##########################################################################
# Quake function
#
# Функция тряски экрана
    class Shaker(object):

        anchors = {
            'top' : 0.0,
            'center' : 0.5,
            'bottom' : 1.0,
            'left' : 0.0,
            'right' : 1.0,
            }

        def __init__(self, start, child, dist):
            if start is None:
                start = child.get_placement()
            #
            self.start = [ self.anchors.get(i, i) for i in start ]  # central position
            self.dist = dist    # maximum distance, in pixels, from the starting point
            self.child = child

        def __call__(self, t, sizes):
            # Float to integer... turns floating point numbers to
            # integers.
            def fti(x, r):
                if x is None:
                    x = 0
                if isinstance(x, float):
                    return int(x * r)
                else:
                    return x

            xpos, ypos, xanchor, yanchor = [fti(a, b) for a, b in zip(self.start, sizes) ]

            xpos = xpos - xanchor
            ypos = ypos - yanchor

            nx = xpos + (1.0-t) * self.dist * (renpy.random.random()*2-1)
            ny = ypos + (1.0-t) * self.dist * (renpy.random.random()*2-1)

            return (int(nx), int(ny), 0, 0)

    def _Shake(start, time, child=None, dist=100.0, **properties):

        move = Shaker(start, child, dist=dist)

        return renpy.display.layout.Motion(move,
                      time,
                      child,
                      add_sizes=True,
                      **properties)

    Quake = renpy.curry(_Shake)

##########################################################################
# Transition condition switch
#
#
    class TransitionConditionSwitch(renpy.Displayable):

        def __init__(self, transition, *args, **kwargs):
            super(TransitionConditionSwitch, self).__init__(**kwargs)
            self.transition = transition
            self.d = zip(args[0::2], args[1::2])
            self.time_reset = True
            self.old_d = None
            self.current_d = None
            self.ta = None
            self.old_st = 0

        def render(self, width, height, st, at):
            if self.ta is None:
                self.per_interact()
            if self.time_reset:
                self.time_reset = False
                self.st = st
                self.at = at
                self.old_st = 0
            if st < self.old_st:
                self.st, self.at, st, at = 0, 0, 1000000.0, 1000000.0
            self.old_st = st
            return renpy.render(self.ta, width, height, st-self.st, at-self.at)

        def per_interact(self):
            for name, d in self.d:
                if eval(name):
                    change_to = d
                    break

            if change_to is not self.current_d:
                self.time_reset = True
                self.old_d = self.current_d
                self.current_d = change_to
                if self.old_d is None:
                    self.old_d = self.current_d
                self.ta = anim.TransitionAnimation(self.old_d, 0.00, self.transition, self.current_d)
                renpy.redraw(self, 0)

        def visit(self):
            return [ self.ta ]

init python:
    def _shake_function(trans, st, at, dt=.5, dist=256, xy="xy"):
        if st <= dt:
            if xy != "y":
                trans.xoffset = int((dt-st)*dist*(.5-renpy.random.random())*2)

            if xy != "x":
                trans.yoffset = int((dt-st)*dist*(.5-renpy.random.random())*2)

            return .01
        else:
            None

    # def _shake_functionx(trans, st, at, dt=.5, dist=256):
    #     if st <= dt:
    #         trans.xoffset = int((dt-st)*dist(.5-renpy.renpy.random.random())*2)
    #         return .01
    #     else:
    #         None

    # def _shake_functiony(trans, st, at, dt=.5, dist=256):
    #     if st <= dt:
    #         trans.yoffset = int((dt-st)*dist(.5-renpy.renpy.random.random())*2)
    #         return .01
    #     else:
    #         None


    class MovieLooped(renpy.display.video.Movie):
        """Play Movie Sprites without loops. Until Ren'Py permits that by default, this can be used.
        """
        def __init__(self, *args, **kwargs):
            super(MovieLooped, self).__init__(*args, **kwargs)
            self.loops = kwargs.get("loops", 1)

        def play(self, old):
            if old is None:
                old_play = None
            else:
                old_play = old._play

            if self._play != old_play:
                if self._play:
                    renpy.audio.music.play([self._play]*self.loops, channel=self.channel, loop=False, synchro_start=True)

                    if self.mask:
                        renpy.audio.music.play([self.mask]*self.loops, channel=self.mask_channel, loop=False, synchro_start=True)

                else:
                    renpy.audio.music.stop(channel=self.channel)

                    if self.mask:
                        renpy.audio.music.stop(channel=self.mask_channel)
