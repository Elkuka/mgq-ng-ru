init python:
########################################
# CUPID
########################################
    def get_cupid():
        girl = Chr()
        girl.unlock = persistent.cupid_unlock == 1
        girl.name = "Cupid"
        girl.pedia_name = "Cupid"
        girl.info = _("""An angel controlling love, she is in the ninth and lowest hierarchy in the classification of angels. Cupid uses her bow and arrows to disturb the heart of her target Her duty was to ensure that descendants were left by joining together men and women. Though combat is not her primary role, she sometimes plays support by using her bow and arrow from range.
Incredibly debauched, she considers raping human men to be the ultimate of pleasures. However since contact with humans outside of her normal duties is forbidden, she takes it upon herself to look for sinners to personally punish. Giving in to her carnal lust, she rapes the sinner until he breaks apart.
In addition, the lowest rank of angels comprise 90\% of all angels, and are considered low-class.""")
        girl.label = "cupid"
        girl.label_cg = "cupid_cg"
        girl.info_rus_img = "cupid_pedia"
        girl.m_pose = cupid_pose
        girl.zukan_ko = "cupid"
        girl.facenum = 6
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 80
        girl.max_hp = 13000
        girl.hp = 13000
        girl.skills = ((3383, "Eros's Handjob"),
            (3660, "Cupid's Blowjob"),
            (3669, "Amores's Tit Fuck"),
            (3670, "Tit Fuck Temptation"),
            (3671, "Love Angel's Lust"))
        girl.scope = {"ruka_tati": 1,
            "mylv": 64,
            "skill01": 16,
            "skill02": 3,
            "skill03": 3,
            "skill04": 3,
            "skill05": 3,
            "item01": 9
            }

        return girl

########################################
# VALKYRIE
########################################
    def get_valkyrie():
        girl = Chr()
        girl.unlock = persistent.valkyrie_unlock == 1
        girl.name = "Valkyrie"
        girl.pedia_name = "Valkyrie"
        girl.info = _("""An angel proud of her combat ability, she rates among the highest of the angels usually tasked with going to the surface world. She is usually the head of the subjugation forces assigned to eliminate high ranking monsters or groups of monsters. She belongs to the lowest of the nine circles of angels (known simply as Angels), but it has been said her promotion to Archangel class is close.
Valkyrie is known for being very faithful to her mission, and almost seems indifferent as she carries out her duties. She is usually paired with Cupid. During operations where she has taken out powerful monsters, she has punished the human men enraptured by them with rape.Wielding powerful sexual skills, she rapes the sinners with that same look of indifference on her face. No matter the reason, Valkyrie will never forgive a man who mates with a monster.
In addition, Valkyrie used to guide the souls of dead soldiers... But that duty was reassigned to another angel, who now sends faithful departed souls to Heaven.""")
        girl.label = "valkyrie"
        girl.label_cg = "valkyrie_cg"
        girl.info_rus_img = "valkyrie_pedia"
        girl.m_pose = valkyrie_pose
        girl.zukan_ko = "valkyrie"
        girl.facenum = 3
        girl.posenum = 4
        girl.bk_num = 5
        girl.x0 = -300
        girl.x1 = -50
        girl.lv = 88
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3672, "War Angel's Handjob"),
           (3673, "War Angel's Heavenly Wings"),
           (3674, "War Angel's Blowjob"),
           (3675, "War Angel's Breast Comfort"),
           (3676, "War Angel's Embrace"),
           (3677, "War Angel's Intercrural"),
           (3678, "War Angel's Vagina"))
        girl.scope = {
            "ruka_tati": 1,
            "mylv": 64,
            "skill01": 16,
            "skill02": 3,
            "skill03": 3,
            "skill04": 3,
            "skill05": 3,
            "item01": 10
            }
        return girl

########################################
# ARIEL
########################################
    def get_ariel():
        girl = Chr()
        girl.unlock = persistent.ariel_unlock == 1
        girl.name = "Archangel Ariel"
        girl.pedia_name = "Archangel Ariel"
        girl.info = _("""An angel of the eighth circle (Archangel) with even more combat experience and ability than Valkyrie, tasked with exacting the Goddess's punishment. Though she rarely goes down to the surface world, she always keeps herself at the ready to descend into a big fight as the primary force. She participated in both the Great Monster Wars of 1,000 years ago, and the "Angel Massacre" incident of 500 years ago.
Her high combat capability has led her to exterminate thousands of high ranking monsters on the surface world. In addition, she has given the merciless punishment of the Heavens to many human sinners who have traveled down the wrong path.
It should be noted that in the circles above Archangel, the duties of angels start to orient more toward duties of the soul and management of the Heavens. It isn't implied that the combat capability of higher circle angels is higher than the Archangel class. Archangels, with their special forces use for battle, are more often than not just as powerful in combat as higher ranking angels.""")
        girl.label = "ariel"
        girl.label_cg = "ariel_cg"
        girl.info_rus_img = "ariel_pedia"
        girl.m_pose = ariel_pose
        girl.zukan_ko = "ariel"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 110
        girl.max_hp = 22000
        girl.hp = 22000
        girl.skills = (
        (4136, "Archangel's Masturbation"),
        (3679, "Archangel's Fellatio"),
        (3680, "Archangel's Feathers"),
        (3681, "Archangel's Tit Fuck"),
        (3682, "Archangel's Embrace"),
        (3683, "Archangel's Rape")
        )
        girl.scope = {"ruka_tati": 1,
        "mylv": 64,
        "skill01": 16,
        "skill02": 3,
        "skill03": 3,
        "skill04": 3,
        "skill05": 3,
        "item01": 10
        }
        return girl

########################################
# C_S2
########################################
    def get_c_s2():
        girl = Chr()
        girl.unlock = persistent.c_s2_unlock == 1
        girl.name = "Experimental Organism S-2"
        girl.pedia_name = "Experimental Organism S-2"
        girl.info = _("""An early experimental organism created in Promestein's laboratory that is a mix of human, slime and tentacle monster embryos. Though there was no issue with her ability to survive, her combat capabilities were not as high as desired, so she was considered a failure.
Still, if her opponent is a male soldier, she is still able to overcome and greedily feast on his semen. She is able to control both her slimy lower body and tentacles to give excellent pleasure to the male target Using those organs, experiments have shown that she will continue to squeeze the man until his death. She can't match up to higher ranking monsters, but she can still be quite dangerous to a man.""")
        girl.label = "c_s2"
        girl.label_cg = "c_s2_cg"
        girl.info_rus_img = "c_s2_pedia"
        girl.m_pose = c_s2_pose
        girl.zukan_ko = "c_s2"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 40
        girl.max_hp = 7500
        girl.hp = 7500
        girl.skills = ((3684, "Semen Sucking Pacifier"),
        (3685, "Absorbing Tit Fuck"),
        (3686, "Semen Sucking Tentacle"),
        (3687, "Bubble Shake"),
        (3688, "Bubble Heaven"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 65,
        "skill01": 17,
        "skill02": 0,
        "skill03": 0,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# C_A3
########################################
    def get_c_a3():
        girl = Chr()
        girl.unlock = persistent.c_a3_unlock == 1
        girl.name = "Experimental Organism A-3"
        girl.pedia_name = "Experimental Organism A-3"
        girl.info = _("""A Chimeric Beast created in Promestein's laboratory, composed of a variety of organs from both monster and angel. She was the result of early experiments of direct transplant of monster organs into the body of an angel.
No good practical use was found for her, and she was simply kept in the tanks until now. Due to the primitive nature of the transplantation, it seems to have deeply impacted her memories and emotions. Her entire being seems to have degraded, and seeks to greedily devour male semen like a common monster.
Lastly, the angel used for this experiment was said to have been offered into it due to having non-punishment sexual relations with humans.""")
        girl.label = "c_a3"
        girl.label_cg = "c_a3_cg"
        girl.info_rus_img = "c_a3_pedia"
        girl.m_pose = c_a3_pose
        girl.zukan_ko = "c_a3"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 42
        girl.max_hp = 9000
        girl.hp = 9000
        girl.skills = ((3689, "Tongue Stroke"),
        (3690, "Breast Press"),
        (3691, "Tentacle Rape"),
        (3692, "Immoral Raid"),
        (3693, "Immoral Feeling"))
        girl.scope = {"ruka_tati": 1,
        "mylv": 65,
        "skill01": 17,
        "skill02": 0,
        "skill03": 0,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# STEIN1
########################################
    def get_stein1():
        girl = Chr()
        girl.unlock = persistent.stein1_unlock == 1
        girl.name = "Promestein"
        girl.pedia_name = "Promestein"
        girl.info = _("""An angel who studies biology and technology that serves llias. Though she is an angel of the ninth circle, she was given a special mission by llias. Her original body was that of a normal angel, but she has since transplanted an ancient seaweed that she spend generations researching. She seems to have fairly high prowess for combat.
The original reason she had for going against the Goddess's will and studying science is not known. It is only known that she was imprisoned as a criminal angel before llias gave her an important mission. A very strange existence among the angels, her fellow angels refer to her as a Heretic sometimes.""")
        girl.label = "stein1"
        girl.label_cg = "stein1_cg"
        girl.info_rus_img = "stein1_pedia"
        girl.m_pose = stein1_pose
        girl.zukan_ko = "stein1"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 115
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3694, "Warmth"),
            (3695, "Softness"),
            (3696, "Pressure"),
            (3697, "Suction"),
            (3698, "Tenderness"),
            (3699, "Temperance"))
        girl.echi = ["stein1_h", "lb_0307a"]
        girl.scope = {"ruka_tati": 1,
            "mylv": 65,
            "skill01": 17,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# RANAEL
########################################
    def get_ranael():
        girl = Chr()
        girl.unlock = persistent.ranael_unlock == 1
        girl.name = "Archangel Ranael"
        girl.pedia_name = "Archangel Ranael"
        girl.info = _("""An angel of the eighth circle (Archangel), her role is executing Heaven's divine punishment A mighty force even on her own, she was also the commander of the forces that assaulted Remina in the past Having a very harsh nature, she cruelly kills her opponents with her various body parts.
Though she has various biological and plant body parts, they are a result of her divine aura assimilating surrounding beings with her, and not an artificial construct Although despite being able to assimilate with other beings, the most effective way of regaining her energy is through male semen. Therefore when she executes her punishment on human males, she always makes sure to squeeze them dry first""")
        girl.label = "ranael"
        girl.label_cg = "ranael_cg"
        girl.info_rus_img = "ranael_pedia"
        girl.m_pose = ranael_pose
        girl.zukan_ko = "ranael"
        girl.facenum = 3
        girl.posenum = 4
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 118
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3700, "Drain Vine"),
            (3701, "Fine Melody"),
            (3702, "Medusa Hair"),
            (3703, "Menacing Caress"),
            (3704, "Ray Heine"),
            (3705, "Arna Principality"))
        girl.scope = {"ruka_tati": 1,
            "mylv": 66,
            "skill01": 18,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_TANGH
########################################
    def get_c_tangh():
        girl = Chr()
        girl.unlock = persistent.c_tangh_unlock == 1
        girl.name = "Chimera Tongue"
        girl.pedia_name = "Chimera Tongue"
        girl.info = _("""A Chimeric Monster created by Promestein's studies. By manipulating the genes of slug based monsters, she was able to make a monster compromised almost solely of tongue-like organs. With taste buds all over her body, she is also able to secrete saliva almost anywhere. In addition, she's able to stretch and extend every tongue to some degree. Her vitality and physical power are much higher than normal, and there was no aberrations with her mental state except excessive hunger. She was an early creation, but a successful one due to the simplicity of her design.
Enjoying the bodily fluids of humans, she has been known to lick both male and female's whole body to lick out everything. Of course her favorite is male semen, and will greedily lick a man's crotch until he is completely dry if she has the chance. Due to her dexterous use of her many tongues, she can quickly lead her prey to numerous ejaculations in a short time. Though she usually licks her prey to death, she has also been known to keep her catch alive so she can keep on feasting.""")
        girl.label = "c_tangh"
        girl.label_cg = "c_tangh_cg"
        girl.info_rus_img = "c_tangh_pedia"
        girl.m_pose = c_tangh_pose
        girl.zukan_ko = "c_tangh"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 57
        girl.max_hp = 15000
        girl.hp = 15000
        girl.skills = ((3706, "Swarm Lick"),
            (3707, "Entangled Lick"),
            (3708, "Disorder Lick"),
            (3709, "Dissolving Lick"),
            (3710, "Winding Lick"),
            (3711, "Hellish Lick"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 66,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# ANGELS
########################################
    def get_angels():
        girl = Chr()
        girl.unlock = persistent.angels_unlock == 1
        girl.name = "Angel Soldier"
        girl.pedia_name = "Angel Soldier"
        girl.info = _("""Angels belonging to the lowest circle, their main role lies in subjugating criminals and carrying out the wrath of the Heavens. Though their combat abilities are each equal to a high ranking monster, their power goes way up when they combine together. They have been known to destroy extremely powerful enemies with their powerful combination attacks.
The items they carry are mainly used on human men who break one of the five commandments of llias. Though their individual personalities are slightly different, they are all similar in their strictness with sinners.""")
        girl.label = "angels"
        girl.label_cg = "angels_cg"
        girl.info_rus_img = "angels_pedia"
        girl.m_pose = angels_pose
        girl.zukan_ko = "angels"
        girl.facenum = 3
        girl.posenum = 5
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 83
        girl.max_hp = 12000
        girl.hp = 12000
        girl.skills = (
            (3712, "Angel Group Handjob"),
            (3713, "Angel Group Footjob"),
            (3714, "Angel Group Blowjob"),
            (3715, "Angel Group Tit Fuck"),
            (3716, "Angel Group Items")
            )
        girl.scope = {
            "ruka_tati": 2,
            "mylv": 66,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }

        return girl

########################################
# NAGAEL
########################################
    def get_nagael():
        girl = Chr()
        girl.unlock = persistent.nagael_unlock == 1
        girl.name = "Principality Nagael"
        girl.pedia_name = "Principality Nagael"
        girl.info = _("""An angel of punishment and judgment accompanied by her sacred snakes, she falls in the seventh circle (Principalities). Usually enshrined in the court of Heavens, her normal role is to judge the souls of humans after their death depending on how they lived their lives.Those found guilty of crimes are punished by her sacred snakes. Tightly coiling around them, they punish the criminal with agonized ecstasy until they repent their sins.
Though in times of great disturbance, she will come down to the surface world herself. In those times, she will use her sacred snakes to exact punishment and judgment directly on the surface world. But those who are judged by Nagael on the ground are not brought up to the Heavens... After reaching their death while being punished on the surface world, their soul is again set for judgment in the Heavens.""")
        girl.label = "nagael"
        girl.label_cg = "nagael_cg"
        girl.info_rus_img = "nagael_pedia"
        girl.m_pose = nagael_pose
        girl.zukan_ko = "nagael"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 115
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3717, "Punishing Handjob"),
            (3718, "Punishing Blowjob"),
            (3719, "Punishing Tit Fuck"),
            (3720, "Foot of Divine Punishment"),
            (3721, "Judge Snake's Curse"),
            (3722, "Judge Snake's Punishment"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 67,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_SLAG
########################################
    def get_c_slag():
        girl = Chr()
        girl.unlock = persistent.hsean_etc6 == 1
        girl.name = "Chimera Slug"
        girl.pedia_name = "Chimera Slug"
        girl.info = _("""A Chimeric Monster created by Promestein that is based on slime-like monsters to operate in specific environments. Improvements were added at a gene level to increase her activity in high and low temperature areas. The basic data her creation and tested provided appear to have been used by the Next Doll program.
Her combat capabilities are not very high (of course still higher than a normal monster), but she was added to the invasion forces to acquire more experimental data.
Able to change the shape of her own body at will to assist in adapting to various environments, it also enables her to change the shape, size and formation of her sexual organs freely. Using that special technique, she can easily force males to ejaculate at near will.""")
        girl.label = "c_slag"
        girl.label_cg = "c_slag_cg"
        girl.info_rus_img = "c_slag_pedia"
        girl.m_pose = c_slag_pose
        girl.zukan_ko = "c_slag"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 42
        girl.max_hp = 10000
        girl.hp = 10000
        girl.echi = ["lb_0316a"]
        girl.scope = {
            "ruka_tati": 2,
            "mylv": 68,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_TENTACLE
########################################
    def get_c_tentacle():
        girl = Chr()
        girl.unlock = persistent.c_tentacle_unlock == 1
        girl.name = "Chimera Tentacle"
        girl.pedia_name = "Chimera Tentacle"
        girl.info = _("""A Chimeric Monster created by Promestein. An experimental being created to see how many tentacles could be controlled by a single brain, with her communicative synapses her expanded to the maximum. Though she looks similar to a Scylla monster, the toughness and speed of her tentacles aren't even comparable with normal monsters. The data from her ultrafast synapses have also been applied to more combat oriented Chimeric Monsters. Although she was originally just an experimental organism, her high abilities led her to be deployed as part of the invasion forces.
But the ultra-fast synapses also require a considerable amount of physical and magical strength to sustain. Due to that, her food intake is equivalent to a monster almost twice as big as her. Very greedy for male semen, she uses her numerous tentacles to squeeze any prey caught dry. Once caught, it's almost assured that the human male will be squeezed dry.""")
        girl.label = "c_tentacle"
        girl.label_cg = "c_tentacle_cg"
        girl.info_rus_img = "c_tentacle_pedia"
        girl.m_pose = c_tentacle_pose
        girl.zukan_ko = "c_tentacle"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 59
        girl.max_hp = 16000
        girl.hp = 16000
        girl.skills = ((3723, "Tentatrain"),
            (3724, "Tentacoil"),
            (3725, "Tentatempest"),
            (3726, "Tentabind"),
            (3727, "Tentafiesta"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 68,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# MARIEL
########################################
    def get_mariel():
        girl = Chr()
        girl.unlock = persistent.mariel_unlock == 1
        girl.name = "Archangel Mariel"
        girl.pedia_name = "Archangel Mariel"
        girl.info = _("""An extremely young Archangel, Mariel was known as a child prodigy. The youngest in age of all the angels, the incredible magical power she wielded after her birth stunned the Goddess's enemies.
Despite her innocent look, she is extremely aggressive. With the quick, dexterous movements of her young limbs she takes great pride in breaking her enemies through thoroughly attacking their weak points.
Since she has such little experience, she takes great joy in defeating enemies. Receiving divine punishment from one as young as Mariel usually brings great shame and immoral feelings to the vanquished. In the past, she would frequently rape even minor sinners until their deaths, which itself almost got her labeled a criminal.""")
        girl.label = "mariel"
        girl.label_cg = "mariel_cg"
        girl.info_rus_img = "mariel_pedia"
        girl.m_pose = mariel_pose
        girl.zukan_ko = "mariel"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 109
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3728, "Angel Hand"),
            (3729, "Angel Mouth"),
            (3730, "Angel Chest"),
            (3731, "Heaven's Foot"))
        girl.echi = ["mariel_ha", "mariel_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 68,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_PRISON
########################################
    def get_c_prison():
        girl = Chr()
        girl.unlock = persistent.hsean_etc1 == 1
        girl.name = "Chimera Prison"
        girl.pedia_name = "Chimera Prison"
        girl.info = _("""A Chimeric Monster created by Promestein. There was no real intent to experiment on her creation, she was designed simply to capture additional subjects. Originally she was going to be mass produced and sent to each city to capture additional subjects. But the maintenance figures for so many Chimera Prisons was considered too high to be worth it.
Due to these circumstances, her personality seems to have been distorted, and became overzealous in tormenting her prey. If captured by her, you will need to prepare for some harsh rape.""")
        girl.label = "c_prison"
        girl.label_cg = "c_prison_cg"
        girl.info_rus_img = "c_prison_pedia"
        girl.m_pose = c_prison_pose
        girl.zukan_ko = "c_prison"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 57
        girl.max_hp = 14000
        girl.hp = 14000
        girl.echi = ["lb_0319"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 68,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_MEDULAHAN
########################################
    def get_c_medulahan():
        girl = Chr()
        girl.unlock = persistent.c_medulahan_unlock == 1
        girl.name = "Chimera Medullahan"
        girl.pedia_name = "Chimera Medullahan"
        girl.info = _("""A Chimeric Monster created by Promestein, created by combining Medusa and a Dullahan monsters. Most experiments of this type of primitive combination failed, but Medullahan surprisingly was a success, able to function in her superior body without issue. The combination of the two monster types resulted in a multiplicative increase in power, instead of additive.
But the issues of metabolism still weren't resolved... To maintain her power, she requires far more energy than a normal monster. Actively seeking out males, she uses her vagina and snakes to greedily devour their semen. In addition, she has the frightening ability to petrify men and rape them due to her Medusa genes.""")
        girl.label = "c_medulahan"
        girl.label_cg = "c_medulahan_cg"
        girl.info_rus_img = "c_medulahan_pedia"
        girl.m_pose = c_medulahan_pose
        girl.zukan_ko = "c_medulahan"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -250
        girl.lv = 61
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3732, "Medullahan's Masturbation"),
            (3733, "Medullahan's Footjob"),
            (3734, "Medullahan's Tit Fuck"),
            (3735, "Medusa's Blowjob"),
            (3736, "Medusa's Hair"),
            (3737, "Snake Violation"),
            (3738, "Serpent Rape"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 68,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# TRINITY
########################################
    def get_trinity():
        girl = Chr()
        girl.unlock = persistent.trinity_unlock == 1
        girl.name = "Trinity"
        girl.pedia_name = "Trinity"
        girl.info = _("""A special group of angels that belong to the lowest circle. Their battle abilities are similar to a normal angel when by themselves, but when together their power rivals that of Archangels. But offensive combat is not their primarily role. Their special duty is to brainwash and punish sinners.
Though their characters are kind and gentle, like other angels they show no mercy to sinners. Specializing in a technique called the Cross of Pleasure, they punish a sinner with pleasure as a group. No matter the sinner, they eventually drown in pleasure and end up devoting themselves fully to the angels and Goddess.
Lastly, two sets of Trinities currently exist, totaling six angels. The only angels allowed to join the Trinities are ones that have shown remarkable talent for brainwashing.""")
        girl.label = "trinity"
        girl.label_cg = "trinity_cg"
        girl.info_rus_img = "trinity_pedia"
        girl.m_pose = trinity_pose
        girl.zukan_ko = "trinity"
        girl.facenum = 4
        girl.posenum = 3
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 94
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3763, "Blissful Masturbation"),
            (3764, "Proselytising Blowjob"),
            (3765, "Pleasurable Intercrural"),
            (3766, "Ascending Tit Fuck"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 69,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_BUG
########################################
    def get_c_bug():
        girl = Chr()
        girl.unlock = persistent.c_bug_unlock == 1
        girl.name = "Chimera Bug"
        girl.pedia_name = "Chimera Bug"
        girl.info = _("""A Chimeric Monster created by Promestein. The Chimera Bug was an original experiment focusing mainly on fertility, with the intent to use them in the future for mass production of additional Chimeric Monsters. Cells from the Beelzebub monsters, now believed extinct, were incorporated to increase the fecundity of her womb. Though her original use was for mass breeding, the incredible agility and strong external skeleton of her insect body led her to be admitted into the City Strike Force after the initial experimental data was completed on her.
Her desire to breed is incredibly high, and will actively seek out males to rape for fertilization purposes. The semen released into her oviduct is immediately used to fertilize the eggs she stores in her abdomen. Once the semen has been harvested from the male, she generally throws them away, as they are no longer needed. However there seems to be the possibility of her keeping a male with especially high quality semen as a permanent breeding slave.""")
        girl.label = "c_bug"
        girl.label_cg = "c_bug_cg"
        girl.info_rus_img = "c_bug_pedia"
        girl.m_pose = c_bug_pose
        girl.zukan_ko = "c_bug"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -150
        girl.lv = 54
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3767, "Slimy Handjob"),
            (3768, "Wet Blowjob"),
            (3769, "Sticky Tit Fuck"),
            (3770, "Human Stomach Rub"),
            (3771, "Insect Stomach Rub"),
            (3772, "Forced Oviduct Mating"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 69,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# SISTERLAMIA
########################################
    def get_sisterlamia():
        girl = Chr()
        girl.unlock = persistent.sisterlamia_unlock == 1
        girl.name = "Sister Lamia"
        girl.pedia_name = "Sister Lamia"
        girl.info = _("""An extremely rare monster that is a pious believer of llias, despite her teachings against Monsters. She was solicited by Black Alice to head to San Ilia itself. She is now acting as an advanced unit, following Nias's commands directly.
After being granted an artificial spirit, her power and strength drastically increased. Any prey caught in her snake body has no choice but to give up. When carrying out Nias's orders, she will show no mercy whatsoever at devouring male's life force, despite her gentle nature. While embracing her prey, she will try to reassure her prey as she squeeze them dry.""")
        girl.label = "sisterlamia"
        girl.label_cg = "sisterlamia_cg"
        girl.info_rus_img = "sisterlamia_pedia"
        girl.m_pose = sisterlamia_pose
        girl.zukan_ko = "sisterlamia"
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3773, "Graceful Blowjob"),
            (3774, "Love-Filled Breast Squeeze"),
            (3775, "Delightful Masturbation"),
            (3776, "Snake Body of Bliss"),
            (3777, "Ecstasy Snake Wrap"),
            (3778, "Love-Filled Breasts"),
            (3779, "Holy Snake's Embrace"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 69,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# CATOBLEPAS
########################################
    def get_catoblepas():
        girl = Chr()
        girl.unlock = persistent.hsean_etc2 == 1
        girl.name = "Catoblepas Girl"
        girl.pedia_name = "Catoblepas Girl"
        girl.info = _("""A Chimeric Monster created by Promestein. She was created in an attempt to reproduce the ancient biological weapons, but her power wasn't that great Though she still does wield more power than an average monster. As such, she was assigned to the San Ilia strike force due to their powerful military.
Her large build makes her require a large amount of energy to sustain herself. As such, she will attack men and women without mercy. Of course it goes without saying that male semen is her preferred meal. She is also extremely dangerous due to her petrification ability.""")
        girl.label = "catoblepas"
        girl.label_cg = "catoblepas_cg"
        girl.info_rus_img = "catoblepas_pedia"
        girl.m_pose = catoblepas_pose
        girl.zukan_ko = "catoblepas"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -250
        girl.lv = 60
        girl.max_hp = 16000
        girl.hp = 16000
        girl.echi = ["lb_0324a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 70,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# MUZUKIEL
########################################
    def get_muzukiel():
        girl = Chr()
        girl.unlock = persistent.muzukiel_unlock == 1
        girl.name = "Power Muzukiel"
        girl.pedia_name = "Power Muzukiel"
        girl.info = _("""A Power angel who belongs in the sixth circle in the angel hierarchy. Her primary duty involves managing the habitats of insects on the surface world, and regulate their populations. The genes of every type of insect in the world are in her body, and she is able to utilize male semen to produce as many new insects as may be required. To accomplish her ecosystem preservation tasks, she sometimes needs to secure men for seeding purposes.
Due to the various genes inside her body, she is frequently mistaken for a monster. Also due to these insect genes, she has a high potential for combat, and is able to descend to the surface world to take part in combat During those times, Muzukiel will sometimes secure a Sinner to assist her in her reproductive duties.""")
        girl.label = "muzukiel"
        girl.label_cg = "muzukiel_cg"
        girl.info_rus_img = "muzukiel_pedia"
        girl.m_pose = muzukiel_pose
        girl.zukan_ko = "muzukiel"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -130
        girl.lv = 122
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3780, "Punishing Handjob"),
            (3781, "Punishing Blowjob"),
            (3782, "Punishing Tit Fuck"),
            (3783, "Punishing Centipede"),
            (3784, "Punishing Leech"),
            (3785, "Punishing Flesh Hole"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 70,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# MERMAID
########################################
    def get_mermaid():
        girl = Chr()
        girl.unlock = persistent.mermaid_unlock == 1
        girl.name = "Mermaid"
        girl.pedia_name = "Mermaid"
        girl.info = _("""A common Mermaid that was following the Queen Mermaid. But also acting as an agent of Black Alice, she was granted a fake spirit of water. Hating humans like the Queen Mermaid, she looks at the Mermaids living with humans with disdain as well.
Although she hates humans, she loves violating and insulting them at the same time. Her sexual organs are of very high quality, and will give intense pleasure to any male who mates with her.""")
        girl.label = "mermaid"
        girl.label_cg = "mermaid_cg"
        girl.info_rus_img = "mermaid_pedia"
        girl.m_pose = mermaid_pose
        girl.zukan_ko = "mermaid"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 53
        girl.max_hp = 16000
        girl.hp = 16000
        girl.skills = ((3786, "Mermaid Blowjob"),
            (3787, "Mermaid Tit Fuck"),
            (3788, "Mermaid Hair"),
            (3789, "Mermaid Tail"),
            (3790, "Gel Blow"),
            (3791, "Song of the Mermaid"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 70,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# G_MERMAID
########################################
    def get_g_mermaid():
        girl = Chr()
        girl.unlock = persistent.g_mermaid_unlock == 1
        girl.name = "Mermaid General"
        girl.pedia_name = "Mermaid General"
        girl.info = _("""A Mermaid warrior who assists the Queen Mermaid. Given a fake spirit from Black Alice, her combat abilities increased even further. Her loyalty to the Queen is so great, she would never dare disobey an order from her.
Though proud of her skilled offensive power, she will use pleasure attacks on someone she is unsure she can beat. Her pleasure techniques are just as polished, and it is said she can water down any man in moments. If that happens, the man will quickly become a slave to her fine techniques as she violates him.""")
        girl.label = "g_mermaid"
        girl.label_cg = "g_mermaid_cg"
        girl.info_rus_img = "g_mermaid_pedia"
        girl.m_pose = g_mermaid_pose
        girl.zukan_ko = "g_mermaid"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -250
        girl.lv = 60
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3792, "General's Blowjob"),
            (3793, "General's Tit Fuck"),
            (3794, "General's Hair"),
            (3795, "General's Tail"),
            (3796, "General's Rape"))
        girl.echi = ["g_mermaid_ha", "g_mermaid_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 70,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# NINGYOHIME
########################################
    def get_ningyohime():
        girl = Chr()
        girl.unlock = persistent.ningyohime_unlock == 1
        girl.name = "El"
        girl.pedia_name = "El"
        girl.info = _("""A young Mermaid who is the Queen Mermaid's daughter. Having never left her mother's side, it seems as though she has never before appeared before a human that her mother hates so much. Due to her frequent swims in rough seas, her small body is surprisingly strong. Although her direct sexual experience is almost nothing, she has received lots of instruction from her mother.
Though she is interested in humans, she also wants to catch a human and toy with them like other Mermaids. But since human contact was forbidden to her by her mother, she hasn't been able to act on those desires.
In addition, the starfish on her waist is an accessory bag. It contains pretty coral, shiny marbles, shells and other treasures. Also assorted candies to snack on.""")
        girl.label = "ningyohime"
        girl.label_cg = "ningyohime_cg"
        girl.info_rus_img = "ningyohime_pedia"
        girl.m_pose = ningyohime_pose
        girl.zukan_ko = "ningyohime"
        girl.facenum = 9
        girl.posenum = 3
        girl.bk_num = 5
        girl.x0 = -190
        girl.lv = 55
        girl.max_hp = 15000
        girl.hp = 15000
        girl.skills = ((3797, "Princess's Blowjob"),
            (3798, "Princess's Tit Fuck"),
            (3799, "Princess's Stomach Rub"),
            (3800, "Princess's Tail Rub"),
            (3801, "Princess's Perversion"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 70,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# QUEENMERMAID
########################################
    def get_queenmermaid():
        girl = Chr()
        girl.unlock = persistent.queenmermaid_unlock == 1
        girl.name = "Queen Mermaid"
        girl.pedia_name = "Queen Mermaid"
        girl.info = _("""The Queen Mermaid over all other Mermaids. She wields intense magical ability, and skilled sexual techniques. Ever since losing her lover due to a tragedy, she has hated humans. As other Mermaids have deepend their relationship with humans in recent years, she has only been associating with her own subordinates. However in recent years she has used the stored semen from her lover to give birth to a daughter, leading some to believe she is fluctuating in her beliefs.
After being solicited by Black Alice, she affiliated herself with her. With her close subordinates, she attacked Port Natalia. Due to her wounded heart, the insult she inflicts upon those she defeats is severe and harsh.""")
        girl.label = "queenmermaid"
        girl.label_cg = "queenmermaid_cg"
        girl.info_rus_img = "queenmermaid_pedia"
        girl.m_pose = queenmermaid_pose
        girl.zukan_ko = "queenmermaid"
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 72
        girl.max_hp = 21000
        girl.hp = 21000
        girl.skills = ((3802, "Mermaid's Sucking Mouth"),
            (3803, "Mermaid's Squeezing Breasts"),
            (3804, "Mermaid's Slimy Stomach"),
            (3805, "Mermaid's Chaotic Tail"),
            (3806, "Mermaid's Cleaning Hand"),
            (3807, "Mermaid's Tight Pussy"),
            (3808, "Mermaid's Vibrating Pussy"))
        girl.echi = ["queenmermaid_ha", "queenmermaid_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 71,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# XX7
########################################
    def get_xx7():
        girl = Chr()
        girl.unlock = persistent.hsean_etc3 == 1
        girl.name = "Experimental Organism XX-7"
        girl.pedia_name = "Experimental Organism XX-7"
        girl.info = _("""A Chimeric Monster created by Promestein.
A fusion of a monster and an angel, the primary focus in her creation was on the use of mechanical organs. Although she was beneficial in improving the basic technology, she wasn't particularly powerful. Due to that, she was assigned to the invasion force for a small country town.
Due to her multiple mechanical organs, she is able to squeeze multiple prey dry very quickly. This technique of being able to process multiple men at once was put to use in later creations as well.""")
        girl.label = "xx7"
        girl.label_cg = "xx7_cg"
        girl.info_rus_img = "xx7_pedia"
        girl.m_pose = xx7_pose
        girl.zukan_ko = "xx7"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -150
        girl.lv = 45
        girl.max_hp = 11000
        girl.hp = 11000
        girl.echi = ["lb_0333"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 71,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# SHADOW
########################################
    def get_shadow():
        girl = Chr()
        girl.unlock = persistent.shadow_unlock == 1
        girl.name = "Shadow Girl"
        girl.pedia_name = "Shadow Girl"
        girl.info = _("""A Monster of unknown lineage who appeared in the Haunted Mansion. She appears to have been birthed from the residual magic leftover from nearby studies of magic such as necromancy. Her body itself is comprised of countless souls of other beings. She continues to grow her body by taking in the souls of other beings into herself. A monster created while Chrome was experimenting with zombies, she was a hidden danger growing in the mansion.
Of course, above all, male semen is still her primary source of power, and she will seek out human men to acquire it She will give genital stimulation to encourage ejaculations in order to extract his semen. But in the end, she will still incorporate their body and soul into her own.""")
        girl.label = "shadow"
        girl.label_cg = "shadow_cg"
        girl.info_rus_img = "shadow_pedia"
        girl.m_pose = shadow_pose
        girl.zukan_ko = "shadow"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 52
        girl.max_hp = 14000
        girl.hp = 14000
        girl.skills = ((3809, "Mouth of Shadow"),
            (3810, "Breasts of Shadow"),
            (3811, "Hair of Shadow"),
            (3812, "Cross of Shadow"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 71,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# GAISTVINE
########################################
    def get_gaistvine():
        girl = Chr()
        girl.unlock = persistent.gaistvine_unlock == 1
        girl.name = "Paintgeist"
        girl.pedia_name = "Paintgeist"
        girl.info = _("""A painting located in the Haunted Mansion, where the soul of a monster with powerful magic was transferred to. Although she was created intentionally by Chrome, she didn't intend for her to be stuck in the painting itself. Chrome left her hanging there, deemed a failure.
Basically harmless, she only tries to drag in men when they interest her. If drawn inside the painting as her prey, there is no escape, and she is free to continue toying with the male forever. Since the flow of time inside the painting world is different from the real world, this rape will continue for eternity.""")
        girl.label = "gaistvine"
        girl.label_cg = "gaistvine_cg"
        girl.info_rus_img = "gaistvine_pedia"
        girl.m_pose = gaistvine_pose
        girl.zukan_ko = "gaistvine"
        girl.facenum = 8
        girl.posenum = 2
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 16000
        girl.hp = 16000
        girl.skills = ((3813, "Kiss of Desire"),
            (3814, "Sensual Handjob"),
            (3815, "Mouth of Desire"),
            (3816, "Ass of Desire"),
            (3817, "Eye of Obedience"),
            (3818, "Baroque Orgasm"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 71,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# CHROM2
########################################
    def get_chrom2():
        girl = Chr()
        girl.unlock = persistent.chrom2_unlock == 1
        girl.name = "Chrome"
        girl.pedia_name = "Chrome (2)"
        girl.info = _("""The young looking Monster who uses the Haunted Mansion as her research site. At one time she was researching zombie technology, she has switched over to ghost research. Her necromantic abilities stem from being a part of the Artiste family. She has been researching and practicing her necromantic abilities in order to bring honor and prestige back to her disgraced family.
Although she has inherited the techniques and knowledge of 2,000 years of her Artiste family history, she continues creating failures due to her inability to fine tune the finer details of her creations and her rough approach to research.
The two ghosts she is now with, Spi and Rit, are human souls able to maintain ghost form due to a new principle. Due to that, they are theoretically more of a ghost than a Monster.""")
        girl.label = "chrom2"
        girl.label_cg = "chrom2_cg"
        girl.info_rus_img = "chrom2_pedia"
        girl.m_pose = chrom2_pose
        girl.zukan_ko = "chrom2"
        girl.facenum = 7
        girl.posenum = 8
        girl.bk_num = 3
        girl.x0 = -210
        girl.x1 = -20
        girl.lv = 58
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3819, "Ghost Handjob"),
            (3820, "Ghost Blowjob"),
            (3821, "Ghost Tit Fuck"),
            (3822, "I'll Give A Handjob!"),
            (3823, "I'll Give A Blowjob!"),
            (3824, "Phantasmagoria"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 71,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# BERRYEL
########################################
    def get_berryel():
        girl = Chr()
        girl.unlock = persistent.berryel_unlock == 1
        girl.name = "Power Berryelle"
        girl.pedia_name = "Power Berryelle"
        girl.info = _("""A Power angel who belongs in the sixth circle in the angel hierarchy. Her role is to judge sinners in the Heavens, but her focus is on punishing gluttony. With her body in the shape of a sweet fruit, she will prey on any sinner who dares attempt to eat her first
She sometimes may descend to the surface in times of great disturbances, and use her predation ability to devour the enemies of Heaven without mercy. An extremely brutal and greedy angel, other angels sometimes make fun of her for being a glutton herself.""")
        girl.label = "berryel"
        girl.label_cg = "berryel_cg"
        girl.info_rus_img = "berryel_pedia"
        girl.m_pose = berryel_pose
        girl.zukan_ko = "berryel"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -140
        girl.x1 = 60
        girl.lv = 123
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3825, "Semen Sipping Mouth"),
            (3826, "Man Melting Breasts"),
            (3827, "Semen Milking Hands"),
            (3828, "Man Devouring Pomegranate"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# REVEL
########################################
    def get_revel():
        girl = Chr()
        girl.unlock = persistent.revel_unlock == 1
        girl.name = "Virtue Revel"
        girl.pedia_name = "Virtue Revel"
        girl.info = _("""A Virtue angel who belongs in the fifth circle in the angel hierarchy. Her ability lies in being able to reincarnate the souls of the dead into new existences. She can reincarnate favored individuals using their own genetic information. Or, she can reincarnate unfavorable people into a completely new existence. Due to her special abilities, she receives many special missions directly from llias.
When she has to reincarnate someone still living, she will first melt down their body. The intense pleasure felt during this process will bring the reincarnee great joy in being reincarnated.""")
        girl.label = "revel"
        girl.label_cg = "revel_cg"
        girl.info_rus_img = "revel_pedia"
        girl.m_pose = revel_pose
        girl.zukan_ko = "revel"
        girl.facenum = 1
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 129
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3829, "Holy Umbilical Bond"),
            (3830, "Holy Return"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# GNOME2
########################################
    def get_gnome2():
        girl = Chr()
        girl.unlock = persistent.hsean_seirei1 == 1
        girl.name = "Gnome"
        girl.pedia_name = "Gnome (2)"
        girl.info = _("""The stoic spirit of earth. Her mind and body now reside inside Luka's, and she lends him her power. Though she is made up of dark magic and is similar to a Monster in many ways, her mind and soul itself are borne from mother nature. Up until several hundred years ago, she was worshipped as a God of the earth in the Safaru Region. After worship of llias became mainstream, she was mostly forgotten. Due to that, she gets lonely very easily.
A lover of quiet and calm, she frequently smacks the noisy Sylph. That smack itself seems to be Gnome's way of showing friendship, too. Though you must never forget that she could truly be irritated and smacking you to show it""")
        girl.label = "gnome2"
        girl.label_cg = "gnome2_cg"
        girl.info_rus_img = "gnome2_pedia"
        girl.m_pose = gnome2_pose
        girl.zukan_ko = "gnome2"
        girl.zukan_ko = None
        girl.facenum = 9
        girl.posenum = 4
        girl.x0 = -200
        girl.lv = 50
        girl.max_hp = 4000
        girl.hp = 4000
        girl.echi = ["lb_0340"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 0,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# C_CHARIOT
########################################
    def get_c_chariot():
        girl = Chr()
        girl.unlock = persistent.hsean_etc4 == 1
        girl.name = "Chimera Chariot"
        girl.pedia_name = "Chimera Chariot"
        girl.info = _("""A Chimeric Monster created by Promestein. A so called "Horseback Model", the intent in her creation was to replicate the ancient huge biological weapons. The body on top is the control for this huge Monster. A reckless attempt at creating something big and powerful, it was surprisingly effective in that the upper body of this Monster retained high control of the lower, larger body. After obtaining experimental data, she was incorporated into the Sabasa assault forces.
Due to her large bulk, she requires a large amount of energy to maintain her bodily functions. After squeezing a man's semen, she will then consume their body itself. The hearts of the soldiers facing her waver as she preys on them during the battle itself.""")
        girl.label = "c_chariot"
        girl.label_cg = "c_chariot_cg"
        girl.info_rus_img = "c_chariot_pedia"
        girl.m_pose = c_chariot_pose
        girl.zukan_ko = "c_chariot"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 60
        girl.max_hp = 19000
        girl.hp = 19000
        girl.echi = ["lb_0340a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# CARMILLA
########################################
    def get_carmilla():
        girl = Chr()
        girl.unlock = persistent.carmilla_unlock == 1
        girl.name = "Carmilla"
        girl.pedia_name = "Carmilla"
        girl.info = _("""A high ranking Vampire that is one of the two aides to the Queen Vampire. Proud of her magical power, equivalent to a Queen's, she assists the Queen Vampire as her right hand. In addition, she was granted the power of a fake spirit from Black Alice, further increasing her power.
Though she seems calm and gentle on the outside, she is merciless against those who would challenge her or the Queen. It is said that she will coldheartedly suck out both the semen and energy from those who challenge her. Though if her opponent isn't hostile to her, she won't kill them. If a submissive attitude is shown, she will simply capture the male as feed.
A high-ranking Vampire's cloak is a part of their body, and can be freely manipulated like other Monster's tentacles. In addition, she is able to infuse it with magic to suck her prey's blood. If wrapped up in her cloak, both the male's semen and blood can be drawn out with ease.""")
        girl.label = "carmilla"
        girl.label_cg = "carmilla_cg"
        girl.info_rus_img = "carmilla_pedia"
        girl.m_pose = carmilla_pose
        girl.zukan_ko = "carmilla"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 56
        girl.max_hp = 19000
        girl.hp = 19000
        girl.skills = ((3831, "Carmilla's Fingering"),
            (3832, "Carmilla's Mouthing"),
            (3833, "Carmilla's Milking"),
            (3834, "Carmilla's Night Clothing"),
            (3835, "Carmilla's Blood Sucking"),
            (3836, "Graveyard of Lilys"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# ELISABETH
########################################
    def get_elisabeth():
        girl = Chr()
        girl.unlock = persistent.elisabeth_unlock == 1
        girl.name = "Elizabeth"
        girl.pedia_name = "Elizabeth"
        girl.info = _("""A high ranking Vampire that is one of the two aides to the Queen Vampire. Along with Carmilla, they are known as the Queen's Royal Guards. She also was granted a fake spirit by Black Alice, further increasing her power.
Despite her appearance, she is several hundred years old, and very arrogant But considering herself a noble, she doesn't torment the weak and powerless for no reason. But opponents who challenge her, prepared for the outcome, are shown no mercy as she sucks them dry.
Like other high-ranking vampires, she can control her cloak freely. Infusing her cloak with magic, she can use it to absorb semen and energy from her prey just as easily as if it was her mouth.""")
        girl.label = "elisabeth"
        girl.label_cg = "elisabeth_cg"
        girl.info_rus_img = "elisabeth_pedia"
        girl.m_pose = elisabeth_pose
        girl.zukan_ko = "elisabeth"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 58
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3837, "Elizabeth's Captivating Hand"),
            (3838, "Elizabeth's Attending Mouth"),
            (3839, "Elizabeth's Admiring Breasts"),
            (3840, "Elizabeth's Bloodsucking"),
            (3841, "Bathory's Cradle"),
            (3842, "Transylvania Midnight"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 72,
            "skill01": 19,
            "skill02": 0,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
        }
        return girl

########################################
# QUEENVANPIRE
########################################
    def get_queenvanpire():
        girl = Chr()
        girl.unlock = persistent.queenvanpire_unlock == 1
        girl.name = "Queen Vampire"
        girl.pedia_name = "Queen Vampire"
        girl.info = _("""The Queen of all Vampires, ruling over the night world. Wielding powerful magic and great majesty about her, the Queen's subordinates all kneel before her. In addition, it seems her biggest ambition is to build a Kingdom of Vampires.
She will not raise her hand toward those who follow her, but will mercilessly kill those who resist Raping her opponent, she will suck out their blood and semen until they are exhausted. One who is sucked dry by the Queen herself will feel the finest of pleasures as they die.""")
        girl.label = "queenvanpire"
        girl.label_cg = "queenvanpire_cg"
        girl.info_rus_img = "queenvanpire_pedia"
        girl.m_pose = queenvanpire_pose
        girl.zukan_ko = "queenvanpire"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 75
        girl.max_hp = 21000
        girl.hp = 21000
        girl.skills = ((3843, "Erotic Fingers"),
        (3844, "Lascivious Tongue"),
        (3845, "Dream Milking"),
        (3846, "Nocturnal Bloodsucker"),
        (3847, "Devilish Membrane"),
        (3848, "Moonlight Nocturne"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 73,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# C_HOMUNCULUS
########################################
    def get_c_homunculus():
        girl = Chr()
        girl.unlock = persistent.c_homunculus_unlock == 1
        girl.name = "Chimera Homunculus"
        girl.pedia_name = "Chimera Homunculus"
        girl.info = _("""Based on Promestein's research, this Monster was created by Lucia. This Chimera is a mix of state of the art biological sciences and old fashioned alchemy. Her ability and intelligence are drastically improved from a normal homunculus, and her power rivals that of top class Monsters.
She submits completely to the orders of her master, but there are no linked ethical standards seen. Her tentacles are able to wrap around her prey and directly suck out semen to feed on. Due to the pleasure that's given to the male, they will be forced to ejaculate one way or another. She doesn't have any hesitation in killing her prey, but due to her preference for drawing out a large amount of semen over an extended time, there have been no reported deaths in Witch Hunt Village due to her actions.""")
        girl.label = "c_homunculus"
        girl.label_cg = "c_homunculus_cg"
        girl.info_rus_img = "c_homunculus_pedia"
        girl.m_pose = c_homunculus_pose
        girl.zukan_ko = "c_homunculus"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3849, "EX Tentacle"),
        (3850, "EX Bust"),
        (3851, "EX Drain Blowjob"),
        (3852, "Alchemist Tentacle"),
        (3853, "Alchemist Euria"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 73,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# IRONMAIDEN_K
########################################
    def get_ironmaiden_k():
        girl = Chr()
        girl.unlock = persistent.ironmaiden_k_unlock == 1
        girl.name = "Iron Maiden v2"
        girl.pedia_name = "Iron Maiden v2"
        girl.info = _("""Originally a homunculus created by Lily, she escaped from the mansion during the confusion and started attacking travelers. After being captured and improved upon by Lucia, she started using her as a subordinate.
After attacking numerous travelers, she has gained a simple intelligence. Accepting her existence as a tool for torture, she derives meaning from her life in squeezing criminals to death. If caught by the Iron Maiden, your semen will be continually squeezed out until the moment of your death.""")
        girl.label = "ironmaiden_k"
        girl.label_cg = "ironmaiden_k_cg"
        girl.info_rus_img = "ironmaiden_k_pedia"
        girl.m_pose = ironmaiden_k_pose
        girl.zukan_ko = "ironmaiden_k"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 57
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3854, "Dream Spider"),
        (3855, "Sweet Pinchers"),
        (3856, "Cave of Roses"),
        (3857, "Sadistic Iron Maiden"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 73,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# LUSIA
########################################
    def get_lusia():
        girl = Chr()
        girl.unlock = persistent.lusia_unlock == 1
        girl.name = "Lucia"
        girl.pedia_name = "Lucia"
        girl.info = _("""A woman, born in Witch Hunt Village, who studied at the National University in Sabasa. She returned to her hometown after reaching the top of her class in Magical Science studies, but was caught by Lily. She lost her sanity once Lily implanted a worm inside her body, but regained her senses once Lily's magical power was cut off. After that, she escaped the village and distrusted humans. She eventually came into contact with Promestein, and became her subordinate.
Though born with a powerful innate ability for magic, her skills were even more refined during her time at the University. After being under Promestein's tutelage for a short while, she was even able to create her own version of Homunculi with her acquired knowledge.
Holding Promestein's trust, the assault of Witch Hunt Village was left to her.
But still conflicted deep in her heart about her hometown, the attack on Witch Hunt Village seems to not have been too thorough after she swallowed Lily whole.""")
        girl.label = "lusia"
        girl.label_cg = "lusia_cg"
        girl.info_rus_img = "lusia_pedia"
        girl.m_pose = lusia_pose
        girl.zukan_ko = "lusia"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 88
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3858, "Tentacle Worm"),
        (3859, "Drain Worm"),
        (3860, "Anal Drain"),
        (3861, "Ecstasy Worm"),
        (3862, "Paralyzing Worm"),
        (3863, "Boa Worm"),
        (3864, "Meltick Drain"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 73,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# SILKIEL
########################################
    def get_silkiel():
        girl = Chr()
        girl.unlock = persistent.silkiel_unlock == 1
        girl.name = "Virtue Silkiel"
        girl.pedia_name = "Virtue Silkiel"
        girl.info = _("""A Virtue angel who falls in the fifth circle in the angel hierarchy. Silkiel's primary role is wrapping souls (whom have lost their body due to some casualty) in her silk, and sending them off to heaven. Despite being a high-ranking angel, her duties seem to take her to the surface world quite often.
Holy energy infuses the silky threads she uses for her cocoons, and is effective at sending souls to heaven in a fit of ecstasy. Should a flesh and blood human be caught in her cocoon, they will relax in ecstasy as they reach continuous orgasms. Normally this wouldn't be allowed, but it's a different story if the flesh and blood human is a sinner. Though judgment of sinners is not her normal duty, she seems to have held a secret desire to judge and dispose sinners with her cocoon one day.""")
        girl.label = "silkiel"
        girl.label_cg = "silkiel_cg"
        girl.info_rus_img = "silkiel_pedia"
        girl.m_pose = silkiel_pose
        girl.zukan_ko = "silkiel"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -110
        girl.x1 = 90
        girl.lv = 128
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3865, "Flowing Blowjob"),
        (3866, "Flowing Tit Fuck"),
        (3867, "Feather Feel"),
        (3868, "Silk-Spinning Intercourse"),
        (3869, "Silky Tide"),
        (3870, "Silky Rose Heaven"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 73,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# ENDIEL
########################################
    def get_endiel():
        girl = Chr()
        girl.unlock = persistent.endiel_unlock == 1
        girl.name = "Dominion Endiel"
        girl.pedia_name = "Dominion Endiel"
        girl.info = _("""A Dominion angel who falls in the fourth circle in the angel hierarchy. Her main role is to ascend Heroic and Saintly souls to the Heavens, and rewarding them with pleasure. Though she is sometimes derided as the "Angel Whore", she herself treats it as a respectable title.
Due to her work, even the most stoic of souls will go mad in the intense pleasure from her skills. Sometimes the souls enjoy themselves so much, that it becomes an issue for later transmigration due to them turning almost disabled. In addition, she has equal skills in pleasuring men and women, since Heroes and Saints aren't solely men.
Originally Valkyrie would be the one to guide Heroic souls to the Heavens. But due to her unpopular serious and business-like manner, that role was replaced by Endiel a few hundred years ago.""")
        girl.label = "endiel"
        girl.label_cg = "endiel_cg"
        girl.info_rus_img = "endiel_pedia"
        girl.m_pose = endiel_pose
        girl.zukan_ko = "endiel"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 132
        girl.max_hp = 21000
        girl.hp = 21000
        girl.skills = ((3871, "Mystic Whore's Foot"),
        (3872, "Mystic Whore's Tit Fuck"),
        (3873, "Mystic Whore's Ass"),
        (3874, "Mystic Whore's Blowjob"),
        (3875, "Mystic Whore's Vagina"),
        (3876, "Hold of Sexual Pleasures"),
        (3877, "Dance of the Membrane"),
        (3878, "Membrane's Requiem"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 0,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# SYLPH2
########################################
    def get_sylph2():
        girl = Chr()
        girl.unlock = persistent.hsean_seirei2 == 1
        girl.name = "Sylph"
        girl.pedia_name = "Sylph (2)"
        girl.info = _("""The spirit of wind. Small in intelligence, but big in dreams. Taking on her spirit form, she again resides in Luka's mind. Her magical power is huge, but due to her low offensive skill, she can't manage her own abilities. But with a skilled summoner, she can let loose her nearly limitless power. Her link with her host is temporarily cut off when the host falls asleep, goes into a rage, or is overcome by lust If that occurs, the host can't hear Sylph, nor can she sense what's happening with the host
Greatly in tune with nature, if Sylph is injured the weather all over the world will be severely impacted and disastrous for the world. Due to that, the army of the Heavens has taken great care in how they deal with her.""")
        girl.label = "sylph2"
        girl.label_cg = "sylph2_cg"
        girl.info_rus_img = "sylph2_pedia"
        girl.m_pose = sylph2_pose
        girl.zukan_ko = "sylph2"
        girl.zukan_ko = None
        girl.facenum = 7
        girl.posenum = 4
        girl.x0 = -300
        girl.lv = 50
        girl.max_hp = 4000
        girl.hp = 4000
        girl.echi = ["lb_0352"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 74,
            "skill01": 19,
            "skill02": 0,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# TYPHON
########################################
    def get_typhon():
        girl = Chr()
        girl.unlock = persistent.hsean_etc5 == 1
        girl.name = "Typhon Girl"
        girl.pedia_name = "Typhon Girl"
        girl.info = _("""A Chimeric Monster created by Promestein. She was created in hopes to replicate the ancient Giganto Weapon, but her power didn't come close. As a result, she decided that a large amount of general purpose weapons was better than a single, powerful one in terms of a cost to force ratio. With the mass production of Chimeric Monsters started, Promestein abandoned attempts to create a single, ultimate Chimera.
Though the attack was derailed, she was assigned to be a part of the Grand Noah assault forces.
Since her huge bulk requires a huge amount of energy, she terrorized the human soldiers by squeezing out their semen and eating them.""")
        girl.label = "typhon"
        girl.label_cg = "typhon_cg"
        girl.info_rus_img = "typhon_pedia"
        girl.m_pose = typhon_pose
        girl.zukan_ko = "typhon"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 57
        girl.max_hp = 18000
        girl.hp = 18000
        girl.echi = ["lb_0352a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 74,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# LAMIANLOID
########################################
    def get_lamianloid():
        girl = Chr()
        girl.unlock = persistent.lamianloid_unlock == 1
        girl.name = "Lamiaroid"
        girl.pedia_name = "Lamiaroid"
        girl.info = _("""A Chimeric Monster created by Promestein as a mix of machine and biological parts. Using a Lamia as a base, she remodeled different parts of her body to drastically increase her power.
In order to test the ability with which she could control her mechanical parts, Promestein equipped her with forty-eight different functions to extract semen. But since Lamiaroid herself doesn't know how to use them all, she generally sticks to five main units. Their effect is incredibly powerful, and she is even able to extract from more than one man at a time.
After obtaining basic data, she was assigned to the assault forces of Grand Noah. Since she kept attacking random men without regards to tactics, she didn't contribute that much to the assault.""")
        girl.label = "lamianloid"
        girl.label_cg = "lamianloid_cg"
        girl.info_rus_img = "lamianloid_pedia"
        girl.m_pose = lamianloid_pose
        girl.zukan_ko = "lamianloid"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -210
        girl.lv = 75
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3879, "Lamiatail"),
            (3880, "Super Bust"),
            (3881, "Snake Tongue"),
            (4132, "Pleasure Foot"),
            (3882, "Rube Arm"),
            (3883, "Plant Arm"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 74,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# KNIGHTLOID
########################################
    def get_knightloid():
        girl = Chr()
        girl.unlock = persistent.knightloid_unlock == 1
        girl.name = "Knightroid"
        girl.pedia_name = "Knightroid"
        girl.info = _("""A mass production version of the Next Doll Arc-En- Ciel created by Promestein. Though her combat abilities are high even for a Chimeric Monster, she still doesn't compare to Arc-En-Ciel. Even though she was a simplified version of Arc-En-Ciel, the production costs were too huge to make more than one of her.
Her main weapons include her huge spear, and a biological whip in her left hand. In addition, she is well trained in sexual skills, in order to pin any soldier that tries to fight against her.
Created as a soldier in mind, she has a deep respect for Chivalry, and looks up to Arc-En-Ciel. On the other hand, she feels extreme contempt for the weak, and is said to thoroughly humiliate anyone she deems unworthy.""")
        girl.label = "knightloid"
        girl.label_cg = "knightloid_cg"
        girl.info_rus_img = "knightloid_pedia"
        girl.m_pose = knightloid_pose
        girl.zukan_ko = "knightloid"
        girl.facenum = 5
        girl.posenum = 4
        girl.bk_num = 5
        girl.x0 = -250
        girl.x1 = -50
        girl.lv = 89
        girl.max_hp = 30000
        girl.hp = 30000
        girl.skills = ((3884, "Oral Drills"),
        (3885, "Chivalric Spear Rub"),
        (3886, "Chivalric Spear Handling"),
        (3887, "Chivalric Whip"),
        (3888, "Knightly Breast Force"),
        (3889, "Knightly Breast Milking"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# AKANAME
########################################
    def get_akaname():
        girl = Chr()
        girl.unlock = persistent.akaname_unlock == 1
        girl.name = "Akaname"
        girl.pedia_name = "Akaname"
        girl.info = _("""An indigenous monster to the Yamatai Region. Apart from their extremely long tongues, they are very human-like. Black Alice endowed them with extra physical strength, drastically increasing their combat abilities. Although their intelligence isn't too low, they are sometimes teased and ridiculed for their happy-go-lucky attitude when it comes to licking any man they can. They seem to have gotten involved in the current situation without really knowing what was going on.
Loving human sweat, semen, and any dirt on a human's body, when they find suitable prey they will continue licking them clean. Their long tongues are quickly able to send any man into the throes of agonized pleasure. They won't ever lick their prey to death, but the intense sessions can leave men unable to stand for days. If attacked by a group of Akaname, they will take turns licking their prey until he is completely dry.""")
        girl.label = "akaname"
        girl.label_cg = "akaname_cg"
        girl.info_rus_img = "akaname_pedia"
        girl.m_pose = akaname_pose
        girl.zukan_ko = "akaname"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 53
        girl.max_hp = 15000
        girl.hp = 15000
        girl.skills = ((3890, "Akaname Entangling"),
        (3891, "Akaname Squeeze"),
        (3892, "Akaname Sucking"),
        (3893, "Akaname Nipple Crawl"),
        (3894, "Akaname Ball Lick"),
        (3895, "Akaname Anal Lick"),
        (3896, "Akaname Triple Entanglement"),
        (3897, "Akaname Licking Kiss"),
        (3898, "Akaname Tongue Garden"),
        (3899, "Akaname Tongue Hell"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# MIKOLAMIA
########################################
    def get_mikolamia():
        girl = Chr()
        girl.unlock = persistent.mikolamia_unlock == 1
        girl.name = "Miko Lamia"
        girl.pedia_name = "Miko Lamia"
        girl.info = _("""A young, neat Lamia that acts as a Shrine Maiden in Yamatai Village. Though she is known and accepted in the village, she harbored a secret desire to rape men whenever she wanted. After being flattered by Black Alice, she decided to turn against the villagers. Though she showed a face of purity, she hid dark sexual desires.
Her sexual torture is very intense, toying with men after completely winding around them with her snake lower body. She will squeeze both her vagina and her tail around her prey, forcing them to come over and over until she is satisfied.""")
        girl.label = "mikolamia"
        girl.label_cg = "mikolamia_cg"
        girl.info_rus_img = "mikolamia_pedia"
        girl.m_pose = mikolamia_pose
        girl.zukan_ko = "mikolamia"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 16000
        girl.hp = 16000
        girl.skills = ((3900, "Ten Finger Masturbation"),
        (3901, "Snake Tongue"),
        (3902, "Abundant Breasts"),
        (3903, "Divining Tail"),
        (3904, "Ninety Inch Tongue Wrap"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# KEZYOROU
########################################
    def get_kezyorou():
        girl = Chr()
        girl.unlock = persistent.kezyorou_unlock == 1
        girl.name = "Kejourou"
        girl.pedia_name = "Kejourou"
        girl.info = _("""A long-haired monster who left Yamatai village for the mountains alongside Shirohebi. A malicious, stubborn monster, she enjoys toying with me using her sexual hair skills. Though she does dislike killing, so she doesn't play with them until they die. After finishing with the man, she usually leaves them abandoned on the roadside.
She sometimes disguises herself as a prostitute to seduce men. In the middle of intercourse, she finds great humor in revealing her true monster character.""")
        girl.label = "kezyorou"
        girl.label_cg = "kezyorou_cg"
        girl.info_rus_img = "kezyorou_pedia"
        girl.m_pose = kezyorou_pose
        girl.zukan_ko = "kezyorou"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 58
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((3905, "Geisha's Masturbation"),
        (3906, "Flutist's Playing"),
        (3907, "Flower Girl's Breasts"),
        (3908, "Indecent Hair"),
        (3909, "Ultimate Hair Charm"),
        (3910, "Courtesan's Pride"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# SIROHEBISAMA
########################################
    def get_sirohebisama():
        girl = Chr()
        girl.unlock = persistent.sirohebisama_unlock == 1
        girl.name = "Shirohebi"
        girl.pedia_name = "Shirohebi"
        girl.info = _("""Once departing Yamatai Village for the mountains, she returned leading a band of monsters to attack it. She was recruited by Black Alice in order to take over Yamatai Village. Since she intended to rule over the village, and not destroy it, she avoided widespread slaughter in the attack.
Both sisters seem to be related to the late Queen Lamia. Directly following the Queen Lamia's unexpected death, though her name was originally put forward as a successor, a large fight broke out over who would claim the seat. Due to this repeated rivalry for the position, the seat of Queen Lamia is still vacant.""")
        girl.label = "sirohebisama"
        girl.label_cg = "sirohebisama_cg"
        girl.info_rus_img = "shirohebisama_pedia"
        girl.m_pose = sirohebisama_pose
        girl.zukan_ko = "sirohebisama"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 70
        girl.max_hp = 21000
        girl.hp = 21000
        girl.skills = ((3911, "Boy Squeezing"),
        (3912, "Ascending Suck"),
        (3913, "Breast Pressure"),
        (3914, "Snake Squeeze"),
        (3915, "Shirohebi's Comforting"),
        (3916, "Snake Mating"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 74,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# WALRAUNE
########################################
    def get_walraune():
        girl = Chr()
        girl.unlock = persistent.walraune_unlock == 1
        girl.name = "Walraune"
        girl.pedia_name = "Walraune"
        girl.info = _("""Originally a peace loving Alraune, her personality turned cruel and greedy after she was brainwashed. Just like Queen Alraune, she suddenly changed and attacked Plansect Forest. She seems to have captured numerous plant and insect monsters, and sucked on their nutrients.
Also loving human semen, she will gladly take any chance she can get to treat herself on their fluids. She will mercilessly extract their semen with her petals, until they are left withered. Many men have happily fainted from exhaustion while watering her with their semen.""")
        girl.label = "walraune"
        girl.label_cg = "walraune_cg"
        girl.info_rus_img = "walraune_pedia"
        girl.m_pose = walraune_pose
        girl.zukan_ko = "walraune"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 17000
        girl.hp = 17000
        girl.skills = ((3924, "Confusing Pollen Handjob"),
        (3925, "Paralyzing Sap Blowjob"),
        (3926, "Ecstasy Sap Tit Fuck"),
        (3927, "Seduction Pollen Hair"),
        (3928, "Hell Tentacle"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 75,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 0,
        "item01": 10
        }
        return girl

########################################
# DRYAD
########################################
    def get_dryad():
        girl = Chr()
        girl.unlock = persistent.dryad_unlock == 1
        girl.name = "Dryad"
        girl.pedia_name = "Dryad"
        girl.info = _("""A normally kind-hearted plant monster. She was slow to be impacted by the brainwashing (dimwitted?), and looked on in confusion as her Queen and friends slowly changed. But if the brainwashing magic is given a chance to manifest itself, her personality suddenly turns into one of brutality.
She is able to trail her roots over an organism, and suck them dry very quickly. Since this process is quick and merciless, she generally doesn't do it... But when her personality changes, she uses it without hesitation. Having their energy quickly drained like this brings men and women alike to fast orgasms. If she's draining a man, they will ejaculate one time after another while writhing in ecstasy.""")
        girl.label = "dryad"
        girl.label_cg = "dryad_cg"
        girl.info_rus_img = "dryad_pedia"
        girl.m_pose = dryad_pose
        girl.zukan_ko = "dryad"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 57
        girl.max_hp = 22000
        girl.hp = 22000
        girl.skills = ((3917, "Arc Foot"),
            (3918, "Arc Blowjob"),
            (3919, "Arc Bust"),
            (3920, "Arc Ivy"),
            (3921, "Arc Hip Heaven"),
            (3922, "Sap of Ecstasy"),
            (3923, "Bloom of the Tree"))
        girl.echi = ["dryad_ha", "dryad_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# QUEENALRAUNE
########################################
    def get_queenalraune():
        girl = Chr()
        girl.unlock = persistent.queenalraune_unlock == 1
        girl.name = "Queen Alraune"
        girl.pedia_name = "Queen Alraune"
        girl.info = _("""The Queen Alraune, reigning over all plant monsters. With powerful magic and vitality, she is known to be able to completely cover large areas with ivy. Though originally friendly and mild, the brainwashing technique turned her brutal and heartless.
Her huge body is centered by a pistil that takes the form of a human female. Using this body, she drastically increases her ability to fertilize herself with men. Her pistil is complete with large breasts, and an entrance similar to a female vagina. In other words, her female pistil evolved as a sort of lure, seen in many other non-monster plants.""")
        girl.label = "queenalraune"
        girl.label_cg = "queenalraune_cg"
        girl.info_rus_img = "queenalraune_pedia"
        girl.m_pose = queenalraune_pose
        girl.zukan_ko = "queenalraune"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 72
        girl.max_hp = 25000
        girl.hp = 25000
        girl.skills = ((3929, "Cardinal Hands"),
            (3930, "Laurean Press"),
            (3931, "Queen Flower Squeeze"),
            (3932, "Squeezing Ivy Dance"),
            (3933, "Nectar of Ecstasy"),
            (3934, "Playful Vines"),
            (3935, "Milk of Ecstasy"),
            (3936, "Ecstasy Caress"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# SLIMELORD
########################################
    def get_slimelord():
        girl = Chr()
        girl.unlock = persistent.slimelord_unlock == 1
        girl.name = "Slime Bess"
        girl.pedia_name = "Slime Bess"
        girl.info = _("""A low class, predatory slime. An extremely self centered monster among slimes, she believes that other creatures exist only for her own simple minded enjoyment. Exploiting her unconscious evilness, Black Alice recruited her. It's believed that she has secretly attacked many men and women in secret from Erubetie.
Though she can easily survive on any sort of meat, she finds devouring humans to be the most fun. Covering her prey with her entire body, the digesting slime paralyzes them with extreme pleasure as she preys on them.""")
        girl.label = "slimelord"
        girl.label_cg = "slimelord_cg"
        girl.info_rus_img = "slimelord_pedia"
        girl.m_pose = slimelord_pose
        girl.zukan_ko = "slimelord"
        girl.facenum = 5
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -130
        girl.lv = 58
        girl.max_hp = 21000
        girl.hp = 21000
        girl.skills = ((3937, "Jelly Hands"),
            (3938, "Melty Bust"),
            (3939, "Slimy Hips"),
            (3940, "Slime Girl's Slimy Blowjob"),
            (3941, "Amoeba Haze"),
            (3942, "Amoeba Drain"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# EGGEL
########################################
    def get_eggel():
        girl = Chr()
        girl.unlock = persistent.eggel_unlock == 1
        girl.name = "Throne Eggiel"
        girl.pedia_name = "Throne Eggiel"
        girl.info = _("""A Throne angel, belonging to the third circle of angels. Her mission is to use human bodies to fertilize her huge egg, to create more angels.
Like sperm, she uses a human's entire body to fertilize one of her eggs. Men who are chosen to act as sperm for her egg are slowly taken inside, their last moments one of pure ecstasy as their genes are used for the creation of a new angel. Since the human used as fertilizer is generally female, pure female angels are overwhelmingly born. However if a human male is used as fertilizer, in theory it's possible for a hermaphrodite angel to be born.""")
        girl.label = "eggel"
        girl.label_cg = "eggel_cg"
        girl.info_rus_img = "eggel_pedia"
        girl.m_pose = eggel_pose
        girl.zukan_ko = "eggel"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -150
        girl.x1 = 50
        girl.lv = 135
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3943, "Angel Hair"),
            (3944, "Angel Feathers"),
            (3945, "Angel Mouth"),
            (3946, "Meltick Bubble"),
            (3947, "Felty Bubble"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# SALAMANDER2
########################################
    def get_salamander2():
        girl = Chr()
        girl.unlock = persistent.hsean_seirei3 == 1
        girl.name = "Salamander"
        girl.pedia_name = "Salamander (2)"
        girl.info = _("""A spirit of fire, known for her temper and high martial abilities. Though a monster, she is also a force of nature. If her body itself is destroyed, the chain reactions would cause global catastrophes, thus the armies of Heaven don't dare to actually kill her.
Though very prideful, she will follow someone with extreme loyalty in the rare case of acknowledging them worthy. She views Undine as more of a rival than as an enemy, their arguments never going past simple fist fights or arguments.""")
        girl.label = "salamander2"
        girl.label_cg = "salamander2_cg"
        girl.info_rus_img = "salamander2_pedia"
        girl.m_pose = salamander2_pose
        girl.zukan_ko = "salamander2"
        girl.zukan_ko = None
        girl.facenum = 4
        girl.posenum = 3
        girl.x0 = -200
        girl.lv = 85
        girl.max_hp = 16000
        girl.hp = 16000
        girl.echi = ["lb_0367"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 0,
            "item01": 10
            }
        return girl

########################################
# TUTIGUMO
########################################
    def get_tutigumo():
        girl = Chr()
        girl.unlock = persistent.tutigumo_unlock == 1
        girl.name = "Tsuchigumo"
        girl.pedia_name = "Tsuchigumo"
        girl.info = _("""A spider monster from the Yamatai Region. Following the Spider Princess's call, she joined in the attack on Grangold. Though her demeanor is soft, her heart is brutal and cold blooded. If caught in her web, you can expect to be sucked dry.
Her already high magic ability among insect monsters was increased further by Black Alice. She is even able to create a sort of alternate dimension, trapping prey in her own private space. The only way out is to either defeat her, or be preyed on.""")
        girl.label = "tutigumo"
        girl.label_cg = "tutigumo_cg"
        girl.info_rus_img = "tutigumo_pedia"
        girl.m_pose = tutigumo_pose
        girl.zukan_ko = "tutigumo"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 55
        girl.max_hp = 19000
        girl.hp = 19000
        girl.skills = ((3948, "Eight Foot Stroke"),
            (3949, "Pinching Monster Breasts"),
            (3950, "Silky Blowjob"),
            (3951, "Breast Milk of Passion"),
            (3952, "Male Binding Silk"),
            (3953, "Male Slurping Vagina"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# ALAKNELOAD
########################################
    def get_alakneload():
        girl = Chr()
        girl.unlock = persistent.alakneload_unlock == 1
        girl.name = "Arachne Lord"
        girl.pedia_name = "Arachne Lord"
        girl.info = _("""A spider monster serving the Spider Princess. One of the most influential of the Arcahne family, her ability was on full display in the assault on Grangold. Using her superior physical agility, she defeated swarms of enemies in one fell swoop. Her victims in the battle of Grangold alone tally over three hundred.
Without exception, members of the Arachne family are cold blooded and ruthless. Regardless of race or gender, she will forcefully entrap them in her web, and suck her prey dry. Other monsters greatly dislike members of the Arachne family for their indiscriminate feeding habits.""")
        girl.label = "alakneload"
        girl.label_cg = "alakneload_cg"
        girl.info_rus_img = "alakneload_pedia"
        girl.m_pose = alakneload_pose
        girl.zukan_ko = "alakneload"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 59
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3954, "Arachne Fellatio"),
            (3955, "Arachne Bust"),
            (3956, "Arachne Spinneret"),
            (3957, "Arachne Dibble"),
            (3958, "Arachne Ruin"))
        girl.echi = ["alakneload_ha", "alakneload_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 75,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# KUMONOMIKO
########################################
    def get_kumonomiko():
        girl = Chr()
        girl.unlock = persistent.kumonomiko_unlock == 1
        girl.name = "Spider Princess"
        girl.pedia_name = "Spider Princess"
        girl.info = _("""The new Queen of Spiders, reigning over all other spider monsters. Proud of her incredible physical and magical prowess, she is also a user of Yamatai native Yin and Yang magic.
A brutal character, she will mercilessly devour anyone she deems prey, regardless of race or gender. Especially brutal towards men, she will first rape them until exhaustion, then feed on them without ever disguising her intentions. Due to her incredible power, she is regarded as extremely dangerous that will never stop preying on others.
A member of the Arachne family, she too is detested by other monsters of all types. As a result, the only ones who follow her act as she does, further ostracizing her group from the rest of the monsters.""")
        girl.label = "kumonomiko"
        girl.label_cg = "kumonomiko_cg"
        girl.info_rus_img = "kumonomiko_pedia"
        girl.m_pose = kumonomiko_pose
        girl.zukan_ko = "kumonomiko"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 75
        girl.max_hp = 23000
        girl.hp = 23000
        girl.skills = ((3959, "Arachne Thread"),
        (3960, "Ecstasy Blow"),
        (3961, "Poison Breasts"),
        (3962, "Rust Thread"),
        (3963, "Arachne Repro"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 75,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# NARCUBUS
########################################
    def get_narcubus():
        girl = Chr()
        girl.unlock = persistent.narcubus_unlock == 1
        girl.name = "Naccubus"
        girl.pedia_name = "Naccubus"
        girl.info = _("""A low-class, wandering Succubus. She hid her true nature, and worked at the town Church, learning basic medicine. She led a hungry life, only feeding on the occasional patient in secret. She joined the attack on Succubus Village after being granted power by Black Alice. She came with the intention of taking some men as her own property. Rather than evil, she seems to be driven purely by greed. A rather simple and carefree monster, she was easily swayed by a little bit of flattery, without too much thought on her part.
Due to her position in the medical field in disguise, she developed extensive skills in making patients "accidentally" ejaculate during medical examinations.""")
        girl.label = "narcubus"
        girl.label_cg = "narcubus_cg"
        girl.info_rus_img = "narcubus_pedia"
        girl.m_pose = narcubus_pose
        girl.zukan_ko = "narcubus"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 53
        girl.max_hp = 19000
        girl.hp = 19000
        girl.skills = ((3964, "Erection Check"),
        (3965, "Oral Penis Examination"),
        (3966, "Stepping Reaction Check"),
        (3967, "Tail Extraction Method"),
        (3968, "Syringe Extraction Method"),
        (3969, "Vaginal Male Examination Method A"),
        (3970, "Vaginal Male Examination Method B"),
        (4135, "Tail Semen Ejection Test"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# INPS
########################################
    def get_inps():
        girl = Chr()
        girl.unlock = persistent.inps_unlock == 1
        girl.name = "Imps"
        girl.pedia_name = "Imp (2)"
        girl.info = _("""An Imp who lives in the Monster Lord's Castle. Wielding no powers outside of her overly large breasts, she swore to avenge her humiliation at Luka's hands. She traveled to the Succubus Village with two friends to train, but got caught up in the incidents there. Meeting Luka, she challenged him without growing in power at all. She has never met Black Alice, much less given her support.
An incredibly weak monster, there's almost no threat unless you're overconfident and succumb to her taunts or invitations. No matter how weak she is, you must never forget that she can prey on men if they let her weaken them first.""")
        girl.label = "inps"
        girl.label_cg = "inps_cg"
        girl.info_rus_img = "inps_pedia"
        girl.m_pose = inps_pose
        girl.zukan_ko = "inps"
        girl.facenum = 4
        girl.posenum = 3
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 4
        girl.max_hp = 50
        girl.hp = 50
        girl.skills = ((4129, "Imp Handjob"),
            (4130, "Imp Blowjob"),
            (4131, "Imp Paizuri"))
        girl.echi = ["inps_ha", "inps_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 76,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# EVA
########################################
    def get_eva():
        girl = Chr()
        girl.unlock = persistent.eva_unlock == 1
        girl.name = "Eva"
        girl.pedia_name = "Eva"
        girl.info = _("""Originally a low level Succubus, she led the assault on Succubus Village. She appears to have lived a hard life as a wandering Succubus, feeding on men by disguising herself as a Nun and a Prostitute. Jumping at Black Alice's proposal, she showed extremely high aptitude for the fake spirits. Utilizing an ancient magic seal, she led the forces in attacking the village.
Suffering from an inferiority complex due to her hard life, she greedily covets men without holding back. If she catches you, relentless torture is sure to follow. Eva will slowly draw out the man's semen, insulting them as they dry out and perish.""")
        girl.label = "eva"
        girl.label_cg = "eva_cg"
        girl.info_rus_img = "eva_pedia"
        girl.m_pose = eva_pose
        girl.zukan_ko = "eva"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 9
        girl.x0 = -200
        girl.lv = 68
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3971, "Succubus Handjob"),
        (3972, "Succubus Tit Fuck"),
        (3973, "Succubus Blowjob"),
        (3974, "Succubus Footjob"),
        (3975, "Succubus Armpit"),
        (3976, "Succubus Intercrural"),
        (3977, "Succubus Hip Swing"),
        (3978, "Energy Drain"),
        (3979, "Succubus Ass Press"),
        (3980, "Tail Drain"),
        (3981, "Succubus Facesitting Milking"),
        (3982, "Succubus Facesitting Sucking"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# TROOPERLOID
########################################
    def get_trooperloid():
        girl = Chr()
        girl.unlock = persistent.trooperloid_unlock == 1
        girl.name = "Trooperoid"
        girl.pedia_name = "Trooperoid"
        girl.info = _("""Mass produced Chimera soldiers designed to assault and occupy human towns. With extensive use of machine technology, her body is 80\% mechanical. Brutal and ruthless, she derives great joy from oppressing others.
In addition to being able to emit shocks, her right arm is also able to be used as a high powered vacuum. Using it to harvest vast amounts of semen on the tanks on her back, it seems semen collection was another one of her missions in the assault.
Any man caught can expect their semen to be sucked up to the last drop. Even if caught as a prisoner of war, the captured prey is likely to be sucked to death.""")
        girl.label = "trooperloid"
        girl.label_cg = "trooperloid_cg"
        girl.info_rus_img = "trooperloid_pedia"
        girl.m_pose = trooperloid_pose
        girl.zukan_ko = "trooperloid"
        girl.facenum = 5
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 80
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((3985, "Exploitative Fellatio"),
        (3986, "Soft Breast Units"),
        (3987, "Vacuum Unit"),
        (3988, "Trooper Rape"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# ASSASSINLOID
########################################
    def get_assassinloid():
        girl = Chr()
        girl.unlock = persistent.assassinloid_unlock == 1
        girl.name = "Assassinroid"
        girl.pedia_name = "Assassinroid"
        girl.info = _("""A Chimeric monster developed to assassinate import people. Able to kill even Queen class monsters, she specializes in stealth and quiet attacks. In order to help her hide herself, her body comes equipped with a special unit to drop all her body heat. Though if she stays too long in this mode, she will then need a few seconds to dump all of the excess heat that has built up. In addition, a second set of genitals was created in her tentacles. Their use is unknown.
She has mastered numerous skills to frustrate any soldier used to attacking head on. Merciless rape awaits any man she defeats.""")
        girl.label = "assassinloid"
        girl.label_cg = "assassinloid_cg"
        girl.info_rus_img = "assassinloid_pedia"
        girl.m_pose = assassinloid_pose
        girl.zukan_ko = "assassinloid"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 88
        girl.max_hp = 25000
        girl.hp = 25000
        girl.skills = ((4134, "Assassin's Hand"),
        (3989, "Assassin's Blowjob"),
        (3990, "Assassin's Bust"),
        (3991, "Assassin's Ass"),
        (3992, "Assassin's Foot"),
        (3993, "Second Genital Suck"),
        (3994, "First Genital Mating"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# YOMOTU
########################################
    def get_yomotu():
        girl = Chr()
        girl.unlock = persistent.yomotu_unlock == 1
        girl.name = "Yomotsu-Shikome"
        girl.pedia_name = "Yomotsu-Shikome"
        girl.info = _("""A strange monster that resembles a mass of hair. Highly concentrated magic of dead young women masses together, creating Yomotsu- Shikome in a process similar to necromancy. She was only one of a series of Chimeric monsters created, and does not seem to have provided any particularly useful data.
Though she is still a powerful force in her own right, holding to the ability to bind any foe with her powerful hair. If captured by her hair, even the strongest of men will be left unable to break free. Like that, their energy and even their body itself will be absorbed and taken by Yomotsu- Shikome.
Lastly, though she is named Yomotsu-Shikome, it was simply a code word attached to her development process. She is not related to the actual Yomotsu-Shikome that inhabits the Yamatai mountain region.""")
        girl.label = "yomotu"
        girl.label_cg = "yomotu_cg"
        girl.info_rus_img = "yomotu_pedia"
        girl.m_pose = yomotu_pose
        girl.zukan_ko = "yomotu"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 65
        girl.max_hp = 19000
        girl.hp = 19000
        girl.skills = ((3995, "Lewd Hair Tighten"),
        (3996, "Lewd Hair Stroke"),
        (3997, "Lewd Hair Draining"),
        (3998, "3,000 Lewd Strands"),
        (4128, "Lewd Hair Hell"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# WORMIEL
########################################
    def get_wormiel():
        girl = Chr()
        girl.unlock = persistent.wormiel_unlock == 1
        girl.name = "Cherub Wormiel"
        girl.pedia_name = "Cherub Wormiel"
        girl.info = _("""A Cherub angel, belonging to the second circle of angels. Without a specific duty, she is more of a reserve force to be dispatched when the assigned angels cannot perform their mission. Exceptionally ruthless, she considers any creature that lives on the surface world to be the same as dirt.
With power beyond human comprehension, her body itself warps and degrades reality when she descends to the surface world. Even armies on the scale of thousands can be quickly eroded and destroyed by her large body. Thus she takes in massive amounts of criminals at once, and disposes them as she toys with their bodies inside her.
In addition, her central female body is comprised of worms. The collection of all the worms together is what gives Wormiel her sense of self.""")
        girl.label = "wormiel"
        girl.label_cg = "wormiel_cg"
        girl.info_rus_img = "wormiel_pedia"
        girl.m_pose = wormiel_pose
        girl.zukan_ko = "wormiel"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 140
        girl.max_hp = 23000
        girl.hp = 23000
        girl.skills = ((4000, "Worm Surface"),
        (4001, "Worm Ceres"),
        (4002, "Prey Play"),
        (4003, "Lewd Tentacle Hell"),
        (4133, "Breast Wrapping"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 76,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 0,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# UNDINE2
########################################
    def get_undine2():
        girl = Chr()
        girl.unlock = persistent.hsean_seirei4 == 1
        girl.name = "Undine"
        girl.pedia_name = "Undine (2)"
        girl.info = _("""A spirit of water, with a cold and strict personality. Though she usually lives in peace in Undine's Spring, she sympathized with Luka and lent him her power. Due to her own mastery of a Serene State, her combat capability is quite high. She seems to be at odds with Salamander most of the time inside Luka's body.
A force of nature herself, the armies of the Heavens were unwilling to kill her and risk causing global catastrophes. It's believed that Promestein's fake spirits were also intended to eventually replace the original Four Great Spirits, enabling them to be killed.""")
        girl.label = "undine2"
        girl.label_cg = "undine2_cg"
        girl.info_rus_img = "undine2_pedia"
        girl.m_pose = undine2_pose
        girl.zukan_ko = "undine2"
        girl.zukan_ko = None
        girl.facenum = 4
        girl.posenum = 3
        girl.x0 = -200
        girl.lv = 65
        girl.max_hp = 12000
        girl.hp = 12000
        girl.echi = ["lb_0382"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 76,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 0,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# ALICE4
########################################
    def get_alice4():
        girl = Chr()
        girl.unlock = persistent.hsean_alice10 == 1
        girl.name = "Alice"
        girl.pedia_name = "Alice (4)"
        girl.info = _("""Alipheese Fateburn, currently the 16th Monster Lord. Said to be the strongest of her family, even exceeding the famed Alice the Eighth.
In response to the Six Ancestors Great Seal, she substituted her main body for one of a young girl's. Since her mind was dragged into this body, it appears she has had a tough time keeping her dignity, and avoiding acting like a child.""")
        girl.label = "alice4"
        girl.label_cg = "alice4_cg"
        girl.info_rus_img = "alice4_pedia"
        girl.m_pose = alice4_pose
        girl.zukan_ko = "alice4"
        girl.zukan_ko = None
        girl.facenum = 10
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 145
        girl.max_hp = 32000
        girl.hp = 32000
        girl.echi = ["lb_0383"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 77,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# DRAINPLANT
########################################
    def get_drainplant():
        girl = Chr()
        girl.unlock = persistent.drainplant_unlock == 1
        girl.name = "Drain Plant"
        girl.pedia_name = "Drain Plant"
        girl.info = _("""An artificial monster created to hold men inside her, endlessly draining their semen.
This is a mass-produced variant based on a prototype for enlarging the womb to hold the extraction specimen. At some point, the genes were crossed with an angel's. Imbued with intelligence and a will, she is able to monitor and care for the subject inside her. They act as their own security, catching any specimens who escape.
Just like a mother caring for a baby, they supply the internal specimen with nutrients through a tube. They then force the male to ejaculate continually to harvest semen. Once taken inside, most men become crippled in around an hour, unable to do anything but come.
In addition, she is able to use harvested semen to fertilize eggs in an efficient manner.""")
        girl.label = "drainplant"
        girl.label_cg = "drainplant_cg"
        girl.info_rus_img = "drainplant_pedia"
        girl.m_pose = drainplant_pose
        girl.zukan_ko = "drainplant"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x1 = 200
        girl.lv = 68
        girl.max_hp = 18000
        girl.hp = 18000
        girl.skills = ((4004, "Plant Flirtation"),
        (4005, "Infinite Plant Fertilization"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# DRAINLOID
########################################
    def get_drainloid():
        girl = Chr()
        girl.unlock = persistent.drainloid_unlock == 1
        girl.name = "Drainroid"
        girl.pedia_name = "Drainroid"
        girl.info = _("""An artificial monster created by Promestein. An efficient machine monster created to harvest vast amounts of semen from disposable samples. She is equipped with forty eight various units to achieve this. When the re-creation was slated to begin, mass produced versions of Drainroid were expected to harvest all of the required semen in days.
Most of her body is comprised of machinery, including her brain and central nervous system. Though she wasn't created for battle, her combat ability is still quite high due to her sheer size and strength. When in self defense mode, she is still a formidable enemy.""")
        girl.label = "drainloid"
        girl.label_cg = "drainloid_cg"
        girl.info_rus_img = "drainloid_pedia"
        girl.m_pose = drainloid_pose
        girl.zukan_ko = "drainloid"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -150
        girl.lv = 85
        girl.max_hp = 23000
        girl.hp = 23000
        girl.skills = ((4006, "Handjob Arm"),
        (4007, "Semen Sucking Mouth Unit"),
        (4008, "Artificial Breast Units"),
        (4009, "Multiple Sucking Unit"),
        (4010, "Sex Simulation Unit"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# REPLICANT
########################################
    def get_replicant():
        girl = Chr()
        girl.unlock = persistent.replicant_unlock == 1
        girl.name = "Replicant"
        girl.pedia_name = "Replicant"
        girl.info = _("""A new type of Human developed for after the re-creation. She was created to strip away the undesirable things about humans that llias disliked, along with some modifications Promestein threw in. Her appearance is quite strange, but her DNA is almost exactly the same as a current human's. With both vastly increased intelligence and physical ability, she is an improvement over current humans. She is able to live on just water, and doesn't require oxygen to breathe.
For reproduction, she preys on males instead of normal mating. Females acquire genetic information by consuming males, who are then reborn. With that, the male can undergo an entire lifetime's worth of pleasure in one mating session, and give life to the next generation.
There are still some problems with her, but they were scheduled to be fixed later on, so mass production has already begun.""")
        girl.label = "replicant"
        girl.label_cg = "replicant_cg"
        girl.info_rus_img = "replicant_pedia"
        girl.m_pose = replicant_pose
        girl.zukan_ko = "replicant"
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 88
        girl.max_hp = 20000
        girl.hp = 20000
        girl.skills = ((4011, "Flats Chest"),
        (4012, "Enigma Tentacle"),
        (4013, "Tristram Hands"),
        (4014, "Predatory Mating"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# LAPLACE
########################################
    def get_laplace():
        girl = Chr()
        girl.unlock = persistent.laplace_unlock == 1
        girl.name = "Laplace"
        girl.pedia_name = "Laplace"
        girl.info = _("""A completely independent machine created by Promestein. Her body is pure machine, containing no biological life at all. With Laplace's birth, Promestein concluded an end to her creation of "Roid" type monsters.
Laplace's brain is linked to the entire Drain Lab, making the entire lab an extension of her body. With her incredible computational power, she can control numerous machines all at once, making her combat capability extremely high. Any intruder into the laboratory is beaten down, and made into a new sample.""")
        girl.label = "laplace"
        girl.label_cg = "laplace_cg"
        girl.info_rus_img = "laplace_pedia"
        girl.m_pose = laplace_pose
        girl.zukan_ko = "laplace"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 8
        girl.x0 = -200
        girl.lv = 118
        girl.max_hp = 24000
        girl.hp = 24000
        girl.skills = ((4015, "Laplace's Fingers"),
        (4016, "Three Lip Style"),
        (4017, "Ultrasmall Breast Units"),
        (4018, "S-Type Mellow Enema"),
        (4019, "L-Type Drain Hole"),
        (4020, "Artificial Pussy: LO-LI"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# AD-5
########################################
    def get_ad5():
        girl = Chr()
        girl.unlock = persistent.hsean_etc7 == 1
        girl.name = "Experimental Organism AD-5"
        girl.pedia_name = "Experimental Organism AD-5"
        girl.info = _("""A zombie created by La Croix out of the corpse of an angel. An incomplete monster created at the very start of her research into using angels in her necromantic research. Her body is covered in bandages and charms in order to stop the rapid degradation of her body. She is currently being fed a constant diet of human semen, so that changes in her mental and physical state can be recorded.
Since she was an early creation, both her physical and mental power are low. But data obtained from her experiments have led to improvements in later angelic zombies.""")
        girl.label = "ad5"
        girl.label_cg = "ad5_cg"
        girl.info_rus_img = "ad5_pedia"
        girl.m_pose = ad5_pose
        girl.zukan_ko = "ad5"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 40
        girl.max_hp = 7000
        girl.hp = 7000
        girl.echi = ["lb_0389a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 77,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# FERMESARA
########################################
    def get_fermesara():
        girl = Chr()
        girl.unlock = persistent.fermesara_unlock == 1
        girl.name = "Ferme Sara"
        girl.pedia_name = "Ferme Sara"
        girl.info = _("""An undead mummy type created by La Croix to act as a guard. More than just a normal mummy, she was created out of the corpse of a past princess of Sabasa, a descendant of Sphinx. She possesses very high physical and magical abilities, and is quite intelligent for a Mummy. Despite this, she has vowed complete obedience to La Croix.
All disposal of intruders has been left up to Ferme Sara. Without exception, all men are drained dry of semen. The former princess rapes men with her famed vagina, quickly draining them dry.""")
        girl.label = "fermesara"
        girl.label_cg = "fermesara_cg"
        girl.info_rus_img = "fermesara_pedia"
        girl.m_pose = fermesara_pose
        girl.zukan_ko = "fermesara"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -180
        girl.lv = 70
        girl.max_hp = 22000
        girl.hp = 22000
        girl.skills = ((4021, "Gaia's Handjob"),
        (4022, "Judgement Fellatio"),
        (4023, "Isis's Lovemaking"),
        (4024, "Ancient Sexual Style: Burial"),
        (4025, "Ancient Sexual Style: Rape"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# ANGELGHOUL
########################################
    def get_angelghoul():
        girl = Chr()
        girl.unlock = persistent.angelghoul_unlock == 1
        girl.name = "Angel Ghoul"
        girl.pedia_name = "Angel Ghoul"
        girl.info = _("""The latest model of angel and zombie fusion created by La Croix. Using AD-5's research data, the fusion rate was very high. Her physical strength and vitality are vastly higher than normal zombies.
But as a result of the fusion, she is in a perpetually aroused state. In order to satisfy her lust, she attacks men on sight. Drowning in her own desire, she frequently rapes men to death, without stop.
She is given free range of the underground prison area, where she rapes any human who escapes. Due to the huge increase of prisoners after the invasions, a lack of security has led to many escapees. With so much prey available, she is quite happy.""")
        girl.label = "angelghoul"
        girl.label_cg = "angelghoul_cg"
        girl.info_rus_img = "angelghoul_pedia"
        girl.m_pose = angelghoul_pose
        girl.zukan_ko = "angelghoul"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 6
        girl.x0 = -180
        girl.lv = 75
        girl.max_hp = 22000
        girl.hp = 22000
        girl.skills = ((4026, "Fallen Hand"),
        (4027, "Hungry Mouth"),
        (4028, "Discarded Breasts"),
        (4029, "Roasting Hair"),
        (4030, "Concealing Ass"),
        (4031, "Immoral Necrophilia"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# DRAGONZONBE
########################################
    def get_dragonzonbe():
        girl = Chr()
        girl.unlock = persistent.dragonzonbe_unlock == 1
        girl.name = "Zombie Dragon Girl"
        girl.pedia_name = "Zombie Dragon Girl"
        girl.info = _("""A dragon created out of the corpse of a violent dragon that lived in the volcanic area of the Gold Region. Her power has risen considerably since she was alive, and is able to use powerful techniques. She is used as the final guard of the underground prison. No human prisoner made it to her, so she only ever preyed on monster specimens.
She is able to send her own stomach outside of her body, a modification similar to a frog or starfish. Her inverted stomach takes the form of a female, and is used to prey on unsuspecting males. Its function is similar to a lure an Anglerfish Girl might use.""")
        girl.label = "dragonzonbe"
        girl.label_cg = "dragonzonbe_cg"
        girl.info_rus_img = "dragonzonbe_pedia"
        girl.m_pose = dragonzonbe_pose
        girl.zukan_ko = "dragonzonbe"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 5
        girl.lv = 87
        girl.max_hp = 24000
        girl.hp = 24000
        girl.skills = ((4032, "Dead Dragon's Fellatio"),
        (4033, "Dead Dragon's Horny Chest"),
        (4034, "Dead Dragon's Sweet Bite"),
        (4035, "Male Lure: Hair"),
        (4036, "Male Lure: Chest"),
        (4037, "Male Lure: Double"))
        girl.scope = {"ruka_tati": 2,
        "mylv": 77,
        "skill01": 19,
        "skill02": 4,
        "skill03": 4,
        "skill04": 4,
        "skill05": 4,
        "item01": 10
        }
        return girl

########################################
# CIRQUE1
########################################
    def get_cirque1():
        girl = Chr()
        girl.unlock = persistent.cirque1_unlock == 1
        girl.name = "Cirque du Croix"
        girl.pedia_name = "Cirque du Croix (1)"
        girl.info = _("""La Croix's masterpiece creation, consisting of seven zombies. The six main Cirque du Croix members were all obtained by assassination or murder. All of them have undergone intense magical treatments, and have become powerful undead monsters.
A famous Monster Swordswoman, Fernandez. Even though she is from the normally physically weak Elf race, she developed her own sword techniques that revolved around her light, small frame. It was believed she secluded herself in intense training for the remainder of her life, but in truth she was killed by La Croix.
The previous Queen Harpy waged war on the llias Continent with her mighty military. Holding an intense desire to expand her power, she waged war after war with the surrounding cities, nearly killing off the Harpy race as a result. She was believed to have been killed in a battle, but in truth was assassinated by a poison dart fired by La Croix.""")
        girl.label = "cirque1"
        girl.label_cg = "cirque1_cg"
        girl.info_rus_img = "cirque1_pedia"
        girl.m_pose = cirque1_pose
        girl.zukan_ko = "cirque1"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = "Average: 80"
        girl.max_hp = "Average: 25000"
        girl.hp = _("Average: 25000")
        girl.skills = ((4038, "Elf Fellatio"),
            (4039, "Elf Paizuri"),
            (4040, "Elf Footjob"),
            (4041, "Ass Press"),
            (4042, "Harpy's Horny Mouth"),
            (4043, "Queen's Tit Fuck"),
            (4044, "Happiness Rondo"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 77,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10,
            "ch1": ("cirque_a st02", -190, 0),
            "ch2": ("cirque_e st42", 180, 0)
            }
        return girl

########################################
# CIRQUE2
########################################
    def get_cirque2():
        girl = Chr()
        girl.unlock = persistent.cirque2_unlock == 1
        girl.name = "Cirque du Croix"
        girl.pedia_name = "Cirque du Croix (2)"
        girl.info = _("""La Croix's masterpiece creation, consisting of seven zombies. The six main Cirque du Croix members were all obtained by assassination or murder. All of them have undergone intense magical treatments, and have become powerful undead monsters.
Titania was a mutation of a normal Fairy. Born with incredible magical power, it was said that she was in tune with nature as much as an actual spirit, and able to control the weather. As a result of her powerful magical mayhem, she got on the wrong side of the current Fairy Queen. She went missing one day when she was out playing. It was believed the Queen Fairy had done something with her, but in truth she was killed by La Croix.
Queen Scylla was the Queen over all tentacle monsters. She was an incredibly powerful monster, especially for her race, but was killed by poison along with Queen Lamia. With their corpses taken soon after their death, many were accused of the crime, but none conclusively.""")
        girl.label = "cirque2"
        girl.label_cg = "cirque2_cg"
        girl.info_rus_img = "cirque2_pedia"
        girl.m_pose = cirque2_pose
        girl.zukan_ko = "cirque2"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = "Average: 80"
        girl.max_hp = "Average: 25000"
        girl.hp = "Average: 25000"
        girl.skills = ((4045, "Urethra Kiss"),
        (4046, "Fairy Hug"),
        (4047, "A Midsummer Night's Dream"),
        (4048, "Queen's Tentacle Caress"),
        (4049, "Queen's Tentacle Squeeze"),
        (4050, "Inferno de Scylla"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 77,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10,
            "ch2": ("cirque_b st12", 180, 0),
            "ch1": ("cirque_f st51", -160, 0)
            }
        return girl

########################################
# CIRQUE3
########################################
    def get_cirque3():
        girl = Chr()
        girl.unlock = persistent.cirque3_unlock == 1
        girl.name = "Cirque du Croix"
        girl.pedia_name = "Cirque du Croix (3)"
        girl.info = _("""La Croix's masterpiece creation, consisting of seven zombies. The six main Cirque du Croix members were all obtained by assassination or murder. All of them have undergone intense magical treatments, and have become powerful undead monsters.
Pirate Queen Roza was once known all across the seven seas. She lived in a different era than Selene, but the two of them are part of the "Big Three" of pirates. Her ship vanished in the middle of a voyage, and reappeared later as an empty ghost ship. In truth, La Croix used chemical weapons to kill everyone on board.
The calm Queen Lamia was known as the "Lamia of Love" due to her unusual gentle nature for a Lamia. During her reign, the frequent infighting among Lamias for the Queen's seat was finally stopped. But she was poisoned along with Queen Scylla at a banquet. Ever since, the internal infighting has resumed, causing the current seat of Queen Lamia to remain vacant.""")
        girl.label = "cirque3"
        girl.label_cg = "cirque3_cg"
        girl.info_rus_img = "cirque3_pedia"
        girl.m_pose = cirque3_pose
        girl.zukan_ko = "cirque3"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = "Average: 80"
        girl.max_hp = "Average: 25000"
        girl.hp = "Average: 25000"
        girl.skills = ((4051, "Roza's Hands"),
            (4052, "Roza's Blowjob"),
            (4053, "Roza's Firetta"),
            (4054, "Roza's Breasts"),
            (4055, "Snake Hands"),
            (4056, "Snake Mouth"),
            (4057, "Snake Bust"),
            (4058, "Snake End"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 77,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10,
            "ch2": ("cirque_c st22", -220, 0),
            "ch1": ("cirque_d st32", 180, 0)
            }

        return girl

########################################
# ALICE15TH
########################################
    def get_alice15th():
        girl = Chr()
        girl.unlock = persistent.alice15th_unlock == 1
        girl.name = "Alipheese the Fifteenth"
        girl.pedia_name = "Alipheese the Fifteenth"
        girl.info = _("""The previous Monster Lord, and Alice's mother. Wanting to bring peace between humans and monsters, she allowed herself to fall to a Hero's blade. But after her grave was robbed by La Croix, she was resummoned into this world. Her memory of the last few years before her death seem to have been erased. Due to that, her desires of reconciliation that appeared late in her life are not shown in her undead form.
Wielding incredible power, La Croix considers Alipheese the Fifteenth to be her ultimate creation. Massive amounts of energy are needed to keep her in this world as an undead monster. Unknown masses of men have been harvested to keep this zombie operating...""")
        girl.label = "alice15th"
        girl.label_cg = "alice15th_cg"
        girl.info_rus_img = "alice15th_pedia"
        girl.m_pose = alice15th_pose
        girl.zukan_ko = "alice15th"
        girl.facenum = 3
        girl.posenum = 5
        girl.bk_num = 6
        girl.x0 = -400
        girl.lv = 150
        girl.max_hp = 40000
        girl.hp = 40000
        girl.skills = ((4059, "Embracing Hand"),
            (4060, "Greedy Lips"),
            (4061, "Royal Breasts"),
            (4062, "Demento Nagi"),
            (4063, "Nephilim Magia"),
            (4064, "Nephilim End"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# SHIROM
########################################
    def get_shirom():
        girl = Chr()
        girl.unlock = persistent.shirom_unlock == 1
        girl.name = "La Croix"
        girl.pedia_name = "La Croix"
        girl.info = _("""A talented monster that was the most skilled practitioner of the Artiste family techniques. Great things were expected of her, especially in the field of necromancy. But in an accident, she turned herself into a zombie after being critically injured protecting her sister. After that, her necromantic research intensified, even going so far as to assassinate famous monsters. Due to her egregious actions, the Monster Lord banished her. On the run, she was eventually recruited by Promestein who was impressed by La Croix's knowledge of biology, surpassing even her own.
A great deal of energy is needed to maintain her undead form, requiring many men to be sacrificed for their semen. As her life grew on, her research turned into an obsession with reclaiming a true, living flesh and blood body.""")
        girl.label = "shirom"
        girl.label_cg = "shirom_cg"
        girl.info_rus_img = "shirom_pedia"
        girl.m_pose = shirom_pose
        girl.zukan_ko = "shirom"
        girl.facenum = 4
        girl.posenum = 8
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 122
        girl.max_hp = 24000
        girl.hp = 24000
        girl.skills = ((4065, "Necrophase"),
            (4066, "Necroreim"),
            (4067, "Necrofaun"),
            (4068, "La Croix's Mod"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# ALICE8TH1
########################################
    def get_alice8th1():
        girl = Chr()
        girl.unlock = persistent.alice8th1_unlock == 1
        girl.name = "Black Alice"
        girl.pedia_name = "Black Alice"
        girl.info = _("""A legendary warmongering Monster Lord that was struck down by Heinrich. She was said to wield incredibly high powered magic, and greedy ambitions. Right after she was defeated by Heinrich, she was saved by llias and taken under her umbrella...""")
        girl.label = "alice8th1"
        girl.label_cg = "alice8th1_cg"
        girl.info_rus_img = "alice8th1_pedia"
        girl.m_pose = alice8th1_pose
        girl.zukan_ko = "alice8th1"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 155
        girl.max_hp = 33000
        girl.hp = 33000
        girl.skills = ((4069, "Tickling Finger Caress"),
            (4070, "Sucking Small Mouth"),
            (4071, "Young Girl's Modest Breasts"),
            (4072, "Devilish Draining Hair"),
            (4073, "Monster Lord's All-Night Vigil"))
        girl.echi = ["alice8th1_ha", "alice8th1_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# TRAPTEMIS
########################################
    def get_traptemis():
        girl = Chr()
        girl.unlock = persistent.traptemis_unlock == 1
        girl.name = "Themis Trap"
        girl.pedia_name = "Themis Trap"
        girl.info = _("""A trap stone statue placed near the "Navel of the World".She spews aphrodisiac gas to disturb the mind of any human that approaches. When the man, overcome by lust, sticks himself into her genitals, a petrifying chemical is released that turns him to stone. There are currently around three hundred statues, half of them with men already attached.
The angels placed these statues around to forbid access to anyone trying to reach the Gate. But most of the men stuck were simple unknowing travellers.""")
        girl.label = "traptemis"
        girl.label_cg = "traptemis_cg"
        girl.info_rus_img = "traptemis_pedia"
        girl.m_pose = traptemis_pose
        girl.zukan_ko = "traptemis"
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -160
        girl.x1 = 20
        girl.lv = 45
        girl.max_hp = 10000
        girl.hp = 10000
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# GARGOYLE
########################################
    def get_gargoyle():
        girl = Chr()
        girl.unlock = persistent.gargoyle_unlock == 1
        girl.name = "Gargoyle Girl"
        girl.pedia_name = "Gargoyle Girl"
        girl.info = _("""A monster stone statue, located in the "Navel of the World". She was originally a spy keeping watch on the Gate during the Sacred Monster Wars. But after two thousand years she became bored and lost interest, and instead switched sides to guard the gate. As she was the second in command under the second Monster Lord, her power is equivalent to a current Queen class.
In truth, she doesn't care about either side or the war going on. She's just looking for things to amuse and occupy her during her eternal vigil. If you fall victim to her, she will turn you into stone with her, and continue to toy with you for several thousand years.""")
        girl.label = "gargoyle"
        girl.label_cg = "gargoyle_cg"
        girl.info_rus_img = "gargoyle_pedia"
        girl.m_pose = gargoyle_pose
        girl.zukan_ko = "gargoyle"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 80
        girl.max_hp = 25000
        girl.hp = 25000
        girl.skills = ((4074, "Eye of Submission"),
            (4075, "Gargoyle Blowjob"),
            (4076, "Gargoyle Paizuri"),
            (4077, "Gargoyle Bosom"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# DOPPELE
########################################
    def get_doppele():
        girl = Chr()
        girl.unlock = persistent.doppele_unlock == 1
        girl.name = "Doppel Luka"
        girl.pedia_name = "Doppel Luka"
        girl.info = _("""A clone of Luka created by the pinnacle of Promestein's genetic engineering technology. But her gender isn't set, and can be switched between male and female at will. She was created using samples taken from Luka during the Wormiel fight, and has all of his memories and physical abilities from that time. When she takes a female form, her thinking and personality also change to become more feminine.""")
        girl.label = "doppele"
        girl.label_cg = "doppele_cg"
        girl.info_rus_img = "doppele_pedia"
        girl.m_pose = doppele_pose
        girl.zukan_ko = "doppele"
        girl.facenum = 4
        girl.posenum = 6
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 78
        girl.max_hp = 25000
        girl.hp = 25000
        girl.skills = ((4078, "Doppel Handjob"),
            (4079, "Doppel Footjob"),
            (4080, "Doppel Blowjob"),
            (4081, "Doppel Tit Fuck"),
            (4082, "Doppel Intercrural"),
            (4083, "Doppel Assjob"),
            (4084, "Sword Semen Drain"),
            (4085, "Doppel Mount"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 19,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10,
            "sinkou": 2
            }
        return girl

########################################
# HAINU
########################################
    def get_hainu():
        girl = Chr()
        girl.unlock = persistent.hainu_unlock == 1
        girl.name = "Hainuwele"
        girl.pedia_name = "Hainuwele"
        girl.info = _("""A Next Doll, one of the strongest Chimeric Monsters created by Promestein. Focused on speed and agility, she was created to defeat Alma Elma of the Four Heavenly Knights. In addition, she can control wind freely, to counter Alma's use of wind magic. Finally, all of her senses were dulled or removed to render her Succubus attacks useless.
But the removal of all pleasure seems to have had an impact on her emotional state. Unable to feel joy or happiness, her mind has become twisted and brutal. In order to increase her speed and agility, her protective armor was stripped to nearly nothing, leaving her near defenseless. If her opponent was an armored warrior type, she would be in trouble. But against another speed user, she should have an advantage.""")
        girl.label = "hainu"
        girl.label_cg = "hainu_cg"
        girl.info_rus_img = "hainu_pedia"
        girl.m_pose = hainu_pose
        girl.zukan_ko = "hainu"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 145
        girl.max_hp = 23000
        girl.hp = 23000
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 20,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# AMPHIS
########################################
    def get_amphis():
        girl = Chr()
        girl.unlock = persistent.amphis_unlock == 1
        girl.name = "Amphisbaena"
        girl.pedia_name = "Amphisbaena"
        girl.info = _("""A Next Doll, one of the strongest Chimeric Monsters created by Promestein. Covered in unique armor and weapons, she was created to defeat Erubetie of the Four Heavenly Knights.
An ultra-vibration blade to destroy on a cellular level, vibration armor to defend against slime, a plasma field to microwave liquid around her, she is completely antislime in focus. Her physical ability is also extremely high, and her ultra-vibration blade seems effective against normal armor, too.
But due to all the sensitive machines, she is quite weak to shock and damage. In addition, the extreme power use of her equipment doesn't enable her to fight for a long period of time. Though lacking in versatility, she is extremely specialized in countering the seemingly-invincible Erubetie.""")
        girl.label = "amphis"
        girl.label_cg = "amphis_cg"
        girl.info_rus_img = "amphis_pedia"
        girl.m_pose = amphis_pose
        girl.zukan_ko = "amphis"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 146
        girl.max_hp = 30000
        girl.hp = 30000
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 20,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# TUKUYOMI
########################################
    def get_tukuyomi():
        girl = Chr()
        girl.unlock = persistent.tukuyomi_unlock == 1
        girl.name = "Tsukuyomi"
        girl.pedia_name = "Tsukuyomi"
        girl.info = _("""A Next Doll, one of the strongest Chimeric Monsters created by Promestein. Made from genetic material gathered from hair collected from one of Tamamo's fluffy tails. After the base clone was made, various mechanical upgrades were made to increase her performance.
To be able to contain the incredible strength of the earth imbued in Tamamo, her body had to be focused on defense. To protect against Tamamo's magic, anti-magic charms and devices were installed throughout her body. Able to use the power of the earth and Kitsune magic, she is powerful all around. She suffers from some speed performance due to all the heavy armor, but it isn't much of an issue.""")
        girl.label = "tukuyomi"
        girl.label_cg = "tukuyomi_cg"
        girl.info_rus_img = "tukuyomi_pedia"
        girl.m_pose = tukuyomi_pose
        girl.zukan_ko = "tukuyomi"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 147
        girl.max_hp = 30000
        girl.hp = 30000
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 20,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# ARCEN
########################################
    def get_arcen():
        girl = Chr()
        girl.unlock = persistent.arcen_unlock == 1
        girl.name = "Arc-En-Ciel"
        girl.pedia_name = "Arc-En-Ciel"
        girl.info = _("""A Next Doll, one of the strongest Chimeric Monsters created by Promestein. Boasting overwhelming physical ability, she was created to counter Granberia. Her body was improved upon with muscle tissue strengthening, ultra-fast synapses, metabolic over-limit through magic, and skin reinforced with nano-carbons.
As a result, her strength, agility, and defense are all unmatched by any force in the Heavens or surface world. The only apparent weakness is her incredible need for energy to maintain her body.
Due to the intense strain on her own body, her lifespan is extremely short. Her life is believed to be maxed out at five years, at the most.""")
        girl.label = "arcen"
        girl.label_cg = "arcen_cg"
        girl.info_rus_img = "arcen_pedia"
        girl.m_pose = arcen_pose
        girl.zukan_ko = "arcen"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 150
        girl.max_hp = 40000
        girl.hp = 40000
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 20,
            "skill02": 4,
            "skill03": 4,
            "skill04": 4,
            "skill05": 4,
            "item01": 10
            }
        return girl

########################################
# RAPUN
########################################
    def get_rapun():
        girl = Chr()
        girl.unlock = persistent.rapun_unlock == 1
        girl.name = "Rapunzel"
        girl.pedia_name = "Rapunzel"
        girl.info = _("""A Next Doll, one of the strongest Chimeric Monsters created by Promestein. She wasn't created to counter any particular Heavenly Knight, but was used as a birthing tool for their creation.
She was created focusing on her reproductive potential, in order to birth the other more specialized Next Dolls. Referred to as the "Egg Machine", her personality itself slowly came to embrace that role and function.
Despite being specialized for reproduction, she was given superior abilities and genes in order to pass them down to the other Next Dolls. Though she doesn't focus in any one element or specialization like the other Next Dolls, she is very well rounded. Since she has no real weakness, she is the most adaptable of the Next Dolls.""")
        girl.label = "rapun"
        girl.label_cg = "rapun_cg"
        girl.info_rus_img = "rapun_pedia"
        girl.m_pose = rapun_pose
        girl.zukan_ko = "rapun"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 146
        girl.max_hp = 32000
        girl.hp = 32000
        girl.skills = ((4086, "Lewd Bug's Blowjob"),
            (4087, "Lewd Bug's Paizuri"),
            (4088, "Lewd Bug's Lewd Honey"),
            (4089, "Lewd Bug's Mating"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 78,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# HEAVENSGATE
########################################
    def get_heavensgate():
        girl = Chr()
        girl.unlock = persistent.heavensgate_unlock == 1
        girl.name = "Heaven's Gate"
        girl.pedia_name = "Heaven's Gate"
        girl.info = _("""A huge angel that is herself the Gate to Heaven. Originally a Cherubim, she was assimilated into the Gate, and ordered to guard it by Mias herself. Rather than prevent surface dwellers from getting into Heaven, her true purpose was to restrict angels from freely being able to reach the surface world without llias's permission.
She was granted free control over devouring any who would attempt to unlawfully pass the gate, and render judgement as she saw fit. Since she is easily bored during her eternal duties, she would violate any trespasser for an eternity.""")
        girl.label = "heavensgate"
        girl.label_cg = "heavensgate_cg"
        girl.info_rus_img = "heavensgate_pedia"
        girl.m_pose = heavensgate_pose
        girl.zukan_ko = "heavensgate"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 148
        girl.max_hp = 40000
        girl.hp = 40000
        girl.skills = ((4090, "Seraphic Blowjob"),
            (4091, "Male Teasing Tentacles"),
            (4092, "Great Gate's Lusting Tongue"),
            (4093, "Gate of Judgement"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# EDEN
########################################
    def get_eden():
        girl = Chr()
        girl.unlock = persistent.eden_unlock == 1
        girl.name = "Seraph Eden"
        girl.pedia_name = "Seraph Eden"
        girl.info = _("""The highest and only member of the Seraphim class of angels. Effectively Dias's second in command, she is in charge of all other angels. Completely loyal to llias, she will obey any command given, no matter what. Due to her extreme faith, she cannot tolerate anything negative said against llias.
Her true form is in fact an enormous garden of paradise. Her female figure is only a small showing of her power. If she unveils her entire body, she was power equivalent to that found in an entire continent on the surface world.
There have only ever been three Seraphs: Lucifina, Micaela, and Eden. It's said that llias herself ordered Eden to turn her body into a garden of paradise for the Heavens.""")
        girl.label = "eden"
        girl.label_cg = "eden_cg"
        girl.info_rus_img = "eden_pedia"
        girl.m_pose = eden_pose
        girl.zukan_ko = "eden"
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 195
        girl.max_hp = 50000
        girl.hp = 50000
        girl.skills = ((4094, "Eden's Serpent"),
            (4095, "Seraph's Wings"),
            (4096, "Paradise Lost"),
            (4097, "Garden of Eden"))
        girl.echi = ["eden_ha", "eden_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# STEIN2
########################################
    def get_stein2():
        girl = Chr()
        girl.unlock = persistent.stein2_unlock == 1
        girl.name = "Promestein"
        girl.pedia_name = "Promestein (Final Form)"
        girl.info = _("""An angel who dedicated her life to the pursuit of knowledge. She imbued her own body with the genes of an ancient form of monster seaweed, in order to become closer to Dark God Alipheese's power. But, unable to control that power, her body explosively expanded. Even this runaway power was predicted by Promestein, though. After assimilating with pure holy energy, she was expecting to be able to stop the runaway power and maintain control.
Taking advantage of the re-creation of the world, she intended to betray llias. Her true aim with Chimeric research was to create an army able to defeat the armies of the Heavens. Her Next Dolls, ostensibly created to counter the Heavenly Knights, were also supposed to be used to defeat high ranking angels. In the end, all of her creations were destroyed, and she fell at the hands of Luka himself.""")
        girl.label = "stein2"
        girl.label_cg = "stein2_cg"
        girl.info_rus_img = "stein2_pedia"
        girl.m_pose = stein2_pose
        girl.zukan_ko = "stein2"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 230
        girl.max_hp = 60000
        girl.hp = 60000
        girl.skills = ((4098, "Swede Demosu"),
            (4099, "Stipe Suck"),
            (4100, "Swede Gigantea"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ALICE8TH2
########################################
    def get_alice8th2():
        girl = Chr()
        girl.unlock = persistent.alice8th2_unlock == 1
        girl.name = "Black Alice"
        girl.pedia_name = "Black Alice (Second Form)"
        girl.info = _("""\"Black Alice\", a former Monster Lord. Imbued with the power of Dark God Alipheese, she consumed llias to increase her power even further. Numerous monster organs cover her body, enabling her to devour men in countless ways.
Even in this state, it doesn't seem as if she's truly showing all of her power. If defeated, expect to be preyed on in a horrible way...""")
        girl.label = "alice8th2"
        girl.label_cg = "alice8th2_cg"
        girl.info_rus_img = "alice8th2_pedia"
        girl.m_pose = alice8th2_pose
        girl.zukan_ko = "alice8th2"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 256
        girl.max_hp = 65000
        girl.hp = 65000
        girl.skills = ((4101, "Sweet Demonic Palm"),
            (4102, "Sweet Demonic Lips"),
            (4103, "Sweet Demonic Breasts"),
            (4104, "Sweet Demonic Tentacles"),
            (4105, "Sweet Demonic Prison"),
            (4106, "Demonic Predation"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ALICE8TH3
########################################
    def get_alice8th3():
        girl = Chr()
        girl.unlock = persistent.alice8th3_unlock == 1
        girl.name = "Black Alice"
        girl.pedia_name = "Black Alice (Third Form)"
        girl.info = _("""\"Black Alice\", a former Monster Lord, now unleashing her true power. Comprised of the genes of seemingly every monster that has ever existed, her power itself is truly that of a God's.
Even this form was defeated by Heinrich in the past, but consuming llias has drastically increased her power. Her brutal nature is no longer hidden, and is on full display.""")
        girl.label = "alice8th3"
        girl.label_cg = "alice8th3_cg"
        girl.info_rus_img = "alice8th3_pedia"
        girl.m_pose = alice8th3_pose
        girl.zukan_ko = "alice8th3"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 256
        girl.max_hp = 65000
        girl.hp = 65000
        girl.skills = ((4107, "Octopus Pat"),
            (4108, "Leech Suck"),
            (4109, "Trampling Shackles"),
            (4110, "Swallow Prey"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ALICE8TH4
########################################
    def get_alice8th4():
        girl = Chr()
        girl.unlock = persistent.alice8th4_unlock == 1
        girl.name = "Black Alice"
        girl.pedia_name = "Black Alice (Final Form)"
        girl.info = _("""\"Black Alice\", a former Monster Lord, who has now drawn up every ounce of power from her Dark God Alipheese heritage. Pushed past the critical point, her body is endlessly expanding at an explosive rate.
Containing the genes of every monster to have ever existed, her power is unmatched by any other monster. Her runaway power has warped her desire to control the world, to instead want to consume it. In response to her overwhelming predation urge, her body itself has morphed into a being able to do just that. If left alone, she truly will consume it.""")
        girl.label = "alice8th4"
        girl.label_cg = "alice8th4_cg"
        girl.info_rus_img = "alice8th4_pedia"
        girl.m_pose = alice8th4_pose
        girl.zukan_ko = "alice8th4"
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -200
        girl.lv = 256
        girl.max_hp = 65000
        girl.hp = 65000
        girl.skills = ((4111, "Elimination: Squeeze"),
            (4112, "Elimination: Chew"),
            (4113, "Elimination: Suck"),
            (4114, "Elimination: Lick"),
            (4115, "Elimination: Milk"),
            (4116, "Elimination: Stroke"),
            (4117, "Elimination: Crush"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 79,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ilias3
########################################
    def get_ilias3():
        girl = Chr()
        girl.unlock = persistent.ilias3_unlock == 1
        girl.name = "Ilias"
        girl.pedia_name = "Goddess Ilias (3)"
        girl.info = _("""An existence that serves as the opposite of Dark God Alipheese. Her holy power circled the world, and was the origin of the sense of "Self" among the surface creatures. After a long fight, she sealed away Dark God Alipheese, and came to be revered by the humans as their Goddess.
She deeply hates monsters, borne from the Dark God, and desires their extinction.
Losing faith in humanity at the same time, she wants to re-create the world anew.""")
        girl.label = "ilias1"
        girl.label_cg = "ilias3_cg"
        girl.info_rus_img = "ilias3_pedia"
        girl.m_pose = ilias3_pose
        girl.zukan_ko = "ilias"
        girl.facenum = 6
        girl.posenum = 3
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 500
        girl.max_hp = 50000
        girl.hp = 50000
        girl.skills = ((4118, "Goddess's Handjob"),
            (4119, "Goddess's Blowjob"),
            (4120, "Goddess's Breasts"),
            (4121, "Wings of Love and Tolerance"),
            (4122, "Goddess's Riding"),
            (4123, "Goddess's Tightening"))
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ilias4
########################################
    def get_ilias4():
        girl = Chr()
        girl.unlock = persistent.ilias4_unlock == 1
        girl.name = "Ilias"
        girl.pedia_name = "Goddess Ilias (Final Form)"
        girl.info = _("""The final and most powerful form of llias, after taking in the power of Dark God Alipheese. The mixture of Dark and Holy magic morphed her body into the irregular mix of angelic and monster properties. Existing as the "ultimate" being, she is in complete control of her overwhelming power. Using the "Womb of the Gods", one of the Dark God's organs, to harvest semen, she can quickly create new life upon the surface. Dias's true goal all along was to obtain this ultimate body. Using it's power, she would be able to wipe out all monsters, the unsealed Dark God, and then re-create the entire world from scratch. Promestein and Black Alice were simple sacrifices for her plans all along.""")
        girl.label = "ilias2"
        girl.label_cg = "ilias4_cg"
        girl.info_rus_img = "ilias4_pedia"
        girl.m_pose = ilias4_pose
        girl.zukan_ko = "ilias"
        girl.facenum = 1
        girl.posenum = 2
        girl.bk_num = 2
        girl.x0 = -150
        girl.lv = 999
        girl.max_hp = 600000
        girl.hp = 600000
        girl.skills = ((4124, "Holy Hair"),
            (4125, "Holy Feathers"),
            (4126, "Embrace of the Goddess"),
            (4127, "Dark Goddess's Semen Extraction"))
        girl.echi = ["ilias2_ha", "ilias2_hb"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10,
            "check06": 1,
            "ivent01": 2,
            "ivent02": 2,
            "ivent03": 1,
            "ivent04": 2,
            "ivent05": 3,
            "ivent06": 2,
            "ivent08": 2,
            "ivent09": 2,
            "ivent10": 2,
            "ivent11": 2,
            "ivent12": 2,
            "ivent14": 3,
            "ivent17": 2,
            "ivent18": 2
            }

        return girl

########################################
# END
########################################
    def get_end():
        girl = Chr()
        girl.unlock = persistent.game_clear == 3
        girl.name = "Other Endings"
        girl.pedia_name = "Other Endings"
        girl.info = _("""The Hero's exploits brought peace to the world. But as he travels around the now peaceful world, he encounters numerous women who have varying thoughts about him... Those who want to gang-rape him... Those who want to repay his kindness with their bodies... Those who want him to be the common property of their entire village... Those who want him to be their husband... Those who want to get revenge by driving him mad with pleasure...
Can the brave Hero truly travel the world safely...?""")
        # girl.label = "end"
        girl.label_cg = "end_cg"
        girl.info_rus_img = "end_pedia"
        girl.m_pose = end_pose
        girl.zukan_ko = "end"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = -121
        girl.x1 = 79
        girl.lv = "None"
        girl.max_hp = "None"
        girl.hp = "None"
        girl.echi = ["city52_14a", "city53_14a", "city56b_13a", "city55_14a", "city57b_11c", "city63_13a", "city64_17a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# GRANBERIA5
########################################
    def get_granberia5():
        girl = Chr()
        girl.unlock = persistent.hsean_shitenno1 == 1 and persistent.game_clear == 3
        girl.name = "Granberia"
        girl.pedia_name = "Granberia (END)"
        girl.info = _("""A cursed sword user, and one of the Four Heavenly Knights. A famous warrior, as skilled with the blade as the Hero Luka.
She was invited to be the military adviser of Grangold, and now drills soldiers in the parade grounds. Famous warriors from all over are now rushing to Grangold to train under her.""")
        girl.label = "granberia5"
        girl.label_cg = "granberia5_cg"
        girl.info_rus_img = "granberia5_pedia"
        girl.m_pose = granberia5_pose
        girl.zukan_ko = "granberia"
        girl.zukan_ko = None
        girl.facenum = 6
        girl.posenum = 2
        girl.x0 = -185
        girl.lv = 126
        girl.max_hp = 24000
        girl.hp = 24000
        girl.echi = ["city45_11j", "city62_15c"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ALMA_ELMA4
########################################
    def get_alma_elma4():
        girl = Chr()
        girl.unlock = persistent.hsean_shitenno2 == 1 and persistent.game_clear == 3
        girl.name = "Alma Elma"
        girl.pedia_name = "Alma Elma (END)"
        girl.info = _("""The Queen Succubus, and one of the Four Heavenly Knights. Living free in the now peaceful world, she still makes frequent appearances in the Colosseum. Even though everyone knows who she is now, she still goes by the pseudonym Kyuba.
Wanting to taste a Queen Succubus just once, male applicants continually flood the Colosseum. As a result, there are far more Male vs. Male battles in the Colosseum, that usually get intense as they take out their frustration on each other.""")
        girl.label = "alma_elma4"
        girl.label_cg = "alma_elma4_cg"
        girl.info_rus_img = "alma_elma4_pedia"
        girl.m_pose = alma_elma4_pose
        girl.zukan_ko = "alma_elma"
        girl.zukan_ko = None
        girl.facenum = 3
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 125
        girl.max_hp = 15000
        girl.hp = 15000
        girl.echi = ["city59_17c"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ERuBETIE3
########################################
    def get_tamamo3():
        girl = Chr()
        girl.unlock = persistent.hsean_shitenno3 == 1 and persistent.game_clear == 3
        girl.name = "Tamamo"
        girl.pedia_name = "Tamamo (END)"
        girl.info = _("""The only Nine-Tailed Kitsune, and one of the Four Heavenly Knights. In actuality, Tamamo is one of the original Six Ancestors, and has served as an adviser to Monster Lords since Alipheese the Eighth's reign. She was instructed to teach and guide future Monster Lords to ensure that disaster didn't happen again.
Even though the world is at peace, she hasn't changed her role. Tired of nagging on the now somewhat wise Alipheese the Sixteenth, she's looking forward to the birth of the next generation.""")
        girl.label = "tamamo3"
        girl.label_cg = "tamamo3_cg"
        girl.info_rus_img = "tamamo3_pedia"
        girl.m_pose = tamamo3_pose
        girl.zukan_ko = "tamamo"
        girl.zukan_ko = None
        girl.facenum = 5
        girl.posenum = 2
        girl.x0 = -200
        girl.lv = 128
        girl.max_hp = 22000
        girl.hp = 22000
        girl.echi = ["city60_12a"]
        girl.scope = {"ruka_tati": 2,
        "mylv": 80,
        "skill01": 21,
        "skill02": 5,
        "skill03": 5,
        "skill04": 5,
        "skill05": 5,
        "item01": 10
        }
        return girl

########################################
# ERUBETIE3
########################################
    def get_erubetie3():
        girl = Chr()
        girl.unlock = persistent.hsean_shitenno4 == 1 and persistent.game_clear == 3
        girl.name = "Erubetie"
        girl.pedia_name = "Erubetie (END)"
        girl.info = _("""The Queen Slime, and one of the Four Heavenly Knights. Once against coexistence with humans, her mind has gradually changed as she has interacted with Hero Luka. She now works to make it so slimes and humans can coexist in peace.
Erubetie pushes forward presentations and seminars on how to properly deal with water treatment and pollution, in order to clean up the environment and make it habitable for slimes. Not only is this beneficial for her slime race, but it improves the human quality of life as well.""")
        girl.label = "erubetie3"
        girl.label_cg = "erubetie3_cg"
        girl.info_rus_img = "erubetie3_pedia"
        girl.m_pose = erubetie3_pose
        girl.zukan_ko = "erubetie"
        girl.zukan_ko = None
        girl.facenum = 1
        girl.posenum = 1
        girl.x0 = 25
        girl.x1 = 225
        girl.lv = 125
        girl.max_hp = 22000
        girl.hp = 22000
        girl.echi = ["city61_17a"]
        girl.scope = {"ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }
        return girl

########################################
# ALICE5
########################################
    def get_alice5():
        girl = Chr()
        girl.unlock = persistent.game_clear == 3
        girl.name = "Alice"
        girl.pedia_name = "Alice (END)"
        girl.info = _("""The current Monster Lord, above all other monsters. After defeating llias, she declared the arrival of worldwide peace. A central figure of the attained peace along with Luka, she's currently traveling the world alongside her husband.
Cooperating with the current human rulers, she is a big pusher of social reforms to promote coexistence between all races. Since there are still minor troubles all over the world, she is still happily traveling with Luka...""")
        girl.label = "alice5"
        girl.label_cg = "alice5_cg"
        girl.info_rus_img = "alice5_pedia"
        girl.m_pose = alice5_pose
        girl.zukan_ko = "alice"
        girl.zukan_ko = None
        girl.facenum = 10
        girl.posenum = 4
        girl.x0 = -20
        girl.x1 = 150
        girl.lv = 145
        girl.max_hp = 32000
        girl.hp = 32000
        girl.echi = ["lb_0399", "lb_0414"]
        girl.scope = {
            "ruka_tati": 2,
            "mylv": 80,
            "skill01": 21,
            "skill02": 5,
            "skill03": 5,
            "skill04": 5,
            "skill05": 5,
            "item01": 10
            }

        return girl