label city31_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 029 with blinds2
    play music "audio/bgm/enrika.ogg"


label city31_main:
    call city_null 
    $ city_bg = "bg 029"
    $ city_button01 = "Betty" #601
    $ city_button02 = "Boy" #602
    $ city_button03 = "Man" #603
    $ city_button04 = "Girl" #604
    $ city_button05 = "Old Man" #605
    $ city_button06 = "Soldier" #606
    $ city_button07 = "Temple Soldier" #607
    $ city_button08 = "Elf" #608
    $ city_button09 = "Dark Elf" #609
    $ city_button10 = "Hospital" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Micaela's House" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city31_01 
        elif result == 2:
            call city31_02 
        elif result == 3:
            call city31_03 
        elif result == 4:
            call city31_04 
        elif result == 5:
            call city31_05 
        elif result == 6:
            call city31_06 
        elif result == 7:
            call city31_07 
        elif result == 8:
            call city31_08 
        elif result == 9:
            call city31_09 
        elif result == 10:
            call city31_10 
        elif result == 11:
            call city31_11 
        elif result == 12:
            call city31_12 
        elif result == 19:
            return


label city31_01:
    show mob obasan with dissolve #700

    betty "Who would have thought you'd fight your way here!{w}\nDid you beat the Monster Lord and make her your friend?"
    l "It's a little difficult to explain it...{w}\nShe did become my friend..."
    l "Come here Alice...{w} Introduce yourself..."

    show alice st71 at xy(X=-170, Y=50) #700
    show mob obasan at xy(X=160)
    with dissolve #701

    a "Hello...{w}{nw}"

    show alice st73 at xy(X=-170, Y=50)
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\n...Ah!{w} Don't treat me like a child!"
    betty "Have some candy, little girl."

    show alice st79 at xy(X=-170, Y=50) with dissolve #700

    a "Yay!{image=note}"
    "Alice starts happily licking the candy.{w}\nIt's really easy to entice the Monster Lord, huh?"

    hide alice
    show mob obasan at center
    with dissolve #700

    betty "But still, Luka...{w}\nYour presence has completely changed in such a short time."
    betty "For some reason, I have this bad feeling like you'll never come back home..."
    l "You're just imagining things, Aunt Betty.{w}\nOnce everything's over, I'll come back for sure."
    betty "Little Monster Lord, I'm entrusting Luka to you.{w}\nDon't let him out of your sight, or he'll do something unreasonable..."

    show alice st71 at xy(X=-170, Y=50) #700
    show mob obasan at xy(X=160)
    with dissolve #701

    a "You can count on me."
    "Alice nods as she starts biting into the candy."
    betty "Make sure you survive and come back, Luka.{w}\nWe'll rebuild and bring Ilias Village back, I swear it."

    return


label city31_02:
    show mob seinen1 with dissolve #700

    boy "Ohh! Luka, you came back safe and sound!{w}\nI still can't believe what happened at the village, but I'm glad you're safe!"

    show alice st71 at xy(X=-170, Y=50) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    boy "By the way, who's that girl?"
    l "Ah... Just, you know... Someone."
    boy "Hey Luka, come here...{w} You know, I've been keeping it from you for a while, but I'm actually totally into lolicon.{w}\nWanna just, you know...{w} Leave her with me for a bit?"
    l "I will never leave her alone with you now."
    boy "Hey, I've got some candy."

    show alice st79 at xy(X=-170, Y=50) with dissolve #700

    a "Yay!{image=note}"
    l "Alice... Don't take candy from strangers..."
    a "Don't treat me like a child!{w}\n*Munch* *Munch*{w} Mmmm!{image=note}"

    return


label city31_03:
    show mob ozisan1 with dissolve #700

    man "H...How could it be...{w} How could Ilias try to destroy us?{w}\nWhat's going to happen to the world!?"
    man "Luka... Please, save the world!{w}\nYou're our only hope!"

    return


label city31_04:
    show mob syouzyo with dissolve #700

    girl "I was so scared...{w}\nWhy are angels doing such horrible things to us?"
    l "............."

    return


label city31_05:
    show mob rouzin2 with dissolve #700

    old_man "This only happened because our faith in Ilias wasn't strong enough.{w}\nIt's because of these no good youths of this generation!"

    return


label city31_06:
    show mob sensi1 with dissolve #700

    soldier "If I had even half of your power, I could have saved everyone...{w}\nEven Ronnie...{w} Damn it! Right before my eyes..."
    l "Ronnie..."
    "He always joked that when I came back, his shop wouldn't be there anymore.{w}\nOf course, it was always related to him griping about the poor sales."
    "But to think it came true in such a horrible way..."
    soldier "Once we rebuild the village, we'll hold a memorial service for those who died.{w}\nRonnie, please wait for a little longer..."

    return


label city31_07:
    show mob heisi1 with dissolve #700

    temple_soldier "I couldn't do anything.{w}\nI wasn't even able to raise my spear in front of that angel."
    temple_soldier "Even though our attacks wouldn't have hit her anyway, it was still a complete destruction to our morale."
    temple_soldier "But as soldiers devoted to the Ilias Temple, what should we have done?{w}\nI still don't know..."

    return


label city31_08:
    show mob elf1 with dissolve #700

    elf "With so many new humans, our village feels so cramped...{w}\nBut of course, we won't drive anybody out."
    elf "We will welcome anyone here.{w}\nThe last stronghold for those with no place to go."
    l "I never knew there was a hidden village of monsters so close."
    elf "This village was populated by monsters that a certain Hero guided away.{w}\nI've never met him personally, but he was our biggest benefactor."
    elf "In some sick twist of fate, I heard he was killed by monsters...{w}\nI wanted to meet him just once, and give him my thanks."
    l "W...What was that Hero's name?"
    elf "I believe it was Marcellus."
    l ".........."
    "So it was Dad after all."

    return


label city31_09:
    show delf_a st01 at xy(X=100, Y=-100) with dissolve #700

    dark_elf "Master Micaela accepted me, even after I fell into darkness.{w}\nIn repayment, I would gladly give my life defending this village!"

    show delf_a st02 at xy(X=100, Y=-100) with dissolve #700

    dark_elf "...By the way, doesn't \"fell into darkness\" sound a little erotic?"
    l "Is that what's always going through your mind...?"

    return


label city31_10:
    scene bg 166 with blinds
    show mob seinen1 at xy(X=-170) #700
    show mob syouzyo at xy(X=170) as mob2
    with dissolve #701

    boy "Ahh... I...it hurts..."
    girl "Oww..."
    "A medium sized building is full of the sounds of wounded.{w}\nElves are moving around treating them all."

    show mob heisi1 at xy(X=-170) #700
    show mob mamono1 at xy(X=280, Y=-50) as mob2
    with dissolve #701

    elf "Please, move your foot forward.{w}\nIf we don't get this bandage on you soon, it might get worse."
    temple_soldier "G...Gah... Who would ever accept help from a monster!?{w}\nGet away from me!"
    "Someone yells out, refusing to be treated by monsters.{w}\nIt must be a still pious follower of Ilias..."

    scene bg 029 with blinds
    return


label city31_11:
    scene bg 151 with blinds
    show mob elf2 with dissolve #700

    elf "I'm sorry, but the shop isn't open for business.{w}\nRight now our biggest concern is keeping our weapons ready to go at a moment's notice."
    elf "You're on some big mission, right?{w}\nWe're all cheering for you, from the bottom of our hearts!"

    scene bg 029 with blinds
    return


label city31_12:
    scene bg 167 with blinds
    show micaela st11 at xy(X=70) with dissolve #700

    micaela "We will take responsibility and protect the people of Ilias Village.{w}\nSo do not worry about those here."
    micaela "Right now, the world is under attack from Ilias's forces.{w}\nPlease wield your sword, and save as many as possible..."

    scene bg 029 with blinds
    return


label city32_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 018 with blinds2
    play music "audio/bgm/city1.ogg"


label city32_main:
    call city_null 
    $ city_bg = "bg 018"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Townie" #602
    $ city_button03 = "Prostitute" #603
    $ city_button04 = "Youth" #604
    $ city_button05 = "Old Woman" #605
    $ city_button06 = "Soldier A" #606
    $ city_button07 = "Soldier B" #607
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Sutherland Inn" #613
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city32_01 
        elif result == 2:
            call city32_02 
        elif result == 3:
            call city32_03 
        elif result == 4:
            call city32_04 
        elif result == 5:
            call city32_05 
        elif result == 6:
            call city32_06 
        elif result == 7:
            call city32_07 
        elif result == 10:
            call city32_10 
        elif result == 11:
            call city32_11 
        elif result == 12:
            call city32_12 
        elif result == 13:
            call city32_13 
        elif result == 19:
            return


label city32_01:
    show mob seinen1 with dissolve #700

    boy "Who would have thought angels would attack in addition to monsters!{w}\nI was so scared, I was trembling for hours."
    boy "My underwear is already soaked...{w}\nI think at this point its new color is yellow forever..."

    return


label city32_02:
    show mob musume1 with dissolve #700

    townie "Angels and monsters are attacking not only here, but all over the world.{w}\nAhh... What's going to happen to the world?"
    townie "That reminds me, Gobby went to make a delivery to Yamatai Village.{w}\nI'm so worried... I hope she wasn't attacked!"

    return


label city32_03:
    show mob musume2 with dissolve #700

    prostitute "You did amazing, Hero!{w}\nHehe... I'll make it extra cheap for you!"

    show alice st71 at xy(X=-170, Y=50) #700
    show mob musume2 at xy(X=335)
    with dissolve #701

    a "............"
    prostitute "Oh my, excuse me.{w} I didn't realize you were with your daughter."

    return


label city32_04:
    show mob seinen2 with dissolve #700

    youth "I felt so ashamed when I saw those three little girls fighting that monster with everything they had."
    youth "I also realized those three were fine citizens of Iliasburg."
    youth "I wonder if there's something I can do to help the town, too.{w}\nPerhaps they are short-handed somewhere?"

    return


label city32_05:
    show mob rouba with dissolve #700

    old_woman "I knew it, those fellows calling themselves \"Heroes\" were no good.{w}\nI've been fed up with those folks hanging off Ilias's name and calling themselves Heroes for no good reason!"
    old_woman "...Oh, but you are a different story.{w}\nYou're a true Hero, worthy of all the praise."

    return


label city32_06:
    show mob sensi1 with dissolve #700

    soldier_a "A...Angels came to attack the town!{w}\nJust what is Ilias thinking?"
    soldier_a "Is it the wrath of Heaven coming down on me!?{w}\nBecause my life is nothing but a pack of horrible, horrible lies!?"
    soldier_a "Ahh Ilias, forgive me!{w} I'll stop lying!{w} And picking on my little sister..."

    return


label city32_07:
    show mob sensi2 with dissolve #700

    soldier_b "I challenged an enemy, too...{w}\nI was totally outclassed, and it was absolutely embarrassing."
    soldier_b "But you were something else!{w}\nYou're so young, too...{w} Just how many battles have you been in already!?"

    return


label city32_10:
    scene bg 152 with blinds
    show dragonp st11 at xy(X=160, Y=100)
    show mob ozisan1 at xy(X=-160) #700
    with dissolve #701

    shopkeeper "Pup has been so down since then...{w}\nShe seems to have realized her weakness ever since losing to that tentacle monster."
    shopkeeper "I thought she did her best, and did a damn fine job of it.{w}\nAfter all, I was cowering under my desk the whole time..."
    dragon_pup "Ugaga... We're pathetic.{w}\nWe couldn't protect the town..."
    l ".........."
    "I understand how frustrating that feeling of powerlessness is...{w}\nI'll leave them alone for now."

    scene bg 018 with blinds
    return


label city32_11:
    scene bg 151 with blinds
    show vgirl st11 at xy(X=160, Y=100)
    show mob seinen1 at xy(X=-160) #700
    with dissolve #701

    shopkeeper "Forgive me, I'm out of medicinal herbs.{w}\n...Oh, are you the Hero?"
    shopkeeper "Would you be able to say something to this child?{w}\nShe has been quite depressed for a while..."
    vgirl "I'm so weak...{w}\nI didn't have any strength to fight. I'm so pathetic..."
    l ".........."
    "I understand how frustrating that feeling of powerlessness is...{w}\nI'll leave her alone for now."

    scene bg 018 with blinds
    return


label city32_12:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "That bitch!{w} What kind of way is that for a god to act!?{w}\nI'll never believe anything about that whore again!"
    priest "From now on, I'm starting down a new path!{w} A path of violence!!{w}\nI will be a priest of corruption!{w} Bound by no rules, leaving a path of horror in my wake!"
    priest "Hahahahahahaha!"
    l "P...Please calm down!"
    "It seems like the priest broke...{w}\nThis must have been absolutely crushing to those who devoted their lives to Ilias..."

    scene bg 018 with blinds
    return


label city32_13:
    scene bg 016 with blinds
    show mob obasan with dissolve #700

    owner "I thought that Ilias was suspicious for a while now.{w}\nWe'd always be forced to give huge discounts to Heroes, and it always made us lose a lot of money."
    owner "Lots of merchants disliked Ilias because of that, from a long time ago.{w}\nWe kept it quiet before due to backlash, but now we can be open about it."
    l "I...Is that so?"
    "It seems like the merchants never trusted her to begin with.{w}\nThe merchants seem to have taken this the lightest out of everyone."
    owner "I hope you're able to smack that Ilias around a bit soon!"
    l "Yes... I'll do my best."

    show mob obasan at xy(X=-160) #700
    show pramia st11 at xy(X=160, Y=100)
    with dissolve #701

    owner "That reminds me, Lammy here has been down ever since then.{w}\nCould you talk to her?{w} Maybe find some way to cheer her up?"
    tiny_lamia "I'm so annoyed...{w}\nI'm so pathetic..."
    l "............."
    "I understand how frustrating that feeling of powerlessness is...{w}\nI'll leave her alone for now."

    scene bg 018 with blinds
    return


label city33_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 023 with blinds2
    play music "audio/bgm/mura2.ogg"


label city33_main:
    call city_null 
    $ city_bg = "bg 023"
    $ city_button01 = "Girl" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Young Woman" #603
    $ city_button04 = "Young Girl" #604
    $ city_button05 = "Young Boy" #605
    $ city_button06 = "Mayor" #606
    $ city_button07 = "Young Man" #607
    $ city_button08 = "Man" #608
    $ city_button10 = "Harpy" #610
    $ city_button11 = "Harpy Sisters" #611
    $ city_button12 = "Queen Harpy" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city33_01 
        elif result == 2:
            call city33_02 
        elif result == 3:
            call city33_03 
        elif result == 4:
            call city33_04 
        elif result == 5:
            call city33_05 
        elif result == 6:
            call city33_06 
        elif result == 7:
            call city33_07 
        elif result == 8:
            call city33_08 
        elif result == 10:
            call city33_10 
        elif result == 11:
            call city33_11 
        elif result == 12:
            call city33_12 
        elif result == 19:
            return


label city33_01:
    show mob musume1 with dissolve #700

    girl "As soon as the family issues were sorted out, angels attacked...{w}\nIf it wasn't for the harpies power, it would have ended very badly..."

    return


label city33_02:
    show mob hapy3 at xy(X=160) as mob2
    show mob obasan at xy(X=-160) #700
    with dissolve #701

    pipi "I was so scared..."
    woman "You're safe now.{w}\nThe Queen will protect us."
    woman "And of course your mother will, too."

    show mob hapy2 at xy(X=160) as mob2 with dissolve #701

    piana "I couldn't do anything against those angels before...{w}\nBut next time, I'll do better for the sake of my daughter and step mother."
    piana "...Eh?{w} My husband?"
    piana "He liked those angel's cross of pleasure so much, I set him up on the scarecrow."

    return


label city33_03:
    show mob musume2 with dissolve #700

    young_woman "My husband has three wives and twelve kids...{w}\nAt first I was filled with pure rage, but I slowly got used to it."
    young_woman "And when the angels attacked, we all cooperated and defended ourselves as a single family."

    return


label city33_04:
    show mob hapy3 at xy(X=160) as mob2
    show mob syouzyo at xy(X=-160) #700
    with dissolve #701

    young_girl "Pym and I hid in the straw."
    harpy_girl "I was really scared, but the Queen helped me."

    return


label city33_05:
    show mob hapy3 at xy(X=160) as mob2
    show mob syounen at xy(X=-160) #700
    with dissolve #701

    young_boy "............."
    harpy_girl "............."
    "A delicate atmosphere hangs around the young couple.{w}\nI wonder if something happened during the attack..."

    return


label city33_06:
    show mob rouzin1 as mob2 with dissolve #700

    village_chief "Happiness Village and the Harpy Village formed an alliance.{w}\nEven though we're still separate villages with separate leaders..."
    village_chief "I get the feeling that the Harpy Queen is gaining more trust here."

    show mob rouzin1 at xy(X=-160) #700
    show mob hapy2 at xy(X=160) as mob2
    with dissolve #701

    mature_harpy "You don't need to worry about that, Village Chief.{w}\nI'll make you feel nice and comfortable again tonight... Ok?"
    village_chief "Yes!{w} Who really cares!"
    l "..........."

    return


label city33_07:
    show mob seinen1 with dissolve #700

    "A young man is bound to a scarecrow..."
    young_man "Piana, forgive me...{w}\nI'm not charmed by the angels anymore..."
    young_man "Waaah... Urhg... Someone, anyone... Let me down...{w}\nPiana, Pipi... Please forgive me..."

    return


label city33_08:
    show mob ozisan1 with dissolve #700

    man "All the men who were enraptured by the angels are getting big anger from their wives...{w}\nAhh, how horrifying!{w} The angel's attack isn't the real thing to be afraid of!"

    return


label city33_10:
    show hapy_a st12 with dissolve #700

    harpy "Thank you, little Hero.{w}\nIf you had not helped me then, I'd have been stuck in that birdcage..."

    show hapy_a st13 with dissolve #700

    harpy "In thanks, I'll let you mate with me.{image=note}{w}\nCome on, just try putting in the tip at least.{image=note}"

    return


label city33_11:
    show hapy_bc st31 at xy(X=160)
    show hapy_bc st11 at xy(X=-160) as hapy_bc2 #700
    with dissolve #701

    younger_harpy "I was so scared, I pretended like I was dead...{w}\nAfter a minute or so, I got really lightheaded and loopy..."
    older_harpy "Pii, I've told you that you have to keep breathing even when you play dead..."

    return


label city33_12:
    show queenharpy st11 with dissolve #700

    queen_harpy "Thanks to your help, many lives were saved.{w}\nHowever, I'm certain that we will be targeted again in the future..."
    queen_harpy "Though if I had the same military power as the previous Queen, then I would not have caused so much trouble for everyone..."

    show alice st71 at xy(X=-170, Y=50) #700
    show queenharpy st11 at xy(X=160)
    with dissolve #701

    a "...Don't be silly, Queen Harpy.{w}\nThe previous Queen was very skilled in the war arts, but she was very ambitious and cruel."
    a "Due to her warmongering, the harpy race sharply declined in number.{w}\nA gentle, intelligent Queen such as yourself is far more suited to the role."
    queen_harpy "Your praise is humbling.{w}\nI swear I will unite everyone here, and overcome this disaster."

    show alice st74 at xy(X=-170, Y=50) with dissolve #700

    a "Yes... Due to my incompetence, this trouble is occurring.{w}\nFor now, prepare to defend against another assault."
    queen_harpy "Your will shall be done.{w} The people of this village are my responsibility."

    show alice st71 at xy(X=-170, Y=50) with dissolve #700

    a "In addition, delivery of strategic goods needs to be restored as soon as possible.{w}\nNamely, Happiness Honey needs to be delivered.{w} The destination is my stomach."
    queen_harpy "Y...Your will shall be done..."
    l "Alice..."

    return


label city35_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 037 with blinds2
    play music "audio/bgm/city1.ogg"


label city35_main:
    call city_null 
    $ city_bg = "bg 037"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Man" #602
    $ city_button03 = "Young Woman" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Mermaid A" #605
    $ city_button06 = "Mermaid B" #606
    $ city_button07 = "Young Mermaid" #607
    $ city_button08 = "Mermaid Merchant" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #613
    $ city_button14 = "Meia's House" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city35_01 
        elif result == 2:
            call city35_02 
        elif result == 3:
            call city35_03 
        elif result == 4:
            call city35_04 
        elif result == 5:
            call city35_05 
        elif result == 6:
            call city35_06 
        elif result == 7:
            call city35_07 
        elif result == 8:
            call city35_08 
        elif result == 10:
            call city35_10 
        elif result == 11:
            call city35_11 
        elif result == 12:
            call city35_12 
        elif result == 13:
            call city35_13 
        elif result == 14:
            call city35_14 
        elif result == 19:
            return


label city35_01:
    show mob mermaid2 at xy(X=160) as mob2
    show mob seinen1 at xy(X=-160) #700
    with dissolve #701

    boy "Honey, thank you for fighting for my sake!{w}\nOh, you're wounded..."
    mermaid "I don't mind, darling!{w}\nFor your sake, I don't care what happens to this body."
    mermaid "...By the way, darling...{w}\nWeren't you sort of staring pretty hard at the Mermaid imperial guards...?"
    boy "No, that was...{w} Uh, that wasn't..."
    mermaid "As I was fighting for my life, were you staring at the enemy...?"
    boy "No, that's not right...{w} That was..."
    mermaid "What do you mean that's not right!?"
    boy "I...I was just a little captivated..."

    hide mob
    hide mob2
    show alice st72 at xy(Y=50)
    with dissolve #700

    a "Hehehe... That's good...{w}\nHate each other... Delicious marital quarreling."
    l "Alice, we're fighting for humans and monsters to coexist together...{w}\nDon't feed on drama..."

    return


label city35_02:
    show mob ozisan1 with dissolve #700

    man "I managed to survive...{w}\nBut a lot of the town was submerged in the water during the battle."
    man "We were able to evacuate before the battle started, so there weren't many victims...{w}\nBut if this keeps up, there won't be a town to return to."

    return


label city35_03:
    show mob musume2 with dissolve #700

    young_woman "Mermaids protected this town.{w}\nBut it's sort of complicated, since Mermaids were also the ones who attacked it..."

    return


label city35_04:
    show mob sensi2 with dissolve #700

    soldier "This town was attacked by Mermaids, but I heard other towns were attacked by angels.{w}\nJust what is happening in the world?"
    soldier "Even worse, I heard that Ilias herself is the one who triggered this...{w}\nIs there no hope for the future, if we have been sentenced to extinction by a Goddess herself?"

    return


label city35_05:
    show mob mermaid2 with dissolve #700

    mermaid_a "I wonder if the Queen will reconsider her belief on humans?{w}\nOr is she too set on coexistence not being possible?"
    mermaid_a "We'll do our best to deepen the relationship with humans...{w}\nIf we do, I'm sure the Queen will come around one day!"

    return


label city35_06:
    show mob mermaid1 with dissolve #700

    mermaid_b "I was born in Port Natalia.{w}\nI won't forgive the Queen for destroying my hometown."
    mermaid_b "If she attacks again, I'll defend with everything I have.{w}\nThere are dear people to me in this town that I would do anything to protect!"

    return


label city35_07:
    show mob mermaid3 with dissolve #700

    young_mermaid "Princess El...{w}\nI wanted to be friends with her..."

    return


label city35_08:
    show meia st21 at xy(X=50, Y=50) with dissolve #700

    mermaid_merchant "After the battle was over, I came back.{w}\nAll of my important merchandise was submerged under the water..."
    mermaid_merchant "My fried starfish, fried sea anemone...{w} Even my fried sea slug!{w}\nAll covered in ocean water for hours..."
    mermaid_merchant "*Munch* *Munch*{w}\nAh. The salt adds to the flavor!{image=note}"
    l "............."
    "This Mermaid must have a horrible sense of taste..."

    return


label city35_10:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "I'm out of stock, so I want to load back up on inventory soon.{w}\nEh? The world is in chaos?{w} Trade routes are closed!?"
    shopkeeper "Then I'll be going out of business...{w}\nWait, no!{w} This is the perfect chance for a weapon dealer!"

    scene bg 037 with blinds
    return


label city35_11:
    scene bg 154 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "Wow, that was such an intense battle.{w}\nThe Mermaids were fighting, and water was going everywhere...{w} All their white shirts became see-through so quickly!"
    shopkeeper "It was really hard deciding which Mermaid to stare at, let me tell you."
    shopkeeper "So I started thinking about clothes that would completely dissolve when wet."
    l "But then they're not clothes anymore..."
    shopkeeper "Not clothes anymore!{w} What a great tag line!{w}\nI hope peace returns quickly, so this can be the next big fashion hit!"

    show alice st71 at xy(X=-170, Y=50) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "............Damn pervert."

    scene bg 037 with blinds
    return


label city35_12:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "Oooh the Mermaids in this town have started fighting among themselves!{w}\nPlease save us weak humans, Ilias!"

    scene bg 037 with blinds
    return


label city35_13:
    scene bg 155 with blinds
    show mob mermaid2 with dissolve #700

    mermaid "The Mermaid Pub is completely submerged under water, so it's closed for now.{w}\nAh, the little Hero...{w} Thank you for defending the town!"
    mermaid "But to think the legend of Laura was a historical fact of the Queen's youth...{w}\nThat was news to me!"
    mermaid "But in this era, no such tragedy would occur...{w}\nI guess that's why we separate things into different eras to begin with...?"

    scene bg 037 with blinds
    return


label city35_14:
    scene bg 049 with blinds
    show meia st02 at xy(X=200, Y=50)
    show mob syounen at xy(X=-160) #700
    with dissolve #701

    meia "Thank you for helping us before, Hero!{w}\nIf you had not have shown up, I wonder what would have happened to us...?"
    boy_q "Thank you, big bro!"
    l "Oh no, the real fighting was done by the Mermaids defending the town...{w}\nMeia did her best too!"

    hide mob
    show alice st71 at xy(X=-170, Y=50) #700
    show meia st02 at xy(X=200, Y=50)
    with dissolve #701

    a "The defense of Port Natalia will need to be done by the Mermaids from now on.{w}\nIf they're going to launch another surprise attack, it will take a while for their preparations to be done."

    show meia st03 at xy(X=200, Y=50) with dissolve #701

    meia "Arara... Your companion is tiny now.{w}\nI knew you agreed with me that smaller is better.{image=note}"
    l "No, I didn't do that to her!{w}\nI don't have anything devious in mind!"

    scene bg 037 with blinds
    return


label city36a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 048 with blinds2
    play music "audio/bgm/city1.ogg"


label city36a_main:
    call city_null 
    $ city_bg = "bg 048"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Three Women" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Children" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "San Ilia Castle" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city36a_01 
        elif result == 2:
            call city36a_02 
        elif result == 3:
            call city36a_03 
        elif result == 4:
            call city36a_04 
        elif result == 5:
            call city36a_05 
        elif result == 6:
            call city36a_06 
        elif result == 10:
            call city36a_10 
        elif result == 11:
            call city36a_11 
        elif result == 12:
            jump city36b_main0
        elif result == 19:
            return


label city36a_01:
    show mob seinen1 with dissolve #700

    boy "Ahh what a disaster...{w}\nGoddess Ilias tried to destroy us!"
    boy "Was it because we didn't have enough faith?{w} Or were we not grateful enough?{w}\nI just don't get it at all..."

    return


label city36a_02:
    show mob zyosei with dissolve #700

    woman_a "That was really horrible, wasn't it my dear?{w}\nI can't believe Ilias tried to destroy us..."

    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_b "The same thing is happening at other towns, isn't it?{w}\nThere's no where to escape to, if that's the case..."

    hide mob
    show mob madam at center as mob2
    with dissolve #700

    woman_c "This disaster was averted thanks to the Hero...{w}\nBut what's going to happen next?"

    if ivent05 != 0:
        show mob madam at xy(X=-160) #700
        show mob zyoseiy at xy(X=160) as mob2
        with dissolve #701

        ghost_lady "There's no feeling at ease, whether you're dead or alive...{w}\nEven me, a ghost, was chased around by angels!"

    show mob madam at xy(X=-260) #700

    if ivent05 > 0:
        show mob zyoseiy at center as mob2 #701

    show fairys_a st03 at xy(X=460, Y=100)
    with dissolve #702

    fairy "I was so scared too!"
    woman_a "It's so horrible, they were even attacking young girls..."
    woman_b "If it happens again, we'll have to request the Hero.{w}\nI'm sure he will be able to do something about it..."

    return


label city36a_03:
    show mob rouzin1 at xy(X=-160) #700
    show fairys_f st51 at xy(X=400, Y=100)
    with dissolve #701

    old_man "Sheesh, what a huge commotion...{w}\nAre you alright?"
    fairy "Yes...{image=note}"

    if ivent05 == 0:
        return

    show mob rouzin1 at xy(X=-280) #700
    show mob seineny as mob2 #701
    show fairys_f st51 at xy(X=500, Y=100)
    with dissolve #702

    ghost "The angels attacked me...{w}\nIt seems like they hate ghosts and fairies more than humans."
    old_man "Sheesh, how embarrassing...{w}\nWhy can't we all just get along?"

    return


label city36a_04:
    show mob syounen at xy(X=-220) #700
    show mob syouzyo as mob2 #701
    show fairys_c st21 at xy(X=450, Y=100)
    with dissolve #702

    young_boy "My Fairy friend helped me!"
    girl "She grew grass and ivy around us, and hid us from view!"
    fairy "You're both my dearest friends after all!{image=note}"

    return


label city36a_05:
    show mob sensi1 with dissolve #700

    if ivent05 == 0:
        soldier_a "Ghosts, pixies, and now angels..."
    elif ivent05 == 1:
        soldier_a "First it was pixies, now it's angels..."

    soldier_a "What next? Robotic monsters?{w} Hah, as if that could ever happen."

    return


label city36a_06:
    show mob sensi2 with dissolve #700

    soldier_b "It's too late, the world is over.{w}\nIf even San Ilia is like this, the rest of the world must be in total despair."
    soldier_b "Ahhh... Everyone is going to die...{w}\nAt the least, I'll spend my final days doing something worthwhile.{w} Drinking."

    return


label city36a_10:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "I've lost my zeal for business at the moment.{w}\nI need to get my stuff together and flee the city as soon as possible."
    shopkeeper "But even now, there are still folks who haven't lost their passion for business...{w}\nDamn, I wish I was like that Item Shop's Simon..."

    scene bg 048 with blinds
    return


label city36a_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "The angel's cross...{w} Damn, I don't understand the meaning of it."
    shopkeeper "I tried chucking crosses at the angels like a throwing star...{w} But they're a little awkward to handle."
    shopkeeper "Now just how am I going to sell these...?"

    scene bg 048 with blinds
    return


label city36b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 044 with blinds2
    play music "audio/bgm/castle1.ogg"


label city36b_main:
    call city_null 
    $ city_bg = "bg 044"
    $ city_button01 = "Guard A" #601
    $ city_button02 = "Guard B" #602
    $ city_button03 = "Arguing Priests" #603
    $ city_button04 = "Scholar" #604
    $ city_button05 = "Soldier" #605
    $ city_button10 = "Chapel" #610
    $ city_button11 = "Underground Library" #611
    $ city_button12 = "Audience Chamber" #612
    $ city_button13 = "Guard Office" #613
    $ city_button19 = "To Town" #619

    while True:
        call cmd_city 

        if result == 1:
            call city36b_01 
        elif result == 2:
            call city36b_02 
        elif result == 3:
            call city36b_03 
        elif result == 4:
            call city36b_04 
        elif result == 5:
            call city36b_05 
        elif result == 10:
            call city36b_10 
        elif result == 11:
            call city36b_11 
        elif result == 12:
            call city36b_12 
        elif result == 13:
            call city36b_13 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 048 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city36a_main


label city36b_01:
    show mob heisi1 with dissolve #700

    guard_a "Many priests and soldiers are in despair, and have lost their reason for living.{w}\nBut I'm going to do my best to protect the weak."
    guard_a "As long as you can find something to get you back on your feet, you won't succumb to fear and despair."
    guard_a "The worst people of all are the ones giving their lives over to alcohol as their crutch..."

    return


label city36b_02:
    show mob heisi1 with dissolve #700

    guard_b "It's over...{w} There's no point guarding this castle.{w}\nIlias is going to destroy us all, after all!"

    return


label city36b_03:
    if ivent05 == 0:
        show mob sinkan as mob2 #701
    elif ivent05 > 0:
        show mob sinkany as mob2 #701

    show mob sinkan at xy(X=-220) #700
    show fairys_e st41 at xy(X=560, Y=100)
    with dissolve #702

    priest_a "How could this happen after all the praying to the Goddess we did?{w}\nIs there no power in our prayer and faith?"

    if ivent05 == 0:
        priest "No, this is surely an ordeal from Ilias.{w}\nThose who don't keep their faith to the very end will not have salvation!"
    elif ivent05 > 0:
        ghost_priest "No, this is surely an ordeal from Ilias.{w}\nThose who don't keep their faith to the very end will not have salvation!"

    fairy "Hey, heeey!{w} Let's plaaay!{w}\nHeeeeey!{w} Heeeeeeeey!"
    "The Fairy still seems to be invisible to the priests..."

    return


label city36b_04:
    show mob gakusya with dissolve #700

    scholar "Now that we're able to discuss it...{w}\nUs scholars have always held questions about the books of scriptures."
    scholar "Archaeological studies have found contradictions in them.{w}\nNot only that, but entire histories of peoples seem to have been deleted."
    scholar "Experts in pretty much every field have found falsehoods and lies in the scriptures.{w}\nOf course, we were unable to say anything until now, lest we be labeled heretics."
    l "It's completely made up then?"
    scholar "Five hundred years ago...{w} The era of Heinrich the Hero.{w}\nSince then, big modifications were made in the existing scriptures, and in any new ones created."
    scholar "In addition, the existence of Fairies and Ghosts was thoroughly deleted from them all."
    scholar "Even more, all folklore and legends regarding marriage customs in monsters was completely erased.{w}\nFor the past five hundred years, records of history have been strictly controlled by Ilias..."

    return


label city36b_05:
    show fairys_b st12 at xy(X=430, Y=100)
    show mob sensi1 at xy(X=-160) #700
    with dissolve #701

    soldier "Are you alright?{w} No injuries?{w}\nIf anywhere hurts, I'll take you to the doctor alright?"
    fairy "Thank you, I'm alright..."

    return


label city36b_10:
    scene bg 157 with blinds

    "The Chapel seems to be full of people who lost their homes in the attack."

    show mob ozisan3 at xy(X=160) as mob2
    show mob obasan at xy(X=-160) #700
    with dissolve #701

    woman "Oh Goddess Ilias...{w}\nWhy have you abandoned us?"
    man "What should I believe in now...?"
    l "........."

    hide mob
    hide mob2
    with dissolve

    "People who have lost their faith, and are in despair...{w}\nAnd others who are still clinging to their faith."
    "Each person is reacting differently.{w}\nThe only thing in common, is that they have all lost hope in the future..."

    show alice st74 at xy(Y=50) with dissolve #700

    a "The emotional strength of each person's heart varies wildly based on the person.{w}\nFamily, friends, lovers..."
    a "People whose true passion lies in art, engineering or martial skills.{w}\nOr even in the Heavens itself."
    a "The ones whose true passion lay in their belief in the Goddess are the ones most in shock, of course."
    l "The most important thing is to bring back peace...{w}\nAfter that, I'm sure the San Ilia King can show them their way again!"
    "It doesn't matter if they once had blind faith in the Goddess...{w}\nI'm sure the King will be able to lead the lost here as he is now."

    scene bg 044 with blinds
    return


label city36b_11:
    show mob heisi1 with dissolve #700

    castle_guard "There's no reason to guard the underground library anymore.{w}\nIt's just full of books about Ilias's teachings."
    "The guard seems really depressed.{w}\nIt's like he doesn't even care about blocking us from going in."

    hide mob
    show alice st71 at xy(Y=50)
    with dissolve #700

    a "Luka, do you need anything in the underground library?"
    l "I thought we might be able to find something about breaking the Six Ancestors' Great Seal..."
    a "You think a human library would have something that valuable?{w}\nThat's a top secret angel development, there's no way."
    l "You're right..."
    "There really isn't any reason to go down there.{w}\nWe turn back around."

    return


label city36b_12:
    scene bg 045 with blinds
    show sanilia n st01 with dissolve #700

    san_ilia_king "Ooh, Hero Luka...{w} We are much in your debt.{w}\nFor now, I've sent missives to every church around the world telling the priests to keep the hope of the people alive."
    san_ilia_king "If the situation becomes better, I plan to travel around the world spreading hope.{w}\nOf course, that is after you rescue the world..."
    l "Yes, I'll fight with everything I've got!{w}\nSo I'm counting on you too, King!"
    san_ilia_king "...I understand.{w}\nI shall shoulder the suffering of the people."
    san_ilia_king "Hero Luka, please protect the citizens of this world with your blade.{w}\nAnd as for Ilias..."
    "After worshipping Ilias for so long, he seems unable to manage to finish his sentence.{w}\nBut I know well what he means."
    l "Yes... I will absolutely reclaim peace!"
    "In front of the King, whose role is to guide Heroes...{w}\nI swear to take down Ilias and reclaim peace for the world."

    scene bg 044 with blinds
    return


label city36b_13:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=160) as mob2
    show mob heisi1 at xy(X=-160) #700
    with dissolve #701

    guard_a "I wasn't really that pious to begin with, I just acted like it around the others.{w}\nSince I was sort of lying about it all along, I'm not really suffering from the same shock as the others."
    guard_a "But this guy..."
    guard_b "Ahhh... What should I do now...?{w}\nGoddess Ilias... P...Please guide me...!"
    guard_a "I know how devoted he was, it's really pitiful...{w}\nI really wish I could have led him down the same path of dishonesty as me..."

    scene bg 044 with blinds
    return


label city37a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 064 with blinds2
    play music "audio/bgm/sabasa.ogg"


label city37a_main:
    call city_null 
    $ city_bg = "bg 064"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Women" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Man" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button07 = "Young Boy" #607
    $ city_button08 = "Prostitute" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #613
    $ city_button14 = "Sabasa Castle" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city37a_01 
        elif result == 2:
            call city37a_02 
        elif result == 3:
            call city37a_03 
        elif result == 4:
            call city37a_04 
        elif result == 5:
            call city37a_05 
        elif result == 6:
            call city37a_06 
        elif result == 7:
            call city37a_07 
        elif result == 8:
            call city37a_08 
        elif result == 10:
            call city37a_10 
        elif result == 11:
            call city37a_11 
        elif result == 12:
            call city37a_12 
        elif result == 13:
            call city37a_13 
        elif result == 14:
            jump city37b_main0
        elif result == 19:
            return


label city37a_01:
    show mob seinen1 with dissolve #700

    boy "The vampires calmly went into the castle in broad daylight.{w}\nI saw them... It was like a nightmare."
    boy "The group of jet-black monsters sizing us up as food as they walked down the main road.{w}\nAt the front of them was the Queen Vampire, walking like royalty."
    boy "I still get chills thinking of being ruled over by them...{w}\nYou truly are Sabasa's savior!"

    return


label city37a_02:
    show mob madam at xy(X=160) as mob2
    show mob obasan at xy(X=-160) #700
    with dissolve #701

    woman "This uproar is happening all over the world, right?{w}\nI hope Princess Sara is safe while she studies in Grangold..."
    young_woman "It's one of the four major powers, and their defense force is top notch, too.{w}\nBut I'm still worried..."
    l "........"
    "So the story is she's studying abroad.{w}\nThe fact that she's in the Monster Lord's Castle is top secret, of course."

    return


label city37a_03:
    show mob rouzin1 with dissolve #700

    old_man "For one who loves legends, this really surprised me.{w}\nTo think I would see the Queen Vampire in person..."
    old_man "Just once, I'd like for her to suck on me...{w}\nI'm sure I wouldn't die if it was just once..."

    return


label city37a_04:
    show mob ozisan2 with dissolve #700

    man "I heard that Ilias was the one who commanded the vampires to attack.{w}\nThe pious ones are shocked, but I'm just angry."
    man "The King declared that we would resist Ilias, and wouldn't bend his knee to her.{w}\nOf course, the majority of the citizens feel the same way as the King."

    return


label city37a_05:
    show mob sensi1 with dissolve #700

    soldier_a "I enlisted in the army for this battle.{w}\nMy neighbor was eaten by a monster during it..."
    soldier_a "When I think about if I had hidden at home instead of enlisting...{w}\nUhg, I still shiver."

    return


label city37a_06:
    show mob sensi2 with dissolve #700

    soldier_b "Ever since that battle, the amount of people lusting after vampires has started to increase.{w}\nI'm totally ashamed as a fellow citizen..."
    soldier_b "Of course, I'd never think such a thing.{w}\nI'd never dream about being sucked by a vampire!"

    return


label city37a_07:
    show mob syounen with dissolve #700

    young_boy "In order to protect the Princess, I'm going to become a soldier when I grow up!"

    show mob seinen1 at xy(X=-160) as mob2 with dissolve #701

    boy "I want to apply right away!"

    show mob ozisan3 at xy(X=160) as mob3 with dissolve #702

    middle_aged_man "I've already sent in my application!"

    show mob sensi1 at xy(X=-300) as mob4 with dissolve #703

    soldier "I joined a long time ago!"

    show mob rouzin1 at xy(X=300) as mob5 with dissolve #704

    old_man "I'm already a retired veteran!"
    young_boy "Woooh!"
    l "........."
    "Are the hearts of the citizens lining up...?"

    return


label city37a_08:
    show mob musume2 with dissolve #700

    prostitute "For some reason, Vampire play has become really popular recently.{w}\nCloaks and fake fangs have all gone out of supply."
    prostitute "Of course I'll be open for business until the end of the world!{w}\nMy pride as a prostitute will continue until I die!"

    return


label city37a_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    owner "Ah, so busy, so busy...{w}\nThe army ordered a large quantity of armor and weapons."
    owner "So I'm afraid I don't have any merchandise for the general public at this time.{w}\nI'm sorry, but it's for the good of the country."

    scene bg 064 with blinds
    return


label city37a_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "My holy water to repel vampires is selling like crazy.{w}\nOn the other hand, so is the Vampire themed porno book..."
    shopkeeper "The most popular one has been \"The Night Sucker\"{w}\nIt's about a Vampire who slips into the town at night and sucks her target completely dry."
    l "Something like that exists...?"
    shopkeeper "There have always been black market monster themed porno books.{w}\nBut due to religious reasons, they weren't widely known by the public."
    shopkeeper "In fact, there are quite a lot of monster lovers..."

    show alice st71 at xy(X=-170, Y=50) #700
    show mob ozisan3 at xy(X=160)
    with dissolve #701

    a "If they like monsters so much, they wouldn't need a book.{w}\nThey could just go outside town and offer themselves up..."
    shopkeeper "Of course, it's scary thinking they might kill you or squeeze you to death.{w}\nNo matter how much I like them, to risk my life..."
    shopkeeper "...I mean my customer's who buy them.{w}\nIt's their thing.{w} Yeah... Not me..."

    scene bg 064 with blinds
    return


label city37a_12:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "It can't be...{w} Ilias couldn't have directed the vampires to Sabasa...{w}\nI don't know what to believe anymore..."

    scene bg 064 with blinds
    return


label city37a_13:
    scene bg 155 with blinds
    show mob mermaid1 with dissolve #700

    mermaid "Just when we finally reopened the pub, this uproar killed business..."
    mermaid "What do Ilias Kreuz and Ilias have against us, anyway!?{w}\nAnd to make things even worse, it seems like vampires suddenly got a lot more popular here..."

    scene bg 064 with blinds
    return


label city37b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle2.ogg"


label city37b_main:
    call city_null 
    $ city_bg = "bg 065"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Chief Minister" #603
    $ city_button04 = "Grandmaster Knight" #604
    $ city_button05 = "Scholar" #605
    $ city_button06 = "Priest" #606
    $ city_button10 = "Audience Room" #610
    $ city_button11 = "Princess's Room" #611
    $ city_button12 = "Guard Room" #612
    $ city_button19 = "Leave Castle" #619

    while True:
        call cmd_city 

        if result == 1:
            call city37b_01 
        elif result == 2:
            call city37b_02 
        elif result == 3:
            call city37b_03 
        elif result == 4:
            call city37b_04 
        elif result == 5:
            call city37b_05 
        elif result == 6:
            call city37b_06 
        elif result == 10:
            call city37b_10 
        elif result == 11:
            call city37b_11 
        elif result == 12:
            call city37b_12 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg black with Dissolve(1.5)
            show bg 064 with Dissolve(1.5)
            play music "audio/bgm/sabasa.ogg"
            jump city37a_main


label city37b_01:
    show mob daizin with dissolve #700

    minister_a "After I was released, I was terrified of coming into the Castle and seeing it all bloody...{w}\nBut luckily, it was pretty much untouched."
    minister_a "However the large cellar filled with the wine Sabasa was proud of was completely drained.{w}\nDid those vampire's really drink it all in just one day?"

    show alice st71 at xy(X=-170, Y=50) #700
    show mob daizin at xy(X=170)
    with dissolve #701

    a "I've heard there are some vampires who like alcohol more than blood..."
    l "..........."
    "In a way, perhaps vampires could get along easiest with humans?"

    return


label city37b_02:
    show mob daizin with dissolve #700

    minister_b "This is the worst national crisis since Sabasa was founded.{w}\nRight now I'm working on refuge locations should another surprise attack occur."
    minister_b "We will also need to train the citizens in how to deal with urban warfare, should it come to that.{w}\nBut we don't have the time or resources needed to devote to training...{w} Oh what to do...?"

    return


label city37b_03:
    show mob rouzin1 with dissolve #700

    chief_minister "I wish to thank you for saving Sabasa from the bottom of my heart.{w}\nYou showed impeccable bravery, as expected of Princess Sara's groom."
    l "...Eh?"

    return


label city37b_04:
    show mob sensi2 with dissolve #700

    grandmaster_knight "Even with the world's strongest military, we were struggling.{w}\nJust how are the other countries faring...?"

    return


label city37b_05:
    show mob gakusya with dissolve #700

    scholar "So that was a vampire...{w} How erotically charming.{w}\nJust once, I'd like to be sucked by one..."
    l "Uhm...{w} Are you alright?"
    scholar "...Ah, excuse me.{w} I've just been completely fascinated.{w}\nI've heard there are many others like me, too..."

    return


label city37b_06:
    show mob sinkan with dissolve #700

    priest "I...I was almost made into a vampire's prey.{w}\nT...Thank you for saving me, Ilias...!"
    priest "What?{w} Ilias was the one who made them attack in the first place?{w}\nI won't believe that..."

    return


label city37b_10:
    scene bg 045 with blinds
    show sabasa st01 with dissolve #700

    sabasa_king "Ah, Hero Luka.{w}\nThings have gotten very busy around here."
    sabasa_king "The first order of business is re-structuring the army to get it back into shape.{w}\nAfter that, I need to send personal letters to each country informing them of the situation."
    l "It truly is difficult being the King of a country..."
    sabasa_king "Hero Luka, let's both do everything we can.{w}\nI'll work with the other countries in cooperation to stave off this crisis."
    sabasa_king "Use your blade to take down Ilias!{w}\nThen come and make my daughter your wife."
    l "Not again..."
    "It looks like he still hasn't given that up..."

    scene bg 065 with blinds
    return


label city37b_11:
    scene bg 067 with blinds
    show mob madam with dissolve #700

    chamberlain "I wonder when the Princess will return?{w}\nIt seems as though she marched into the Monster Lord's Castle by herself..."
    chamberlain "Of course, I never revealed that to anyone.{w}\nBut I wonder what business she had there...?"

    scene bg 065 with blinds
    return


label city37b_12:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=160) as mob2
    show mob heisi1 at xy(X=-160) #700
    with dissolve #701

    soldier_a "The previous fight was extremely intense...{w}\nOf course, I've been a part of monster subjugation missions before, but never any that strong."
    soldier_b "We can't fight in battles of that scale too many times.{w}\nAfter one or two more of those, our country will be in a critical state..."

    scene bg 065 with blinds
    return


label city38_main0:
    $ BG = "bg 020"
    play sound "audio/se/habataki.ogg"
    scene bg 072 with blinds2
    play music "audio/bgm/mura2.ogg"


label city38_main:
    call city_null 
    $ city_bg = "bg 072"
    $ city_button01 = "Man" #601
    $ city_button02 = "Village Girl" #602
    $ city_button03 = "Lucia's Younger Sister" #603
    $ city_button04 = "Woman" #604
    $ city_button05 = "Old Man" #605
    $ city_button06 = "Farmer" #606
    $ city_button07 = "Young Boy" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Lord's Mansion" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city38_01 
        elif result == 2:
            call city38_02 
        elif result == 3:
            call city38_03 
        elif result == 4:
            call city38_04 
        elif result == 5:
            call city38_05 
        elif result == 6:
            call city38_06 
        elif result == 7:
            call city38_07 
        elif result == 10:
            call city38_10 
        elif result == 11:
            call city38_11 
        elif result == 12:
            call city38_12 
        elif result == 19:
            return


label city38_01:
    show mob seinen1 with dissolve #700

    man "To think my wife's body was like that...{w}\nOf course, I don't intend to discriminate because of it..."
    man "However, now that I think about it, there were some strange things.{w}\nShe started blindfolding me sometimes during our night activities..."
    man "It felt really amazing those times.{w} I wonder if..."

    return


label city38_02:
    show wormv st11 with dissolve #700

    village_girl "The villagers are treating us as they always did.{w}\nThis village won't ever repeat the tragedies of the past ever again."
    village_girl "No matter how many times they attack us, we'll beat them back.{w}\nWe aren't afraid to show ourselves to defend our hometown."

    return


label city38_03:
    show mob zyosei with dissolve #700

    lucias_younger_sister "My older Sister Lucia studied magic science in Sabasa.{w}\nHer talent and knowledge were both extraordinary..."
    lucias_younger_sister "I'm sure she's still looking for a place that will accept her for who she is.{w}\nI wish she would hurry up and realize it's right back here, at home."

    return


label city38_04:
    show mob obasan with dissolve #700

    woman "We're already past the embarrassing behavior shown during the period of Lily's rule.{w}\nNo matter what they look like, those girls are fine, upstanding members of our village."
    woman "I've even known that girl Lucia since she was a baby...{w}\nIf you ever see her again, please let her know that she's welcome home any time."

    return


label city38_05:
    show mob rouzin1 with dissolve #700

    old_man "Despite that surprise attack, not even a single person lost their life.{w}\nI think Lucia's true feelings kept her from really attacking the village in earnest."
    old_man "That girl was always a kind child...{w}\nI know none of the other villagers have forgotten that."

    return


label city38_06:
    show mob ozisan2 with dissolve #700

    farmer "What's with everyone looking all strange here for?{w}\n...Eh?{w} The world is in danger?"
    farmer "Ah, I'm glad that they're ok...{w} Wait, what?"

    return


label city38_07:
    show mob syounen with dissolve #700

    young_boy "What do you think the proper name of this village originally was?{w}\nI've asked around and looked through books, but I came up with nothing..."
    young_boy "There were some documents in the government buildings mentioning a \"Baron Meister Jurisdiction\".{w}\nBut that's less a village name, and more of just who was in charge of the area..."
    l "It looks like \"Witch Hunt Village\" has been its name for a long time, huh?"
    "This village's true name...{w}\nPerhaps it's already lost for good?"

    return


label city38_10:
    scene bg 151 with blinds
    show mob ozisan2 with dissolve #700

    shopkeeper "Ah, that was dangerous...{w} I was almost a victim of the Iron Maiden."
    shopkeeper "But I sort of wanted to feel it..."
    shopkeeper "I...I wonder what it would have felt like...{w}\nJ...Just for a moment..."

    scene bg 072 with blinds
    return


label city38_11:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "There have been hardships here ever since I first took my post.{w}\nEven worse, we could be attacked again soon..."
    priest "I must set this church up to be able to take in refugees in case of an emergency.{w}\nI need to hurry and collect all of the medicinal supplies I can here..."

    scene bg 072 with blinds
    return


label city38_12:
    scene bg 073 with blinds
    show mob heisi1 with dissolve #700

    soldier "We really need to destroy this mansion.{w}\nWe've talked about it for a while, but kept putting it off..."
    soldier "There are a lot of dangerous chemicals and materials inside, and it's going to be a huge project that nobody really wants to take the initiative on..."

    show alice st71 at xy(X=-170, Y=50) #700
    show mob heisi1 at xy(X=160)
    with dissolve #701

    a "From my brief observation, I saw the distilled mercury drug Necrozyle.{w}\nI also saw \"Hell's Rose\" and \"Fool's Stone\"."
    a "They are hazardous drugs that no normal handler should even get near.{w}\nOnce things have settled down, I'll dispatch a crew from the Monster Lord's Castle."
    l "Such dangerous things just laying around..."
    "We can't just leave them sitting there...{w}\nBut our first priority is the world in danger."

    scene bg 072 with blinds
    return


label city39a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 082 with blinds2
    play music "audio/bgm/city1.ogg"


label city39a_main:
    call city_null 
    $ city_bg = "bg 082"
    $ city_button01 = "Man" #601
    $ city_button02 = "Boy" #602
    $ city_button03 = "Town Girl" #603
    $ city_button04 = "Young Woman" #604
    $ city_button05 = "Noblewoman" #605
    $ city_button06 = "Soldier A" #606
    $ city_button07 = "Soldier B" #607
    $ city_button08 = "Harpy" #608
    $ city_button09 = "Slime Girl" #609
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Minotauros's Milk Shop" #612
    $ city_button13 = "Spider Girl's Sewing Shop" #613
    $ city_button14 = "Alraune's Florist Shop" #614
    $ city_button15 = "Church" #615
    $ city_button16 = "Grand Noah Castle" #616
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city39a_01 
        elif result == 2:
            call city39a_02 
        elif result == 3:
            call city39a_03 
        elif result == 4:
            call city39a_04 
        elif result == 5:
            call city39a_05 
        elif result == 6:
            call city39a_06 
        elif result == 7:
            call city39a_07 
        elif result == 8:
            call city39a_08 
        elif result == 9:
            call city39a_09 
        elif result == 10:
            call city39a_10 
        elif result == 11:
            call city39a_11 
        elif result == 12:
            call city39a_12 
        elif result == 13:
            call city39a_13 
        elif result == 14:
            call city39a_14 
        elif result == 15:
            call city39a_15 
        elif result == 16:
            jump city39b_main0
        elif result == 19:
            return


label city39a_01:
    show mob ozisan1 with dissolve #700

    man "Most of the enemies who invaded headed straight for the castle.{w}\nSo there hasn't been too much damage to the town itself."
    man "But to think... Even though it was for a short time, the Queen was still kidnapped.{w}\nHow crazy."

    return


label city39a_02:
    show mob seinen1 with dissolve #700

    boy "A Lamia attacked me during the confusion.{w}\nShe squeezed me so tight, it was intense."
    l "Th...That sounds horrible..."
    boy "Ahh... I want to do it again..."
    l ".........."

    return


label city39a_03:
    show mob musume1 with dissolve #700

    town_girl "I was captured in the Colosseum as a prisoner.{w}\nThank you for saving me!"
    town_girl "I'm sure you'll save the world, too!"

    return


label city39a_04:
    show mob musume2 with dissolve #700

    young_woman "A monster raped my boyfriend...{w}\nAnd he was so happy after that robot snake monster raped him!"
    young_woman "The next time they show up, I'll fight too.{w}\nThis grudge... This anger... This fury... They will feel all of it!"

    return


label city39a_05:
    show mob madam with dissolve #700

    noblewoman "I'm an even bigger fan of yours now.{w}\nYou marched into the Colosseum so bravely, and rescued the Queen!"
    noblewoman "But I sort of wanted to see your face as you got pleasured...{w}\nI couldn't help but feel a little disappointed."

    return


label city39a_06:
    show mob sensi1 with dissolve #700

    soldier "I've only ever been played with at the Colosseum, but losing outside of it was a real shock.{w}\nThis just proves I'm good for nothing when it comes to actual combat."
    soldier "I disgraced myself...{w} Outside of the Colosseum, I mean.{w}\nEveryone saw my shame...{w} Outside of the Colosseum, I mean."
    l "........."

    return


label city39a_07:
    show mob sensi2 with dissolve #700

    soldier "Although the physical damage to the town was minimal, many men were raped.{w}\nI can't help but feel a little jealous..."

    return


label city39a_08:
    show mob hapy2 with dissolve #700

    fishing_loving_harpy "Just what in the world happened...?{w}\nI was swinging my fishing rod around like crazy as chaos erupted all over the place."
    fishing_loving_harpy "And thanks to that, my favorite rod broke!{w}\nI'll have to stop fishing for a while..."

    return


label city39a_09:
    show mob mamono4 at xy(X=244, Y=80) with dissolve #700

    slime_girl "Purupurupuru... That was so scary!{w}\nMy friend Kenta got hurt, too."
    slime_girl "Eh? Am I alright?{w}\nI just pretended to be a puddle, so I wasn't attacked..."

    return


label city39a_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    weapon_shop_owner "I never thought I'd see monsters lording around the downtown area!{w}\nBut I guess my store was fine since they all headed toward the castle."
    weapon_shop_owner "But for some reason, that sort of made me feel lonely...{w}\nThe guys attacked almost seem happy about it."

    scene bg 082 with blinds
    return


label city39a_11:
    scene bg 151 with blinds
    show mob mamono1 at xy(X=100, Y=-100) with dissolve #700

    elf "Originally, I was a fighter in the Colosseum.{w}\nWhen they attacked, I started shooting arrows at the invaders."
    elf "So now my store is totally out of arrows.{w}\nPerhaps I was a little too enthusiastic with them..."

    scene bg 082 with blinds
    return


label city39a_12:
    show mob mamono5 with dissolve #700

    minotauros_milkmonster "My milk is out of stock for a while.{w}\nEver since I took that arrow to my knee in that battle, it hasn't been tasting too good."
    "I knew it.{w} She herself is the supplier of that milk..."
    minotauros_milkmonster "But was there even a bow user among the invaders...?{w}\nI remember that crazy Elf shooting arrows and laughing like she had gone nuts, but..."
    minotauros_milkmonster "W...Wait a second!"

    return


label city39a_13:
    show mob mamono3 with dissolve #700

    spider_girl_shop_owner "The sign is a lie.{w} It's really Tarantula Girl's Sewing Shop."
    spider_girl_shop_owner "My sewing skills are horrible, so I'm not turning up a profit..."
    spider_girl_shop_owner "That reminds me, there was a call sent out to all spider monsters to assemble.{w}\nI didn't feel like going, so I ignored it."

    show alice st71 at xy(X=-170, Y=50) #700
    show mob mamono3 at xy(X=160)
    with dissolve #701

    a "A call for all spider monsters to assemble?{w}\nCould this be the Spider Princess's doing...?"
    l "The Spider Princess...?"
    a "She's the Queen over all spiders at the moment.{w}\nIf she's calling all of her subordinates together...{w} It can't be good."

    return


label city39a_14:
    show mob mamono2 at xy(X=147) with dissolve #700

    florist_shop_owner "I was so surprised when my flowers all bloomed!{w}\nThey are so beautiful!{w}\nFlower, flower, flower!"

    return


label city39a_15:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "The Goddess is punishing us!{w} Punishing!{w}\nSince this town is filled with monsters, we were punished!"
    l "But didn't they also attack the monsters...?"
    priest "But it must still be Heaven's vengeance!{w} Heaven's vengeance!!!"
    "He's ignoring the reality and clinging to his old beliefs...{w}\nI guess I was like him earlier, wasn't I?"

    scene bg 082 with blinds
    return


label city39b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle3.ogg"


label city39b_main:
    call city_null 
    $ city_bg = "bg 065"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Minister C" #603
    $ city_button04 = "Guard A" #604
    $ city_button05 = "Guard B" #605
    $ city_button06 = "Guard Captain" #606
    $ city_button07 = "Dullahan" #607
    $ city_button10 = "Queen" #610
    $ city_button11 = "Guard Room" #611
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city39b_01 
        elif result == 2:
            call city39b_02 
        elif result == 3:
            call city39b_03 
        elif result == 4:
            call city39b_04 
        elif result == 5:
            call city39b_05 
        elif result == 6:
            call city39b_06 
        elif result == 7:
            call city39b_07 
        elif result == 10:
            call city39b_10 
        elif result == 11:
            call city39b_11 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 082 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city39a_main


label city39b_01:
    show mob daizin with dissolve #700

    minister_a "The fighters in the Colosseum were instrumental in the defense.{w}\nWithout their help, the castle would have fallen."
    minister_a "Of course, you played the biggest role.{w}\nI, and the citizens, thank you from the bottom of our hearts."

    return


label city39b_02:
    show mob daizin with dissolve #700

    minister_b "To think the enemy could have invaded the castle itself...{w}\nSince I'm an old man, they passed me by without raping me."
    minister_b "I'm so jealous...{w}\nErr no, that was..."

    return


label city39b_03:
    show mob daizin with dissolve #700

    minister_c "The fact that Ilias ordered the surprise attack is already all over the town.{w}\nWe must control the unrest and restore order."
    minister_c "We've also heard that the taboo of crossing with monsters has all but gone out the window, too.{w}\nThere are stories of wanton promiscuity in the alleys."
    minister_c "I sort of want to join in...{w}\n...I'm joking, joking."

    return


label city39b_04:
    show mob heisi1 with dissolve #700

    guard_a "To be honest, I was against hiring the Colosseum fighters as Royal Guards.{w}\nI thought if there ever was an emergency, they'd instantly switch sides."
    guard_a "But in fact, they were the ones who fought most bravely.{w}\nI'm ashamed of myself at my discrimination..."

    return


label city39b_05:
    show mob heisi1 with dissolve #700

    guard_b "You were amazing in the Colosseum!{w} I was watching!{w}\nIf I was the one out there, I would have surrendered before even trying to attack!"

    return


label city39b_06:
    show mob sensi2 with dissolve #700

    guard_captain "Some of my men were just raped, but others were actually eaten...{w}\nEven if my opponent is Ilias herself, I'll never forgive her..."

    return


label city39b_07:
    show dullahan st01 at xy(X=228) with dissolve #700

    dullahan "I'm in your debt, Hero.{w}\nI must steadily increase my own strength for next time..."
    dullahan "If enemies are to attack again, I will protect Her Majesty!"

    return


label city39b_10:
    scene bg 045 with blinds
    show grandnoa st01 with dissolve #700

    grand_noah_queen "Oh... If it isn't the Hero Luka.{w}\nJust the sight of you is enough to inspire me with hope."
    grand_noah_queen "As you may have noticed, we have created big countermeasures for another possible invasion.{w}\nIf another assault comes, we will be ready and waiting."

    if not ivent06 < 2:
        "In addition, we have received a letter from the Sabasa King.{w}\nOur wills have become one in this time of crisis."
        "At the center of that is you, Hero Luka."
        l "I'm not that great of a person..."
        grand_noah_queen "No, no... The whole hope of the world rests on your shoulders.{w}\nSo please don't be too reckless."

    scene bg 065 with blinds
    return


label city39b_11:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_a "Luckily, nobody was killed from my unit.{w}\nOh sure a lot of us were raped, and our reputation suffered some..."
    guard_a "Rather, it feels like the guys who weren't raped took some mental damage.{w}\nThey feel like they were passed over because they weren't manly enough, or something."
    guard_b "The monsters ran right by me!{w} I have no appeal!{w}\nThey could all tell that my skill with my Spear is horrible!"

    scene bg 065 with blinds
    return


label city40_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 088 with blinds2
    play music "audio/bgm/yamatai.ogg"


label city40_main:
    call city_null 
    $ city_bg = "bg 088"
    $ city_button01 = "Old Man" #601
    $ city_button02 = "Boy" #602
    $ city_button03 = "Village Girl" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Man" #605
    $ city_button10 = "Cat Shrine" #610
    $ city_button11 = "Shirohebi's Shrine" #611
    $ city_button12 = "Kitsune Shrine" #612
    $ city_button13 = "Meeting Hall" #613
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city40_01 
        elif result == 2:
            call city40_02 
        elif result == 3:
            call city40_03 
        elif result == 4:
            call city40_04 
        elif result == 5:
            call city40_05 
        elif result == 10:
            call city40_10 
        elif result == 11:
            call city40_11 
        elif result == 12:
            call city40_12 
        elif result == 13:
            call city40_13 
        elif result == 19:
            return


label city40_01:
    show mob rouzin1 with dissolve #700

    old_man "I liked those Akaname youngsters.{w}\n...Oops, not good.{w} Gotta stop talking about them, or a bloodbath will occur."
    old_man "I'm just an innocent old man...{w}\nI don't want my wife to go through with the Blood Carnival..."

    return


label city40_02:
    show mob seinen1 with dissolve #700

    boy "All of the people that were a victim of the Miko Lamia have recovered.{w}\nThough they had so much squeezed out of them, many won't be able to stand for a while..."

    return


label city40_03:
    show mob musume1 with dissolve #700

    village_girl "Ever since that attack, all the men can talk about are those monsters.{w}\n\"That monster was beautiful, no that one was more...\"{w}\nIt makes me really angry."

    return


label city40_04:
    show mob rouba with dissolve #700

    old_woman "When the village was attacked, the Elf defenders all rushed over.{w}\nWithout their aid, I think there would have been a lot more victims."

    return


label city40_05:
    show mob ozisan3 with dissolve #700

    man "This all happened right as we were going to build an Ilias Shrine.{w}\nNow we really have to hurry up and build it!"
    l "After what just happened, why would you build it?"
    l "I don't think anyone would pray at it..."
    man "If you don't build a shrine, the Gods will curse you!{w}\nThat's the Yamatai Village way."

    return


label city40_10:
    scene bg 159 with blinds

    if ivent01 == 0:
        call city_noiv 
        scene bg 088 with blinds
        return

    show nekomata st04 at xy(X=200, Y=60)
    show gob st01 at xy(X=-200) #700
    with dissolve #701

    goblin_girl "Ah, Luka!{w}\nThank you for before!"
    nekomata "...Unya.{image=note}"
    "The two are happily eating roasted fish.{w}\nIt looks like they're good friends."

    show alice st71 at xy(X=-240, Y=50) #700
    show gob st01 at xy(X=-30) #701
    show nekomata st04 at xy(X=240, Y=60)
    with dissolve #702

    a "Hum, fish.{w}\nGood temperature.{w} Good seasoning..."

    show gob st04 at xy(X=-30)  with dissolve #701

    goblin_girl "...D...Do you want to try some?"

    show alice st79 at xy(X=-240, Y=50) with dissolve #700

    a "Of course.{image=note}"

    show gob st01 at xy(X=-30) with dissolve #701

    "The three of them sit around a fire, eating roasted fish.{w}\nIt's a heartwarming sight...{w} But I can't forget about the threat to the world."

    show gob st04 at xy(X=-30) with dissolve #701

    goblin_girl "Due to this mess, I won't be able to go back for a while...{w}\nThere are a lot of broken buildings, so I'm going to help clear debris and bring lumber."
    goblin_girl "Is everyone in Iliasburg alright?"
    l "The damage was minimal there, it's all right.{w}\nAnd your three friends are all fine."

    show gob st01 at xy(X=-30) with dissolve #701

    goblin_girl "Yaay!{w} Alright!{w}\nI hope the world gets peaceful soon!"
    nekomata "Unya!"

    scene bg 088 with blinds
    return


label city40_11:
    scene bg 159 with blinds
    show sirohebi st11 with dissolve #700

    shirohebi "We are truly in your debt now.{w}\nThanks to you, the damage we sustained was very minor."
    shirohebi "My older sister is a greedy, aggressive and perverse person...{w} But she is not evil by nature."
    shirohebi "She isn't willing to listen to me at the moment...{w} But I believe if I keep preaching to her, she will eventually go back to how she was in the long past."

    scene bg 088 with blinds
    return


label city40_12:
    scene bg 159 with blinds
    show youko st01 with dissolve #700

    kitsune "I...I was so scared!{w}\nBut I still fought with everything I had in my own way!"
    l "How did you fight?"
    kitsune "I threw stones from a distance...{w}\nAnd threw tacks all over the road..."
    l "Ah... Yeah, it sounds like you did your best..."

    show alice st71 at xy(X=-170, Y=50) #700
    show youko st01 at xy(X=160)
    with dissolve #701

    a "Good, Fox.{w} I'll give you a present!{w}\nHere, fried tofu!"
    kitsune "Yaaay!{w} *Crunch*{w}{nw}"

    show youko st03 at xy(X=160)
    $ renpy.transition(dissolve, layer="master") #701

    extend "{w=.7}\nS...Spicy!{w} Spicy spicy spicy!!!!"

    show alice st72 at xy(X=-170, Y=50) with dissolve #700

    a "Bahahahahaha!{w}\nThe most powerfully spicy fried tofu in the world!"
    a "I was saving it for your master, but this is good too!"
    l "How childish..."

    scene bg 088 with blinds
    return


label city40_13:
    scene bg 090 with blinds

    "Some beds are laid out, with weak looking men laying on them."

    show mob seinen1 at xy(X=-240) with dissolve #700

    youngster_a "I was milked all the way dry by a monster.{w}\nShe kept sucking and sucking until I couldn't even stand..."

    show mob seinen1 at xy(X=-80) as mob2 with dissolve #701

    youngster_b "I was attacked by the Akaname girls!{w}\nThey licked my entire body clean..."

    show mob seinen1 at xy(X=80) as mob3 with dissolve #702

    youngster_c "I was attacked by Kejourou...{w}\nShe forced me to come over and over with her hair!"

    show mob seinen1 at xy(X=240) as mob4 with dissolve #703

    youngster_d "Hmph.{w} Plebeians.{w} I was attacked by Miko Lamia!{w}\nGet on my level."
    l ".............."

    hide mob2
    hide mob3
    hide mob4
    show mob rouzin1 at center
    with dissolve #700

    village_chief "Deplorable.{w}\nI didn't even get to see any of them..."
    l "I...I'm sorry?"

    scene bg 088 with blinds
    return


label city41_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 095 with blinds2
    play music "audio/bgm/plansect.ogg"


label city41_main:
    call city_null 
    $ city_bg = "bg 095"
    $ city_button01 = "Alraune" #601
    $ city_button02 = "Dryad" #602
    $ city_button03 = "Alra Arum" #603
    $ city_button04 = "Alra Rooty" #604
    $ city_button05 = "Alra Mash" #605
    $ city_button06 = "Alra Priestess" #606
    $ city_button10 = "Tarantula Girl" #610
    $ city_button11 = "Moth Girl" #611
    $ city_button12 = "Mosquito Girl" #612
    $ city_button13 = "Caterpillar Girl" #613
    $ city_button14 = "Silkworm Girl" #614
    $ city_button15 = "Hornet Girl" #615
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city41_01 
        elif result == 2:
            call city41_02 
        elif result == 3:
            call city41_03 
        elif result == 4:
            call city41_04 
        elif result == 5:
            call city41_05 
        elif result == 6:
            call city41_06 
        elif result == 10:
            call city41_10 
        elif result == 11:
            call city41_11 
        elif result == 12:
            call city41_12 
        elif result == 13:
            call city41_13 
        elif result == 14:
            call city41_14 
        elif result == 15:
            call city41_15 
        elif result == 19:
            return


label city41_01:
    show mob mamono2 at xy(X=157) with dissolve #700

    alraune "This is the village of coexistence, Plansect.{w}\nWith Alra Priestess as the new Queen, our beliefs are all in synch."

    return


label city41_02:
    show dryad st01 with dissolve #700

    dryad "If it wasn't for the sacrifice of our previous Queen, just how many of us would have been killed...?"
    dryad "She traded her own life for our own..."

    show dryad st11 with dissolve #700

    dryad "...I'll kill all of those assholes!{w}\nI'll never forgive those heaven pieces of shit!"

    show dryad st01 with dissolve #700

    dryad "Ah...{w} Excuse me.{w}\nEver since then, my bad side has come out a couple times..."

    return


label city41_03:
    show a_alm st01 at xy(X=200) with dissolve #700

    alra_arum "We've reaffirmed the oath we took before.{w}\nNever again will we contest this shared village."
    alra_arum "I feel that our mutual struggles are strengthening the bond between plant and insect."

    return


label city41_04:
    show a_looty st03 at xy(X=220, Y=130) with dissolve #700

    alra_rooty "Almost all of my sap was sucked up...{w}\nThose plant people are all unfeeling losers...!"
    alra_rooty "...Eh, wait a second..."

    return


label city41_05:
    show a_mash st01 at xy(X=260, Y=70) with dissolve #700

    alra_mash "Those three Canaan sisters are still sealed.{w}\nQueen Alraune's death came as a huge shock to them."

    return


label city41_06:
    show a_emp st11b at xy(X=230, Y=60) with dissolve #700

    alra_priestess "The rank of Queen...{w} It's not something I will be used to for a long time.{w}\nBut I will not fold under the pressure of this position."
    alra_priestess "I must mobilize everyone for a time of upcoming hardship.{w}\nAs Queen, I will do everything I can."

    return


label city41_10:
    show mob mamono3 with dissolve #700

    tarantula_girl "Right as I thought our long war was over, a new global one starts...{w}\nIt's only a matter of time until an army assaults this village."
    tarantula_girl "Before that happens, we're using the precious time to fix our defenses and assemble our own soldiers.{w}\nBut the trouble comes when we're unable to even damage the angels..."

    return


label city41_11:
    show moss st01 at xy(X=230, Y=20) with dissolve #700

    moth_girl "Even us insects were saved by Queen Alraune's sacrifice.{w}\nWe have no complaints about joining together with the plant family now."

    return


label city41_12:
    show mosquito st01 at xy(X=220, Y=70) with dissolve #700

    mosquito_girl "I flew all over the forest with Arum to help all of our friends.{w}\nI think our friendship is growing stronger by the day..."

    return


label city41_13:
    show imomusi st02 at xy(X=180, Y=130) with dissolve #700

    caterpillar_girl "Thank you for helping us again!{image=note}{w}\nThis makes two times you've saved us..."
    caterpillar_girl "Have a delicious orange!{image=note}"

    show alice st79 at xy(X=-170, Y=50) #700
    show imomusi st02 at xy(X=340, Y=130)
    with dissolve #701

    a "Mmm... Sweet.{w}\nMmm... Delicious."
    l "Don't eat so fast..."

    return


label city41_14:
    show kaiko st01 with dissolve #700

    silkworm_girl "To think I would be the one about to be sucked dry...{w}\nBut... It did feel very good.{w} I think I could get addicted to it..."

    return


label city41_15:
    show suzumebati st01 with dissolve #700

    hornet_girl "OUR BATTLE POTENTIAL... LOWER.{w}\nWITHOUT QUEEN, GREAT DANGER."

    return


label city42_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 100 with blinds2
    play music "audio/bgm/city1.ogg"


label city42_main:
    call city_null 
    $ city_bg = "bg 100"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Young Woman" #602
    $ city_button03 = "Man" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Ant Girl" #605
    $ city_button06 = "Mud Golem Girl" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Magic Laboratory" #612
    $ city_button13 = "Grangold Castle" #613
    $ city_button14 = "Assembly Hall" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city42_01 
        elif result == 2:
            call city42_02 
        elif result == 3:
            call city42_03 
        elif result == 4:
            call city42_04 
        elif result == 5:
            call city42_05 
        elif result == 6:
            call city42_06 
        elif result == 10:
            call city42_10 
        elif result == 11:
            call city42_11 
        elif result == 12:
            call city42_12 
        elif result == 13:
            call city42_13 
        elif result == 14:
            call city42_14 
        elif result == 19:
            return


label city42_01:
    show mob seinen1 with dissolve #700

    boy "The Ant Girls helped us again this time.{w}\nIf not for them, Grangold would be gone..."

    return


label city42_02:
    show mob musume2 with dissolve #700

    young_woman "My boyfriend left me for an Ant Girl!{w}\nI stormed out of the house in anger..."
    young_woman "Eh?{w} Why was an Ant Girl in our house?{w}\nWell, because I got her to do all the housework instead of me..."

    return


label city42_03:
    show mob ozisan2 with dissolve #700

    man "A...An Ant Girl helped me...{w}\nShe carried me to a safe place when I couldn't even stand!"
    man "I was always complaining about them, too... That we would never be able to live in peace when they were around..."

    return


label city42_04:
    show mob rouba with dissolve #700

    old_woman "First ants, then spiders...{w} I'm so sick of insects.{w}\nBut I'll thank the Ant Girls at least for helping me."

    return


label city42_05:
    show ant st01 with dissolve #700

    ant_girl "PROTECT FROM ATTACK.{w}\nNEED WALLS HERE..."
    "The Ant Girls seem to be absorbed in constructing new defensive fortifications."

    return


label city42_06:
    show madgolem st01 with dissolve #700

    mud_golem_girl "CURRENTLY BUILDING WALLS.{w}\nDANGEROUS HERE. BE CAREFUL."

    return


label city42_10:
    scene bg 151 with blinds
    show mob ozisan1 at xy(X=-160) #700
    show ant st01 at xy(X=160)
    with dissolve #701

    ant_girl "SORRY.{w} CLOSED NOW."
    shopkeeper "All of my weapons were sold during the last fight.{w}\nI wonder if it's the same for every weapon shop around the world?"

    scene bg 100 with blinds
    return


label city42_11:
    scene bg 156 with blinds
    show ant st01 with dissolve #700

    ant_girl "I SHALL PRAY FOR YOU..."
    l "Are you sure?{w} I mean this is a church that was built for Ilias after all..."
    ant_girl "PRAYER..."
    l "Ah... Ok... Sure."
    "After I pray with the Ant Girl, I leave the church."

    scene bg 100 with blinds
    return


label city42_12:
    scene bg 161 with blinds
    show mob madou with dissolve #700

    magic_engineer "The Golem was destroyed by the attackers, and we're all busy with her repair.{w}\nI'm taking over from my predecessor at such a confusing time..."
    l "Did the former person in charge quit?"
    magic_engineer "He took responsibility for the Ant Girls rebellion, and was fired... Ostensibly.{w}\nIn truth, he was doing some illegal things, underneath even the King's nose."
    magic_engineer "He made the power source of the Golem convicted criminals.{w}\nAnd stockpiling poisons against international treaties."
    magic_engineer "We've completely cut relations with Promestein, as well.{w}\nOr rather, she hasn't tried to contact us."

    scene bg 100 with blinds
    return


label city42_13:
    l "Nobody is allowed through here?"
    "The Castle seems to have been damaged in the last attack again.{w}\nThe entrance is blocked off for repairs."

    return


label city42_14:
    scene bg 105 with blinds
    show grangold st04 with dissolve #700

    grangold_king "I feel like I'm always in this warehouse."
    grangold_king "Just when I was thinking the Castle was going to be repaired after the rebellion...{w}\nIt gets attacked and destroyed again."
    grangold_king "I wonder if my reign will be nicknamed \"The Warehouse King\"."

    show grangold st04 at xy(X=-160) zorder 2 #700
    show queenant st01 at xy(X=160)
    with dissolve #701

    queen_ant "Great things remain to be completed.{w}\nI'm sure you will be remembered for those, not this warehouse."
    grangold_king "Hmmm...{w} But I'd like a nickname like the founder's \"Magic King\" or the third generation's \"Patriot King\"."
    grangold_king "So I must do my best to survive this time of national crisis."
    grangold_king "Luka, let us do our best together!{w}\nThe King as King, and the Hero as the Hero!"

    scene bg 100 with blinds
    return


label city43_main0:
    $ BG = "bg 041"
    play sound "audio/se/habataki.ogg"
    scene bg 103 with blinds2
    play music "audio/bgm/mura2.ogg"


label city43_main:
    call city_null 
    $ city_bg = "bg 103"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Prostitute" #602
    $ city_button03 = "Young Sisters" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Fortuneteller" #605
    $ city_button06 = "Amira" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Bar" #612
    $ city_button13 = "Mayor's House" #613
    $ city_button14 = "Ceremony Room" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city43_01 
        elif result == 2:
            call city43_02 
        elif result == 3:
            call city43_03 
        elif result == 4:
            call city43_04 
        elif result == 5:
            call city43_05 
        elif result == 6:
            call city43_06 
        elif result == 10:
            call city43_10 
        elif result == 11:
            call city43_11 
        elif result == 12:
            call city43_12 
        elif result == 13:
            call city43_13 
        elif result == 14:
            call city43_14 
        elif result == 19:
            return


label city43_01:
    show mob seinen1 with dissolve #700

    feed_a "Even though they sucked too much, they kept sucking...{w}\nAhh... I'm so dizzy..."

    return


label city43_02:
    show maccubus st11 at xy(X=175)
    with dissolve #700

    maccubus "To think they would come to this village to steal our men!{w}\nThey are our common property... Such impudence!"

    return


label city43_03:
    show minccubus st01 at xy(X=335, Y=50)
    show renccubus st01 at xy(X=15) #700
    with dissolve #701

    lencubus "I fought as hard as I could this time, but..{w}\nI never thought such an intense barrier could be set up!"
    minccubus "I fought my hardest too!{w}\nEven though I wasn't too useful..."

    return


label city43_04:
    show mob sensi1 with dissolve #700

    feed_b "Haa! Haa! Hoo!"
    "There seem to still be some after effects..."

    return


label city43_05:
    show witchs st11 at xy(Y=50) with dissolve #700

    witch_succubus "Even my fortune telling wasn't able to predict that...{w}\nI still can't predict what's going to happen in the future, either."
    witch_succubus "But I can tell it depends on your sword.{w}\nI'm looking forward to the future, little Hero.{image=note}"

    return


label city43_06:
    play music "audio/bgm/amira.ogg"
    show amira st01 with Dissolve(1.5) #700

    amira "It's Amira, the one who watches over you day and night.{w}\nDo your best!{w} We'll be watching you closely from the shadows!"
    l "That's kind of scary..."

    show amira st01 at xy(X=-160) #700
    show zannen st01 at xy(X=180)
    with Dissolve(1.5) #699

    pyhar "I wanted hot guys!{w} They gave me fried potatoes!{w}\nDid you think I'd eat them without noticing the difference!?{w} Stupid Village Mayor!"

    hide amira
    show zannen st02 at xy(X=-200) as zannen2
    with Dissolve(1.5)  #698

    diamrem "W...Water..."

    hide zannen
    show zannen st03 at xy(X=160) as zannen3
    with Dissolve(1.5) #697

    ga_star_d "................."
    l "............."

    hide zannen2
    show alice st71 at xy(X=-170, Y=50) behind zannen3
    with Dissolve(1.5) #700

    a ".............."

    play music "audio/bgm/mura2.ogg"
    return


label city43_10:
    scene bg 151 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "I just fell down and started ejaculating...{w} It didn't feel good at all!{w}\nU...Uhh...{w} Uwaaaah!"
    "He's crying a lot..."

    scene bg 103 with blinds
    return


label city43_11:
    scene bg 156 with blinds

    "\"The Church is closed. The Priest is corrupt.\" is written on a note attached to the door.{w}\nI wonder if they ever used the Church in this village?"

    scene bg 103 with blinds
    return


label city43_12:

    "\"Closed\" is posted on the door.{w}\nI guess it wouldn't make sense to open up during the attack going on."

    return


label city43_13:
    scene bg 106 with blinds
    show succubus st01 at xy(Y=50) with dissolve #700

    village_chief "Thank you for helping me out.{w}\nIn appreciation, I'm going to make you an honorary citizen."
    l "No... That's alright.{w}\nBy the way, what's your name Ms. Mayor?"
    "I've actually been curious for a while now...{w} I've never heard anyone call her by her name."
    village_chief "Ara, my name?{w}\nMy name is Succubus."
    l "Eh...?{w} Your name itself is Succubus?"

    show alice st71 at xy(X=-170, Y=50) #700
    show succubus st01 at xy(X=160, Y=50)
    with dissolve #701

    a "It may be strange from a human naming sense, but high-ranking monsters will grant their race name to their daughters."
    a "In particular, the leaders of the tribe will bestow their race name upon their children.{w}\nI'm sure Succubus after Succubus was the mayor of this village, right?"
    village_chief "That's right, Monster Lord.{w}\nThose who inherit our family's birthright have been named Succubus for generations."
    l "I see... So that's how it is."
    "I get it.{w}\nThe Mayor's name is Succubus."

    scene bg 103 with blinds
    return


label city43_14:
    scene bg 110 with blinds
    show lilith st01 at xy(X=138) with dissolve #700

    lilith "I never thought I would see the Great Barrier of Babylon in this era.{w}\nThe one who first created it was the legendary Succubus nicknamed the \"Great Whore of Babylon\"."
    l "A legendary Succubus...{w}\nIsn't that what you two were also called?"
    lilim "It's true we became famous around five hundred years ago...{w}\n...But we aren't in that mythological class, unfortunately."
    lilim "I think that somewhere through the years, our stories got mixed up with the Great Whore of Babylon's."

    show alice st74 at xy(X=-170, Y=50) #700
    show lilith st01 at xy(X=298)
    with dissolve #701

    a "A mixture of legends... A common happening.{w}\nIsn't a case of mistaken identity not too far fetched, after hundreds of years?"
    l "Then could there be another legendary Succubus out there?"

    hide alice
    show lilith st01 at xy(X=138)
    with dissolve #700

    lilith "One of the six ancestors, the progenitor of all Succubi...{w}\nA mythological level Succubus worthy of the title of Great Whore of Babylon."
    lilith "There's no way we could have been mistaken for her and summoned on accident..."
    lilim "Even if we say that, we were still the Queen Succubi from five hundred years ago.{w}\nWe may not have been mythological level...{w} But we were still the top Succubi."
    l "I...I understand."
    "They seem to take it personally..."

    scene bg 103 with blinds
    return


label city44_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 119 with blinds2
    play music "audio/bgm/gordport.ogg"


label city44_main:
    call city_null 
    $ city_bg = "bg 119"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Town Girl" #602
    $ city_button03 = "Lupton" #603
    $ city_button04 = "Arakure" #604
    $ city_button05 = "Soldier" #605
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Ilias Kreuz Headquarters" #612
    $ city_button13 = "Port" #613
    $ city_button14 = "Hero's Shrine" #614
    $ city_button15 = "Cemetery" #615
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city44_01 
        elif result == 2:
            call city44_02 
        elif result == 3:
            call city44_03 
        elif result == 4:
            call city44_04 
        elif result == 5:
            call city44_05 
        elif result == 10:
            call city44_10 
        elif result == 11:
            call city44_11 
        elif result == 12:
            call city44_12 
        elif result == 13:
            call city44_13 
        elif result == 14:
            call city44_14 
        elif result == 15:
            call city44_15 
        elif result == 19:
            return


label city44_01:
    show mob seinen1 with dissolve #700

    boy "A...A machine monster attacked me...{w}\nWhat should I do if they come back?"
    boy "Please, Hero... Protect our town forever..."

    return


label city44_02:
    show mob musume1 with dissolve #700

    town_girl "The Ant Girls are better than the mechanical monsters.{w}\nI guess I should go back to Grangold..."

    return


label city44_03:
    show mob gorotuki3 with dissolve #700

    lupton "I can't believe Lazarus protected the town at the cost of his own life.{w}\nI never thought he'd do something like that."
    lupton "I heard that he was some straight shooting big shot soldier in his youth.{w}\nMaybe he somehow reclaimed his past at the end?"
    lupton "Anyway, as his former vice chief, I'll pray for him to rest in peace."
    l "You're the former vice chief of Ilias Kreuz?{w}\nHey...{w} Haven't I seen you somewhere before?"
    lupton "Hey!{w} Did you forget?{w}\nWe met at the western shrine!"
    lupton "Don't ya remember me explaining about that huge egg?{w}\nI'm the vice chief of Ilias Kreuz!"
    lupton "...No, former vice chief.{w}\nIlias Kreuz has been completely wiped out..."
    l "............."

    return


label city44_04:
    show mob gorotuki1 with dissolve #700

    arakure "My friend was taken by those machine soldiers!{w}\nDamn it... Where did he go!?"
    arakure "Hey... Please, can you help us out?{w}\nThe guy who was kidnapped has a lot of friends looking for him..."
    l ".............."
    "I want to help, but I have no idea where he might have been taken..."

    return


label city44_05:
    show mob sensi1 with dissolve #700

    soldier "I can't believe Ilias sent those machine monsters at us.{w}\nI was never a firm believer, but it still left me in shock."
    soldier "Odd as it may sound, there weren't too many pious believers here to begin with.{w}\nStrange, when you think that Ilias Kreuz was headquartered here..."

    return


label city44_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "My weapons are selling like crazy... I can't keep any in stock!{w}\nI bet there's a weapon shortage all over the world."
    shopkeeper "If I may say so myself, I'm a pretty good sword smith.{w}\nIf the demand is this high, I'm going to make a killing!"

    scene bg 119 with blinds
    return


label city44_11:
    scene bg 154 with blinds
    show mob ozisan2 with dissolve #700

    shopkeeper "We've got a lot of parts from the broken machine monsters, but the technology in them is insane."
    shopkeeper "I always thought the tales about Ilias were all old, boring stories...{w} To think she had this kind of advanced tech available!"

    scene bg 119 with blinds
    return


label city44_12:
    scene bg 117 with blinds

    l "............"
    "With Lazarus dead, nobody is here.{w}\nIt seems like the organization known as Ilias Kreuz has fallen apart."

    scene bg 119 with blinds
    return


label city44_13:
    scene bg 118 with blinds
    show mob sentyou with dissolve #700

    captain "I've heard rumors of a ghost ship sailing along the coast.{w}\nI'm sure it's a lie, but they said it was flying the flag of Captain Selene..."
    captain "But if it's true, does she have some sort of regret here?{w}\nThis town was known as her hometown, after all..."

    scene bg 119 with blinds
    return


label city44_14:
    scene bg 092 with blinds
    show mob sinkan at xy(X=220) as mob3
    show mob sinkan as mob2 #701
    show mob sinkan at xy(X=-220) #700
    with dissolve #702

    sage_a "We are the Three Sages..."
    sage_b "That surprise attack shocked the world.{w}\nEh? Shocked?{w} Or should it be astounded?"
    sage_c "It doesn't matter!"
    "These three seem to be less energetic than before...{w}\nThey must be shocked at what happened."
    l "Are you three alright?{w} You didn't evacuate, did you?"
    sage_a "They didn't want to attack three old men."
    sage_b "We're just going to play cards here until the end of the world."
    sage_c "True Hero, please join us.{w}\nPlaying Hearts with three people is boring..."
    l "No... I'm alright."
    "I refuse the invitation to join their game, and quickly leave."

    scene bg 119 with blinds
    return


label city44_15:
    scene bg 168 with blinds
    play music "audio/bgm/kanasimi2.ogg"

    l ".........."
    "This is where my father's grave is.{w}\nI don't even really know all that much about him."
    "Why did he first decide to go on a journey to subdue the Monster Lord?{w}\nWhen did he meet my mother, a Fallen Angel?"
    "What exactly made him decide to stop exterminating monsters?{w}\nAnd why did he start protecting monsters by sending them to Enrika?"
    l "I want to know more about you, father..."
    "I'm sure Lazarus knew a lot, but he's gone too.{w}\nAnd my mother is resting in Iliasburg..."
    "In other words, the three who knew about my parent's past are gone."

    show alice st71 at xy(Y=50) with dissolve #700

    a "Nobody remains who knows of his story...{w}\nJust what sorts of adventures did your father go on?"
    l "I want to know, but there's nothing I can do...{w}\nAll I can do... Is stand here."
    l "I'm sure my father's death was not a waste.{w}\nBecause I'm standing here, carrying on his wishes."

    show alice st74 at xy(Y=50) with dissolve #700

    a "And I'm here beside you.{w}\nFate is strange, isn't it?"
    "We vow again in front of my father's grave.{w}\nWith our own hands, we'll create a world where humans and monsters can coexist peacefully."

    play music "audio/bgm/gordport.ogg"
    scene bg 119 with blinds
    return


label city45_main0:
    scene bg 149 with blinds2
    $ hanyo[0] = 0
    play music "audio/bgm/maouzyou2.ogg"


label city45_main:
    call city_null 
    $ city_bg = "bg 149"
    $ city_button01 = "Vampire" #601
    $ city_button02 = "Behemoth" #602
    $ city_button03 = "Elder Succubus" #603
    $ city_button04 = "First Lounge" #604
    $ city_button05 = "Second Lounge" #605
    $ city_button06 = "Dining Hall" #606
    $ city_button07 = "Large Library" #607
    $ city_button10 = "Conference Room" #610
    $ city_button11 = "Granberia's Room" #611
    $ city_button12 = "Alma Elma's Room" #612
    $ city_button13 = "Tamamo's Room" #613
    $ city_button14 = "Erubetie's Room" #614
    $ city_button15 = "Monster Lord's Throne" #615
    $ city_button19 = "Back to my Room" #619

    while True:
        call cmd_city 

        if result == 1:
            call city45_01 
        elif result == 2:
            call city45_02 
        elif result == 3:
            call city45_03 
        elif result == 4:
            call city45_04 
        elif result == 5:
            call city45_05 
        elif result == 6:
            call city45_06 
        elif result == 7:
            call city45_07 
        elif result == 10:
            call city45_10 
        elif result == 11 and hanyo[0] == 0:
            call city45_11 
        elif result == 11 and hanyo[0] == 1:
            call city45_11k 
        elif result == 12:
            call city45_12 
        elif result == 13:
            call city45_13 
        elif result == 14:
            call city45_14 
        elif result == 15:
            call city45_15 
        elif result == 19:
            return


label city45_01:
    show vampire st01 at xy(X=175) with dissolve #700

    vampire "I was never hostile to humans to begin with.{w}\nMost everyone inside the Monster Lord's Castle is a member of the reconciliation faction, too."
    vampire "Most monsters who were in opposition to the reconciliation approach toward humans left the castle during the reign of the previous Monster Lord."
    vampire "Though there were some who remained that didn't take a stance one way or the other."

    return


label city45_02:
    show behemoth st01 at xy(X=-30) with dissolve #700

    behemoth "Those Chimeras are crazy strong.{w}\nEven us beast type monsters are being overpowered by their strength and agility."
    behemoth "If we don't start using some teamwork, we won't win against them.{w}\nI don't like it, but I guess there's no choice but to do some joint operations with humans, huh?"

    return


label city45_03:
    show esuccubus st01 at xy(X=188) with dissolve #700

    elder_succubus "To think I'd be fighting alongside a human Hero...{w}\nBut I know well that you wield that sword for our sakes, too."

    show esuccubus st02 at xy(X=188) with dissolve #700

    elder_succubus "Ah... But, you know...{image=note}{w}\nIf you let me have just a small taste of your semen, all of my misgivings will be completely gone.{image=note}"
    elder_succubus "Hey, just let me suck a little, please?{image=note}"

    return


label city45_04:
    scene bg 230 with blinds

    if ivent08 < 2 or ivent11 < 2:
        call city_noiv 
        scene bg 149 with blinds
        return

    show grandnoa st01 at xy(X=160)
    show grangold st04 at xy(X=-160) #700
    with dissolve #701

    grangold_king "It has been two years, hasn't it?{w}\nYou haven't changed a bit since then, I see."
    grand_noah_queen "And you...{w} Seem to look more like a King than before.{w}\nWhen last we were together, I was worried about whether you would fit into the role of King or not..."
    grangold_king "Haha... That was embarrassing, wasn't it?"
    "Two of the rulers of the major powers seem to be talking on really familiar terms..."
    l "You two know each other already?"
    grangold_king "Until a few decades ago, Grangold and Grand Noah were on bad terms.{w}\nOur countries fought over who had claim to the Lima mine, where you could mine magic stones."
    l "I see..."
    grangold_king "But after years of war, the country grew tired of constant strife.{w}\nSo, a double marriage alliance was proposed."
    grangold_king "The Prince of Grand Noah was to wed the daughter of the Grangold King.{w}\nAnd the Grand Noah Princess to the Grangold Prince."
    grangold_king "The children birthed from those respective unions are the current Grand Noah Queen, and me.{w}\nSo it's slightly convoluted, but we are related by blood."
    grand_noah_queen "Our lineage itself is the proof of friendship between our two countries.{w}\nSince the double marriage, there hasn't been a single battle."
    grand_noah_queen "I hope these friendly relations will continue forever...{w}\n...But first we have to survive this crisis threatening the world."
    grangold_king "We'll both do our best.{w}\nLuka, you have the most difficult role...{w} But we trust you."
    l "Yes, leave it to me!"
    "My determination redoubles in front of the two related leaders."

    scene bg 149 with blinds
    return


label city45_05:
    scene bg 230 with blinds

    if ivent01 < 2:
        call city_noiv 
        scene bg 149 with blinds
        return

    show mob obasan with dissolve #700

    "The woman lounging on a sofa in the room...{w}\nIt's the owner of the Sutherland Inn from Iliasburg!"
    l "Miss, why are you here in the Monster Lord's Castle?"
    owner "I'm here as a representative of Iliasburg..."
    owner "Iliasburg is currently the economic power in the world.{w}\nOur leadership is run by a council of ten members, drawn from the top ten biggest guilds."
    owner "And I'm the leader of the Inn guild.{w}\nSo the council voted to send me here as the representative of Iliasburg."
    l "I see, so that's why..."
    owner "I didn't participate in the meeting because I was hesitant to sit alongside all those Kings and Queens."
    owner "But I heard the outline of the strategy.{w}\nIliasburg will fully cooperate."
    l "Yes, let's do our best!"
    owner "...By the way, have you seen my little Lammy?{w}\nI brought her with me, but I think she got lost somewhere."
    l "If I see her, I'll let you know."
    "I wouldn't be surprised if she got lost in this huge Castle.{w}\nSince she's a monster herself, I doubt she'll run into any problems, but still..."

    scene bg 149 with blinds
    return


label city45_06:
    scene bg 170 with blinds

    if ivent06 < 2 or ivent14 < 1:
        call city_noiv 
        scene bg 149 with blinds
        return

    show sabasa st03 at xy(X=-160) #700
    show sara st33 at xy(X=200)
    with dissolve #699

    sabasa_king "I thought you admired Granberia, and wished to be her apprentice for swordplay?{w}\nWhy are you dressed as a maid and serving people here?"
    sara "Uhm...{w}\nIt's true I admire Granberia, and came here after her..."
    "Sara cringes as her father interrogates her.{w}\nAlso, I think both of their uses of the word \"admire\" may not be meaning the same thing..."
    sabasa_king "...Hrrm?{w}\nSara... You... You aren't..."
    "As the Sabasa King stares at Sara, his eyes slowly open wide.{w}\nI think he just noticed the inhuman power hiding behind her eyes..."
    sara "F...Father..."

    show sabasa st04 at xy(X=-160) with dissolve #700

    sabasa_king "I see...{w} So your monster blood awoke?"
    sara "I...I'm sorry..."
    sabasa_king "Was the true reason you left Sabasa because your blood awoke?{w}\nSo you came here to the Monster Lord's Castle yourself..."
    sara "No, that isn't exactly why...{w}\nBut I have been taught here how to control my power."

    show sabasa st03 at xy(X=-160) with dissolve #700

    sabasa_king "Hmmhmmhmm...{w}\nIt isn't unheard of for our royal line... But..."
    "The King's gaze turns toward me."

    show sabasa st04 at xy(X=-160) with dissolve #700

    sabasa_king "As you can see, my daughter has turned into a partial monster.{w}\nBut you, as a firm believer in coexistence, have no issues with this, correct?"
    l "............"
    "What a tricky question...{w} If I say no, it's like I'm accepting his proposal."
    "If I say yes, It's like I'm refuting my views on coexistence, even though that's not really what he's asking about..."
    "I can't help but reluctantly nod..."
    l "Whether human or monster, it doesn't matter..."

    show sabasa st02 at xy(X=-160) with dissolve #700

    sabasa_king "I see, thank you.{w}\nI'm entrusting my daughter to you."
    l ".............."
    sara ".............."
    "This is getting complicated..."

    scene bg 149 with blinds
    return


label city45_07:
    scene bg 172 with blinds

    if ivent18 < 2:
        call city_noiv 
        scene bg 149 with blinds
        return

    show sanilia n st04 with dissolve #700

    "The large, quiet library where monsters don't seem to ever go...{w}\nSurprisingly, the San Ilia King is here moving about restlessly."
    san_ilia_king "Incredible!{w} The original copy of \"The Magic Hammer\" still exists!{w}\nAnd the complete set of the \"Noel Historia\"!!!"
    san_ilia_king "Mountains of fabled books, believed to have been lost from this world...{w}\nOoooh!{w} The \"Gnosis Book of Hours\"!"
    "The King is shaking in excitement before a pile of books."
    san_ilia_king "Honestly, I want to live here for a decade and just read!{w}\nThe catalogue!{w} Where is the library's catalogue!?"

    show sanilia n st02 at xy(X=-160) #700
    show fairys_a st01 at xy(X=370, Y=75) behind sanilia
    with dissolve #701

    fairy "These books are so difficult...{w}\nHey, hey, is that a picture book?"
    san_ilia_king "This corner seems to have picture books for monsters.{w}\n\"Spirit Sylph's Chi Pa Pa\"...{w} I've never heard of this one before."

    if ivent14 < 1:
        scene bg 149 with blinds
        return

    show queenfairy st01 at xy(X=240)
    show fairys_a st01 at xy(X=250, Y=75) #701
    show sanilia n st02 at xy(X=-250) #700
    with dissolve #702

    queen_fairy "Oh my... You seem to be quite attached to him.{w}\nFor Fairies to be so friendly toward a religious leader..."
    fairy "Yaay!{w} It's the Queen!"
    san_ilia_king "...These girls helped changed my mind.{w}\nWe humans have been too ignorant about the beings known as monsters."
    san_ilia_king "We believed them pure evil, and our fear turned into hostility.{w}\nOur religious teachings themselves only served to amplify that vicious circle."
    queen_fairy "Hmm... So these girls can open even the most closed of human hearts.{w}\nThat they became so attached to you is proof that at your core you are a sincere, honest human."
    queen_fairy "So please take care of my daughters in the future, too."
    queen_fairy "And I hope that in the future the entire race known as Fairies will come to be accepted as you have accepted these girls.{w} It would bring me great happiness."
    san_ilia_king "Of course, I shall make great efforts for that.{w}\nBut first, we must regain peace."

    scene bg 149 with blinds
    return


label city45_10:
    scene bg 169 with blinds

    if check06 == 0 or ivent01 < 2 or ivent04 < 2:
        call city_noiv 
        scene bg 149 with blinds
        return

    "When I come back to the conference room...{w}\nThere are even more monsters around than there were during the summit!"

    show queenharpy st11 at xy(X=-250) #702
    show queenelf st01 at xy(X=40) #701
    show queenfairy st01 at xy(X=300)
    with dissolve #700

    queen_harpy "The monsters who rule each locale...{w}\nThis should be quite an interesting meeting."
    queen_elf "More than just humans, we don't even truly know each other.{w}\nWe will need meetings like this to communicate and come to understand each other..."
    "Queen Harpy, Queen Elf, Queen Fairy...{w}{nw}"

    hide queenharpy
    hide queenelf
    hide queenfairy
    show a_emp st21 at xy(X=300, Y=30)
    show queenant st01 at xy(X=-200) #700
    $ renpy.transition(dissolve, layer="master") #701

    extend "{w=.7}\nQueen Ant, and even the new Queen Alraune..."

    hide queenant
    hide a_emp
    show succubus st01 at xy(X=160, Y=50)
    show sirohebi st11 at xy(X=-160) #700
    with dissolve #701

    shirohebi "The monsters who rule over each area have all gathered together.{w}\nI govern Yamatai Village, and am near to attaining the seat of Queen Lamia."
    succubus "I govern the Succubus Village...{w}\nI'm not eyeing the seat of Queen Succubus."
    "Succubus and Shirohebi, too.{w}\nIt looks like every powerful monster is here..."

    hide succubus
    hide sirohebi
    show kraken st01
    with dissolve #700

    kraken "I am Queen of the South Seas.{w}\nA friendly meeting with monsters from other regions seemed interesting..."

    hide kraken
    show poseidones st01
    with dissolve #700

    poseidoness "The North Seas are my domain...{w}\nI tend to avoid collusion, but this seems a useful meeting to be a part of."

    hide poseidones
    show sphinx st01
    with dissolve #700

    sphinx "I am the master of the Sabasa region, Sphinx.{w}\nI was initially hesitant to come here, but I hope to find some answers..."
    "Kraken, Poseidoness and even Sphinx...{w}\nTruly all the leading monsters in the world."
    "And..."

    play music "audio/bgm/comi1.ogg"
    hide sphinx
    show pramia st11 at xy(Y=100)
    with dissolve #700

    tiny_lamia "I'm the representative of Iliasburg, Tiny Lamia.{w}\nI taste bad.{w} Please don't eat me."
    "...Tiny Lamia huddles herself into a ball with tears in her eyes.{w}\nHow did she get here...?"

    show pramia st11 at xy(X=-200, Y=100) #700
    show kraken st01 at xy(X=160) behind pramia
    with dissolve #701

    kraken "Oh... A monster representative of Iliasburg?{w} That's news to me..."

    hide kraken
    show sirohebi st11 at xy(X=160) behind pramia
    with dissolve #701

    shirohebi "Hehe, another lead Lamia?{w}\nDo you wish to compete against me for the seat of Queen Lamia?"

    hide sirohebi
    show poseidones st01 at xy(X=160) behind pramia
    with dissolve #701

    poseidoness "Sheesh, don't bully her."
    tiny_lamia "*Sniff*"
    "With the small, nervous Tiny Lamia in attendance, the meeting between the leading monsters starts..."

    scene bg 169 with blinds2
    play music "audio/bgm/maouzyou2.ogg"

    "After the meeting is over, the monsters leave the room one by one."

    show pramia st11 at xy(Y=100) with dissolve #700

    l "...Why did you slip in there?"
    tiny_lamia "I was following the old lady, but I got lost...{w}\nI just wandered in there while I was looking around..."
    "She's still terrified, even after the meeting broke up."
    l "She's looking for you, you should go back right away."

    show pramia st01 at center with dissolve #700

    tiny_lamia "Yeah, thank you..."
    tiny_lamia "That reminds me, I got three pretty jewels from Ms. Tamamo.{w}\nShe said they were filled with Spirit power, and to use them if we were in danger...{w} But what's going to happen?"
    l "I don't know what it will do...{w}\nBut if Tamamo gave it to you, I'm sure it will be useful."
    "Iliasburg will be under attack tomorrow, like everywhere else.{w}\nPerhaps Tamamo gave it to her as some sort of trump card?"
    tiny_lamia "Ok, then I'll keep good care of it.{w}\nSince there's three, I'll give one to Vampy and Puppy..."
    l "Do your best tomorrow!"
    tiny_lamia "Yes, we'll protect everyone in the town!{w}\nYou do your best too, Luka!"
    "Both of us leave, with renewed determination..."

    scene bg 149 with blinds
    return


label city45_11:
    scene bg 174 with blinds
    show granberia st62 with dissolve #700

    g "Finally, the decisive battle...{w} I'm itching to get going.{w}\nAfter spending so long in bed recovering, there's nothing I want to do more than rampage around."
    l "Yes...{w} But what's with those clothes?"

    show granberia st61 with dissolve #700

    g "Mmm?{w} They're just normal clothes.{w}\nDid you think I would even sleep in my armor?"
    l "............"
    "I sort of did..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_11}":
            jump city45_11a

        "Ask about Arc-En-Ciel{#city45_11}":
            jump city45_11b

        "What will you do during peacetime?{#city45_11}":
            jump city45_11c

        "Ask about the Dragon Seal{#city45_11}":
            jump city45_11d


label city45_11a:
    g "Naturally, I just wanted to test out how far my sword skills have come.{w}\nThe daughter of the Monster Lord, Alipheese...{w} What better opponent could there have been?"

    show granberia st62 with dissolve #700

    g "But she outmatched me.{w}\nI determined she was one worthy enough to entrust my sword to."
    l "...If you had won, would you have taken the Monster Lord's seat?"

    show granberia st61 with dissolve #700

    g "To be honest, I had no interest in the position itself.{w}\nI would have relinquished it right away, and continued training."

    menu:
        "Ask about Arc-En-Ciel{#city45_11a}":
            jump city45_11b

        "What will you do during peacetime?{#city45_11a}":
            jump city45_11c

        "Ask about the Dragon Seal{#city45_11a}":
            jump city45_11d

        "No more questions{#city45_11a}":
            jump city45_11e


label city45_11b:
    g "When we fought before, I was already wounded.{w}\nEven without that, I know she was stronger than me."
    g "But I wasn't just beaten into inaction for no reason...{w}\nI evaluated her fighting style, and took note of many things."
    g "I'll have a rematch with her.{w}\nAnd don't worry, I have no intention of losing..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_11b}":
            jump city45_11b

        "What will you do during peacetime?{#city45_11b}":
            jump city45_11c

        "Ask about the Dragon Seal{#city45_11b}":
            jump city45_11d

        "No more questions{#city45_11b}":
            jump city45_11e


label city45_11c:
    g "I haven't thought about anything past this impending battle.{w}\nBut... In a world of peace, what use is there for a Swordswoman?"
    g "I'll confront that issue when it comes.{w}\nBut don't worry, I don't intend to disturb the gained peace."

    menu:
        "About the Monster Lord's Throne fight...{#city45_11c}":
            jump city45_11a

        "Ask about Arc-En-Ciel{#city45_11c}":
            jump city45_11b

        "Ask about the Dragon Seal{#city45_11c}":
            jump city45_11d

        "No more questions{#city45_11c}":
            jump city45_11e


label city45_11d:
    l "That reminds me... Do you know what this is, Granberia?"
    "I show Granberia the mark on the back of my hand.{w}\nIt's hard to see normally, but under the right light it's visible."
    g "You...{w} That's...{w}\nDo you know what it means to show this to me?"
    l "Eh...?"
    g "Don't show this mark to someone of the Dragon race without thinking carefully.{w}\nYou'll cause quite a few... Misunderstandings..."
    l "......???"

    menu:
        "About the Monster Lord's Throne fight...{#city45_11d}":
            jump city45_11a

        "Ask about Arc-En-Ciel{#city45_11d}":
            jump city45_11b

        "What will you do during peacetime?{#city45_11d}":
            jump city45_11c

        "No more questions{#city45_11d}":
            jump city45_11e


label city45_11e:
    $ hanyo[0] = 1

    g "If you have no more questions, then I will be taking off now."
    l "That reminds me...{w} Are you going to your duel with Alma Elma?{w}\nCan I watch you two spar?"
    g "...No.{w} A true fight is no spectacle."
    l ".....?"
    "I see where she's coming from, but her response is still strange."
    g "If you're done here, then you should return to your room."

    play sound "audio/se/door2.ogg"

    scene bg 149 with blinds

    "I'm bluntly driven out of her room."
    "She's going to the duel with Alma Elma now...{w}\nI'm interested in just what sort of fight they would have together..."

    menu:
        "Watch in secret{#city45_11e}":
            jump city45_11h

        "Don't watch{#city45_11e}":
            jump city45_11i


label city45_11h:
    l "Should I watch them in secret?"
    "I really am just too interested in their duel.{w}\nMoving as quietly as I can, I head to the Practice Grounds..."

    play sound "audio/se/asioto3.ogg"
    scene bg 218 with blinds2
    play music "audio/bgm/kiki2.ogg"

    l "............."
    "Completely erasing any indication of my presence, I watch the two of them from a hiding spot in the Practice Grounds."

    show alma_elma st11 at xy(X=160)
    show granberia st01 at xy(X=-160) #700
    with dissolve #701

    alma "You never learn, do you, Granberry?{w}\nYou can never beat me..."

    show granberia st02 at xy(X=-160) with dissolve #700

    g "Quiet!{w} Here I come!"

    play sound "audio/se/karaburi.ogg"

    "Granberia swings down with a quick, powerful slash!{w}{nw}"

    play sound "audio/se/miss.ogg"

    extend "\nAlma Elma's face goes serious, and dodges by a hair's breadth."
    alma "Oh my, that was quite fast.{w}\nDid you think you'd win if you got in the first attack?"
    alma "But I was expecting that sort of strategy.{image=note}"

    show granberia st07 at xy(X=-160) with dissolve #700

    g "Gah!"
    "Seeing her chance, Alma Elma jumps closer to Granberia."
    "Even I could see that one coming...{w}\nAlma Elma taunted her into it, and Granberia attacked too quick as a result."

    show alma_elma st13 at xy(X=160) with dissolve #701


label city45_11j:
    if _in_replay:
        play music "audio/bgm/kiki2.ogg"
        scene bg 218
        show alma_elma st13 at xy(160, 0)
        show granberia st07 at xy(-160, 0)
        with Dissolve(1.5)

    alma "Caught you.{image=note}"
    "Alma Elma grabs hold of Granberia.{w}\nHer almost playful grapple is devoid of any bloodthirst, leaving a Serene State useless."
    g "Get off!"
    "Granberia struggles intensely...{w}\nBut suddenly her body jumps in surprise."

    play music "audio/bgm/ero1.ogg"

    g "...!{w} Haa...!"

    show alma_elma st13 at xy(X=160) with dissolve #701

    alma "And the match has been decided.{image=note}"
    "Alma Elma starts to caress Granberia's body, slipping her hands through her armor's opening."
    "The sweet, seductive caresses continue as Granberia struggles and rages to get Alma Elma off her."
    g "I...I won't... To this much...!"
    "Though she says that, her movements are slowing down.{w}\nIt's taking all of her concentration just to resist what she's feeling."
    alma "Haha... Granberry, you're really weak to pleasure attacks, aren't you?{image=note}"
    g "F...Fine...{w} I yield..."
    "Something Granberia would never say in a normal fight.{w}\nBut Alma Elma tramples on them in this situation."
    alma "Hehe... I don't accept.{w}\nI'll need to carve this defeat into your body..."
    "Alma Elma strips Granberia of her armor.{w}{nw}"

    hide alma_elma
    show granberia he1 at center
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nDeftly taking her own clothes off at the same time, she presses her large breasts against Granberia's."
    "And like that, starts to rub her own breasts against hers."
    g "S...Stop!{w} Get off me!"
    alma "Hehe... Isn't this what you wanted when you challenged me?{w}\nTo feel the joys of a woman, after so long?"
    alma "I was the one who first taught you them, after all.{image=note}"
    g "D...Don't fool around!{w}\nTo this rape, I would never..."
    "Granberia twists and squirms to escape, but is too firmly held against Alma Elma's body to go anywhere."
    alma "Then I'll give you a piece of paradise here...{w}\nHehe, it's going just as you hoped, isn't it?"
    g "Y...You're wrong!{w} Ah!"

    show granberia he2 with dissolve #700

    "Alma Elma reaches down to Granberia's vagina.{w}\nHer nimble finger slowly traces around her entrance."
    g "Guh..."
    "Granberia's body trembles as she holds back her own voice.{w}\nIt's all she can do to endure the finger tracing around her vagina..."
    alma "Look at how wet you're getting...{w} Such an obscene body you have.{w}\nSee... You wanted to do this, didn't you?"
    g "Wh...Who would... Nnn!"
    "When Granberia opens her mouth, her refusal is interrupted by a sweet moan.{w}\nShe quickly clamps her mouth shut again, going back to trying to endure what she's feeling."
    alma "Hehe... Do you want me to put my finger in?{w}\nThis finger that took Granberry's virginity?"
    g "S...Stop... Don't do that!"
    alma "It has been a while, so I'm going to make you come over and over.{w}\nNow, I'll rape you with my finger...{image=note}"

    play hseanwave "audio/se/hsean03_innerworks_a3.ogg"
    show granberia he3 with dissolve #700

    "Alma Elma's finger slowly circles around her vagina, getting closer to her entrance...{w}\nArriving at her slit, she slowly pushes her finger inside."
    g "...!"
    "Granberia's body jumps as she holds her voice back.{w}\nAlma Elma continues slowly moving her finger, giggling."
    alma "Hehe... You're getting tighter and tighter Granberry.{w}\nThis tightness would make a man come in seconds, you know?"
    g "Tch...!"
    alma "Are you so ashamed of your own moans that much?{w}\nYou always reach orgasm while biting it back, after all.{image=note}"
    alma "...Here, I'll make you come with my finger now.{w}\nBy using this finger you love so much, right inside here..."
    g "~~~~~~~~~~!!!"
    "Granberia's body suddenly spasms."
    "Her eyes go wide, but she still holds her voice back.{w}\nAlma Elma used her finger to force her to come in seconds..."

    stop hseanwave fadeout 1

    alma "And off you go to paradise...{w}\nDid it feel good, Granberry?{image=note}"
    alma "Hehe... I win again.{w}\nThis makes forty three wins in a row.{image=note}"
    "Alma Elma licks her finger, covered in Granberia's wetness."
    g "............"
    "Granberia's eyes are unfocused, as her breathing gets rougher.{w}\nShe seems to be immersed in the afterglow of her orgasm..."
    l "Wow..."
    "I just saw something incredible..."

    $ renpy.end_replay()
    $ persistent.hsean_shitenno5 = 1
    play music "audio/bgm/maouzyou2.ogg"
    scene bg 149 with blinds
    return


label city45_11i:
    l "It wouldn't be good to watch in secret."
    "I'm interested in their duel, but she seemed pretty clear about it.{w}\nI better not watch."

    return


label city45_11k:
    l "Granberia, are you there?"

    play sound "audio/se/knock.ogg"

    g "................"
    "There's no reply from inside."

    scene bg 149 with blinds
    return


label city45_12:
    scene bg 175 with blinds
    show alma_elma st11 with dissolve #700

    alma "With tomorrow being the final battle, I'm feeling a little excited.{w}\nLet's both do our best, Luka.{image=note}"
    "She seems awfully happy and excited before the big fight...{w}\nI wonder if she isn't nervous at all?"

    menu:
        "About the Monster Lord's Throne fight...{#city45_12}":
            jump city45_12a

        "Ask about Hainuwele{#city45_12}":
            jump city45_12b

        "What will you do during peacetime?{#city45_12}":
            jump city45_12c

        "Ask about how she fights{#city45_12}":
            jump city45_12d


label city45_12a:
    show alma_elma st13 with dissolve #700

    alma "Because it seemed fun, of course.{w}\nThe fight of the century that every monster pays close attention to...{w} It gives me shivers just thinking about it."
    "It seems like she wasn't interested in the position itself...{w}\nJust like in the Colosseum, it seems she just likes fighting before a crowd."
    l "But I heard that you just surrendered when you could have kept fighting..."
    alma "I had enough fun, so why keep going after that?{w}\nIt would have been ugly to be seen desperately fighting, after all...{image=note}"
    "Vague answers as always..."

    show alma_elma st11 with dissolve #700

    menu:
        "Ask about Hainuwele{#city45_12a}":
            jump city45_12b

        "What will you do during peacetime?{#city45_12a}":
            jump city45_12c

        "Ask about how she fights{#city45_12a}":
            jump city45_12d

        "No more questions{#city45_12a}":
            jump city45_12e


label city45_12b:
    alma "When we fought before, I was already wounded...{w}\nI won't lose to someone that can only move that fast."
    l "But she was created just to fight against you.{w}\nI'm sure she has weapons other than just her speed..."

    show alma_elma st13 with dissolve #700

    a "Hehe, are you worried?{w}\nDon't worry, I have lots of unexpected weapons of my own.{image=note}"

    show alma_elma st11 with dissolve #700

    menu:
        "About the Monster Lord's Throne fight...{#city45_12b}":
            jump city45_12a

        "What will you do during peacetime?{#city45_12b}":
            jump city45_12c

        "Ask about how she fights{#city45_12b}":
            jump city45_12d

        "No more questions{#city45_12b}":
            jump city45_12e


label city45_12c:
    alma "Even when it's peaceful...{w} I won't change.{w}\nI'll just live carefree, like the wind."
    alma "Of course, I won't hurt humans or be careless.{w}\nI promise that to you, Luka-boy.{image=note}"

    menu:
        "About the Monster Lord's Throne fight...{#city45_12c}":
            jump city45_12a

        "Ask about Hainuwele{#city45_12c}":
            jump city45_12b

        "Ask about how she fights{#city45_12c}":
            jump city45_12d

        "No more questions{#city45_12c}":
            jump city45_12e


label city45_12d:
    l "Alma, how do you use the power of wind?"
    alma "Ah... Well from the start, Succubi and the power of wind have good compatibility.{image=note}"
    l "But that doesn't mean you're using it right...{w}\nIf you're rough with it, you can't use proper techniques."
    l "You can't just have talent or unfocused training to master it...{w}\nRather than having the proper power... I guess you need the proper frame of mind?"
    alma "Oh my... Luka is so mean."
    l "Isn't it the same for martial arts?{w}\nWithout proper technique, it's just fancy names for punching and kicking."
    l "If you were more serious, you'd be amazing..."
    alma "But...{w} In the beginning, even martial arts were created just for show.{w}\nIf the audience enjoys it, that's enough for me.{image=note}"
    l "It seems like sort of a waste...{w}\nYou'd be so much stronger if you were serious with it."
    alma "Unlike Granberry, I don't train diligently.{w}\nI like things that feel good more than violence...{image=note}"
    l "............."
    "Vague answers as always..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_12d}":
            jump city45_12a

        "Ask about Hainuwele{#city45_12d}":
            jump city45_12b

        "What will you do during peacetime?{#city45_12d}":
            jump city45_12c

        "No more questions{#city45_12d}":
            jump city45_12e


label city45_12e:
    show alma_elma st13 with dissolve #700

    alma "That's it for story time?{w}\nOr did you want to play together a little...?"
    l "T...That's alright..."

    play sound "audio/se/door2.ogg"
    hide alma_elma with dissolve

    "With that, I leave Alma's room..."

    scene bg 149 with blinds
    return


label city45_13:
    scene bg 176 with blinds
    play music "audio/bgm/tamamo.ogg"
    show tamamo st03 with dissolve #700

    t "Busy busy...{w}\nThere's so much to do before tomorrow's battle, I'm very busy..."
    "Tucked away in a corner of the Monster Lord's Castle is a room that reminds me of Yamatai Village."
    "Tamamo is busy writing something as I walk in.{w} Her eyes flick up to me, a serious look on her face."

    play music "audio/bgm/maouzyou2.ogg"
    show tamamo st01 with dissolve #700

    t "But... The Monster Lord truly is holding the same dreams as her mother in her."
    t "...It appears that her mother's values were instilled completely in her.{w}\nEven the one about sacrificing herself to promote peace with humans..."
    t "Of course, the dangerous alternative was a path of hatred against humans who killed her mother..."
    l "Yes, I know..."
    "Alice hating humans for harming her mother...{w}\nIt was what Tamamo wanted to avoid most in raising her."
    t "And her mother's actions were a result of my flawed training and direction.{w}\nI still hold a deep feeling of guilt for not stopping her..."
    t "To think she'd yield herself to that simplistic decision..."
    l ".............."

    show tamamo st04 with dissolve #700

    t "...And I'm grateful for you.{w}\nIf it wasn't for you, that disaster would have been repeated."
    t "I'm entrusting the Monster Lord to you in the future too, Luka.{w}\nBy the way, did you have something you wanted to ask me?"

    menu:
        "About the Monster Lord's Throne fight...{#city45_13}":
            jump city45_13a

        "Ask about Tsukuyomi{#city45_13}":
            jump city45_13b

        "What will you do during peacetime?{#city45_13}":
            jump city45_13c

        "Ask about Angel Halo{#city45_13}":
            jump city45_13d


label city45_13a:
    show tamamo st01 with dissolve #700

    t "...As you know, the Monster Lord selection was traditionally a one on one fight.{w}\nThat was the first time more than one competed for the title."
    t "Even more, three of them were known, powerful monsters.{w}\nAlice would never have lost one against one, to any of them."
    t "But if the three colluded together, they could have attacked her three against one.{w}\nIf that happened, it would have been difficult for her to win."
    "That's true...{w}\nIf Granberia, Alma Elma and Erubetie all attacked Alice at once, I don't think even she could have won."
    t "So... I participated to prevent that.{w}\nIf they tried to concentrate their attacks against her, I would stop them."
    t "But in the end, none tried any sort of cowardly behavior.{w}\nIt was a very refreshing battle, where each used their own powers freely to their utmost."

    menu:
        "Ask about Tsukuyomi{#city45_13a}":
            jump city45_13b

        "What will you do during peacetime?{#city45_13a}":
            jump city45_13c

        "Ask about Angel Halo{#city45_13a}":
            jump city45_13d

        "No more questions{#city45_13a}":
            jump city45_13e


label city45_13b:
    show tamamo st03 with dissolve #700

    t "I haven't fought her yet, but apparently she was created with my cells.{w}\nShe will be an awkward opponent, if she really can use Kitsune skills..."

    show tamamo st04 with dissolve #700

    t "Though with that said, I can't lose.{w}\nWhy?{w} Because my tail is fluffier than hers!"
    l "..........."
    "...Is she really going to be alright...?"

    menu:
        "About the Monster Lord's Throne fight...{#city45_13b}":
            jump city45_13a

        "What will you do during peacetime?{#city45_13b}":
            jump city45_13c

        "Ask about Angel Halo{#city45_13b}":
            jump city45_13d

        "No more questions{#city45_13b}":
            jump city45_13e


label city45_13c:
    show tamamo st01 with dissolve #700

    t "Even during peace, I won't change what I'm doing.{w}\nI'll just keep training and nagging each generation of Monster Lord."
    t "The current Monster Lord, the next, and the next... Forever."
    t "More importantly, what will you do?"
    l "I... Haven't thought about it."
    t "...You haven't thought about it?{w}\nOr are you unable to imagine yourself in a peaceful world?"
    l "........."
    t "Don't be seized by the temptation of self sacrifice.{w}\nThat is the easy way out..."
    t "As someone who stopped Alice from doing just that, you already know it well.{w}\nPlease, value your own body and life..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_13c}":
            jump city45_13a

        "Ask about Tsukuyomi{#city45_13c}":
            jump city45_13b

        "Ask about Angel Halo{#city45_13c}":
            jump city45_13d

        "No more questions{#city45_13c}":
            jump city45_13e


label city45_13d:
    show tamamo st01 with dissolve #700

    t "Alice took that sword with her when she left the Castle.{w}\nIn order to challenge Ilias, a weapon like that was necessary, after all."
    t "The history of that blade is complex...{w} But originally, it was created to be used by Fallen Angels."
    l "Fallen Angels?"
    t "A long time ago, there was a Hero who was unbaptized.{w}\nHe was given a sword from a Fallen Angel, and set out for adventure."
    t "He defeated the Monster Lord, but became aware of the true evil...{w}\nThat was of course, the Goddess Ilias."
    t "That Hero's second battle then began.{w}\nHe cut apart angel after angel with that sword the Fallen Angel granted him."
    t "Slicing through angel after angel, both the sword and his own body were covered in pure holy energy."
    t "After killing more than two hundred, he started to go insane."
    t "Engrossed in slaughter, he became nothing more than an instrument of death.{w}\nKnown as the \"Angel Killer\", he was feared in the Heavens."
    t "In the end, his body itself was taken into the sword.{w}\nIlias separated his soul from it, and sealed it away."
    t "That Hero's name was...{w} Well, you already know, don't you?"
    l "Yes..."
    "The legendary Hero, Heinrich Hein...{w}\nI've been fighting together with him, this whole time."

    menu:
        "About the Monster Lord's Throne fight...{#city45_13d}":
            jump city45_13a

        "Ask about Tsukuyomi{#city45_13d}":
            jump city45_13b

        "What will you do during peacetime?{#city45_13d}":
            jump city45_13c

        "No more questions{#city45_13d}":
            jump city45_13e


label city45_13e:
    show tamamo st03 with dissolve #700

    t "Sorry, but I'm fairly busy at the moment.{w}\nI have to attend meetings all over the place..."

    hide tamamo with dissolve
    play sound "audio/se/door2.ogg"

    "I don't have anything to ask her at the moment, so I excuse myself from her room."

    scene bg 149 with blinds
    return


label city45_14:
    scene bg 177 with blinds
    show erubetie st01 with dissolve #700

    e "..........."
    e "...Did you need me for something?"
    e "..........."
    "It doesn't look like she's going to avoid me...{w}\nThe atmosphere here is somewhat strange..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_14}":
            jump city45_14a

        "Ask about Amphisbaena{#city45_14}":
            jump city45_14b

        "What will you do during peacetime?{#city45_14}":
            jump city45_14c

        "Ask about her room{#city45_14}":
            jump city45_14d


label city45_14a:
    e "To rescue my brethren, who were being pushed to the edge.{w}\nI intended to rule over humans using the power of a Monster Lord."
    e "Of course, I know now that my path was wrong...{w}\nIf I had called for an assault against the humans, this crisis would have been far worse."

    menu:
        "Ask about Amphisbaena{#city45_14a}":
            jump city45_14b

        "What will you do during peacetime?{#city45_14a}":
            jump city45_14c

        "Ask about her room{#city45_14a}":
            jump city45_14d

        "No more questions{#city45_14a}":
            jump city45_14e


label city45_14b:
    e "I was stuck.{w}\nNone of my attacks could connect with her."
    e "But I did notice something in our fight...{w}\nI think that maybe..."
    l "Is there a chance of winning...?"
    e "............"
    "Erubetie falls silent.{w}\nShe seems to have something in mind, though..."

    menu:
        "About the Monster Lord's Throne fight...{#city45_14b}":
            jump city45_14a

        "What will you do during peacetime?{#city45_14b}":
            jump city45_14c

        "Ask about her room{#city45_14b}":
            jump city45_14d

        "No more questions{#city45_14b}":
            jump city45_14e


label city45_14c:
    e "First, I'll need to counteract the poison in Undine's Spring.{w}\nEven if I gather together all of my brethren, I don't know how long it will take."
    e "In addition, we must push forward peaceful relations with humans...{w}\nWill you assist me at that time...?"
    l "Yes, of course!"
    e "............"
    "Erubetie silently stares at me.{w}\nDid I give her the wrong answer...?"

    menu:
        "About the Monster Lord's Throne fight...{#city45_14c}":
            jump city45_14a

        "Ask about Amphisbaena{#city45_14c}":
            jump city45_14b

        "Ask about her room{#city45_14c}":
            jump city45_14d

        "No more questions{#city45_14c}":
            jump city45_14e


label city45_14d:
    l "By the way, Erubetie...{w} What's up with this room?"
    "...Actually, is this even a room?{w}\nIt seems more like a waterway..."
    e "This room is comfortable...{w}\nThree times a day, a strong current comes through, and refills it."
    e "When I lay in it during those times, my heart is eased...{w}\nDo you want to try it with me, Luka?"
    l "A...Ah... Sure, maybe next time..."
    "Isn't this just the drainage for the Castle...?"

    menu:
        "About the Monster Lord's Throne fight...{#city45_14d}":
            jump city45_14a

        "Ask about Amphisbaena{#city45_14d}":
            jump city45_14b

        "What will you do during peacetime?{#city45_14d}":
            jump city45_14c

        "No more questions{#city45_14d}":
            jump city45_14e


label city45_14e:
    e "You don't need anything else...?"
    l "Ah... Yes, that was all..."
    e "............."
    l "S...See you later..."
    "I don't quite get it, but there's a strange atmosphere between us.{w}{nw}"

    play sound "audio/se/door2.ogg"

    extend "\nI quickly leave Erubetie's strange room."

    scene bg 149 with blinds
    return


label city45_15:
    scene bg 141 with blinds
    show alice st71 at xy(Y=50) with dissolve #700

    a "Ah... Luka?{w} I'm very busy at the moment.{w}\nI'm stuck writing handwritten letters about the decisive battle coming up to everyone."
    a "Sorry, but I can't keep you company at the moment.{w}\nI'll head to your room later, so please wait a while..."
    l "She'll head to my room... Eh...?"
    "I have a bad feeling..."

    scene bg 149 with blinds
    return



label city51_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 003 with blinds2
    play music "audio/bgm/mura1.ogg"


label city51_main:
    call city_null 
    $ city_bg = "bg 003"
    $ city_button01 = "Man A" #601
    $ city_button02 = "Man B" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Girl" #604
    $ city_button05 = "Elder" #605
    $ city_button06 = "Soldier" #606
    $ city_button07 = "Former Temple Soldier A" #607
    $ city_button08 = "Former Temple Soldier B" #608
    $ city_button10 = "Betty" #610
    $ city_button11 = "Weapon Shop" #611
    $ city_button12 = "Tool Shop" #612
    $ city_button13 = "Ilias's Temple" #613
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city51_01 
        elif result == 2:
            call city51_02 
        elif result == 3:
            call city51_03 
        elif result == 4:
            call city51_04 
        elif result == 5:
            call city51_05 
        elif result == 6:
            call city51_06 
        elif result == 7:
            call city51_07 
        elif result == 8:
            call city51_08 
        elif result == 10:
            call city51_10 
        elif result == 11:
            call city51_11 
        elif result == 12:
            call city51_12 
        elif result == 13:
            call city51_13 
        elif result == 19:
            return


label city51_01:
    show mob seinen1 with dissolve #700

    man_a "You did it, Luka!{w}\nNot only did you kill that Monster Lord, you brought down Ilias too!"
    man_a "...By the way, where did that girl from before go?"

    show alice st01b at xy(X=-120) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "............"
    man_a "Woah, you got huge!{w} You sure grew up fast!{w}\nHere, have a cookie."

    show alice st02b at xy(X=-120) with dissolve #700

    a "Naturally, I'll gladly eat it."
    l ".........."

    return


label city51_02:
    show mob seinen1 with dissolve #700

    man_b "Dang!{w} So Luka came back with a beautiful Monster Lord wife!{w}\nI never thought you'd end up hitched to someone so hot, man!"
    l "W...We aren't!"

    hide mob
    show alice st07b
    with dissolve #700

    a "W...Who's spreading those rumors!?"

    return


label city51_03:
    show mob ozisan1 with dissolve #700

    old_man "I never doubted for a moment!{w}\nI believed in you since I first set eyes on you!"
    old_man "By the way...{w} Could you sign this?{w}\nThe bar hostess Risa wants it..."

    return


label city51_04:
    show mob syouzyo with dissolve #700

    girl "Yaaay!{w} Lukey's back!{w}\nHe went and forced the Monster Lord to be his bride!{w} Now he's back!"
    l "Wait, that's not right at all!"

    hide mob
    show alice st07b
    with dissolve #700

    a "What's the meaning of this!?"

    return


label city51_05:
    show mob rouzin2 with dissolve #700

    elder "Today's youth is pathetic.{w}\nIlias this. Ilias that.{w} They all just got damn Ilias on their minds."
    elder "But I always believed in you, Luka.{w}\nI knew you'd keep the faith, and bring her down!"

    return


label city51_06:
    show mob sensi1 with dissolve #700

    soldier "Oi, Luka!{w} You did it!{w}\nNot even the Monster Lord, you defeated the Goddess!{w} And saved the world!"
    soldier "And I'll really go down in history as the soldier who saw you off...{w}\nIt is truly an honor."

    return


label city51_07:
    show mob heisi1 with dissolve #700

    former_temple_soldier_a "Thanks to the downfall of Ilias, nobody is passing through to her temple."
    former_temple_soldier_a "But tourism has exploded due to people wanting to see the Hero's hometown!{w}\nYou're the true pride of Ilias Village, Luka!"

    return


label city51_08:
    show mob mamono1 at xy(X=280, Y=-50) as mob2
    show mob heisi1 at xy(X=-170) #700
    with dissolve #701

    l "Eh... You're..."
    "I'm sure I saw these two in Enrika.{w}\nHe was one of the injured ones, refusing the Elf's help."
    "But now it looks like their relationship is totally opposite..."
    former_temple_soldier_b "Oh, I'm so embarassed...{w}\nEver since then, we've sort of... Gotten involved."
    elf "Our wedding is next week.{w}\nI'll be a part of this village, too!"

    return


label city51_10:
    scene bg 038 with blinds
    show mob obasan with dissolve #700

    betty "Ooh, you came back safely!{w}\nThat's all I ever wanted."
    betty "I felt so worried after our last goodbye...{w}\nI never stopped wishing you luck, hoping to meet again."
    l "R...Really...?{w}\nWell, more importantly, the village's reconstruction is really moving along."
    l "I heard that the Kitsune race is helping out."
    betty "Ah, that's right.{w}\nAll of those foxes came and helped to rebuild the village."
    betty "They did it to thank you, it seems."
    "They rebuilt my hometown while I was in bed...{w}\nI really need to thank Tamamo."
    betty "...What are you doing the rest of today?{w}\nWe were planning on holding a huge festival for when you came back..."
    l "Sorry, but I'm not coming back just yet...{w}\nRight now, I'm taking a look around the world."
    betty "That's right, the Hero who saved the world has to show his face to everyone!"
    l "Yeah...{w} So, I'll be heading off now!"
    betty "It was good to see you both!{w}\nBy the way, when is the wedding?"
    l "W...What!?"

    hide mob
    show alice st07b
    with dissolve #700

    a "Where did this rumor come from!?"

    scene bg 003 with blinds
    return


label city51_11:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    smith "The sword I made has been by your side all through your quest...{w}\nI know you barely used it, but it still makes me proud."
    smith "I've gotten thousands of orders from all over the world.{w}\nThey're all addressed to the \"Smith who made Hero Luka's sword\"..."

    scene bg 003 with blinds
    return


label city51_12:
    scene bg 151 with blinds

    l "........."
    "Ronnie fell during the angel's attack.{w}\nIt's quiet here...{w} I spend a moment of silence in his honor."

    scene bg 003 with blinds
    return


label city51_13:
    scene bg 007 with blinds
    show mob sinkan with dissolve #700

    priest "Now that Ilias is gone, the temple has been deserted.{w}\nThough, it does still hold historical value."
    priest "And just like the temples, us priests still have a role to play."
    priest "We shall not preach the word of the Goddess any longer, but as from ancient times, our role of guiding those who were lost will not change."

    scene bg 003 with blinds
    return


label city52_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 018 with blinds2
    play music "audio/bgm/city1.ogg"


label city52_main:
    call city_null 
    $ city_bg = "bg 018"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Girl" #602
    $ city_button03 = "Prostitute" #603
    $ city_button04 = "Man" #604
    $ city_button05 = "Old Woman" #605
    $ city_button06 = "Soldier A" #606
    $ city_button07 = "Soldier B" #607
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Sutherland Inn" #613
    $ city_button14 = "Sutherland Inn's Back Door" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city52_01 
        elif result == 2:
            call city52_02 
        elif result == 3:
            call city52_03 
        elif result == 4:
            call city52_04 
        elif result == 5:
            call city52_05 
        elif result == 6:
            call city52_06 
        elif result == 7:
            call city52_07 
        elif result == 10:
            call city52_10 
        elif result == 11:
            call city52_11 
        elif result == 12:
            call city52_12 
        elif result == 13:
            call city52_13 
        elif result == 14:
            call city52_14 
        elif result == 19:
            return


label city52_01:
    show mob seinen1 with dissolve #700

    youth "Awesome!{w} It's the Hero!{w}\nI got so excited when I saw you, I let loose in my pants!"
    youth "You know, I still haven't changed my pants from last time this happened, too.{w}\nThey dry quick, so it's alright."

    return


label city52_02:
    show mob musume1 with dissolve #700

    girl "The little monsters did a lot during the final assault.{w}\nThey suddenly grew up, and were really strong."
    girl "After the fight was over, they all were in bed recovering for a week!{w}\nI guess it was a huge burden on their bodies..."
    girl "Of course, they're running around healthy now.{w}\nPlease let me know where you find them!"

    return


label city52_03:
    show mob musume2 with dissolve #700

    prostitute "You're incredible, Hero!{w}\nJust for you, I'll give some extra big services. No charge.{image=note}"

    show alice st01b at xy(X=-120) #700
    show mob musume2 at xy(X=335)
    with dissolve #701

    a "..........."
    prostitute "Oh, excuse me...{w} The Hero already has his beautiful Monster Lord..."
    l "What the heck!?{w} Why is this already all over the world!?"

    return


label city52_04:
    show mob seinen2 with dissolve #700

    man "I was doing my best during the final fight, too!{w}\n...I was actually the one supplying all the arrows."
    man "When the angels finally surrendered, I was crying tears of joy.{w}\nI really worked hard and did something back then!"
    man "...But now that we're back at peace, I feel myself getting lazy again..."

    return


label city52_05:
    show mob rouba with dissolve #700

    old_woman "This time, those bragging Heroes actually helped out.{w}\nThey withered under the glares of the citizens for so long after they stood by during Granberia's assault..."
    old_woman "This time the Heroes, the citizens, and even the littles all came together to defend the town."

    return


label city52_06:
    show mob sensi1 with dissolve #700

    soldier_a "I think it's time to quit being a soldier.{w}\nI'm tired of living in fear..."

    return


label city52_07:
    show mob sensi2 with dissolve #700

    soldier_b "We did our best during the final battle, unlike when Granberia came...{w}\nI think this will be enough to stop the citizens from glaring at us!{w} Probably..."

    return


label city52_10:
    scene bg 152 with blinds
    show dragonp st13 at xy(X=160, Y=100)
    show mob ozisan1 at xy(X=-160) #700
    with dissolve #701

    shopkeeper "Thank you two for returning peace to the world!{w}\nPup here is happy as can be, too!"
    dragon_pup "Uga!{w} I'm working hard!{w}\nI'll defeat all the enemies!"
    l "Yes, you did great."
    dragon_pup "I'll keep training as a blacksmith!{w}\nMy dream for the future is to become a legendary blacksmith!"

    hide mob
    show alice st02b at xy(X=-120) #700
    show dragonp st13 at xy(X=160, Y=100)
    with dissolve #701

    a "Yes, Dragons have had a few legendary blacksmiths.{w}\nI'm sure you'll become a great one too, better than any human could be."

    hide dragonp
    show mob ozisan1 at xy(X=160, Y=100) behind alice
    with dissolve #701

    shopkeeper "Eh?{w} Seriously?"
    l "............."

    hide mob
    hide alice
    show dragonp st13 at xy(Y=100)
    with dissolve #700

    l "...By the way, what happened to Goblin Girl?{w}\nIs she still running around the world?"
    dragon_pup "Gob's still working as a mover, carrying things all over the world.{w}\nSo she hasn't settled down in a town yet... It seems kind of lonely to me."
    dragon_pup "...Oh, the three of us also wanted to give you a present.{w}\nCan you come behind the Sutherland Inn later so we can give it to you?"
    "A present?{w}\nI wonder what the three of them want to give me...?"

    $ check08 = 1
    scene bg 018 with blinds
    return


label city52_11:
    scene bg 151 with blinds
    show vgirl st13 at xy(X=160, Y=100)
    show mob seinen1 at xy(X=-160) #700
    with dissolve #701

    shopkeeper "Oh! If it isn't the Hero who saved the world!{w}\nThanks to you, this town is peaceful."
    shopkeeper "Vampire Girl did her best, too!{w}\nShe's already up so early, giving it her best!"
    vgirl "Hehehe... It's all in aiming to have my own store one day!"
    l "I see...{w} I'll be sure to shop there once you open it."

    hide mob
    show alice st02b at xy(X=-120) #700
    show vgirl st13 at xy(X=160, Y=100)
    with dissolve #701

    a "I'll go if you sell something delicious."

    hide alice
    show vgirl st13 at xy(Y=100)
    with dissolve #700

    vgirl "Oh that reminds me...{w} The three of us wanted to give you a present.{w}\nWhen you have time later, come to the back entrance of the Sutherland Inn."
    "A present?{w}\nI wonder what the three of them want to give me...?"

    $ check08 = 1
    scene bg 018 with blinds
    return


label city52_12:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Thank you for coming...{w} To the Sacred Bar!{w}\nNow, my child...{w} What would you like to order!?"
    l "A...Apple juice..."
    "It looks like the Priest turned his temple into a bar..."

    scene bg 018 with blinds
    return


label city52_13:
    scene bg 016 with blinds
    show pramia st13 at xy(X=160, Y=100)
    show mob obasan at xy(X=-160) #700
    with dissolve #701

    owner "Oh! If it isn't the Hero who saved the world!{w}\nI didn't think you were a normal guy from the first time I laid eyes on you...{w}\nAnd it seems like I was spot on!"
    owner "You two have a permanent free pass here.{w} Stay for free whenever you want!"
    tiny_lamia "I've learned how to make Amamadangos too!{w}\nI'll treat you both to as much as you want next time!"

    hide mob
    show alice st01b at xy(X=-120) #700
    show pramia st13 at xy(X=160, Y=100)
    with dissolve #701

    a "Hehe... I'm looking forward to it."

    hide alice
    show pramia st13 at xy(Y=100)
    with dissolve #700

    tiny_lamia "Oh, the three of us also got you a present.{w}\nCan you come to the back entrance here later so we can give it to you?"
    "A present?{w}\nI wonder what the three of them want to give me...?"

    $ check08 = 1
    scene bg 018 with blinds
    return


label city52_14:
    if cont == 1:
        play music "audio/bgm/city1.ogg"

    $ cont = 0
    scene bg 178 with blinds

    if check08 == 0:
        call city_noiv 
        scene bg 018 with blinds
        return

    l "I wonder what the present is?"
    "Rather than feeling pleased at the thought, for some reason I have a bad feeling...{w}\nAs I approach the back of the inn, I hear some whispers."
    dragon_pup "Uga... Will it really work?"
    vgirl "I'll put all my magic power into it...{w}\nIf that doesn't work, we'll do it by brute strength..."
    tiny_lamia "We can't do it by force!"
    "What are they talking about...?"
    l "Uhm...{w} I came as you asked..."

    show vgirl st01 with dissolve #700

    vgirl "Alright, a chance!"
    "Vampire Girl jumps out, aiming for my upper body!"

    menu:
        "Don't Resist{#city52_14}":
            jump city52_14a

        "Resist{#city52_14}":
            jump city52_14b


label city52_14a:
    if _in_replay:
        play music "audio/bgm/city1.ogg"
        show bg 178 with Dissolve(1.5)

    show vgirl st02 with dissolve #700

    vgirl "Nnn... *Kiss*"

    play sound "audio/se/ero_chupa2.ogg"

    "Vampire Girl clings to my body, and kisses me softly on the lips."
    l "Nnn..."
    "A sweet sensation runs through my body, as I go limp in her kiss.{w}\nHer soft lips press harder against me, her small tongue eagerly rubbing against mine."
    l "Haa... Ahh..."

    hide vgirl
    show dragonp st01 at xy(X=-180) #700
    show pramia st01 at xy(X=180)
    with dissolve #701

    dragon_pup "Uga!{w} It worked!"
    tiny_lamia "Alright, bring him inside!"
    "The three of them lift me up and carry me off, my body momentarily too weak to resist."

    scene bg 179 with blinds2

    "They bring me to a barn just outside of town.{w}\nToo weak to stop them, the three little monsters kidnapped me..."
    "Then..."

    play music "audio/bgm/ero1.ogg"
    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    show end1 h1 with dissolve #700

    l "Ahh..."
    "Since then, the little monster girls have been using me for their amusement.{w}\nKept in the barn, they rape me on a daily basis."
    vgirl "Hehe, how do you like my waist movements?{w}\nTake this, and this.{image=note}"
    "Sitting astride me, Vampire Girl's tight vagina is clenching down on my penis.{w}\nAt the same time, she's grinding her small body back and forth against me."
    l "Uwa!"
    "My body jerks up in intense pleasure at the movements.{w}\nThe other two girls hold me from both sides, keeping me still as she rides me."
    dragon_pup "*Lick* *Lick*"
    tiny_lamia "*Lick*"
    "Their tongues reach out and play with my nipples, filling my upper body with the ticklish pleasure."
    l "Stop... Ahhh!"
    "The relentless tonguing on my nipples brings me to the very limit.{w}\nAt the same time, my hard penis is stuck deep inside Vampire Girl's tiny vagina."
    vgirl "Hehehe... I'll try a new skill today.{w}\nI'll squeeze out your semen with Energy Drain!"
    l "W...Wait... Ahh!"
    "Vampire Girl's vagina swells up with magic power, causing her temperature to sharply rise.{w}\nAt the same time, her vaginal walls tense up and stick to my penis."
    "Infused with magic power, her soft flesh starts to pull on my penis, as if she was sucking on me with her vagina.{w}\nMy body slackens, as the suction feels like I'm already ejaculating before even an orgasm."
    l "Haaa... Don't suck...{w}\nI'm going to come...!"
    vgirl "Hehe... It looks like the Hero is at his limit.{w}\nThe Hero who saved the world is now our own personal toy... Hehe."
    "Far from weakening it, she starts to put even more effort into the suction.{w}\nInfusing even more magic into her vagina, Vampire Girl's face flushes as her small vagina squeezes down harder on me."

    show end1 h2 with dissolve #700

    vgirl "Nnn... This is the finish...{w}\nBe finished inside me!"
    l "Haaa!!!"
    "As the suction pulls at me with unnatural ecstasy, I stop trying to resist her.{w}\nGiving myself over to the feelings, I let out a sigh as I reach a climax."

    call syasei1 
    show end1 h3 #700
    call syasei2 

    "I explode inside the small Vampire's vagina.{w}\nAs if my energy and soul itself is being sucked out, I feel myself becoming a captive to this pleasure..."
    l "Ahhh..."
    "As the flood of ecstasy fills my body, the sense of defeat is complete.{w}\nEven though these girls are so much weaker, I can't resist them."
    "I can only shiver in pleasure as Vampire Girl rides on top of me."

    show end1 h4 with dissolve #700

    vgirl "Hehehe... You let out so much!{w}\nDoes my Energy Drain feel that good?"
    vgirl "Do you want me to draw out even more?{w}\nI'll give you some extra special service today..."
    l "Ah!{w} Hya!"
    "Right after my ejaculation, her vagina is already sucking on my penis again.{w}\nWithout giving me a break, Vampire Girl already restarts her Energy Drain..."
    "More energy is drawn out of my sensitive penis...{w}\nI moan as the humiliation and ecstasy mix together inside my head."
    tiny_lamia "*Lick*{w} ...Hey, that's no fair!{w} You don't get two turns in a row!"
    dragon_pup "*Lick*{w}\nIt was my turn next..."
    "The other two girls mutter as they continue to lick my nipples.{w}\nTheir wet touch makes the rest of my body extra sensitive..."
    vgirl "Hehehe... Sorry.{w}\nLuka's penis just wants me to suck on it so bad.{image=note}"
    l "Ahhh..."
    "As my energy is sucked up into her vagina, the desire to release again slowly wells up.{w}\nIf I come now, she'll just suck it up as more food for her..."
    l "Ahh... Get off...{w}\nI'm going to come again..."
    vgirl "Hehehe... I won't let you go.{w}\nGive me more of your fresh semen!"
    "Grinding against me, her tiny vagina squeezes down even harder on me as she moves to finish me off.{w}\nAs my head goes blank in pleasure, I relax my body and soak in the climax."

    call syasei1 
    show end1 h5 #700
    call syasei2 

    l "Haa..."
    "With a soft moan, I explode inside her small body again.{w}\nUnable to resist her at all, she easily brought me to another orgasm..."
    vgirl "Hehe, you leaked out again.{w}\nI'll suck it all up..."
    "She shrinks and contracts her small vagina, creating a suction.{w}\nMy semen is slowly pulled deeper inside her small body, as if she truly was drinking it all up."
    "Those small contractions themselves stimulate me even further, brining forth more spasms of pleasure."
    l "Ah... Ahh..."
    "I sigh in pure ecstasy as I feel my semen being drawn up by her.{w}\nI've already been made completely helpless to these girls..."
    vgirl "He's a total slave to my vagina.{w}\nHehehe... He's my prey today."
    tiny_lamia "No fair, no fair!{w}\nI've been licking his nipple until it's all wrinkly!"
    dragon_pup "Uga...{w} Then I get to make him a slave tomorrow!"
    "The two other monster girls continue licking my nipples as Vampire Girl's rape continues.{w}\nLike this, they continue to force me to ejaculate all day..."
    l "Haa..."

    call syasei1 
    show end1 h6 #700
    call syasei2 
    stop hseanwave fadeout 1

    "Every day, the girls come here and rape me until I pass out.{w}\nThe rest of my days live out as a toy to these small girls."
    "A pitiful end for a Hero..."

    scene bg black with Dissolve(3.0)

    "............."

    $ renpy.end_replay()
    $ persistent.hsean_end1 = 1
    $ end_go = "city52_14"
    jump badend2


label city52_14b:
    l "...Hey, what are you doing!?"

    play sound "audio/se/miss_aqua.ogg"

    "I dodge to the side, avoiding her lunge."

    play sound "audio/se/down.ogg"
    show vgirl st03 with dissolve #700

    "Vampire Girl flies past me, and lands on the ground."

    show vgirl st03 at xy(X=250)
    show pramia st03 #701
    show dragonp st03 at xy(X=-250) #700
    with dissolve #702

    dragon_pup "Uga!{w} It failed!"
    tiny_lamia "That strategy would never have worked..."
    vgirl "R...Run away!"

    play sound "audio/se/escape.ogg"
    hide dragonp
    hide pramia
    hide vgirl
    with dissolve

    "The three scared girls run off..."
    l "...What was that about?"
    "I stand puzzled at the back entrance for a few moments before moving on."

    scene bg 018 with blinds
    return


label city53_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 023 with blinds2
    play music "audio/bgm/mura2.ogg"


label city53_main:
    call city_null 
    $ city_bg = "bg 023"
    $ city_button01 = "Girl A" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Young Woman" #603
    $ city_button04 = "Girl B" #604
    $ city_button05 = "Young Boy" #605
    $ city_button06 = "Deputy Village Mayor" #606
    $ city_button07 = "Young Man" #607
    $ city_button08 = "Man" #608
    $ city_button10 = "Trinity" #610
    $ city_button11 = "Harpy Sisters" #611
    $ city_button12 = "Queen Harpy" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city53_01 
        elif result == 2:
            call city53_02 
        elif result == 3:
            call city53_03 
        elif result == 4:
            call city53_04 
        elif result == 5:
            call city53_05 
        elif result == 6:
            call city53_06 
        elif result == 7:
            call city53_07 
        elif result == 8:
            call city53_08 
        elif result == 10:
            call city53_10 
        elif result == 11:
            call city53_11 
        elif result == 12:
            call city53_14 
        elif result == 19:
            return


label city53_01:
    show mob musume1 with dissolve #700

    girl_a "We managed to repulse the invaders with the Harpies help.{w}\nAnd at last, peace has come to this village..."
    girl_a "Also, even some angels have settled here.{w}\nAs long as they aren't violent, I don't really mind."

    return


label city53_02:
    show mob hapy3 at xy(X=160) as mob2
    show mob obasan at xy(X=-160) #700
    with dissolve #701

    pipi "They broke some beehives...{w}\nA lot of bees escaped."
    woman "But as long as our family is safe, that's all I could ask for.{w}\nI'm so happy..."

    show mob hapy2 at xy(X=160) as mob2 with dissolve #701

    piana "Hero... Thanks to you, our village was protected.{w}\nWe are truly grateful to you..."
    piana "...Eh?{w} Our husband?{w}\nHe died in the assault."

    return


label city53_03:
    show mob musume2 with dissolve #700

    young_woman "All seventeen of us in our family fought as one in the battle.{w}\nAnd in the battle, our bond deepened..."
    young_woman "...And as soon as peace came, we got back to some good old family bickering."

    return


label city53_04:
    show mob hapy3 at xy(X=160) as mob2
    show mob syouzyo at xy(X=-160) #700
    with dissolve #701

    girl_b "I was really scared, but I was cheering with Pim from behind!"
    harpy_girl "We kept everyone healthy with the Harpy Dance!{w}\nI hope we were useful..."

    return


label city53_05:
    show mob hapy3 at xy(X=160) as mob2
    show mob syounen at xy(X=-160) #700
    with dissolve #701

    young_boy "........."
    young_harpy_girl "........."
    "They aren't looking at each other anymore...{w}\nI wonder if the war split the young couple up..."

    return


label city53_06:
    show mob rouzin1 as mob2 with dissolve #700

    deputy_village_chief "...The village mayor?{w}\nWhy, that's Queen Harpy, of course."
    deputy_village_chief "Me?{w} I'm just an old man..."

    show mob hapy2 at xy(X=160) as mob2
    show mob rouzin1 at xy(X=-160) #700
    with dissolve #701

    mature_harpy "That's right, honey.{w}\nHehe, forget all of your worries tonight again with me."
    village_chief "Oooh ho ho ho.{image=note}"
    l "..........."

    return


label city53_07:
    show mob seinen1 with dissolve #700

    "There's a man buried in the ground..."
    young_man "Piana... Forgive me...{w}\nI won't flirt with the angels anymore!"
    young_man "Urhg... Someone... Help me...{w}\nPiana!{w} Pipi!{w} Forgive meeeee!!!"

    return


label city53_08:
    show mob ozisan1 with dissolve #700

    man "The Harpies and Angels are both scrambling for men to marry.{w}\nOooh... It's even worse than the surprise attack..."

    return


label city53_10:
    show trinity_c st21
    show trinity_b st11 #701
    show trinity_a st01 #700
    with dissolve #702

    trinity "Who should we crucify with pleasure today?"

    hide trinity_a
    hide trinity_b
    hide trinity_c
    show hapy_a st31
    with dissolve #700

    harpy "I...I won't lose to some angel!{w}\nUs Harpies will learn new tricks too!"

    show hapy_a st31 at xy(X=-240) #700
    show mob musume1 at xy(X=335)
    with dissolve #701

    young_woman "We won't lose either!{w}\nNever underestimate the human spirit!"

    return


label city53_11:
    show hapy_bc st31 at xy(X=160) as hapy_bc2
    show hapy_bc st11 at xy(X=-160) #700
    with dissolve #701

    younger_harpy "I fought with everyone, too.{w}\nBut whenever I got scared, I played dead..."
    older_harpy "All the arrows you shot hit an ally.{w}\nPii was quite annoying..."

    show hapy_bc st32 at xy(X=160) as hapy_bc2 with dissolve #701

    younger_harpy "...Ehehe...{image=note}"

    return


label city53_14:
    if cont == 1:
        play music "audio/bgm/mura2.ogg"
        scene bg 023 with blinds

    $ cont = 0
    show queenharpy st11 with dissolve #700

    queen_harpy "Hero Luka...{w} Thanks to you, the world has been saved.{w}\nOf course, everyone else in the village is grateful as well."
    l "Ah no, no...{w} It wasn't just me.{w}\nIt was everyone's power together that defeated Ilias."
    queen_harpy "So humble...{w} If not for you, victory never would have come about.{w}\nAs Queen of the Harpies, I thank you deeply."
    queen_harpy "As thanks, would you accept the special hospitality of our village?{w}\nEveryone wishes to truly thank you..."
    l "Special hospitality...?"
    "I don't know why, but I have a bad feeling about this.{w}\nI feel like I should turn this down..."

    menu:
        "Accept it{#city53_14}":
            jump city53_14a

        "Refuse{#city53_14}":
            jump city53_14b


label city53_14a:
    if _in_replay:
        play music "audio/bgm/mura2.ogg"
        scene bg 023
        show queenharpy st11
        with Dissolve(1.5)

    queen_harpy "Hehe... Then please come to my house."
    "With a seductive smile, Queen Harpy leads me to her house..."

    play sound "audio/se/asioto2.ogg"
    scene bg 180 with blinds2

    "Wondering what hospitality she's offering, I follow her inside.{w}{nw}"

    show hapy_a st24 at xy(X=-240) #702
    show hapy_bc st32 at xy(X=260) as hapy_bc2
    show hapy_bc st21 at xy(X=20) #700
    $ renpy.transition(dissolve, layer="master") #701

    extend "{w=.7}\nInside, all of the village's harpies are gathered."
    "But contrary to what I thought, there's no feast lain out.{w}\nWhat does she mean by special hospitality...?"

    hide hapy_a
    hide hapy_bc
    hide hapy_bc2
    show queenharpy st11
    with dissolve #700

    queen_harpy "Now for our hospitality..."

    hide queenharpy
    show hapy_a st24 at xy(X=-240) #702
    show hapy_bc st32 at xy(X=260) as hapy_bc2
    show hapy_bc st21 at xy(X=20) #700
    with dissolve #701

    harpy "Alright, get him!{image=note}"
    l "Eh...?{w} Uwa!"

    play music "audio/bgm/ero1.ogg"
    hide hapy_a
    hide hapy_bc
    hide hapy_bc2
    show end2 h1
    with dissolve #700

    "Following Queen Harpy's signal, all of the harpies rush forward, and press their bodies against me from all sides."
    "Completely off guard, I'm quickly bound."
    "Jumping on top of me, the feel of Queen Harpy's feathers makes me go weak for a moment."
    queen_harpy "As the representative of our race, I'll mate with you first.{w}\nPlease give me a lot..."
    "With a seductive smile, Queen Harpy drops her waist.{w}\nMy penis slowly slips inside her warm vagina, pushing past her soft feathers."
    l "W...Wait...!{w}\nAhh!"

    play hseanwave "audio/se/hsean04_innerworks_a4.ogg"

    "I'm deep inside her in only moments.{w}\nI can't help but moan in surprised pleasure at her touch."
    "Her hot, soft vagina presses down on me from every side.{w}\nAs my tip presses against her deepest part, tiny little bumps rub against my sensitive glans."
    l "Ahhh!"
    harpy "Aha, doesn't it feel great to be violated by the Queen?{w}\nJust look at his face, it's all loose!{image=note}"
    "A Harpy giggles as she watches my face loosen in pleasure.{w}\nOthers are closely studying my face with varying looks of curiosity and amusement."
    "The Queen is raping me as they all watch...{w}\nThe humiliating situation just deepens all of the feelings running through me."
    "Even more, the Queen isn't even moving her waist yet...{w}\nJust from insertion, I'm already on the verge of orgasm."
    queen_harpy "Hehe, are you already near your limit?{w}\nEveryone, get a good look at his face as he comes..."
    l "Ahh!"
    "Queen Harpy slowly starts to tighten her vagina, clearly trying to bring me to ejaculation quickly..."
    "At the same time, the tiny bumps inside her vagina rub and stroke against me, all promoting the desire to come inside her that's filling my body..."
    l "I'm going to come... Ah..."
    "Finally surrendering, I relax my body as I give myself over to the ecstasy."

    call syasei1 
    call syasei2 

    l "Haa..."
    "My body shakes as my penis pulses inside her, shooting off spurt after spurt of semen into her warm insides."
    "As weakness washes over me, pure pleasure replaces the strength I used to feel."

    stop hseanwave fadeout 1

    harpy "Wah!{w} He already leaked...{image=note}"
    "Another Harpy closely stares at my face during my orgasm.{w}\nThey're all watching me with mixed looks of curiosity and excitement as I come."
    "All of them are looking at me so closely as I orgasm, every embarrassing face and sound I make..."
    l "Ahh...{w}\nThis is too embarrassing..."
    queen_harpy "Oh my, you came so much...{w}\nShould we let everyone watch you come next time?"

    play hseanwave "audio/se/hsean07_innerworks_a7.ogg"

    "Queen Harpy, who hasn't moved until now, starts to slowly move her waist.{w}\nVery slowly, she moves side to side, and up and down."
    "The gentle roll of pleasure slowly builds up as my penis is moved around inside her, causing my head to go blank."
    l "Ah..."
    "Her wet walls rub against my penis as she sways side to side, the little bumps deep inside her massaging my glans as she moves."
    "I can't endure this for even a minute...{w}\nI feel pathetic, but I can't help but surrender to her."
    l "No... I'm going to come again..."
    queen_harpy "Hehe, you really are easy...{w}\nAlright then, let's show everyone your ejaculation..."
    "Queen Harpy slowly rises up, pulling my penis out of her vagina."
    "Right before I'm all the way out of her, she clenches down on the neck of my penis and pulls on my tip.{w}\nThe final stimulation acts as the finishing blow, sending me over."
    l "Ahhh!!!"

    call syasei1 
    show end2 h2 #700
    call syasei2 

    "The same instant I'm completely out of her vagina, I reach a powerful orgasm.{w}\nWith my whole body shaking in a powerful climax, I come right into the air."

    show end2 h3 with dissolve #700

    "It lands back down all over my body, causing me to pollute myself with my own semen."

    stop hseanwave fadeout 1

    harpy "Waah!{w} Look at how much he shot!"
    "The eyes of the Harpies surrounding me all go bright with amusement and interest.{w}\nThe ones who had semen land on their feathers all go to lick it up."
    queen_harpy "Oh my, oh my...{w} I thought you might dribble a little bit onto your stomach.{w}\nBut to shoot out so much all over the place...{w} Aren't you just a naughty little boy?"
    "Queen Harpy looks down at me and giggles, her eyes taking in all the semen I spilled everywhere."
    harpy "My Queen...{w} We want more semen..."
    "One of the Harpies who was licking up the semen on her feathers calls out to the Queen."
    queen_harpy "I see...{w} Then we'll need to ask our honored guest for some more."

    play hseanwave "audio/se/hsean17_sigoki.ogg"

    "The Queen spreads open her legs, and starts to rub her thighs against my penis.{w}\nMoving her body up and down, her smooth skin rubs against my sensitive penis in a rhythm."
    "Her ticklish feathers rub against my shaft from the sides, as the back of my penis rubs against the wet entrance to her vagina."
    l "Ahhh!!!"
    queen_harpy "You won't be able to last that long with this, will you?{w}\nEveryone, make sure you can get a good view of his penis as he ejaculates."
    l "No... I'm going to come again...{w}\nAhh!"

    call syasei1 
    show end2 h4 #700
    call syasei2 

    "As the Queen violates me with intercrural sex, I finally succumb to the rising climax.{w}{nw}"

    show end2 h5
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nMy semen shoots straight into the air, landing back down on all the closely watching Harpies."

    stop hseanwave fadeout 1

    harpy "Waah... So much semen!{image=note}"
    "The Harpies all happily start licking up my semen as the Queen stares at me, satisfied.{w}\nHer gaze slowly drops as she meets my eyes with a smile."
    queen_harpy "Hehe... You are a guest of honor in our village."
    queen_harpy "We'll keep entertaining you forever, so just give us a delicious feast of semen in return..."
    l "Ahh..."
    "It doesn't seem like I'll be able to escape from this village.{w}\nThis obscene feast will never end..."

    scene bg black with Dissolve(3.0)

    ".............."

    $ renpy.end_replay()
    $ persistent.hsean_end2 = 1
    $ end_go = "city53_14"
    jump badend2


label city53_14b:
    queen_harpy "...I see, that's a shame.{w}\nAll of our precious preparations will go to waste..."
    "Her shoulders slump down in dejection."
    l "I...I'm sorry..."
    "I give her a sincere apology."

    return


label city55_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 037 with blinds2
    play music "audio/bgm/city1.ogg"


label city55_main:
    call city_null 
    $ city_bg = "bg 037"
    $ city_button01 = "Young Man" #601
    $ city_button02 = "Man" #602
    $ city_button03 = "Young Woman" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Mermaid" #605
    $ city_button06 = "Mermaid General" #606
    $ city_button07 = "El" #607
    $ city_button08 = "Mermaid Merchant" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #613
    $ city_button14 = "Meia's House" #614
    $ city_button15 = "Queen's Palace" #615
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city55_01 
        elif result == 2:
            call city55_02 
        elif result == 3:
            call city55_03 
        elif result == 4:
            call city55_04 
        elif result == 5:
            call city55_05 
        elif result == 6:
            call city55_06 
        elif result == 7:
            call city55_07 
        elif result == 8:
            call city55_08 
        elif result == 10:
            call city55_10 
        elif result == 11:
            call city55_11 
        elif result == 12:
            call city55_12 
        elif result == 13:
            call city55_13 
        elif result == 14:
            call city55_14 
        elif result == 15:
            call city55_15 
        elif result == 19:
            return


label city55_01:
    show mob mermaid2 at xy(X=160) as mob2
    show mob seinen1 at xy(X=-160) #700
    with dissolve #701

    young_man "Our bond is eternal, Honey!"
    mermaid "Oooh Darling, I love you forever!"
    l "Oh, did they make up?"
    young_man "No, I broke up with that other Mermaid.{w}\nMy new Honey is a different one."
    l "T...That's horrible..."
    mermaid "Not at all... He's a new Darling for me, too!"
    l "Oh... I guess that makes it alright...?"

    hide mob
    hide mob2
    show alice st07b
    with dissolve #700

    a "...No it doesn't!{w} That's not good at all!{w}\nIt's been a month!{w} What kind of people are you two!?"

    return


label city55_02:
    show mob ozisan1 with dissolve #700

    man "The mermaids following the Queen Mermaid settled down here, too.{w}\nOn the surface, the men here hesitated...{w} But deep down, we were all jumping for joy."
    man "The women aren't very receptive, of course...{w}\nIt's true they attacked the village once...{w} But they reformed, and helped defend it the second time."

    return


label city55_03:
    show mob musume2 with dissolve #700

    young_woman "After the decisive battle was won, more mermaids came here to settle down.{w}\nI'm not a racist, but...{w} Now there's going to be a lot of competition for men..."

    return


label city55_04:
    show mob sensi2 with dissolve #700

    soldier "Should I retire from being a soldier, and settle down here?{w}\nI want to take a mermaid wife, and take it easy..."

    return


label city55_05:
    show mob mermaid2 with dissolve #700

    mermaid "Mermaids under the direct control of the Queen Mermaid are living here now.{w}\nBut the Queen herself is living in a palace a little ways away..."
    mermaid "Still, she has accepted humans and mermaids living together...{w}\nI'm so happy!"

    return


label city55_06:
    show g_mermaid st01 with dissolve #700

    mermaid_general "Hero Luka...{w} I apologize for my actions before.{w}\nI've taken the role of Guard Captain for this town, now."
    mermaid_general "The humans here are actually quite kind.{w}\nI truly lament the fact that I once tried to destroy this town..."

    return


label city55_07:
    show ningyohime st06 at xy(X=160, Y=150)
    show mob mermaid3 at xy(X=-160) #700
    with dissolve #701

    young_mermaid "I made friends with El!{image=note}"
    el "Hey, hey!{w} What should we play next!?"

    return


label city55_08:
    show meia st21 at xy(X=50, Y=50) with dissolve #700

    mermaid_merchant "Neither the Fried Sea Slug or Fried Starfish sold at all...{w}\nEven my secret item, Best Best Manju Crab didn't sell at all."
    l "Like I said, why are you obsessed with these strange foods...?"
    mermaid_merchant "I'm out of ideas now...{w}\nSo now I'm selling myself!"
    l "Ehh!?{w} FRIED MERMAID!?"
    mermaid_merchant "No!{w} I'm trying to sell my personality so someone will take me as a bride!{w}\nWhat did you think I meant!?"
    l "........"

    return


label city55_10:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "Since then, the Queen Mermaid and her group have moved in.{w}\nThose mermaid blacksmiths under the Queen are quite good."
    shopkeeper "I can't match their quality, and I never had great sales during peacetime anyway...{w}\nI think I'll have to look into opening up a different kind of shop."

    scene bg 037 with blinds
    return


label city55_11:
    scene bg 154 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "I've done it!{w} I've created Mermaid Knee Socks!{w}\n...But it doesn't look that sexy."
    shopkeeper "It sort of just looks like a tight fin bag.{w}\nAnd if it isn't sexy, there is no meaning to its existence."

    show alice st01b at xy(X=-120) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "............Damn pervert."

    scene bg 037 with blinds
    return


label city55_12:
    scene bg 156 with blinds
    show mob mermaid2 with dissolve #700

    mermaid_priest "Heellloooo!{w} Welcome, welcome!{image=note}{w}\nIf you have something to confess, I'll listen!{image=note}"

    show mob mermaid2 at xy(X=-160) #700
    show mob sinkan at xy(X=160) as mob2
    with dissolve #701

    priest "After Ilias fell, I looked for something new for my church.{w}\nThe result is this Mermaid Church."
    l "I...I see..."

    show mob mermaid2 at xy(X=-160) #700
    show mob seinen1 at xy(X=160) as mob2
    with dissolve #701

    boy "I...I'll confess...{w}\nI've been feeling strange in my lower body whenever I look at the mermaids..."
    mermaid_priest "Then let me just fix that for you...{image=note}"
    l "...Is this just a brothel!?"

    scene bg 037 with blinds
    return


label city55_13:
    scene bg 155 with blinds
    show mob mermaid2 at xy(X=160) as mob2
    show mob mermaid1 at xy(X=-160) #700
    with dissolve #701

    mermaid_a "We're having our grand re-re-opening!{w}\nWe've got new employees, too!{w} A complete revival!"
    mermaid_b "Hehe... It's nice to meet you, Mr. Hero.{w}\nAs thanks for saving the world...{w} I'd love to give you some extra friendly service..."
    l "No...{w} That's alright..."

    hide mob
    show alice st01b at xy(X=-120)
    with dissolve #700

    a "..........."
    mermaid_b "M...Monster Lord!{w}\nIt was just a joke...!"
    mermaid_b "Forgive me, I'd never intend to take away your husband.{w}\nPlease, make it quick if you have to!"

    show alice st03b at xy(X=-120) with dissolve #700

    a "Come on, don't be so scared!{w}\nBut why are these strange rumors spreading all over the place!?"

    scene bg 037 with blinds
    return


label city55_14:
    if cont == 1:
        play music "audio/bgm/city1.ogg"

    $ cont = 0

    scene bg 049 with blinds
    show meia st01 at xy(X=50, Y=50) with dissolve #700

    meia "Thank you, Hero!{w}\nYou've helped me out so much..."
    l "Oh no, no..."
    meia "I really must thank you, no matter what.{w}{nw}"

    show meia st02 at xy(X=50, Y=50)
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nSo... If you would like, why not enjoy my pussy?"
    l "I...I can't do that!{w}\nBesides, aren't you married!?"
    meia "Of course...{w}\nA Hero like you would never be satisfied with a married woman's used pussy..."
    meia "But I still have confidence in myself, because I know I can make any man come..."
    l "..........."
    "I nervously swallow.{w}\nBut she's still married..."

    show meia st03 at xy(X=50, Y=50) with dissolve #700

    meia "My husband only lasts ten seconds every night.{w}\nOnce you're deep inside, it feels so good you could just melt away in ecstasy..."
    meia "But would a Hero still not be satisfied with that?{w}\nIf you don't mind, I'd love for you to try me..."
    l "T...That's..."

    menu:
        "Just once...{#city55_14}":
            jump city55_14a

        "Absolutely not{#city55_14}":
            jump city55_14b


label city55_14a:
    if _in_replay:
        play music "audio/bgm/city1.ogg"
        scene bg 049
        show meia st03 at xy(50, 50)
        with Dissolve(1.5)

    play music "audio/bgm/ero1.ogg"

    meia "Hehe... Then please enjoy this married woman's pussy."
    "Meia hugs me, bringing my face next to her soft, warm breasts."
    l "Nnnn... M...Meia... This..."
    meia "Hehehe... My husband loves being held like this.{image=note}"
    "While saying that, Meia presses her waist against me.{w}\nThe tip of my already erect penis presses against her wet opening."
    meia "Mmm...{image=note}"

    play hseanwave "audio/se/hsean02_innerworks_a2.ogg"
    hide meia
    show end4 h1
    with dissolve #700

    "Pressing her waist against mine, Meia brings me inside her vagina!"
    "Her folds rub against me as I slip inside, her already wet flesh letting me slip in easily."
    l "Ahh!!!"
    "The incredible touch of her leaves me moaning in no time.{w}\nThe wet folds inside her seem to lick me clean at the same time."
    meia "Hehehe, already moaning?{w}\nOh my, Hero, you're so sensitive.{image=note}"
    l "But...{w}\nYour vagina feels so good, Meia..."
    meia "Hehe, that makes me so happy...{image=note}"
    "Meia presses the back of my head harder against her soft breasts.{w}\nAt the same time, her warm vagina squeezes my penis, deep inside her."
    "It feels too good...{w}\nThe comfortable pleasure invites me to a climax in only moments."
    l "Ahh... I'm going to come..."
    meia "Ara?{w} Already satisfied?{w}\nBut even my husband can endure more..."
    "Meia strokes my head as she giggles."
    meia "Alright, then I'll tighten my insides...{w}\nI'm sure you'll come right away."
    meia "Please come right inside a married woman's pussy.{image=note}"
    l "Ahh... I...I shouldn't..."
    "The immoral nature of what I'm doing attacks me again...{w}\nI can't come inside someone who's married...!"
    l "Ah... Meia... Wait..."
    meia "I'll tighten... Now.{image=note}"
    l "No, wait... Ah!"
    "Meia's vagina squeezes down, pressing her soft flesh against my penis.{w}\nWith my mind focused on the immorality of what we're doing, my body loosens in pleasure."

    call syasei1 
    show end4 h2 #700
    call syasei2 

    l "Ahh..."
    "I explode inside her, my semen shooting into the married mermaid's vagina."
    "Going slack in her embrace, I let the sensations from my orgasm flow through my loose body."
    meia "Hehehe... The face the Hero makes when he comes is so cute...{image=note}"
    "Meia smiles and giggles as I come inside her.{w}\nI came inside a married woman...{w} What am I doing...?"
    l "I'm so sorry Meia...{w} Let's stop..."
    "After I finish coming inside her, I try to pull myself out..."
    l "Eh...?{w} N...No..."
    meia "Do you want to taste the technique I haven't even shown my husband yet?"
    l "Ahhh!!!"

    play hseanwave "audio/se/hsean05_innerworks_a5.ogg"

    "Deep inside her, something pulls in my glans.{w}\nIt's as if a mouth inside her is sucking on my tip..."
    l "Haa... What... Is this...?"
    meia "I sucked in your tip with my womb...{w}\nIt's too soon for my husband.{w} But for you..."
    l "N...No... Ahhh!"
    "My waist shakes in ecstasy as her womb sucks on my glans.{w}\nThe intense pleasure blows away my reason all at once."
    "It's like she's sucking everything from me...{w}\nThere's no way I can endure this!"
    meia "You'll come right inside a married woman's womb...{w}\nShould we see if you get me pregnant? Hehehe.{image=note}"
    l "N...No... Not that...{w}\nAhh!"
    "No matter how much I try to endure, I'm helpless in front of this mad pleasure.{w}\nWith my body going limp, I surrender to the immoral ecstasy..."

    call syasei1 
    show end4 h4 #700
    call syasei2 

    l "Ahh..."
    "My penis pulses, shooting semen straight into Meia's womb...{w}\nThe guilt of having possibly gotten a married woman pregnant flows through me along with the pleasure from my orgasm."
    "Meia's vagina squeezes me as I come, accepting everything I have to shoot into her..."
    l "Ahh..."
    meia "Hehe... Are you satisfied?"

    stop hseanwave fadeout 1

    "Meia holds me as I bask in the afterglow of my orgasm.{w}\nWith that, I start an illicit relationship with a married mermaid..."

    scene bg black with Dissolve(3.0)

    "I become a captive of a mermaid's pussy.{w}\nIn search of pleasure, I keep coming to Meia's embrace."
    "This illicit relationship continues, my heart slowly coming to belong to Meia.{w}\nA captive of Meia, my life as a Hero ends..."

    scene bg black with Dissolve(3.0)

    "............."

    $ renpy.end_replay()
    $ persistent.hsean_end4 = 1
    $ end_go = "city55_14"
    jump badend2


label city55_14b:
    show meia st04 at xy(X=50, Y=50) with dissolve #700

    meia "I see...{w} That's too bad.{w}\nSo my body isn't enough to satisfy the Hero..."
    l "No, that's not it..."

    hide meia with dissolve

    "Anyway, there's no good way out of this situation.{w}\nI quickly run away from Meia's house."

    scene bg 037 with blinds
    return


label city55_15:
    scene bg 231 with blinds
    show queenmermaid st01 with dissolve #700

    queen_mermaid "My misanthropy cannot be changed now.{w}\nBut there's no reason for me to pass it down upon my descendants..."
    queen_mermaid "So I've decided to migrate here for the sake of my family.{w}\nI will let them experience for themselves, and let them decide on their own."
    l "I see...{w}\nI'm sure everyone can live together."
    "And one day, even the Queen herself...{w}\nIf she lives here and sees how everyone lives together, I'm sure she'll come around someday..."
    queen_mermaid "I'm also here to ensure a tragedy as what happened to me before doesn't arise again.{w}\nI'm expecting great things from you..."

    scene bg 037 with blinds
    return


label city56a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 048 with blinds2
    play music "audio/bgm/city1.ogg"


label city56a_main:
    call city_null 
    $ city_bg = "bg 048"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Women" #602
    $ city_button03 = "Children" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Sister Lamia" #605
    $ city_button06 = "Chimera Prison" #606
    $ city_button07 = "Mariel" #607
    $ city_button08 = "Cupid" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "San Ilia Castle" #613
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city56a_01 
        elif result == 2:
            call city56a_02 
        elif result == 3:
            call city56a_03 
        elif result == 4:
            call city56a_04 
        elif result == 5:
            call city56a_05 
        elif result == 6:
            call city56a_06 
        elif result == 7:
            call city56a_07 
        elif result == 8:
            call city56a_08 
        elif result == 10:
            call city56a_10 
        elif result == 11:
            call city56a_11 
        elif result == 12:
            call city56a_12 
        elif result == 13:
            jump city56b_main0
        elif result == 19:
            return


label city56a_01:
    show mob seinen1 with dissolve #700

    boy "After that battle, the angels started living on the surface world, too.{w}\nMost of them gathered in this holy city of San Ilia."
    boy "Even though they're angels, I think they're at a loss as to what to do now...{w}\nThat makes me feel relieved, somehow."

    return


label city56a_02:
    show mob zyosei with dissolve #700

    woman_a "There sure are a lot of angels in the city...{w}\nI don't know whether to be afraid or scared."

    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_b "But they aren't harming anyone now.{w}\nSince Ilias was brought down, things have been peaceful."

    hide mob2
    show mob madam at center
    with dissolve #700

    woman_c "Most of them look really depressed and sad...{w} I feel kind of sorry for them."

    if ivent05 == 0:
        return

    show mob madam at xy(X=-160) #700
    show mob zyoseiy at xy(X=160) as mob2
    with dissolve #701

    ghost_lady "I won't forget my grudge at trying to be \"purified\"...{w}\nBut I'm feeling a little sorry for them now, too."

    return


label city56a_03:
    show fairys_c st21 at xy(X=450, Y=100)
    show mob syouzyo as mob2 #701
    show mob syounen at xy(X=-220) #700
    with dissolve #702

    young_boy "Yaaay!{w} The angels are playing with us!"
    girl "We're playing hide and seek with Cupid!"
    fairy "And she said she'll teach us things that are much more fun when we're older!"
    l "..............."

    return


label city56a_04:
    show mob sensi1 with dissolve #700

    soldier "Dancing fairies...{w} Wandering ghosts...{w} Plotting angels...{w}\nWhen I think about how this is supposed to be the most holy place in the world, I can't help but laugh."
    soldier "...Though, I like this city.{w}\nI'm never bored here!"

    return


label city56a_05:
    show sisterlamia st04 with dissolve #700

    sister_lamia "Surprisingly, I was selected as an official priest...{w}\nThe Church's new policy is to spread awareness and change in regards to human and monster coexistence."
    sister_lamia "To pick me... One who attacked this city in the past...{w}\nI must work as hard as I can, to repay the compassion of this wonderful place!"

    return


label city56a_06:
    show fairys_e st41 at xy(X=330, Y=190)
    show c_prison st01 at topleft #700
    with dissolve #701

    "A fairy is being held captive inside the Chimera Prison!"
    l "W...What's going on!?{w}\nRelease that fairy at once!"
    fairy "Noooo!{w} This is my house!"
    chimera_prison "She seems to like me for some reason...{w}\nPeople keep thinking I'm abusing her...{w} And they throw stones..."
    l "S...Sorry for accusing you..."

    return


label city56a_07:
    show mariel st04 with dissolve #700

    mariel "After Ilias's downfall, most of the angels have fallen into grief and despair.{w}\nI don't know what I should do now, either..."
    "The angels seem to be more at a loss than anyone else...{w}\nIt doesn't look like they even have the energy left to do anything bad."
    "I hope they're able to live alongside humans and monsters someday, too..."

    return


label city56a_08:
    show cupid st12 at xy(X=30) with dissolve #700

    cupid "The humans here are so serious...{w} It isn't interesting at all!{w}\nShould I go play in another town?"
    l ".........."
    "Even after Ilias is gone, it doesn't seem like some angels will change..."

    return


label city56a_10:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "I thought I'd make a huge profit after the war, when things start to get wild in lawlessness...{w}\nBut the world is so war weary, violence is almost completely gone."
    shopkeeper "It looks like weapons aren't needed, either...{w}\nNow...{w} How to make money to eat...?"

    scene bg 048 with blinds
    return


label city56a_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "The \"Legend of Hero Luka\" sure is selling!{w}\nEvery time I get more in stock, they vanish in less than an hour!"
    l "Ehh!?{w} W...What's that!?"
    shopkeeper "Oh, you didn't know?{w}\nIt's a bestselling biography describing all of the battles of the Hero Luka."
    shopkeeper "I mean, you're cosplaying as him, right?{w}\nAnd a good one, too...{w} You look just like him!"
    l "There's a biography out...?"

    show alice st01b at xy(X=-120) #700
    show mob ozisan3 at xy(X=160)
    with dissolve #701

    a "I figured one would show up someday... But so soon?{w}\nWhat the heck...?"
    "Since the book is sold out, I can't even see what's in it.{w}\nCould the source of all those strange rumors be that book...?"

    scene bg 048 with blinds
    return


label city56a_12:
    scene bg 156 with blinds

    if ivent05 < 3:
        call city_noiv 
        scene bg 048 with blinds
        jump city56a_main

    show chrom st22 with dissolve #700

    chrome "Ooh, it's Luka."
    l "...Chrome?{w}\nThis doesn't seem like a place you'd hang out at...{w} What are you doing here?"
    chrome "I'm delivering a list of all those buried at the Northern Mansion.{w}\nNone of them were on the central registry, so I thought I should add them."
    l "I see...{w} Is Frederika still with you?"

    show chrom st21 with dissolve #700

    chrome "...After the fight was over, I laid Frederika back to rest.{w}\nShe shouldn't be in this world any longer."
    chrome "Frederika, Spi, and Rit's souls have all been sent on.{w}\nThey're all enjoying their peaceful sleep..."
    l "I see, that's for the best.{w}\nSo you've really reflected on your actions, Chrome?"

    show chrom st24 with dissolve #700

    chrome "Chrome doesn't reflect on anything!{w}\nSince the Monster Lord is easily scared, I just won't make Ghosts or Zombies any longer!"
    l "That's... A rather calculating reason..."

    show chrom st22 with dissolve #700

    chrome "So from now on, I'm Dollmaker Chrome!{w}\nMy new magical arts won't scare the Monster Lord!"

    show alice st07b at xy(X=-120) #700
    show chrom st22 at xy(X=160)
    with dissolve #701

    a "I...I wasn't scared of anything!{w}\nI was just worried about the impact on morale such horrible arts would have...{w} As the Monster Lord...{w} On the populace..."
    "...After all this, she's still pretending?{w}\nEven though she's scared, she's being so stubborn about it..."

    hide alice
    show chrom st22 at center
    with dissolve #700

    l "...Anyway, good luck.{w}\nI'm looking forward to seeing your new arts, Dollmaker Chrome."
    chrome "Yes, we'll meet again in the Monster Lord's Castle!"
    "If she's just making dolls, she shouldn't be able to hurt anyone.{w}\nI wish the best for Chrome in the future."

    scene bg 048 with blinds
    return


label city56b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 044 with blinds2
    play music "audio/bgm/castle1.ogg"


label city56b_main:
    call city_null 
    $ city_bg = "bg 044"
    $ city_button01 = "Castle Guard" #601
    $ city_button02 = "Priests" #602
    $ city_button03 = "Scholar" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Angel Soldiers" #605
    $ city_button06 = "Valkyrie" #606
    $ city_button10 = "Large Chapel" #610
    $ city_button11 = "Underground Library" #611
    $ city_button12 = "Audience Room" #612
    $ city_button13 = "Guard Captain's Room" #613
    $ city_button19 = "Back to Town" #619

    while True:
        call cmd_city 

        if result == 1:
            call city56b_01 
        elif result == 2:
            call city56b_02 
        elif result == 3:
            call city56b_03 
        elif result == 4:
            call city56b_04 
        elif result == 5:
            call city56b_05 
        elif result == 6:
            call city56b_06 
        elif result == 10:
            call city56b_10 
        elif result == 11:
            call city56b_11 
        elif result == 12:
            call city56b_12 
        elif result == 13:
            call city56b_13 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            show bg 048 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city56a_main


label city56b_01:
    show mob heisi1 with dissolve #700

    castle_guard "Everyone is slowly adjusting back to peace.{w}\nThe fact that it was Ilias herself that brought about war sure didn't help."
    castle_guard "With the cornerstone of most people's faith gone, it's more difficult than usual...{w}\nBut still, we all must continue to move forward."

    return


label city56b_02:
    show fairys_e st41 at xy(X=560, Y=100)

    if ivent05 == 0:
        show mob sinkan as mob2 #701
    elif ivent05 > 0:
        show mob sinkany as mob2 #701

    show mob sinkan at xy(X=-220) #700
    with dissolve #702

    priest_a "Waah!?{w} What is this strange little creature!?"

    if ivent05 == 0:
        priest "Hyaa!{w} Where'd it come from!?"
    elif ivent05 > 0:
        ghost_priest "Hyaa!{w} Where'd it come from!?"

    fairy "Heeeeeyyyy!{w} Let's play!"
    "It seems like the priests are finally able to see the fairies..."

    return


label city56b_03:
    show mob gakusya with dissolve #700

    scholar "Many books we had believed lost were located in the Monster Lord's library.{w}\nJust looking at the list of books made me start to tremble..."
    scholar "In honor of the new friendship between our races, many of those books were donated to our library, too.{w}\nPretty much all of us scholars are openly welcoming our new Monster friends as a result."
    scholar "Thousands of applications to visit the library in person have also been submitted...{w}\nMy application was approved for next month.{w} I'm already looking forward to it!"

    show alice st02b at xy(X=-140) #700
    show mob gakusya at xy(X=160)
    with dissolve #701

    a "Hmm...{w} It was Tamamo's idea to increase cultural exchange by lending out our books...{w}\nIt seems like it's more effective than I thought it would be."

    return


label city56b_04:
    show fairys_b st12 at xy(X=430, Y=100)
    show mob sensi1 at xy(X=-160) #700
    with dissolve #701

    soldier "I had fun mountain climbing the other day.{w}\nYou want to try hiking to the sea tomorrow?"
    fairy "Yaaay!{w} I wanna see the sea!"

    return


label city56b_05:
    show angels_b st11 at xy(X=280)
    show angels_d st31 at xy(X=90) #702
    show angels_c st21 at xy(X=-90) #701
    show angels_a st01 at xy(X=-280) #700
    with dissolve #703

    angel_soldier_a "Our weapons will no longer be used for heaven's punishment.{w}\nBut for beings like us... Who only know how to punish sinners..."
    angel_soldier_a "I decided to apply to San Ilia as a governmental punishment official."
    angel_soldier_a "I'll use this sacred whip of mine to enact punishment on criminals.{w} Within the bounds of the laws of San Ilia."
    angel_soldier_b "I will light up the dark nights with this candle.{w}\nI wish to ease the anxiety of the populace, if only a little."
    angel_soldier_c "I'll use this sacred feather duster to clean the floors of the Castle.{w}\nI love cleaning..."
    angel_soldier_d "This sacred onahole...{w} Uhm... Err...{w}\nH...How else can I use this...?"

    return


label city56b_06:
    show valkyrie st01 at xy(X=-50) with dissolve #700

    valkyrie "I don't intend to cause any trouble over Ilias.{w}\nI intend to live quietly on the surface world, protecting the Churches."
    "It looks like Valkyrie is working as a Guard now."

    show valkyrie st01 at xy(X=-280) #700
    show mob heisi1 at xy(X=160)
    with dissolve #699

    soldier "Hey!{w} A woman like you shouldn't be in this castle!"
    valkyrie "You insult an angel...?{w}\nYour punishment is a triple ejaculation.{w} I shall carry it out now."

    scene bg black with Dissolve(1.0)

    soldier "Ah...Aiiieee!"

    call syasei1 
    call syasei2 
    scene bg 044
    show valkyrie st01 at xy(X=-50) #701
    show valkyrie bk03 at xy(X=-50) zorder 10 as bk #700
    with dissolve

    valkyrie "I won't let anyone insult me to my face.{w}\nBut no matter how many times I punish them, they keep doing it."
    valkyrie "They must truly dislike me..."
    l "No...{w} I think they're doing it for another reason..."
    valkyrie "...Hero Luka, there was something I wished to discuss with you.{w}\nI wished to talk with you about how to coexist with humans on the surface world."
    valkyrie "Would you please come to the soldier's waiting room a little bit later?"
    l "Of course, I'd be glad to talk with you about that!"

    $ check09 = 1

    return


label city56b_10:
    scene bg 157 with blinds
    play music "audio/bgm/comi1.ogg"
    show mob musume2 at xy(X=395) as mob2
    show sylph st21r at xy(X=335, Y=190) #701
    show mob seinen1 at xy(X=-220) #700
    with dissolve #702

    boy "Oooh! It's Sylph!"
    sylph "Ahem!"
    girl "She helped out the brave Hero and took down the evil Goddess!"
    sylph "Ahem!"
    "Sylph is in the cathedral!{w} And surrounded by tourists...?"

    play music "audio/bgm/castle1.ogg"

    l "Sylph... What's going on?"

    hide sylph
    show mob gakusya at xy(X=160) as mob2
    show mob sinkan at xy(X=-160) #700
    with dissolve #701

    priest "We asked her to live here."
    scholar "Long ago, Sylph was revered in this land.{w}\nNow that Ilias is gone, attempts to revive the old faith are being pushed."
    l "I see..."
    "So Sylph was worshipped a long time ago..."

    hide mob2
    show sylph st21r at xy(X=130, Y=190) #700
    show mob sinkan at xy(X=160)
    with dissolve #701

    sylph "I'm hungryyyy!{w}\nI want to eat candy!"
    priest "Then, please have this piece..."
    sylph "No, I want what I had before!{w} Baumkuchen!"
    priest "P...Please wait a moment, I'll prepare some at once..."
    l "Sylph, you shouldn't act selfish..."
    sylph "Yeah, I know...{w}\nBut I was alone for so long, I'm just living it up for now.{image=note}"
    l "...Oh well."
    "Sylph has probably felt lonely for a long time..."

    hide sylph
    show alice st01b at xy(X=-120) #700
    show mob sinkan at xy(X=160)
    with dissolve #701

    a "I'd like some of that Baumkuchen..."
    l "You'll be waiting a while, it seems."
    priest "How about a Sylph Manju, Ms. Monster Lord?"

    show alice st02b at xy(X=-120) with dissolve #700

    a "Oooh... So you had something like that?"
    l "............"
    "It's already a total tourist trap..."

    scene bg 044 with blinds
    return


label city56b_11:
    castle_guard "Oooh! Hero Luka!{w} Perfect timing!{w}\nThere are monsters rampaging around the library!"

    play music "audio/bgm/kiki2.ogg"

    l "W...What!?"

    show alice st01b with dissolve #700

    a "We must address this at once!"

    hide alice with dissolve

    "After hearing that, we rush into the underground library..."

    play sound "audio/se/asioto4.ogg"
    scene bg 046 with blinds2
    show page65537 st23 with dissolve #700

    page_65537 "Finally...{w} I've finally regained my power!{w}\nYou damn Hero... I won't let it end with that!"
    "She's one of the book monsters I fought before!{w}\nSo she has been sealed since then..."
    page_65537 "You appeared, Hero!{w}\nThis time, I'll get revenge for-"

    play music "audio/bgm/castle1.ogg"
    show alice st01b at xy(X=-120) #700
    show page65537 st23 at xy(X=200)
    with dissolve #701

    a "...Wait.{w} Everything has changed since then."
    page_65537 "M...Monster Lord!{w}\nWhy are you in a place like this!?"
    a "The Hero marched into the Monster Lord's Castle, and defeated all four of the Heavenly Knights."
    page_65537 "Egads!"
    a "After that, I myself lost to him."
    page_65537 "Egads!"
    a "Then, Ilias was going to destroy the world itself."
    page_65537 "Egads!"
    a "Then the Hero and I came together, and attacked her."
    page_65537 "Egads!"
    a "So with that, humans and monsters have come together.{w}\nThere's no need to attack humans any longer."
    page_65537 "...Forgive me, Monster Lord.{w}\nIn the midst of such historic fights, I was unable to do anything..."
    a "...No, my fire was the cause of you getting sealed...{w} Sorry."
    page_65537 "Egads!"

    scene bg 044 with blinds
    return


label city56b_12:
    scene bg 045 with blinds
    show sanilia n st02 with dissolve #700

    san_ilia_king "Ooh!{w} The Hero Luka!{w}\nI've been worrying nonstop since I heard you were in a coma!"
    san_ilia_king "First, on behalf of every citizen, I wish to thank you.{w}\nIf not for your actions, the world would have come to an end."
    l "No, no... It was the result of everyone coming together as one.{w}\nSo... How are you and the other priests?"
    "They were all betrayed by the Goddess they believed in...{w}\nI wonder how they've been this past month?"

    show sanilia n st01 with dissolve #700

    san_ilia_king "Many are still unsure of what to do now.{w}\nThe huge hole where their faith was can't be so simply filled..."
    san_ilia_king "But they've lived their lives guiding those who were lost until now.{w}\nI'm sure everyone will come to find their own paths, with time."

    show alice st01b at xy(X=-120) #700
    show sanilia n st01 at xy(X=160)
    with dissolve #701

    a "How about the general populace?"
    san_ilia_king "There is, unexpectedly, little unrest.{w}\nIt's ironic, but the dwindling faith of the past few generations may have saved us all..."

    show sanilia n st03 at xy(X=160) with dissolve #701

    san_ilia_king "Ilias was less popular than we all believed."
    l "..........."

    hide alice
    show sanilia n st01 at center
    with dissolve #700

    san_ilia_king "...Though the devout believers are at a big loss.{w}\nOur role will be to show them how to continue on a new path..."
    l "I see... We're all counting on you for the future.{w}\nIt looks like the priesthood still has a big role to play going forward."

    show sanilia n st03 with dissolve #700

    san_ilia_king "...I've been thinking about the Holy Sword, given by Ilias.{w}\nThe one that the Monster Lord destroyed so easily."

    show alice st01b at xy(X=-120) #700
    show sanilia n st04 at xy(X=160)
    with dissolve #701

    a "Ah, yes... It was pretty brittle."
    san_ilia_king "Why did Ilias bestow that sword upon us?{w}\nShe told us to give it to a true Hero..."
    san_ilia_king "With our faith in her, we couldn't see the sword for what it truly was, of course.{w}\nBut why give us a useless sword, if she truly wanted the Monster Lord defeated?"
    a "...Aren't you thinking about it too much?{w}\nShe was just an evil Goddess harassing you."
    a "It gave me quite a laugh when I saw you all treating that sword like some sacred object, though."
    san_ilia_king "It's true she may have just been toying with us...{w}\nBut I would like to think it was some sort of message."

    show sanilia n st02 at xy(X=160) with dissolve #701

    san_ilia_king "...Anyway, I thank you, Hero Luka.{w}\nYour name and actions will become the thing of legends!"

    scene bg 044 with blinds
    return


label city56b_13:
    if cont == 1:
        play music "audio/bgm/castle1.ogg"

    $ cont = 0
    scene bg 060 with blinds

    if check09 == 0:
        call city_noiv 
        scene bg 044 with blinds
        jump city56b_main

    show valkyrie st01 at xy(X=80)
    show cupid st13 at xy(X=-160) #700
    with dissolve #701

    valkyrie "So you came...{w}\nThen, please give me some counsel."
    valkyrie "You don't mind if I take a little bit of your time, do you?"
    cupid "Ehehe...{image=note}"
    l "..........."
    "...I have a bad feeling.{w}\nThere's nothing wrong with an angel trying to live peacefully on the ground..."
    "But there's a strange gleam in her eyes.{w}\nShould I accept her invitation...?"

    menu:
        "Give her counsel{#city56b_13}":
            jump city56b_13a

        "Refuse{#city56b_13}":
            jump city56b_13b


label city56b_13a:
    if _in_replay:
        play music "audio/bgm/castle1.ogg"
        scene bg 060
        show valkyrie st01 at xy(80, 0)
        show cupid st13 at xy(-160, 0)
        with Dissolve(1.5)

    l "Sure, I'll help you out."

    show valkyrie st02 at xy(X=80) with dissolve #701

    valkyrie "Then the grudges I've...{w}\nNo, I mean the questions I've built up...{w} Please hear them."
    "Valkyrie locks the door to the room.{w}\nJust then, I notice it's a trap!"

    scene bg 060 with blinds2
    play music "audio/bgm/ero1.ogg"
    play hseanwave "audio/se/hsean17_sigoki.ogg"
    show end3 h1 with dissolve #700

    l "Ahhh..."
    "Valkyrie squeezes my penis between her soft breasts...{w}\nAnd from behind, Cupid is pressing an arrow against my ass."
    "Their magic is holding me down, leaving me unable to resist..."
    l "W...Why are you doing this...?"
    valkyrie "Divine punishment...{w} No, you don't deserve that.{w}\nThe era of Ilias is over."
    valkyrie "But I'm still not satisfied with this.{w}\nI just want you to feel some justice from the Heavens..."
    cupid "Let me help you out.{image=note}{w}\nHehe, spin, spin!{image=note}"
    l "Ahhh!"
    "With a laugh, Cupid presses her arrow around inside my ass.{w}\nFull of some sort of aphrodisiac magic, each movement of the arrow sends waves of pleasure through my body."
    l "Haa... Ahh!"
    "The unique pleasure causes my waist to push forward, rubbing against Valkyrie's breasts.{w}\nPressing her breasts together, it's like she's trying to control an unruly pet..."
    valkyrie "Now, please come on my chest...{w}\nLet your body tremble in shame as you climax."
    l "Ahh..."
    "Despite her cold gaze, Valkyrie's breasts feel so warm...{w}\nHer soft breasts press back against my penis, crushing me in pleasure from every direction."
    l "No... I'm going to come..."
    cupid "Ahaha, you're already going to come?"
    "While laughing, Cupid continues moving her arrow gently in and out of my ass."
    l "Haa!"
    "Pressing it inside me, the tip pokes against my prostate.{w}\nAs the wave of pleasure washes over me, my waist trembles."
    cupid "Aha, I pushed the male ejaculation switch.{image=note}"

    call syasei1 
    show end3 h2 #700
    call syasei2 

    l "Haa..."
    "As soon as she pressed against me like that, I climaxed.{w}\nFrom the front, Valkyrie squeezes her breasts together on my pulsing penis, catching my semen on her chest."
    "My body trembles as I come on Valkyrie, weak from the forced orgasm."
    valkyrie "...You let out quite a lot.{w}\nBut it won't end with just this much."
    cupid "Ehehe... Let's give him a taste of orgasm hell!{image=note}"
    l "S...Sto... Ahhh!"
    "Cupid starts to attack me with her arrow like a piston.{w}\nPressing against my prostate with each thrust, it's like she's actually raping me."
    "From the front, my twitching penis rubs against Valkyrie's smooth skin as my body jumps.{w}\nThe combined sensations cause me to moan in pleasure, unable to stop myself."
    l "I...It feels too good... Ahh!"
    valkyrie "So pitiful...{w} Are men truly helpless when their penis is being teased a little?{w}\nSuch a pathetic weak point..."
    "Valkyrie's cold eyes bore into me as she squeezes me between her breasts.{w}\nHer glare makes it look like she's looking at something disgusting..."
    l "Ahh..."
    cupid "Aha, it looks like he's going to come again soon.{w}\nWhy don't you try making him come with your breasts this time, Valkyrie?"
    valkyrie "Very well... Now, please come between my breasts."
    l "Haa... Ahh..."
    "Using her hands, Valkyrie starts to rub her breasts up and down in unison.{w}\nHer soft, smooth breasts become a weapon of pure pleasure..."
    "Overwhelmed by that sweet pleasure, I let loose with desire."
    l "Ahh..."

    call syasei1 
    show end3 h3 #700
    call syasei2 

    "I explode again, my semen spurting out between her breasts.{w}\nAfter two ejaculations, her breasts are covered in semen..."
    l "Haa... Ahh..."
    cupid "Ahaha, you don't get a rest!{image=note}"
    valkyrie "It isn't going to end with just that.{w}\nWe'll make you come until you can no longer stand..."
    "The two angels don't let up at all...{w} Raped from the front and the back, I'm left screaming in ecstasy in only minutes."
    l "Hyaaa!!!"
    valkyrie "It's useless to cry out...{w}\nThis is our place, nobody else will ever come here."
    cupid "For the next five hours, you get to receive the wrath of heaven!{image=note}{w}\nNow that you know, hurry up and come again!{image=note}"
    "Valkyrie continues rubbing me with her breasts, at times pressing down hard to crush me between her softness."
    "At the same time, Cupid's arrow continues pressing against my prostate, violating me from behind."
    l "Haa... Ahhh!"
    "The assault of pleasure overwhelms me from both sides.{w}\nHelpless between them, I'm quickly led to shoot out another white flag of surrender..."
    l "Ahh... I'm going to come again..."
    cupid "Aha, that makes three.{image=note}"
    valkyrie "Already at the end of your endurance?{w}\nGo ahead and waste more semen on my breasts..."
    l "Ahhh!!!"

    call syasei1 
    show end3 h4 #700
    call syasei2 

    "I explode again with a moan, my semen shooting straight onto Valkyrie's face.{w}\nWatching me during my orgasm, Valkyrie scowls with disgust."

    stop hseanwave fadeout 1

    valkyrie "Defiling an angel's face?{w} Do you know what you're doing?{w}\nPolluting the face of an angel demands further punishment..."
    cupid "Valkyrie, let's change places...{w}\nThis time I get his penis, and you get his ass!{image=note}"
    valkyrie "Alright, let's switch."
    l "Stop this.... Haaa..."
    "Ever since then, the teasing from these two angels continues on..."

    scene bg black with Dissolve(3.0)

    "............."

    $ renpy.end_replay()
    $ persistent.hsean_end3 = 1
    $ end_go = "city56b_13"
    jump badend2


label city56b_13b:
    l "M....Maybe some other time."

    show cupid st06 at xy(X=-160) #700
    show valkyrie st03 at xy(X=80)
    with dissolve #701

    valkyrie "He's perceptive, as we thought..."
    cupid "Eeeehhh?{w} It failed?"
    l "............"
    "I knew they were plotting something.{w}\nI quickly leave the room."

    scene bg 044 with blinds
    return


label city57a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 064 with blinds2
    play music "audio/bgm/sabasa.ogg"


label city57a_main:
    call city_null 
    $ city_bg = "bg 064"
    $ city_button01 = "Boy" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Man" #604
    $ city_button05 = "Soldier" #605
    $ city_button06 = "Young Boy" #606
    $ city_button07 = "Prostitute" #607
    $ city_button08 = "Elizabeth" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Park" #613
    $ city_button14 = "Mermaid Pub Two" #614
    $ city_button15 = "Vampire Pub" #615
    $ city_button16 = "Sabasa Castle" #616
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city57a_01 
        elif result == 2:
            call city57a_02 
        elif result == 3:
            call city57a_03 
        elif result == 4:
            call city57a_04 
        elif result == 5:
            call city57a_05 
        elif result == 6:
            call city57a_06 
        elif result == 7:
            call city57a_07 
        elif result == 8:
            call city57a_08 
        elif result == 10:
            call city57a_10 
        elif result == 11:
            call city57a_11 
        elif result == 12:
            call city57a_12 
        elif result == 13:
            call city57a_13 
        elif result == 14:
            call city57a_14 
        elif result == 15:
            call city57a_15 
        elif result == 16:
            jump city57b_main0
        elif result == 19:
            return


label city57a_01:
    show mob seinen1 with dissolve #700

    boy "The lost vampires seem to have come here to settle down.{w}\nSome are scared, others are happy, and others don't mind..."
    boy "...Eh?{w} Me? Well, I don't really mind.{w}\nThe Vampire Pub?{w} I...I don't go there that often..."

    return


label city57a_02:
    show mob obasan at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman "I've been seeing Princess Sara a lot recently.{w}\nShe was even in the victory parade earlier."
    woman "She seems to be doing well."
    young_woman "And there's a certain atmosphere about her...{w}\nIt's like there's something seductive hidden behind that smile of hers..."

    return


label city57a_03:
    show mob rouzin1 with dissolve #700

    old_man "After the reconciliation with the monsters, large areas of land that were once unexplored are opening up."
    old_man "Since I've always loved hearing various legends, it has been truly incredible for me."
    old_man "Also, they're doing lots of repair and rebuilding work on the Pyramid.{w}\nApparently it's going to be open to tourists very soon."
    l "They're rebuilding it?{w}\nThat seems too bad...{w} They're important historical remains."

    hide mob
    show alice st01b
    with dissolve #700

    a "There were some people protesting, saying it should be protected as is...{w}\nBut after some research was done, it was found that the place is really dangerous, due to erosion."
    a "It was expected to collapse in a few years...{w}\nSo it was decided that it shouldn't be left to just be destroyed."
    l "Isn't Sphinx sad...?"
    a "No...{w} Sphinx was one of the major players pushing for repair and rebuilding work.{w}\nSo things have been going very smoothly."
    l "I see..."

    return


label city57a_04:
    show mob ozisan2 with dissolve #700

    man "This country accepted monsters in the past.{w}\nSo even if more vampires show up, I don't really mind."
    man "Of course, my attitude will change if they attack anyone...{w}\nBut those girls are behaving themselves."

    return


label city57a_05:
    show mob sensi1 with dissolve #700

    soldier "That last fight was something fierce!{w}\nWe lost a lot, but we managed to win."
    soldier "Even if we're all happy now that peace has come...{w} Nobody has forgotten those who lost their lives to buy it."

    return


label city57a_06:
    show mob syounen with dissolve #700

    young_boy "Don't you think the atmosphere around the princess has changed somewhat...?"

    show mob seinen1 at xy(X=-160) as mob2 with dissolve #701

    boy "Yeah, it's sort of... Erotic feeling!"

    show mob ozisan3 at xy(X=160) as mob3 with dissolve #702

    middle_aged_man "I wonder if she had some sexy encounters while studying abroad!?"

    show mob sensi1 at xy(X=-300) as mob4 with dissolve #703

    soldier "I want to study abroad too!"

    show mob rouzin1 at xy(X=300) as mob5 with dissolve #704

    old_man "No, I don't think that would help any of you..."
    young_boy "Aww come on, don't destroy our dreams!"
    l "............."
    "The young boys sure are growing up fast..."

    return


label city57a_07:
    show mob musume2 with dissolve #700

    prostitute "I prepared all this stuff for vampire-play since it got so popular!{w}\nBut what will I do if actual vampires start working as whores!?"

    return


label city57a_08:
    show elisabeth st01 with dissolve #700

    elizabeth "Hrm, hrm...{w} Direct sunlight is so bad for the skin...{w}\nShould I petition the government to put a roof over this neighborhood?"
    elizabeth "We should all sign a petition to make this place livable for us..."
    l "................"

    return


label city57a_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    old_man "Anti-vampire weapons like silver knives have been selling pretty good.{w}\nThere are still people who don't trust them, after all."
    old_man "But there are just as many buying black cloaks, to dress up as them..."

    scene bg 064 with blinds
    return


label city57a_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "\"Legend of Hero Luka\" books are on sale now!{w}\nAll of the great Hero Luka's adventures, in one book!"
    l "Err...{w} Can I buy one?"
    shopkeeper "Coming right up!"

    play sound "audio/se/kaimono.ogg"
    pause 2.0

    "As I quickly read over the book..."
    l "Luka: \"Alice, my love, I cannot bring myself to attack you!\""
    l "Alice: \"Oh, my Luka! Please fight against Ilias with me!\""
    l "...What the heck is this!?"

    hide mob
    show alice st01b
    with dissolve #700

    a "It's a rather... Strange adaptation, but at least it's still based in some sort of fact."
    a "Other than the intense over-dramatization of it, the places and outcomes are all right."
    a "It must have been written by someone who was following us every step of the way...{w}\nLook, they even have stuff written about the spirits..."
    l "Who wrote this thing?{w}\nLet's see...{w} In the preface..."
    l "\"I have been deeply involved with the Hero Luka's quest, since the start.{w}\nThis is the result of countless interviews, and hours spent watching his adventures from the side.\""
    l "...Someone I met during my quest?"
    a "They're well informed, have a lot of contacts, and seem to have been everywhere..."
    l "Good at drawing out information...{w} Overly dramatic...{w} Following me everywhere...{w}\nCould it be...?"

    show alice st04b with dissolve #700

    a ".............."
    l ".............."
    "Only one person comes to mind..."

    $ check10 = 1
    scene bg 064 with blinds
    return


label city57a_12:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "With Ilias gone, the Church is no longer necessary...{w}\nThere are many who have lost their place, and are wandering lost..."
    priest "...But why be sad about it!?{w}\nBecause you're now in the Holy Casino!!!{w}\nCome on everyone, it's CASINO FEVER TIME!"
    "The priests all over seem to be adjusting in rather different ways..."

    scene bg 064 with blinds
    return


label city57a_13:
    show mob syouzyo at xy(X=160)
    show gnome st01 at xy(X=-160) #700
    with dissolve #701

    "Gnome is in the middle of the central park.{w}\nShe's playing in the sand with a girl..."
    l "Eh?{w} You didn't go back to Safaru Desert?"
    "Compared to her vast desert, this park is nothing..."

    hide mob
    show gnome st01 at center
    with dissolve #700

    gnome "............"
    l "I see...{w} So you were lonely in the desert."
    "It seems like playing in this small park is better than being alone in that huge desert.{w}\nGnome has been alone for a while, too..."

    show gnome st01 at xy(X=-160) #700
    show mob syouzyo at xy(X=160) behind gnome
    with dissolve #701

    girl "Gnomeyyy!{w} Let's make a huge castle!"
    gnome "............"
    "Gnome and the small girl continue to play in the sand."
    "Spirit worship used to be big here.{w}\nThe spirits themselves grew with that attention..."
    "And it looks like with Ilias gone, that Spirit worship might start back up again."

    return


label city57a_14:
    scene bg 155 with blinds
    show mob mermaid1 with dissolve #700

    mermaid "The Vampire Pub is full of guests, but we're empty!{w}\nT...This is so frustrating!{w} This is the birth of a new rivalry between Mermaid and Vampire!"

    scene bg 064 with blinds
    return


label city57a_15:
    scene bg 155 with blinds
    show carmilla st01 with dissolve #700

    carmilla "Welcome to the Vampire Pub!{w}\nThe most popular part of our pub is... Of course, the Queen!"

    show carmilla st01 at xy(X=-180, Y=50) #701
    show queenvanpire st01 at xy(X=160)
    with dissolve #700

    queen_vampire "Hahaha... Of course I'm the most popular!"
    l "W...What are they doing?"

    hide carmilla
    show alice st07b at xy(X=-120) behind queenvanpire
    with dissolve #701

    a "They're enjoying this too much..."

    scene bg 064 with blinds
    return


label city57b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle2.ogg"


label city57b_main:
    call city_null 
    $ city_bg = "bg 065"

    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Chief Minister" #603
    $ city_button04 = "Grandmaster Knight" #604
    $ city_button05 = "Scholar" #605
    $ city_button06 = "Priest" #606
    $ city_button10 = "Audience Room" #610
    $ city_button11 = "Princess's Room" #611
    $ city_button12 = "Guardroom" #612
    $ city_button13 = "Dungeon" #613
    $ city_button19 = "Town" #619

    while True:
        call cmd_city 

        if result == 1:
            call city57b_01 
        elif result == 2:
            call city57b_02 
        elif result == 3:
            call city57b_03 
        elif result == 4:
            call city57b_04 
        elif result == 5:
            call city57b_05 
        elif result == 6:
            call city57b_06 
        elif result == 10:
            call city57b_10 
        elif result == 11:
            call city57b_11 
        elif result == 12:
            call city57b_12 
        elif result == 13:
            call city57b_13 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            show bg 064 with blinds2
            play music "audio/bgm/sabasa.ogg"
            jump city57a_main


label city57b_01:
    show mob daizin with dissolve #700

    minister_a "There were many who didn't agree on welcoming the vampires...{w}\nBut the King himself chimed in, and let them come."
    minister_a "If they were left to their own devices, they might bring about more harm...{w}\nInstead, they should be kept close, so an eye could be kept on them."
    a "A shrewd King..."

    return


label city57b_02:
    show mob daizin with dissolve #700

    minister_b "There haven't been any real incidents since the vampires came here to live.{w}\nBut we've been getting a ton of petitions..."
    minister_b "They want covered sidewalks, black paint for the walls, and a lot more bars...{w}\nThey're surprisingly needy."

    return


label city57b_03:
    show mob rouzin1 with dissolve #700

    chief_minister "Princess Sara has returned to Sabasa."
    chief_minister "This time of rebuilding is quite difficult for us all...{w}\nHer presence has helped to boost the citizens morale."

    return


label city57b_04:
    show mob sensi2 with dissolve #700

    grandmaster_knight "Since the final battle, we haven't had any sorties.{w}\nA bored army is a glorious thing...{w} There haven't been any discipline problems either, since everyone's too tired."

    return


label city57b_05:
    show mob gakusya with dissolve #700

    scholar "Peace has returned... I'm so happy...{w}\nOh...{w} My anemia..."
    l "You... Couldn't be..."
    scholar "No, I'm not letting the vampires suck my blood every night.{w}\nI'm not doing that, so don't worry."
    scholar "Oh... Dizzy again..."
    l "............"
    "He seems to have let them suck an awful lot..."

    return


label city57b_06:
    show mob sinkan with dissolve #700

    priest "The Castle has been accepting a lot more advisers and ministers."
    priest "Most of them are former priests.{w}\nI'm grateful I wasn't just tossed out..."
    priest "The King is thinking hard about where all of us priests will go, it seems..."

    return


label city57b_10:
    if cont == 1:
        play music "audio/bgm/castle2.ogg"

    $ cont = 0

    scene bg 045 with blinds
    show sabasa st02 with dissolve #700

    sabasa_king "You truly are amazing, Hero Luka.{w}\nThanks to your blade, Ilias was defeated, and the world saved."
    sabasa_king "I knew you were more than ordinary when I saw you.{w}\nBut to think you would not just save my daughter, but the world..."
    l "No, no... It was everyone's power together that did it."
    sabasa_king "But it was due to you that everyone stood together to begin with.{w}\nIf not for you, monsters and humans could never have come together."
    sabasa_king "Now then...{w} It's a long, cherished tradition that a Hero who does something grand marries a King's daughter.{w}\nAnd... Why... I just remembered!{w} I am a King with a daughter!"
    l "............."
    "I knew it."
    l "But what Sara wants is the most important, right?"

    show sabasa st01 with dissolve #700

    sabasa_king "If it's you, Sara would accept marriage.{w}\nIt's no falsehood, this is truly what she wants."
    l "Eh, really...?"
    "I didn't think Sara would give up on Granberia...{w}\nBut I don't think he is the type to lie."
    sabasa_king "...Now then, Hero Luka, I will ask you again.{w}\nWill you take my daughter Sara as your bride?"
    "He isn't joking, if I answer here, he is intending to act on it..."

    menu:
        "Yes{#city57b_10}":
            jump city57b_11b

        "No{#city57b_10}":
            jump city57b_10a


label city57b_10a:
    show sabasa st03 with dissolve #700

    sabasa_king "...Guard."
    "The King snaps his fingers.{w}\nResponding instantly, the sentry at the door locks it shut."
    sabasa_king "Don't worry about the doors being locked.{w}\nNow.{w} You are saying that you're not going to say yes to me asking you to take my daughter as your bride?"
    "He's trying to confuse me...{w} I need to answer this right..."

    menu:
        "Yes{#city57b_10a}":
            jump city57b_10b

        "No{#city57b_10a}":
            jump city57b_11b


label city57b_10b:
    sabasa_king "..........."
    "The King silently stands up, and moves over to the door."

    show sabasa st05 with dissolve #700

    sabasa_king "...Don't worry about why I am standing here.{w}\nNow.{w} One last time."
    sabasa_king "You're refusing to say no to me asking you to change your stance away from no to marrying my daughter Sara?"
    l "Eh!?"
    "This one's difficult...{w}\nBut I can't afford to give the wrong answer now!"

    menu:
        "Yes{#city57b_10b}":
            jump city57b_11b

        "No{#city57b_10b}":
            jump city57b_10c


label city57b_10c:
    show sabasa st02 with dissolve #700

    sabasa_king "...I see, I'll stop trying to force it, then.{w}\nBesides, you're not one to be a Princess's husband."
    sabasa_king "...Please, don't hesitate to return here.{w}\nI'd very much enjoy speaking to you at length in the future!"

    scene bg 065 with blinds
    return


label city57b_11b:
    show sabasa st02 with dissolve #700

    sabasa_king "Ah, so you are willing to accept!{w}\nChief Minister, start preparations for a ceremony at once!"

    show sabasa st02 at xy(X=-160) #701
    show mob rouzin1 at xy(X=190)
    with dissolve #700

    chief_minister "Understood, my King.{w}\nThen I will initiate Plan A."
    l "Eh...?{w} Eh...?"
    "Without time to catch my breath, things move along in a whirlwind..."

    scene bg 067 with blinds2
    play music "audio/bgm/hutuu1.ogg"

    l "H...How did this happen!?"

    show sara st11r with dissolve #700

    sara "What do you mean?{w} You agreed to marry me.{w}\nYou'd hurt me if you called it off at this point..."
    "Rushed around without a moment to think about what was happening, the ceremony was finished, and Sara and I were left alone in this room."
    "How did it get to this so fast...?"
    l "Sara, are you really okay with this?{w} Marriage and all..."

    show sara st12r with dissolve #700

    sara "Well since you already know my circumstances, I don't mind if it's you, Luka.{w}\nIf it's you, I wouldn't even mind bearing your children..."
    l "B...Bearing my children?{w}\nShouldn't you think about this a little more...?"
    "No, the one who didn't really think it through was me.{w}\nBecause of that, things happened so fast..."


label city57b_11c:
    if _in_replay:
        play music "audio/bgm/hutuu1.ogg"
        scene bg 067
        show sara st12r
        with Dissolve(1.5)

    sara "Well, we'll work out the troublesome stuff later.{w}\nWe should do it first."
    l "Eh...?{w} Do what?"
    sara "\"What\"?{w} It's our marriage night, Luka.{w}\nSo, how do you want it?"
    sara "Do you want to stick it in yourself and do it normally?{w}\nOr perhaps a little rape roleplay...?"
    l "W...What... It's too soon for this!"
    sara "Too soon?{w} It's our wedding night."
    l "B...But this is too sudden..."

    show sara st13r with dissolve #700

    sara "Ahhh geez!{w} Do you want me to rape you that badly?"

    play music "audio/bgm/ero1.ogg"

    "Sara pushes me down as I'm fretting in confusion, easily stripping me of my clothes and getting on top."

    show sara st12r with dissolve #700

    sara "Look at you, your penis is already so big...{w}\nSo you did want to be forced a little."
    l "N...No...{w} You're wrong..."
    sara "What do you mean I'm wrong?{w}\nAre you ashamed at having your bridge take the initiative on your wedding night?"
    "Sara grabs hold of my erect penis, and pushes it against her entrance.{w}{nw}"

    play hseanwave "audio/se/hsean02_innerworks_a2.ogg"
    hide sara
    show end5 h1
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nAnd drops down on top of me all at once!"
    l "Ah!"
    "My head goes pure white as soon as Sara violates me.{w}\nThe feel of her soft flesh around my penis fills my every sense."
    "The soft pressure, her wet hole... It feels incredible just being inside her."
    sara "Hehehe... Well?"
    l "Ahh... It feels good..."
    "Trembling, I lose control of myself inside of her."

    call syasei1 
    show end5 h2 #700
    call syasei2 

    "And in no time, I come inside of her, my semen spurting into that amazing hole."
    "I didn't even last ten seconds inside of her.{w}\nNo matter how I think of this, it's clearly not normal."
    l "Y...You didn't...{w} Your Succubus power?"
    sara "Ehe... You found out?{w}\nI transformed just my pussy with my Succubus powers."
    sara "Thanks to it, you felt really good, didn't you?{w}\nYou came right from insertion.{image=note}"
    l "I...I knew it...{w}\nAh..."
    "She slowly squeezes down on my penis with her vagina...{w} Infused with Succubus powers, it's enough to leave me completely at her mercy."
    sara "Hehe... It's incredible, isn't it?{w}\nJust think... You can use this amazing pussy whenever you want."
    l "Uwaaa!"
    "Sara squeezes my penis with her Succubus vagina, leaving me writhing in unbearable pleasure under her."
    sara "Hey... It's our wedding night, it's not nice to make the bride do all the work.{w}\nYou need to give it your best too, Luka."
    l "N...No way..."
    sara "Come on, you aren't going to let your bride ride you like this, are you?{w}\nShouldn't your pride as a man make you go on some sort of counterattack?"
    l "Guh..."
    "I try to push up into her from below... But if I move inside her, I know I'd come right away."
    "After moving less than an inch, I stop with a gasp of pleasure.{w}\nSeeing that, Sara looks down with a look of triumph on her face."
    sara "Ahaha, is that it?{w}\nThat's so pathetic, Dear."
    sara "Then I guess I'll attack you from this side.{image=note}"
    l "Ahhh!!!"
    "Squeezing me tight inside her, Sara starts to violently move her waist."
    "My sensitive penis slips easily in and out of her wet vagina, the folds and creases inside rubbing and pulling against my tip with each completed cycle."
    "Feeling far too good, the desire to ejaculate again overtakes me in only seconds."
    l "Ahh... I'm going to come again!"
    sara "What, already giving up?{w}\nCome on, I just moved a little bit and you're already surrendering to me?{image=note}"
    sara "Then I'll finish you off inside...{w}\nNow come!{image=note}"
    l "Ahhh!!!"
    "Pressing down and pulling me deep inside, Sara squeezes on my penis, forcing the feel of her warm flesh into me."

    call syasei1 
    show end5 h3 #700
    call syasei2 

    l "Ahhh..."
    "Unable to take it, my body spasms in ecstasy as I explode inside her again.{w}\nSara stares into my eyes as I come inside her, and giggles."
    sara "Hey, how does it feel?{w}\nRight after saving the world, losing to a Princess's pussy?"
    "Sara grin and laughs as she looks at me.{w}\nEven if she's teasing me like this, I'm in no state to resist her."
    sara "If I beat you down with my pussy some more, you won't be able to resist me..."

    play hseanwave "audio/se/hsean16_kyuin3.ogg"

    "Smiling in triumph, Sara suddenly changes the stimulation in her vagina...{w}\nHer vagina starts to contract and release, pulling on me penis like it was a mouth sucking on me."
    "Pleasure runs like electricity through my body, along with an incredible sense of weakness.{w}\nThis couldn't be..."
    l "Are you...{w} Sucking my energy...?"
    sara "You've been completely dominated by your wife on our wedding night, Dear.{w}\nYour punishment is to be Energy Drained.{image=note}"
    l "Ahh... Don't suck...{w}\nDon't suck on me..."
    "Sara starts using a Succubus skill to drain my energy through her vagina...{w}\nThe incredible contractions pull and suck on my penis."
    "As would be expected, the pleasure from it is incredible, too...{w}\nIt feels like my soul itself could be sucked out into Sara's vagina."
    sara "Aha... I wonder if this is the first time the husband's energy has been sucked on the wedding night?"
    sara "We'll need to be careful, I wouldn't want to accidentally suck you to death when we're newlyweds...{image=note}"
    l "Stop... Wait...{w}\nAhh...!"

    call syasei1 
    show end5 h4 #700
    call syasei2 

    "As she sucks on my energy, I reach a powerful orgasm.{w}\nExploding inside her, I shoot both my energy and my semen into Sara."
    l "Ahhh!!!"
    sara "Ahaha, a swift orgasm.{w}\nLook at you screaming and squirming.{image=note}"
    l "But... It feels too good!"
    "Sara's Succubus vagina continues to suck and pull on me...{w}\nIf it's like this, it's just like I'm prey to a Succubus..."
    l "N...No... Ahh!"

    call syasei1 
    call syasei2 

    sara "Ahaha, you came again.{w}\nBut I can make you come a lot more than this..."
    sara "How does it feel, Luka?{w}\nBeing treated like this on your wedding night?{image=note}"
    l "Haa... Stop...{w}\nAhh!"

    call syasei1 
    call syasei2 

    sara "Ah... Luka's semen is so delicious..."
    sara "I'm such a horrible bride...{w}\nI'm just feeding on all of this semen that should be used for making a child..."
    l "Ah...ah..."

    call syasei1 
    call syasei2 

    "Since then, she brought me to orgasm after orgasm...{w}\nUntil I fainted, Sara sucked up my energy with her Succubus vagina."
    "Once I woke up, she started again...{w} It kept going until nothing more came out."
    "Of course she didn't take my life, but I was left unable to even move a finger."

    stop hseanwave fadeout 1

    sara "Ah... It's already morning.{w}\nPlease treat me well from now on, Dear.{image=note}"
    l "Ah...uh..."
    "After being preyed on by my new Succubus wife our first night together, I'm completely drained of energy..."

    scene bg black with Dissolve(3.0)

    "................"

    $ renpy.end_replay()
    $ persistent.hsean_end5 = 1
    $ end_go = "city57b_10"
    jump badend2


label city57b_11:
    scene bg 067 with blinds
    show sara st12r with dissolve #700

    sara "Luka...{w}\nTo think you would save the world...{w} That's amazing!"
    sara "I came back to Sabasa to do what only I can, too.{w}\nI've been involving myself in national affairs to boost the spirits of the citizens."
    sara "Since I can use Transfer Magic myself now, I can go back to the Monster Lord's Castle whenever I want.{w}\nI'm still working as one of Granberia's maids in secret, after all..."
    l "I see...{w} Still following her..."
    sara "If you live in the Monster Lord's Castle too, we'll see each other every so often."
    l "Why would I live there...?"

    show sara st14r with dissolve #700

    sara "Eh?{w} You didn't marry the Monster Lord?"
    l "............."
    "Where did she hear that...?"

    scene bg 065 with blinds
    return


label city57b_12:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    soldier_a "Since the final battle, there hasn't been any calls for the army.{w}\nWe prepared a lot of anti-vampire weapons, but none have been needed."
    soldier_b "But the relationship between the vampires and mermaids seems to be getting tense..."
    soldier_b "They've been getting in a lot fights, but nothing serious...{w} Yet."

    scene bg 065 with blinds
    return


label city57b_13:
    scene bg 066 with blinds
    show sphinx st01 with dissolve #700

    "For some reason, Sphinx is in the dungeon!"
    l "W...Why are you in prison!?"
    sphinx "The Sabasa King has put forward reconstruction efforts on the Pyramid.{w}\nI'm living here while that is going on."
    sphinx "The King was against me living here, saying it doesn't suit one of the Founders of the Royal Line.{w}\nBut the surrounding stone walls just make me feel so at ease."
    l "I...is that how it is...?"

    show alice st01b at xy(X=-140) #700
    show sphinx st01 at xy(X=160)
    with dissolve #701

    a "The Pyramid is being rebuilt to honor the relationship between humans and monsters.{w}\nIt will also be open to the public, and is expected to draw a lot of tourists."
    sphinx "Tourists...{w} The world is changing so fast.{w}\nBut it's better than keeping everyone away, and letting it slowly fall in to ruin."
    sphinx "I'll think up a lot of good riddles to ask the tourists...{w}\nOf course, I won't eat them if they get it wrong."

    show sphinx st02 at xy(X=160) with dissolve #701

    sphinx "...I might just squeeze out a shot or two of semen..."
    l "..........."
    "You'll get a lot who answer wrong on purpose, then..."

    scene bg 065 with blinds
    return


label city58_main0:
    $ BG = "bg 020"
    play sound "audio/se/habataki.ogg"
    scene bg 072 with blinds2
    play music "audio/bgm/mura2.ogg"


label city58_main:
    call city_null 
    $ city_bg = "bg 072"
    $ city_button01 = "Young Man" #601
    $ city_button02 = "Village Girl" #602
    $ city_button03 = "Lucia's Younger Sister" #603
    $ city_button04 = "Woman" #604
    $ city_button05 = "Old Man" #605
    $ city_button06 = "Scholar" #606
    $ city_button07 = "Young Boy" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Lord's Mansion" #612
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city58_01 
        elif result == 2:
            call city58_02 
        elif result == 3:
            call city58_03 
        elif result == 4:
            call city58_04 
        elif result == 5:
            call city58_05 
        elif result == 6:
            call city58_06 
        elif result == 7:
            call city58_07 
        elif result == 10:
            call city58_10 
        elif result == 11:
            call city58_11 
        elif result == 12:
            call city58_12 
        elif result == 19:
            return


label city58_01:
    show mob seinen1 with dissolve #700

    young_man "My wife now has one of those worm arms, but it hasn't been an issue.{w}\nIt's actually pretty convenient, since she can reach things that are high up and from far away."
    young_man "And at night...{w}\nHehehehe.{w} Hehehe..."

    return


label city58_02:
    show wormv st11 with dissolve #700

    village_girl "I haven't had any problems living here.{w}\nEveryone is very nice to me."
    village_girl "And Lucia came back, too!{w} I'm really happy..."

    return


label city58_03:
    show mob zyosei with dissolve #700

    lucias_younger_sister "My older sister is staying in the mansion.{w}\nShe's heading up the disposal of all the dangerous chemicals and equipment."
    lucias_younger_sister "Also, it looks like Lily left a lot of notes about her experiments...{w}\nSome may be useful, so we've been organizing her notes."

    return


label city58_04:
    show mob obasan with dissolve #700

    woman "Even during that heated battle, those girls gave it their all."
    woman "They didn't let a single invader step foot into our village.{w}\nIf it wasn't for them, this village would have been destroyed..."

    return


label city58_05:
    show mob rouzin1 with dissolve #700

    old_man "At last, peace has come to our village.{w}\nNow that we're not hostile to outsiders, we've had a lot more tourists coming through."
    old_man "I've even heard of fairies hanging around...{w}\nI hope this peace lasts forever."

    return


label city58_06:
    show mob gakusya with dissolve #700

    magic_scholar "We must not let Lily's research simply be destroyed.{w}\nEven though the methods were horrible, some good could still come of them."
    magic_scholar "Besides... Burning knowledge when it already exists is just a waste.{w}\nI hope the people here realize that..."

    return


label city58_07:
    show mob syounen with dissolve #700

    young_boy "I found a really old map, with the ancient name of this village!{w}\nIt was called Witch's Village."
    young_boy "I wonder just when the Witch became the bad person...?"

    return


label city58_10:
    scene bg 151 with blinds
    show mob ozisan2 with dissolve #700

    shopkeeper "I want that Iron Maiden...{w}\nNo, no, not for wicked uses."
    l "Whether you use it, or you use it on someone else...{w}\nI don't think it has any use besides wicked things."
    shopkeeper "Well, it could be a decoration...{w}\nI'm going to talk with Lucia about it."
    l "........."

    scene bg 072 with blinds
    return


label city58_11:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "There are still many who are coming to repent.{w}\nThe Church is still greatly needed in this village."
    "Every village is different..."

    scene bg 072 with blinds
    return


label city58_12:
    scene bg 074 with blinds
    show lusia st21 with dissolve #700

    lucia "...Thank you for your help.{w}\nI was finally able to find a place to call my home."
    lucia "I have many bad memories of this mansion, but I can't keep running away from my past."
    l "I see..."
    lucia "By the way...{w} Did Promestein say something before she died?"
    l "Well... Just that humans would become her successor."
    "Since Lucia was one of her subordinates, I decide to tell her.{w}\nThat she found something in common with humans, and thought that they would take up her same path."

    show lusia st23 with dissolve #700

    lucia "Hmm...{w} So Promestein thought that..."
    l "...But I don't think she's right.{w}\nHumans won't walk the same horrible path as Promestein did."
    lucia "...Are you sure?{w}\nThe scope wasn't as big, but Lily was doing the same things."
    lucia "And me... I was already walking down that path, before you pulled me off of it."
    l "T...That may be true..."
    lucia "...Human experimentation is a complete evil, for you?"
    l "Of course!{w} That's simply inhumane!"
    lucia "But all of those medicinal herbs, used all over the world to save lives.{w}\nThere are 252 that have been credited by the Hospital Guild."
    lucia "How many humans do you think became sick, or even died, testing out various herbs and drugs?"
    lucia "Dosage, side effects, new treatments...{w} Lots of humans suffered in the hope of leading to a better future."
    lucia "It could be argued that is simply human experimentation."
    l "T...that may be so..."
    lucia "Before new drugs or medicines can be offered, they must be tested on a small group."
    lucia "If not, then the risk of death from overdose or unknown side effects would be too high."
    lucia "Since it's for human use, animal experiments won't give accurate test results.{w}\nIf there was a single wrong step, it would be a catastrophe."
    l ".........."
    lucia "As our knowledge progresses, this will become a bigger and bigger issue.{w}\nSo I'll ask again...{w} Is human experimentation a complete evil?"
    l "Err...{w} If they ask permission...{w} And they are rewarded and paid for it..."

    show alice st01b at xy(X=-140) #700
    show lusia st23 at xy(X=170)
    with dissolve #701

    a "But in that case, almost every time the people putting themselves at risk will be the poor, or those socially vulnerable."
    a "You're just encouraging experimentation on the poor or outcast."
    l "This...{w} Isn't a simple problem..."

    hide alice
    show lusia st21 at center
    with dissolve #700

    lucia "Yes...{w} The point of the question wasn't to try to put you on the spot."
    lucia "I don't believe it's evil, or good.{w}\nIt's just the final phase of the scientific method."
    lucia "And as the Monster Lord said, when you try to make it seem less evil, it ends up preying on the poor."
    lucia "The conflict between politics, science, social stigma, and money...{w}\nIt's the dangerous path researchers like us are trying to walk."
    l "..........."
    lucia "That reminds me...{w} Promestein was always saying that technology is neither blasphemy nor human arrogance."
    lucia "It's just the application of the knowledge that has been gained, to make lives a little easier or happier."

    show alice st01b at xy(X=-140) #700
    show lusia st23 at xy(X=170)
    with dissolve #701

    a "It's stupid to dismiss advancement just because it's going to change how things have been done.{w}\nI don't deny that, either."
    lucia "Promestein was in too big of a hurry."
    lucia "Problems that should have been taken slowly, handed down to generation after generation...{w}\nShe tried to do it all alone, all at once."
    lucia "As a result, she kept banging against walls all by herself, proving her own theories wrong.{w}\nSince she never stopped to apply what she learned, she wasn't able to make anyone happy with her research."
    lucia "Just continuing to research, all alone..."
    l "..............."

    scene bg 072 with blinds
    return


label city59a_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 082 with blinds2
    play music "audio/bgm/city1.ogg"


label city59a_main:
    call city_null 
    $ city_bg = "bg 082"
    $ city_button01 = "Man" #601
    $ city_button02 = "Boy" #602
    $ city_button03 = "Town Girl" #603
    $ city_button04 = "Young Woman" #604
    $ city_button05 = "Noblewoman" #605
    $ city_button06 = "Soldier" #606
    $ city_button07 = "Harpy" #607
    $ city_button08 = "Slime Girl" #608
    $ city_button09 = "Knightroid" #609
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Minotauros's Milk Shop" #612
    $ city_button13 = "Spider Girl's Sewing Shop" #613
    $ city_button14 = "Alraune's Florist Shop" #614
    $ city_button15 = "Church" #615
    $ city_button16 = "Colosseum" #616
    $ city_button17 = "Grand Noah Castle" #617
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city59a_01 
        elif result == 2:
            call city59a_02 
        elif result == 3:
            call city59a_03 
        elif result == 4:
            call city59a_04 
        elif result == 5:
            call city59a_05 
        elif result == 6:
            call city59a_06 
        elif result == 7:
            call city59a_07 
        elif result == 8:
            call city59a_08 
        elif result == 9:
            call city59a_09 
        elif result == 10:
            call city59a_10 
        elif result == 11:
            call city59a_11 
        elif result == 12:
            call city59a_12 
        elif result == 13:
            call city59a_13 
        elif result == 14:
            call city59a_14 
        elif result == 15:
            call city59a_15 
        elif result == 16:
            call city59a_17 
        elif result == 17:
            jump city59b_main0
        elif result == 19:
            return


label city59a_01:
    show mob ozisan1 with dissolve #700

    man "A lot of those machine monsters came here to settle down.{w}\nAnd, as I thought, they're really popular in the Colosseum."
    man "It's sort of odd... But nobody seems to mind.{w}\nGrand Noah is just that kind of place..."

    return


label city59a_02:
    show mob seinen1 with dissolve #700

    boy "There are a lot of new residents... But they're all sexy monster women.{w}\nI get to...{w} Every day... Hehehe..."
    l "............"

    return


label city59a_03:
    show mob musume1 with dissolve #700

    town_girl "Just like I thought, you really saved the world!{w}\nAs a Hero birthed from our Colosseum, I'm super proud!"
    l "............"
    "I only participated once..."

    return


label city59a_04:
    show lamianloid st03 at xy(X=160)
    show mob musume2 at xy(X=15) #700
    with dissolve #701

    young_woman "Damn you!{w} This is for stealing my boyfriend!"
    lamiaroid "H...Hey!{w} What are you doing!?"
    "A young woman is punching Lamiaroid...{w}\nShould I stop her?"

    return


label city59a_05:
    show mob madam with dissolve #700

    noblewoman "I'm so happy the Colosseum fights have resumed.{w}\nA lot of those \"Roids\" are joining in, too."
    noblewoman "They're teasing men in ways I've never seen before!{w}\nIt's so interesting... I can't help myself!"

    return


label city59a_06:
    show mob sensi1 with dissolve #700

    soldier "Even when fighting in the Colosseum, I just keep getting raped.{w}\nAs a soldier, I'm pathetic."
    soldier "It's so frustrating...{w} But it feels so good..."
    l ".............."

    return


label city59a_07:
    show mob hapy2 with dissolve #700

    fishing_loving_harpy "Now that the world is peaceful, I can enjoy a life of fishing.{w}\nI've been fishing in the river every day since then..."
    fishing_loving_harpy "Those mermaids are as annoying as always, but one day, I'll catch them with my hook, too!"

    return


label city59a_08:
    show mob mamono4 at xy(X=244, Y=80) with dissolve #700

    slime_girl "Purupurupuru... Peace is so fun!{image=note}{w}\nHey, let's purupurupuru together!{image=note}"

    return


label city59a_09:
    show knightloid st01 at xy(X=-50) with dissolve #700

    knightroid "I'm a fighter in the Colosseum now, too.{w}\nIt would be an honor to face you on the arena floor again."

    show knightloid st05 at xy(X=-50) with dissolve #700

    knightroid "...If Arc-En-Ciel had lived to see this day, she may have found a reason to live on, as I have..."

    return


label city59a_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "Weapon shops all over are in trouble, but I'm fine.{w}\nAs long as the Colosseum is running, I'll never be short on customers!"

    scene bg 082 with blinds
    return


label city59a_11:
    scene bg 151 with blinds
    show mob mamono1 at xy(X=100, Y=-100) with dissolve #700

    elf "In the middle of the final battle, that crazy Minotauros tried to hug me!{w}\nThere were so many enemies, though, that she gave up."
    elf "I guess she's mad that I hit her with an arrow earlier..."

    scene bg 082 with blinds
    return


label city59a_12:
    show mob mamono5 with dissolve #700

    minotauros_milkmonster "I tried to catch that Elf to get her back for her damn arrow...{w}\nBut it was too chaotic, so I couldn't grab her."
    minotauros_milkmonster "That damn Elf...{w}\nOne day I'm going to squeeze your milk, and sell it!!!"

    return


label city59a_13:
    show mob mamono3 with dissolve #700

    spider_girl_shop_owner "The sign is a lie.{w} It's really Tarantula Girl's Sewing Shop."
    spider_girl_shop_owner "I heard all of the spider monsters moved to Grangold..."
    spider_girl_shop_owner "My business isn't going too well, so I was thinking about moving there myself.{w}\nBut I have a fondness for Grand Noah that I don't think I can get over..."

    return


label city59a_14:
    show mob mamono2 at xy(X=147) with dissolve #700

    florist_shop_owner "The flowers are all growing well!{w}\nWould you like to buy one?{w} They are very delicious!{image=note}"

    return


label city59a_15:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "Welcome to the Church of the Machines!{w} I am Father Machine!{w}\nCome on everybody, let's do the screw!"
    l "Pandering to the sudden popularity of machine monsters..."

    scene bg 082 with blinds
    return


label city59a_17:
    if cont == 1:
        play music "audio/bgm/city1.ogg"
        scene bg 082 with blinds

    $ cont = 0
    show alma_elma st11 with dissolve #700

    alma "Oh, it's Luka.{w}\nWhat a nice coincidence, to meet at the Colosseum..."
    "Alma Elma is waiting in front of the Colosseum entrance.{w}\nI get the feeling she was waiting for me..."
    l "Alma...{w}\nDid you come here to participate?"

    show alma_elma st13 with dissolve #700

    alma "Of course...{w} I can't have fun unless I'm moving my body.{w}\nI love this kind of play, after all.{image=note}"
    alma "Why don't you enter the next tournament, Luka?{w}\nI'm going to participate too...{w} I'll give you extra special attention.{image=note}"
    l "Hmm... Well..."
    "It might be nice to have a friendly, low effort match to move my body again after that coma.{w}\nBut I have a strange sense of unease about this..."

    menu:
        "Participate{#city59a_17}":
            jump city59_17a

        "Refuse{#city59a_17}":
            jump city59_17b


label city59_17a:
    alma "Hehe, I'm so happy...{w}\nAlright then, I'll take good care of you in the arena...{image=note}"
    l "..........."
    "I casually agreed to it... But it should be okay, right?"

    scene bg 084 with blinds2
    play music "audio/bgm/colosseum.ogg"

    announcer "And now, the final match!{w}\nThe surprise challenger, the Hero who saved the world...{w} Hero Luka!"
    "Without much of a surprise, I made it to the finals.{w}\nAnd my opponent is, of course..."
    announcer "Here she comes!{w} The undefeated fighter, one of the Four Heavenly Knights...{w}\nThe walking obscenity... Kyuba!"

    show alma_elma st13 with dissolve #700

    alma "Hiiii, Luka-boy...{image=note}"
    l "I won't lose!"
    "I've beaten her once before, I won't lose this time!"

    scene bg 084 with blinds2


label city59_17c:
    if _in_replay:
        scene bg 084 with Dissolve(1.5)

    play music "audio/bgm/ero1.ogg"
    show alma_elma hg1 with dissolve #700

    l "Ahhg..."
    "Alma's tail coils around me, and seals off my movements in no time.{w}\nBefore I can blink, she has already stripped off my clothes."
    l "W...Why...?"
    alma "Just like Granberry...{w}\nYou honest, straightforward sword types are easy to read."
    alma "And most importantly...{w} You never show your true power when your opponent isn't a bad person.{image=note}"
    l "Ahh...!"
    spectator_a "No man has ever escaped from Kyuba's tail.{w}\nThe fight's over...!"
    spectator_b "I wonder how she's going to deal with that boy...?{image=note}"
    "Now that the match has been decided, the audience is starting to get excited.{w}\nAll of the women are on the edge of their seats, all eager to see me get raped..."

    voice "audio/voice/alma_elma5_01.ogg"

    alma "You look like a mess, Luka...{w}\nNow, it's time for some fun.{image=note}"

    voice "audio/voice/alma_elma5_02.ogg"

    alma "I'll use my breasts today to finish you o-f-f.{image=note}"
    "Before I can say anything, Alma presses her breasts against my groin!"
    l "Ahh!"

    play hseanwave "audio/se/hsean17_sigoki.ogg"

    "My penis sinks into her huge chest, her right and left breasts pressing against me.{w}\nSo warm and soft...{w} Just the feeling of being between them makes me moan."
    l "Ahh... That feels good..."

    voice "audio/voice/alma_elma5_03.ogg"

    alma "I'll take you to paradise now.{w}\nHoora... Energy Drain...{image=note}"
    l "Haa... Ah..."
    "As Alma's body lightly glows light, my brain explodes in pure pleasure.{w}\nI feel my energy leaking out through my penis, with shocks of ecstasy filling my body in its place."
    "It's like she took away my sanity all at once."
    l "Ah...ah..."

    voice "audio/voice/alma_elma5_04.ogg"

    alma "Ara, what's wrong?{w}\nYour face is looking really embarrassing, you know?"
    "Alma giggles as she looks at my loose face."
    spectator_a "Woah... Incredible...{w}\nShe can even use energy drain with her breasts...!"
    spectator_b "Make him come!"
    spectator_c "Why isn't that boy resisting?{w}\nHe's just letting her do whatever she wants...{w} That's kind of sad."
    "Some of the audience is clapping, impressed at Alma's skills.{w}\nOthers are laughing at me, and making comments to their friends..."

    voice "audio/voice/alma_elma5_05.ogg"

    alma "Hehe, the customers seem pleased, too.{w}\nNow to tease your penis with my breasts..."

    voice "audio/voice/alma_elma5_06.ogg"

    alma "When you can't endure it any longer, just Pew Pew it out!{image=note}"
    "Putting her hands on her breasts, Alma starts to increase the pressure on my penis.{w}\nWhile still draining my energy, her soft breasts slowly start to crush my penis between them."
    l "Ahh...ah..."

    voice "audio/voice/alma_elma5_07.ogg"

    alma "Arara... Did it feel so good, you lost the ability to speak?{w}\nAre you that happy at being the prey of a Succubus's breasts?{image=note}"
    "Alma starts to slowly shake her breasts side to side.{w}\nHer soft skin rubs against my penis, sandwiched between them."
    "Crushed in her softness, I feel my patience quickly nearing its limit.{w}\nA tingling sensation rises up through my waist as Alma continues slowly moving her breasts."
    l "Haa... I'm going to come..."

    voice "audio/voice/alma_elma5_08.ogg"

    alma "Then... This is the finishing blow.{w}\nHoora... Come all over my breasts.{image=note}"
    "Alma presses even harder on her breasts, squeezing me between them.{w}\nMy head goes pure white as I lose all control."
    l "Ahhh..."

    call syasei1 
    show alma_elma hg2 #700
    call syasei2 

    "Tasting incredible pleasure, I explode on her breasts, covering them with semen.{w}\nMy mind melts in pure ecstasy as my orgasm combines with the energy drain, leaving me completely weak."
    "The audience all starts to laugh and clap as my semen shoots onto her."

    voice "audio/voice/alma_elma5_09.ogg"

    alma "Aha... Luka's shameful juice got my breasts all dirty.{w}\nIt felt great, didn't it? Hehe.{image=note}"

    voice "audio/voice/alma_elma5_10.ogg"

    alma "What a shameful child... Leaking all over my breasts.{w}\nA child like that needs lots of punishment...{image=note}"
    l "Ah..."
    "Alma starts to bounce her breasts up and down quickly, tossing my penis back and forth between them.{w}\nWith the energy drain still going, I'm left gasping and moaning helplessly under her."

    voice "audio/voice/alma_elma5_11.ogg"

    alma "Horahorahora...{w}\nIf I do it like this, the premature ejaculator Luka will come in no time, won't he?"
    l "Haa... Ah..."
    "Alma starts to raise and lower her breasts in an alternating motion."
    "It's like she's kneading my penis between her huge breasts...{w}\nAs my sensitive penis rubs against her soft skin, my body relaxes in pure bliss."
    "Without resisting at all, I allow the sensations to flood my body."
    l "Ahh... I'm going to come again..."

    voice "audio/voice/alma_elma5_12.ogg"

    alma "Hehe... Already can't take it?{w}\nThen... Come!{image=note}"
    "Alma presses her breasts together hard around my penis.{w}\nAt the same time, she drastically increases the power of the energy drain."
    "Assaulted by pure ecstasy, I quickly blow far over the edge."
    l "Ahhhh!!!"

    call syasei1 
    show alma_elma hg3 #700
    call syasei2 

    "I explode between her breasts, the powerful orgasm causing my semen to even land on her face.{w}\nDue to the energy drain, the amount of semen is far higher than normal."
    "In addition, I feel far weaker than I should..."

    stop hseanwave fadeout 1

    l "Ahh..."
    "The sweet feeling of release fills my body, still wound by Alma's tail.{w}\nI feel myself about to faint as Alma looks at me and laughs."

    voice "audio/voice/alma_elma5_13.ogg"

    alma "Hahaha... Since there was an audience, I got a little carried away.{w}\nIf I squeezed any more, Luka-boy would die."

    voice "audio/voice/alma_elma5_14.ogg"

    alma "We'll stop here for today, with my overwhelming victory.{image=note}"
    "Alma giggles, announcing my complete defeat to her.{w}\nAll of the audience members who watched start to laugh and tease me, too."
    "Feeling completely humiliated, I slowly drift into unconsciousness..."

    scene bg black with Dissolve(3.0)

    ".............."

    $ renpy.end_replay()
    $ persistent.hsean_shitenno2 = 1
    $ end_go = "city59a_17"
    jump badend2


label city59_17b:
    alma "Oh my, are you scared?{w}\nAre you worried your white stuff will shoot straight up in front of a huge audience?"
    alma "Hehe...{w} Come back if you change your mind.{image=note}"

    return


label city59b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle3.ogg"


label city59b_main:
    call city_null 
    $ city_bg = "bg 065"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Castle Guard" #603
    $ city_button04 = "Guard Captain" #604
    $ city_button05 = "Dullahan" #605
    $ city_button06 = "Cerberus" #606
    $ city_button10 = "Audience Hall" #610
    $ city_button11 = "Guard Room" #611
    $ city_button19 = "Town" #619

    while True:
        call cmd_city 

        if result == 1:
            call city59b_01 
        elif result == 2:
            call city59b_02 
        elif result == 3:
            call city59b_03 
        elif result == 4:
            call city59b_04 
        elif result == 5:
            call city59b_05 
        elif result == 6:
            call city59b_06 
        elif result == 10:
            call city59b_10 
        elif result == 11:
            call city59b_11 
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            show bg 082 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city59a_main


label city59b_01:
    show mob daizin with dissolve #700

    minister_a "The Colosseum is an even bigger success than ever!{w}\nBoth participants and audience numbers are hitting new records every day!"
    minister_a "Our economy is going through a huge boom, and our military power is increasing every day!{w}\nThe Colosseum truly is the treasure of Grand Noah!"

    return


label city59b_02:
    show mob daizin with dissolve #700

    minister_b "Those \"Roid\" monsters are living here, but nobody seems to mind."
    minister_b "But their maintenance is quite troublesome...{w}\nThey seem to randomly break down every so often."
    minister_b "We may need to hire some Magic Science Engineers from Grangold.{w}\nPerhaps we could start a university of our own here..."

    return


label city59b_03:
    show mob heisi1 with dissolve #700

    castle_guard "Some of the machine monsters enlisted to guard the Queen.{w}\nThanks to that, our duties have been reduced a little."
    castle_guard "It's nice that they're strong...{w} But it's a little distressing to be replaced like that..."

    return


label city59b_04:
    show mob heisi1 with dissolve #700

    guard_captain "The last battle was so intense, there were lots of lives lost.{w}\nBut their sacrifice gave us all peace..."
    guard_captain "...But I can't sit around depressed forever.{w}\nI'll enjoy the peace they gave me until the very end!"

    return


label city59b_05:
    show dullahan st01 at xy(X=228) with dissolve #700

    dullahan "I asked Salamander to take me as an apprentice, but she knocked me out, and sent me away."
    dullahan "I tried Undine next, but she was too strong.{w}\nMy sword just went through her formless body..."
    dullahan "After that, I gave Gnome a shot.{w} But she easily buried me in sand...{w}\nFinally I decided on Sylph.{w} But then she just blew me out of the forest with her wind..."
    l "...That sounds rough."

    return


label city59b_06:
    show cerberus st01 with dissolve #700

    cerberus "The machine monsters have started fightin' in the Colosseum.{w}\nIt has gotten a lot harder to progress now..."
    cerberus "Of course, I ain't depressed over...{w} Rather, I'm happy.{w}\nMore opponents to train against and grow stronger!"

    return


label city59b_10:
    scene bg 045 with blinds
    show grandnoa st02 with dissolve #700

    grand_noah_queen "Luka, a true Hero who saved the world.{w}\nYour legendary feats will become tales handed down through history."
    grand_noah_queen "Fighting on with faith and courage, no fear of death as you enact your justice...{w}\nI'm honored to live in the same era as you."
    l "No, no...{w} I didn't do anything that great..."
    grand_noah_queen "It is the duty of the Hero to create peace.{w} And it's the duty of us administrators to maintain that peace."
    grand_noah_queen "We will protect the peace you have created.{w}\nAll of the Kings and Queens of the world are united in that desire."
    grand_noah_queen "So...{w} You have borne more than any one person should have had to.{w}\nHow are you holding up?"
    l "...Thank you for your concern."
    l "But I wasn't alone.{w}\nAlice was always by my side..."
    grand_noah_queen "I see...{w} A good companion."
    grand_noah_queen "You are always welcome here in Grand Noah.{w}\nNext time you visit, I will prepare a grand welcome."

    scene bg 065 with blinds
    return


label city59b_11:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_a "There are tons of powerful monsters all over the castle...{w}\nBut since they're guards, it just means we're all the more safer."
    guard_a "There were some drunk people rioting after the Colosseum match, but the machine monsters arrested him only seconds after the report came in."
    guard_b "Thanks to them, our role is slowly going away..."

    scene bg 065 with blinds
    return


label city60_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 088 with blinds2
    play music "audio/bgm/yamatai.ogg"


label city60_main:
    call city_null 
    $ city_bg = "bg 088"
    $ city_button01 = "Old Man" #601
    $ city_button02 = "Boy" #602
    $ city_button03 = "Village Girl" #603
    $ city_button04 = "Akaname Girls" #604
    $ city_button05 = "Miko Lamia" #605
    $ city_button10 = "Cat Shrine" #610
    $ city_button11 = "Shirohebi's Shrine" #611
    $ city_button12 = "Kitsune Shrine" #612
    $ city_button13 = "Meeting Hall" #613
    $ city_button14 = "Yamata no Orochi" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city60_01 
        elif result == 2:
            call city60_02 
        elif result == 3:
            call city60_03 
        elif result == 4:
            call city60_04 
        elif result == 5:
            call city60_05 
        elif result == 10:
            call city60_10 
        elif result == 11:
            call city60_11 
        elif result == 12:
            call city60_12 
        elif result == 13:
            call city60_13 
        elif result == 14:
            call city60_14 
        elif result == 19:
            return


label city60_01:
    show mob rouzin1 with dissolve #700

    old_man "We've gotten a lot of new villagers recently.{w}\nThese old bones are getting excited!"
    old_man "I'm even feeling some sensation return down there!"

    return


label city60_02:
    show mob seinen1 with dissolve #700

    boy "Those Akaname Girls are kinda sexy...{w}\nAll you have to do is ask and... Hehehehe..."

    return


label city60_03:
    show mob musume1 with dissolve #700

    village_girl "The other day, I applied to train with the Kunoichi Elves.{w}\nWhen I come back...{w} I'll take all of the men with my ninja sex techniques!"

    return


label city60_04:
    show akaname st01 with dissolve #700

    akaname_a "We get to lick so much every day!{w}\nThis village is wonderful!{image=note}"
    akaname_b "We licked that boy just now until he fainted.{w}\nHe came twenty times first, though. Hehe."
    akaname_c "I think it was his first time reaching orgasm, though...{w}\nHe was so shy when he asked us!{w} I hope he isn't traumatized..."

    return


label city60_05:
    show mikolamia st01 with dissolve #700

    miko_lamia "I attacked the villagers out of lust earlier...{w}\nThankfully nobody lost their lives."
    miko_lamia "They are pardoning my crimes, and allowing me to live here.{w}\nI swear upon my heart that I'll never attack people again."
    l "That's great, keep it up!"

    show mikolamia st03 with dissolve #700

    miko_lamia "But everyone keeps coming here, asking me to tease them like I did before.{w}\nThey seem to really want me to keep doing it..."
    l "..............."

    return


label city60_10:
    scene bg 159 with blinds

    if ivent01 < 1:
        call city_noiv 
        scene bg 088 with blinds
        jump city60_main

    show gob st01 at xy(X=-200) #700
    show nekomata st04 at xy(X=200, Y=60)
    with dissolve #701

    goblin_girl "Ah, Luka!"
    nekomata "...Nya!{image=note}"
    "At the Cat Shrine, Nekomata and Goblin Girl are eating some fish together.{w}\nThey seem to be good friends now."
    l "You look like you're doing well, Goblin Girl.{w}\nAnd how about you, Nekomata?"
    nekomata "Nya.{image=note}"
    "Seeing her work so hard makes me want to work hard too, for some reason."
    goblin_girl "I've been running all over the place the last month.{w}\nI wonder if I've been here in Yamatai or Iliasburg more..."
    goblin_girl "But one day, I want to travel the world with my three old buddies!"
    nekomata "...Unya!"
    goblin_girl "You want to go, too?{w}\nAlright, we'll all go together then!"

    scene bg 088 with blinds
    return


label city60_11:
    scene bg 159 with blinds
    show sirohebisama st01 at xy(X=170)
    show sirohebi st11 at xy(X=-170) #700
    with dissolve #701

    shirohebi_the_older "S...Sorry... For being a...{w} Bother...{w}\nHmph!"
    shirohebi_the_younger "She's being a little tsundere about it, but she swore to never do anything bad again.{w}\nThis is all thanks to you, as well."
    shirohebi_the_older "Hmph.{w} There's no \"dere\"."
    l "..........."
    "It looks like she won't harm anyone now.{w}\nBut maybe I should keep an eye on this village for a while..."

    scene bg 088 with blinds
    return


label city60_12:
    scene bg 159 with blinds
    play music "audio/bgm/tamamo.ogg"
    show tamamo st04 with dissolve #700

    t "Oh, if it isn't Luka!{w}\nDid you come to worship at the Fox Shrine?{w} That's admirable of you."
    l "No, that's not why...{w}\n...Well, whatever."
    "I throw some money into the shrine, and clap like the villagers do.{w}\nAfter I do that, Tamamo looks at me, and starts to excitedly hop back and forth."

    show tamamo st01 with dissolve #700

    t "...By the way, Luka.{w} Will you become my lover?"
    l "Ehh!?"

    show tamamo st02 with dissolve #700

    t "It's not a bad thing.{w}\nYou'll just attend to me every night.{w} It's a life of nothing but pleasure."
    l "............."

    menu:
        "I will{#city60_12}":
            jump city60_12a

        "I won't{#city60_12}":
            jump city60_12b


label city60_12a:
    if _in_replay:
        play music "audio/bgm/tamamo.ogg"
        scene bg 159
        show tamamo st02
        with Dissolve(1.5)

    "I casually agree after only a second, without giving it serious thought."
    t "Hehe, I'm glad.{w}\nNow, let's return to the Monster Lord's Castle..."
    "Thus, I'm invited back to Tamamo's room..."

    play sound "audio/se/asioto2.ogg"
    scene bg 176 with blinds2
    play music "audio/bgm/ero1.ogg"
    show tamamo st52 with dissolve #700

    tamamo_no_mae "Hehe, I haven't had a meal in this body for a long time.{w}\nNow, please give me a lot..."
    l "Wa...Wait a second!{w}\nThis isn't what you..."
    l "Hyaa!"
    "Tamamo looms over me, filled with an incredible power.{w}\nMy body instinctively knows not to cross her..."
    "The one in front of me is an existence of pure dark magic.{w}\nIf I have sex with her, my soul might not survive..."

    voice "audio/voice/tamamo3_01.ogg"

    tamamo_no_mae "Hehe... Don't be afraid.{w}\nBeing violated by me is a thing of pure joy."

    voice "audio/voice/tamamo3_02.ogg"

    tamamo_no_mae "Sticking themselves into me, wanting to come out everything they have...{w}\nMany men have given up their souls for that."
    l "Ahh..."
    "A body that numerous men sold their souls just to taste... Right in front of me..."

    voice "audio/voice/tamamo3_03.ogg"

    tamamo_no_mae "Hoore, leave everything to me.{w}\nI'll take great care in leading you to paradise..."
    l "W...Wait... Ah!"

    show tamamo hd01 with dissolve #700

    "Her fluffy tails all reach out, and seal off my movements.{w}\nLike that, Tamamo bends forward, and presses her ass against me."
    "Sitting on top of me, Tamamo grabs hold of my penis, and guides it to her own entrance."

    voice "audio/voice/tamamo3_04.ogg"

    tamamo_no_mae "Hoore... Your penis will be swallowed up by my vagina.{w}\nAnd your semen will all be drawn out... Hehehe."

    voice "audio/voice/tamamo3_05.ogg"

    tamamo_no_mae "Are you ready?{w}\nThen here I go..."
    l "Ahhh!!!"

    play hseanwave "audio/se/hsean05_innerworks_a5.ogg"
    show tamamo hd02 with dissolve #700

    "With that, Tamamo drops her waist, bringing me inside her.{w}\nJust from insertion itself, a soul crushing ecstasy explodes inside me."
    "Her soft, wet flesh wraps around my penis, slowly tightening around me."
    "An entrance to a paradise of the flesh, that could blow away the sanity of any man."
    l "Ah... Haaa..."

    voice "audio/voice/tamamo3_06.ogg"

    tamamo_no_mae "Hehe... In ecstasy at once?{w}\nYou are tasting my body after all, so there's no helping it I suppose..."
    l "Haa..."
    "My mind is taken over by her tight flesh, making it feel like I could melt away inside her...{w}\nI'm already lost for words."

    voice "audio/voice/tamamo3_07.ogg"

    tamamo_no_mae "At this rate, it will be over in no time...{w}\nBut I want to play with you a little more..."

    voice "audio/voice/tamamo3_08.ogg"

    tamamo_no_mae "Hehe... Now then, I'll move some.{w}\nGo ahead and writhe under me a little..."

    show tamamo hd03 with dissolve #700

    "Tamamo starts to slowly move her body, back and forth.{w}\nThe slow, controlled movements lead me to even higher heights of pleasure."
    "Her slow, up and down movements let me feel every inch of her small vagina, the wet walls pulling and rubbing against me with each cycle."
    l "Ahh... Haa..."

    voice "audio/voice/tamamo3_09.ogg"

    tamamo_no_mae "Well... Is it good?{w}\nYou can't take anymore, I see."

    voice "audio/voice/tamamo3_10.ogg"

    tamamo_no_mae "Hehe... You can go ahead and come inside me.{w}\nHoore... I'll finish you with my waist movements..."
    "Tamamo's movements get slightly faster as she leads me to a finish...{w}\nResponding to her, my body arches back as I reach a climax inside her."
    l "Ah... Ahhh!"

    call syasei1 
    show tamamo hd04 #700
    call syasei2 

    "My body trembles as I come inside her, the ecstasy from release filling every inch of my body."

    show tamamo hd05 with dissolve #700
    voice "audio/voice/tamamo3_11.ogg"

    tamamo_no_mae "Hehe... The first shot was drawn out quite easily.{w}\nYou're coming inside me, you know?"
    l "Ahh..."
    "The feeling of coming inside Tamamo...{w}\nIt was as if my soul itself was drawn into her."
    "The overwhelming pleasure leaves me completely defenseless under her.{w}\nWith a grin, Tamamo continues..."

    show tamamo hd06 with dissolve #700
    voice "audio/voice/tamamo3_12.ogg"

    tamamo_no_mae "Horehore, you don't have time to rest.{w}\nHoora... Release more..."
    l "Ah!{w} Ahhh!"

    play hseanwave "audio/se/hsean09_innerworks_b1.ogg"

    "Tamamo starts to move much faster, completely dominating me with her intense movements."
    "She stops at random times, only to squeeze me incredibly tight...{w}\nThe intense riding leaves me in a corner in no time."
    l "Haa... Stop... I'm going to come...!"
    "Even though I came only a few moments ago, I can already feel another climax coming.{w}\nTamamo's skilled movements don't allow her partner to have a rest..."

    voice "audio/voice/tamamo3_13.ogg"

    tamamo_no_mae "Hehe... Complaining, just when I put a little effort into it?{w}\nHoora... I'll finish you off at once!"
    "Tamamo presses down, bringing me deep inside her.{w}\nAfter I'm all the way in, she starts to turn side to side, sending me crashing over the edge."
    l "Ahhh!"

    call syasei1 
    show tamamo hd07 #700
    call syasei2 

    "With a moan, I explode inside her, my pulsing penis shooting spurts of semen inside her tight vagina."
    l "Ahh..."
    "A feeling of weakness floods through me.{w}\nEven though I only came twice, it felt like so much more..."
    "She must be absorbing a huge amount of energy with each orgasm."

    show tamamo hd08 with dissolve #700
    voice "audio/voice/tamamo3_14.ogg"

    tamamo_no_mae "Hehe, not yet...{w}\nI'll squeeze out every drop of semen from your roots."
    l "Ahh... I can't do any more...{w}\nAhhh...!"
    "Tamamo's movements get even faster, riding on top of me, and squeezing me inside her tight insides."

    voice "audio/voice/tamamo3_15.ogg"

    tamamo_no_mae "Hoore... I'll make it super tight...{w}\nCan you endure this? Hehe... You can't, can you?"
    l "Hyaa..."
    "My lower body tingles in ecstasy, as I let out a moan of pleasure.{w}\nI know it will be dangerous if I come more..."
    "But there's no way to fight against my desire to come inside her."

    call syasei1 
    show tamamo hd09 #700
    call syasei2 

    l "Ahhh..."
    "My semen shoots into Tamamo's body, my energy almost all taken by her devilish vagina."
    l "Ah...ah..."
    "I'm completely limp beneath her.{w}\nEven though I came three times, the lasting pleasure and weakness makes it feel like thirty."

    stop hseanwave fadeout 1
    show tamamo hd10 with dissolve #700
    voice "audio/voice/tamamo3_16.ogg"

    tamamo_no_mae "Hehe... Even though I went easy on you, you're already finished.{w}\nWell, I sucked up enough to be satisfied."
    "With a flushed face of satisfaction and joy, Tamamo looks down at me."
    l "Ah...uh..."

    voice "audio/voice/tamamo3_17.ogg"

    tamamo_no_mae "I'm going to show you a lot of love, every night.{w}\nYou'll keep attending me through the night from now on..."
    l "Ah... Uh..."
    "With Tamamo still on top of me, I lose consciousness..."

    scene bg black with Dissolve(3.0)

    "..........."

    $ renpy.end_replay()
    $ persistent.hsean_shitenno3 = 1
    $ end_go = "city60_12"
    jump badend2


label city60_12b:
    show tamamo st03 with dissolve #700

    t "Hrrm... That's too bad...{w}\nWhat should I do about my empty stomach...?"
    t "Ever since I awoke my sealed body during that battle, my stomach has been growling the entire time..."
    l "..........."
    "I'm sorry...{w} But I just can't do that."

    play music "audio/bgm/yamatai.ogg"
    scene bg 088 with blinds
    return


label city60_13:
    scene bg 090 with blinds
    show mob seinen1 at xy(X=-240) with dissolve #700

    youngster_a "Peace is great..."

    show mob seinen1 at xy(X=-80) as mob2 with dissolve #701

    youngster_b "The beauty of nature...{w} That's the true draw of Yamatai Village."

    show mob seinen1 at xy(X=80) as mob3 with dissolve #702

    youngster_c "Meetings and farewells...{w} Treasured memories, one and all."

    show mob seinen1 at xy(X=240) as mob4 with dissolve #703

    youngster_d "It is better to be the head of a chicken, than the ass of an ox."
    l "W...What happened!?"

    hide mob2
    hide mob3
    hide mob4
    show mob rouzin1 at center
    with dissolve #700

    village_chief "They've entered Wise Man Mode.{w}\nThey'll go back to being manwhores in about an hour."

    scene bg 088 with blinds
    return


label city60_14:
    scene bg 092 with blinds
    show yamatanooroti st01 with dissolve #700

    yamata_no_orochi "They unsealed me on the condition that I don't do anything bad again.{w}\nOf course, I intend to keep my promise."
    yamata_no_orochi "Besides... Those young men keep coming here in secret anyway.{w}\nI never go hungry for semen... Hahaha."
    l "..........."
    "As I thought, they're still coming here.{w}\nI guess she has no reason to break her promise, anyway."

    show yamatanooroti st02 with dissolve #700

    yamata_no_orochi "You should stop by whenever you want, too.{w}\nI'll lick that body of yours as much as you want..."
    l "No thank you..."

    scene bg 088 with blinds
    return


label city61_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 095 with blinds2
    play music "audio/bgm/plansect.ogg"


label city61_main:
    call city_null 
    $ city_bg = "bg 095"
    $ city_button01 = "Alraune" #601
    $ city_button02 = "Dryad" #602
    $ city_button03 = "Alra Arum" #603
    $ city_button04 = "Alra Rooty" #604
    $ city_button05 = "Canaan Sisters" #605
    $ city_button06 = "Alra Priestess" #606
    $ city_button07 = "Experimental Organism S-2" #607
    $ city_button08 = "Merchant" #608
    $ city_button10 = "Tarantula Girl" #610
    $ city_button11 = "Mosquito Girl" #611
    $ city_button12 = "Caterpillar Girl" #612
    $ city_button13 = "Silkworm Girl" #613
    $ city_button14 = "Queen Bee" #614
    $ city_button15 = "Slime Girl" #615
    $ city_button16 = "Undine" #616
    $ city_button17 = "Erubetie" #617
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city61_01 
        elif result == 2:
            call city61_02 
        elif result == 3:
            call city61_03 
        elif result == 4:
            call city61_04 
        elif result == 5:
            call city61_05 
        elif result == 6:
            call city61_06 
        elif result == 7:
            call city61_07 
        elif result == 8:
            call city61_08 
        elif result == 10:
            call city61_10 
        elif result == 11:
            call city61_11 
        elif result == 12:
            call city61_12 
        elif result == 13:
            call city61_13 
        elif result == 14:
            call city61_14 
        elif result == 15:
            call city61_15 
        elif result == 16:
            call city61_16 
        elif result == 17:
            call city61_17 
        elif result == 19:
            return


label city61_01:
    show mob mamono2 at xy(X=157) with dissolve #700

    alraune "Plansect is now a village of peace and harmony.{w}\nPriestess became the new Plant Queen... But she's co-ruling along with Queen Bee."
    alraune "Nobody has complained about anything so far."

    return


label city61_02:
    show dryad st01 with dissolve #700

    dryad "I beat up a whole group of angels.{w}\nI tied them up with my ivy, then I started teasing them..."

    show dryad st11 with dissolve #700

    dryad "And now we've accepted some as immigrants into the village.{w}\nI hope we can be good friends..."

    show dryad st01 with dissolve #700

    dryad "Eh?{w} Did it reverse?{w}\nEh?{w} Ehhh...?"

    return


label city61_03:
    show a_alm st01 at xy(X=200) with dissolve #700

    alra_arum "Even though we're at peace, I'm still looking for some excitement...{w}\nLuckily, I found something perfect."
    alra_arum "I think I'll take Mosquito with me next time I head to Grand Noah..."

    return


label city61_04:
    show a_looty st03 at xy(X=220, Y=130) with dissolve #700

    alra_rooty "I haven't been hanging out with Arum as much recently...{w}\nIt looks like she's hanging around with Mosquito a lot more instead."
    alra_rooty "Mmmm... So lonely..."

    return


label city61_05:
    show a_mash st01 at xy(X=260, Y=70)  with dissolve #700

    alra_mash "Here's some water..."
    "A mushroom type monster is watering the three carnivorous Canaan sisters..."
    "Listening in, I can hear heartbreak in their voices."

    hide a_mash with dissolve

    raffia "Sisters... Shouldn't we just give up already?{w}\nLet's just accept the peace, and let them undo our seals for good..."
    diana "Hmph...{w} I'll never accept peace.{w}\nA mercenary is meaningless in times of peace!"
    dorothy "I'm so sick of water...{w}\nEven a beetle is fine, I just want to eat!"

    return


label city61_06:
    show a_emp st22 at xy(X=140, Y=30) with dissolve #700

    alra_priestess "When our villages unite, we can overcome any hardship.{w}\nBut without the sacrifices of those who came before us, we never would have been able to attain this peace..."
    alra_priestess "And I can never thank you enough.{w}\nYour actions and sacrifice for us have brought this wonderful coexistence."
    alra_priestess "With our two races mixing for the first time, I'm sure there will be tensions...{w}\nBut we'll make it work, with everything we have."

    show alice st01b at xy(X=-120) #700
    show a_emp st22 at xy(X=300, Y=30)
    with dissolve #701

    a "Good...{w}\nNever forget your responsibility as Queen Alraune."
    alra_priestess "Yes, leave it to me.{w}\nHero, Monster Lord...{w} I wish eternal happiness for your family."
    l "...Eh?"

    show a_emp st23 at xy(X=300, Y=30) with dissolve #701

    alra_priestess "...Eh?"

    return


label city61_07:
    show c_s2 st01 with dissolve #700

    s_2 "Awawa..."
    l "Oh?{w} One of Promestein's experimental monsters is here, too?"
    s_2 "Someone named Micaela helped me...{w} They said I could live here."
    s_2 "This huge forest is so much more fun than a confined laboratory!{w}\nAwawawawawa..."

    return


label city61_08:
    show mob ozisan3 with dissolve #700

    traveling_merchant "I stopped by here a while ago.{w}\nIt was terrifying back then...{w} But it's a nice place now."
    traveling_merchant "They buy a lot of various female clothing here.{w}\nAnd the honey and medicines I purchase here are great sellers in other towns."
    traveling_merchant "There are also rumors going around about merchants opening up permanent stores here, now that it's peaceful.{w}\nAll of us merchants are hoping to get a piece of the action."

    show alice st01b at xy(X=-120) #700
    show mob ozisan3 at xy(X=160)
    with dissolve #701

    a "No matter the times, merchants truly are the harbinger of cultural interchange.{w}\nPerhaps one of the best ways to bring about coexistence is to promote trade..."

    return


label city61_10:
    show mob mamono3 with dissolve #700

    tarantula_girl "Peace has finally come.{w}\nI hope it continues forever..."
    tarantula_girl "But why do people always get so afraid when they see how I look?{w}\nTarantulas love peace...{w} We're actually really friendly..."

    return


label city61_11:
    show mosquito st01 at xy(X=220, Y=70) with dissolve #700

    mosquito_girl "We suck blood just like vampires...{w} Why don't we have that cool, mysterious image like they do!?"
    mosquito_girl "The noble monster of the summer... Mosquitoes!"

    return


label city61_12:
    show imomusi st01 at xy(X=180, Y=130) with dissolve #700

    caterpillar_girl "I wonder when I'll grow bigger...{w}\nI won't... Stay like this forever, right!?"
    caterpillar_girl "Maybe it's because I'm so picky about food...{w}\nStarting tomorrow, I'm eating vegetables as well as fruit!"

    return


label city61_13:
    show kaiko st01 with dissolve #700

    silkworm_girl "I can't forget that pleasure from when my energy was sucked..."
    silkworm_girl "Even if I make my own cocoon of ecstasy, it isn't enough.{w}\nAfter all, it's just boring masturbation..."

    return


label city61_14:
    show queenbee st01 with dissolve #700

    queen_bee "I was freed the day before that battle.{w}\nNo matter what, I don't want to be sealed again..."
    queen_bee "I reconciled with the Plant race, and am coming around to this way of life.{w}\nI'll cooperate with Alra Priestess, and govern this village in the name of peace."

    return


label city61_15:
    show slime st01 at xy(X=244, Y=80) with dissolve #700

    slime_girl "I'm living here while Undine's Spring is being purified!{image=note}"
    slime_girl "Yaaay, yaaaay!{image=note}{w}\nI love plants and bugs!{image=note}"

    return


label city61_16:
    show undine st01 with dissolve #700

    undine "..........."
    l "You're living here too, Undine?"
    undine "This village is the closest one to my spring, after all.{w}\nI go there every day to purify it..."
    undine "But it will take years before it's done."
    l "I see..."

    show undine st02 with dissolve #700

    undine "But there's no need to rush it.{w}\nPerhaps there is no need for a refuge for Slime monsters now..."

    hide undine
    show slime st01 at xy(X=-50, Y=80) #700
    show mob syouzyo at xy(X=-40) behind slime #701
    show imomusi st02 at xy(X=470, Y=130)
    with dissolve #702

    slime_girl "Yaaay!{w} Yaaaay!"
    girl "Ahaha, run away!!!"
    caterpillar_girl "Wait, wait!"
    l "...Maybe not."

    hide mob
    hide slime
    hide imomusi
    show undine st02
    with dissolve #700

    "A secluded, sacred refuge for the slimes...{w}\nThey may no longer need to hide away in places like that now."
    "No, we have to work hard to make sure they never need a place to hide."
    undine "I'm expecting great things, Luka..."
    l "Yeah, leave it to me!"

    return


label city61_17:
    if cont == 1:
        play music "audio/bgm/plansect.ogg"
        scene bg 095 with blinds

    $ cont = 0
    show erubetie st01 at xy(X=225) with dissolve #700

    e "You came all the way here just to see me, Luka..."
    l "No, err...{w} I came to see how everything is going."
    "Erubetie seems to be staying in Plansect Village as well.{w}\nAlong with some of the other powerful slimes, she's working to purify Undine's Spring."
    e "...I'd like to have a nice, long talk with you.{w}\nAbout wet, sticky coexistence between humans and monsters."
    e "So... Won't you please come in the Monster Lord's Castle to me?"

    menu:
        "Absolutely!{#city61_17}":
            jump city61_17a

        "Maybe next time...{#city61_17}":
            jump city61_17b


label city61_17a:
    if _in_replay:
        play music "audio/bgm/plansect.ogg"
        scene bg 095
        show erubetie st01
        with Dissolve(1.5)

    e "Then let's return to the Monster Lord's Castle.{w}\nWe'll talk about coexistence all over the place..."
    l "Ah, yeah..."

    hide erubetie with dissolve

    "Thus, I return to the Monster Lord's Castle with Erubetie..."

    play sound "audio/se/asioto2.ogg"
    scene bg 177 with blinds2
    play music "audio/bgm/ero1.ogg"
    play hseanwave "audio/se/hsean18_slime1.ogg"
    show erubetie hc1 with dissolve #700

    l "Ahh..."

    voice "audio/voice/erubetie3_01.ogg"

    e_a "Hora, you're happy, right?"

    voice "audio/voice/erubetie3_02.ogg"

    e_b "Hehe, does it feel good?{w}\nPlease melt away more..."
    "Erubetie's split bodies are all around me, caressing me at the same time.{w}\nTheir slimy bodies all stroke and rub against me at once, my body loose in comfort."
    l "Ahh... T...This isn't...{w}\nCoexistence with the Slime race..."

    voice "audio/voice/erubetie3_03.ogg"

    e_a "I'll share the fluids I squeeze from you with my brethren as nourishment."

    voice "audio/voice/erubetie3_04.ogg"

    e_b "This act is itself coexistence with us...{w}\nIf you understand, then just obediently drown in the pleasure..."
    l "Ahhh..."
    "Every part of my body is submerged inside Erubetie's own.{w}\nLaying on a bed of her body, her slime massages and strokes my defenseless flesh."

    voice "audio/voice/erubetie3_05.ogg"

    e_a "Hoora... You love it when these are toyed with, don't you?"
    "The divided Erubetie's fingers start to gently rub and tickle my nipples."
    "Covered in her slimy touch, my back arches up at the sudden, powerful stimulation."

    voice "audio/voice/erubetie3_06.ogg"

    e_b "You like this too, don't you?"
    "Tongues of slime run up and down my middle and sides, the creepy sensation of her warm body causing my body to tingle."
    l "Ahh..."
    "As one of her slime tongues circles around my bellybutton, I let out a sigh of ecstasy.{w}\nAs Erubetie caresses my entire body, it feels like I'm immersed in heaven..."

    voice "audio/voice/erubetie3_07.ogg"

    e_c "But what you like the most..."

    voice "audio/voice/erubetie3_08.ogg"

    e_d "Is here, isn't it?"
    "Multiple Erubetie's reach out to my groin, their interlocking hands grabbing hold of my penis."

    voice "audio/voice/erubetie3_09.ogg"

    e_c "You wanted me to play with your penis like this, didn't you?"

    voice "audio/voice/erubetie3_10.ogg"

    e_d "I'll keep teasing it until you come a lot..."
    l "Ah... Haa..."
    "Almost like floating in a dream, I entrust myself to Erubetie's touch."
    "Her slimy fingers run up and down my shaft, as her formless hands stroke my glans and neck..."
    "Her hands sometimes form together, creating a mass of slime.{w}\nThe incredible pleasure is enough to melt away my body and mind in ecstasy."
    l "Ahh... I'm going to come..."

    voice "audio/voice/erubetie3_11.ogg"

    e_a "Fast, aren't you? Then please come..."

    voice "audio/voice/erubetie3_12.ogg"

    e_b "Please let loose inside my slime..."
    "The pressure of her slime suddenly increases, feeling like she's completely surrounding me.{w}\nAs her slime moves up and down, I reach a powerful climax."
    l "Ahhh!!!"

    call syasei1 
    show erubetie hc2 #700
    call syasei2 

    "Immersed in ecstasy, I explode inside her hands, my semen mixing with her warm slime."
    l "Haa..."

    voice "audio/voice/erubetie3_13.ogg"

    e_a "You leaked out so much...{w}\nBut it's too early to be satisfied."

    voice "audio/voice/erubetie3_14.ogg"

    e_b "Hoora... Have a taste of Heaven...{w}\nI'll squeeze your semen until you're empty."

    play hseanwave "audio/se/hsean19_slime2.ogg"

    "Her slime hands continue to gently stroke me, right after my orgasm.{w}\nMy mind goes blank in ecstasy, as the intense stimulation on my sensitive penis is too much to take."
    l "Haaa... That feels good..."

    voice "audio/voice/erubetie3_15.ogg"

    e_c "...What a bad child, letting out a voice like that..."

    voice "audio/voice/erubetie3_16.ogg"

    e_d "Just become my captive...{w}\nContinue to live in our embrace forever..."
    "Her warm, sticky slime continues to caress my body and penis.{w}\nMy body and soul are both wearing away in the pleasure..."
    "I want to do nothing more than give myself over to this paradise."
    l "Ahh... Haa..."

    voice "audio/voice/erubetie3_17.ogg"

    e_a "Hehe... How easy...{w}\nYou're already loose in the head..."

    voice "audio/voice/erubetie3_18.ogg"

    e_b "Alright then, I'll lead you to paradise again...{w}\nLeak out more of that proof that you belong to us..."
    "Her slime compresses right underneath the neck of my penis, rubbing against an extra sensitive part."
    "A shock of pleasure races up my spine, as my every sense is focused on that stimulation..."
    l "Ahh..."

    call syasei1 
    show erubetie hc3 #700
    call syasei2 

    "And just like Erubetie wanted, I come into her slime hands."
    l "Ahh..."
    "Laying down on the ground, I'm squeezed one time after another by the countless copies of Erubetie."
    "I can't imagine there being anything that feels better than this...{w}\nBut before being completely drowned, a final moment of clarity hits me."
    "...Is this really alright?{w}\nCan I really drown in Erubetie's body here...?"

    voice "audio/voice/erubetie3_19.ogg"

    e_a "Hoora... Let's bind our friendship together even more.{w}\nYou're happy... My brethren and I are happy... It's beneficial for everyone."

    voice "audio/voice/erubetie3_20.ogg"

    e_b "Offer up your own body, and show us the path to coexistence...{w}\nAnd we must show plenty of our thanks, too..."
    l "Ahh... This wasn't what I...{w}\nHaaa..."
    "My words of refusal are meaningless before this ecstasy.{w}\nMy thoughts mix and get confused, as the pleasure takes over my very mind."
    "I don't care about anything else...{w}\nI just want to taste this paradise forever."

    voice "audio/voice/erubetie3_21.ogg"

    e_a "Horahora... Offer up more proof of our friendship..."

    voice "audio/voice/erubetie3_22.ogg"

    e_b "Don't resist... I'll even squeeze it out by force..."
    l "Ahh...ahh..."

    call syasei1 
    show erubetie hc4 #700
    call syasei2 

    "After being forced to come over and over, my body and soul come to belong to Erubetie.{w}\nEvery day will be spent squeezed and caressed by her split bodies..."

    voice "audio/voice/erubetie3_23.ogg"

    e_a "Hoora... Let out more..."

    voice "audio/voice/erubetie3_24.ogg"

    e_b "If this is coexistence, it isn't bad at all..."
    l "Ah...ah..."

    call syasei1 
    call syasei2 

    "I'll be able to live in this paradise of pleasure forever...{w}\nIt's a life of happiness for me...{w} And for Erubetie as well, I'm sure."

    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)

    "..........."

    $ renpy.end_replay()
    $ persistent.hsean_shitenno4 = 1
    $ end_go = "city61_17"
    jump badend2


label city61_17b:
    e "I see... That's too bad...{w}\nTruly a pity...{w}\n............."
    "Erubetie intensely stares at me..."

    return


label city62_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 100 with blinds2
    play music "audio/bgm/city1.ogg"


label city62_main:
    call city_null 
    $ city_bg = "bg 100"

    $ city_button01 = "Boy" #601
    $ city_button02 = "Young Woman" #602
    $ city_button03 = "Man" #603
    $ city_button04 = "Ant Girl" #604
    $ city_button05 = "Mud Golem Girl" #605
    $ city_button06 = "Tsuchigumo" #606
    $ city_button07 = "Salamander" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Magic Science Laboratory" #612
    $ city_button13 = "Grangold Castle" #613
    $ city_button14 = "Meeting Hall" #614
    $ city_button15 = "Parade Grounds" #615
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city62_01 
        elif result == 2:
            call city62_02 
        elif result == 3:
            call city62_03 
        elif result == 4:
            call city62_04 
        elif result == 5:
            call city62_05 
        elif result == 6:
            call city62_06 
        elif result == 7:
            call city62_07 
        elif result == 10:
            call city62_10 
        elif result == 11:
            call city62_11 
        elif result == 12:
            call city62_12 
        elif result == 13:
            call city62_13 
        elif result == 14:
            call city62_14 
        elif result == 15:
            call city62_15 
        elif result == 19:
            return


label city62_01:
    show mob seinen1 with dissolve #700

    boy "Grangold has been peaceful since that final battle.{w}\nBut those spider monsters all decided to live here..."
    boy "The Ant Girls are keeping a close eye on them, so it should be all right.{w}\nBut I can't help but feel a little anxious..."

    return


label city62_02:
    show mob musume2 at xy(X=15) #700
    show ant st01 at xy(X=160)
    with dissolve #701

    young_woman "Awww geez!{w} I'M GOING TO MARRY AN ANT GIRL THEN!{w}\nHey, Ant Girl!{w} Let's get married!!!"
    ant_girl "STAY BACK..."
    "She seems to have become really desperate..."

    return


label city62_03:
    show mob ozisan2 with dissolve #700

    man "I won't forget the Ant Girls who saved me.{w}\nI won't ever hate monsters again."

    return


label city62_04:
    show ant st01 with dissolve #700

    ant_girl "MORE SPIDERWEBS..."

    show ant st01 at xy(X=-240) #700
    show mob kumo2 at xy(X=120) behind ant
    with dissolve #701

    spider_girl "Ahhh!{w} Stop destroying my home!"
    "It seems like the Ant Girls work has only increased..."

    return


label city62_05:
    show madgolem st01 with dissolve #700

    mud_golem_girl "REPAIRS STILL ONGOING...{w}\nIT'S DANGEROUS, PLEASE STEP BACK."
    "So the castle is still being repaired?"

    return


label city62_06:
    show tutigumo st01 with dissolve #700

    tsuchigumo "I'll live quietly in this town..."
    tsuchigumo "I won't eat humans or monsters anymore.{w}\nI'll just suck  some bodily fluids every so often..."
    l "............."
    "Is this really alright...?"

    return


label city62_07:
    show salamander st03 with dissolve #700

    salamander "After Heinrich defeated Alipheese the Eighth, he marched on the Heavens without saying anything to us."
    salamander "I know he didn't want us caught up in it...{w} But can you understand the frustration of someone left behind?"
    l "W...Well..."
    salamander "I know you were thinking you would die in the end, too.{w}\nBut you wanted to keep us in the dark, sparing our feelings."
    salamander "You damn Heroes keep undervaluing your own lives!{w}\nIf you keep doing that stuff, you'll just piss everyone off!"
    l "That... I'm sorry..."
    salamander "If you didn't use every ounce of your power, you wouldn't have been able to defeat Ilias.{w} I know that."
    salamander "But why didn't you consult with us first?{w}\nWe were fighting together...{w} If you told us of the risks, we could have been there for you."
    l "I... Didn't want to worry you..."

    show alice st03b at xy(X=-120) #700
    show salamander st03 at xy(X=170)
    with dissolve #701

    a "Then you did the same thing as Heinrich, you idiot!{w}\nYou aren't allowed to shoulder these burdens alone!"
    l "A...Alice too...?{w}\nS...Sorry..."
    salamander "I'll be watching you closely from now on.{w}\nYou're a danger to yourself..."
    l "..........."
    "She's angry..."

    return


label city62_10:
    scene bg 151 with blinds
    show ant st01 at xy(X=160)
    show mob ozisan1 at xy(X=-160) #700
    with dissolve #701

    ant_girl "ALL WEAPONS ARE HALF PRICE."
    shopkeeper "Sales have dried up ever since that final battle.{w}\nThese Ant Girls can do anything, but it's tough for me to change jobs..."

    scene bg 100 with blinds
    return


label city62_11:
    scene bg 156 with blinds
    show ant st01 with dissolve #700

    ant_girl "I SHALL PRAY FOR YOU..."
    l "Ah... Thanks..."
    ant_girl "PRAY..."
    "Even after Ilias is gone, this Ant Girl seems to want to keep managing this Church."

    scene bg 100 with blinds
    return


label city62_12:
    scene bg 161 with blinds
    play music "audio/bgm/kiki2.ogg"

    "In the basement of the Magic Science Laboratory...{w}{nw}"

    show kumonomiko st03
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nIt's the sealed Spider Princess!"
    spider_princess "How frustrating...{w}\nTo think that I would be a prisoner of humans..."
    l "They sealed the Spider Princess here?"

    show alice st01b at xy(X=-120) #700
    show kumonomiko st23 at left
    with dissolve #701

    a "We've set it up so the science engineers here are maintaining her seal.{w}\nIf it ever breaks, the Four Heavenly Knights have orders to all rush here."
    a "She is too dangerous to let loose.{w}\nHumans and monsters will both take responsibility for her imprisonment."
    spider_princess "Curse you, Monster Lord!{w}\nEven if the world goes soft, my nature won't change!"
    spider_princess "Our appetite is what makes us monsters!{w}\nYou cannot be a true Monster Lord if you turn away from that truth!"

    show alice st04b at xy(X=-120) with dissolve #700

    a "...This talk of appetite is making me hungry.{w}\nLuka, let's go eat some Baumkuchen at that food stand we saw earlier."
    spider_princess "Gah, wait!{w}\nI wasn't done speaking!"

    show alice st01b at xy(X=-120) with dissolve #700

    a "Our appetite may be part of what makes us monsters, but that isn't the only thing."
    a "I'll give you some Baumkuchen to try at some point in the future.{w}\n...Now let's go, Luka."
    l "Yes..."

    play music "audio/bgm/city1.ogg"

    scene bg 100 with blinds
    return


label city62_13:
    l "The road is closed from here."
    "Looks like the castle is still being repaired."

    return


label city62_14:
    scene bg 105 with blinds
    show grangold st04 with dissolve #700

    grangold_king "First, as King, I thank you on behalf of all citizens.{w}\nThanks to your efforts, the world has been saved."
    grangold_king "And from me as a person... I thank you again.{w}\nIf not for your intervention, I would never have realized my own inadequacy."
    grangold_king "I would have continued abusing the Ant Girls with no guilt...{w}\nTreating them like mere tools..."
    l "But you understand the Ant Girls now."

    show grangold st03 with dissolve #700

    grangold_king "Yes... I have.{w}\nAnd so have most of the citizens here."
    grangold_king "But the same can't be said of the entire world.{w}\nAs King, I want to get rid of any prejudices in the world."
    grangold_king "Because without understanding, there can't be coexistence.{w}\nAs long as humans treat monsters as strange \"others\"... We'll be apart."
    l "Yes, you're right."
    "If I never met Alice, I would have held that same prejudice..."

    show grangold st04 with dissolve #700

    grangold_king "To that end, I'm putting forth some new cultural exchange initiatives.{w}\nI want the change to be true and gradual, and not something simply forced by law and police."
    grangold_king "Our first event will be the \"Human and Monster Matchmaking Service\".{w}\nWe handed out lots of flyers, and a lot of excitement is building for it."

    show grangold st04 at xy(X=-160) #700
    show queenant st01 at xy(X=160) behind grangold
    with dissolve #701

    queen_ant "I've gathered together all of the monster participants."
    queen_ant "Of course, it won't be nothing but Ant Girls."
    queen_ant "We've also got Lamias, Scyllas, and Alraunes coming for it."
    l "So there will be a lot more human and monster couples...{w}\nPerhaps prejudices will go away with that."
    grangold_king "We're also going to start study abroad and youth exchange programs.{w}\nMy goal as King is to turn Grangold into a true city of humans and monsters."
    queen_ant "Of course, I'm going to completely cooperate.{w}\nQueen Elf and Queen Fairy are also going to attend."
    l "If there's anything I can do, please let me know."

    hide queenant
    show alice st02b at xy(X=-120) #700
    show grangold st04 at xy(X=160)
    with dissolve #701

    a "Hmm, I'll help where I can, too.{w}\nI think an exchange of local foods would be a great idea..."
    l "Alice..."

    hide alice
    show grangold st04 at xy(X=-160) #700
    show queenant st01 at xy(X=160) behind grangold
    with dissolve #701

    grangold_king "Thank you for your offers... We may take you both up on them."
    queen_ant "Hero... Monster Lord...{w} I thank you both for everything."

    scene bg 100 with blinds
    return


label city62_15:
    if cont == 1:
        play music "audio/bgm/city1.ogg"

    $ cont = 0
    scene bg 060 with blinds
    show mob heisi1 at xy(X=160)
    show granberia st02 at xy(X=-160) #700
    with dissolve #701

    g "What are you doing!?{w} That footwork is lousy!"
    "Granberia is instructing two soldiers in the military parade grounds..."
    g "That's no good...{w} You have to use your leg to pivot!"
    soldier_a "Y...Yes!"
    soldier_b "Haa... Haa... Hya!"
    l "Granberia, what are you doing...?"

    show granberia st01 at xy(X=-160) with dissolve #700

    g "Mmm... Luka.{w} Alright, take a break!"
    soldier_a "Y...Yes!{w} Oh god... Water..."
    soldier_b "I... I can't move my arms..."

    hide mob
    show granberia st01 at center
    with dissolve #700

    "Ignoring the complaining soldiers, Granberia turns toward me."
    g "The King here offered me a position as military advisor.{w}\nI'm teaching sword skills to their most promising soldiers now."
    l "Granberia as a teacher?{w}\nI... Don't know what to say to that."
    "She doesn't really strike me as a teaching sort of a person, but..."

    show granberia st04 with dissolve #700

    g "I've been thinking...{w} There's no room in this era for someone who lives only to fight.{w}\nThis peaceful world you and the Monster Lord created... I won't disturb it."
    "Granberia suddenly falls silent, and stares at the ground.{w}\nAfter a second, she nervously looks up and fidgets as she speaks."

    show granberia st05 with dissolve #700

    g "By the way...{w} Luka...{w} Would you like to spend some time with me?"
    l "Spend some time? What?{w}\nOh, you mean to spar?"

    show granberia st03 with dissolve #700

    g "T...That's right.{w} Of course, not to the death.{w}\nDo you want to spar in the Monster Lord's Castle some?"
    "For some reason, Granberia seems a little off..."

    menu:
        "Accept{#city62_15}":
            jump city62_15a

        "Refuse{#city62_15}":
            jump city62_15b


label city62_15a:
    g "T...Then let's go to the Monster Lord's Castle."

    show mob heisi1 at xy(X=180)
    show granberia st02 at xy(X=-160) #700
    with dissolve #701

    g "I'll show you the fruits of my training!"
    soldier_a "Thank you for your instruction!{w} Oh god, my leg!"
    soldier_b "M...My muscles are screaming!"
    "With the training over, the soldiers all collapse on the spot."

    play sound "audio/se/asioto2.ogg"
    scene bg 047 with blinds2
    play music "audio/bgm/colosseum.ogg"
    show granberia st41 with dissolve #700

    l "Uwa...!"

    play sound "audio/se/down.ogg"

    "Granberia's powerful slash knocks me to the ground."
    "Her strength is incredible...{w}\nIt seems like a lie that I ever managed to beat her before."

    show granberia st11_b with dissolve #700

    g "Well... This is what I expected."
    g "You only ever fight seriously when you're fighting for your beliefs.{w}\nLast time, I was standing in the way of something you had to do."
    g "...But it's different now.{w}\nYou can't bring yourself to truly wield your blade against me, can you?"
    l "Yeah... That's right."
    "After all, this is like a game.{w}\nUnlike that battle, my mental focus isn't as sharp."

    show granberia st12_b with dissolve #700

    g "When it's just sparring, you don't even reach half of your potential.{w}\nBut when you're fighting for something, you're the strongest being in the world."
    g "That's why I stood no chance against you before...{w}\nIt was unreasonable to ask you to spar with me."
    "Granberia slowly puts away her sword with that."

    show granberia st03 with dissolve #700


label city62_15c:
    if _in_replay:
        play music "audio/bgm/colosseum.ogg"
        scene bg 047
        show granberia st03
        with Dissolve(1.5)

    g "...Though, a win is a win.{w}\nNow, I challenge you to a match in the bed!"
    "With her brute strength, Granberia tears off my clothes.{w}{nw}"

    play music "audio/bgm/ero1.ogg"
    show granberia hf1
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nAnd, stripping off her own clothes, jumps on me!"
    l "W...What are you doing all of a sudden!?"
    g "...What are you complaining about?{w}\nYou're clearly looking forward to this match!"
    "Granberia presses her ass against my penis, slowly getting erect.{w}\nShe angles it right toward her entrance..."

    voice "audio/voice/granberia5_01.ogg"

    g "Now, I'll put it in.{w}\nI wonder how long you can resist?"
    l "W...Wai...{w} Hyaa!"

    play hseanwave "audio/se/hsean04_innerworks_a4.ogg"
    show granberia ct11 at topleft zorder 15 as ct with dissolve #690

    "Granberia pushes her waist against me, and takes me deep inside!"

    voice "audio/voice/granberia5_02.ogg"

    g "Hoora... I brought your thing into me.{w}\nWhat's wrong? You aren't going to fight?"
    l "Ah...ah..."
    "Her vagina is so hot, it feels like I could melt inside her...{w}\nAnd it's so tight inside of her...{w} Her vagina must be surrounded by powerful muscles."
    "All the way from my root to tip, I can feel her soft, hot flesh squeezing me like a hand."

    voice "audio/voice/granberia5_03.ogg"

    g "Well... How does it feel inside me?"
    l "Ah... It feels so good..."
    "A smirk of satisfaction crosses Granberia's face as she watches me."

    voice "audio/voice/granberia5_04.ogg"

    g "It's my complete advantage from the start...{w}\n...In that case, I'll go full on right away!"
    l "Eh...?{w}\nAh...!"
    "Suddenly, Granberia starts to bounce up and down, riding me like a piston!{w}\nEach time she moves, her tight vagina clamps down even harder around me."
    "Her quick movements cause her tight flesh to rub hard against me, leaving me breathless."

    voice "audio/voice/granberia5_05.ogg"

    g "Hoora... How about this!?"
    "Using her trained body as a spring to bounce up and down, her piston movements only get faster as time goes on."
    "Subjected to this intense stimulation, no amount of resistance will let me hold on."

    voice "audio/voice/granberia5_06.ogg"

    g "What's wrong? Just writhing in agony?{w}\nDon't tell me you lost the will to counterattack with just that!?"
    l "Ahg... Urhg..."
    "My waist pushes up into her...{w} But it's not for a counterattack."

    call syasei1 
    show granberia hf2 #700
    show granberia ct12 at topleft zorder 15 as ct #690
    call syasei2 

    "Not even countering, I instead shoot out my flag of surrender.{w}\nI was forced to submit in only seconds..."
    l "Ahhh..."

    voice "audio/voice/granberia5_07.ogg"

    g "Surely... You didn't leak with just that?"
    "Granberia looks both surprised and disappointed.{w}\nThe mix of pleasure and humiliation in my own mind makes my face go bright red."

    voice "audio/voice/granberia5_08.ogg"

    g "So it is this easy to force men to ejaculate...{w}\nAn instant loss, just from moving my waist a little... A rather anti-climactic finish."

    voice "audio/voice/granberia5_09.ogg"

    g "Aren't you ashamed at coming so easily inside me?"
    l "Urhg..."
    "Granberia's surprise makes the humiliation all the deeper.{w}\nAt the same time, she starts to slowly continue moving."

    voice "audio/voice/granberia5_10.ogg"

    g "I can't feel happy from a win like that.{w}\nWell? Are you frustrated? Try counterattacking."

    voice "audio/voice/granberia5_11.ogg"

    g "You're a man, aren't you?{w}\nDon't you feel miserable from being overpowered by a woman on a bed like this?"
    "As she taunts me, I feel my fighting spirit start to rise somehow.{w}\nI don't want it to end like this...!"
    "At the least, I'll get in one good blow!"
    l "Uh... Rrhg!"
    "I thrust up into her from below, my penis pushing deep into Granberia..."

    voice "audio/voice/granberia5_12.ogg"

    g "...It's like child's play.{w}\nWere you trying to attack me?"
    l "Ah... Hya!"

    play hseanwave "audio/se/hsean07_innerworks_a7.ogg"

    "Granberia suddenly tightens her vagina around my tip, halting my counterattack.{w}\nWithout even moving, she completely overpowered me again..."
    l "Ahh... So tight..."
    "She continues to use her female weapons to mercilessly defeat me.{w}\nAble to do nothing but moan and climax, I'm left helpless underneath her as Granberia toys around with me."
    "Granberia stares down at me as she toys with me...{w}\nHer face is slowly brightening as she starts to enjoy this feeling."

    voice "audio/voice/granberia5_13.ogg"

    g "You seem to understand that you can't do anything...{w}\nApparently this seems to be my complete victory!"
    "While violently moving up and down, Granberia continues to squeeze me with her vagina."
    l "Ahhh!{w} Ah!"
    "I'm at her complete mercy..."
    "There's no fighting back, it's Granberia's total victory."
    l "Ah..."
    "While riding me, Granberia squeezes down hard as I near another climax.{w}\nMy penis starts to pulse inside of her, as I approach another surrender to her."

    voice "audio/voice/granberia5_14.ogg"

    g "Hoora... I'll finish you off.{w}\nNow, shoot out the proof of your defeat into me!"
    l "Ahhh!"
    "Granberia uses her powerful muscles to squeeze my tip inside of her..."

    call syasei1 
    show granberia hf3 #700
    show granberia ct13 at topleft zorder 15 as ct #690
    call syasei2 

    "And just as she wanted, I come inside of her.{w}\nAll during my orgasm, Granberia stares into my eyes with a smile."

    stop hseanwave fadeout 1

    l "Ahh..."

    voice "audio/voice/granberia5_15.ogg"

    g "...Miserable.{w}\nGetting such a loose face, after being defeated..."
    "Granberia looks down at me satisfied, a new look of sadism on her face."

    scene bg black with Dissolve(3.0)

    "With this \"Battle\", our relationship is defined."
    "Every day, we train together in our swordplay, growing stronger.{w}\nBut in bed, she makes me taste defeat as a man..."
    "Thus, I give myself over to these nights of defeat.{w}\nI spend every night with Granberia, tasting the pleasure of defeat..."
    ".............."

    $ renpy.end_replay()
    $ persistent.hsean_shitenno1 = 1
    $ end_go = "city62_15"
    jump badend2


label city62_15b:
    show granberia st04 with dissolve #700

    g "I see... I see..."

    show granberia st02 at xy(X=-160) #700
    show mob heisi1 at xy(X=180)
    with dissolve #701

    g "...Break time's over!{w} One thousand reps, now!"
    soldier_a "Aiiieee!!!"
    soldier_b "H...Help us!"
    g "Those who complained, five hundred more!"
    soldier_b "Y...Yes Ma'am!"
    l "............."
    "Is Granberia angry...?"

    scene bg 100 with blinds
    return


label city63_main0:
    $ BG = "bg 041"
    play sound "audio/se/habataki.ogg"
    scene bg 103 with blinds2
    play music "audio/bgm/mura2.ogg"


label city63_main:
    call city_null 
    $ city_bg = "bg 103"
    $ city_button01 = "Feed" #601
    $ city_button02 = "Prostitute" #602
    $ city_button03 = "Young Sisters" #603
    $ city_button04 = "Soothsayer" #604
    $ city_button05 = "Naccubus" #605
    $ city_button06 = "Eva" #606
    $ city_button07 = "Amira" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Bar" #612
    $ city_button13 = "Mayor's House" #613
    $ city_button14 = "Ceremony Room" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city63_01 
        elif result == 2:
            call city63_02 
        elif result == 3:
            call city63_03 
        elif result == 4:
            call city63_04 
        elif result == 5:
            call city63_05 
        elif result == 6:
            call city63_06 
        elif result == 7:
            call city63_07 
        elif result == 10:
            call city63_10 
        elif result == 11:
            call city63_11 
        elif result == 12:
            call city63_12 
        elif result == 13:
            call city63_13 
        elif result == 14:
            call city63_14 
        elif result == 19:
            return


label city63_01:
    show mob seinen1 with dissolve #700

    feed_a "O...oh... Mmm...{w}\nEhe... Oh... Yes..."
    "The men seem the same as always."

    return


label city63_02:
    show maccubus st11 at xy(X=175) with dissolve #700

    maccubus "Those Succubi who attacked are all living here now.{w}\nI don't believe in refusing anyone from living here, but..."
    maccubus "I don't like the thought of having to share my food.{w}\nWe need to work on getting more male immigrants here..."
    maccubus "Even if we have tons of potatoes, that just isn't enough."
    l "...Potatoes?"

    return


label city63_03:
    show minccubus st03 at xy(X=335, Y=50)
    show renccubus st03 at xy(X=15) #700
    with dissolve #701

    lencubus "Those potatoes the Mayor gave me...{w} I still have so many!{w} There's no way I could eat all of these."
    minccubus "I think she's bringing more tomorrow, too.{w}\nOur house is going to just be a storehouse for potatoes soon..."
    l "Why not give them to someone else?"

    show renccubus st01 at xy(X=15) with dissolve #700

    lencubus "The Mayor has been giving them to everyone.{w} Everyone has a ton of them."
    lencubus "Since it's the Mayor, we can't just refuse.{w}\nBut other than semen, all of our meals have just been potatoes..."

    return


label city63_04:
    show witchs st11 at xy(Y=50) with dissolve #700

    witch_succubus "To thank that you would go on to save the world...{w}\nMy fortune telling wasn't able to see something that huge."
    witch_succubus "Hehe, would you like a reward?{w}\nI'll make you feel wonderful..."
    l "No, that's alright..."
    witch_succubus "Then how about some potatoes at least?{w}\nI'm sick of them..."
    "What's with all of the potatoes...?"

    return


label city63_05:
    show narcubus st11 with dissolve #700

    naccubus "In the end, I'm able to live here!{image=note}"
    naccubus "Does anyone need their white stuff harvested!?{w}\nI'll extract it all with my syringe!{image=note}"

    return


label city63_06:
    show eva st01 with dissolve #700

    eva "Since then, they have accepted us into the village."
    eva "As punishment for what we did, we've been assigned five years of labor...{w}\nBut it's better than those miserable days spent wandering in search of semen."

    show alice st04b at xy(X=-120) #700
    show eva st01 at xy(X=160)
    with dissolve #701

    a "Well, it's not like you were always evil.{w}\nPoverty can make you desperate...{w} And suddenly getting power like that made you become drunk on it."
    a "Hard labor for five years is a good punishment.{w}\nReflect on your actions during them."
    eva "I will, Monster Lord."
    eva "...Once I'm released, I'll target that potato Mayor first.{w}\nThen I'll take over the village, and make all the men mine..."
    l ".............."
    "I don't think she has really reflected yet..."

    return


label city63_07:
    play music "audio/bgm/amira.ogg"
    show amira st01 with dissolve #700

    amira "Thank you, Hero!{w} You saved the world!"

    show amira st01 at xy(X=-160) #700
    show zannen st01 at xy(X=180) #699
    with Dissolve(1.5)

    pyhar "I'll need to give you thaaaaanks!"

    scene bg 140
    show zannen st02 at xy(Y=-250) #700
    with dissolve

    diamrem "............."

    scene bg 103
    show amira st01 at xy(X=-160) #700
    show zannen st03 at xy(X=160) #699
    with dissolve

    ga_star_d "............."
    l "Wait a second, the Unfortunate Mermaid...?"

    hide zannen
    show amira st01 at center
    with Dissolve(1.5) #700

    amira "She became a star in that final battle...{w}\nBut her soul is still burning brightly inside our stomachs."

    show amira st01 at xy(X=-160) #700
    show zannen st01  at xy(X=180) #699
    with Dissolve(1.5)

    pyhar "Wait...{w} Was that sushi you gave us as a reward...?"

    show amira st01 at xy(X=-160) #700
    show zannen st03 at xy(X=160) #699
    with Dissolve(1.5)

    ga_star "............."
    l "Hey, is it just me or is Ga******'s censoring getting worse?"
    stars "............."
    l "Oh, now that's just mean."
    ga_star "............."
    l "Yes, that's better."
    garf_star "............."
    l "Keep it up, you're nearly there!"
    garfun_star "............."
    l "...Wait, what?"
    garfunkel "............."
    l "What the!?"

    hide zannen
    hide amira
    show alice st01b
    with Dissolve(1.5) #700

    a "Luka...{w} Stay back before you catch the stupid."
    l "R...Really?"

    if check10 == 0:
        call city_noiv 
        return

    "I always feel odd after talking with these Unfortunate types...{w}\nAnyway..."

    hide alice
    show amira st01
    with Dissolve(1.5) #700

    l "...Amira, thank you for all of your help.{w}\nWithout you, things would have been really difficult."
    a "Yes, I thank you as well."
    a "By the way...{w}\nDo you recognize this book?"
    "Alice holds up the \"Legend of Hero Luka\" book we bought earlier."
    amira "I...I know nothing!{w}\nI've never seen such a book!"
    "Amira's voice suddenly squeaks as she denies it.{w}\nShe was the one who wrote it then, wasn't she?"

    show amira st01 at xy(X=160)
    show alice st01b at xy(X=-120) #700
    with dissolve #701

    a "So it was you.{w}\nDid you write it just to make some money?"
    amira "...I just wanted to tell the world the story of my Darling's battles!{w}\nThe trials and tribulations undergone as he fought on to the end!"
    l "Amira..."

    hide alice
    show zannen st01 at xy(X=160) behind amira #701
    show amira st01 at xy(X=-160) #700
    with Dissolve(1.5)

    pyhar "You were cackling like a lunatic as those royalties were coming in.{w}\nShe built a huge mansion in Iliasburg with a gold-plated gate emblazoned with \"Palace Luka\"."
    l ".............."
    "So that's how it is."

    hide zannen
    show alice st03b at xy(X=-120) #700
    show amira st01 at xy(X=160)
    with dissolve #701

    a "What the hell is this final chapter, too!?{w}\n\"The Hero and Monster Lord's final Love Love art: Love Lova Giga!\""
    amira "I wasn't able to interview you before the final battle!{w} I had to improvise!"
    amira "Though you're right, that is fairly embarrassing.{w} You two would never do something like combine your love and power together to launch it out in a single dramatic blow..."
    l "R...Right..."

    show alice st08b at xy(X=-120) with dissolve #700

    a "Y...Yeah, right..."

    hide alice
    show amira st01 at center
    with Dissolve(1.5) #700

    amira "But I should have gotten permission first.{w}\nMy author's spirit simply leaped free as that page was before me, and I couldn't contain myself!"
    amira "If you two desire, I'll stop publication..."
    l "No... I don't care.{w}\nThe book is all about fighting for coexistence, after all..."
    "It's all silly over dramatization anyway.{w}\nThe character in the story may be based on me, but it's too exaggerated for anyone to take seriously."
    "But if it's able to get across the message of coexistence, then I guess it's fine.{w}\nThat was the most important part, anyway."
    amira "I'll be careful with my Heroic Tales from now on.{w}\nI wouldn't want to create any false stories that are handed down, after all..."

    show alice st01b at xy(X=-120) #700
    show amira st01 at xy(X=160)
    with dissolve #701

    a "Ah... That reminds me.{w}\nI won't interfere too much with the content, but..."

    show alice st04b at xy(X=-120) with dissolve #700

    a "Why did you have to go crazy with the relationship part in the story!?{w}\nNow all these strange rumors are all over the place..."
    amira "But it was a special instruction from an important dignitary...{w}\nShe told me to play up the love aspect, and make you two lovers."
    amira "She wanted it to become a widespread fact, fast..."

    show alice st07b at xy(X=-120) with dissolve #700

    a "W...Who told you to do such a thing!?"
    amira "Hint 1: Kitsune."

    show alice st52b at xy(X=-120) with dissolve #700

    a "TAMAMOOOOOOOOOOOOOOOOOO!!!"

    $ persistent.ruka_book = 1
    play music "audio/bgm/mura2.ogg"
    return


label city63_10:
    scene bg 151 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "The world is so peaceful now... I can't stop crying!{w}\nWaaah!{w} Uwaaahhh!!!"
    shopkeeper "I can't sell any weapons... I'm going to be ruined...{w}\nWaaaahhh!!!"
    "He's really crying..."

    scene bg 103 with blinds
    return


label city63_11:
    scene bg 156 with blinds
    show inp_c st21 at xy(X=280) #700
    show inp_b st11 at xy(X=-250)
    show inp_a st01 #698
    with dissolve #699

    imp "This building looks empty...{image=note}{w}\nThis is going to be our secret base!{image=note}"
    remi "Meh meh..."
    rumi "Erp..."
    l "............"
    "The small monsters seem to have taken up residence in the abandoned church..."

    scene bg 103 with blinds
    return


label city63_12:
    "I hear voices coming from the bar..."
    succubus_a "I'm going to suck more out of you!"
    man_a "Ahhhyyaaaa!!!"
    succubus_b "What, already coming?{w}\nNot yet, I won't let you!"
    man_b "Nnnngggg!!!"
    l "..........."
    "I turn around and leave."

    return


label city63_13:
    if cont == 1:
        play music "audio/bgm/mura2.ogg"

    $ cont = 0
    scene bg 106 with blinds
    show succubus st03 at xy(Y=50) with dissolve #700

    succubus "There has been some nasty things being said behind my back recently.{w}\nI want us to be a self sufficient village, so I've started a plan to grow potatoes in the fields."
    succubus "We produced so much this year, we distributed a large load to everyone.{w}\nAnd now they've taken to calling me Potato Mayor behind my back!"
    l "M...My sympathies...{w}\nBut why potatoes?"
    succubus "Even us Succubi can't suck in semen all the time.{w}\nIn preparation for a time of famine, we're harvesting potatoes."
    succubus "Right now we have lots of men, so we're always feasting, but..."
    succubus "Sheesh, these spoiled Succubi today never have experienced true hunger."
    l "............"

    show succubus st01 at xy(Y=50) with dissolve #700

    succubus "Anyway, I thank you, savior of our village.{w}\nI've prepared a grand feast in your honor."

    show succubus st02 at xy(Y=50) with dissolve #700

    succubus "Please, come this way.{w}\nEnjoy this feast to your heart's content..."
    l "A feast...?{w} For me?"
    "I'm thankful for the gesture, but I have a bad feeling about this.{w}\nShould I accept her invitation...?"

    menu:
        "Accept{#city63_13}":
            jump city63_13a

        "Refuse{#city63_13}":
            jump city63_13b


label city63_13a:
    if _in_replay:
        play music "audio/bgm/mura2.ogg"
        scene bg 106
        show succubus st02 at xy(0, 50)
        with Dissolve(1.5)

    l "Well if you went to the effort, I'll join in..."
    succubus "Then please come this way.{w}\nIt's right behind this door..."

    play sound "audio/se/door2.ogg"
    hide succubus
    show bg 181
    with Dissolve(1.5)

    "I move past the door into a surprisingly spacious room.{w}{nw}"

    show minccubus st02 at xy(X=420)
    show renccubus st04 at xy(X=170) #701
    show maccubus st12 at xy(X=-80) #700
    $ renpy.transition(dissolve, layer="master") #702

    extend "{w=.7}\nInside, numerous Succubi are standing around."
    l "Eh...?{w} Where is the food for the feast?"

    hide renccubus
    hide maccubus
    hide minccubus
    show succubus st02 at xy(Y=50)
    with dissolve #700

    succubus "Hehe... The feast just arrived.{w}\nEveryone, get him!{image=note}"
    l "Eh...?{w} Uwaa!"

    play music "audio/bgm/ero1.ogg"
    hide succubus
    show end6 h1
    with dissolve #700

    "Succubi rush at me from every direction.{w}\nCompletely taken by surprise, I'm held down in a sitting position while I'm still in shock."

    play hseanwave "audio/se/hsean02_innerworks_a2.ogg"

    lencubus "Nnn... *Kiss* *Lick*"
    "Lencubus takes advantage of my open mouth, and gives me a deep kiss.{w}\nAs her tongue rubs against mine, a sweet feeling fills my head."
    l "Ahh..."
    "A relaxing feeling of weakness flows through me, as the Succubus passionately kisses me."
    lencubus "The Hero tastes so good... Mmmm... *Kiss*"
    l "Haa... Wait..."
    "Still reeling from the sudden kiss, I can't manage to summon the strength to push off the Succubi, who should be weaker than me."
    "With a seductive smile on her face, Maccubus positions herself against my waist."
    maccubus "Hehe... I'll feast on your power.{w}\nI'll spend a nice, long time using Level Drain to suck you dry..."
    l "Wh...Why are you doing this?"
    maccubus "The world is at peace, so you don't need your power anymore, right?{w}\nSo here...{w} Let me suck it all out from here..."
    "Maccubus uses her fingers to spread open her vagina, showing it off in front of my face."
    "The wet, bright pink flesh seems to pulse with desire, as if wanting nothing more than me inside it..."
    l "S...Stop this!"
    maccubus "Hehe... I'm so happy...{w}\nI get to suck up the semen from the Hero who saved the world!"
    "With a lewd smile, Maccubus drops down, bringing my penis deep inside in one stroke!"
    l "Ahhh!!!"

    show end6 h2 with dissolve #700

    "My penis is wrapped up by her soft flesh, her winding insides rubbing against my penis as I pierce her.{w}\nThe incredible feeling of ecstasy from being inside a Succubus's vagina causes me to go limp in shocked pleasure."
    maccubus "Ahaha... I swallowed it.{image=note}{w}\nMaster Hero, how does a Succubus's pussy feel?"
    maccubus "I'll give you a feast of pleasure inside me, so let's make this last a long time."
    "Her soft vaginal walls clench down on my penis, locking me inside.{w}\nAt the same time, another hole deep inside her sucks in my tip, as if an inner mouth was sucking on me."
    "Just from insertion, I'm already writhing in unbearable pleasure."
    l "Ahh... L...Let me go..."
    "She'll absorb my power if I ejaculate inside her.{w}\nI try to struggle against the pleasure, but no strength is filling my limbs."
    "Once a Succubi starts to prey on a man, they aren't able to fight back..."
    lencubus "*Lick*... Nnn..."
    "Lencubus starts to alternate between licking my face, and giving me deep kisses.{w}\nAs one Succubus rapes me, and another covers my face with her warm saliva, I feel like I'm trapped in a torturous hell of pleasure."
    l "Ah... Haa..."
    maccubus "Oh my, what's wrong?{w}\nYour face seems to be getting all loose..."
    "Frustrated at being controlled so easily by the Succubi, the sudden climax takes me by surprise.{w}\nUnable to even try to hold it back, my body relaxes as the explosion of pleasure runs through me."

    call syasei1 
    show end6 h3 #700
    call syasei2 

    l "Haaa..."
    "I explode inside her vagina, which made me helpless so quickly.{w}\nThe sudden storm of ecstasy that came over me out of nowhere almost feels like an accidental orgasm."
    maccubus "Aha, you already came?{w}\nYou didn't even last a minute after insertion...{w} You're just like a little virgin."
    maccubus "You're so silly, Master Hero... Hehe."
    l "Ahhh..."
    "The humiliation at being toyed with so easily mixes with the deep male satisfaction at my orgasm."
    "The various emotions swirl around my head, as the afterglow of the orgasm fills my senses."
    maccubus "Hehe, you seem to have enjoyed your orgasm.{w}\nI made you feel good first, so now it's my turn, right?"
    maccubus "Now it's my turn to be satisfied...{w}\nI'll suck up all your power..."
    l "S...Stop..."
    "Maccubus's body starts to glow with a faint, white light.{w}\nAt the same time, I feel her vagina start to increase in temperature..."
    maccubus "Level Drain..."
    l "Ah!{w} Ahhh!!!"

    play hseanwave "audio/se/hsean15_kyuin2.ogg"

    "Her vagina slowly starts to squeeze and release, creating a suction inside her.{w}\nThe intense stimulation really feels like a powerful mouth is sucking hard on my penis."
    "At the same time, her vaginal walls compress and rub against my sensitive penis.{w}\nThe combination of pleasure is too much to take, leaving me a writhing mess underneath her."
    maccubus "Now come inside me...{w}\nForget everything, and just give me everything..."
    l "Ahhh!!!"

    call syasei1 
    show end6 h4 #700
    call syasei2 

    "Just as Maccubus demands, I explode inside her.{w}\nUnable to do anything, I shoot more semen into the Succubus's vagina."
    "At the same time, a strange feeling of weakness comes over me.{w}\nAlong with my orgasm, a small fragment of power was sucked out of me."
    maccubus "Hehe... I sucked up a little bit of your power.{w}\nBut as expected of the Hero who saved the world, there's so much..."
    maccubus "I'll squeeze you for a long, long time until you're empty..."
    maccubus "Here, like this...{w}\nI'll squeeze, and suck on you with my pussy..."
    l "Haaa..."
    "Her vaginal walls squeeze tighter, the wavy flesh massaging my penis as if it had a will of its own.{w}\nThe touch alone is enough to make me tremble in ecstasy under her."
    maccubus "Slowly tighten and massage...{w}\nAnd wring out your power..."
    l "No... I'm going to come again..."
    "The soft flesh presses around my penis from every side, leaving me barely able to gasp between my moans of pleasure."
    "Becoming prey to a Succubus, and having all my energy squeezed out...{w} Even if I know what's happening, I can't fight against the pleasure to stop it."
    maccubus "Now for the finishing blow with Level Drain..."
    "The suction starts inside her vagina again, sucking on my penis.{w}\nSurrendering to that incredible touch, I gasp as I reach another climax."
    l "Haaa!"

    call syasei1 
    show end6 h5 #700
    call syasei2 

    "I explode inside her again, my semen spurting into her Succubus vagina.{w}\nAnother strange feeling of weakness comes over me, but it isn't much."
    "Very little of my power is sucked out, but if this continues for a long time, it will slowly take a toll..."
    maccubus "Hehehe... We'll take a good, long time sucking you dry.{w}\nOf course it won't just be me, every Succubus in this village will have a turn."
    maccubus "Even once you're dry, there's no need to worry.{w}\nWe'll take care of you in this village forever."
    maccubus "It's your reward for saving the world. Hehehe..."
    l "Ahh..."

    stop hseanwave fadeout 1

    "Thus, I'm subjected to a continual Level Drain...{w}\nAfter a long time, eventually every drop of my power is sucked up by the Succubi of the village."
    "Even after losing all of my power, they keep me captive in the village.{w}\nEvery day, they treat me to a feast of carnal pleasure as a guest of honor in the village."
    "A horrible fate for a Hero..."

    scene bg black with Dissolve(3.0)

    "................."

    $ renpy.end_replay()
    $ persistent.hsean_end6 = 1
    $ end_go = "city63_13"
    jump badend2


label city63_13b:
    show succubus st03 at xy(Y=50) with dissolve #700

    succubus "...This is truly a shame, it was such a perfect opportunity.{w}\nPlease forgive us for not giving you a true celebration worthy of a Succubus Village..."
    l "I...It's fine..."
    "She seems really disappointed..."

    scene bg 103 with blinds
    return


label city63_14:
    scene bg 110 with blinds
    show lilith st01 at xy(X=138) with dissolve #700

    lilith "Ilias is gone... This is great.{w}\nI'll give you some extra huge service..."
    lilim "But Alipheese the Eighth is a little more complicated...{w}\nTo think she was trying to destroy us, too."
    l "You two were her subordinates five hundred years ago, right?"
    lilith "Yes... We dreamed of world conquest along with her.{w}\nBut we were both beaten down by Heinrich."
    lilim "Her Majesty was very nice to her subordinates.{w}\nI don't know at what point she went insane like that..."
    lilim "She wasn't one to want to destroy anything...{w}\nWhat's the point of ruling over a field of ashes?"

    show alice st01b at xy(X=-120) #700
    show lilith st01 at xy(X=298)
    with dissolve #701

    a "Her big ambitions may have been warped by that \"White Rabbit\" drug.{w}\nPerhaps her desire to rule the world got twisted into one of destroying it, when she realized she couldn't do it any longer."
    a "In the end, she was swallowed by her own power..."
    lilith "At the least, the two of us will hold a funeral for her.{w}\nWe may be the only two alive that know her for who she used to be..."
    lilim "She will go down in history as pure evil...{w}\nBut at the least, we will remember her before that."

    scene bg 103 with blinds
    return


label city64_main0:
    play sound "audio/se/habataki.ogg"
    scene bg 119 with blinds2
    play music "audio/bgm/gordport.ogg"


label city64_main:
    call city_null 
    $ city_bg = "bg 119"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Town Girl" #602
    $ city_button03 = "Lupton" #603
    $ city_button04 = "Arakure" #604
    $ city_button05 = "Soldier" #605
    $ city_button06 = "Trooperoid" #606
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Ilias Kreuz Headquarters" #612
    $ city_button13 = "Harbor" #613
    $ city_button14 = "Hero's Shrine" #614
    $ city_button15 = "Cemetery" #615
    $ city_button16 = "Serena's House" #616
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city 

        if result == 1:
            call city64_01 
        elif result == 2:
            call city64_02 
        elif result == 3:
            call city64_03 
        elif result == 4 and ivent14 == 3:
            call city64_04a 
        elif result == 4 and ivent14 < 3:
            call city64_04b 
        elif result == 5:
            call city64_05 
        elif result == 6:
            call city64_06 
        elif result == 10:
            call city64_10 
        elif result == 11:
            call city64_11 
        elif result == 12:
            call city64_12 
        elif result == 13:
            call city64_13 
        elif result == 14:
            call city64_14 
        elif result == 15:
            call city64_15 
        elif result == 16:
            call city64_17 
        elif result == 19:
            return


label city64_01:
    show mob seinen1 with dissolve #700

    youth "That last battle was something fierce."
    youth "Those marine monsters lead by that giant Poseidoness...{w}\nThen the ghost ship showing up, with Captain Selene on it!"
    youth "Thanks to them, this town was protected.{w}\nEven though we hated monsters... We owe them our lives."

    return


label city64_02:
    show mob musume1 with dissolve #700

    town_girl "But why did Captain Selene's ship appear?{w}\nDoes she like this town or something?"

    return


label city64_03:
    show mob gorotuki3 with dissolve #700

    lupton "There are still flowers on Lazarus's grave.{w}\nFor now, he's being remembered as a Hero who died defending the town..."
    l "............."
    lupton "Oh, don't worry.{w}\nIlias Kreuz won't ever reform."
    lupton "It's dead, along with Lazarus.{w}\nAll of us know that the previous era is over with."

    return


label city64_04a:
    show mob gorotuki1 with dissolve #700

    arakure "My bud came back.{w}\nHe said you busted into that Drain Lab place and sprung him out!"
    arakure "Thanks a million, man!{w} You the best!"

    return


label city64_04b:
    show mob gorotuki1 with dissolve #700

    arakure "My bud came back.{w}\nThat Granberia liz sprung him out!"

    return


label city64_05:
    show mob sensi1 with dissolve #700

    soldier "Us humans took up all of the long range attacks during that final battle.{w}\nWe were chucking bombs and shooting arrows as fast as we could."
    soldier "I'm truly grateful that Poseidoness and her army took the front line."

    return


label city64_06:
    show trooperloid st01 with dissolve #700

    trooperoid "Hey, can I suck out some semen?"
    l "You... You're still attacking people here!?"
    trooperoid "Wait, no!{w}\nI'm asking permission before I do!"
    trooperoid "Just about everyone is fine with it, though.{w}\nI took up a part time job harvesting and selling semen, it's going well."
    l "Oh... That's good, then.{w} I think?"

    return


label city64_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "I made a ton of weapons, but now the demand is gone.{w}\nHow am I going to get rid of all these spears and swords...?"

    scene bg 119 with blinds
    return


label city64_11:
    scene bg 154 with blinds
    show mob ozisan2 with dissolve #700

    shopkeeper "I was stunned when I saw Captain Selene's boat."
    shopkeeper "Idiot!{w} I'm so damn stupid!{w} I can't believe I missed my chance to ask her where she buried her treasure..."

    scene bg 119 with blinds
    return


label city64_12:
    scene bg 117 with blinds

    l ".........."
    "With Lazarus dead, the building is abandoned.{w}\nI hope no group like Ilias Kreuz ever form again."

    scene bg 119 with blinds
    return


label city64_13:
    scene bg 118 with blinds
    show mob sentyou with dissolve #700

    captain "There's a vacant house on the west side of town...{w}\nBut my friend said he saw some woman there."
    captain "Even more, she was dressed like a pirate!{w}\nWhen he tried to get closer to take a look, she vanished!"
    "West side of town...{w} That's Serena's... No, I mean Selene's house."
    captain "That house is mighty suspicious, actually.{w}\nIt's been vacant for a while, but it still looks clean and furnished..."
    captain "It's well maintained, and quite big.{w}\nWhy hasn't anyone bought it yet, I wonder?"
    l "............."
    "Hmm... Maybe I should take a look?"

    scene bg 119 with blinds
    return


label city64_14:
    scene bg 092 with blinds
    show mob sinkan at xy(X=-220) #700
    show mob sinkan as mob2 #701
    show mob sinkan at xy(X=220) as mob3
    with dissolve #702

    sage_a "Oooh, Hero Luka!{w} Well done, defeating False God Ilias!"
    sage_b "No matter the era, we shall remain relevant!"
    sage_c "That is us!{w} The Three Wise Men!"
    l "............"
    "These three are something else...{w}\nBut at least they seem to have their spirits back."
    l "Now that Ilias is gone, what will you three do?{w}\nI don't think any more Heroes will visit..."
    sage_a "...We have fulfilled our duty.{w}\nWe shall write down what happened, to inform the future generations!"
    sage_b "The truth of the world we have come to understand in this small shrine.{w}\nWe shall record everything in a book!"
    sage_c "Please, Hero Luka...{w}\nCan you give us your thoughts on what we have written so far?"
    "One of the Sages hands me a handwritten book."
    l "E...Err... Well..."
    l "\"Chapter One: About Peace.{w}\nPeace is simple. But it is profound. That profoundness makes it complex."
    l "To understand it, you must also know what it means to \"Shoot the Moon\".{w} Sometimes the best---\""
    l "...Hey!{w} This is just a guide on how to play Hearts!"
    sage_a "This is the result of our research, patience, and meditation."
    sage_b "Indeed, the culmination of years of effort.{w}\nWe shall entrust the generations to come with this..."
    sage_c "Our Hearts will shine forth!"

    scene bg 119 with blinds
    return


label city64_15:
    scene bg 168 with blinds
    play music "audio/bgm/kanasimi2.ogg"

    l "I'm done, Father.{w}\nSomeone like me actually managed to save the world..."
    "Standing in front of my father's grave, I tell him about my battle with Ilias.{w}\nThis is a giant step forward in coexistence among every race."
    l "Father... Mother... Please continue watching over me in the future.{w}\nNow, and always..."

    show alice st04b with dissolve #700

    a ".................."

    play music "audio/bgm/gordport.ogg"
    scene bg 119 with blinds
    return


label city64_17:
    if cont == 1:
        play music "audio/bgm/gordport.ogg"

    $ cont = 0
    scene bg 123 with blinds

    "I head to the house that Selene had invited me to before..."

    show serene st11 with dissolve #700

    selene "It has been a while, Luka."
    "Captain Selene is still here!"
    l "Serena...{w} No, Selene!?{w}\nWhy are you here!?"

    show serene st13 with dissolve #700

    selene "During the final battle, I returned to help protect my hometown along with my pirate crew."
    selene "But after fighting for so long, I used up too much energy...{w}\nThanks to that, I can't move on."
    l "I...Is that so?"

    show serene st11 with dissolve #700

    selene "So Luka... Will you give me some energy?{w}\nThe method is simple, you just need to have sex with me."
    l "No, no, no, absolutely not!"
    "Having sex with Selene's spirit...?{w}\nThat's just wrong, no matter how you look at it."

    show serene st13 with dissolve #700

    selene "You're so mean, Luka...{w}\nI'm a spirit who can't move on, right?"
    l "There must be some other way..."
    "Having sex with a ghost is just no good.{w}\nI mean... They're already dead, right?"

    show serene st11 with dissolve #700

    selene "...Hey, Luka.{w}\nIt's tough to be a female in the world of violent pirates..."
    selene "But I was able to rule the seven seas as a woman by using a \"Carrot\" and \"Stick\"."
    l "Eh...?{w} What are you talking about?"
    selene "My \"Stick\" was built up with the overwhelming power of my crew.{w}\nNobody would underestimate me just because I was a woman."
    selene "No matter the fight, we wouldn't lose.{w}\nBut just that alone isn't enough to rule the seven seas..."
    l "It's true you can't just rule by force..."
    selene "What's important is the \"Carrot\".{w}\nI'd bring the pirates that we defeated into my bedroom, one by one..."
    l "Y...You didn't..."
    selene "Yes, as you thought..."
    "With a seductive smile, Selene traces her finger around her groin area..."
    selene "After tasting my sweet \"Carrot\", all of the other pirate groups aligned with me.{w}\nNo matter how strong the man, they all surrendered after just a little rape..."
    selene "There was even one who said my pussy was better than a mermaid's."
    l "............"
    "I can't help but swallow.{w}\nBetter than a mermaid's...?"
    selene "...Well, Luka?{w}\nDon't you want to taste my body?"
    selene "You'll feel good, and I'll get some energy...{w}\nWe both profit, right?"
    l "Err... That's..."

    menu:
        "Give Energy{#city64_17}":
            jump city64_17a

        "Refuse{#city64_17}":
            jump city64_17b


label city64_17a:
    if _in_replay:
        play music "audio/bgm/gordport.ogg"
        scene bg 123
        show serene st11
        with Dissolve(1.5)

    l "F...Fine...{w}\nI want to help others, after all..."

    show serene st12 with dissolve #700

    selene "Hehe... You're such a good child.{w}\nLet's get started right away..."

    play music "audio/bgm/ero1.ogg"
    hide serene
    show end7 h1
    with dissolve #700

    "Selene quickly sits down on me.{w}{nw}"

    play hseanwave "audio/se/hsean03_innerworks_a3.ogg"

    extend "\nMy penis sinks deep into her warm vagina."
    l "Ahhh!"
    "An intense feeling of pressure attacks me all at once.{w}\nIt feels like my skin is all being pulled down toward the root of my penis."
    l "Ahh... W...What...?"
    "Selene giggles as my eyes roll in confusion."
    selene "My pussy gets tighter the deeper you get...{w}\nIt pushes back all the skin, leaving your glans completely unguarded."
    l "Ahhh..."
    "Her vagina itself peeled away my foreskin, completely revealing my weakest point to direct stimulation."
    l "W...What the... My tip... Ahh!"
    selene "After pressing down all the skin, I can make my pussy extra tight..."
    l "Haa... Ahhh!"
    "Squeeze... Squeeze... She presses down hard on my defenseless glans, deep inside of her.{w}\nThe intense pleasure causes an ejaculation to bubble up in seconds."
    l "Ahh... N...No...!{w}\nDon't move that much!"
    "I gasp in pleasure as the torturous ecstasy assaults me.{w}\nThere's no way I could endure this!"
    selene "Sorry, Luka...{w}\nI'm not doing it on purpose."
    selene "I was born with my pussy like this, it's difficult for me at times, too."
    l "Ha... Ahhh!"

    call syasei1 
    show end7 h2 #700
    call syasei2 

    "After only around ten seconds after insertion, I explode inside of her incredible vagina."
    selene "...Well, that's what normally happens.{w}\nDon't be ashamed, most men come right away their first time."
    selene "It's good for breaking and training men, but...{w}\nSince men always come so fast, I've never been able to find a man that can satisfy me."
    "While sitting on top of me, Selene sighs.{w}\nAt the same time, her incredible vagina continues to squeeze and attack me..."
    l "Ahhh!{w}\nI'm going to come again!"
    selene "Sorry, but can I have you come one more time?{w}\nThere didn't seem to be enough last time..."

    play hseanwave "audio/se/hsean07_innerworks_a7.ogg"

    "With that said, Selene starts to slowly move her waist.{w}\nHer soft vaginal flesh rubs against me as she moves, sending fresh sensations through my spine."
    l "Ahhh!!!"
    "Even though she wasn't moving before, I came in no time...{w}\nHer vagina is like a weapon itself, able to force a man to submit."
    l "N...No... Ahh!"
    selene "Hehe, I'll finish you off quickly.{w}\nPlease give me enough energy to move on to the next world..."
    l "Haaa!!!"
    "Selene's skilled movements bring me to the edge again in seconds.{w}\nThe unmatched vagina she boasted about is no lie..."
    "As she slowly moves, her vagina's shape itself causes each movement to press and pull on my shaft and tip, as the sweet tightness erodes my patience."
    l "Ahhh!!!"

    call syasei1 
    show end7 h3 #700
    call syasei2 

    "I'm easily brought to another orgasm, my penis pulsing inside her with spurts of pure pleasure."
    l "Ahh..."
    selene "Now, I can rest in peace...{w} Or not?{w}\nWas that not enough?"
    "Selene looks puzzled on top of me."
    selene "If I was lacking energy, I thought I would have had enough after the first orgasm.{w}\nI mean, your semen is packed full of energy..."
    selene "But if it's still not working...{w} Then is there another reason?"
    "Even as she thinks aloud, focused on other things, her vagina doesn't stop squeezing on me."
    "While she's not even looking at me or trying to make me come, I'm led to another height."
    l "Ah... Ahh!"

    call syasei1 
    call syasei2 

    "As I come inside her again, she turns back toward me, a look of surprise on her face."
    selene "Ah... Sorry.{w}\nI was thinking about something else for a second."
    l "Haa..."
    "My body relaxes in total weakness beneath her body.{w}\nOverwhelmed by pleasure, my soul itself has been captured..."

    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)

    "Even after that, Selene couldn't move on.{w}\nIs it because she was so powerful in life?{w} Or due to fighting angels in spirit form?"
    "Selene's mansion became a legend among men.{w}\nNumerous men would come by every night, and have sex with Selene's spirit."
    "After tasting her body even once, a man becomes her slave.{w}\nI, of course, was no exception..."

    play hseanwave "audio/se/hsean08_innerworks_a8.ogg"
    show bg 123
    show end7 h1 #700
    with Dissolve(1.5)

    l "Haa... Selene, it feels so good..."
    selene "Luka, you're completely a captive, aren't you?"
    selene "Here, I'll squeeze it inside...{w}\nTight around your tip, your favorite..."
    l "Ah...!{w} I...I'm coming!"

    call syasei1 
    show end7 h2 #700
    call syasei2 

    "My body trembles in ecstasy as I explode as hard as I can inside her.{w}\nThis vivid pleasure doesn't feel like it could come from a ghost at all..."
    l "Haa... So good..."
    selene "You'll let me suck more energy?{w}\nHehe... You're as energetic as always, Luka."
    l "Haa..."
    "Thus, I continue having sex every night with the ghost of this female pirate...{w}\nA slave to her body, I continue giving her my energy..."

    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)

    "............"

    $ renpy.end_replay()
    $ persistent.hsean_end7 = 1
    $ end_go = "city64_17"
    jump badend2


label city64_17b:
    l "I...I can't do that.{w}\nPlease wait a little longer, I'll bring an expert here..."

    show serene st13 with dissolve #700

    selene "Oh my... So cold."

    if ivent05 < 3:
        "I'll bring Tamamo by later..."
    elif ivent05 == 3:
        "I'll talk this over with Tamamo or Chrome later."

    "I quickly leave Selene's house..."

    scene bg 119 with blinds
    return


label city_noiv:
    "Nothing seems to be going on here..."

    return
