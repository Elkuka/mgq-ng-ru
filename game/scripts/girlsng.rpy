init python:
    def get_granberia_ng():
        girl = Chr()
        girl.unlock = persistent.granberia_ng_unlock == 1
        girl.name = "Гранберия"
        girl.pedia_name = "Гранберия (NG+)"
        girl.info = _("""Молодая Драконица и одна из Четвёрки Небесных Рыцарей. Гранберия невероятно сильная Проклятая Мечница, способная использовать множество смертиельных и эффективных техник. Вероятно является самой сильной из Небесных Рыцарей, так как была единственной, кто сразилась против Владыки Монстров на отборочном турнире. С другой стороны, Альма Эльма может легко победить её, а против истинной силы Тамамо скорее всего не продержится даже Алиса.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "granberia_ng"
        girl.label_cg = "granberia_ng_cg"
        girl.m_pose = granberia_ng_pose
        girl.facenum = 9
        girl.posenum = 8
        girl.bk_num = 0
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 24000
        girl.scope = {"ruka_tati": 1,
        "mylv": 80,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_queenharpy_ng():
        girl = Chr()
        girl.unlock = persistent.queenharpy_ng_unlock == 1
        girl.name = "Королева Гарпий"
        girl.pedia_name = "Королева Гарпий (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "queenharpy_ng"
        girl.label_cg = "queenharpy_ng_cg"
        girl.info_rus_img = "queenharpy_ng_pedia"
        girl.m_pose = queenharpy_ng_pose
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 0
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 25000
        girl.scope = {"ruka_tati": 1,
        "mylv": 81,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_tamamo_ng():
        girl = Chr()
        girl.unlock = persistent.tamamo_ng_unlock == 1
        girl.name = "Тамамо"
        girl.pedia_name = "Тамамо (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "tamamo_ng"
        girl.label_cg = "tamamo_ng_cg"
        girl.info_rus_img = "tamamo_ng_pedia"
        girl.m_pose = tamamo_ng_pose
        girl.facenum = 7
        girl.posenum = 4
        girl.bk_num = 0
        girl.x0 = -200
        girl.lv = "128 (запечатанная), 300 (текущая сила), 500 (предел)"
        girl.hp = "48000 (текущая сила), 72000 (предел)"
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 26,
        "item01": 9
        }
        return girl

    def get_alma_ng():
        girl = Chr()
        girl.unlock = persistent.alma_ng_unlock == 1
        girl.name = "Альма Эльма"
        girl.pedia_name = "Альма Эльма (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = alma_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_sylph_ng():
        girl = Chr()
        girl.unlock = persistent.sylph_ng_unlock == 1
        girl.name = "Сильфа"
        girl.pedia_name = "Сильфа (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = sylph_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_gnome_ng():
        girl = Chr()
        girl.unlock = persistent.gnome_ng_unlock == 1
        girl.name = "Гнома"
        girl.pedia_name = "Гнома (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = gnome_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_erubetie_ng():
        girl = Chr()
        girl.unlock = persistent.erubetie_ng_unlock == 1
        girl.name = "Эрубети"
        girl.pedia_name = "Эрубети (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = erubetie_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_undine_ng():
        girl = Chr()
        girl.unlock = persistent.undine_ng_unlock == 1
        girl.name = "Ундина"
        girl.pedia_name = "Ундина (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = undine_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl

    def get_salamander_ng():
        girl = Chr()
        girl.unlock = persistent.salamander_ng_unlock == 1
        girl.name = "Саламандра"
        girl.pedia_name = "Саламандра (NG+)"
        girl.info = _("""The Dragon Swordswoman who is one of the Four Heavenly Knights. Skilled in flame-based sword skills, she devotes herself to her own training. Her weapon skills are said to be the most powerful among all monsters, with nobody coming even close.
Though most Dragonkin raise their young, Granberia was left alone at a very young age. Along with Salamander, she traveled the world as a vagrant. As Salamander taught Granberia swordplay, she became like her adoptive parent. Though Granberia is more powerful than Salamander now, she has never lost her respect or gratitude toward her.""")
        girl.zukan_ko = None
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.info_rus_img = "alma_ng_pedia"
        girl.m_pose = salamander_ng_pose
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 126
        girl.hp = 48000
        girl.scope = {"ruka_tati": 1,
        "mylv": 82,
        "skill01": 25,
        "item01": 9
        }
        return girl
