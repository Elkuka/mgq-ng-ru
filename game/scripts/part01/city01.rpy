label city01_main:
    call city_null
    $ city_bg = "bg 003"
    $ city_button01 = "Мужчина A" #601
    $ city_button02 = "Мужчина B" #602
    $ city_button03 = "Старик" #603
    $ city_button04 = "Девочка" #604
    $ city_button05 = "Старейшена" #605
    $ city_button06 = "Солдата" #606
    $ city_button07 = "Рыцарь" #607
    $ city_button10 = "Бетти" #610
    $ city_button11 = "Оружейный магазин" #611
    $ city_button12 = "Лавка" #612
    $ city_button13 = "Храм Илиас" #613
    $ city_button19 = "Покинуть деревню" #619

    while True:
        call cmd_city

        if result == 1:
            call city01_01
        elif result == 2:
            call city01_02
        elif result == 3:
            call city01_03
        elif result == 4:
            call city01_04
        elif result == 5:
            call city01_05
        elif result == 6:
            call city01_06
        elif result == 7:
            call city01_07
        elif result == 10:
            call city01_10
        elif result == 11:
            call city01_11
        elif result == 12:
            call city01_12
        elif result == 13:
            call city01_13
        elif result == 19:
            return


label city01_01:
    show mob seinen1 with dissolve #700

    man_a "О, Лука! Разве сегодня не твоё крещение?"
    man_a "Ты выглядишь совершенно иначе, чем раньше!"
    l "................."
    "Так оно и есть..."
    man_a "Обязательно навести нас, как вернёшься со своего путешествия!"


    return


label city01_02:
    show mob seinen1 with dissolve #700

    man_b "Мой друг из Илиасбурга был похищен гарпией...{w}\nПохоже, его вынудили стать одним из её мужей..."
    man_b "Мне так его жаль!{w}\nЯ ненавижу монстров всей душой...{w}\nПожалуйста, убей их столько, сколько сможешь..."
    l "................."
    "Я понимаю, что это вина Илиас в том, что все здесь так ненавидят монстров.{w}\nНо всё же... это так ужасно."

    return


label city01_03:
    show mob ozisan1 with dissolve #700

    old_man "Боже... монстр так близко подошёл к нашей деревне.{w}\nЯ так напугался..."
    old_man "Пожалуйста, Лука, убей Владыку Монстров и искорени всех её подданых!{w}\nЯ буду с нетерпением ждать этого!"

    return


label city01_04:
    show mob syouzyo with dissolve #700

    $ girl = Character("Девочка", ctc_pause="ctc_wait", ctc="ctc")
    girl "Удачи тебе, братик!{w}\nПобей всех плохих монстров!"
    $ girl = Character("Девушка", ctc_pause="ctc_wait", ctc="ctc")

    return


label city01_05:
    show mob rouzin2 with dissolve #700

    elder "У молодёжи в наши дни совсем не осталось никакой веры в Илиас...{w}\nИменно в наказание за это монстр ворвался сюда..."
    elder "Удалось ли тебе получить благославление, Лука?{w}\nУбедись, что служишь Илиас верой и правдой."

    return


label city01_06:
    show mob sensi1 with dissolve #700

    soldier "О, Лука! Уже отправляешься?{w}\nХоть ты и очень молод, но уже довольно неплох в бою..."
    soldier "Уверен, ты сможешь победить Владыку Монстров!!"
    soldier "Если так, войду ли я в историю как человек, проводивший Героя?"
    soldier "Просто шучу... удачи в твоём странствии!"

    return


label city01_07:
    show mob heisi1 with dissolve #700

    knight "Никогда не поддавайся на соблазны монстров.{w}\nЕсли ты позволишь монстру заставить тебя кончить, то весь твой боевой дух исчезнет."
    knight "Я думаю, он назывался «Критический экстаз».{w}\nУчёные пришли к выводу, что акт эякуляции истощает магию, необходимую для борьбы."
    knight "Я до сих пор не могу в это поверить…{w}\nНо, другими словами, если ты кончишь, то потеряешь и разум, и боевой дух для сражений."
    knight "С этого момента ты не сможешь сопротивляться ничему, что бы они с тобой не сделали..."
    knight "Однако есть много сумасшедших людей, которые на самом деле желают подобного...{w}\nКак прискорбно..."
    knight "О, великая Илиас, пожалуйста, даруй своё божественное наказание таким развратным типам..."

    return


label city01_10:
    scene bg 038 with blinds
    show mob obasan with dissolve #700

    betty "Лука... Я слышала, что ты не получил благословения.{w}\nЭто правда?"
    l "Именно так.{w}\nНо я всё равно собираюсь отправиться в приключение."
    betty "Ты не бросишь эту затею, даже будучи некрещённым?{w}\nУходить без защиты Илиас опасно..."
    l "Я знаю..."
    betty "Ты ведь не послушаешься меня, чтобы я не сказала, не так ли?{w}\nНе перенапрягайся, Лука.{w}\nТы можешь вернуться сюда в любое время."
    l "Спасибо..."
    "Этот диалог звучит так нелепо после всего, что со мной произошло."

    play sound "audio/se/asioto2.ogg"
    pause 1.0

    betty "О, великая Илиас... пожалуйста, защити этого ребёнка."

    scene bg 003 with blinds
    return


label city01_11:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    smith "О, Лука! Ты уже отправляешься, верно?"
    l "Да. Я... спасибо за этот меч."
    "Мой меч был выкован на заказ местным кузнецом.{w}\nОн изготовлен таким образом, чтобы даже такой маленький, как я, мог его легко использовать."
    "К сожалению, я им так по настоящему и не воспользовался в своём путешествии.{w}\nНо даже несмотря на это он мне всё ещё дорог."
    l "Я буду дорожить этим мечом, что вы сделали для меня."
    smith "Всё в порядке, не переживай.{w}\nВо время путешествия, пожалуйста, не стесняйся использовать лучшее оружие."
    smith "Если не будешь постоянно получать более сильное оружие, то никогда не сможешь победить Владыку монстров."
    smith "Будет очень странно продолжать использовать слабое оружие."
    l "К-конечно. И всё же, я буду беречь его."
    smith "Ох... делай как сочтёшь нужным!{w}\nТолько не делай глупостей!"
    l "Постараюсь! Не волнуйтесь!"

    scene bg 003 with blinds
    return


label city01_12:
    scene bg 151 with blinds
    show mob ozisan2 with dissolve #700

    ronnie "Ох, Лука... ты пришёл сюда, чтобы попрощаться со мной?"
    ronnie "Хоть я не так долго был в этой деревне, ты был хорошим клиентом."
    l "Да... в конце концов, я получал много ран."
    "Раз в три дня я приходил сюда и покупал лечебные травы.{w}\nВо время своих тренировок я много травмировался."
    "... Хотя, учитывая как много \"пользы\" эти тренировки принесли, видимо я делал что-то не так."
    ronnie "Когда ты вернёшься в эту деревню, моего магазина здесь может уже не оказаться.{w}\nВ последнее время продажи идут довольно плохо... {w}Люди потеряли веру...?"
    l "Буря..."
    ronnie "Хм?"
    l "Н-ничего!"
    ronnie "Ну что ж, мне нет никакого смысла жаловаться на это тебе.{w}\nУдачи, Лука! Я буду молиться за тебя!"
    l "Спасибо..."
    "Надеюсь, ты будешь всё ещё жив, когда я вернусь..."

    scene bg 003 with blinds
    return


label city01_13:
    scene bg 007 with blinds
    show mob sinkan with dissolve #700

    priest "Лука.{w}\nТы даже не появился на церемонии своего благославления!"
    l "Мне оно не нужно.{w}\nИлиас ведь всё равно не сошла с Небес, дабы благославить меня, не так ли?"
    priest "Н-ну... на самом деле так оно и есть..."
    priest "Похоже Илиас бросила тебя.{w}\nНо даже так, ты всё ещё собираешься отправиться в странствие?"
    l "Да. Есть много вещей, которые мне нужно сделать."
    priest "Даже если Илиас не присматривает за тобой, я буду за тебя молиться.{w}\nЛука, пожалуйста, береги себя."
    l "Спасибо."

    scene bg 003 with blinds
    return


label city02_main:
    call city_null
    $ city_bg = "bg 018"
    $ city_button01 = "Юноша" #601
    $ city_button02 = "Девушка" #602
    $ city_button03 = "Проститутка" #603
    $ city_button04 = "Мужчина" #604
    $ city_button05 = "Старушка" #605
    $ city_button06 = "Солдат A" #606
    $ city_button07 = "Солдат B" #607
    $ city_button10 = "Оружейный магазин" #610
    $ city_button11 = "Лавка" #611
    $ city_button12 = "Церковь" #612
    $ city_button13 = "Гостиница Сазерленд" #613
    $ city_button19 = "Уйти" #619

    while True:
        if hanyo[0] == 0:
            $ city_act10 = 1
        elif hanyo[0] == 1:
            $ city_act10 = 0

        call cmd_city

        if result == 1:
            call city02_01
        elif result == 2:
            call city02_02
        elif result == 3:
            call city02_03
        elif result == 4:
            call city02_04
        elif result == 5:
            call city02_05
        elif result == 6:
            call city02_06
        elif result == 7:
            call city02_07
        elif result == 10:
            call city02_10
        elif result == 11:
            call city02_11
        elif result == 12:
            call city02_12
        elif result == 13:
            call city02_13
        elif result == 19:
            return


label city02_01:
    show mob seinen1 with dissolve #700

    youth "Я правда думал, что я умру...{w}\nОдна из Четвёрки Небесных Рыцарей в самом деле... была невероятно сильной."
    youth "Она только раз на меня посмотрела...{w}\n... и я сразу намочил штаны..."

    return


label city02_02:
    show mob musume1 with dissolve #700

    girl "Монстры нападают на нас и здесь...{w}\nНе осталось ни одного Героя способного их прогнать...?"

    return


label city02_03:
    show mob musume2 with dissolve #700

    prostitute "А-а, Герой, спасший город.{w}\nТы выглядишь слабым, но всё же ты смог её прогнать..."
    prostitute "Хи-хи... Я сделаю тебе особую скидку."
    l "Эмм, извини...{w} Я думаю мне не следует это делать..."
    prostitute "Хи-хи-хи… какой же ты милашка."

    return


label city02_04:
    show mob seinen2 with dissolve #700

    man "Монстры в наши дни творят всё, что им заблагорассудится...{w}\nЕсли так пойдёт и дальше, человечество вымрет...?"
    man "Проклятье... неужели никто ничего не может сделать?"

    return


label city02_05:
    show mob rouba with dissolve #700

    old_woman "Эти никчёмные люди, зовущие себя Героями.{w}\nПри первых признаках опасности они все пропадают."
    old_woman "Убегают... притворяются мёртвыми... как это жалко."
    old_woman "Но мне сказали, что пришёл маленький мальчик и сразил Гранберию своим лазерным лучом из глаз!"
    l "................"
    "... Что?"

    return


label city02_06:
    show mob sensi1 with dissolve #700

    soldier_a "Вероятно, Гранберия хотела использовать город как плацдарм для атаки на Храм Илиас..."
    soldier_a "Разве храм чем-либо угрожает монстрам?"

    return


label city02_07:
    show mob sensi2 with dissolve #700

    soldier_b "Несмотря на то, что Гранберия — монстр, у неё истинно рыцарский дух.{w}\nОна не убивала детей или стариков, не убивала всех подряд."
    soldier_b "Даже если монстров и нельзя хвалить, многие из нас тоже должны стремиться к такой силе..."

    return


label city02_10:
    scene bg 152 with blinds

    l "Хмм, доспехи..."
    "На стендах висело много крепко выглядещей брони.{w}\nХотя я не привык носить такие тяжёлые доспехи."

    show alice st16b with dissolve #700

    a "Ты ведь на самом деле не хочешь покупать ничего из этого дерьма?"
    l "Ага."
    "В конце концов...{w}\nМне очень нравится эта рубашка."
    a "И хотя у тебя уже есть такая..."
    "Сказав это, Алиса посмотрела на обычную на вид рубашку, лежащую на прилавке."
    l "Эта...?{w}\nНо точно такая же на мне сейчас надета."
    "Она выглядит просто, но сшита добротно."

    show alice st11b at xy(X=-120) #700
    show mob ozisan1 at xy(X=160)
    with dissolve #701

    shopkeeper "А-ах, у вас зоркий глаз, мисс.{w}\nЭта рубашка была соткана особым образом для обеспечения максимальной защиты."
    shopkeeper "Пусть и выглядит просто, но она обладает превосходной защитой.{w}\nХотя в последнее время Героев интересует только внешний вид."
    shopkeeper "Подобные люди не замечают, насколько хороша эта броня.{w}\nПохоже, что вы не такая!"
    l "Думаю запасная рубашка никогда не повредит...{w} Хорошо, беру!"
    $ item21 = 1
    shopkeeper "Спасибо за покупку!"

    play sound "audio/se/kaimono.ogg"
    pause 2.0

    $ hanyo[0] = 1
    scene bg 018 with blinds
    return


label city02_11:
    scene bg 151 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "Добро пожаловать! Вы ищете что-то конкретное?"
    l "Я просто осматриваюсь..."
    shopkeeper "Вам нужны медицинские травы или противоядия?"
    l "Нет, ничего подобного..."
    "... Разве я когда-нибудь использовал их в своём путешествии?{w}\nНе думаю..."
    shopkeeper "Монстры в наши дни не стремятся сильно ранить людей."
    shopkeeper "Они просто соблазняют их непристойными вещами.{w}\nЧто ж, тогда как насчёт этой кованой брони с поясом?"
    l "Кажется, в ней будет сложно двигаться...{w}\nИ, похоже, она уже была использована..."
    shopkeeper "Вы сможете её использовать для разных целей..."
    "Ничего не купив, я покидаю лавку."

    scene bg 018 with blinds
    return


label city02_12:
    scene bg 153 with blinds

    l "Церковь..."

    "По-моему раньше я здесь молился Илиас.{w}\nТеперь, после всего что случилось, находиться здесь неуютно."

    show alice st11b with dissolve #700

    a "Как скучно..."

    show alice st11b at xy(X=-120) #700
    show mob sinkan at xy(X=160)
    with dissolve #701

    priest "Молиться нужно искренне."
    a "Появился скучный человек."
    priest "... Ну, уж простите за то, что я такой скучный."
    priest "Что более важно, нужно помнить: молиться следует искренне.{w}\nЭто одна из пяти заповедей."
    a "Правда, как же скучно."
    l "...................."
    "Мне в самом деле не хочется здесь оставаться."
    l "Можем мы уже уйти отсюда?{w}\nПожалуйста?"
    a "С удовольствием."
    priest ".........."

    scene bg 018 with blinds
    return


label city02_13:
    scene bg 016 with blinds
    show mob obasan with dissolve #700

    owner "О? Вы сегодня остаётесь здесь?{w}\nДля вас сегодня особые расценки..."
    owner "Но у меня больше нет Ама-Ама Данго, потому что Мёд Счастья кончился."
    owner "Если герой вскоре не отправится в деревню Счастья, я уже никогда не смогу их сделать."
    "Сказав это, она выжидающе смотрит на меня."

    scene bg 018 with blinds
    return


label city03_main0:
    $ BG = "bg 019"
    play sound "audio/se/asioto2.ogg"
    show bg 018 with blinds2
    play music "audio/bgm/city1.ogg"


label city03_main:
    call city_null
    $ city_bg = "bg 018"
    $ city_button01 = "Юноша" #601
    $ city_button02 = "Девушка" #602
    $ city_button03 = "Проститутка" #603
    $ city_button04 = "Мужчина" #604
    $ city_button05 = "Старушка" #605
    $ city_button06 = "Солдат A" #606
    $ city_button07 = "Солдат B" #607

    if ivent01 == 1:
        $ city_button08 = "Девочка-гоблин" #608

    $ city_button10 = "Оружейный магазин" #610
    $ city_button11 = "Лавка" #611
    $ city_button12 = "Церковь" #612
    $ city_button13 = "Гостиница Сазерленд" #613
    $ city_button19 = "Покинуть город" #619

    while True:
        if hanyo[0] == 0:
            $ city_act03 = 1
        elif hanyo[0] == 1:
            $ city_act03 = 0
        if hanyo[1] == 0:
            $ city_act04 = 1
        elif hanyo[1] == 1:
            $ city_act04 = 0

        call cmd_city

        if result == 1 and ivent01 == 0:
            call city03_01a
        elif result == 1 and ivent01 == 1:
            call city03_01b
        elif result == 2 and ivent01 == 0:
            call city03_02a
        elif result == 2 and ivent01 == 1:
            call city03_02b
        elif result == 3 and ivent01 == 0:
            call city03_03a
        elif result == 3 and ivent01 == 1:
            call city03_03b
        elif result == 4 and ivent01 == 0:
            call city03_04a
        elif result == 4 and ivent01 == 1:
            call city03_04b
        elif result == 5 and ivent01 == 0:
            call city03_05a
        elif result == 5 and ivent01 == 1:
            call city03_05b
        elif result == 6 and ivent01 == 0:
            call city03_06a
        elif result == 6 and ivent01 == 1:
            call city03_06b
        elif result == 7 and ivent01 == 0:
            call city03_07a
        elif result == 7 and ivent01 == 1:
            call city03_07b
        elif result == 8:
            call city03_08
        elif result == 10 and ivent01 == 0:
            call city03_10a
        elif result == 10 and ivent01 == 1:
            call city03_10b
        elif result == 11 and ivent01 == 0:
            call city03_11a
        elif result == 11 and ivent01 == 1:
            call city03_11b
        elif result == 12 and ivent01 == 0:
            call city03_12a
        elif result == 12 and ivent01 == 1:
            call city03_12b
        elif result == 13 and ivent01 == 0 and ivent02 == 0:
            call city03_13a
        elif result == 13 and ivent01 == 1 and ivent02 == 0:
            call city03_13b
        elif result == 13 and ivent02 == 1:
            call city03_13c
        elif result == 19:
            return


label city03_01a:
    show mob seinen1 with dissolve #700

    youth "Банда воров...?{w}\nЯ слышал, что среди них есть дракон."
    youth "В последний раз, покидая город, я услышал голос и обмочился."

    return


label city03_01b:
    show mob seinen1 with dissolve #700

    youth "И как я мог обмочиться из-за такого мелкого монстра..."

    return


label city03_02a:
    show mob musume1 with dissolve #700

    girl "Монстры нападают на людей без разбору...{w}\nМир ужасен..."

    return


label city03_02b:
    show mob musume1 with dissolve #700

    girl "Они не такие, как я думала...{w}\nСейчас мне всего лишь немного страшно."

    return


label city03_03a:
    show mob musume2 with dissolve #700

    prostitute "Все побеждённые солдаты покинули город...{w}\nДела уже не так хорошо идут..."
    prostitute "Эй, мальчик... не хочешь со мной немножко поиграть?"

    show alice st11b at xy(X=-120) #700
    show mob musume2 at xy(X=335)
    with dissolve #701

    a ".........."
    prostitute "Ох, прости, не знала, что ты уже занят...{w}\nЭта одежда... ты работаешь в той же сфере, что и я...?"
    show alice st13b with dissolve
    a "Ты...!"
    show alice st14b with dissolve
    a "... Хмпф.{w}\nЯ никогда не стала бы опускаться до секса со случайными мужчинами ради денег, как ты."
    prostitute "Ну извините меня, ваше Величество...{w}\nЯ полагаю с таким нарядом ты делаешь это вообще за бесплатно?"
    show alice st12b with dissolve
    a "У меня есть стандарты мужчин, в отличии от тебя."
    prostitute "Хмпф!"
    play sound "audio/se/asioto3.ogg"
    hide mob with dissolve
    show alice st12b at center with dissolve
    "Проститука раздражённо уходит."
    l "Алиса...{w}\nПожалуйста, не обижай проституток."
    show alice st14b with dissolve
    a "Эта лекция пойдёт ей на пользу.{w}\nРазве называть незнакомую тебе женщину проституткой - это не серьёзное оскорбление в людском обществе?"
    l "Ну, вроде того..."
    show alice st11b with dissolve
    a ".........{w}{nw}"
    show alice st13b with dissolve
    extend "\n... Что?"
    show alice st11b with dissolve
    l "Просто...{w}\nТвоя одежда..."
    show alice st13b with dissolve
    a "Да что не так с моей одеждой?{w}\nНа Хеллгондо жарища. Что ты вообще ожидал увидеть?"
    l "Алиса, если я немного наклоню голову, то смогу увидеть что у тебя под юбкой.{w}\nТы даже под ней ничего не носишь..."
    show alice st14b with dissolve
    a "Довольно грубо заглядывать женщинам под юбки, Лука."
    l "Мммм..."
    show alice st13b with dissolve
    a "Да и в конце концов, я же ламия.{w}\nЧто по-твоему мы должны использовать в качестве нижнего белья? Идиот..."
    show alice st11b with dissolve
    l "Хорошо, но...{w}\nДаже твоя верхняя одежда не выдерживает никакой критики..."
    show alice st13b with dissolve
    a "В{w} Хеллгондо{w} жарко.{w}\nЯ вообще не планировала задерживаться на этом континенте надолго и ходить в такой одежде по человеческим городам."
    show alice st16b with dissolve
    a "Да и ты серьёзно?{w}\nЖалуешься из-за того, что смотришь на моё оголённое тело?{w}\nЧто, чёрт возьми, с тобой не так?"
    show alice st11b at xy(X=-120) with dissolve
    show mob gorotuki3 at xy(X=175) with dissolve
    nosy_pervert "Да! Что с тобой не так, мужик?"
    stop music fadeout 1.5
    l ".............."
    a ".............."
    a "Мы знакомы?{w} Катись отсюда, чудик."
    nosy_pervert "Гах...!"
    play sound "audio/se/escape.ogg"
    hide mob
    show alice st11b at center
    with dissolve
    "Странный подслушивающий человек убежал."
    play music "audio/bgm/city1.ogg"
    l "Хорошо-хорошо!"
    l "Я лишь хотел, чтобы ты оставила проститутку в покое.{w}\nТебе нет нужды быть такой врединой..."
    show alice st14b with dissolve
    a "Хорошо, как скажешь...{w}\n... Тупица."
    l "Блин..."
    $ hanyo[0] = 1
    $ iliasburg_event1 = 1

    return


label city03_03b:
    show mob musume2 with dissolve #700

    prostitute "Я лишь видела Дракона и Вампира...{w}\nЭтот Дракон станет больше?"

    return


label city03_04a:
    show mob seinen2 with dissolve #700

    man "Все боятся покидать город из-за воров.{w}\nВы можете что-нибудь сделать?"
    show mob seinen2 at xy(X=200) with dissolve
    show alice st13b at xy(X=-120) with dissolve
    a "Зачем вам тогда вообще городская стража, если они не могут вас защитить?"
    show alice st11b with dissolve
    man "Никто из них не хотел выходить на бой и рисковать своей жизнью ради нас..."
    show alice st14b with dissolve
    a "Именно поэтому вместо стражи вы просите ребёнка, который достаточно глуп чтобы возразить, рисковать ради ваших жизней?"
    l "Алиса!"
    "Она такая бестактная...{w}\nИ почему она вообще захотела поговорить с горожанами?"
    man "Но этот мальчик победил Гранберию!{w}\nОн потрясающий!"
    man "Если кто и может защитить город от воров, то это он."
    show alice st14b with dissolve
    a "Кхм...{w}\nНадеюсь ты прав насчёт этого."
    l "Не все люди настолько ужасны, как ты думаешь, Алиса..."
    show alice st13b with dissolve
    a "Бьюсь об заклад, что они точно также умоляли тебя сразиться с этими монстрами, когда ты был 4 уровня."
    show alice st11b with dissolve
    l "Н-не в этом дело..."
    $ iliasburg_event4 = 1
    $ hanyo[1] = 1

    return


label city03_04b:
    show mob seinen2 with dissolve #700

    man "Я не хочу видеть монстров в этом городе...{w}\nНо, похоже, что я в меньшинстве, поэтому больше ничего не скажу."

    return


label city03_05a:
    show mob rouba with dissolve #700

    old_woman "Дракон и Вампир...{w}\nОт мысли, что они могут напасть на город в любой момент, мне становится страшно..."
    old_woman "Ты молился сегодня Илиас?{w}\nО, пожалуйста, защити наш беззащитный город..."

    return


label city03_05b:
    show mob rouba with dissolve #700

    old_woman "So the band of thieves were just those little things..."
    old_woman "What are their parents doing? Do monsters not care at all about their children?"

    show alice st11b at xy(X=-120) #700
    show mob rouba at xy(X=160)
    with dissolve #701

    a "Monsters raise their children differently depending on the type. Some let their children be independent from birth."
    a "Since I'm a Lamia, I don't really know how a dragon or Vampire would raise their kids."
    l "I hope the people of this city can raise them..."

    return


label city03_06a:
    show mob sensi1 with dissolve #700

    soldier_a "Я бы с удовольствием выгнал и Дракона, и Вампира, но сегодня у меня болит желудок."
    soldier_a "О-ой! Похоже на завтра у меня запланирована простуда, а после этого головная боль.{w}\nДумаю, какое-то время я не смогу этого сделать..."

    return


label city03_06b:
    show mob sensi1 with dissolve #700

    soldier_a "Crap... I'm really sick now...{w}\nIs this punishment for before?"
    soldier_a "I won't lie anymore, Ilias, I promise!{w}\nI won't bully my little sister either..."

    return


label city03_07a:
    show mob sensi2 with dissolve #700

    soldier_b "Банда воров?{w}\nХотел бы я их прогнать, но боюсь, что слишком слаб..."

    return


label city03_07b:
    show mob sensi2 with dissolve #700

    soldier_b "You had the courage to go and fight them.{w}\nBut they turned out to be so little..."
    soldier_b "Your courage should still be praised, though."

    return


label city03_08:
    show gob st23 at xy(Y=70) with dissolve #700

    goblin_girl "Move, move, move!{image=note}"
    goblin_girl "Ah! The Hero from before!{w}\nI'm working hard on this street!{image=note}"
    l "Yeah, you'll grow up to be a splendid monster.{w}\nYou seem a lot different from the other three."
    goblin_girl "Ah, they were born in the north of Sentora, and I came from the east."
    goblin_girl "I met them when we were all heading south."
    l "Ah... is that how you met?"

    show alice st11b at xy(X=-120) #700
    show gob st23 at xy(X=160, Y=70)
    with dissolve #701

    a "From the east? I didn't think they dressed like you do..."
    goblin_girl "I used to dress like they did, but I changed.{w}\nThis is the bandit style from the Natalia area."
    l "I... see..."

    return


label city03_10a:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "Банда воров? Да, они украли пять моих кинжалов."
    shopkeeper "Сэм обнаружил, что у него украли топор и его обед.{w}\nОни довольно хладнокровны."
    shopkeeper "И это ещё не всё!{w}\nЯблоня Томми также была наполовину объедена."
    shopkeeper "Однажды ночью, когда он вернулся, от яблок остались только огрызки.{w}\nЭто ужасно."
    l ".........."
    "... Не вижу в этом ничего особенного..."

    scene bg 018 with blinds
    return


label city03_10b:
    scene bg 152 with blinds
    show mob ozisan1 at xy(X=-160) #700
    show dragonp st13 at xy(X=160, Y=100)
    with dissolve #701

    shopkeeper "«Куй железо, пока горячо» — не просто пословица!{w}\nЭто стиль жизни!"

    play sound "audio/se/kazi.ogg"

    dragon_pup "Ugaa!"
    "The little dragon seems to be training with the blacksmith."
    shopkeeper "Oh, Hero!{w}\nThis little child is giving it her all!"
    shopkeeper "Once she can learn a useful skill, I'm sure she won't do bad things anymore."

    play sound "audio/se/kazi.ogg"

    dragon_pup "Uga! I'll do my best!{w}\nI'll grow up to be a good blacksmith!"
    l "Good luck!"

    scene bg 018 with blinds
    return


label city03_11a:
    scene bg 151 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "Банда воров?{w}\nДа, они стащили груз мёда."
    shopkeeper "Дракон и Вампир...{w}\nЯ боюсь, как бы они не напали на город."

    scene bg 018 with blinds
    return


label city03_11b:
    scene bg 151 with blinds
    show mob seinen1 at xy(X=-160) #700
    show vgirl st13 at xy(X=160, Y=100)
    with dissolve #701

    shopkeeper "Welcome!"
    vgirl "Welcome!"

    call show_skillname(_("Eyes of Obedience"))
    play sound "audio/se/flash.ogg"
    with flash

    "The Vampire Girl's eyes start to shine!"

    call hide_skillname

    vgirl "Buy 3,000 herbs."
    shopkeeper "Hey! Don't use your eyes like that!"
    "It looks like the Vampire girl is helping the tool shop owner."
    owner "Sheesh, I can't take my eyes off this girl for a second."
    l "Haha, thanks for everything."

    scene bg 018 with blinds
    return


label city03_12a:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Я уверен, если буду молиться усердней, Илиас ниспошлёт правосудие на банду монстров-воров."

    scene bg 018 with blinds
    return


label city03_12b:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Мы не должны позволить монстрам жить в нашем городе.{w}\nИлиас точно покарает их."

    scene bg 018 with blinds
    return


label city03_13a:
    scene bg 016 with blinds
    show mob obasan with dissolve #700

    owner "О? Вы сегодня остаётесь здесь?{w}\nДля вас сегодня особые расценки..."
    owner "Но у меня больше нет Ама-Ама Данго, потому что Мёд Счастья кончился."
    owner "Если герой вскоре не отправится в деревню Счастья, я уже никогда не смогу их сделать."
    "Сказав это, она выжидающе смотрит на меня."

    scene bg 018 with blinds
    return


label city03_13b:
    scene bg 016 with blinds
    show mob obasan at xy(X=-160) #700
    show pramia st13 at xy(X=160, Y=100)
    with dissolve #701

    owner "Welcome!"
    tiny_lamia "Welcome!{image=note}"
    l "Oh, are you working here?"
    tiny_lamia "Yep! I'm doing my best!"
    owner "She's a surprisingly hard worker.{w}\nI want to teach her how to make Ama-ama Dangos too."
    owner "But I don't have any Happiness Honey."
    owner "If a Hero doesn't go to Happiness Village soon, we may never be able to make them again."
    "She says as she looks at me expectantly."

    scene bg 018 with blinds
    return


label city03_13c:
    scene bg 016 with blinds

    if ivent01 == 0:
        show mob obasan with dissolve #700
    elif ivent01 == 1:
        show mob obasan at xy(X=-160, Y=0)
        show pramia st13 at xy(X=160, Y=100)
        with dissolve #700

    owner "Oh! Welcome!"

    if ivent01 != 0:
        tiny_lamia "Welcome!{image=note}"

    owner "I heard that you resolved the situation at Happiness Village!"
    owner "I'll let you stay here whenever you want!"

    scene bg 018 with blinds
    return


label city04_main0:
    $ BG = "bg 011"
    play sound "audio/se/asioto2.ogg"
    show bg 023 with blinds2
    play music "audio/bgm/mura2.ogg"


label city04_main:
    call city_null
    $ city_bg = "bg 023"
    $ city_button01 = "Молодая девушка" #601
    $ city_button02 = "Женщина A" #602
    $ city_button03 = "Женщина B" #603
    $ city_button04 = "Девушка" #604
    $ city_button05 = "Мальчик" #605

    if ivent02 == 0:
        $ city_button06 = "Старейшина" #606
        $ city_button19 = "Подождать вечера" #619
    elif ivent02 == 1:
        $ city_button06 = "Глава деревни" #606
        $ city_button10 = "Юноша" #610
        $ city_button11 = "Старик" #611
        if acheck(pm, 7) == False:
            $ city_button12 = "Гарпия" #612
        $ city_button13 = "Сёстры-гарпии" #613
        $ city_button14 = "Королева гарпий" #614
        $ city_button19 = "Покинуть деревню" #619

    while True:
        call cmd_city

        if result == 1 and ivent02 == 0:
            call city04_01a
        elif result == 1 and ivent02 == 1:
            call city04_01b
        elif result == 2 and ivent02 == 0:
            call city04_02a
        elif result == 2 and ivent02 == 1:
            call city04_02b
        elif result == 3 and ivent02 == 0:
            call city04_03a
        elif result == 3 and ivent02 == 1:
            call city04_03b
        elif result == 4 and ivent02 == 0:
            call city04_04a
        elif result == 4 and ivent02 == 1:
            call city04_04b
        elif result == 5 and ivent02 == 0:
            call city04_05a
        elif result == 5 and ivent02 == 1:
            call city04_05b
        elif result == 6 and ivent02 == 0:
            call city04_06a
        elif result == 6 and ivent02 == 1:
            call city04_06b
        elif result == 10:
            call city04_10
        elif result == 11:
            call city04_11
        elif result == 12:
            call city04_12
        elif result == 13:
            call city04_13
        elif result == 14:
            call city04_14
        elif result == 19:
            return


label city04_01a:
    show mob musume1 with dissolve #700

    young_girl "Я могу использовать эту палку…{w}\nК тому же она с острым наконечником."
    young_girl "Машущие штуки — не вилы на ферме…{w}\nНо я хочу спасти моего отца!"

    return


label city04_01b:
    show mob musume1 with dissolve #700

    young_girl "Я рада, что мой отец вернулся…{w}\nНо теперь у меня новая мачеха и сводная сестра…{w}\nПривыкнуть ко всему этому немного сложновато."

    return


label city04_02a:
    show mob obasan with dissolve #700

    woman_a "Интересно, можно ли использовать пчёл как оружие и приказать им напасть на Гарпий…{w}\nНет, это было бы смешно. Это звучит, словно в какой-то глупой игре…"

    return


label city04_02b:
    show mob obasan at xy(X=-160) #700
    show mob hapy3 at xy(X=160) as mob2
    with dissolve #701

    woman_a "Эй, Пипи, принеси вон тот улей сюда!{w}\nОн тяжёлый, поэтому будь аккуратна."
    pipi "Хорошо! ♪"

    show mob hapy2 at xy(X=160) as mob2 with dissolve #701

    piana "Сюда, матушка?"
    woman_a "Хорошо, спасибо."

    hide mob2
    show mob obasan at center with dissolve #700

    woman_a "Я рада любой помощи.{w}\nУ нас здесь всё в порядке, но я слышала, что у других семей много проблем."
    woman_a "Всё же это лучше, чем если бы всех мужчин похитили."

    return


label city04_03a:
    show mob musume2 with dissolve #700

    woman_b "Пожалуйста, дай мне сил, как в прошлый раз…{w}\nНа время, чтобы я избила своего мужа за его измену!"

    return


label city04_03b:
    show mob musume2 with dissolve #700

    woman_b "Три жены и двенадцать детей… Я развелась с ним и съехала.{w}\nМожет, мне стоит перебраться в Илиасбург?.."

    return


label city04_04a:
    show mob syouzyo with dissolve #700

    girl "Мне сказали остаться дома…{w}\nЯ буду молиться за всех…"

    return


label city04_04b:
    show mob syouzyo at xy(X=-160) #700
    show mob hapy3 at xy(X=160) as mob2
    with dissolve #701

    girl "У меня новая сестра! ♪"
    harpy_child "У меня новая сестра! ♪"

    return


label city04_05a:
    show mob syounen with dissolve #700

    boy "Я тоже хочу сражаться, но мне сказали сидеть дома…{w}\nКогда я вырасту, буду сражаться, чтобы всех защитить…"
    l "Если ты так думаешь, то ты уже силён."

    show alice st14b at xy(X=-110) #700
    show mob syounen at xy(X=160)
    with dissolve #701

    a "Надеюсь, ты вырастешь не таким же идиотом, разрушающим всё подряд, как Лука."
    l "Отстань от меня!"

    return


label city04_05b:
    show mob syounen at xy(X=-160) #700
    show mob hapy3 at xy(X=160) as mob2
    with dissolve #701

    boy "Когда я вырасту, я женюсь на этой девочке!"
    harpy_girl "Когда вырасту, я хочу иметь десять детей!{w}\nЯ рассчитываю на тебя, дорогой!"
    l "..."
    "Мило."

    hide mob
    hide mob2
    hide alice
    with dissolve #700

    return


label city04_06a:
    show mob rouba with dissolve #700

    elder "Ты думаешь, я ужасный человек...?{w}\nХотя если я хотела всех защитить, у меня не было другого выбора..."
    elder "Я совершила такую ужасную вещь, послав стольких на смерть...{w}\nКак мне искупить вину за то, что я сделала...?"

    return


label city04_06b:
    show mob rouzin1 with dissolve #700

    village_chief "Даже когда я пропал, похоже, моя жена всё держала под контролем.{w}\nВидимо, она больше подходит на роль старосты, нежели я…"

    show mob rouzin1 at xy(X=-160) #700
    show mob hapy2 at xy(X=160) as mob2
    with dissolve #701

    old_harpy "Не грусти.{w}\nЯ успокою тебя…"
    village_chief "О-хо-хо-хо. ♪"

    show mob rouzin1 at xy(X=-160) #700
    show mob rouba at xy(X=160) as mob2
    with dissolve #701

    village_chiefs_wife "Ты идиот!"
    village_chief "Гах!"

    return


label city04_10:
    show mob seinen1 with dissolve #700

    youth "Я хочу побыстрее закончить работу и поиграть со своей невестой!{w}\nУоо! Я утрою скорость!"

    return


label city04_11:
    show mob ozisan1 with dissolve #700

    old_man "Я так давно не работал на ферме, мне очень тяжело.{w}\nНо я сделаю всё возможное ради моей милой жёнушки и дочерей!"

    return


label city04_12:
    show hapy_a st12 with dissolve #700

    harpy "Эй-эй, ты вернулся!{w}\nТы решил взять меня с собой?"

    l "Ух..."
    "Действительно ли стоит это делать?"
    menu:
        "Взять её":
            l "Хорошо, почему бы и нет?{w}\nМожешь пойти вместе с нами."
            show hapy_a st12 with dissolve
            harpy "Еееей~!{image=note}"
            show alice st01b at xy(X=-133)
            show hapy_a st11 at xy(X=200)
            with dissolve
            a "..................."
            "Алиса просто уставилась на меня с недовольным выражением лица."
            stop music fadeout 1.0
            hide alice
            show hapy_a st11 at center
            with dissolve
            "............................."
            call join(7)
            pause 0.4
            play music "audio/bgm/yaei.ogg"
            l "Мы отправляемся завтра утром. Для тебя это нормально?"
            show hapy_a st12 with dissolve
            harpy "Океюшки!{w}\nВ таком случае я просто посплю вместе с вами, ребята!"
            show alice st04b at xy(X=-133)
            show hapy_a st11 at xy(X=200)
            with dissolve
            a "{i}вздох{/i}"
            if party_member[2] == 7:
                $ party1a = 1
            elif party_member[3] == 7:
                $ party2a = 1

            return

        "Оставить":
            l "Ух, нет...{w}\nПРости..."
            show hapy_a st11 with dissolve
            harpy "Ау, хорошо..."

            return

label city04_13:
    show hapy_bc st01 with dissolve #700

    younger_harpy "Может, нам собрать весь мёд?"
    older_harpy "Будем стараться весь день, а ночью — играть!"

    return


label city04_14:
    show queenharpy st01 with dissolve #700

    queen_harpy "Кажется, всё идёт довольно хорошо.{w}\nЭтому поспособствовали и мы, гарпии, помогая с фермерством и пчеловодством."
    queen_harpy "Надеюсь, этот мир будет длиться вечно..."

    return


label city05_main:
    call city_null
    $ city_bg = "bg 031"
    $ city_button01 = "Юноша" #601
    $ city_button02 = "Молодая женщина" #602
    $ city_button03 = "Богатая женщина" #603
    $ city_button04 = "Хулиган" #604
    $ city_button05 = "Солдат" #605
    $ city_button06 = "Мальчик" #606
    $ city_button10 = "Оружейный магазин" #610
    $ city_button11 = "Лавка" #611
    $ city_button12 = "Церковь" #612
    $ city_button13 = "Порт" #613

    if item01 == 5:
        $ city_button19 = "Пещера Сокровищ" #619
    elif item01 == 6:
        $ city_button19 = "Покинуть порт" #619

    while True:
        if item01 == 5:
            $ city_act19 = 1
        elif item01 == 6:
            $ city_act19 = 0

        call cmd_city

        if result == 1 and item01 == 5:
            call city05_01a
        elif result == 1 and item01 == 6:
            call city05_01b
        elif result == 2 and item01 == 5:
            call city05_02a
        elif result == 2 and item01 == 6:
            call city05_02b
        elif result == 3 and item01 == 5:
            call city05_03a
        elif result == 3 and item01 == 6:
            call city05_03b
        elif result == 4 and item01 == 5:
            call city05_04a
        elif result == 4 and item01 == 6:
            call city05_04b
        elif result == 5 and item01 == 5:
            call city05_05a
        elif result == 5 and item01 == 6:
            call city05_05b
        elif result == 6 and item01 == 5:
            call city05_06a
        elif result == 6 and item01 == 6:
            call city05_06b
        elif result == 10 and item01 == 5:
            call city05_10a
        elif result == 10 and item01 == 6:
            call city05_10b
        elif result == 11 and item01 == 5:
            call city05_11a
        elif result == 11 and item01 == 6:
            call city05_11b
        elif result == 12 and item01 == 5:
            call city05_12a
        elif result == 12 and item01 == 6:
            call city05_12b
        elif result == 13 and item01 == 5:
            call city05_13a
        elif result == 13 and item01 == 6:
            return
        elif result == 19:
            return


label city05_01a:
    show mob seinen1 with dissolve #700

    youth "Ранее в порту я видел маленькую лису.{w}\nЯ был удивлён, не думал, что они могут зайти так далеко от сельской местности."

    show alice st11b at xy(X=-120) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "… А давай найдём лису и съедим её."
    l "И это говорит та девушка, которая мне по пути сюда говорила, что не ест монстров, да?"
    show alice st13b with dissolve
    a "Ну... э...{w}\nЛисы не монстры!"
    l "......"

    return


label city05_01b:
    show mob seinen1 with dissolve #700

    translator "Кицуне такие пушистые и милые.{w}\nЯ пленён их красотой."

    show alice st11b at xy(X=-120) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "Надеюсь, лисы найдут тебя и вырвут все твои волосы."
    "Почему она так сильно их ненавидит...?"

    return


label city05_02a:
    show mob musume1 with dissolve #700

    young_woman "Легендарный пират Селена... Она властвовала над морями сто лет назад.{w}\nНесмотря на то, что она была женщиной, она возглавляла команду в сотни человек."
    young_woman "Я так завидую..."

    return


label city05_02b:
    show mob musume1 with dissolve #700

    young_woman "Однажды на Селену посреди шторма напали монстры и она смогла от них отбиться."
    young_woman "В последнем её путешествии она охотилась на какое-то загадочное сокровище.{w}\nНекоторые говорят, что её корабль затонул, но я не знаю…"
    young_woman "Я не помешена на пиратах.{w}\nЯ просто восхищаюсь ей…"

    return


label city05_03a:
    show mob madam with dissolve #700

    wealthy_woman "Раньше тут было так много роскошных магазинов…{w}\nЖемчуга, керамика, картины... Множество прекрасных товаров."
    wealthy_woman "Сейчас в продаже не осталось ничего ценного…"

    show alice st11b at xy(X=-120) #700
    show mob madam at xy(X=160)
    with dissolve #701

    a "Меня не интересуют несъедобные вещи."
    wealthy_woman "Продавали даже самые изысканные деликатесы…{w}\nРыба, конфеты, хлеб... Ах, как я это все обожала."
    wealthy_woman "Это всё из-за этих монстров…"

    show alice st13b at xy(X=-120) with dissolve #700

    a "Проклятые монстры, нет вам прощенья!"
    "Ты же сама монстр…"

    return


label city05_03b:
    show mob madam with dissolve #700

    wealthy_woman "Как обычно, покупать нечего.{w}\nБудь даже все деньги мира, здесь их негде тратить…"
    wealthy_woman "Может, стоит перебраться на Сентору?{w}\nНо я слышала, что там есть множество сильных монстров, нападающих на поселения…"
    wealthy_woman "Думаю, я не смогу этого вынести…"

    return


label city05_04a:
    show mob gorotuki1 with dissolve #700

    hooligan "Говорят, что на востоке находится пещера сокровищ.{w}\nТам столько золота и серебра, достаточно, чтобы безбедно прожить до конца своих дней…"
    hooligan "Теперь это прибежище монстров.{w}\nТот парень отправился за наживой, но так и не вернулся…"
    hooligan "Прости, брат…"

    return


label city05_04b:
    show mob gorotuki1 with dissolve #700

    hooligan "Что? Ты был в той пещере и смог вернуться?!{w}\nТы шутишь…"

    return


label city05_05a:
    show mob sensi1 with dissolve #700

    soldier "Монстры, находящиеся вдалеке от храма Илиас — самые сильные на континенте."
    soldier "Чем дальше на север ты зайдёшь, тем сильнее они будут, пока не доберёшься до замка Владыки монстров."

    return


label city05_05b:
    show mob sensi1 with dissolve #700

    soldier "Монстры здесь по силам обычным солдатам.{w}\nЯ бы не смог сражаться на южной окраине Сенторы."
    soldier "Наверное, люди там живут в страхе…"

    return


label city05_06a:
    show mob syounen with dissolve #700

    boy "Из-за монстра, преградившего кораблям путь в море, магазин моего отца закрылся.{w}\nНеужели монстры рады, доставляя нам столько проблем...?"
    l "........"
    "Мне нужно что-то сделать…"

    return


label city05_06b:
    show mob syounen with dissolve #700

    boy "Если бы Генрих был здесь, он бы смог остановить этого монстра."
    boy "Но Героев сейчас нет…{w}\nВсе только хорохорятся, а как только почувствуют опасность, убегают со всех ног."

    show alice st11b with dissolve #700

    a "Герои в наше время просто трусы.{w}\nДаже когда Гранберия напала, все убежали."
    a "У них даже нет права носить меч."
    l "Я понимаю твои чувства…{w}\nТолько получив благословение, они не являются настоящими Героями.{w}\nЛишь защищая слабых можно называться истинным Героем!"
    a "Фальшивый герой разглагольствует о героях."

    return


label city05_10a:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "Добро пожаловать в «Магазин «Вокруг света»», где собрано оружие со всего мира!"
    shopkeeper "... Но из-за того, что морской путь преграждён, у нас не так много товаров."
    "... На полках полно отличного оружия.{w}\nНо раз я не хочу убивать своих противников, я оставлю этот меч."
    shopkeeper "Если ничего тебя не заинтересует, я могу заточить имеющийся у тебя меч."
    shopkeeper "Позволишь взглянуть на твой меч? Наверное, он затупился после такого путешествия."

    show angelsword at xy(X=-150) #701
    show mob ozisan1 zorder 2 at xy(X=160)
    with dissolve #700

    l "Хорошо... Держите."
    shopkeeper "ААААА!!! Черт возьми, что это такое?!"
    "Похоже, от вида этого меча торговец перепугался до смерти."

    scene bg 031 with blinds
    return


label city05_10b:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "Если вскоре морской путь не откроется, мне придётся закрыть магазин."
    shopkeeper "Я не могу победить монстра, навлекающего шторм на суда...{w}\nВосемь поколений владения «Магазином «Вокруг света»..."

    scene bg 031 with blinds
    return


label city05_11a:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    master "Добро пожаловать в мой магазин... К несчастью, у нас не так много товаров с тех пор, как корабли не могут выйти в море."
    l "Если так… Наверное, это тяжело."
    "Столько проблем…{w}\nМне нужно разобраться с Альмой."

    scene bg 031 with blinds
    return


label city05_11b:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    master "Добро пожаловать! Пополнений пока не было."
    master "Я хотел отправиться в порт Наталия в этом году...{w}\nИнтересно, как поживает русалка Мана..."
    master "Упс! Неосторожное замечание об этом городе...{w}\nЕсли бы кто-то это услышал, священник прочёл бы мне трёхчасовую лекцию."

    show mob ozisan3 at xy(X=-160) #700
    show mob obasan at xy(X=160) as mob2
    with dissolve #701

    woman "Ты что-то сказал о Русалке?"
    master "Эммм… Ничего… Это…{w}\nЙАААААА!!!"

    scene bg 031 with blinds
    return


label city05_12a:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Помолимся же Илиас, дабы она прогнала монстра.{w}\nО, Илиас, даруй нам спасение."

    scene bg 031 with blinds
    return


label city05_12b:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Сколько бы я ни молился, буря продолжается.{w}\nО, Илиас, что мне сделать, чтобы ты помогла нам...?"
    priest "Ах! Это те люди в порту Наталия! Вероятно, они связаны с монстрами!{w}\nВот почему Илиас отрезала нас от них!"

    scene bg 031 with blinds
    return


label city05_13a:
    show mob hunanori with dissolve #700

    sailor "Интересно, как долго продержатся торговцы…{w}\nОдин за другим, они закрывают свои магазины."
    sailor "Единственное, что нам остаётся, рыбачить весь день…"
    l "Правда? Это, наверное, ужасно..."
    "Мне нужно чем-то помочь этим людям..."

    return
