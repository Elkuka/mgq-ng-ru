init python:
########################################
# SLIME
########################################
    def get_slime():
        girl = Chr()
        girl.unlock = persistent.slime_unlock == 1
        girl.name = "Девушка-слизь"
        girl.pedia_name = "Девушка-слизь"
        girl.info = _("""Though the Slime Girl is usually considered a weak monster, the ability to freely control the shapeof her body can make her a threatening enemy. A weak adventurer is sure to struggle against her.
They are usually happy, and not hostile towards humans. However since their staple food is male semen, they regularly seek out weak men to attack. They pleasure them by wrapping up the man's body in their slime to extract their semen. Keeping their catch stuck in their body, they are able to feed ten or more times on a single man. Most times after feeding, the Slime Girl will release her catch. However, there have been reports of the Slime Girl's taking their prey home if they really like the taste of his semen, keeping them there indefinitely.
In addition, male semen is required for Slime Girls to divide into new Slime Girls.""")
        girl.label = "slime"
        girl.label_cg = "slime_cg"
        girl.info_rus_img = "slime_pedia"
        girl.zukan_ko = "slime"
        girl.m_pose = slime_pose
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 50
        girl.x1 = 244
        girl.y0 = 80
        girl.lv = 1
        girl.max_hp = 20
        girl.hp = 20
        girl.skills = ((3001, "Slime Tentacle"),
                        (3002, "Slime Handjob"),
                        (3003, "Slime Heaven"))
        girl.scope = {"ruka_tati": 1,
                       "mylv": 80,
                       "skill01": 25,
                       "item01": 1}

        return girl

########################################
# ALICE1
########################################
    def get_alice1():
        girl = Chr()
        girl.unlock = persistent.alice1_unlock == 1
        girl.name = "Alice"
        girl.pedia_name = "Alice (1)"
        girl.info = _("""A mysterious Monster only matched by her mysterious power. With her identity unknown, it's unsure if there are even any monsters who can match her strength. Though it appears she means no harm to humans, she is merciless if one dares to challenge her. The price for challenging her is pricy, indeed.""")
        girl.label = "alice1"
        girl.label_cg = "alice1_cg"
        girl.info_rus_img = "alice1_pedia"
        girl.m_pose = alice1_pose
        girl.zukan_ko = "alice1"
        girl.facenum = 9
        girl.posenum = 8
        girl.x0 = -20
        girl.x1 = 150
        girl.lv = 145
        girl.max_hp = 32000
        girl.hp = 32000
        girl.echi = ["alice1_h", "lb_0050", "lb_0088x", "lb_0091", "lb_0135"]
        girl.scope = {"ruka_tati": 0,
                        "mylv": 2,
                        "skill01": 0,
                        "item01": 1}
        return girl

########################################
# NAME
########################################
    def get_slug():
        girl = Chr()
        girl.unlock = persistent.slug_unlock == 1
        girl.name = "Slug Girl"
        girl.pedia_name = "Slug Girl"
        girl.info = _("""With her body that of a soft slug, weak attacks will only get stuck in her mucus covered membrane. The Slug Girl's behavior is to take walks at night and attack any men she encounters. Like most monsters, her staple food is male semen. Using the lower half of her body, she carefully stimulates her catch to feed.
Once she catches a male in her slimy mucus membrane, they are forced to ejaculate with surprising ease. With that, she is able to feed.
In addition, if the Slug Girl particularly likes her catch, she will use the genitals in the slug half of her body to mate with her prey. Using the same techniques as feeding, she will force the male to release just enough semen for copulation.
Once a suitable male has been found to copulate with, the Slug Girl will continue to use him as both a mate and as a source of food for the rest of his life.""")
        girl.label = "slug"
        girl.label_cg = "slug_cg"
        girl.info_rus_img = "slug_pedia"
        girl.m_pose = slug_pose
        girl.zukan_ko = "slug"
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 3
        girl.x1 = 170
        girl.lv = 2
        girl.max_hp = 30
        girl.hp = 30
        girl.skills = ((3004, "Sticky Mucus"),
                       (3005, "Sticky Caress"),
                       (3006, "Slug Rape"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 2,
                      "skill01": 0,
                      "item01": 1}
        return girl

########################################
# MDG
########################################
    def get_mdg():
        girl = Chr()
        girl.unlock = persistent.mdg_unlock == 1
        girl.name = "Мандрагора"
        girl.pedia_name = "Мандрагора"
        girl.info = _("""A monster born from human blood and semen; usually buried underground, she is able to sleep for decades at a time. Normally the photosynthesis from their leaves above ground is enough to sustain them.
Mandragoras are mostly harmless, staying underground. However, if they are forced out against their will, they will let loose a powerful shriek that paralyzes all living beings that hear it. Left in a bad mood, they regularly attack whoever pulled them out. If it's a man, they are sure to supplement their diet with his semen.
As a plant that's dose to being human, Mandragoras have female genitals that can be used for both feeding and reproduction. In addition, they are known to use their mouths and breasts to wring semen from their prey.
As they are generally harmless to humans, it's wise to be wary of pulling them out from the ground.""")
        girl.label = "mdg"
        girl.label_cg = "mdg_cg"
        girl.info_rus_img = "mdg_pedia"
        girl.m_pose = mdg_pose
        girl.zukan_ko = "mdg"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -230
        girl.lv = 3
        girl.max_hp = 40
        girl.hp = 40
        girl.echi = ["mdg_ha", "mdg_hb"]
        girl.skills = ((3181, "Fellatio"),
                      (3182, "Tit Fuck"),
                      (3195, "Monster Hair"))
        girl.scope = {"ruka_tati": 0,
                     "mylv": 3,
                     "skill01": 1,
                     "item01": 1}
        return girl

########################################
# GRANBERIA1
########################################
    def get_granberia1():
        girl = Chr()
        girl.unlock = persistent.granberia1_unlock == 1
        girl.name = "Granberia"
        girl.pedia_name = "Granberia (1)"
        girl.info = _("""A powerful monster of the Dragon race, she is one of the Four Heavenly Knights. Devoting herself to the sword, there is nobody in the world who can match her flaming sword. Swearing loyalty to the Monster Lord, she takes great pride in her position. With a chivalrous spirit, she avoids useless violence against the weak.
Those of the Dragon race have an omnivorous diet like humans, but prefer semen. Even though Granberia generally doesn't feed on semen, unlike fellow members of her tribe, she sometimes cannot help herself playing with defeated men in the rush of battle.
Believing strongly in her chivalry, she only chooses to associate with the strong. With an unmatched swordswoman like Granberia, it's hard for her to acknowledge someone.""")
        girl.label = "granberia1"
        girl.label_cg = "granberia1_cg"
        girl.info_rus_img = "granberia1_pedia"
        girl.m_pose = granberia1_pose
        girl.zukan_ko = "granberia1"
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -185
        girl.lv = 126
        girl.max_hp = 24000
        girl.hp = 24000
        girl.scope = {"ruka_tati": 0,
                            "mylv": 3,
                            "skill01": 2,
                            "item01": 1}
        return girl

########################################
# MIMIZU
########################################
    def get_mimizu():
        girl = Chr()
        girl.unlock = persistent.mimizu_unlock == 1
        girl.name = "Earthworm Girl"
        girl.pedia_name = "Earthworm Girl"
        girl.info = _("""Similar to a normal earthworm, her entire body is slimy. Though she usually lives underground, Earthworm Girls are known to come above ground to attack men when they're hungry.
With their staple food being male semen, they are able to both absorb it through their skin and to suck it out directly with their tails. Their basic attack pattern is to wrap their bodies around their catch to seal off their movements as they feed. If their prey is combative, they can use their tails to forcefully suck out their food by forcing pleasure onto their catch.
As with feeding, when they wish to mate they use their tails. When they are finished feeding, in most cases they release their catch, however cases have been reported of men being brought back to the Earthworm Girl's underground nest to be kept as their mate if the male catches her interest.""")
        girl.label = "mimizu"
        girl.label_cg = "mimizu_cg"
        girl.info_rus_img = "mimizu_pedia"
        girl.m_pose = mimizu_pose
        girl.zukan_ko = "mimizu"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -185
        girl.lv = 3
        girl.max_hp = 50
        girl.hp = 50
        girl.skills = ((3007, "Sticky Mucus"),
                         (3008, "Tighten"),
                         (3009, "Sweet Worm"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 3,
                        "skill01": 2,
                        "item01": 5}
        return girl

########################################
# GOB
########################################
    def get_gob():
        girl = Chr()
        girl.unlock = persistent.gob_unlock == 1
        girl.name = "Goblin Girl"
        girl.pedia_name = "Goblin Girl"
        girl.info = _("""A monster, though still a young girl. Even so, she holds insane strength that shouldn't be underestimated. This Goblin Girl appears to have become a thief, stealing from travelers and merchants. In addition, she has been known to attack and rape young men.
Since their diet is similar to humans, she only attacks young men for fun. With her already tight vagina, the Goblin Girl also has muscles that she can control at will to adjust her own tightness. Her ultimate attack is [Super Tightening], in which she uses all of her muscles at the same time to force the person she's raping to orgasm in an instant.
After raping her targets for fun, she releases them. However, it seems she is looking for someone she really likes to take back to her hideout to keep for fun.""")
        girl.label = "gob"
        girl.label_cg = "gob_cg"
        girl.info_rus_img = "gob_pedia"
        girl.m_pose = gob_pose
        girl.zukan_ko = "gob"
        girl.facenum = 5
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 4
        girl.max_hp = 75
        girl.hp = 75
        girl.skills = ((3185, "Forceful Handjob"),
                      (3186, "Forceful Fellatio"),
                      (3187, "Forceful Intercrural"),
                      (3183, "Goblin Insertion"),
                      (3184, "Super Tightening"))
        girl.scope = {"ruka_tati": 0,
                     "mylv": 4,
                     "skill01": 2,
                     "item01": 5}
        return girl

########################################
# PRAMIA
########################################
    def get_pramia():
        girl = Chr()
        girl.unlock = persistent.pramia_unlock == 1
        girl.name = "Tiny Lamia"
        girl.pedia_name = "Tiny Lamia"
        girl.info = _("""A young Lamia that is still immature in both power and ability. One of the four bandits, she too steals from travelers and merchants. In addition, if her target is a weak man, she tries to wrap her small tail around them.
Following her Lamia instincts in wanting to coil around men, her weak tail is barely able to cause pain. However her tight vagina is more than enough to force a man to submit to her. If she manages to force a man into her, they are sure to be squeezed dry.
Her only purpose in attacking men is from instinct and the desire to feed. She is unable to rape for reproductive purposes yet.""")
        girl.label = "pramia"
        girl.label_cg = "pramia_cg"
        girl.info_rus_img = "pramia_pedia"
        girl.m_pose = pramia_pose
        girl.zukan_ko = "pramia"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -190
        girl.lv = 4
        girl.max_hp = 80
        girl.hp = 80
        girl.skills = ((3188, "Tiny Lick"),
                         (3189, "Tiny Tit Fuck"),
                         (3190, "Tiny Tightening"),
                         (3191, "Tiny Tailjob"),
                         (3010, "Death by Tightening"),
                         (3011, "Death by Pumping"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 4,
                        "skill01": 2,
                        "item01": 5}
        return girl

########################################
# VGIRL
########################################
    def get_vgirl():
        girl = Chr()
        girl.unlock = persistent.vgirl_unlock == 1
        girl.name = "Vampire Girl"
        girl.pedia_name = "Vampire Girl"
        girl.info = _("""Another young monster of the four bandits. Even though she is a young vampire, she already possesses some of the powerful abilities of adult vampires, such as the ability to transform into a bat and to use her eyes to control her targets mind.
Like an adult vampire, she is able to suck the energy from her prey, especially from a man's penis. Most men would be quick to give in to her energy sucking, happily giving both their energy and semen to her mouth. In addition, she is capable of sucking energy from her target with her vagina. Squeezing the man as if she was milking him, she can extract his energy. She enjoys playing with men like toys until they faint.
Even though she is young and immature, one must not forget that she is still a powerful vampire.""")
        girl.label = "vgirl"
        girl.label_cg = "vgirl_cg"
        girl.info_rus_img = "vgirl_pedia"
        girl.m_pose = vgirl_pose
        girl.zukan_ko = "vgirl"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -180
        girl.lv = 5
        girl.max_hp = 85
        girl.hp = 85
        girl.echi = ["vgirl_ha", "vgirl_hb"]
        girl.skills = ((3192, "Mantle Rub"),
                        (3193, "Panty Rub"),
                        (3012, "Energy Steal"),
                        (3013, "Eyes of Obedience"),
                        (3014, "Face Ride"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 5,
                       "skill01": 2,
                       "item01": 5}
        return girl

########################################
# DRAGONP
########################################
    def get_dragonp():
        girl = Chr()
        girl.unlock = persistent.dragonp_unlock == 1
        girl.name = "Dragon Pup"
        girl.pedia_name = "Dragon Pup"
        girl.info = _("""An energetic girl from the Dragon race. Able to eat
anything, she can live just like a human. Liking meat more than anything, they aren't known to attack men for semen. Even though she's young, she still retains the characteristics of full grown Dragons.
However when her opponent is a young man, it seems as though she can't help but rape him. Like others of the Dragon race, her body temperature is naturally high, which causes her vagina to be abnormally hot. Most men are forced to quickly submit to the combined tightness and heat of the Dragon Pup.
After a particularly fierce battle, she is not satisfied with just raping her opponent once. Submitting to her reproductive instincts, she will continue to rape her opponent over and over repeatedly. Even though she is a young girl, she is still a member of the dangerous dragon race.""")
        girl.label = "dragonp"
        girl.label_cg = "dragonp_cg"
        girl.info_rus_img = "dragonp_pedia"
        girl.m_pose = dragonp_pose
        girl.zukan_ko = "dragonp"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -180
        girl.lv = 5
        girl.max_hp = 90
        girl.hp = 90
        girl.scope = {"ruka_tati": 0,
                         "mylv": 5,
                         "skill01": 2,
                         "item01": 5}
        return girl

########################################
# MITUBATI
########################################
    def get_mitubati():
        girl = Chr()
        girl.unlock = persistent.mitubati_unlock == 1
        girl.name = "Bee Girl"
        girl.pedia_name = "Bee Girl"
        girl.info = _("""An insect type monster that resembles a bee. She carries a large source of honey with her at all times, but that alone is not enough to feed her.
The Bee Girl's favorite food is to mix their honey with male semen. To create this mixture, she will aggressively attack travelers. She will first cover her target with honey and then slowly lick it off as it mixes with his body fluids. Due to the combination of her incredibly long tongue and her sticky honey, she is able to easily force men to ejaculate. Forcing her prey to ejaculate as much as possible, she will feast on her mixture.
The Bee Girl is particularly fickle, and will generally get tired of the taste of their catch after around five ejaculations. After that, they are known to release their prey; however, if they find a rare taste that they really enjoy, they may keep their prey forever.""")
        girl.label = "mitubati"
        girl.label_cg = "mitubati_cg"
        girl.info_rus_img = "mitubati_pedia"
        girl.m_pose = mitubati_pose
        girl.zukan_ko = "mitubati"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -150
        girl.lv = 6
        girl.max_hp = 150
        girl.hp = 150
        girl.echi = ["mitubati_ha", "mitubati_hb"]
        girl.skills = ((3015, "Lick"),
                           (3016, "Binding Lick"),
                           (3221, "Nipple Lick"))
        girl.scope = {"ruka_tati": 0,
                          "mylv": 6,
                          "skill01": 2,
                          "item01": 5}
        return girl

########################################
# HAPY_A
########################################
    def get_hapy_a():
        girl = Chr()
        girl.unlock = persistent.hapy_a_unlock == 1
        girl.name = "Harpy"
        girl.pedia_name = "Harpy"
        girl.info = _("""A typical bird monster. Though there are some slight differences between Harpies, they generally have the same behaviors. Primarily herbivores, they generally feed on nuts, vegetables and bread. Though they don't feed on male semen, many Harpies are very eager to mate with humans. In addition, they are known to forcibly kidnap men and force them to marry.
In a form of female domination, they will force men to mate with them for reproductive purposes. Forcing men into them, their feathers tickle the male's body as the Harpies soft vagina quickly forces the male to inseminate them. The unique and forceful nature of this mating usually forces men to inseminate the Harpy within a minute.
If their partner has high quality sperm, the Harpy will force them to ejaculate many times to ensure fertilization. It's not uncommon for the mating to continue uninterrupted for multiple days in a row.
If the Harpy really enjoys their mate, they will continue to have sex, even if not for reproduction. A man forced to marry a Harpy will be unable to rest, and will spend almost all of their time in intercourse with the Harpy.""")
        girl.label = "hapy_a"
        girl.label_cg = "hapy_a_cg"
        girl.info_rus_img = "hapy_a_pedia"
        girl.m_pose = hapy_a_pose
        girl.zukan_ko = "hapy_a"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 6
        girl.max_hp = 150
        girl.hp = 150
        girl.skills = ((3017, "Harpy Footjob"),
                         (3018, "Wing Massage"),
                         (3019, "Harpy Rape"),
                         (3020, "Harpy Waist Shake"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 6,
                        "skill01": 2,
                        "item01": 5}
        return girl

########################################
# HAPY_BC
########################################
    def get_hapy_bc():
        girl = Chr()
        girl.unlock = persistent.hapy_bc_unlock == 1
        girl.name = "Harpy Twins"
        girl.pedia_name = "Harpy Twins"
        girl.info = _("""Close sisters that live in the Harpy Village. Not limited to Harpies, twins and sisters are actually quite common in the monster world. Since Harpies love mating, there are many sisters and twins that live in the village.
Living in close quarters in the village, Harpies have a very strong sense of community. The bond between sisters is especially strong. It isn't unheard of for them to share a husband. In this case, the elder sister is able to assist the less experienced younger sister. However in these cases, the man is exhausted twice as fast.
Luckily, there is a method handed down in the Harpy Village that is able to rejuvenate exhausted men through a combination of diet and other treatments.""")
        girl.label = "hapy_bc"
        girl.label_cg = "hapy_bc_cg"
        girl.info_rus_img = "hapy_bc_pedia"
        girl.m_pose = hapy_bc_pose
        girl.zukan_ko = "hapy_bc"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 6
        girl.max_hp = 120
        girl.hp = 120
        girl.skills = ((3021, "Harpy Footjob"),
                          (3022, "Feather Massage"),
                          (3023, "Wing Wrap Caress"),
                          (3024, "Harpy Blowjob"))
        girl.scope = {"ruka_tati": 0,
                         "mylv": 6,
                         "skill01": 2,
                         "item01": 5}
        return girl

########################################
# QUEENHArPY
########################################
    def get_queenharpy():
        girl = Chr()
        girl.unlock = persistent.queenharpy_unlock == 1
        girl.name = "Queen Harpy"
        girl.pedia_name = "Queen Harpy"
        girl.info = _("""Reigning over the other Harpies as their Queen, her dignity and strength cannot be matched by the other Harpies. Not only does she excel in physical and magical strength, but also in her ability to reproduce. To be able to have as many children as possible, her sexual organs are specialized to wring out a large amount of semen as quickly as possible.
Her specialized vagina has bump-like projections covering her soft walls. The stimulation will quickly force her partner to inseminate her. Continuous ejaculations aren't rare, either. Once a man has a taste of the Queen's vagina, it's likely they will be made a prisoner of the pleasure it can bring. When the Queen picks a mating partner, it is said that the mating will continue for three full days. Forcing 100 ejaculations at a minimum in this time, she is able to conceive over a dozen children.
It seems as though she is hesitating over something, and is putting out barely half of her power. Normally, a Hero would not stand a chance against her.""")
        girl.label = "queenharpy"
        girl.label_cg = "queenharpy_cg"
        girl.info_rus_img = "queenharpy_pedia"
        girl.m_pose = queenharpy_pose
        girl.zukan_ko = "queenharpy"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 50
        girl.max_hp = 300
        girl.hp = 300
        girl.skills = ((3025, "Harpy Footjob"),
                            (3026, "Harpy Blowjob"),
                            (3027, "Feather Massage"),
                            (3028, "Royal Tit Fuck"),
                            (3029, "Royal Pussy"),
                            (3030, "Royal Pumping"),
                            (3031, "Happiness Rondo"))
        girl.scope = {"ruka_tati": 0,
                           "mylv": 7,
                           "skill01": 2,
                           "item01": 5}
        return girl

########################################
# DELH_A
########################################
    def get_delf_a():
        girl = Chr()
        girl.unlock = persistent.delf_a_unlock == 1
        girl.name = "Dark Elf Fencer"
        girl.pedia_name = "Dark Elf Fencer"
        girl.info = _("""An Elf who has fallen to corruption is normally referred to as a Dark Elf. Giving in to darkness and degeneration of mind and spirit, their bodies turn black.
Though able to live as herbivores, Dark Elves prefer to have intercourse with humans and feed on semen. With magical power flowing through their sexual organs, they will violate men and corrupt them. As soon as a male ejaculates inside of them, they will fall into darkness. Once that happens, the male forgets about everything but pleasure. Like that, they will single-mindedly have sex with the Dark Elf until they waste away.
Once the Dark Elf is corrupted, they lose their reproductive ability. Their only purpose in attacking man is to sate their appetite lust.""")
        girl.label = "delf_a"
        girl.label_cg = "delf_a_cg"
        girl.info_rus_img = "delf_a_pedia"
        girl.m_pose = delf_a_pose
        girl.zukan_ko = "delf_a"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -100
        girl.x1 = 100
        girl.lv = 8
        girl.max_hp = 180
        girl.hp = 180
        girl.skills = ((3032, "Elf Blowjob"),
                         (3033, "Elf Handjob"),
                         (3034, "Elf Tit Fuck"),
                         (3035, "Kiss of Ecstasy"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 8,
                        "skill01": 2,
                        "item01": 5}
        return girl

########################################
# DELH_B
########################################
    def get_delf_b():
        girl = Chr()
        girl.unlock = persistent.delf_b_unlock == 1
        girl.name = "Dark Elf Mage"
        girl.pedia_name = "Dark Elf Mage"
        girl.info = _("""An Elf who has fallen to corruption, she uses her power to summon tentacles and fuses them with her own body in order to feed on her prey.
When she finds a man she likes, she will use her summoned tentacles to wrap around his body and seal his movement. Like that, she will wrap her tentacles around his genitals and suck out his semen. Summoned specifically to pleasure the male and suck out his semen, it is not possible to resist her. The tentacles secrete a special mucus as it completely wraps around the male's penis, forcing her prey to ejaculate many times.
Once caught, there is no chance to escape. Her prey's semen will be sucked until his death.""")
        girl.label = "delf_b"
        girl.label_cg = "delf_b_cg"
        girl.info_rus_img = "delf_b_pedia"
        girl.m_pose = delf_b_pose
        girl.zukan_ko = "delf_b"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -90
        girl.x1 = 150
        girl.lv = 8
        girl.max_hp = 185
        girl.hp = 185
        girl.skills = ((3036, "Tentacle Caress"),
                         (3037, "Bound Tentacle Caress"),
                         (3038, "Tentacle Drain"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 8,
                        "skill01": 2,
                        "item01": 5}
        return girl

########################################
# HIRU
########################################
    def get_hiru():
        girl = Chr()
        girl.unlock = persistent.hiru_unlock == 1
        girl.name = "Leech Girl"
        girl.pedia_name = "Leech Girl"
        girl.info = _("""With the lower body of a giant Leech, she is a ring type monster. Though she can feed on all of a human's bodily fluids, they will only catch men. Once the Leech Girl locates a healthy looking male, she will suck his entire body into her mouth as she wrings out all of his body fluids.
Able to feast on more than Semen, the Leech Girl will force the man to both sweat and urinate as she sucks everything out of her catch. Equipped with thousands of tongues inside her mouth, she is able to force pleasure into the man. With his body stuck in her mouth, there is no escape as he is forced to continually ejaculate. In addition, by tickling her prey she can force him to incontinence whenever she wishes.
Continuing her feast for multiple days, there are many men who are weakened so much that they die. If she finds a high quality catch, she can direct the semen he discharges in her mouth to her genitals for reproductive purposes.""")
        girl.label = "hiru"
        girl.label_cg = "hiru_cg"
        girl.info_rus_img = "hiru_pedia"
        girl.m_pose = hiru_pose
        girl.zukan_ko = "hiru"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 9
        girl.max_hp = 210
        girl.hp = 210
        girl.skills = ((3039, "Leech Tail"),
                       (3085, "Leech Fellatio"),
                       (3040, "Tentacle Hell"),
                       (3041, "Fluid Suck"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 9,
                      "skill01": 2,
                      "item01": 5}
        return girl

########################################
# RAHURE
########################################
    def get_rahure():
        girl = Chr()
        girl.unlock = persistent.rahure_unlock == 1
        girl.name = "Rafflesia Girl"
        girl.pedia_name = "Rafflesia Girl"
        girl.info = _("""A plant type monster that lives primarily in tropical forests. Though it appears to have two separate bodies, it is in fact a single being. She is able to provide her own food via photosynthesis, but requires men to breed.
To assist with this, the Rafflesia Girl has a special aroma she can discharge to put her partner in a trance. In this state, she can force him into her flower before he is able to run away. First, she will use her stamen to spread pollen over the man. Next, she will use her pistil to force him to ejaculate. As soon as her pistil is covered in semen, she will be fertilized.
In most cases, she releases the man after being pollinated; however, if they determine that their catch is of exceptionally high quality, they are known to keep the man as a reproductive slave.""")
        girl.label = "rahure"
        girl.label_cg = "rahure_cg"
        girl.info_rus_img = "rahure_pedia"
        girl.m_pose = rahure_pose
        girl.zukan_ko = "rahure"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 9
        girl.max_hp = 250
        girl.hp = 250
        girl.skills = ((3042, "Tentacle Caress"),
                         (3043, "Petal Massage"),
                         (3044, "Forced Pollination"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 9,
                        "skill01": 3,
                        "item01": 5}
        return girl

########################################
# ROPA
########################################
    def get_ropa():
        girl = Chr()
        girl.unlock = persistent.ropa_unlock == 1
        girl.name = "Roper Girl"
        girl.pedia_name = "Roper Girl"
        girl.info = _("""A meat eating monster that prefers to live in caves and tropical areas. An extremely cruel monster, it considers humans to be nothing but food. Even among other man-eating monsters, it is known for it's cruelty.
The Roper Girl uses it's many tentacles to wrap around her prey, and pulls them to her sticky body. Stuck to her body, she secretes digestive juices over the catch. To prevent men from struggling, she will force their penis into her genitals to calm them with pleasure. In that way the prey cannot escape once caught, doomed to be slowly digested as they are stuck to her body. Oddly enough, the Roper Girl does not feed on semen. She only uses pleasure to weaken her prey as she digests them.""")
        girl.label = "ropa"
        girl.label_cg = "ropa_cg"
        girl.info_rus_img = "ropa_pedia"
        girl.m_pose = ropa_pose
        girl.zukan_ko = "ropa"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 10
        girl.max_hp = 270
        girl.hp = 270
        girl.skills = ((3045, "Tentacle Caress"),
                       (3046, "Bound Tentacle Caress"),
                       (3047, "Bound Tentacle Tightening"),
                       (3048, "Predation"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 10,
                      "skill01": 3,
                      "item01": 5}
        return girl

########################################
# YOUKO
########################################
    def get_youko():
        girl = Chr()
        girl.unlock = persistent.youko_unlock == 1
        girl.name = "Kitsune"
        girl.pedia_name = "Kitsune"
        girl.info = _("""An animal-based monster of the fox family, Kitsunes are known to have the highest potential magical power among animal-type monsters. Though this Kitsune is still young, a normal human would struggle to fight her on equal terms.
Fascinated by male genitals, they will use any chance they can take to play with them with either their hands or tails. When played with by their soft tails, most men are unable to resist giving in to the Kitsune. In addition, a Kitsune's vagina is many times more pleasurable than a human female's. Able to skillfully control every muscle inside of them, they can adjust the tightness to whichever level they wish.
In the Kitsune family, to get married you only need your partner to orgasm three times. Once the man gives in to temptation three times, they are recognized as agreeing to be the Kitsune's husband. Unfortunately, due to the sexual skills of a Kitsune, if they fall in love with a man at first sight, the man will have no choice in the matter.""")
        girl.label = "youko"
        girl.label_cg = "youko_cg"
        girl.info_rus_img = "youko_pedia"
        girl.m_pose = youko_pose
        girl.zukan_ko = "youko"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 12
        girl.max_hp = 350
        girl.hp = 350
        girl.skills = ((3049, "Fluffy Tail"),
                        (3050, "Lick Lick"),
                        (3051, "Rub Rub"),
                        (3052, "Double Tail"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 10,
                       "skill01": 4,
                       "item01": 5}
        return girl

########################################
# MEDA
########################################
    def get_meda():
        girl = Chr()
        girl.unlock = persistent.meda_unlock == 1
        girl.name = "Meda"
        girl.pedia_name = "Meda"
        girl.info = _("""An insect-based monster, they are known to only live in dark caves. Though their eyesight is poor, their hearing and sense of smell are very strong.
They feed on body fluids of other living beings, but prefer male semen. As soon as a man enters a cave, they use their hearing to detect where they are in the cave. Using an opening in the lower half of their body that's filled with tentacles, they can easily force a man to ejaculate against their will. They do not release their prey after feeding once, and instead choose to continually squeeze out everything with their small tentacles.
In addition to feeding, her lower opening is used for reproduction.""")
        girl.label = "meda"
        girl.label_cg = "meda_cg"
        girl.info_rus_img = "meda_pedia"
        girl.m_pose = meda_pose
        girl.zukan_ko = "meda"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 10
        girl.max_hp = 280
        girl.hp = 280
        girl.skills = ((3053, "Tentacle"),
                       (3054, "Bound Tentacle"),
                       (3055, "Thousand Worms"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 10,
                      "skill01": 4,
                      "item01": 5}
        return girl

########################################
# KUMO
########################################
    def get_kumo():
        girl = Chr()
        girl.unlock = persistent.kumo_unlock == 1
        girl.name = "Spider Girl"
        girl.pedia_name = "Spider Girl"
        girl.info = _("""An insect-based monster from the spider family.
Primarily living in caves, they create giant webs to catch prey. An exceptionally cruel predator, they eat man and monster alike.
Though they are carnivorous, they treat male semen as a rare treat of protein. Once they catch a man, they will wrap them up in their webbing to seal off their movement. After that, they use their spinneret to squeeze out semen from their catch. Due to the complex nature of their spinneret, they are able to use both their muscles and the webbing itself to force the man to continually ejaculate.
After sucking out all of the man's semen, the Spider Girl will use external digestion to eat them. After creating a cocoon with the man inside, she injects her digestive juices directly into them by biting their neck. Like that, the man will slowly be digested from inside his own body as the Spider Girl sucks up the melted meat. It's said that her juices are designed to bring insane levels of pleasure to her prey to prevent them from struggling while being eaten.""")
        girl.label = "kumo"
        girl.label_cg = "kumo_cg"
        girl.info_rus_img = "kumo_pedia"
        girl.m_pose = kumo_pose
        girl.zukan_ko = "kumo"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 11
        girl.max_hp = 300
        girl.hp = 300
        girl.echi = ["kumo_ha", "kumo_hb"]
        girl.skills = ((3056, "Eight-Legged Massage"),
                       (3057, "Penis Cocoon"),
                       (3058, "Body Wrap"),
                       (3059, "Forced Violation"),
                       (3060, "Predation"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 11,
                      "skill01": 4,
                      "item01": 5,
                      "kousoku": 4}
        return girl

########################################
# MIMIC
########################################
    def get_mimic():
        girl = Chr()
        girl.unlock = persistent.mimic_unlock == 1
        girl.name = "Mimic"
        girl.pedia_name = "Mimic"
        girl.info = _("""A monster that hides in a treasure chest, it appears to be a part of the undead family. Their body is composed of the chest, and the digestive juices contained inside. When an adventurer opens the chest, the Mimic will drag them inside and close the lid, bathing them in her digestive juices.
A very timid monster, it spends almost it's entire life shut inside the box but once she catches her prey, her attitude completely changes as she enjoys dominating them. Dragging her prey inside of her, she enjoys watching their despair as they are slowly eaten. Moreover, Mimics will be sure to cover the male's genitals with her digestive juices first, cruelly watching their reactions. A man captured by a Mimic can only look forward to a disgraceful, miserable death.
There have been cases where the Mimic really enjoys the reactions of her catch and chooses to instead torment them for the rest of their life, or until they become bored. When this occurs, they will then eat their prey.""")
        girl.label = "mimic"
        girl.label_cg = "mimic_cg"
        girl.info_rus_img = "mimic_pedia"
        girl.m_pose = mimic_pose
        girl.zukan_ko = "mimic"
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 15
        girl.max_hp = 500
        girl.hp = 500
        girl.echi = ["mimic_ha", "mimic_hb"]
        girl.skills = ((3061, "Slow Swallow"),
                        (3062, "Swallow"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 11,
                       "skill01": 4,
                       "item01": 5}
        return girl

########################################
# NANABI
########################################
    def get_nanabi():
        girl = Chr()
        girl.unlock = persistent.nanabi_unlock == 1
        girl.name = "Nanabi"
        girl.pedia_name = "Nanabi"
        girl.info = _("""A powerful animal-based monster of the fox family and a subordinate of Tamamo; she is fiercely loyal.
Though she can eat anything, she prefers male semen above anything else. Using all seven of her tails, she will carefully stimulate the man's body. Using her tails, she holds down the male as she forces him to ejaculate. In addition, she is able to turn her tails into semen-sucking tentacles, sucking in the male's penis and using them to milk the man.
Though she normally just feeds, if she finds a male she likes, she will make him her mating partner. Using her amazing vagina, she will force the male to mate with her continually until she conceives.""")
        girl.label = "nanabi"
        girl.label_cg = "nanabi_cg"
        girl.info_rus_img = "nanabi_pedia"
        girl.m_pose = nanabi_pose
        girl.zukan_ko = "nanabi"
        girl.facenum = 4
        girl.posenum = 3
        girl.bk_num = 7
        girl.x0 = -200
        girl.lv = 45
        girl.max_hp = 2900
        girl.hp = 2900
        girl.echi = ["nanabi_ha", "nanabi_hb"]
        girl.skills = ((3063, "Fox Blowjob"),
                         (3064, "Fox Tit Fuck"),
                         (3065, "Tail Massage"),
                         (3066, "Bound Tail Massage"),
                         (3067, "Two Moons"),
                         (3068, "Seven Moons"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 12,
                        "skill01": 4,
                        "item01": 5}
        return girl

########################################
# TAMAMO1
########################################
    def get_tamamo1():
        girl = Chr()
        girl.unlock = persistent.tamamo1_unlock == 1
        girl.name = "Tamamo"
        girl.pedia_name = "Tamamo"
        girl.info = _("""Among animal-based monsters, Tamamo is the most powerful. One of the Four Heavenly Knights, she is a master of Earth magic. In addition, despite her looks, she holds powerful physical strength.
Proud of her fluffy nine tails, she enjoys using it to play with men. Binding the male up with her tails, Tamamo will tickle every corner of the man's body as she plays with him. There is no man who can resist this stimulation. Once she finishes playing, she almost always releases the man; however if she finds a man she really likes, she may make him into her lover. As the man would have already been made into a prisoner of her fluffy tail, there is no man who would be able to refuse her.
Also, she loves fried tofu.""")
        girl.label = "tamamo1"
        girl.label_cg = "tamamo1_cg"
        girl.info_rus_img = "tamamo1_pedia"
        girl.m_pose = tamamo1_pose
        girl.zukan_ko = "tamamo1"
        girl.facenum = 4
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -180
        girl.lv = 128
        girl.max_hp = 22000
        girl.hp = 22000
        girl.scope = {"ruka_tati": 0,
                         "mylv": 13,
                         "skill01": 4,
                         "item01": 6}
        return girl

########################################
# ALMA_ELMA1
########################################
    def get_alma_elma1():
        girl = Chr()
        girl.unlock = persistent.alma_elma1_unlock == 1
        girl.name = "Alma Elma"
        girl.pedia_name = "Alma Elma"
        girl.info = _("""Called the Queen Succubus, she is the most powerful of the succubi. One of the Four Heavenly Knights, she is a master of Wind magic. In addition, her flexible body makes her the most powerful hand to hand fighter of the Four Heavenly Knights. A moody woman, she is easy to hate. Living in the moment, all she cares about is her own pleasure. Though she has no sense of loyalty, she acknowledges the Monster Lord's power, and follows her instructions.
A lewd Monster, her only enjoyment in life is playing with men. Once she catches a man, she tries to finish him off using a wide variety of methods.
Using her tail, it's said that the man will faint after tasting the overwhelming pleasure. After squeezing every drop of semen out of the man, she will usually eat them. In addition to being able to eat semen with her tail, she can expand it to swallow men whole. alma_elma prefers powerful warriors who challenge her instead of weak ordinary men.""")
        girl.label = "alma_elma1"
        girl.label_cg = "alma_elma1_cg"
        girl.info_rus_img = "alma_elma1_pedia"
        girl.m_pose = alma_elma1_pose
        girl.zukan_ko = "alma_elma1"
        girl.facenum = 3
        girl.posenum = 4
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 125
        girl.max_hp = 15000
        girl.hp = 15000
        girl.skills = ((3069, "Tail Massage"),
                              (3070, "Tail Mucus"),
                              (3071, "Bound Tail Massage"),
                              (3072, "Tail Suck"),
                              (3173, "Tail Drain"))
        girl.scope = {"ruka_tati": 0,
                             "mylv": 13,
                             "skill01": 5,
                             "item01": 6}
        return girl

########################################
# NAMAKO
########################################
    def get_namako():
        girl = Chr()
        girl.unlock = persistent.namako_unlock == 1
        girl.name = "Sea Cucumber Girl"
        girl.pedia_name = "Sea Cucumber Girl"
        girl.info = _("""An extremely greedy monster, they infest the shorelines. Their staple food is male semen. When they find a healthy male, they attack and suck his body into her mouth. Looking like a huge female genital, she is able to control every muscle inside of her to force the man to ejaculate. Able to shrink her inner wall, the Sea Cucumber Girl is able to give an amazingly pleasurable squeezing stimulation. Like that, she can force her catch to ejaculate until they are sucked dry. Greedily sucking up everything, she is a very dangerous monster.
Unable to differentiate between feeding and reproduction, the semen she squeezes is used for conceiving children. In addition, there have been cases of Sea Cucumbers attacking human females and other monsters. Holding them in her mouth, she will squeeze out all of their bodily fluids.""")
        girl.label = "namako"
        girl.label_cg = "namako_cg"
        girl.info_rus_img = "namako_pedia"
        girl.m_pose = namako_pose
        girl.zukan_ko = "namako"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -100
        girl.lv = 14
        girl.max_hp = 400
        girl.hp = 400
        girl.skills = ((3073, "Semen Squeeze"),
                         (3074, "Ecstasy Rub"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 14,
                        "skill01": 5,
                        "item01": 7}
        return girl

########################################
# KAI
########################################
    def get_kai():
        girl = Chr()
        girl.unlock = persistent.kai_unlock == 1
        girl.name = "Shellfish Girl"
        girl.pedia_name = "Shellfish Girl"
        girl.info = _("""An aquatic monster that often infests beaches. Able to catch men off guard by her human female form, she catches men in her shell and squeezes out their semen. Since they do not generally eat too much, the Shellfish Girl doesn't kill her catch by sucking out too much. For some reason, they tend to fall in love very quickly, claiming that they have met their "Man of Destiny". Once they fall in love, they will confine him in her shell and forcefully mate. They mate by wrapping the man's penis directly in her fallopian tube. Unfortunately for the male, once the mating finishes, it isn't possible to separate from her. Like that, they will live out the rest of their lives together as a married couple. Though the Shellfish Girl doesn't intend harm, they are a rather troublesome monster.
In addition. Shellfish Girls are usually friends with Sea Cucumber Girls. It isn't unusual to see them playing together around Port Natalia.""")
        girl.label = "kai"
        girl.label_cg = "kai_cg"
        girl.info_rus_img = "kai_pedia"
        girl.m_pose = kai_pose
        girl.zukan_ko = "kai"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 14
        girl.max_hp = 380
        girl.hp = 380
        girl.skills = ((3075, "Tentacle Suction"),
                      (3076, "Love Jail Hand"),
                      (3077, "Love Jail Fellatio"),
                      (3078, "Love Jail Anal"))
        girl.scope = {"ruka_tati": 0,
                     "mylv": 14,
                     "skill01": 5,
                     "item01": 7}
        return girl

########################################
# LAMIA
########################################
    def get_lamia():
        girl = Chr()
        girl.unlock = persistent.lamia_unlock == 1
        girl.name = "Lamia"
        girl.pedia_name = "Lamia"
        girl.info = _("""A very famous monster with the lower body of a snake; within Lamias, there can be a very wide disparity in power. Older Lamias are assumed to have extremely powerful magic, however.
This particular Lamia has average power, but is extremely cruel. She enjoys coiling around her catch and hurting them. After weakening a man enough, she enjoys swallowing them whole. If she finds a high-quality male, it's possible that she will mate with them before eating. Like other Lamias, she has complete control over the muscles inside her vagina, allowing her to adjust the tightness to force men to ejaculate when she chooses. This is a very dangerous monster to run into as a man.
There have been instances of Lamias falling in love with men. Once they fall in love, they are extremely relentless in making the man hers. It's said that they get so jealous that they will stay coiled around the man for the rest of his life, never allowing him to escape from her love.""")
        girl.label = "lamia"
        girl.label_cg = "lamia_cg"
        girl.info_rus_img = "lamia_pedia"
        girl.m_pose = lamia_pose
        girl.zukan_ko = "lamia"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -200
        girl.lv = 14
        girl.max_hp = 580
        girl.hp = 580
        girl.echi = ["lamia_ha", "lamia_hb", "lamiab_h"]
        girl.skills = ((3079, "Tail Massage"),
                        (3080, "Constrict"),
                        (3081, "Coiled Handjob"),
                        (3082, "Coiled Fellatio"),
                        (3083, "Coiled Tit Fuck"),
                        (3084, "Swallow Whole"),
                        (3088, "Lamia Vagina"),
                        (3089, "Sure-Death Tightening"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 14,
                       "skill01": 5,
                       "item01": 7}
        return girl

########################################
# GRANBERIA2
########################################
    def get_granberia2():
        girl = Chr()
        girl.unlock = persistent.granberia2_unlock == 1
        girl.name = "Granberia"
        girl.pedia_name = "Granberia (2)"
        girl.info = _("""One of the Four Heavenly Knights, Granberia is a powerful magic swordswoman. A relatively young monster, she has devoted herself to the sword. Widely known for her power, human soldiers tremble at her name. She's proud of her skill, but has found capable opponents lacking recently. She's anxiously looking for a capable opponent in which she can use all of her techniques against.
On the other hand, she dislikes weak people who take up the sword anyway. Due to the increasing number of poor soldiers, she has become worried.""")
        girl.label = "granberia2"
        girl.label_cg = "granberia2_cg"
        girl.info_rus_img = "granberia2_pedia"
        girl.m_pose = granberia2_pose
        girl.zukan_ko = "granberia2"
        girl.facenum = 5
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -185
        girl.lv = 126
        girl.max_hp = 24000
        girl.hp = 24000
        girl.scope = {"ruka_tati": 0,
                            "mylv": 14,
                            "skill01": 5,
                            "item01": 7}
        return girl

########################################
# PAGE17
########################################
    def get_page17():
        girl = Chr()
        girl.unlock = persistent.page17_unlock == 1
        girl.name = "Page 17"
        girl.pedia_name = "Page 17"
        girl.info = _("""A very unusual monster that lives in a book. As soon as a man opens the book she's hiding in, she will pop out and attack them. If defeated by her, she will draw the man into her book and continually violate them. Since these types of monsters have very few opportunities to catch prey, they will draw them into their universe and squeeze semen from them for the rest of their lives. Since the flow of time inside their universe is different, it will feel like around 1,000 years for a human. Once the life of her prey is exhausted, she will wait for the next catch.
Her strange name comes from the page number in the book she's possessing. Since they don't usually come in contact with other monsters, it holds no special meaning.""")
        girl.label = "page17"
        girl.label_cg = "page17_cg"
        girl.info_rus_img = "page17_pedia"
        girl.m_pose = page17_pose
        girl.zukan_ko = "page17"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 15
        girl.max_hp = 160
        girl.hp = 160
        girl.skills = ((3090, "Sweet Pacifier"),
                         (3091, "Sweet Tit Fuck"),
                         (3092, "Pacifier Paralysis"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 15,
                        "skill01": 5,
                        "item01": 7}
        return girl

########################################
# PAGE257
########################################
    def get_page257():
        girl = Chr()
        girl.unlock = persistent.page257_unlock == 1
        girl.name = "Page 257"
        girl.pedia_name = "Page 257"
        girl.info = _("""A monster that lurks in books. Since these types rarely come in contact with humans, it's not rare to find one that has never seen a human male before. This particular monster has never had contact with a man, and only has information on them she has gleaned from books. Therefore, she is very curious to find out more. If caught by her, it's likely the man will be played and experimented on until she's satisfied. Since she enjoys the taste of semen, it's easy to believe the experiments would continue until the man's death.
It seems as though she has been waiting for a catch ever since being born. When not moving from the book, she's able to go around 100 years before dying of hunger.""")
        girl.label = "page257"
        girl.label_cg = "page257_cg"
        girl.info_rus_img = "page257_pedia"
        girl.m_pose = page257_pose
        girl.zukan_ko = "page257"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 15
        girl.max_hp = 500
        girl.hp = 500
        girl.skills = ((3093, "Poke"),
                          (3094, "Stroke"),
                          (3095, "Massage"),
                          (3096, "Fiddle"))
        girl.scope = {"ruka_tati": 0,
                         "mylv": 15,
                         "skill01": 5,
                         "item01": 7}
        return girl

########################################
# PAGE65537
########################################
    def get_page65537():
        girl = Chr()
        girl.unlock = persistent.page65537_unlock == 1
        girl.name = "Page 65537"
        girl.pedia_name = "Page 65537"
        girl.info = _("""A Queen-like monster that possesses a book. Having possessed many different books up until now, her name is derived from how many different pages she has possessed. Boasting very powerful magic, any man who opens the book she is in will be quickly dragged into her world. Dragging them into her body, she is able to force any man to ejaculate. Any man caught by her will be forced to continuously ejaculate until nothing is left.
Unusual among other monsters that possess books, she prefers to quickly squeeze her catches to death instead of keeping them alive.
In addition, it is extremely difficult to seal her. It appears as though someone created her with this particular characteristic, but the details are not known. It appears as though Page 65537 is on a quest to learn more about the meaning and purpose of her existence.""")
        girl.label = "page65537"
        girl.label_cg = "page65537_cg"
        girl.info_rus_img = "page65537_pedia"
        girl.m_pose = page65537_pose
        girl.zukan_ko = "page65537"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 24
        girl.max_hp = 2100
        girl.hp = 2100
        girl.skills = ((3097, "Tentacle Massage"),
                            (3098, "Tentacle Hell"),
                            (3099, "Forced Semen Squeeze"),
                            (3100, "Eye of Paralysis"),
                            (3101, "Eye of Submission"))
        girl.scope = {"ruka_tati": 0,
                           "mylv": 15,
                           "skill01": 5,
                           "item01": 7,
                           "sinkou": 6}
        return girl

########################################
# KANI
########################################
    def get_kani():
        girl = Chr()
        girl.unlock = persistent.kani_unlock == 1
        girl.name = "Crab Girl"
        girl.pedia_name = "Crab Girl"
        girl.info = _("""An aquatic based monster, she appears mainly on beaches. Protected by her powerful shell, she uses her claws to hold down her prey. An extreme neat-freak, she has a fanatical obsession with keeping her catch clean. Therefore, after catching a man, she will carefully clean him. Carefully scrubbing and cleaning the man's genitals, it's said that the pleasure is addictive. Each ejaculation causes the Crab Girl to have to start washing all over, entering into an almost endless loop. In addition, the semen that covers her hands is used for food. Moreover, if her catch is of high quality, it's said that she will mate after the cleaning has finished. Even though the washing brings the male close to death, the Crab Girl will release her catch.
She is known to be extremely vengeful if made into an enemy.""")
        girl.label = "kani"
        girl.label_cg = "kani_cg"
        girl.info_rus_img = "kani_pedia"
        girl.m_pose = kani_pose
        girl.zukan_ko = "kani"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 16
        girl.max_hp = 450
        girl.hp = 450
        girl.skills = ((3102, "Washwash"),
                       (3103, "Bubble Wash"),
                       (3104, "Bubble Scrub"),
                       (3105, "Bubble Handjob"),
                       (3106, "Full-Body Bubble Massage"),
                       (3107, "Melty Wash"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 16,
                      "skill01": 5,
                      "item01": 8}
        return girl

########################################
# KURAGE
########################################
    def get_kurage():
        girl = Chr()
        girl.unlock = persistent.kurage_unlock == 1
        girl.name = "Jellyfish Girl"
        girl.pedia_name = "Jellyfish Girl"
        girl.info = _("""An aquatic monster that lives in the sea. The Jellyfish Girl usually feeds by grabbing men off of boats with her tentacle; she will also capture men who have fallen into the sea. Her venomous tentacles are able to paralyze her prey so that they are unable to move as they are eaten.
Though she is able to feed by using her elastic tentacles, the Jellyfish Girl is also known to force her prey into her umbrella. Once her prey is inside her umbrella, she will squeeze his entire body, and force them to ejaculate. After ensuring she has squeezed out all of his semen, she will then eat the man. With her stomach located in her umbrella, she is able to digest him as is.
Her digestive juices have extremely powerful aphrodisiac effects. She uses her juices to ensure that her prey doesn't struggle while inside her soft body.""")
        girl.label = "kurage"
        girl.label_cg = "kurage_cg"
        girl.info_rus_img = "kurage_pedia"
        girl.m_pose = kurage_pose
        girl.zukan_ko = "kurage"
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 16
        girl.max_hp = 550
        girl.hp = 550
        girl.skills = ((3108, "Tentacle Caress"),
                         (3109, "Paralytic Tentacle"),
                         (3110, "Tentacle Penis Torture"),
                         (3111, "Jelly Prison"),
                         (3112, "Predation"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 16,
                        "skill01": 5,
                        "item01": 8}
        return girl

########################################
# ISO
########################################
    def get_iso():
        girl = Chr()
        girl.unlock = persistent.iso_unlock == 1
        girl.name = "Sea Anemone Girl"
        girl.pedia_name = "Sea Anemone Girl"
        girl.info = _("""An aquatic monster that lives on the bottom of the sea. Like the Jellyfish Girl, she uses her tentacles to catch men off of boats and beaches, drawing them to her. Unlike the Jellyfish Girl, she uses her tentacles to hurt and torture her prey to weaken them, instead of pleasuring them. Once weakened, she uses her tentacles to force him to ejaculate continuously so she can feed on his semen.
After humiliating her prey, she will draw him into her. Once inside, she will start to digest him at the same time she squeezes the rest of his semen out. Extremely cruel and almost impossible to escape from once caught. Luckily, they are few in number.""")
        girl.label = "iso"
        girl.label_cg = "iso_cg"
        girl.info_rus_img = "iso_pedia"
        girl.m_pose = iso_pose
        girl.zukan_ko = "iso"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 16
        girl.max_hp = 160
        girl.hp = 160
        girl.skills = ((3113, "Tentacle Penis Caress"),
                      (3114, "Full-Body Tentacle Caress"),
                      (3115, "Tentacle Trance"),
                      (3116, "Semen Squeeze Movement"),
                      (3117, "Predation"))
        girl.scope = {"ruka_tati": 0,
                     "mylv": 16,
                     "skill01": 5,
                     "item01": 8}
        return girl

########################################
# ANKOU
########################################
    def get_ankou():
        girl = Chr()
        girl.unlock = persistent.ankou_unlock == 1
        girl.name = "Anglerfish Girl"
        girl.pedia_name = "Anglerfish Girl"
        girl.info = _("""A large fish monster, she is close to the mermaid family. However since she lives in the deep sea, her face and behavior are much different. Unable to see due to living in the darkness of the deep sea, she uses temperature to detect her prey. Her abdomen has a mouth that is not seen in other monsters. As if a digestive organ, her flesh is able to suck in and assimilate her prey. Once her prey gets stuck in her flesh, they are slowly dragged into the Anglerfish Girl's body. Even after being assimilated, it appears as though the man's consciousness remains. Like that, the man will remain aware of being one with the Anglerfish Girl, but unable to speak or do anything.
Her digestive organ is able to bring extreme pleasure to the man as it fuses him with her. Like that, when her catch is a man, she can obtain additional food.""")
        girl.label = "ankou"
        girl.label_cg = "ankou_cg"
        girl.info_rus_img = "ankou_pedia"
        girl.m_pose = ankou_pose
        girl.zukan_ko = "ankou"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 16
        girl.max_hp = 270
        girl.hp = 270
        girl.skills = ((3118, "Absorb"),)
        girl.scope = {"ruka_tati": 0,
                       "mylv": 16,
                       "skill01": 5,
                       "item01": 8}
        return girl

########################################
# KRAKEN
########################################
    def get_kraken():
        girl = Chr()
        girl.unlock = persistent.kraken_unlock == 1
        girl.name = "Kraken"
        girl.pedia_name = "Kraken"
        girl.info = _("""A monster entrusted to rule over the Southern Seas by the Monster Lord. From the same family as the previous Queens, she rules over half of the ocean. As well as performing various ceremonies, she is also the judge for any crimes committed in the ocean. Any man who harms the monsters of the seas is brought before her to be given a punishment fit for their crime. For severe crimes, the Queen is known to perform the punishment herself. She prefers to use her tentacles to disgrace and shame men, and in some cases likes to violate them anally for maximum humiliation. She does not like to kill, so after the punishment is finished she usually releases the criminal.
A fairly clumsy monster, but due to her caring heart for her subjects, there usually aren't any big problems. However, due to her clumsy nature, it seems there are a lot of false charges against humans.""")
        girl.label = "kraken"
        girl.label_cg = "kraken_cg"
        girl.info_rus_img = "kraken_pedia"
        girl.m_pose = kraken_pose
        girl.zukan_ko = "kraken"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 60
        girl.max_hp = 4700
        girl.hp = 4700
        girl.skills = ((3119, "Tentacle Caress"),
                         (3120, "Tentacle Raid"),
                         (3121, "Bound Tentacle Caress"),
                         (3122, "Bound Tentacle Raid"),
                         (3123, "Tentacle Smash"),
                         (3124, "Tentacle Heaven"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 17,
                        "skill01": 5,
                        "item01": 8}
        return girl

########################################
# MEIA
########################################
    def get_meia():
        girl = Chr()
        girl.unlock = persistent.hsean_meia1 == 1
        girl.name = "Meia"
        girl.pedia_name = "Meia"
        girl.info = _("""A young mermaid that lives in Port Natalia. Though mermaids used to tempt men into mating with them, they now live peacefully with humans in Port Natalia. With their diet being almost identical to a human's, their main reason to associate with humans is for reproduction. While most mermaids in Port Natalia have jobs and live in peace, there is some occasional trouble.
Easy-going, clumsy, and optimistic; Meia shares many of the common behaviors of her fellow mermaids. With the ability to manipulate water, Meia is fairly powerful even among her fellow mermaids. She uses her powerful magic to keep her spouse young. She separates chastity into mind and body. As long as her love remains for a single person, she doesn't consider using her body a form of adultery.""")
        girl.label = "meia"
        girl.label_cg = "meia_cg"
        girl.info_rus_img = "meia_pedia"
        girl.m_pose = meia_pose
        girl.zukan_ko = "meia"
        girl.zukan_ko = None
        girl.facenum = 4
        girl.posenum = 1
        girl.x0 = -150
        girl.lv = 17
        girl.max_hp = 500
        girl.hp = 500
        girl.echi = ["lb_0107a"]
        girl.scope = {"ruka_tati": 0,
                      "mylv": 17,
                      "skill01": 5,
                      "item01": 8}
        return girl

########################################
# GHOST
########################################
    def get_ghost():
        girl = Chr()
        girl.unlock = persistent.ghost_unlock == 1
        girl.name = "Ghost Girl"
        girl.pedia_name = "Ghost Girl"
        girl.info = _("""A collection of human souls that was infused with magic. It prefers dark, damp places and avoids sunlight at all costs. Though her face resembles the face of one of the souls, she has no memory of any previous life. Desiring nothing but the energy of living beings, it's difficult for her to communicate.
She needs male energy to maintain her form, so she will go to any length to get it. Her vagina has been changed to be as close to perfection as possible, as her continued existence depends on forcing the man to ejaculate. Unable to control herself once catching a man, she will continue squeezing him until his death.
Since she is a collection of souls with no physical body, physical attacks do not work. However, an attack imbued with the power of darkness is said to be quite effective.""")
        girl.label = "ghost"
        girl.label_cg = "ghost_cg"
        girl.info_rus_img = "ghost_pedia"
        girl.m_pose = ghost_pose
        girl.zukan_ko = "ghost"
        girl.facenum = 2
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 17
        girl.max_hp = 480
        girl.hp = 480
        girl.skills = ((3125, "Ghost Blowjob"),
                        (3126, "Ghost Tit Fuck"),
                        (3127, "Ghost Handjob"),
                        (3128, "Ghost Rape"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 17,
                       "skill01": 5,
                       "item01": 8}
        return girl

########################################
# DOLL
########################################
    def get_doll():
        girl = Chr()
        girl.unlock = persistent.doll_unlock == 1
        girl.name = "Cursed Doll"
        girl.pedia_name = "Cursed Doll"
        girl.info = _("""When the soul of the previous owner of this worn-out doll combined with magic after her death, she was forced to possess the doll and become a monster. Because of that, she is classified in the undead family. Though the memory from her human life has vanished, she retained her innocent, playful demeanor.
Like most undead types, she requires male energy to maintain her life. In addition to using her genitals to suck out energy, the Cursed Doll is able to use her magical hair to wrap around the man's penis and directly suck out his energy. Like this, it's said that she can steal the very soul of the man. Due to her innocent and playful behavior, she doesn't realize that her excessive playing is killing the man. An extremely dangerous monster, not to be underestimated by her young appearance.""")
        girl.label = "doll"
        girl.label_cg = "doll_cg"
        girl.info_rus_img = "doll_pedia"
        girl.m_pose = doll_pose
        girl.zukan_ko = "doll"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -20
        girl.x1 = 180
        girl.lv = 17
        girl.max_hp = 680
        girl.hp = 680
        girl.echi = ["doll_ha", "doll_hb"]
        girl.skills = ((3129, "Playful Handjob"),
                       (3130, "Playful Hug"),
                       (3131, "Playful Hair"),
                       (3132, "Soul Sucking Hair"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 17,
                      "skill01": 5,
                      "item01": 8}
        return girl

########################################
# ZONBE
########################################
    def get_zonbe():
        girl = Chr()
        girl.unlock = persistent.zonbe_unlock == 1
        girl.name = "Zombie Girl"
        girl.pedia_name = "Zombie Girl"
        girl.info = _("""Magic collected in a human corpse that turns it into an undead monster. Due to the brain rotting away, the Zombie Girl has almost no intellect, and barely any memory of her previous life. Cold and craving warmth, they desperately attack and rape men trying to reclaim the faint memories of their old bodies. Though they are very slow, their vitality is very high. Once they mount a man, it's not possible to get them off no matter how much you attack them. The strange feeling of her rotting genitals is able to force men to ejaculate. Since the Zombie Girl will never reclaim what she has lost, she will continue raping the man until he dies. In some rare cases, the magic sustaining the Zombie Girl will flow into the man and turn him into a Zombie. Like that, he will be raped for an eternity.""")
        girl.label = "zonbe"
        girl.label_cg = "zonbe_cg"
        girl.info_rus_img = "zonbe_pedia"
        girl.m_pose = zonbe_pose
        girl.zukan_ko = "zonbe"
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 45
        girl.x1 = 245
        girl.lv = 18
        girl.max_hp = 1400
        girl.hp = 1400
        girl.skills = ((3086, "Corrupted Kiss"),
                        (3087, "Thirsting Mouth"),
                        (3133, "Pointless Mating"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 18,
                       "skill01": 5,
                       "item01": 8}
        return girl

########################################
# ZONBES
########################################
    def get_zonbes():
        girl = Chr()
        girl.unlock = persistent.zonbes_unlock == 1
        girl.name = "Zombie Girls"
        girl.pedia_name = "Zombie Girl (Group)"
        girl.info = _("""Since Zombie Girls have no intelligence, they have no sense of companionship, not even recognizing other Zombie Girls. The only exception to this is when they attack a man in a group. Once they defeat him, they will take turns raping him. Whichever Zombie Girl is strongest will go first as the rest of them lick and bite at him. It's said that as soon as a man's semen touches a Zombie Girl, they will momentarily weaken as they experience the faint warmth. As soon as they weaken, the next strongest Zombie Girl will take the chance to force her off the man so that she can take over the rape. This pattern will continue endlessly.
In addition, the specially created Zombie Girls in the northern haunted mansion have no infectious diseases or hygiene issues due to the care of their creator.""")
        girl.label = "zonbes"
        girl.label_cg = "zonbes_cg"
        girl.info_rus_img = "zonbes_pedia"
        girl.m_pose = zonbes_pose
        girl.zukan_ko = "zonbes"
        girl.facenum = 2
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = 80
        girl.x1 = 280
        girl.lv = 18
        girl.max_hp = 2400
        girl.hp = 2400
        girl.skills = ((3134, "Corrupted Nipple Lick"),
                         (3135, "Corrupted Bite"),
                         (3136, "Corrupted Tit Fuck"),
                         (3137, "Pointless Mating"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 18,
                        "skill01": 5,
                        "item01": 8}
        return girl

########################################
# FREDERIKA
########################################
    def get_frederika():
        girl = Chr()
        girl.unlock = persistent.frederika_unlock == 1
        girl.name = "Frederika"
        girl.pedia_name = "Frederika"
        girl.info = _("""The ultimate zombie of the necromancer Chrome. With her core body based off a human named Frederika, Chrome used multiple parts of other corpses to improve her body. Because of all the additions, her body became almost that of a giant. She has incredible physical power, and enough intelligence to follow any order Chrome gives her. Though Chrome takes great pride in Frederika, she still wishes to improve her even more. Taking great pains to improve Frederika, Chrome meticulously constructed her vagina to near perfection. Any man forced into her will be forced to ejaculate continuously. Under Chrome's orders, it's believed that the amount of men raped to death by Frederika is quite high.
Chrome had nothing to do with Frederika's natural death. Frederika's natural affinity for magic caused her to be susceptible to a deadly disease, which brought her death.""")
        girl.label = "frederika"
        girl.label_cg = "frederika_cg"
        girl.info_rus_img = "frederika_pedia"
        girl.m_pose = frederika_pose
        girl.zukan_ko = "frederika"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 7
        girl.x0 = -72
        girl.x1 = 88
        girl.lv = 27
        girl.max_hp = 2100
        girl.hp = 2100
        girl.skills = ((3138, "Corrupted Intercrural"),
                            (3139, "Corrupted Blowjob"),
                            (3140, "Corrupted Tit Fuck"),
                            (3141, "Immoral Trio"),
                            (3142, "Corrupted Footjob"),
                            (3143, "Pointless Mating"))
        girl.scope = {"ruka_tati": 0,
                           "mylv": 18,
                           "skill01": 5,
                           "item01": 8}
        return girl

########################################
# CHROM
########################################
    def get_chrom():
        girl = Chr()
        girl.unlock = persistent.chrom_unlock == 1
        girl.name = "Chrome"
        girl.pedia_name = "Chrome"
        girl.info = _("""A monster with top-notch necromantic abilities. Chrome is a type of succubus, but her powers of seduction aren't too high. She's obsessed with satisfying her curiosity and is going to extreme lengths to create the perfect Zombie. Chrome believes that Frederika, her ultimate creation, still holds room for improvement. Though her powers of seduction as a succubus aren't very high, her ability to summon zombies is very dangerous. Even a skilled adventurer can quickly be overwhelmed by the zombies she summons.
Up until now, her crimes were limited to grave robbing. Recently, she's taken a great interest in genital experiments between living humans and her zombies. If not rooted out by Alice and Luka, she may have gone on to even more inhuman experiments. However, after being terrified by Alice's assault, she regretfully discontinued her zombie experiments. She returned to her residence and is now researching ghost monsters...""")
        girl.label = "chrom"
        girl.label_cg = "chrom_cg"
        girl.info_rus_img = "chrom_pedia"
        girl.m_pose = chrom_pose
        girl.zukan_ko = "chrom"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -25
        girl.x1 = 175
        girl.lv = 21
        girl.max_hp = 1200
        girl.hp = 1200
        girl.skills = ((3144, "Thirsting Arms"),
                        (3145, "Thirsting Mouth"),
                        (3146, "I'll Give An Injection!"),
                        (3147, "I'll Give A Handjob!"),
                        (3148, "I'll Give A Blowjob!"),
                        (3149, "I'll Rape You!"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 18,
                       "skill01": 5,
                       "item01": 8}
        return girl

########################################
# FAIRY
########################################
    def get_fairy():
        girl = Chr()
        girl.unlock = persistent.fairy_unlock == 1
        girl.name = "Fairy"
        girl.pedia_name = "Fairy"
        girl.info = _("""A generic name for the different types of fairies all over the world. Extremely curious and optimistic, they live a care free life. They love being mischievous and playing pranks, and are known to even venture into human villages to play tricks on the humans. They're very curious about males, and love to play with a man's genitals. The act of ejaculation is especially interesting to them, so they are known to use various plants to force men to ejaculate for their amusement. They will drink the semen to quench their thirst, but the majority of their nutrition comes from the various nectar and honey found in flowers.
It's said that many men are unable to even see fairies. Devout believers in llias are said to be included among those who cannot see fairies. Due to that, fairies are assumed to only be seen by those with weak faith.""")
        girl.label = "fairy"
        girl.label_cg = "fairy_cg"
        girl.info_rus_img = "fairy_pedia"
        girl.m_pose = fairy_pose
        girl.zukan_ko = "fairy"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = 70
        girl.x1 = 270
        girl.lv = 17
        girl.max_hp = 600
        girl.hp = 600
        girl.skills = ((3150, "Succubus Flower"),
                        (3151, "Wing Tickle"),
                        (3152, "Fairy Pranks"),
                        (3153, "Fairy Hug"),
                        (3154, "Fairy Dance"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 18,
                       "skill01": 6,
                       "item01": 8}
        return girl

########################################
# ELF
########################################
    def get_elf():
        girl = Chr()
        girl.unlock = persistent.elf_unlock == 1
        girl.name = "Elf"
        girl.pedia_name = "Elf"
        girl.info = _("""The Guardians of the Forest , Elves are a type of sub-monster that live in the forest. They like to take the role of the innocent fairies parent. Due to their role of defending the forests, there have been many instances of Elves fighting with humans. Purely defensive in nature, they will only attack a human who has entered their domain.
To punish the trespassers, they will rape and discipline them. Though they use the pretext of punishment, in most cases it's done for their own pleasure and enjoyment, or even for reproduction.
Primarily vegetarians, they normally don't eat male semen. If they develop a taste for male semen, it begins to affect both their mind and body. This process is called [Degeneration], and will slowly corrupt the Elf into a new type called a [Dark Elf].""")
        girl.label = "elf"
        girl.label_cg = "elf_cg"
        girl.info_rus_img = "elf_pedia"
        girl.m_pose = elf_pose
        girl.zukan_ko = "elf"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -100
        girl.x1 = 100
        girl.lv = 18
        girl.max_hp = 700
        girl.hp = 700
        girl.skills = ((3155, "Elf Handjob"),
                      (3156, "Elf Blowjob"),
                      (3157, "Elf Tit Fuck"),
                      (3158, "Elf Waist Shake"),
                      (3159, "Elf Tightening"))
        girl.scope = {"ruka_tati": 0,
                     "mylv": 18,
                     "skill01": 6,
                     "item01": 8}
        return girl

########################################
# TFAIRY
########################################
    def get_tfairy():
        girl = Chr()
        girl.unlock = persistent.tfairy_unlock == 1
        girl.name = "Twin Fairies"
        girl.pedia_name = "Twin Fairies"
        girl.info = _("""Fairies generally live and play in groups. If a man is caught by a group of fairies, they will work together to play with him. Very eager for interesting things to play with, it's said that if a man is captured by multiple fairies, he will be forced to remain their plaything for a long time. Once made into a toy, the fairies will care for him like a pet. Even though they aren't hostile towards men, their innocence and desire to play is just as dangerous.""")
        girl.label = "tfairy"
        girl.label_cg = "tfairy_cg"
        girl.info_rus_img = "tfairy_pedia"
        girl.m_pose = tfairy_pose
        girl.zukan_ko = "tfairy"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = 20
        girl.x1 = 220
        girl.lv = 19
        girl.max_hp = 700
        girl.hp = 700
        girl.skills = ((3160, "Succubus Flower"),
                         (3161, "Twin Wing Tickle"),
                         (3162, "Twin Fairy Pranks"),
                         (3163, "Twin Fairy Hug"),
                         (3164, "Twin Fairy Dance"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 19,
                        "skill01": 6,
                        "item01": 8}
        return girl

########################################
# FAIRYS
########################################
    def get_fairys():
        girl = Chr()
        girl.unlock = persistent.fairys_unlock == 1
        girl.name = "Fairies"
        girl.pedia_name = "Fairies"
        girl.info = _("""When a large group of fairies come together, they usually have a leader. If someone antagonizes the fairies, the leader is said to instruct the others in how to attack and defend themselves. Their attacks generally involve playing tricks and tormenting the antagonist. While a single fairy isn't too dangerous, a large group cannot be taken lightly.
Palm sized fairies are difficult to catch or hit. If a large amount of fairies are able to cover your body, there is little you can do to get them off. Once caught, there is nothing that awaits you but miserable days of being played with by the group. It's very dangerous to antagonize fairies, so be sure not to anger them.""")
        girl.label = "fairys"
        girl.label_cg = "fairys_cg"
        girl.info_rus_img = "fairys_pedia"
        girl.m_pose = fairys_pose
        girl.zukan_ko = "fairys"
        girl.facenum = 3
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = 20
        girl.x1 = 220
        girl.lv = 22
        girl.max_hp = 925
        girl.hp = 925
        girl.skills = ((3165, "Group Tickle"),
                         (3166, "Group Penis Tickle"),
                         (3167, "Group Pranks"),
                         (3168, "Group Dance"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 19,
                        "skill01": 6,
                        "item01": 8}
        return girl

########################################
# SYLPH
########################################
    def get_sylph():
        girl = Chr()
        girl.unlock = persistent.sylph_unlock == 1
        girl.name = "Sylph"
        girl.pedia_name = "Sylph"
        girl.info = _("""One of the four great spirits, Sylph rules over the wind. Though she's a type of fairy, her magic far surpasses a normal one. Since she is averse to battle. Sylph is unable to properly use her powerful magic in a fight. Even so, she isn't an opponent that a normal adventurer can handle.
Due to her fairy nature, she loves playing tricks on people. Interested by male genitalia, she will use her wind to play with men until they ejaculate. In addition, she absorbs male semen to replenish her magic.
Though she has a physical body, she is able to turn into a spirit body. Using her spirit body, she can possess a man and allow him to use her powers. However, the man needs to be very skilled to manage the enormous magic Sylph holds. For that reason, the spirits will only give their assistance to those they deem worthy.""")
        girl.label = "sylph"
        girl.label_cg = "sylph_cg"
        girl.info_rus_img = "sylph_pedia"
        girl.m_pose = sylph_pose
        girl.zukan_ko = "sylph"
        girl.facenum = 7
        girl.posenum = 3
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 50
        girl.max_hp = 4000
        girl.hp = 4000
        girl.skills = ((3169, "Tickling Wind"),
                        (3170, "Wind of Pleasure"),
                        (3171, "Wind of Ecstasy"),
                        (3172, "Wind Serenade"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 19,
                       "skill01": 6,
                       "item01": 8}
        return girl

########################################
# C_DRYAD
########################################
    def get_c_dryad():
        girl = Chr()
        girl.unlock = persistent.c_dryad_unlock == 1
        girl.name = "Chimera Dryad"
        girl.pedia_name = "Chimera Dryad"
        girl.info = _("""It appears to be a plant-type monster, but the details are uncertain. Apparently a parasitic plant took over the corpse of a human female. Its unique structure allows it to regenerate extremely quickly. A unique monster never seen before, the first sighting was in the Forest of Spirits.
Apparently devoid of intellect, it mindlessly catches prey to sate her thirst. Regardless of race or sex, she attacks anything that moves and sucks out all of their bodily fluids. Strange flowers bloom all over her body that are capable of sucking any fluid out of any body part. After draining her prey's body dry, the corpse is wrapped in her plants as she breaks it down and fuses it to her body.
A very strange being, her existence is even feared by other monsters.""")
        girl.label = "c_dryad"
        girl.label_cg = "c_dryad_cg"
        girl.info_rus_img = "c_dryad_pedia"
        girl.m_pose = c_dryad_pose
        girl.zukan_ko = "c_dryad"
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 6
        girl.x0 = -60
        girl.x1 = 140
        girl.lv = 65
        girl.max_hp = 9500
        girl.hp = 9500
        girl.echi = ["c_dryad_ha"]
        girl.skills = ((3174, "Ivy Stroking"),
                          (3175, "Semen Slurping Lily"),
                          (3194, "Maneating Sunflower"),
                          (3177, "Rose of Strangulation"),
                          (3178, "Intoxicating Incense"),
                          (3179, "Flower Energy Suck"),
                          (3180, "Floral Funeral"))
        girl.scope = {"ruka_tati": 0,
                         "mylv": 20,
                         "skill01": 6,
                         "item01": 8}
        return girl

########################################
# TARAN
########################################
    def get_taran():
        girl = Chr()
        girl.unlock = persistent.taran_unlock == 1
        girl.name = "Tarantula Girl"
        girl.pedia_name = "Tarantula Girl"
        girl.info = _("""An insect-based monster. Though she holds the ability to create webs, she chooses instead to attack her prey directly and use her silk to deprive them of freedom. She usually sucks out all of the body fluid of her prey, but if she particularly likes the man, she will mate with him instead.
The peculiar structure of her spinneret is able to act as both a normal spider's spinneret and as a female vagina that can force men to ejaculate with surprising ease. She is able to manipulate her spider silk inside her own body as she uses it to stimulate the man. Even after mating with a man for reproductive purposes, she will almost always eat him anyway. There have been cases of a Tarantula Girl keeping a man as a partner for life, though.""")
        girl.label = "taran"
        girl.label_cg = "taran_cg"
        girl.info_rus_img = "taran_pedia"
        girl.m_pose = taran_pose
        girl.zukan_ko = "taran"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 20
        girl.max_hp = 900
        girl.hp = 900
        girl.skills = ((3196, "Tarantula Caress"),
                        (3197, "Tarantula Blowjob"),
                        (3176, "Breast Pressure Hell"),
                        (3198, "Restrained Tarantula Caress"),
                        (3199, "Tarantula Torture"),
                        (3200, "Tarantula Rape"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 20,
                       "skill01": 7,
                       "item01": 8}
        return girl

########################################
# MINO
########################################
    def get_mino():
        girl = Chr()
        girl.unlock = persistent.mino_unlock == 1
        girl.name = "Minotauros"
        girl.pedia_name = "Minotauros"
        girl.info = _("""A monster that resembles a cow, she has a surprisingly human face. There is no human soldier that can match her in brute strength, though. Able to knock down any man, she will then take that opening to rape them. In most cases the goal is to feed instead of reproduction. With strong muscles in her vagina, she is able to freely tighten and control every movement inside of her. When a man is caught inside of her, it's said that she holds him like a vise as she uses her muscles to squeeze everything out of him. For your own safety, you should never make a Minotauros angry when she's raping you.
In almost every case, the man will be released after the Minotauros has finished raping him but when the Minotauros really likes a man, she may carry him back to her home and force him to be her husband.""")
        girl.label = "mino"
        girl.label_cg = "mino_cg"
        girl.info_rus_img = "mino_pedia"
        girl.m_pose = mino_pose
        girl.zukan_ko = "mino"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 20
        girl.max_hp = 1200
        girl.hp = 1200
        girl.skills = ((3201, "Strong Handjob"),
                       (3202, "Furious Intercrural"),
                       (3203, "Busty Tit Fuck"),
                       (3204, "Ferocious Blowjob"),
                       (3205, "Minotauros Rape"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 20,
                      "skill01": 7,
                      "item01": 8}
        return girl

########################################
# SASORI
########################################
    def get_sasori():
        girl = Chr()
        girl.unlock = persistent.sasori_unlock == 1
        girl.name = "Scorpion Girl"
        girl.pedia_name = "Scorpion Girl"
        girl.info = _("""An insect-based monster that lives in the desert. Designed to live in arid regions, her shell is very durable. Equipped with powerful scissors, it's very difficult to run away once held down. In addition, her tail contains very powerful venom that can kill her prey with ease, but since squeezing the semen out of her prey would not be possible if they were dead, she instead uses a small amount to simply paralyze her catch.
The tip of her tail is able to open to enable a man's penis to fit inside. She is able to manipulate the flesh inside of her tail, and quickly pump him with her strong tail muscles to force a man to ejaculate. She will continue to feed on the man until he dies, making her one of the most feared desert monsters.
Unique among other insect-based monsters, she reproduces by taking the semen squeezed out into her tail and injects it directly into her oviduct using the tip of her tail.""")
        girl.label = "sasori"
        girl.label_cg = "sasori_cg"
        girl.info_rus_img = "sasori_pedia"
        girl.m_pose = sasori_pose
        girl.zukan_ko = "sasori"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 21
        girl.max_hp = 1100
        girl.hp = 1100
        girl.skills = ((3206, "Scorpion Blowjob"),
                         (3207, "Scorpion Tit Fuck"),
                         (3208, "Venom Tail"),
                         (3209, "Petra Scorpio"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 21,
                        "skill01": 7,
                        "item01": 8}
        return girl

########################################
# LAMP
########################################
    def get_lamp():
        girl = Chr()
        girl.unlock = persistent.lamp_unlock == 1
        girl.name = "Lamp Genie"
        girl.pedia_name = "Lamp Genie"
        girl.info = _("""A half-human half-monster that possesses an antique lamp; she is only able to leave her dimension when a man rubs the lamp and summons her into his dimension. Using the guise of granting wishes, she skillfully seduces men. Most men are unable to resist and make a wish of lust. Extremely skilled at sexual techniques, she can fulfill that wish and leave the man satisfied. After the man is exhausted, she will show her true nature and swallow him whole.
If the man wishes instead for [Wealth and Fame], or any variation thereof, she will show him an illusion with her magic. Like that, she will again swallow him. It's believed that any strong-minded man who holds no selfish desires will actually have their wish granted by her.""")
        girl.label = "lamp"
        girl.label_cg = "lamp_cg"
        girl.info_rus_img = "lamp_pedia"
        girl.m_pose = lamp_pose
        girl.zukan_ko = "lamp"
        girl.facenum = 3
        girl.posenum = 4
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 24
        girl.max_hp = 1600
        girl.hp = 1600
        girl.skills = ((3210, "Hand of Pleasure"),
                       (3211, "Mouth of Joy"),
                       (3212, "Chest of Delight"),
                       (3213, "Intercrural Enjoyment"),
                       (3214, "Fingers of Ecstasy"),
                       (3215, "Heavenly Kamasutra"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 21,
                      "skill01": 7,
                      "item01": 9}
        return girl

########################################
# MUMMY
########################################
    def get_mummy():
        girl = Chr()
        girl.unlock = persistent.mummy_unlock == 1
        girl.name = "Mummy Girl"
        girl.pedia_name = "Mummy Girl"
        girl.info = _("""An undead monster that lurks in the Pyramid. Originally she just wandered through the Pyramid looking for energy, but she was later tasked by Sphinx to be the first challenge of those undergoing the [Dragon Seal Trial], though this hasn't changed the way she treats the men she catches.
Able to freely control her bandages, she uses them to bind her catch. She infuses the bandages with magic that is able to suck energy and uses that to suck out everything. If she wraps those bandages around a man's penis, she can suck energy at the same time as she wrings out his semen.
For hundreds of years, there has yet to be a challenger that has gotten past the Mummy Girl. Only a few people have ever managed to escape her once stepping inside the Pyramid as well.""")
        girl.label = "mummy"
        girl.label_cg = "mummy_cg"
        girl.info_rus_img = "mummy_pedia"
        girl.m_pose = mummy_pose
        girl.zukan_ko = "mummy"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 21
        girl.max_hp = 1300
        girl.hp = 1300
        girl.skills = ((3216, "Mummy Handjob"),
                        (3217, "Mummy Bandage"),
                        (3218, "Holding Bandage"),
                        (3219, "Holding Nightmare"),
                        (3220, "Mummy Package"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 21,
                       "skill01": 7,
                       "item01": 9}
        return girl

########################################
# KOBURA
########################################
    def get_kobura():
        girl = Chr()
        girl.unlock = persistent.kobura_unlock == 1
        girl.name = "Cobra Girl"
        girl.pedia_name = "Cobra Girl"
        girl.info = _("""A subspecies of Lamia, she lives primarily in arid regions. A subordinate of Sphinx, she was given the role of testing those who would attempt the [Dragon Seal Trial]. Very cruel, and just as intelligent, she takes pleasure in tormenting those she deems unqualified.
Like most Lamias, she coils around her catch as she rapes them. Once the man's genitalia are strangled inside her tight vagina, most men will surrender with surprising quickness. In addition, there is a very powerful poison inside of her body that paralyzes her catch. Once she rapes you, it is not possible to return alive.""")
        girl.label = "kobura"
        girl.label_cg = "kobura_cg"
        girl.info_rus_img = "kobura_pedia"
        girl.m_pose = kobura_pose
        girl.zukan_ko = "kobura"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 21
        girl.max_hp = 1400
        girl.hp = 1400
        girl.skills = ((3222, "Cobra Blowjob"),
                         (3223, "Cobra Tit Fuck"),
                         (3224, "Cobra Bite"),
                         (3225, "Nightmare Tightening"),
                         (3226, "Mellow Tail"),
                         (3227, "Cobra Anal"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 21,
                        "skill01": 7,
                        "item01": 9}
        return girl

########################################
# LAMIAS
########################################
    def get_lamias():
        girl = Chr()
        girl.unlock = persistent.lamias_unlock == 1
        girl.name = "Nefertiti Lamias"
        girl.pedia_name = "Nefertiti Lamias"
        girl.info = _("""Four sisters of the same mother that serve Sphinx. Just like most Lamias, they coil around their catch as they play with him.
Given the role to test challengers of the trial, they play with anyone deemed unqualified as a group. With all four of them coiling around the man, they will take turns raping him, forcing him to ejaculate repeatedly. They will also mate for reproduction if they like the man, and will again take turns raping him. If the man is too weak, he will die from the gang-rape. If he is strong enough to survive, he will spend the rest of his life shared by the four Lamias as they keep him as a group sex slave.""")
        girl.label = "lamias"
        girl.label_cg = "lamias_cg"
        girl.info_rus_img = "lamias_pedia"
        girl.m_pose = lamias_pose
        girl.zukan_ko = "lamias"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 5
        girl.x0 = -200
        girl.lv = 22
        girl.max_hp = 1700
        girl.hp = 1700
        girl.skills = ((3228, "Group Handjob"),
                         (3229, "Group Licking"),
                         (3230, "Group Tailjob"),
                         (3231, "Group Tightening"),
                         (3232, "Group Blowjob"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 22,
                        "skill01": 7,
                        "item01": 9}
        return girl

########################################
# SPHINX
########################################
    def get_sphinx():
        girl = Chr()
        girl.unlock = persistent.sphinx_unlock == 1
        girl.name = "Sphinx"
        girl.pedia_name = "Sphinx"
        girl.info = _("""A mixed beast monster that guards the Pyramid, believed to be older than 1,000 years old. Through her long life, she has come to wield powerful magic. Through various circumstances, she has become the judge over the [Dragon Seal Trial]; however, it seems she despairs at her role and lives her life immersed in hopelessness.
Those who fail the trial are violently raped, then eaten. She forces her prey to be entranced by pleasure as she swallows them whole and slowly digests them inside her gigantic stomach.
Her enormous magic, built up over hundreds of years, renders her all but immune to any type of sealing magic.""")
        girl.label = "sphinx"
        girl.label_cg = "sphinx_cg"
        girl.info_rus_img = "sphinx_pedia"
        girl.m_pose = sphinx_pose
        girl.zukan_ko = "sphinx"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 4
        girl.x0 = -200
        girl.lv = 35
        girl.max_hp = 2600
        girl.hp = 2600
        girl.skills = ((3233, "Finger Offering"),
                         (3234, "Mouth Offering"),
                         (3235, "Isis's Lovemaking"),
                         (3236, "Kiss of Death"),
                         (3237, "Ancient Law of Ecstasy"))
        girl.scope = {"ruka_tati": 0,
                        "mylv": 22,
                        "skill01": 7,
                        "item01": 9}
        return girl

########################################
# SARA
########################################
    def get_sara():
        girl = Chr()
        girl.unlock = persistent.hsean_sara1 == 1
        girl.name = "Sara"
        girl.pedia_name = "Sara"
        girl.info = _("""The Princess of Sabasa, she's the current heir to the throne. She's a descendant of Sphinx but the monster blood has slowly thinned out over the years. Though she's slightly stronger and more intelligent than a normal human, she behaves like a normal human.
Even though she's an entry in the [Monster Encyclopedia], there's no doubt she's almost fully human.
Though she's intelligent, she acts before thinking things through. After she fell in love with Granberia, she devoted herself to swordplay under the guise of training for self-defense. Though she's a decent swordswoman for a human, she is still no match for the monsters that live in the desert.
Sara has also undergone extensive training to behave and act like a lady of the Royal Family, which includes manners, history, and sexual skills. All of her knowledge has been learned from books and seems to have never been practiced on an actual man before.""")
        girl.label = "sara"
        girl.label_cg = "sara_cg"
        girl.info_rus_img = "sara_pedia"
        girl.m_pose = sara_pose
        girl.zukan_ko = "sara"
        girl.zukan_ko = None
        girl.facenum = 4
        girl.posenum = 3
        girl.x0 = -200
        girl.lv = 10
        girl.max_hp = 85
        girl.hp = 85
        girl.echi = ["lb_0146a"]
        girl.scope = {"ruka_tati": 0,
                      "mylv": 22,
                      "skill01": 7,
                      "item01": 9}
        return girl

########################################
# SUCKVORE
########################################
    def get_suckvore():
        girl = Chr()
        girl.unlock = persistent.suckvore_unlock == 1
        girl.name = "Suck Vore"
        girl.pedia_name = "Suck Vore"
        girl.info = _("""A low grade organism created from a female villager by Lily's experiments. Her intelligence and reason has been completely erased and the Suck Vore's only function is to catch men and suck out their semen. It isn't even capable of reproduction. When it catches a man, it draws it into it's body and uses it's tentacles to force him to ejaculate. Due to the large amount of slimy tentacles, it's able to force a man to orgasm surprisingly quick. As soon as he ejaculates, the Suck Vore can absorb the semen through it's flesh.
Once it catches a man, it will continue to suck out his semen until he dies. Since it's unable to think or communicate, there is no escape once caught, making this a very dangerous monster. This was one of the first creations of Lily.""")
        girl.label = "suckvore"
        girl.label_cg = "suckvore_cg"
        girl.info_rus_img = "suckvore_pedia"
        girl.m_pose = suckvore_pose
        girl.zukan_ko = "suckvore"
        girl.facenum = 2
        girl.posenum = 2
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 22
        girl.max_hp = 1200
        girl.hp = 1200
        girl.skills = ((3238, "Full Body Tentacle Rub"),
                           (3239, "Groin Tentacle Rub"),
                           (3240, "Full Body Energy Suck"),
                           (3241, "Genital Energy Suck"))
        girl.scope = {"ruka_tati": 0,
                          "mylv": 22,
                          "skill01": 7,
                          "item01": 9}
        return girl

########################################
# WORMV
########################################
    def get_wormv():
        girl = Chr()
        girl.unlock = persistent.wormv_unlock == 1
        girl.name = "Worm Villager"
        girl.pedia_name = "Worm Villager"
        girl.info = _("""With a tentacle for an arm, she was driven mad by Lily's magic. Though her personality has been twisted, she retains the intelligence and memories she had from before she was transformed, but due to Lily's corruption, she can't stop herself from desiring male semen. Crushed by the desire, she uses her tentacle to force men to sate her thirst. Since her tentacle was designed specifically to force men to ejaculate, the pleasure is extremely potent.
No matter how much semen she consumes, she will never feel sated. Forced to desire ever more, she can't do anything but endlessly squeeze men. Intelligent enough to not kill her source of semen, she will carefully keep the man on the brink of death, ensuring he remains alive so she can continue to milk him. If a man is caught by her, they will spend the rest of their lives weakened and unable to do anything but feed her.""")
        girl.label = "wormv"
        girl.label_cg = "wormv_cg"
        girl.info_rus_img = "wormv_pedia"
        girl.m_pose = wormv_pose
        girl.zukan_ko = "wormv"
        girl.facenum = 3
        girl.posenum = 2
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 22
        girl.max_hp = 1400
        girl.hp = 1400
        girl.skills = ((3242, "Worm Touch"),
                        (3243, "Worm Caress"),
                        (3244, "Worm Drain"),
                        (3245, "Worm Spread"),
                        (3246, "Anal Stroke"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 22,
                       "skill01": 7,
                       "item01": 9}
        return girl

########################################
# IRONMAIDEN
########################################
    def get_ironmaiden():
        girl = Chr()
        girl.unlock = persistent.ironmaiden_unlock == 1
        girl.name = "Iron Maiden"
        girl.pedia_name = "Iron Maiden"
        girl.info = _("""The result of another of Lily's experiments in transforming a villager into a torture device, it locks a man inside it's body and uses every aspect of her interior to stimulate the man. Filled with tentacles inside of it's body, it attacks every inch of the man's body as he's left unable to move.
Once locked inside, the amount of force needed to open her back up is greater than the power of twenty men. If caught inside, there is no escape. The Iron Maiden will force the man to orgasm repeatedly until their death. Lily uses the device to both collect a large amount of semen quickly and to dispose of men that have lost their use.
The Iron Maiden takes considerable magic to create, so only a few exist inside Lily's mansion. It's also believed that one escaped from her dungeon into the wild.""")
        girl.label = "ironmaiden"
        girl.label_cg = "ironmaiden_cg"
        girl.info_rus_img = "ironmaiden_pedia"
        girl.m_pose = ironmaiden_pose
        girl.zukan_ko = "ironmaiden"
        girl.facenum = 1
        girl.posenum = 3
        girl.x0 = -200
        girl.lv = 23
        girl.max_hp = 2000
        girl.hp = 2000
        girl.skills = ((3247, "Nightmare Embrace"),)
        girl.scope = {"ruka_tati": 0,
                            "mylv": 23,
                            "skill01": 7,
                            "item01": 9}
        return girl

########################################
# LILY
########################################
    def get_lily():
        girl = Chr()
        girl.unlock = persistent.lily_unlock == 1
        girl.name = "Lily"
        girl.pedia_name = "Lily"
        girl.info = _("""A powerful magic user that took over lordship of the [Witch Hunt Village]. The illegitimate daughter of the previous lord, she studied magic from forbidden books. Using that power, she killed her father and brothers.
After becoming Lord, Lily increased the witch hunts in order to capture the villagers for experiments. She used the females to create new twisted monsters or transformed humans, and would wring the semen from the males for use in testing. If the man held weak latent magic power inside, she would quickly dispose of him. Weak women, unable to completely change their body, would end up with tentacles for various body parts.
After experimenting on villagers for years, she remodeled her own body by implanting two tentacles in her arms. Using those tentacles, she would harvest hundreds of men's semen for use in her testing. A significant number of villagers and travelers have been killed by her hand.""")
        girl.label = "lily"
        girl.label_cg = "lily_cg"
        girl.info_rus_img = "lily_pedia"
        girl.m_pose = lily_pose
        girl.zukan_ko = "lily"
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 2
        girl.x0 = -200
        girl.lv = 30
        girl.max_hp = 2000
        girl.hp = 2000
        girl.skills = ((3248, "Worm Touch"),
                       (3249, "Worm Drain"),
                       (3250, "Worm Ecstasy"),
                       (3251, "Anal Drain"),
                       (3252, "Worm Spread"),
                       (3253, "Drain Heaven"))
        girl.scope = {"ruka_tati": 0,
                      "mylv": 23,
                      "skill01": 7,
                      "item01": 9}
        return girl

########################################
# ARIZIGOKU
########################################
    def get_arizigoku():
        girl = Chr()
        girl.unlock = persistent.arizigoku_unlock == 1
        girl.name = "Antlion Girl"
        girl.pedia_name = "Antlion Girl"
        girl.info = _("""An insect-based monster that lives mainly in the desert, she creates a cone-shaped nest in the ground to catch unsuspecting travelers in order to squeeze out their bodily fluids. Preferring male semen, she will use her massive scissors to lock the man's waist in place as she sucks everything out. Once her catch has been completely sucked dry, she will dispose of their dried corpse in the desert. Although her trap is extremely dangerous, her body is very fragile. The only way to escape is to quickly defeat her before you're brought too far in.
Though it's called an Antiion Girl, it looks like a doodlebug. However, for the monster Antiion Girl, this is her adult form.""")
        girl.label = "arizigoku"
        girl.label_cg = "arizigoku_cg"
        girl.info_rus_img = "arizigoku_pedia"
        girl.m_pose = arizigoku_pose
        girl.zukan_ko = "arizigoku"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = 24
        girl.max_hp = 500
        girl.hp = 500
        girl.skills = ((3254, "Fluid Squeezing"),)
        girl.scope = {"ruka_tati": 0,
                           "mylv": 24,
                           "skill01": 7,
                           "item01": 9}
        return girl

########################################
# SANDW
########################################
    def get_sandw():
        girl = Chr()
        girl.unlock = persistent.sandw_unlock == 1
        girl.name = "Sandworm"
        girl.pedia_name = "Sandworm"
        girl.info = _("""A huge monster that lives in arid regions. It usually hides under the sand, but when it senses the vibrations of a moving human, it will attack them. Forcing the man into her mouth, she will suck on his body like a piece of candy. Her giant tongue can quickly force a man to orgasm. Once the man has reached orgasm, she will begin to use her tongue to suck and lick up all of the semen, forcing the man to continually ejaculate. Once she sucks out everything from the man, she will burrow back under the sand to await her next meal. Though she is very slow and absentminded, you cannot be careless around her.
Most desert monsters greedily suck up men's fluids to slake their thirsts. Great care needs to be taken when traveling through the desert, since being caught by a monster usually entails certain death.""")
        girl.label = "sandw"
        girl.label_cg = "sandw_cg"
        girl.info_rus_img = "sandw_pedia"
        girl.m_pose = sandw_pose
        girl.zukan_ko = "sandw"
        girl.facenum = 3
        girl.posenum = 1
        girl.bk_num = 1
        girl.x0 = -120
        girl.lv = 24
        girl.max_hp = 2500
        girl.hp = 2500
        girl.skills = ((3255, "Lick"),
                        (3256, "Suck"),
                        (3257, "Kiss"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 24,
                       "skill01": 7,
                       "item01": 9}
        return girl

########################################
# GNOME
########################################
    def get_gnome():
        girl = Chr()
        girl.unlock = persistent.gnome_unlock == 1
        girl.name = "Gnome"
        girl.pedia_name = "Gnome"
        girl.info = _("""Holding huge magic, the Earth Spirit Gnome can freely manipulate the Earth itself. However, she has very little combat experience, so she doesn't know how to effectively use her magic in a fight. Gnome usually spends her time in the desert playing with the sand and mud. When she senses a human approaching, she will usually hide and silently observe them.
Due to her curious nature, if Gnome catches a man, she will take her time to play and inspect him. To replenish her magic, she will use her mud dolls to collect semen.
If you defeat Gnome, she will lend you her power. When she possesses you with her spirit body, you can use her Earth magic.
Though it looks like she bullies Sylph, it may just be because she doesn't know how to express her feelings.""")
        girl.label = "gnome"
        girl.label_cg = "gnome_cg"
        girl.info_rus_img = "gnome_pedia"
        girl.m_pose = gnome_pose
        girl.zukan_ko = "gnome"
        girl.facenum = 4
        girl.posenum = 4
        girl.bk_num = 3
        girl.x0 = -200
        girl.lv = 50
        girl.max_hp = 4000
        girl.hp = 4000
        girl.skills = ((3258, "Mud Doll Caress"),
                        (3259, "Mud Doll Groin Fondle"),
                        (3260, "Gnome's Hand Games"),
                        (3261, "Gnome's Mouth Play"),
                        (3262, "Gnome's Mud Vagina"))
        girl.scope = {"ruka_tati": 0,
                       "mylv": 24,
                       "skill01": 7,
                       "item01": 9}
        return girl

########################################
# IlIAS1
########################################
    def get_ilias1():
        girl = Chr()
        girl.unlock = persistent.count_hanseikai > 49
        girl.name = "Goddess Ilias"
        girl.pedia_name = "Goddess Ilias"
        girl.info = _("""The Goddess who created the world. Though she holds enormous power, she's rarely seen on the Earth itself. However, her will is handed down to the humans living on the Earth and she is revered as their goddess.
It's said that she hates her failed creations, the monsters, but loves humanity. She would object very strongly to being included in this [Monster Encyclopedia].""")
        girl.label = "ilias1"
        girl.label_cg = "ilias1_cg"
        girl.info_rus_img = "ilias1_pedia"
        girl.m_pose = ilias1_pose
        girl.zukan_ko = None
        girl.facenum = 6
        girl.posenum = 2
        girl.bk_num = 1
        girl.x0 = -200
        girl.lv = "????"
        girl.max_hp = "????"
        girl.hp = "????"
        girl.echi = ["hansei_h"]
        girl.scope = {"ruka_tati": 0,
                        "mylv": 24,
                        "skill01": 7,
                        "item01": 9}

        return girl