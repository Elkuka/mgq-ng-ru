label slime_cg:
    scene
    show bg 004
    show slime h1
    with dissolve
    pause
    show slime h2 with dissolve
    pause
    show slime h3 with dissolve
    pause
    show slime h4 with dissolve
    pause
    return


label alice1_cg:
    scene
    if persistent.defeat["alice1"]:
        show bg 006
        show alice h01_1
        with dissolve
        pause
        show alice h01_2 with dissolve
        pause
        show alice h01_3 with dissolve
        pause
        show alice h01_4 with dissolve
        pause

    if persistent.hsean_alice2:
        show bg 015
        show alice h02_1
        with dissolve
        pause
        show alice h02_2 with dissolve
        pause
        show alice h02_3 with dissolve
        pause
        show alice h02_4 with dissolve
        pause
        show alice h02_5 with dissolve
        pause

    if persistent.hsean_alice3:
        show bg 035
        show alice h03_1
        with dissolve
        pause

    if persistent.hsean_alice4:
        show bg 038
        show alice h04_1
        with dissolve
        pause
        show alice h04_2 with dissolve
        pause
        show alice h04_3 with dissolve
        pause
        show alice h04_4 with dissolve
        pause

    if persistent.hsean_alice5:
        show bg 067
        show alice h05_1
        with dissolve
        pause
        show alice h05_2 with dissolve
        pause
        show alice h05_3 with dissolve
        pause
        show alice h05_4 with dissolve
        pause

    show alice iv01 with dissolve
    pause
    show alice iv02 with dissolve
    pause
    return


label slug_cg:
    scene
    show slug h01 with dissolve
    pause
    show slug h02 with dissolve
    pause
    show slug h03 with dissolve
    pause
    show slug h04 with dissolve
    pause
    return


label mdg_cg:
    scene
    show bg 011

    if renpy.seen_label("mdg_ha"):
        show mdg ct11 at topright zorder 15 as ct
        show mdg st01
        with dissolve
        pause
        show mdg bk02 as bk
        show mdg ct12 at topright zorder 15 as ct
        show mdg st01
        with dissolve
        pause
        show mdg bk02 as bk
        show mdg ct13 at topright zorder 15 as ct
        show mdg st02
        with dissolve
        pause

    if not renpy.seen_label("mdg_hb"):
        return

    hide bk
    show mdg ct21 at topleft zorder 15 as ct
    show mdg st01
    with dissolve
    pause
    show mdg bk03 as bk
    show mdg ct22 at topleft zorder 15 as ct
    show mdg st01
    with dissolve
    pause
    show mdg bk03 as bk
    show mdg ct23 at topleft zorder 15 as ct
    show mdg st02
    with dissolve
    pause
    hide bk
    show mdg ct31 at topright zorder 15 as ct
    show mdg st02
    with dissolve
    pause
    show mdg ct32 at topright zorder 15 as ct with dissolve
    pause
    show mdg ct33 at topright zorder 15 as ct with dissolve
    pause
    show mdg ct34 at topright zorder 15 as ct with dissolve
    pause
    return


label granberia1_cg:
    scene
    show bg 013
    show granberia ha1
    with dissolve
    pause
    show granberia ha2 with dissolve
    pause
    show granberia ha3 with dissolve
    pause
    show granberia ha4 with dissolve
    pause
    return


label mimizu_cg:
    scene
    show bg 019
    show mimizu h1
    with dissolve
    pause
    show mimizu h2 with dissolve
    pause
    show mimizu h3 with dissolve
    pause
    show mimizu h4 with dissolve
    pause
    show mimizu h5 with dissolve
    pause
    show mimizu h6 with dissolve
    pause
    return


label gob_cg:
    scene
    show bg 020
    show gob h1
    with dissolve
    pause
    show gob h2 with dissolve
    pause
    show gob h3 with dissolve
    pause
    return



label pramia_cg:
    scene
    show bg 022
    show pramia ct01 at topright zorder 15 as ct
    show pramia h1
    with dissolve
    pause
    show pramia ct02 at topright zorder 15 as ct
    show pramia h2
    with dissolve
    pause
    show pramia ct03 at topright zorder 15 as ct
    show pramia h3
    with dissolve
    pause
    show pramia ct04 at topright zorder 15 as ct
    show pramia h4
    with dissolve
    pause
    show pramia ct05 at topright zorder 15 as ct
    show pramia h5
    with dissolve
    pause
    return


label vgirl_cg:
    scene
    show bg 022

    if renpy.seen_label("vgirl_ha"):
        show vgirl st02
        show vgirl ct01 zorder 15 as ct
        with dissolve
        pause
        show vgirl ct02 zorder 15 as ct with dissolve
        pause
        show vgirl ct03 zorder 15 as ct with dissolve
        pause
        show vgirl ct04 zorder 15 as ct with dissolve
        pause
        show vgirl ct05 zorder 15 as ct with dissolve
        pause
        show vgirl ct06 zorder 15 as ct with dissolve
        pause

        if not renpy.seen_label("vgirl_hb"):
            return

    hide ct
    show vgirl h1 with dissolve
    pause
    show vgirl h2 with dissolve
    pause
    show vgirl h3 with dissolve
    pause
    show vgirl h4 with dissolve
    pause
    return


label dragonp_cg:
    scene
    show bg 022
    show dragonp h1
    with dissolve
    pause
    show dragonp h2 with dissolve
    pause
    show dragonp h3 with dissolve
    pause
    show dragonp h4 with dissolve
    pause
    return


label mitubati_cg:
    scene
    show bg 011

    if renpy.seen_label("mitubati_ha"):
        show mitubati h1
        with dissolve
        pause
        show mitubati ct01 at topright zorder 15 as ct with dissolve
        pause
        show mitubati ct02 at topright zorder 15 as ct
        show mitubati h2
        with dissolve
        pause
        show mitubati ct03 at topright zorder 15 as ct
        show mitubati h3
        with dissolve
        pause
        show mitubati ct04 at topright zorder 15 as ct
        show mitubati h4
        with dissolve
        pause

        if not renpy.seen_label("mitubati_hb"):
            return

    show mitubati h1
    show mitubati ctb01 at topright zorder 15 as ct
    with dissolve
    pause
    show mitubati h2
    show mitubati ctb02 at topright zorder 15 as ct
    with dissolve
    pause
    show mitubati h3
    show mitubati ctb03 at topright zorder 15 as ct
    with dissolve
    pause
    show mitubati h4
    show mitubati ctb04 at topright zorder 15 as ct
    with dissolve
    pause
    return


label hapy_a_cg:
    scene
    show bg 024
    show hapy_a h1 with dissolve
    pause
    show hapy_a h2 with dissolve
    pause
    show hapy_a h3 with dissolve
    pause
    show hapy_a h4 with dissolve
    pause
    show hapy_a h5 with dissolve
    pause
    show hapy_a h6 with dissolve
    pause
    show hapy_a h7 with dissolve
    pause
    show hapy_a h8 with dissolve
    pause
    return


label hapy_bc_cg:
    scene
    show bg 027
    show hapy_bc ha1
    with dissolve
    pause
    show hapy_bc ha2 with dissolve
    pause
    show hapy_bc ha3 with dissolve
    pause
    show hapy_bc ha4 with dissolve
    pause
    show hapy_bc hc1
    show hapy_bc ct01 at topright as ct
    with dissolve
    pause
    show hapy_bc hc2
    show hapy_bc ct02 at topright as ct
    with dissolve
    pause
    show hapy_bc ct03 at topright as ct
    with dissolve
    pause
    hide ct
    show hapy_bc hb1
    with dissolve
    pause
    show hapy_bc hb2 with dissolve
    pause
    show hapy_bc hb3 with dissolve
    pause
    return


label queenharpy_cg:
    scene
    show bg 027
    show queenharpy h1
    with dissolve
    pause
    show queenharpy ct01 at topleft zorder 15 as ct with dissolve
    pause
    show queenharpy h2
    show queenharpy ct02 at topleft as ct
    with dissolve
    pause
    show queenharpy h3
    show queenharpy ct03 at topleft as ct
    with dissolve
    pause
    show queenharpy h4
    show queenharpy ct04 at topleft as ct
    with dissolve
    pause
    return


label delf_a_cg:
    scene
    show bg 004
    show delf_a h1 with dissolve
    pause
    show delf_a h2 with dissolve
    pause
    show delf_a h3 with dissolve
    pause
    return


label delf_b_cg:
    scene
    show bg 004
    show delf_b h1a at xy(X=150, Y=30)
    with dissolve
    pause
    show delf_b h2a at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h3a at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h4a at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h1 at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h2 at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h3 at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h4 at xy(X=150, Y=30) with dissolve
    pause
    show delf_b h5 at xy(X=150, Y=30) with dissolve
    pause
    return


label hiru_cg:
    scene
    show bg 019
    show hiru st11
    with dissolve
    pause
    show hiru ct01 at topleft zorder 15 as ct
    show hiru st11
    with dissolve
    pause
    show hiru ct02 at topleft zorder 15 as ct
    show hiru st12
    with dissolve
    pause
    show hiru ct03 at topleft zorder 15 as ct
    show hiru st13
    with dissolve
    pause
    show hiru ct04 at topleft zorder 15 as ct
    show hiru st14
    with dissolve
    pause
    return


label rahure_cg:
    scene
    show bg 030
    show rahure st11
    show rahure ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st12
    show rahure ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st13
    show rahure ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st14
    show rahure ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st11
    show rahure ct11 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st12
    show rahure ct12 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st13
    show rahure ct13 at topright zorder 15 as ct
    with dissolve
    pause
    show rahure st14
    show rahure ct14 at topright zorder 15 as ct
    with dissolve
    pause
    return


label ropa_cg:
    scene
    show bg 030
    show ropa h1
    with dissolve
    pause
    show ropa ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show ropa h2
    show ropa ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show ropa h3
    show ropa ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show ropa h4
    show ropa ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show ropa h5
    with dissolve
    pause
    return


label youko_cg:
    scene
    show bg 032
    show youko st01
    show youko ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show youko ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show youko ct03 at topright zorder 15 as ct
    show youko bk04 as bk
    with dissolve
    pause
    show youko ct04 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    hide bk
    show youko h1
    with dissolve
    pause
    show youko h2 with dissolve
    pause
    show youko h3 with dissolve
    pause
    show youko h4 with dissolve
    pause
    return


label meda_cg:
    scene
    show bg 032
    show meda h1
    with dissolve
    pause
    show meda ct01 at topright zorder 15 as ct
    show meda h1
    with dissolve
    pause
    show meda ct02 at topright zorder 15 as ct
    show meda h2
    with dissolve
    pause
    show meda ct03 at topright zorder 15 as ct
    show meda h3
    with dissolve
    pause
    show meda ct04 at topright zorder 15 as ct
    show meda h4
    with dissolve
    pause
    return


label kumo_cg:
    scene
    show bg 032
    show kumo st02
    show kumo ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumo ct02 at topleft zorder 15 as ct with dissolve
    pause
    show kumo ct03 at topleft zorder 15 as ct with dissolve
    pause
    show kumo ct04 at topleft zorder 15 as ct with dissolve
    pause
    hide ct

    if renpy.seen_label("kumo_ha"):
        show kumo ha1
        with dissolve
        pause
        show kumo ha2 with dissolve
        pause
        show kumo ha3 with dissolve
        pause
        show kumo ha4 with dissolve
        pause
        show kumo ha5 with dissolve
        pause
        show kumo ha6 with dissolve
        pause

        if not renpy.seen_label("kumo_hb"):
            return

    show kumo hb1
    with dissolve
    pause
    show kumo hb2 with dissolve
    pause
    show kumo hb3 with dissolve
    pause
    show kumo hb4 with dissolve
    pause
    return


label mimic_cg:
    scene
    show bg 033
    show mimic st21
    with dissolve
    pause

    if renpy.seen_label("mimic_ha"):
        show mimic ct01 at topright zorder 15 as ct with dissolve
        pause
        show mimic ct02 at topright zorder 15 as ct
        show mimic st22
        with dissolve
        pause
        show mimic ct03 at topright zorder 15 as ct with dissolve
        pause

        if not renpy.seen_label("mimic_hb"):
            return

    show mimic ct11 at topleft zorder 15 as ct with dissolve
    pause
    show mimic ct12 at topleft zorder 15 as ct
    show mimic st22
    with dissolve
    pause
    show mimic ct13 at topleft zorder 15 as ct with dissolve
    pause
    show mimic ct14 at topleft zorder 15 as ct with dissolve
    pause
    return


label nanabi_cg:
    scene
    show bg 034
    show nanabi st01
    show nanabi cta01 at topright zorder 15 as ct
    with dissolve
    pause
    show nanabi cta02 at topright zorder 15 as ct with dissolve
    pause
    show nanabi bk05 as bk
    show nanabi cta03 at topright zorder 15 as ct
    with dissolve
    pause
    show nanabi cta04 at topright zorder 15 as ct with dissolve
    pause
    hide bk
    show nanabi ctb01 at topright zorder 15 as ct
    with dissolve
    pause
    show nanabi ctb02 at topright zorder 15 as ct with dissolve
    pause
    show nanabi bk05 as bk
    show nanabi bk06 as bk2
    show nanabi bk07 as bk3
    show nanabi ctb03 at topright zorder 15 as ct
    with dissolve
    pause
    show nanabi ctb04 at topright zorder 15 as ct with dissolve
    pause
    hide bk
    hide bk2
    hide bk3
    hide ct

    if renpy.seen_label("nanabi_ha"):
        show nanabi h1
        with dissolve
        pause
        show nanabi h2 with dissolve
        pause
        show nanabi h3 with dissolve
        pause
        show nanabi h4 with dissolve
        pause

        if not renpy.seen_label("nanabi_hb"):
            return

    show nanabi h5
    with dissolve
    pause
    show nanabi h6
    show nanabi ctc01 at topleft zorder 15 as ct
    with dissolve
    pause
    show nanabi h7
    show nanabi ctc02 at topleft zorder 15 as ct
    with dissolve
    pause
    show nanabi h8
    show nanabi ctc03 at topleft zorder 15 as ct
    with dissolve
    pause
    show nanabi h9
    show nanabi ctc04 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label tamamo1_cg:
    scene
    show bg 035
    show tamamo st01
    show tamamo cta01 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo cta02 at topright zorder 15 as ct with dissolve
    pause
    show tamamo bk04 as bk
    show tamamo cta03 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo cta04 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    hide bk
    show tamamo h1 with dissolve
    pause
    show tamamo h2 with dissolve
    pause
    show tamamo h3 with dissolve
    pause
    show tamamo h4 with dissolve
    pause
    show tamamo h5 with dissolve
    pause
    show tamamo h6 with dissolve
    pause
    return


label alma_elma1_cg:
    scene
    show bg 036
    show alma_elma st32
    show alma_elma ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct02 at topright zorder 15 as ct with dissolve
    pause
    show alma_elma ct03 at topright zorder 15 as ct with dissolve
    pause
    show alma_elma ct04 at topright zorder 15 as ct with dissolve
    pause
    show alma_elma st32b
    show alma_elma ct05 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct06 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    show alma_elma ha1
    with dissolve
    pause
    show alma_elma hb1 zorder 15 as ct
    with dissolve
    pause
    show alma_elma ha2
    show alma_elma hb2 zorder 15 as ct
    with dissolve
    pause
    show alma_elma ha3
    show alma_elma hb3 zorder 15 as ct
    with dissolve
    pause
    show alma_elma ha4
    show alma_elma hb4 zorder 15 as ct
    with dissolve
    pause
    show alma_elma ha4
    show alma_elma hb5 zorder 15 as ct
    with dissolve
    pause
    return


label namako_cg:
    scene
    show bg 039
    show namako h1
    with dissolve
    pause
    show namako ct01 at topright as ct with dissolve
    pause
    show namako h2
    show namako ct02 at topright as ct
    with dissolve
    pause
    show namako ct03 at topright as ct with dissolve
    pause
    show namako ct04 at topright as ct
    show namako h3
    with dissolve
    pause
    return


label kai_cg:
    scene
    show bg 039
    show kai h1
    with dissolve
    pause
    show kai ct11 at topright zorder 15 as ct with dissolve
    pause
    show kai h2
    show kai ct12 at topright zorder 15 as ct
    with dissolve
    pause
    show kai h3
    show kai ct01 at topright zorder 15 as ct2
    show kai ct12 at xy(X=-129) zorder 15 as ct
    with dissolve
    pause
    show kai ct02 at topright zorder 15 as ct2
    show kai ct13 at xy(X=-129) zorder 15 as ct
    with dissolve
    pause
    show kai ct03 at topright zorder 15 as ct2
    show kai ct14 at xy(X=-129) zorder 15 as ct
    with dissolve
    pause
    return


label lamia_cg:
    scene
    show bg 041

    if renpy.seen_label("lamia_ha"):
        show lamia ha1
        with dissolve
        pause
        show lamia ha2 with dissolve
        pause
        show lamia ha3 with dissolve
        pause
        show lamia ha4 with dissolve
        pause
        show lamia ha5 with dissolve
        pause
        show lamia ha6 with dissolve
        pause
        show lamia ha7 with dissolve
        pause
        show lamia ha8 with dissolve
        pause

    if renpy.seen_label("lamia_hb"):
        show lamia ct01 at topright zorder 15 as ct
        show lamia st02:
            xpos -160
        with dissolve
        pause
        show lamia ct02 at topright zorder 15 as ct with dissolve
        pause
        show lamia ct03 at topright zorder 15 as ct with dissolve
        pause
        show lamia ct04 at topright zorder 15 as ct with dissolve
        pause
        show lamia ct05 at topright zorder 15 as ct with dissolve
        pause

        if not renpy.seen_label("lamiab_h"):
            return

    hide ct
    show lamia hb1
    with dissolve
    pause
    show lamia ctb01 at topleft zorder 15 as ct
    with dissolve
    pause
    show lamia hb2
    show lamia ctb02 at topleft zorder 15 as ct
    with dissolve
    pause
    show lamia hb3
    show lamia ctb03 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label granberia2_cg:
    scene
    show bg 044
    show granberia st03 at xy(X=11)
    show granberia ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show granberia ct02 at topright zorder 15 as ct with dissolve
    pause
    show granberia ct03 at topright zorder 15 as ct with dissolve
    pause
    show granberia ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label page17_cg:
    scene
    show bg 046
    show page17 ct01 at topright zorder 15 as ct
    show page17 st01
    with dissolve
    pause
    show page17 bk03 as bk
    show page17 ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show page17 ct03 at topright zorder 15 as ct with dissolve
    pause
    return


label page257_cg:
    scene
    show bg 046
    show page257 h1
    with dissolve
    pause
    show page257 ct01 at topright zorder 15 as ct with dissolve
    pause
    show page257 ct02 at topright zorder 15 as ct with dissolve
    pause
    show page257 ct03 at topright zorder 15 as ct with dissolve
    pause
    show page257 ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label page65537_cg:
    scene
    show bg 046
    show page65537 h1a
    with dissolve
    pause
    show page65537 ct01 at topright zorder 15 as ct with dissolve
    pause
    show page65537 h2a
    show page65537 ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show page65537 h3a
    show page65537 ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show page65537 h1b
    with dissolve
    pause
    show page65537 ct01 at topright zorder 15 as ct with dissolve
    pause
    show page65537 h2b
    show page65537 ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show page65537 h3b
    show page65537 ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show page65537 h1c
    with dissolve
    pause
    show page65537 ct01 at topright zorder 15 as ct with dissolve
    pause
    show page65537 h2c
    show page65537 ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show page65537 h3c
    show page65537 ct03 at topright zorder 15 as ct
    with dissolve
    pause
    return


label kani_cg:
    scene
    show bg 039
    show kani st11
    with dissolve
    pause
    show kani ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show kani ct02 at topright zorder 15 as ct
    show kani st12
    with dissolve
    pause
    show kani ct03 at topright zorder 15 as ct
    show kani st13
    with dissolve
    pause
    show kani ct11 at topright zorder 15 as ct
    show kani st11
    with dissolve
    pause
    show kani ct12 at topright zorder 15 as ct
    show kani st12
    with dissolve
    pause
    show kani ct13 at topright zorder 15 as ct
    show kani st13
    with dissolve
    pause
    return


label kurage_cg:
    scene
    show bg 050
    show kurage st02
    show kurage ct01 at topright as ct
    with dissolve
    pause
    show kurage ct02 at topright as ct with dissolve
    pause
    show kurage ct03 at topright as ct with dissolve
    pause
    show kurage ctb01 at right zorder 15 as ct
    with dissolve
    pause
    show kurage ctb02 at right zorder 15 as ct
    with dissolve
    pause
    show kurage ctb03 at right zorder 15 as ct
    with dissolve
    pause
    show kurage ctb04 at right zorder 15 as ct
    with dissolve
    pause
    return


label iso_cg:
    scene
    show bg 051
    show iso h1
    with dissolve
    pause
    show iso ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show iso h2
    show iso ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show iso h3
    hide ct
    with dissolve
    pause
    show iso ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show iso h4
    show iso ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show iso h5
    show iso ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show iso ct04 at topright zorder 15 as ct with dissolve
    pause
    show iso h6
    hide ct
    with dissolve
    pause
    show iso ctb01 at right zorder 15 as ct with dissolve
    pause
    show iso ctb02 at right zorder 15 as ct with dissolve
    pause
    show iso ctb03 at right zorder 15 as ct with dissolve
    pause
    show iso ctb04 at right zorder 15 as ct with dissolve
    pause
    return


label ankou_cg:
    scene
    show bg 052
    show ankou st01
    show ankou cta01 at left zorder 15 as ct
    with dissolve
    pause
    show ankou cta02 at left zorder 15 as ct with dissolve
    pause
    show ankou cta03 at left zorder 15 as ct with dissolve
    pause
    show ankou ctb01 as ct with dissolve
    pause
    show ankou ctb02 as ct with dissolve
    pause
    show ankou ctb03 as ct with dissolve
    pause
    show ankou ctb04 as ct with dissolve
    pause
    return


label kraken_cg:
    scene
    show bg 053
    show kraken st11
    with dissolve
    pause
    show kraken h1 with dissolve
    pause
    show kraken h2 with dissolve
    pause
    show kraken h2a with dissolve
    pause
    show kraken h3 with dissolve
    pause
    show kraken h3a with dissolve
    pause
    show kraken h4 with dissolve
    pause
    show kraken h5 with dissolve
    pause
    return


label meia_cg:
    scene
    show bg 049
    show meia h1
    with dissolve
    pause
    show meia h2 with dissolve
    pause
    show meia h3 with dissolve
    pause
    show meia h4 with dissolve
    pause
    return


label ghost_cg:
    scene
    show bg 055
    show ghost st11
    with dissolve
    pause
    show ghost ct01 at topright zorder 15 as ct with dissolve
    pause
    show ghost ct02 at topright zorder 15 as ct with dissolve
    pause
    show ghost ct03 at topright zorder 15 as ct with dissolve
    pause
    show ghost ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label doll_cg:
    scene
    if renpy.seen_label("doll_ha"):
        show doll ha1 with dissolve
        pause
        show doll ha2 with dissolve
        pause
        show doll ha3 with dissolve
        pause

        if not renpy.seen_label("doll_hb"):
            return

    show doll hb1 with dissolve
    pause
    show doll hb2 with dissolve
    pause
    show doll hb3 with dissolve
    pause
    return


label zonbe_cg:
    scene
    show zonbe h0 with dissolve
    pause
    show zonbe h1 with dissolve
    pause
    show zonbe h2 with dissolve
    pause
    show zonbe h3 with dissolve
    pause
    show zonbe h4 with dissolve
    pause
    return


label zonbes_cg:
    scene
    show zonbes h1 with dissolve
    pause
    show zonbes h2 with dissolve
    pause
    show zonbes h3 with dissolve
    pause
    show zonbes h4 with dissolve
    pause
    show zonbes h5 with dissolve
    pause
    show zonbes h6 with dissolve
    pause
    show zonbes h7 with dissolve
    pause
    return


label frederika_cg:
    scene
    show frederika h1 with dissolve
    pause
    show frederika h2 with dissolve
    pause
    show frederika h3 with dissolve
    pause
    show frederika h4 with dissolve
    pause
    show frederika tuika with dissolve
    pause
    hide frederika
    show chrom tuika
    with dissolve
    pause
    return


label chrom_cg:
    scene
    show chrom ha1 with dissolve
    pause
    show chrom ha2 with dissolve
    pause
    show chrom ha3 with dissolve
    pause
    show chrom hb1 with dissolve
    pause
    show chrom hb2 with dissolve
    pause
    show chrom hb3 with dissolve
    pause
    return


label fairy_cg:
    scene
    show bg 061
    show fairy st02 at xy(X=270, Y=130)
    show fairy ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show fairy bk03 at xy(X=270, Y=130) as bk
    show fairy ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    hide bk
    show fairy ct11 at topleft zorder 15 as ct
    with dissolve
    pause
    show fairy ct12 at topleft zorder 15 as ct with dissolve
    pause
    show fairy bk01 at xy(X=270, Y=130) as bk
    show fairy ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label elf_cg:
    scene
    show bg 061
    show elf h1
    with dissolve
    pause
    show elf h2 with dissolve
    pause
    show elf h3 with dissolve
    pause
    show elf h4 with dissolve
    pause
    show elf h5 with dissolve
    pause
    return


label tfairy_cg:
    scene
    show bg 061
    show tfairy st02 at xy(X=220, Y=130)
    show tfairy ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show tfairy ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show tfairy bk04 at xy(X=220, Y=130) as bk
    show tfairy ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    hide bk
    show tfairy ct11 at topleft zorder 15 as ct
    with dissolve
    pause
    show tfairy ct12 at topleft zorder 15 as ct with dissolve
    pause
    show tfairy bk05 at xy(X=220, Y=130) as bk
    show tfairy ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    hide bk
    show tfairy ct21 at topright zorder 15 as ct
    with dissolve
    pause
    show tfairy ct22 at topright zorder 15 as ct with dissolve
    pause
    show tfairy bk01 at xy(X=220, Y=130) as bk
    show tfairy bk02 at xy(X=220, Y=130) as bk2
    show tfairy ct23 at topright zorder 15 as ct
    with dissolve
    pause
    return


label fairys_cg:
    scene
    show fairys h1 with dissolve
    pause
    show fairys h2 with dissolve
    pause
    show fairys h3 with dissolve
    pause
    show fairys h4 with dissolve
    pause
    show fairys h5 with dissolve
    pause
    return


label sylph_cg:
    scene
    show bg 061
    show sylph h1
    with dissolve
    pause
    show sylph h2 with dissolve
    pause
    show sylph h3 with dissolve
    pause
    return


label c_dryad_cg:
    scene
    show bg 061
    show c_dryad st11
    with dissolve
    pause
    show c_dryad st12 with dissolve
    pause
    show c_dryad st13 with dissolve
    pause
    show c_dryad st14 with dissolve
    pause
    show c_dryad st15 with dissolve
    pause
    return


label taran_cg:
    scene
    show bg 030
    show taran st02
    show kumo ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumo ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumo ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumo ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    hide ct
    show taran h1
    with dissolve
    pause
    show taran h2 with dissolve
    pause
    show taran h2
    show taran ct01 at right zorder 15 as ct
    with dissolve
    pause
    show taran h3
    show taran ct02 at right zorder 15 as ct
    with dissolve
    pause
    show taran h4
    show taran ct03 at right zorder 15 as ct
    with dissolve
    pause
    show taran h5
    show taran ct04 at right zorder 15 as ct
    with dissolve
    pause
    return


label mino_cg:
    scene
    show bg 019
    show mino st02
    show mino ct11 at topright zorder 15 as ct
    with dissolve
    pause
    show mino ct12 at topright zorder 15 as ct with dissolve
    pause
    show mino ct13 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    show mino h1
    with dissolve
    pause
    show mino ct01 at topright zorder 15 as ct with dissolve
    pause
    show mino h2
    show mino ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show mino ct03 at topright zorder 15 as ct with dissolve
    pause
    show mino h3
    show mino ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show mino ct05 at topright zorder 15 as ct with dissolve
    pause
    return


label sasori_cg:
    scene
    show bg 062
    show sasori h1
    with dissolve
    pause
    show sasori ct01 at topleft zorder 15 as ct with dissolve
    pause
    show sasori h2
    show sasori ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show sasori h3
    show sasori ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show sasori h4
    show sasori ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label lamp_cg:
    scene
    show bg 062
    show lamp h1
    with dissolve
    pause
    show lamp h2 with dissolve
    pause
    show lamp h3 with dissolve
    pause
    show lamp h4 with dissolve
    pause
    show lamp h5 with dissolve
    pause
    show lamp h6 with dissolve
    pause
    show lamp h7 with dissolve
    pause
    show lamp ct01 at topright zorder 15 as ct with dissolve
    pause
    show lamp ct02 at topright zorder 15 as ct with dissolve
    pause
    show lamp ct03 at topright zorder 15 as ct with dissolve
    pause
    show lamp ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label mummy_cg:
    scene
    show bg 068
    show mummy st11
    with dissolve
    pause
    show mummy st12 with dissolve
    pause
    show mummy h1 with dissolve
    pause
    show mummy h2 with dissolve
    pause
    show mummy h3 with dissolve
    pause
    show mummy h4 with dissolve
    pause
    return


label kobura_cg:
    scene
    show bg 068
    show kobura h1
    with dissolve
    pause
    show kobura h2
    show kobura ct01 at left zorder 15 as ct
    with dissolve
    pause
    show kobura h3
    show kobura ct02 at left zorder 15 as ct
    with dissolve
    pause
    show kobura h4
    show kobura ct03 at left zorder 15 as ct
    with dissolve
    pause
    show kobura h5
    show kobura ct04 at left zorder 15 as ct
    with dissolve
    pause
    return


label lamias_cg:
    scene
    show bg 068
    show lamias ha1
    with dissolve
    pause
    show lamias ha2 with dissolve
    pause
    show lamias ha3 with dissolve
    pause
    show lamias ha4 with dissolve
    pause
    show lamias hb1 with dissolve
    pause
    show lamias hb2 with dissolve
    pause
    show lamias hb3 with dissolve
    pause
    show lamias hb4 with dissolve
    pause
    show lamias hb5 with dissolve
    pause
    show lamias hc1 with dissolve
    pause
    show lamias hc2 with dissolve
    pause
    show lamias hc3 with dissolve
    pause
    show lamias hd1 with dissolve
    pause
    show lamias hd2 with dissolve
    pause
    show lamias hd3 with dissolve
    pause
    return


label sphinx_cg:
    scene
    show bg 069
    show sphinx h1
    with dissolve
    pause
    show sphinx ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show sphinx h2
    show sphinx ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show sphinx h3
    show sphinx ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show sphinx h4
    with dissolve
    pause
    show sphinx h4 at xy(X=-160)
    show sphinx cta01 at topright zorder 15 as ct
    with dissolve
    pause
    show sphinx cta02 at topright zorder 15 as ct with dissolve
    pause
    show sphinx cta03 at topright zorder 15 as ct with dissolve
    pause
    show sphinx cta04 at topright zorder 15 as ct with dissolve
    pause
    show sphinx cta05 at topright zorder 15 as ct with dissolve
    pause
    return


label sara_cg:
    scene
    show bg 067
    show sara ha1r
    with dissolve
    pause
    show sara ha2r with dissolve
    pause
    show sara ha3r with dissolve
    pause
    return


label suckvore_cg:
    scene
    show bg 071
    show suckvore h1
    with dissolve
    pause
    show suckvore h2 with dissolve
    pause
    show suckvore h3 with dissolve
    pause
    show suckvore h4 with dissolve
    pause
    show suckvore h5 with dissolve
    pause
    show suckvore h6 with dissolve
    pause
    show suckvore h7 with dissolve
    pause
    return


label wormv_cg:
    scene
    show bg 071
    show wormv h1
    with dissolve
    pause
    show wormv h2 with dissolve
    pause
    show wormv h3 with dissolve
    pause
    show wormv h4 with dissolve
    pause
    show wormv h5 with dissolve
    pause
    show wormv h6 with dissolve
    pause
    show wormv h7 with dissolve
    pause
    show wormv h8 with dissolve
    pause
    show wormv h9 with dissolve
    pause
    return


label ironmaiden_cg:
    scene
    show bg 071
    show ironmaiden h1
    with dissolve
    pause
    show ironmaiden h2 with dissolve
    pause
    show ironmaiden h3 with dissolve
    pause
    return


label lily_cg:
    scene
    show bg 075
    show lily h01
    with dissolve
    pause
    show lily h02 with dissolve
    pause
    show lily h03 with dissolve
    pause
    show lily h04 with dissolve
    pause
    show lily h05 with dissolve
    pause
    show lily h06 with dissolve
    pause
    show lily h07 with dissolve
    pause
    show lily h08 with dissolve
    pause
    show lily h09 with dissolve
    pause
    show lily h10 with dissolve
    pause
    show lily h11 with dissolve
    pause
    show lily h12 with dissolve
    pause
    show lily h13 with dissolve
    pause
    return


label arizigoku_cg:
    scene
    show arizigoku h1 with dissolve
    pause
    show arizigoku h2 with dissolve
    pause
    show arizigoku h3 with dissolve
    pause
    show arizigoku h4 with dissolve
    pause
    return


label sandw_cg:
    scene
    show bg 076
    show sandw h1
    with dissolve
    pause
    show sandw h2 with dissolve
    pause
    show sandw h3 with dissolve
    pause
    show sandw ct01 at topleft zorder 15 as ct with dissolve
    pause
    show sandw h4
    show sandw ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show sandw h5
    show sandw ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show sandw ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label gnome_cg:
    scene
    show bg 076
    show gnome h0
    with dissolve
    pause
    show gnome h1 with dissolve
    pause
    show gnome h2 with dissolve
    pause
    show gnome h3 with dissolve
    pause
    show gnome h4 with dissolve
    pause
    return


label ilias1_cg:
    scene
    show bg 001
    show ilias ct01 at topright zorder 15 as ct
    show ilias st02 at xy(X=-100)
    with dissolve
    pause
    show ilias ct02 at topright zorder 15 as ct
    show ilias bk01 as bk at xy(X=-100)
    with dissolve
    pause
    return


label alice2_cg:
    scene
    show bg 083
    show alice h06_1r
    with dissolve
    pause
    show alice h06_2r with dissolve
    pause
    show alice h06_3r with dissolve
    pause
    show alice h06_4r with dissolve
    pause
    show alice h06_5r with dissolve
    pause
    show alice h06_6r with dissolve
    pause
    show alice h06_7r with dissolve
    pause

    if not persistent.hsean_alice7:
        return

    show alice h07_1r with dissolve
    pause
    show alice h07_2r with dissolve
    pause
    show alice h07_3r with dissolve
    pause
    show alice h07_4r with dissolve
    pause
    show alice h07_5r with dissolve
    pause

    if not persistent.hsean_alice8:
        return

    show bg 164
    show alice h08_1r
    with dissolve
    pause
    show alice h08_2r with dissolve
    pause
    show alice h08_3r with dissolve
    pause
    show alice h08_4r with dissolve
    pause
    return


label centa_cg:
    scene
    show bg 077
    show centa h1
    with dissolve
    pause
    show centa ct01 at topright zorder 15 as ct with dissolve
    pause
    show centa ct02 at topright zorder 15 as ct
    show centa h2
    with dissolve
    pause
    show centa ct03 at topright zorder 15 as ct with dissolve
    pause
    return


label kaeru_cg:
    scene
    show bg 079

    if renpy.seen_label("kaeru_ha"):
        show kaeru ha1
        with dissolve
        pause
        show kaeru ha2 with dissolve
        pause
        show kaeru ha3 with dissolve
        pause
        show kaeru ha4 with dissolve
        pause
        show kaeru ha5 with dissolve
        pause
        show kaeru ha6 with dissolve
        pause
        show kaeru ha7 with dissolve
        pause

        if not renpy.seen_label("kaeru_hb"):
            return

    show kaeru hb0 with dissolve
    pause
    show kaeru hb1 with dissolve
    pause
    show kaeru hb2 with dissolve
    pause
    show kaeru hb3 with dissolve
    pause
    show kaeru hb4 with dissolve
    pause
    return


label alraune_cg:
    scene
    show bg 078
    show alraune st02
    show alraune ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show alraune ct02 at topleft zorder 15 as ct with dissolve
    pause
    show alraune ct03 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show alraune h1
    with dissolve
    pause
    show alraune h2 with dissolve
    pause
    show alraune h3 with dissolve
    pause
    show alraune h4 with dissolve
    pause
    show alraune h5 with dissolve
    pause
    show alraune h6 with dissolve
    pause
    return


label dullahan_cg:
    scene
    show bg 084
    show dullahan h1
    with dissolve
    pause
    show dullahan h2 with dissolve
    pause
    show dullahan h3 with dissolve
    pause
    show dullahan h4 with dissolve
    pause
    show dullahan h5 with dissolve
    pause
    show dullahan h6 with dissolve
    pause
    show dullahan h7 with dissolve
    pause
    show dullahan h8 with dissolve
    pause
    return


label cerberus_cg:
    scene
    show bg 084
    show cerberus st02
    show cerberus ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show cerberus ct02 at topleft zorder 15 as ct
    show cerberus bk04 as bk
    with dissolve
    pause
    show cerberus ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show cerberus ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label alma_elma2_cg:
    scene
    show bg 084

    if renpy.seen_label("alma_elma2_ha"):
        show alma_elma he1 with dissolve
        pause
        show alma_elma he2 with dissolve
        pause
        show alma_elma he3 with dissolve
        pause
        show alma_elma he4 with dissolve
        pause

        if not renpy.seen_label("alma_elma2_hb"):
            return

    show alma_elma hf1 with dissolve
    pause
    show alma_elma ct11 at topright zorder 15 as ct with dissolve
    pause
    show alma_elma ct12 at topright zorder 15 as ct
    show alma_elma hf2
    with dissolve
    pause
    show alma_elma ct13 at topright zorder 15 as ct
    show alma_elma hf3
    with dissolve
    pause
    return


label yukionna_cg:
    scene
    show bg 085

    if renpy.seen_label("yukionna_ha"):
        show yukionna h1 with dissolve
        pause
        show yukionna h2 with dissolve
        pause
        show yukionna h3 with dissolve
        pause
        show yukionna h4 with dissolve
        pause
        show yukionna h5 with dissolve
        pause
        show yukionna h6 with dissolve
        pause

        if not renpy.seen_label("yukionna_hb"):
            return

    show yukionna st11 with dissolve
    pause
    show yukionna ct01 at topright zorder 15 as ct with dissolve
    pause
    show yukionna ct02 at topright zorder 15 as ct with dissolve
    pause
    show yukionna ct03 at topright zorder 15 as ct with dissolve
    pause
    show yukionna ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label nekomata_cg:
    scene
    show bg 086
    show nekomata h1 with dissolve
    pause
    show nekomata h2 with dissolve
    pause
    show nekomata h3 with dissolve
    pause
    show nekomata h4 with dissolve
    pause
    show nekomata h5 with dissolve
    pause
    return


label samuraielf_cg:
    scene
    show bg 086
    show samuraielf st02
    show samuraielf ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show samuraielf ct02 at topleft zorder 15 as ct with dissolve
    pause
    show samuraielf ct03 at topleft zorder 15 as ct with dissolve
    pause
    show samuraielf ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label kunoitielf_cg:
    scene
    show bg 086
    show kunoitielf st02
    show kunoitielf ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kunoitielf ct02 at topleft zorder 15 as ct
    show kunoitielf bk05 as bk
    with dissolve
    pause
    show kunoitielf ct03 at topleft zorder 15 as ct with dissolve
    pause
    show kunoitielf ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label yamatanooroti_cg:
    scene
    show bg 092
    show yamatanooroti h01
    with dissolve
    pause
    show yamatanooroti h02 with dissolve
    pause
    show yamatanooroti h03 with dissolve
    pause
    show yamatanooroti h04 with dissolve
    pause
    show yamatanooroti h05 with dissolve
    pause
    show yamatanooroti h06 with dissolve
    pause
    show yamatanooroti h07 with dissolve
    pause
    show yamatanooroti h08 with dissolve
    pause
    show yamatanooroti h09 with dissolve
    pause
    show yamatanooroti h10 with dissolve
    pause
    show yamatanooroti h11 with dissolve
    pause
    return


label moss_cg:
    scene
    show bg 093
    show moss h1
    show moss ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show moss h2
    show moss ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show moss h3
    with dissolve
    pause
    show moss h4
    show moss ct03 at topright zorder 15 as ct
    with dissolve
    pause
    return


label mosquito_cg:
    scene
    show bg 093
    show mosquito h1
    show mosquito ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show mosquito h2
    show mosquito ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show mosquito h3
    with dissolve
    pause
    show mosquito h4
    show mosquito ct03 at topright zorder 15 as ct
    with dissolve
    pause
    return


label imomusi_cg:
    scene
    show bg 093
    show imomusi h1a at xy(X=180, Y=160) with dissolve
    pause
    show imomusi h2a at xy(X=180, Y=160) with dissolve
    pause
    show imomusi h1 at xy(X=180, Y=160) with dissolve
    pause
    show imomusi h2 at xy(X=180, Y=160) with dissolve
    pause
    show imomusi h3 at xy(X=180, Y=160) with dissolve
    pause
    show imomusi h4 at xy(X=180, Y=160) with dissolve
    pause
    return


label mukade_cg:
    scene
    show bg 094
    show mukade h1
    with dissolve
    pause
    show mukade ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show mukade h2
    show mukade ct02 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show mukade h3
    with dissolve
    pause
    show mukade ct03 at left zorder 15 as ct with dissolve
    pause
    show mukade h4
    show mukade ct04 at left zorder 15 as ct
    with dissolve
    pause
    show mukade h5
    show mukade ct05 at left zorder 15 as ct
    with dissolve
    pause
    show mukade ct06 at left zorder 15 as ct
    with dissolve
    pause
    return


label kaiko_cg:
    scene
    show bg 094
    show kaiko st01
    show kumo ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumo ct02 at topleft zorder 15 as ct with dissolve
    pause
    show kumo ct03 at topleft zorder 15 as ct with dissolve
    pause
    show kumo ct04 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show kaiko h01
    with dissolve
    pause
    show kaiko h11 with dissolve
    pause
    show kaiko h12 with dissolve
    pause
    show kaiko h13 with dissolve
    pause
    show kaiko h14 with dissolve
    pause
    show kaiko h15 with dissolve
    pause
    show kaiko h21 with dissolve
    pause
    show kaiko h22 with dissolve
    pause
    show kaiko h23 with dissolve
    pause
    show kaiko h24 with dissolve
    pause
    show kaiko h25 with dissolve
    pause
    return


label suzumebati_cg:
    scene
    show bg 022
    show suzumebati st01
    show suzumebati ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show suzumebati ct02 at topright zorder 15 as ct
    show suzumebati bk03 as bk
    with dissolve
    pause
    show suzumebati ct03 at topright zorder 15 as ct with dissolve
    pause
    show suzumebati ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label queenbee_cg:
    scene
    show bg 022
    show queenbee st02
    show queenbee ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show queenbee ct02 at topright zorder 15 as ct with dissolve
    pause
    show queenbee ct03 at topright zorder 15 as ct with dissolve
    pause
    show queenbee ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label a_alm_cg:
    scene
    show bg 022
    show a_alm h1a
    with dissolve
    pause
    show a_alm ct01 at topright zorder 15 as ct
    show a_alm h2a
    with dissolve
    pause
    show a_alm ct02 at topright zorder 15 as ct
    show a_alm h3a
    with dissolve
    pause
    show a_alm ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show a_alm h1
    with dissolve
    pause
    show a_alm ct01 at topright zorder 15 as ct
    show a_alm h2
    with dissolve
    pause
    show a_alm ct02 at topright zorder 15 as ct
    show a_alm h3
    with dissolve
    pause
    show a_alm ct03 at topright zorder 15 as ct with dissolve
    pause
    return


label a_looty_cg:
    scene
    show bg 022
    show a_looty h1a at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h2a at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h3a at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h1 at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h2 at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h3 at xy(X=220, Y=130) with dissolve
    pause
    show a_looty h4 at xy(X=220, Y=130) with dissolve
    pause
    return


label a_vore_cg:
    scene
    show bg 094
    show a_vore h1a at xy(X=170, Y=90)
    with dissolve
    pause
    show a_vore h2a at xy(X=170, Y=90) with dissolve
    pause
    show a_vore ct01 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct02 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct03 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct04 at topright as ct
    show a_vore h3a at xy(X=170, Y=90)
    with dissolve
    pause
    hide ct
    show a_vore h1 at xy(X=170, Y=90)
    with dissolve
    pause
    show a_vore h2 at xy(X=170, Y=90)
    with dissolve
    pause
    show a_vore ct01 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct02 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct03 at topright zorder 15 as ct with dissolve
    pause
    show a_vore ct04 at topright zorder 15 as ct
    show a_vore h3 at xy(X=170, Y=90)
    with dissolve
    pause
    show a_vore ct05 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    show a_vore h4 at xy(X=170, Y=90)
    with dissolve
    pause
    show a_vore h4 at xy(X=80, Y=90)
    show a_vore ct11 at topright as ct
    with dissolve
    pause
    show a_vore ct12 at topright as ct with dissolve
    pause
    show a_vore ct13 at topright as ct with dissolve
    pause
    show a_vore ct14 at topright as ct with dissolve
    pause
    show a_vore ct15 at topright as ct with dissolve
    pause
    return


label a_parasol_cg:
    scene
    show bg 094
    show a_parasol h1a
    with dissolve
    pause
    show a_parasol h2a with dissolve
    pause
    show a_parasol h1 with dissolve
    pause
    show a_parasol h2 with dissolve
    pause
    show a_parasol h3 with dissolve
    pause
    show a_parasol h4 with dissolve
    pause
    return


label a_prison_cg:
    scene
    show bg 094
    show a_prison h1 at xy(X=160, Y=70)
    with dissolve
    pause
    show a_prison ct01 at topright as ct zorder 15 with dissolve
    pause
    show a_prison ct02 at topright as ct zorder 15 with dissolve
    pause
    show a_prison ct03 at topright as ct zorder 15 with dissolve
    pause
    show a_prison ct04 at topright as ct zorder 15 with dissolve
    pause
    show a_prison ct05 at topright as ct zorder 15 with dissolve
    pause
    return


label a_emp_cg:
    scene
    show bg 095
    show a_emp ha1a
    with dissolve
    pause
    show a_emp ha2a with dissolve
    pause
    show a_emp ha3a with dissolve
    pause
    show a_emp ha1 with dissolve
    pause
    show a_emp ha2 with dissolve
    pause
    show a_emp ha3 with dissolve
    pause
    show a_emp ha4 with dissolve
    pause
    show a_emp hb1a with dissolve
    pause
    show a_emp hb2a with dissolve
    pause
    show a_emp hb3a with dissolve
    pause
    show a_emp hb4a with dissolve
    pause
    show a_emp hb1 with dissolve
    pause
    show a_emp hb2 with dissolve
    pause
    show a_emp hb3 with dissolve
    pause
    show a_emp hb4 with dissolve
    pause
    return


label dorothy_cg:
    scene
    show bg 095
    show dorothy h1
    with dissolve
    pause
    show dorothy ct01 at topright zorder 15 as ct with dissolve
    pause
    show dorothy h2
    show dorothy ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show dorothy h3
    hide ct
    with dissolve
    pause
    show dorothy ct01 at topright zorder 15 as ct with dissolve
    pause
    show dorothy h4
    show dorothy ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show dorothy h5
    show dorothy ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show dorothy h6
    show dorothy ct04 at topright zorder 15 as ct
    with dissolve
    pause
    return


label rafi_cg:
    scene
    show bg 095
    show rafi h1
    with dissolve
    pause
    show rafi h2 with dissolve
    pause
    show rafi h3 with dissolve
    pause
    show rafi h4 with dissolve
    pause
    show rafi h5 with dissolve
    pause
    show rafi h6 with dissolve
    pause
    show rafi h3
    show rafi ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show rafi ct02 at topright zorder 15 as ct with dissolve
    pause
    show rafi ct03 at topright zorder 15 as ct with dissolve
    pause
    show rafi ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label dina_cg:
    scene
    show bg 095
    show dina h1
    with dissolve
    pause
    show dina h2
    show dina ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show dina h3
    show dina ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show dina h4
    show dina ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show dina h5
    show dina ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show dina h6
    with dissolve
    pause
    return


label jelly_cg:
    scene
    show bg 097
    show jelly h1
    with dissolve
    pause
    show jelly h2 with dissolve
    pause
    show jelly h3 with dissolve
    pause
    show jelly h4 with dissolve
    pause
    show jelly h5 with dissolve
    pause
    show jelly h6 with dissolve
    pause
    return


label blob_cg:
    scene
    show bg 097
    show blob h1
    with dissolve
    pause
    show blob h2 with dissolve
    pause
    show blob h3 with dissolve
    pause
    show blob h4 with dissolve
    pause
    show blob h5 with dissolve
    pause
    show blob h6 with dissolve
    pause
    show blob h7 with dissolve
    pause
    show blob h8 with dissolve
    pause
    show blob h9 with dissolve
    pause
    return


label slime_green_cg:
    scene
    show bg 097
    show slime_green h1
    with dissolve
    pause
    show slime_green h2 with dissolve
    pause
    show slime_green h3 with dissolve
    pause
    return


label slime_blue_cg:
    scene
    show bg 097
    show slimes hc1
    with dissolve
    pause
    show slimes hc2 with dissolve
    pause
    show slimes hc3 with dissolve
    pause
    return


label slime_red_cg:
    scene
    show bg 097
    show slimes hb1
    with dissolve
    pause
    show slimes hb2 with dissolve
    pause
    show slimes hb3 with dissolve
    pause
    return


label slime_purple_cg:
    scene
    show bg 097
    show slimes ha1
    with dissolve
    pause
    show slimes ha2 with dissolve
    pause
    show slimes ha3 with dissolve
    pause
    show slimes hd1 with dissolve
    pause
    show slimes hd2 with dissolve
    pause
    show slimes hd3 with dissolve
    pause
    return


label erubetie1_cg:
    scene
    show bg 097
    show erubetie ha1r
    with dissolve
    pause
    show erubetie ha2r with dissolve
    pause
    show erubetie ha3r with dissolve
    pause
    show erubetie ha4r with dissolve
    pause
    return


label undine_cg:
    scene
    show bg 097
    show undine ha1
    with dissolve
    pause
    show undine ct01 at topleft zorder 15 as ct with dissolve
    pause
    show undine ct02 at topleft zorder 15 as ct with dissolve
    pause
    show undine ha2 with dissolve
    pause
    show undine ct03 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show undine ha3 with dissolve
    pause
    return


label kamakiri_cg:
    scene
    show bg 061
    show kamakiri ha1
    with dissolve
    pause
    show kamakiri ha2 with dissolve
    pause
    show kamakiri ha3 with dissolve
    pause
    show kamakiri ha4 with dissolve
    pause
    show kamakiri ha5 with dissolve
    pause
    show kamakiri hb1 with dissolve
    pause
    show kamakiri hb2 with dissolve
    pause
    show kamakiri hb3 with dissolve
    pause
    show kamakiri hb4 with dissolve
    pause
    return


label scylla_cg:
    scene
    show bg 099
    show scylla h0
    with dissolve
    pause
    show scylla h1 with dissolve
    pause
    show scylla h2 with dissolve
    pause
    show scylla h3 with dissolve
    pause
    show scylla h4 with dissolve
    pause
    show scylla h5 with dissolve
    pause
    show scylla h6 with dissolve
    pause
    return


label medusa_cg:
    scene
    show bg 041
    show medusa h1
    with dissolve
    pause
    show medusa h2 with dissolve
    pause
    show medusa h3 with dissolve
    pause
    show medusa h4 with dissolve
    pause
    show medusa h5 with dissolve
    pause
    show medusa h6 with dissolve
    pause
    return


label golem_cg:
    scene
    show bg 161
    show golem h1
    with dissolve
    pause
    show golem h2 with dissolve
    pause
    show golem h3 with dissolve
    pause
    show golem h4 with dissolve
    pause
    show golem h5 with dissolve
    pause
    return


label madgolem_cg:
    scene
    show bg 100
    show madgolem st01
    show madgolem ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show madgolem ct02 at topleft zorder 15 as ct
    show madgolem bk01 as bk
    with dissolve
    pause
    show madgolem ct03 at topleft zorder 15 as ct with dissolve
    pause
    show madgolem ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label artm_cg:
    scene
    show bg 100
    show artm st01
    show artm ct01 zorder 15 as ct
    with dissolve
    pause
    show artm ct02 zorder 15 as ct
    with dissolve
    pause
    show artm ct11 zorder 15 as ct
    with dissolve
    pause
    show artm ct12 zorder 15 as ct
    with dissolve
    pause
    show artm ct13 zorder 15 as ct
    with dissolve
    pause
    show artm ct14 zorder 15 as ct
    with dissolve
    pause
    hide ct
    show artm ha1
    with dissolve
    pause
    show artm ha2 with dissolve
    pause
    show artm ha3 with dissolve
    pause
    show artm hb1 with dissolve
    pause
    show artm hb2 with dissolve
    pause
    show artm hb3 with dissolve
    pause
    return


label ant_cg:
    scene
    show bg 101
    show ant st11
    show ant ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show ant ct02 at topleft zorder 15 as ct
    show ant bk04 as bk
    with dissolve
    pause
    show ant ct03 at topleft zorder 15 as ct with dissolve
    pause
    show ant ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label queenant_cg:
    scene
    show bg 102
    show queenant st01
    show queenant ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show queenant ct02 at topleft zorder 15 as ct with dissolve
    pause
    show queenant ct03 at topleft zorder 15 as ct with dissolve
    pause
    show queenant ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label maccubus_cg:
    scene
    show bg 107
    show maccubus st01 at xy(X=400)
    show maccubus ct01 zorder 15 as ct
    with dissolve
    pause
    show maccubus st02 at xy(X=400)
    show maccubus ct02 zorder 15 as ct
    show maccubus bk02 as bk at xy(X=400)
    with dissolve
    pause
    hide bk
    show maccubus st01 at left
    show maccubus ct21 zorder 15 as ct
    with dissolve
    pause
    show maccubus st02
    show maccubus ct22 zorder 15 as ct
    show maccubus bk01 as bk at left
    with dissolve
    pause
    hide ct
    hide bk
    show maccubus h21 at center
    with dissolve
    pause
    show maccubus h22 with dissolve
    pause
    show maccubus h00 with dissolve
    pause
    show maccubus h01 with dissolve
    pause
    show maccubus h02 with dissolve
    pause
    show maccubus h03 with dissolve
    pause
    show maccubus h04 with dissolve
    pause
    show maccubus h05 with dissolve
    pause
    show maccubus h06 with dissolve
    pause
    show maccubus h07 with dissolve
    pause
    return


label minccubus_cg:
    scene
    show bg 163
    show minccubus st01 at left
    show minccubus ct01 zorder 15 as ct
    with dissolve
    pause
    show minccubus st02 at left
    show minccubus ct02 zorder 15 as ct
    show minccubus bk01 as bk
    with dissolve
    pause
    hide bk
    show minccubus st01 at left
    show minccubus ct11 zorder 15 as ct
    with dissolve
    pause
    show minccubus st02 at left
    show minccubus ct12 zorder 15 as ct
    show minccubus bk01 as bk
    with dissolve
    pause
    hide bk
    show minccubus st01 at left
    show minccubus ct21 zorder 15 as ct
    with dissolve
    pause
    show minccubus st02 at left
    show minccubus ct22 zorder 15 as ct
    show minccubus bk02 as bk
    with dissolve
    pause
    hide ct
    hide bk
    show minccubus ha1 at center
    with dissolve
    pause
    show minccubus ha2 with dissolve
    pause
    show minccubus ha3 with dissolve
    pause
    show minccubus ha4 with dissolve
    pause
    show minccubus hc1 with dissolve
    pause
    show minccubus hc2 with dissolve
    pause
    show minccubus hc3 with dissolve
    pause
    show minccubus hc4 with dissolve
    pause
    show minccubus hc5 with dissolve
    pause
    show minccubus hb01 with dissolve
    pause
    show minccubus hb02 with dissolve
    pause
    show minccubus hb03 with dissolve
    pause
    show minccubus hb04 with dissolve
    pause
    show minccubus hb05 with dissolve
    pause
    show minccubus hb06 with dissolve
    pause
    show minccubus hb07 with dissolve
    pause
    show minccubus hb08 with dissolve
    pause
    show minccubus hb09 with dissolve
    pause
    show minccubus hb10 with dissolve
    pause
    show minccubus hb11 with dissolve
    pause
    show minccubus hb12 with dissolve
    pause
    return


label renccubus_cg:
    scene
    show bg 104
    show renccubus st02
    show renccubus ct01 zorder 15 as ct
    with dissolve
    pause
    show renccubus ct02 zorder 15 as ct with dissolve
    pause
    show renccubus ct03 zorder 15 as ct with dissolve
    pause
    show renccubus ct11 zorder 15 as ct with dissolve
    pause
    show renccubus ct12 zorder 15 as ct with dissolve
    pause
    show renccubus ct13 zorder 15 as ct with dissolve
    pause
    show renccubus ct21 zorder 15 as ct with dissolve
    pause
    show renccubus ct22 zorder 15 as ct with dissolve
    pause
    show renccubus ct23 zorder 15 as ct with dissolve
    pause
    hide ct
    show renccubus h1
    with dissolve
    pause
    show renccubus h2 with dissolve
    pause
    show renccubus h3 with dissolve
    pause
    show renccubus h4 with dissolve
    pause
    show renccubus h5 with dissolve
    pause
    show renccubus h6 with dissolve
    pause
    show renccubus h7 with dissolve
    pause
    return


label succubus_cg:
    scene
    show bg 106
    show succubus ha1
    with dissolve
    pause
    show succubus ha2 with dissolve
    pause
    show succubus ha3 with dissolve
    pause
    show succubus ha4 with dissolve
    pause
    show succubus ha5 with dissolve
    pause
    show succubus ha6 with dissolve
    pause
    show succubus hb1 with dissolve
    pause
    show succubus hb2 with dissolve
    pause
    show succubus hb3 with dissolve
    pause
    show succubus hb4 with dissolve
    pause
    show succubus hb5 with dissolve
    pause
    show succubus hb6 with dissolve
    pause
    show succubus hc1 with dissolve
    pause
    show succubus hc2 with dissolve
    pause
    show succubus hc3 with dissolve
    pause
    show succubus hc4 with dissolve
    pause
    show succubus hc5 with dissolve
    pause
    show succubus hc6 with dissolve
    pause
    show succubus hc7 with dissolve
    pause
    show succubus hc8 with dissolve
    pause
    show succubus hc9 with dissolve
    pause
    show succubus hd1 with dissolve
    pause
    show succubus hd2 with dissolve
    pause
    show succubus hd3 with dissolve
    pause
    show succubus hd4 with dissolve
    pause
    return


label witchs_cg:
    scene
    show bg 110
    show witchs hc1
    with dissolve
    pause
    show witchs ct01 at topleft zorder 15 as ct with dissolve
    pause
    show witchs hc2
    show witchs ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show witchs hc3
    show witchs ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show witchs hc4
    show witchs ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    hide ct
    show witchs hd1
    with dissolve
    pause
    show witchs hd2 with dissolve
    pause
    show witchs hd3 with dissolve
    pause
    show witchs hd4 with dissolve
    pause
    show witchs hd5 with dissolve
    pause
    show witchs hd6 with dissolve
    pause
    show witchs hi1 with dissolve
    pause
    show witchs hi2 with dissolve
    pause
    show witchs hi3 with dissolve
    pause
    show witchs hi4 with dissolve
    pause
    show witchs hh1 with dissolve
    pause
    show witchs hh2 with dissolve
    pause
    show witchs hh3 with dissolve
    pause
    show witchs hh4 with dissolve
    pause
    show witchs hg1
    show witchs ct21 at topleft zorder 15 as ct
    with dissolve
    pause
    show witchs hg2
    show witchs hgc1 as bk
    show witchs ct22 at topleft zorder 15 as ct
    show witchs ctc01 zorder 15 as ct2
    with dissolve
    pause
    show witchs hg3
    show witchs hgc2 as bk
    show witchs ct23 at topleft zorder 15 as ct
    show witchs ctc02 zorder 15 as ct2
    with dissolve
    pause
    show witchs ctc03 zorder 15 as ct2 with dissolve
    pause
    hide ct
    hide ct2
    hide bk
    show witchs hj1
    with dissolve
    pause
    show witchs hj2 with dissolve
    pause
    show witchs hj3 with dissolve
    pause
    show witchs hk1 with dissolve
    pause
    show witchs hk2 with dissolve
    pause
    show witchs hk3 with dissolve
    pause

    if renpy.seen_label("witchs_ha"):
        show witchs ha01 with dissolve
        pause
        show witchs ha02 with dissolve
        pause
        show witchs ha03 with dissolve
        pause
        show witchs ha04 with dissolve
        pause
        show witchs ha05 with dissolve
        pause
        show witchs ha06 with dissolve
        pause
        show witchs ha07 with dissolve
        pause
        show witchs ha08 with dissolve
        pause
        show witchs ha09 with dissolve
        pause
        show witchs ha10 with dissolve
        pause

        if not renpy.seen_label("witchs_hb"):
            return

    show witchs hb1 with dissolve
    pause
    show witchs hb2 with dissolve
    pause
    show witchs hb3 with dissolve
    pause
    show witchs hb4 with dissolve
    pause
    show witchs hb5 with dissolve
    pause
    show witchs hf1 with dissolve
    pause
    show witchs hf2 with dissolve
    pause
    show witchs hf3 with dissolve
    pause
    show witchs st01
    show witchs ct11 at xy(X=-200, Y=-100) zorder 15 as ct
    with dissolve
    pause
    hide ct
    show witchs he1
    with dissolve
    pause
    show witchs he2 with dissolve
    pause
    show witchs he3 with dissolve
    pause
    show witchs he4 with dissolve
    pause
    show witchs he5 with dissolve
    pause
    show witchs ct12 at xy(X=-200, Y=-100) zorder 15 as ct with dissolve
    pause
    return


label lilith_cg:
    scene
    show bg 110
    show lilith st02
    show lilith ct01 zorder 15 as ct
    with dissolve
    pause
    show lilith ct02 zorder 15 as ct with dissolve
    pause
    hide ct
    show lilith ha0
    with dissolve
    pause
    show lilith ha1 with dissolve
    pause
    show lilith ha2 with dissolve
    pause
    show lilith ha3 with dissolve
    pause
    show lilith ha4 with dissolve
    pause
    show lilith hb1 with dissolve
    pause
    show lilith hb2 with dissolve
    pause
    show lilith hb3 with dissolve
    pause
    show lilith hb4 with dissolve
    pause
    return


label madaminsect_cg:
    scene
    show bg 111
    show madaminsect h1
    with dissolve
    pause
    show madaminsect ct01 at topright zorder 15 as ct with dissolve
    pause
    show madaminsect h2
    show madaminsect ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show madaminsect h3
    show madaminsect ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show madaminsect h4
    show madaminsect ct04 at topright zorder 15 as ct
    with dissolve
    pause
    return


label madamumbrella_cg:
    scene
    show bg 111
    show madamumbrella h01
    with dissolve
    pause
    show madamumbrella h02 with dissolve
    pause
    show madamumbrella h03 with dissolve
    pause
    show madamumbrella h04 with dissolve
    pause
    show madamumbrella h05 with dissolve
    pause
    show madamumbrella h06 with dissolve
    pause
    show madamumbrella h07 with dissolve
    pause
    show madamumbrella h08 with dissolve
    pause
    show madamumbrella h09 with dissolve
    pause
    show madamumbrella h10 with dissolve
    pause
    show madamumbrella h11 with dissolve
    pause
    return


label maidscyulla_cg:
    scene
    show bg 112
    show maidscyulla h01
    with dissolve
    pause
    show maidscyulla h02 with dissolve
    pause
    show maidscyulla h03 with dissolve
    pause
    show maidscyulla h04 with dissolve
    pause
    show maidscyulla ct11 at topright zorder 15 as ct with dissolve
    pause
    show maidscyulla h05
    show maidscyulla ct12 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show maidscyulla h06
    with dissolve
    pause
    show maidscyulla h07 with dissolve
    pause
    show maidscyulla h08 with dissolve
    pause
    show maidscyulla h09 with dissolve
    pause
    show maidscyulla ct01 at topright zorder 15 as ct with dissolve
    pause
    show maidscyulla ct02 at topright zorder 15 as ct with dissolve
    pause
    show maidscyulla ct03 at topright zorder 15 as ct with dissolve
    pause
    show maidscyulla h10 with dissolve
    pause
    return


label emily_cg:
    scene
    show bg 113
    show emily h5
    with dissolve
    pause
    show emily h6 with dissolve
    pause
    show emily h1 with dissolve
    pause
    show emily ct01 at topright zorder 15 as ct with dissolve
    pause
    show emily h2
    show emily ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show emily h3
    show emily ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show emily ct04 at topright zorder 15 as ct with dissolve
    pause
    show emily h4
    show emily ct05 at topright zorder 15 as ct
    with dissolve
    pause
    return


label cassandra_cg:
    scene
    show bg 160
    show cassandra ha1
    with dissolve
    pause
    show cassandra ha2 with dissolve
    pause
    show cassandra ha3 with dissolve
    pause
    show cassandra ha4 with dissolve
    pause
    show cassandra ha5 with dissolve
    pause
    show cassandra ha6 with dissolve
    pause

    if renpy.seen_label("cassandra_ha"):
        show cassandra hb1 with dissolve
        pause
        show cassandra hb2 with dissolve
        pause
        show cassandra hb3
        show cassandra ct01 at left zorder 15 as ct
        with dissolve
        pause
        show cassandra hb4
        show cassandra ct02 at left zorder 15 as ct
        with dissolve
        pause
        show cassandra hb5
        show cassandra ct03 at left zorder 15 as ct
        with dissolve
        pause

        if not renpy.seen_label("cassandra_hb"):
            return

    hide ct
    show cassandra hc1
    with dissolve
    pause
    show cassandra hc2 with dissolve
    pause
    show cassandra hc3 with dissolve
    pause
    show cassandra hc4 with dissolve
    pause
    show cassandra hc5 with dissolve
    pause
    show cassandra hc6 with dissolve
    pause
    return


label yougan_cg:
    scene
    show bg 115
    show yougan h1
    with dissolve
    pause
    show yougan h2 with dissolve
    pause
    show yougan h3 with dissolve
    pause
    show yougan h4 with dissolve
    pause
    show yougan h5 with dissolve
    pause
    show yougan h6 with dissolve
    pause
    return


label basilisk_cg:
    scene
    show bg 115
    show basilisk st02
    show basilisk ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show basilisk ct02 at topleft zorder 15 as ct
    show basilisk bk05 as bk
    with dissolve
    pause
    show basilisk ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show basilisk ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label dragon_cg:
    scene
    show bg 115
    show dragon h1
    with dissolve
    pause
    show dragon h2 with dissolve
    pause
    show dragon h3 with dissolve
    pause
    show dragon h4 with dissolve
    pause
    return


label salamander_cg:
    scene
    show bg 115
    show salamander ha1
    with dissolve
    pause
    show salamander ha2 with dissolve
    pause
    show salamander ha3 with dissolve
    pause
    return


label granberia3_cg:
    scene
    show bg 115
    show granberia hc1
    with dissolve
    pause
    show granberia hc2 with dissolve
    pause
    show granberia hc3 with dissolve
    pause
    show granberia hc4 with dissolve
    pause
    return


label kani2_cg:
    scene
    show bg 039
    show kani st11
    with dissolve
    pause
    show kani ct01 at topright zorder 15 as ct with dissolve
    pause
    show kani ct02 at topright zorder 15 as ct
    show kani st12
    with dissolve
    pause
    show kani ct03 at topright zorder 15 as ct
    show kani st13
    with dissolve
    pause
    show kani ct11 at topright zorder 15 as ct
    show kani st11
    with dissolve
    pause
    show kani ct12 at topright zorder 15 as ct
    show kani st12
    with dissolve
    pause
    show kani ct13 at topright zorder 15 as ct
    show kani st13
    with dissolve
    pause
    hide ct
    show kani h1
    with dissolve
    pause
    show kani ct21 at right zorder 15 as ct with dissolve
    pause
    show kani h2
    show kani ct22 at right zorder 15 as ct
    with dissolve
    pause
    show kani ct23 at right zorder 15 as ct with dissolve
    pause
    show kani ct24 at right zorder 15 as ct with dissolve
    pause
    hide ct
    show kani h3
    with dissolve
    pause
    show kani ct21 as ct with dissolve
    pause
    show kani h4
    show kani ct22 as ct
    with dissolve
    pause
    show kani ct23 as ct with dissolve
    pause
    show kani ct24 as ct with dissolve
    pause
    return


label dagon_cg:
    scene
    show bg 050
    show dagon h1
    with dissolve
    pause
    show dagon ct01 at right zorder 15 as ct with dissolve
    pause
    show dagon ct01a at right zorder 15 as ct with dissolve
    pause
    show dagon ct02 at right zorder 15 as ct with dissolve
    pause
    show dagon h2
    show dagon ct03 at right zorder 15 as ct
    with dissolve
    pause
    hide ct
    show dagon h3
    with dissolve
    pause
    show dagon ct01 at right zorder 15 as ct with dissolve
    pause
    show dagon ct02 at right zorder 15 as ct with dissolve
    pause
    show dagon ct03 at right zorder 15 as ct
    show dagon h4
    with dissolve
    pause
    show dagon ct04 at right zorder 15 as ct
    show dagon h5
    with dissolve
    pause
    show dagon ct05 at right zorder 15 as ct
    show dagon h6
    with dissolve
    pause
    return


label poseidones_cg:
    scene
    show bg 053
    show poseidones ha1
    with dissolve
    pause
    show poseidones ha2
    show poseidones ct01 at right zorder 15 as ct
    with dissolve
    pause
    show poseidones ct02 at right zorder 15 as ct with dissolve
    pause
    show poseidones ct03 at right zorder 15 as ct with dissolve
    pause
    show poseidones ct04 at right zorder 15 as ct with dissolve
    pause
    hide ct
    show poseidones hb1
    with dissolve
    pause
    show poseidones hb2 with dissolve
    pause
    show poseidones hb3 with dissolve
    pause
    show poseidones hb4 with dissolve
    pause
    return


label seiren_cg:
    scene
    show bg 125
    show seiren st02
    show seiren ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show seiren ct02 at topleft zorder 15 as ct
    show seiren bk07 as bk
    with dissolve
    pause
    show seiren ct03 at topleft zorder 15 as ct with dissolve
    pause
    show seiren ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label hitode_cg:
    scene
    show bg 130
    show hitode ha1
    with dissolve
    pause
    show hitode ct01 at right zorder 15 as ct with dissolve
    pause
    show hitode ha2
    show hitode ct02 at right zorder 15 as ct
    with dissolve
    pause
    show hitode ha3
    hide ct
    with dissolve
    pause
    show hitode ct01 at right zorder 15 as ct with dissolve
    pause
    show hitode ha4
    show hitode ct02 at right zorder 15 as ct
    with dissolve
    pause
    show hitode ha5
    show hitode ct03 at right zorder 15 as ct
    with dissolve
    pause
    show hitode ha6
    show hitode ct04 at right zorder 15 as ct
    with dissolve
    pause
    hide ct
    show hitode hb1
    with dissolve
    pause
    show hitode hb2 with dissolve
    pause
    show hitode hb3 with dissolve
    pause
    show hitode hb4 with dissolve
    pause
    return


label beelzebub_cg:
    scene
    show bg 132
    show beelzebub_a h01
    with dissolve
    pause
    show beelzebub_a h02 with dissolve
    pause
    hide beelzebub_a
    show beelzebub_b h11
    with dissolve
    pause
    show beelzebub_b h12 with dissolve
    pause
    hide beelzebub_b
    show beelzebub_c h21
    with dissolve
    pause
    show beelzebub_c h22 with dissolve
    pause
    hide beelzebub_c
    show beelzebub h31
    with dissolve
    pause
    show beelzebub h32 with dissolve
    pause
    show beelzebub h33 with dissolve
    pause
    show beelzebub h34 with dissolve
    pause
    return


label trickfairy_cg:
    scene
    show bg 135
    show trickfairy h1
    with dissolve
    pause
    show trickfairy h2 with dissolve
    pause
    show trickfairy h3 with dissolve
    pause
    show trickfairy h4 with dissolve
    pause
    return


label queenfairy_cg:
    scene
    show bg 136
    show queenfairy st02
    show queenfairy ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show queenfairy ct02 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct11 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct12 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct21 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct22 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct31 at topright zorder 15 as ct with dissolve
    pause
    show queenfairy ct32 at topright zorder 15 as ct with dissolve
    pause
    return


label queenelf_cg:
    scene
    show bg 136
    show queenelf st02
    show queenelf ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show queenelf ct02 at topleft zorder 15 as ct with dissolve
    pause
    show queenelf ct03 at topleft zorder 15 as ct with dissolve
    pause
    show queenelf ct04 at topleft zorder 15 as ct with dissolve
    pause
    return


label saraevil_cg:
    scene
    show bg 136
    show saraevil st02
    show saraevil cta01 zorder 15 as ct
    with dissolve
    pause
    show saraevil cta02 zorder 15 as ct with dissolve
    pause
    show saraevil cta03 zorder 15 as ct with dissolve
    pause
    show saraevil cta04 zorder 15 as ct with dissolve
    pause
    show saraevil ctb01 zorder 15 as ct with dissolve
    pause
    show saraevil ctb02 zorder 15 as ct with dissolve
    pause
    show saraevil ctb03 zorder 15 as ct with dissolve
    pause
    show saraevil ctb04 zorder 15 as ct with dissolve
    pause
    hide ct
    show saraevil ha1
    with dissolve
    pause
    show saraevil hb1 with dissolve
    pause
    show saraevil hb2 with dissolve
    pause
    show saraevil hb3 with dissolve
    pause
    show saraevil hb4 with dissolve
    pause
    return


label wyvern_cg:
    scene
    show bg 140
    show wyvern st02
    show wyvern ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show wyvern ct02 at topright zorder 15 as ct
    show wyvern bk04 as bk
    with dissolve
    pause
    show wyvern ct03 at topright zorder 15 as ct with dissolve
    pause
    show wyvern ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label kyoryuu_cg:
    scene
    show bg 142
    show kyoryuu st11
    show kyoryuu ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kyoryuu st12
    show kyoryuu ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show kyoryuu st11
    show kyoryuu ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show kyoryuu st12
    show kyoryuu ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    show kyoryuu st12
    show kyoryuu ct05 at topleft zorder 15 as ct
    with dissolve
    pause
    show kyoryuu st12
    show kyoryuu ct06 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label c_beast_cg:
    scene
    show bg 143
    show c_beast st01
    show c_beast ct11 at topright zorder 15 as ct
    with dissolve
    pause
    show c_beast ct12 at topright zorder 15 as ct
    show c_beast bk04 as bk
    with dissolve
    pause
    show c_beast ct13 at topright zorder 15 as ct with dissolve
    pause
    show c_beast ct14 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    hide bk
    show c_beast h1
    with dissolve
    pause
    show c_beast h2 with dissolve
    pause
    show c_beast h3 with dissolve
    pause
    show c_beast h4 with dissolve
    pause
    show c_beast ct01 at topright zorder 15 as ct with dissolve
    pause
    show c_beast h5
    show c_beast ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show c_beast h6
    show c_beast ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show c_beast ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label c_dryad_vore_cg:
    scene
    show bg 144
    show c_dryad_vore st11
    with dissolve
    pause
    show c_dryad_vore st11a  with dissolve
    pause
    show c_dryad_vore st12 with dissolve
    pause
    show c_dryad_vore st13 with dissolve
    pause
    show c_dryad_vore st14 with dissolve
    pause
    show c_dryad_vore st21 with dissolve
    pause
    show c_dryad_vore st21a  with dissolve
    pause
    show c_dryad_vore st22 with dissolve
    pause
    show c_dryad_vore st23 with dissolve
    pause
    show c_dryad_vore st24 with dissolve
    pause
    show c_dryad_vore st25 with dissolve
    pause
    show c_dryad_vore st31 with dissolve
    pause
    show c_dryad_vore st32 with dissolve
    pause
    show c_dryad_vore st33 with dissolve
    pause
    return


label vampire_cg:
    scene
    show bg 142
    show vampire h1
    with dissolve
    pause
    show vampire h2 with dissolve
    pause
    show vampire h3 with dissolve
    pause
    show vampire h4 with dissolve
    pause
    return


label behemoth_cg:
    scene
    show bg 142
    show behemoth st02
    show behemoth ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show behemoth ct02 at topleft zorder 15 as ct
    show behemoth bk05 as bk
    with dissolve
    pause
    show behemoth ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show behemoth ct04 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label esuccubus_cg:
    scene
    show bg 148
    show esuccubus h1
    with dissolve
    pause
    show esuccubus h2 with dissolve
    pause
    show esuccubus h3 with dissolve
    pause
    show esuccubus h4 with dissolve
    pause
    return


label hatibi_cg:
    scene
    show bg 148
    show hatibi st01
    show hatibi ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show hatibi ct02 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct03 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct04 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct11 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct12 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct13 at topright zorder 15 as ct with dissolve
    pause
    show hatibi ct14 at topright zorder 15 as ct with dissolve
    pause

    if renpy.seen_label("hatibi_ha"):
        hide ct
        show hatibi ha1
        with dissolve
        pause
        show hatibi ha2 with dissolve
        pause
        show hatibi ha3 with dissolve
        pause
        show hatibi ha4 with dissolve
        pause
        show hatibi ha5 with dissolve
        pause
        show hatibi ha6 with dissolve
        pause
        show hatibi ha7 with dissolve
        pause

        if not renpy.seen_label("hatibi_hb"):
            return

    show hatibi hb1 with dissolve
    pause
    show hatibi hb2 with dissolve
    pause
    show hatibi hb3 with dissolve
    pause
    show hatibi hb4 with dissolve
    pause
    show hatibi hb5 with dissolve
    pause
    show hatibi hb6 with dissolve
    pause
    show hatibi hb7 with dissolve
    pause
    return


label inp_cg:
    scene
    show bg 149
    show inp_a hd1 with dissolve
    pause
    show inp_a hd2 with dissolve
    pause
    show inp_a hd3 with dissolve
    pause
    show inp_a hd4 with dissolve
    pause
    show inp_a hd5 with dissolve
    pause
    show inp_a hd6 with dissolve
    pause
    show inp_a hd7 with dissolve
    pause
    show inp_a hd8 with dissolve
    pause
    show inp_a hc1 with dissolve
    pause
    show inp_a hc2 with dissolve
    pause
    show inp_a hc3 with dissolve
    pause
    show inp_a hc4 with dissolve
    pause

    if renpy.seen_label("inp_ha"):
        show inp_a ha1 with dissolve
        pause
        show inp_a ha2 with dissolve
        pause
        show inp_a ha3 with dissolve
        pause
        show inp_a ha4 with dissolve
        pause
        show inp_a ha5 with dissolve
        pause
        show inp_a ha6 with dissolve
        pause

        if not renpy.seen_label("inp_hb"):
            return

    show inp_a hb1 with dissolve
    pause
    show inp_a hb2 with dissolve
    pause
    show inp_a hb3 with dissolve
    pause
    show inp_a hb4 with dissolve
    pause
    return


label gigantweapon_cg:
    scene
    show bg 149
    show gigantweapon st01
    show lilith ct01 zorder 15 as ct
    with dissolve
    pause
    show lilith ct02 zorder 15 as ct with dissolve
    pause
    hide ct
    show gigantweapon h1
    with dissolve
    pause
    show gigantweapon h2 with dissolve
    pause
    show gigantweapon h3 with dissolve
    pause
    show gigantweapon h4 with dissolve
    pause
    show gigantweapon h5 with dissolve
    pause
    show gigantweapon h6 with dissolve
    pause
    return


label alma_elma3_cg:
    scene
    show bg 047
    show alma_elma st51
    show alma_elma ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show alma_elma ct05 at topright zorder 15 as ct
    show alma_elma bk21 as bk
    with dissolve
    pause
    show alma_elma ct06 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    hide bk
    show alma_elma hc1
    with dissolve
    pause
    show alma_elma hc2 with dissolve
    pause
    show alma_elma hc3 with dissolve
    pause
    show alma_elma hc4 with dissolve
    pause
    show alma_elma hd0 with dissolve
    pause
    show alma_elma hd1 with dissolve
    pause
    show alma_elma hd2 with dissolve
    pause
    show alma_elma hd3 with dissolve
    pause
    show alma_elma hd4 with dissolve
    pause
    return


label tamamo2_cg:
    scene
    show bg 047
    show tamamo st11
    show tamamo ctc01 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo ctc02 at topright zorder 15 as ct with dissolve
    pause
    show tamamo bk03 as bk
    show tamamo ctc03 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo ctc04 at topright zorder 15 as ct with dissolve
    pause
    hide bk
    show tamamo cta01 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo cta02 at topright zorder 15 as ct with dissolve
    pause
    show tamamo bk04 as bk
    show tamamo cta03 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo cta04 at topright zorder 15 as ct with dissolve
    pause
    hide bk
    show tamamo ctb01 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo ctb02 at topright zorder 15 as ct with dissolve
    pause
    show tamamo bk04 as bk
    show tamamo ctb03 at topright zorder 15 as ct
    with dissolve
    pause
    show tamamo ctb04 at topright zorder 15 as ct with dissolve
    pause
    hide bk
    hide ct
    show tamamo hc1
    with dissolve
    pause
    show tamamo hc2 with dissolve
    pause
    show tamamo hc3 with dissolve
    pause
    show tamamo hc4 with dissolve
    pause
    show tamamo hc5 with dissolve
    pause
    show tamamo hc6 with dissolve
    pause
    show tamamo hc7 with dissolve
    pause
    show tamamo ha1 with dissolve
    pause
    show tamamo ha2 with dissolve
    pause
    show tamamo ha3 with dissolve
    pause
    show tamamo ha4 with dissolve
    pause
    show tamamo ha5 with dissolve
    pause
    show tamamo ha6 with dissolve
    pause
    show tamamo ha7 with dissolve
    pause
    show tamamo hb1 with dissolve
    pause
    show tamamo hb2 with dissolve
    pause
    show tamamo hb3 with dissolve
    pause
    show tamamo hb4 with dissolve
    pause
    show tamamo hb5 with dissolve
    pause
    show tamamo hb6 with dissolve
    pause
    show tamamo hb7 with dissolve
    pause
    show tamamo hb8 with dissolve
    pause
    return


label erubetie2_cg:
    scene
    show bg 047
    show erubetie hb1r
    with dissolve
    pause
    show erubetie hb2r with dissolve
    pause
    show erubetie hb3r with dissolve
    pause
    show erubetie hb4r with dissolve
    pause
    show erubetie hb5r with dissolve
    pause
    return


label granberia4_cg:
    scene
    show bg 047
    show granberia hd1
    with dissolve
    pause
    show granberia hd2 with dissolve
    pause
    show granberia hd3 with dissolve
    pause
    show granberia hd4 with dissolve
    pause
    return


label alice3_cg:
    scene
    show alice h09_1r
    show bg 141
    with dissolve
    pause
    show alice h09_2r with dissolve
    pause
    show alice h09_3r with dissolve
    pause
    show alice h09_4r with dissolve
    pause
    return


label ilias2_cg:
    scene
    show bg 001
    show ilias h1
    with dissolve
    pause
    show ilias h2 with dissolve
    pause
    show ilias h3 with dissolve
    pause
    return


#########################################
# Третья часть
#########################################
label cupid_cg:
    scene
    show bg 047
    show cupid hb1
    with dissolve
    pause
    show cupid hb2 with dissolve
    pause
    show cupid hb3 with dissolve
    pause
    show cupid hb4 with dissolve
    pause
    show cupid hb5 with dissolve
    pause
    show cupid hb6 with dissolve
    pause
    show cupid ha1 with dissolve
    pause
    show cupid ha2 with dissolve
    pause
    show cupid ha3 with dissolve
    pause
    show cupid ha4 with dissolve
    pause
    show cupid ha5 with dissolve
    pause
    show cupid ha6 with dissolve
    pause
    show cupid ha7 with dissolve
    pause
    return


label valkyrie_cg:
    scene
    show bg 047
    show valkyrie ha1
    with dissolve
    pause
    show valkyrie ha2 with dissolve
    pause
    show valkyrie ha3 with dissolve
    pause
    show valkyrie ha4 with dissolve
    pause
    show valkyrie ha5 with dissolve
    pause
    show valkyrie ha6 with dissolve
    pause
    show valkyrie ha7 with dissolve
    pause
    show valkyrie ha8 with dissolve
    pause
    return


label ariel_cg:
    scene
    show bg 047
    show ariel h1
    with dissolve
    pause
    show ariel h2 with dissolve
    pause
    show ariel h3 with dissolve
    pause
    show ariel h4 with dissolve
    pause
    return


label c_s2_cg:
    scene
    show bg 184
    show c_s2 ha1
    with dissolve
    pause
    show c_s2 ha2 with dissolve
    pause
    show c_s2 ha3 with dissolve
    pause
    show c_s2 ha4 with dissolve
    pause
    show c_s2 ha5 with dissolve
    pause
    show c_s2 hb1 with dissolve
    pause
    show c_s2 ct01 at right zorder 15 as ct with dissolve
    pause
    show c_s2 ct02 at right zorder 15 as ct
    show c_s2 hb2
    with dissolve
    pause
    show c_s2 ct03 at right zorder 15 as ct
    show c_s2 hb3
    with dissolve
    pause
    show c_s2 ct04 at right zorder 15 as ct
    show c_s2 hb4
    with dissolve
    pause
    return


label c_a3_cg:
    scene
    show bg 185
    show c_a3 ha1
    with dissolve
    pause
    show c_a3 ha2 with dissolve
    pause
    show c_a3 ha3 with dissolve
    pause
    show c_a3 ha4 with dissolve
    pause
    show c_a3 ha5 with dissolve
    pause
    show c_a3 hb1 with dissolve
    pause
    show c_a3 hb2 with dissolve
    pause
    show c_a3 hb3 with dissolve
    pause
    show c_a3 hb4 with dissolve
    pause
    return


label stein1_cg:
    scene
    show bg 186

    if renpt.seen_label("stein1_ha"):
        show stein st21
        show stein ct11 at right zorder 15 as ct
        with dissolve
        pause
        show stein ct12 at right zorder 15 as ct with dissolve
        pause
        show stein ct13 at right zorder 15 as ct with dissolve
        pause
        show stein ct14 at right zorder 15 as ct with dissolve
        pause
        hide ct
        show stein hb0
        with dissolve
        pause
        show stein hb1 with dissolve
        pause
        show stein hb2 with dissolve
        pause
        show stein ct01 at right zorder 15 as ct with dissolve
        pause
        show stein ct02 at right zorder 15 as ct
        show stein hb3
        with dissolve
        pause
        show stein ct03 at right zorder 15 as ct
        show stein hb4
        with dissolve
        pause
        hide ct
        show stein hb5
        with dissolve
        pause
        show stein ct04 at right zorder 15 as ct with dissolve
        pause

        if not renpy.seen_label("stein1_hb"):
            return

    hide ct
    show stein hc01
    with dissolve
    pause
    show stein hc02 with dissolve
    pause
    show stein hc03 with dissolve
    pause
    show stein hc04 with dissolve
    pause
    show stein hc05 with dissolve
    pause
    show stein hc06 with dissolve
    pause
    show stein hc07 with dissolve
    pause
    show stein hc08 with dissolve
    pause
    show stein hc09 with dissolve
    pause
    show stein hc10 with dissolve
    pause
    show stein hc11 with dissolve
    pause
    show stein hc12 with dissolve
    pause
    show stein hc13 with dissolve
    pause
    show stein hc14 with dissolve
    pause
    show stein hc15 with dissolve
    pause
    show stein hc16 with dissolve
    pause
    show stein hc17 with dissolve
    pause
    show stein hc18 with dissolve
    pause
    show stein hc19 with dissolve
    pause
    show stein hc20 with dissolve
    pause
    show stein hc21 with dissolve
    pause
    show stein hc22 with dissolve
    pause
    show stein hc23 with dissolve
    pause
    show stein hc24 with dissolve
    pause
    show stein hc25 with dissolve
    pause
    show stein hc26 with dissolve
    pause
    show stein hc27 with dissolve
    pause
    show stein hc28 with dissolve
    pause
    show stein hc29 with dissolve
    pause
    show stein hc30 with dissolve
    pause
    show stein hc31 with dissolve
    pause
    show stein hc32 with dissolve
    pause
    show stein hc33 with dissolve
    pause
    show stein hc34 with dissolve
    pause
    show stein hc35 with dissolve
    pause
    show stein hc36 with dissolve
    pause
    show stein hc37 with dissolve
    pause
    show stein hc38 with dissolve
    pause
    show stein hc39 with dissolve
    pause
    show stein hc40 with dissolve
    pause
    show stein hc41 with dissolve
    pause
    show stein hc42 with dissolve
    pause
    show stein hc43 with dissolve
    pause
    show stein hc44 with dissolve
    pause
    show stein hc45 with dissolve
    pause
    show stein hc46 with dissolve
    pause
    show stein hc47 with dissolve
    pause
    show stein hc48 with dissolve
    pause
    show stein hc49 with dissolve
    pause
    show stein hc50 with dissolve
    pause
    show stein hc51 with dissolve
    pause
    return


label ranael_cg:
    scene
    show bg 187
    show ranael h0
    with dissolve
    pause
    show ranael ha1 with dissolve
    pause
    show ranael ha2 with dissolve
    pause
    show ranael ha3 with dissolve
    pause
    show ranael ha4 with dissolve
    pause
    show ranael hb1 with dissolve
    pause
    show ranael hb2 with dissolve
    pause
    show ranael hb3 with dissolve
    pause
    show ranael hb4 with dissolve
    pause
    show ranael hc1 with dissolve
    pause
    show ranael hc2 with dissolve
    pause
    show ranael hc3 with dissolve
    pause
    show ranael hc4 with dissolve
    pause
    show ranael hd1 with dissolve
    pause
    show ranael hd2 with dissolve
    pause
    show ranael hd3 with dissolve
    pause
    show ranael hd4 with dissolve
    pause
    show ranael he1 with dissolve
    pause
    show ranael he2 with dissolve
    pause
    show ranael he3 with dissolve
    pause
    show ranael he4 with dissolve
    pause
    show ranael he5 with dissolve
    pause
    show ranael hf1 with dissolve
    pause
    show ranael ct01 at right zorder 15 as ct with dissolve
    pause
    show ranael ct02 at right zorder 15 as ct
    show ranael hf2
    with dissolve
    pause
    show ranael ct03 at right zorder 15 as ct
    show ranael hf3
    with dissolve
    pause
    show ranael ct04 at right zorder 15 as ct
    show ranael hf4
    with dissolve
    pause
    return


label c_tangh_cg:
    scene
    show bg 029
    show c_tangh st11
    with dissolve
    pause
    show c_tangh h1 with dissolve
    pause
    show c_tangh h2 with dissolve
    pause
    show c_tangh h3 with dissolve
    pause
    show c_tangh h4 with dissolve
    pause
    show c_tangh h5 with dissolve
    pause
    show c_tangh h6 with dissolve
    pause
    hide c_tangh
    show mob elf2 h1
    with dissolve
    pause
    show mob elf2 h2 with dissolve
    pause
    return


label angels_cg:
    scene
    show bg 029
    show angels h01
    with dissolve
    pause
    show angels h02 with dissolve
    pause
    show angels h03 with dissolve
    pause
    show angels h04 with dissolve
    pause
    show angels h05 with dissolve
    pause
    show angels h06 with dissolve
    pause
    show angels h07 with dissolve
    pause
    show angels h08 with dissolve
    pause
    show angels h09 with dissolve
    pause
    show angels h10 with dissolve
    pause
    return


label nagael_cg:
    scene
    show bg 029
    show nagael h1
    with dissolve
    pause
    show nagael ct01 at right zorder 15 as ct with dissolve
    pause
    show nagael h2
    show nagael ct02 at right zorder 15 as ct
    with dissolve
    pause
    show nagael h3 with dissolve
    pause
    show nagael h4
    show nagael ct03 at right zorder 15 as ct
    with dissolve
    pause
    hide ct
    hide nagael
    show mob elf1 h1
    with dissolve
    pause
    show mob elf1 h2 with dissolve
    pause
    return


label c_slag_cg:
    scene
    show bg 018
    show c_slag h01
    with dissolve
    pause
    show c_slag ct01 at topright zorder 15 as ct with dissolve
    pause
    show c_slag h02
    show c_slag ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag ct03 at topright zorder 15 as ct with dissolve
    pause
    show c_slag ct04 at topright zorder 15 as ct with dissolve
    pause
    show c_slag ct11 at topleft zorder 15 as ct with dissolve
    pause
    show c_slag h03
    show c_slag ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag ct13 at topleft zorder 15 as ct with dissolve
    pause
    show c_slag ct14 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show c_slag h04
    with dissolve
    pause
    show c_slag h05
    show c_slag ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h06
    show c_slag ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h07
    show c_slag ct11 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag h08
    show c_slag ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag h06
    show c_slag ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h09
    show c_slag ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h08
    show c_slag ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag h10
    show c_slag ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag h09
    show c_slag ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h11
    show c_slag ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show c_slag h10
    show c_slag ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    show c_slag h12
    show c_slag ct14 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label c_tentacle_cg:
    scene
    show bg 018
    show c_tentacle st11
    with dissolve
    pause
    show c_tentacle h1 with dissolve
    pause
    show c_tentacle h2 with dissolve
    pause
    show c_tentacle h3 with dissolve
    pause
    show c_tentacle h4 with dissolve
    pause
    return


label mariel_cg:
    scene
    show bg 017

    if renpy.seen_label("mariel_ha"):
        show mariel ha01
        with dissolve
        pause
        show mariel ha02 with dissolve
        pause
        show mariel ha03 with dissolve
        pause
        show mariel ha04 with dissolve
        pause
        show mariel ha05 with dissolve
        pause
        show mariel ha06 with dissolve
        pause
        show mariel ha07 with dissolve
        pause
        show mariel ha08 with dissolve
        pause
        show mariel ha09 with dissolve
        pause
        show mariel ha10 with dissolve
        pause

        if not renpy.seen_label("mariel_hb"):
            return

    show mariel hb1
    with dissolve
    pause
    show mariel hb2 with dissolve
    pause
    show mariel hb3 with dissolve
    pause
    show mariel hb4 with dissolve
    pause
    show mariel hb5 with dissolve
    pause
    show mariel hb6 with dissolve
    pause
    show mariel hb7 with dissolve
    pause
    show mariel hb8 with dissolve
    pause
    show mariel hb9 with dissolve
    pause
    show mariel hc1 with dissolve
    pause
    show mariel hc2 with dissolve
    pause
    show mariel hc3 with dissolve
    pause
    show mariel hc4 with dissolve
    pause
    show mariel hc5 with dissolve
    pause
    show mariel hc6 with dissolve
    pause
    show mariel hc7 with dissolve
    pause
    show mariel hc8 with dissolve
    pause
    show mariel hc9 with dissolve
    pause
    return


label c_prison_cg:
    scene
    show bg 220
    show c_prison h1
    with dissolve
    pause
    show c_prison ct01 at topright zorder 15 as ct with dissolve
    pause
    show c_prison ct02 at topright zorder 15 as ct with dissolve
    pause
    show c_prison h2
    show c_prison ct03 at topright zorder 15 as ct
    with dissolve
    pause
    show c_prison h3
    show c_prison ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show c_prison h4
    show c_prison ct05 at topright zorder 15 as ct
    with dissolve
    pause
    return


label c_medulahan_cg:
    scene
    show bg 220
    show c_medulahan st11
    with dissolve
    pause
    show c_medulahan ha1 with dissolve
    pause
    show c_medulahan ct01 at left zorder 15 as ct with dissolve
    pause
    show c_medulahan ct02 at left zorder 15 as ct
    show c_medulahan ha2
    with dissolve
    pause
    show c_medulahan ct03 at left zorder 15 as ct
    show c_medulahan ha3
    with dissolve
    pause
    show c_medulahan ct04 at left zorder 15 as ct
    show c_medulahan ha4
    with dissolve
    pause
    hide ct
    show c_medulahan hb0
    with dissolve
    pause
    show c_medulahan hb1 with dissolve
    pause
    show c_medulahan ct11 at topleft zorder 15 as ct with dissolve
    pause
    show c_medulahan ct12 at topleft zorder 15 as ct
    show c_medulahan hb2
    with dissolve
    pause
    show c_medulahan ct13 at topleft zorder 15 as ct
    show c_medulahan hb3
    with dissolve
    pause
    show c_medulahan ct14 at topleft zorder 15 as ct
    show c_medulahan hb4
    with dissolve
    pause
    hide ct
    show c_medulahan hb5
    with dissolve
    pause
    show c_medulahan ct11 at topleft zorder 15 as ct with dissolve
    pause
    show c_medulahan ct12 at topleft zorder 15 as ct
    show c_medulahan hb6
    with dissolve
    pause
    show c_medulahan ct13 at topleft zorder 15 as ct
    show c_medulahan hb7
    with dissolve
    pause
    show c_medulahan ct14 at topleft zorder 15 as ct
    show c_medulahan hb8
    with dissolve
    pause
    return


label trinity_cg:
    scene
    show bg 221
    show trinity ct01 at left zorder 15 as ct
    show trinity_a ha1
    with dissolve
    pause
    show trinity ct02 at topleft zorder 15 as ct
    show trinity_a ha2
    with dissolve
    pause
    show trinity ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    show trinity ct04 at topleft zorder 15 as ct
    show trinity_a ha3
    with dissolve
    pause
    show trinity ct05 at topleft zorder 15 as ct
    show trinity_a ha4
    with dissolve
    pause
    show trinity ct06 at topleft zorder 15 as ct
    show trinity_a ha5
    with dissolve
    pause
    show trinity ct07 at topleft zorder 15 as ct
    show trinity_a ha6
    with dissolve
    pause
    show trinity ct08 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    hide trinity_a
    show trinity hb1
    with dissolve
    pause
    show trinity hb2 with dissolve
    pause
    show trinity hb3 with dissolve
    pause
    show trinity hb4 with dissolve
    pause
    show trinity hb5 with dissolve
    pause
    show trinity hb6 with dissolve
    pause
    show trinity hb7 with dissolve
    pause
    show trinity hb8 with dissolve
    pause
    show trinity hb9 with dissolve
    pause
    return


label c_bug_cg:
    scene
    show bg 048
    show c_bug st02
    show c_bug ct01 as ct
    with dissolve
    pause
    show c_bug ct02 as ct with dissolve
    pause
    show c_bug ct11 as ct with dissolve
    pause
    show c_bug ct12 as ct with dissolve
    pause
    show c_bug ct21 as ct with dissolve
    pause
    show c_bug ct22 as ct with dissolve
    pause
    show c_bug ct23 as ct with dissolve
    pause
    hide ct
    show c_bug hb01 with dissolve
    pause
    show c_bug hb02 with dissolve
    pause
    show c_bug hb03 with dissolve
    pause
    show c_bug hb04 with dissolve
    pause
    show c_bug hb05 with dissolve
    pause
    show c_bug hb06 with dissolve
    pause
    show c_bug hb07 with dissolve
    pause
    show c_bug hb08 with dissolve
    pause
    show c_bug hb09 with dissolve
    pause
    show c_bug hb10 with dissolve
    pause
    show c_bug hb11 with dissolve
    pause
    show c_bug hb12 with dissolve
    pause
    show c_bug hb13 with dissolve
    pause
    show c_bug hb14 with dissolve
    pause
    show c_bug hb15 with dissolve
    pause
    show c_bug hb16 with dissolve
    pause
    show c_bug hb17 with dissolve
    pause
    show c_bug hb18 with dissolve
    pause
    show c_bug hb19 with dissolve
    pause
    show c_bug hb20 with dissolve
    pause
    return


label sisterlamia_cg:
    scene
    show bg 048
    show sisterlamia h1
    show sisterlamia ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show sisterlamia h2
    show sisterlamia ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show sisterlamia h3
    show sisterlamia ct03 at topleft zorder 15 as ct
    with dissolve
    pause
    hide ct
    show sisterlamia hb1
    with dissolve
    pause
    show sisterlamia hb2
    show sisterlamia bk08 as bk
    with dissolve
    pause
    return


label catoblepas_cg:
    scene
    show bg 044
    show catoblepas h1
    with dissolve
    pause
    show catoblepas h2 with dissolve
    pause
    return


label muzukiel_cg:
    scene
    show bg 157
    show muzukiel st21
    with dissolve
    pause
    show muzukiel st22 with dissolve
    pause
    show muzukiel ha1 with dissolve
    pause
    show muzukiel ha2 with dissolve
    pause
    show muzukiel ha3 with dissolve
    pause
    show muzukiel ha4 with dissolve
    pause
    show muzukiel hb1 with dissolve
    pause
    show muzukiel hb2 with dissolve
    pause
    show muzukiel hb3 with dissolve
    pause
    show muzukiel hb4 with dissolve
    pause
    return


label mermaid_cg:
    scene
    show bg 037
    show mermaid h1
    with dissolve
    pause
    show mermaid h2 with dissolve
    pause
    show mermaid h3 with dissolve
    pause
    show mermaid h4 with dissolve
    pause
    return


label g_mermaid_cg:
    scene
    show bg 125

    if renpy.seen_label("g_mermaid_ha"):
        show g_mermaid ha1
        with dissolve
        pause
        show g_mermaid ha2 with dissolve
        pause
        show g_mermaid ha3 with dissolve
        pause

        if not renpy.seen_label("g_mermaid_hb"):
            return

    show g_mermaid hb1 with dissolve
    pause
    show g_mermaid ct01 at topleft zorder 15 as ct with dissolve
    pause
    show g_mermaid ct02 at topleft zorder 15 as ct
    show g_mermaid hb2
    with dissolve
    pause
    show g_mermaid ct03 at topleft zorder 15 as ct
    show g_mermaid hb3
    with dissolve
    pause
    show g_mermaid ct04 at topleft zorder 15 as ct
    show g_mermaid hb4
    with dissolve
    pause
    return


label ningyohime_cg:
    scene
    show bg 050
    show ningyohime h1
    with dissolve
    pause
    show ningyohime h2 with dissolve
    pause
    show ningyohime h3 with dissolve
    pause
    show ningyohime h4 with dissolve
    pause
    show ningyohime h5 with dissolve
    pause
    return


label queenmermaid_cg:
    scene
    show bg 050
    show queenmermaid ha1
    with dissolve
    pause
    show queenmermaid ct01 at topleft zorder 15 as ct with dissolve
    pause
    show queenmermaid ct02 at topleft zorder 15 as ct
    show queenmermaid ha2
    with dissolve
    pause
    show queenmermaid ct03 at topleft zorder 15 as ct
    show queenmermaid ha3
    with dissolve
    pause

    if not renpy.seen_label("queenmermaid_ha"):
        return

    show bg 039
    show queenmermaid hb1
    show queenmermaid ct11 as ct
    with dissolve
    pause
    show queenmermaid ct12 as ct
    show queenmermaid hb2
    with dissolve
    pause
    show queenmermaid ct13 as ct
    show queenmermaid hb3
    with dissolve
    pause
    show queenmermaid ct14 as ct
    show queenmermaid hb4
    with dissolve
    pause
    return


label xx_7_cg:
    scene
    show bg 055
    show xx_7 h1
    with dissolve
    pause
    show xx_7 ct01 at topright zorder 15 as ct with dissolve
    pause
    show xx_7 ct02 at topright zorder 15 as ct with dissolve
    pause
    show xx_7 ct03 at topright zorder 15 as ct with dissolve
    pause
    show xx_7 ct04 at topright zorder 15 as ct with dissolve
    pause
    show xx_7 h2 with dissolve
    pause
    show xx_7 h3 with dissolve
    pause
    show xx_7 h4 with dissolve
    pause
    return


label shadow_cg:
    scene
    show bg 056
    show shadow st11
    with dissolve
    pause
    show shadow st12 with dissolve
    pause
    show shadow h1 with dissolve
    pause
    show shadow h2 with dissolve
    pause
    show shadow h3 with dissolve
    pause
    show shadow h4 with dissolve
    pause
    show shadow h5 with dissolve
    pause
    return


label gaistvine_cg:
    scene
    show bg 057
    show gaistvine st21
    with dissolve
    pause
    show gaistvine st22 with dissolve
    pause
    show gaistvine st23 with dissolve
    pause
    return


label chrom2_cg:
    scene
    show bg 058
    show chrom hc01
    with dissolve
    pause
    show chrom hc02 with dissolve
    pause
    show chrom hc03 with dissolve
    pause
    show chrom hc04 with dissolve
    pause
    show chrom hc05 with dissolve
    pause
    show chrom hc06 with dissolve
    pause
    show chrom hc07 with dissolve
    pause
    show chrom hc08 with dissolve
    pause
    show chrom hc09 with dissolve
    pause
    show chrom hc10 with dissolve
    pause
    show chrom hc11 with dissolve
    pause
    show chrom hc12 with dissolve
    pause
    hide chrom
    show chromg h1
    with dissolve
    pause
    show chromg h2 with dissolve
    pause
    show chromg h3 with dissolve
    pause
    show chromg h4 with dissolve
    pause
    show chromg h5 with dissolve
    pause
    show chromg h6 with dissolve
    pause
    show chromg h7 with dissolve
    pause
    show chromg h8 with dissolve
    pause
    show chromg h9 with dissolve
    pause
    return


label berryel_cg:
    scene
    show bg 061
    show berryel st11
    with dissolve
    pause
    show berryel st12 with dissolve
    pause
    show berryel st13 with dissolve
    pause
    show berryel st14 with dissolve
    pause
    show berryel st15 with dissolve
    pause
    show berryel st21 with dissolve
    pause
    show berryel st22 with dissolve
    pause
    show berryel st23 with dissolve
    pause
    show berryel st31 with dissolve
    pause
    show berryel st32 with dissolve
    pause
    show berryel st33 with dissolve
    pause
    show berryel st34 with dissolve
    pause
    return


label revel_cg:
    scene
    show bg 061
    show revel ct11 at topleft zorder 15 as ct
    show revel h1
    with dissolve
    pause
    show revel ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show revel ct13 at topleft zorder 15 as ct
    show revel h2
    with dissolve
    pause
    show revel ct14 at topleft zorder 15 as ct
    show revel h3
    with dissolve
    pause
    show revel ct15 at topleft zorder 15 as ct with dissolve
    pause
    show revel ct16 at topleft zorder 15 as ct with dissolve
    pause
    show revel ct17 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show revel st12
    with dissolve
    pause
    show revel st13 with dissolve
    pause
    show revel ct01 at topright zorder 15 as ct
    show revel st01
    with dissolve
    pause
    show revel ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show revel ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show revel st14
    with dissolve
    pause
    return


label gnome2_cg:
    scene
    show bg 061
    show gnome hb1
    with dissolve
    pause
    show gnome hb2 with dissolve
    pause
    show gnome hb3 with dissolve
    pause
    show gnome hb4 with dissolve
    pause
    return


label c_chariot_cg:
    scene
    show bg 062
    show c_chariot h1
    with dissolve
    pause
    show c_chariot h2 with dissolve
    pause
    return


label carmilla_cg:
    scene
    show bg 223
    show carmilla ha1
    with dissolve
    pause
    show carmilla ha2 with dissolve
    pause
    show carmilla ha3 with dissolve
    pause
    show carmilla ha4 with dissolve
    pause
    show carmilla ha5 with dissolve
    pause
    show carmilla ha6 with dissolve
    pause
    show carmilla ha7 with dissolve
    pause
    show carmilla ha8 with dissolve
    pause
    show carmilla ha9 with dissolve
    pause
    show carmilla hb1 with dissolve
    pause
    show carmilla hb2 with dissolve
    pause
    show carmilla hb3 with dissolve
    pause
    show carmilla hb4 with dissolve
    pause
    show carmilla hb5 with dissolve
    pause
    show carmilla hb6 with dissolve
    pause
    show carmilla hb7 with dissolve
    pause
    show carmilla hb8 with dissolve
    pause
    show carmilla hb9 with dissolve
    pause
    return


label elisabeth_cg:
    scene
    show bg 223
    show elisabeth st02
    show elisabeth ct01 at left zorder 15 as ct
    with dissolve
    pause
    show elisabeth ct02 at left zorder 15 as ct with dissolve
    pause
    hide ct
    show elisabeth ha1
    with dissolve
    pause
    show elisabeth ha2 with dissolve
    pause
    show elisabeth ha3 with dissolve
    pause
    show elisabeth ha4 with dissolve
    pause
    show elisabeth ha5 with dissolve
    pause
    show elisabeth ha6 with dissolve
    pause
    show elisabeth ha7 with dissolve
    pause
    show elisabeth hb1 with dissolve
    pause
    show elisabeth hb2 with dissolve
    pause
    show elisabeth hb3 with dissolve
    pause
    show elisabeth hb4 with dissolve
    pause
    show elisabeth hb5 with dissolve
    pause
    show elisabeth hb6 with dissolve
    pause
    return


label queenvanpire_cg:
    scene
    show bg 224
    show queenvanpire ha1
    with dissolve
    pause
    show queenvanpire ha2 with dissolve
    pause
    show queenvanpire ha3 with dissolve
    pause
    show queenvanpire ha4 with dissolve
    pause
    show queenvanpire ha5 with dissolve
    pause
    show queenvanpire hb1 with dissolve
    pause
    show queenvanpire hb2 with dissolve
    pause
    show queenvanpire hb3 with dissolve
    pause
    show queenvanpire hb4 with dissolve
    pause
    show queenvanpire hb5 with dissolve
    pause
    show queenvanpire hb6 with dissolve
    pause
    show queenvanpire hb7 with dissolve
    pause
    show queenvanpire hc1 with dissolve
    pause
    show queenvanpire hc2 with dissolve
    pause
    show queenvanpire hc3 with dissolve
    pause
    show queenvanpire hc4 with dissolve
    pause
    show queenvanpire hc5 with dissolve
    pause
    show queenvanpire hc6 with dissolve
    pause
    show queenvanpire hc7 with dissolve
    pause
    show queenvanpire hc8 with dissolve
    pause
    return


label c_homunculus_cg:
    scene
    show bg 072
    show c_homunculus h1
    with dissolve
    pause
    show c_homunculus h2 with dissolve
    pause
    show c_homunculus h3 with dissolve
    pause
    show c_homunculus h4 with dissolve
    pause
    show c_homunculus h5 with dissolve
    pause
    show c_homunculus h6 with dissolve
    pause
    return


label ironmaiden_k_cg:
    scene
    show bg 072
    show ironmaiden_k st11
    with dissolve
    pause
    show ironmaiden_k st12 with dissolve
    pause
    show ironmaiden_k st13 with dissolve
    pause
    show ironmaiden_k st14 with dissolve
    pause
    show ironmaiden_k h1 with dissolve
    pause
    show ironmaiden_k h2 with dissolve
    pause
    show ironmaiden_k h3 with dissolve
    pause
    show ironmaiden_k h4 with dissolve
    pause
    show ironmaiden_k h5 with dissolve
    pause
    show ironmaiden_k h6 with dissolve
    pause
    return


label lusia_cg:
    scene
    show bg 075
    show lusia ha1
    with dissolve
    pause
    show lusia ha2 with dissolve
    pause
    show lusia ha3 with dissolve
    pause
    show lusia ha4 with dissolve
    pause
    show lusia ha5 with dissolve
    pause
    show lusia hb1 with dissolve
    pause
    show lusia ct01 at right zorder 15 as ct with dissolve
    pause
    show lusia ct02 at right zorder 15 as ct
    show lusia hb2
    with dissolve
    pause
    show lusia ct03 at right zorder 15 as ct with dissolve
    pause
    show lusia hb3 with dissolve
    pause
    show lusia ct04 at right zorder 15 as ct
    show lusia hb4
    with dissolve
    pause
    return


label silkiel_cg:
    scene
    show bg 076
    show silkiel st21
    with dissolve
    pause
    show silkiel st22 with dissolve
    pause
    show silkiel st23 with dissolve
    pause
    show silkiel st24 with dissolve
    pause
    show silkiel st25 with dissolve
    pause
    show silkiel st26 with dissolve
    pause
    show silkiel st27 with dissolve
    pause
    show silkiel st31 with dissolve
    pause
    show silkiel st32 with dissolve
    pause
    show silkiel st33 with dissolve
    pause
    show silkiel st34 with dissolve
    pause
    show silkiel st35 with dissolve
    pause
    show silkiel st36 with dissolve
    pause
    return


label endiel_cg:
    scene
    show bg 076
    show endiel st11
    show endiel ct01 at right zorder 15 as ct
    with dissolve
    pause
    show endiel ct02 at right zorder 15 as ct with dissolve
    pause
    show endiel ct03 at right zorder 15 as ct with dissolve
    pause
    show endiel ct04 at right zorder 15 as ct with dissolve
    pause
    hide ct
    show endiel st20
    with dissolve
    pause
    show endiel st21 with dissolve
    pause
    show endiel st22 with dissolve
    pause
    show endiel st23 with dissolve
    pause
    show endiel st24 with dissolve
    pause
    show endiel st25 with dissolve
    pause
    show endiel h1 with dissolve
    pause
    show endiel h2 with dissolve
    pause
    show endiel h3 with dissolve
    pause
    show endiel h4 with dissolve
    pause
    show endiel h5 with dissolve
    pause
    show endiel h6 with dissolve
    pause
    show endiel h7 with dissolve
    pause
    return


label sylph2_cg:
    scene
    show bg 076
    show sylph hb1
    with dissolve
    pause
    show sylph hb2 with dissolve
    pause
    show sylph hb3 with dissolve
    pause
    show sylph hb4 with dissolve
    pause
    return


label typhon_cg:
    scene
    show bg 065
    show typhon h1
    with dissolve
    pause
    show typhon h2 with dissolve
    pause
    show typhon h3 with dissolve
    pause
    return


label lamianloid_cg:
    scene
    show bg 190
    show lamianloid st02
    show lamianloid ct41 zorder 15 as ct
    with dissolve
    pause
    show lamianloid ct01 zorder 15 as ct with dissolve
    pause
    show lamianloid ct02 zorder 15 as ct with dissolve
    pause
    show lamianloid ct11 zorder 15 as ct with dissolve
    pause
    show lamianloid ct12 zorder 15 as ct with dissolve
    pause
    show lamianloid ct21 zorder 15 as ct with dissolve
    pause
    show lamianloid ct22 zorder 15 as ct with dissolve
    pause
    show lamianloid ct31 zorder 15 as ct with dissolve
    pause
    show lamianloid ct32 zorder 15 as ct with dissolve
    pause

    if renpy.seen_label("lamianloid_ha"):
        hide ct
        show lamianloid ha01
        with dissolve
        pause
        show lamianloid ha02 with dissolve
        pause
        show lamianloid ha03 with dissolve
        pause
        show lamianloid ha04 with dissolve
        pause
        show lamianloid ha05 with dissolve
        pause
        show lamianloid ha06 with dissolve
        pause
        show lamianloid ha07 with dissolve
        pause
        show lamianloid ha08 with dissolve
        pause
        show lamianloid ha09 with dissolve
        pause
        show lamianloid ha10 with dissolve
        pause
        show lamianloid ha11 with dissolve
        pause
        show lamianloid ha12 with dissolve
        pause
        show lamianloid ha13 with dissolve
        pause
        show lamianloid ha14 with dissolve
        pause
        show lamianloid ha15 with dissolve
        pause
        show lamianloid ha16 with dissolve
        pause
        show lamianloid ha17 with dissolve
        pause
        show lamianloid ha18 with dissolve
        pause
        show lamianloid ha19 with dissolve
        pause
        show lamianloid ha20 with dissolve
        pause
        show lamianloid ha21 with dissolve
        pause
        show lamianloid ha22 with dissolve
        pause
        show lamianloid ha23 with dissolve
        pause
        show lamianloid ha24 with dissolve
        pause
        show lamianloid ha25 with dissolve
        pause
        show lamianloid ha26 with dissolve
        pause
        show lamianloid ha27 with dissolve
        pause
        show lamianloid ha28 with dissolve
        pause
        show lamianloid ha29 with dissolve
        pause
        show lamianloid ha30 with dissolve
        pause
        show lamianloid ha31 with dissolve
        pause
        show lamianloid ha32 with dissolve
        pause
        show lamianloid ha33 with dissolve
        pause
        show lamianloid ha34 with dissolve
        pause
        show lamianloid ha35 with dissolve
        pause
        show lamianloid ha36 with dissolve
        pause
        show lamianloid ha37 with dissolve
        pause
        show lamianloid ha38 with dissolve
        pause
        show lamianloid ha39 with dissolve
        pause

        if not renpy.seen_label("lamianloid_hb"):
            return

    show lamianloid hb01 with dissolve
    pause
    show lamianloid hb02 with dissolve
    pause
    show lamianloid hb03 with dissolve
    pause
    show lamianloid hb04 with dissolve
    pause
    show lamianloid hb05 with dissolve
    pause
    show lamianloid hb06 with dissolve
    pause
    show lamianloid hb07 with dissolve
    pause
    show lamianloid hb08 with dissolve
    pause
    show lamianloid hb09 with dissolve
    pause
    show lamianloid hb10 with dissolve
    pause
    show lamianloid hb11 with dissolve
    pause
    show lamianloid hb12 with dissolve
    pause
    show lamianloid hb13 with dissolve
    pause
    show lamianloid hb14 with dissolve
    pause
    show lamianloid hb15 with dissolve
    pause
    show lamianloid hb16 with dissolve
    pause
    show lamianloid hb17 with dissolve
    pause
    show lamianloid hb18 with dissolve
    pause
    show lamianloid hb19 with dissolve
    pause
    show lamianloid hb20 with dissolve
    pause
    show lamianloid hb21 with dissolve
    pause
    show lamianloid hb22 with dissolve
    pause
    show lamianloid hb23 with dissolve
    pause
    show lamianloid hb24 with dissolve
    pause
    show lamianloid hb25 with dissolve
    pause
    show lamianloid hb26 with dissolve
    pause
    show lamianloid hb27 with dissolve
    pause
    show lamianloid hb28 with dissolve
    pause
    show lamianloid hb29 with dissolve
    pause
    show lamianloid hb30 with dissolve
    pause
    show lamianloid hb31 with dissolve
    pause
    show lamianloid hb32 with dissolve
    pause
    show lamianloid hb33 with dissolve
    pause
    show lamianloid hb34 with dissolve
    pause
    show lamianloid hb35 with dissolve
    pause
    return


label knightloid_cg:
    scene
    show bg 225
    show knightloid st01
    show knightloid ct01 zorder 15 as ct
    with dissolve
    pause
    show knightloid ct31 zorder 15 as ct with dissolve
    pause
    show knightloid ct11 zorder 15 as ct with dissolve
    pause
    show knightloid ct12 zorder 15 as ct with dissolve
    pause
    show knightloid ct13 zorder 15 as ct with dissolve
    pause
    show knightloid ct21 zorder 15 as ct with dissolve
    pause
    show knightloid ct22 zorder 15 as ct with dissolve
    pause
    show knightloid ct23 zorder 15 as ct with dissolve
    pause
    hide ct
    show knightloid ha01 with dissolve
    pause
    show knightloid ha04 with dissolve
    pause
    show knightloid ha19 with dissolve
    pause
    show knightloid ha21 with dissolve
    pause
    show knightloid ha22 with dissolve
    pause
    show knightloid hb01 with dissolve
    pause
    show knightloid hb02 with dissolve
    pause
    show knightloid hb03 with dissolve
    pause
    show knightloid hb04 with dissolve
    pause
    show knightloid hb05 with dissolve
    pause
    show knightloid hb06 with dissolve
    pause
    show knightloid hb07 with dissolve
    pause
    show knightloid hb08 with dissolve
    pause
    show knightloid hb09 with dissolve
    pause
    show knightloid hb10 with dissolve
    pause
    show knightloid hb11 with dissolve
    pause
    show knightloid hb12 with dissolve
    pause
    show knightloid hb13 with dissolve
    pause
    show knightloid hb14 with dissolve
    pause
    show knightloid hb15 with dissolve
    pause
    show knightloid hb16 with dissolve
    pause
    show knightloid hb17 with dissolve
    pause
    show knightloid hb18 with dissolve
    pause
    show knightloid hb19 with dissolve
    pause
    show knightloid hb20 with dissolve
    pause
    show knightloid hb21 with dissolve
    pause
    show knightloid hb22 with dissolve
    pause
    show knightloid hb23 with dissolve
    pause
    show knightloid hb24 with dissolve
    pause
    show knightloid hb25 with dissolve
    pause
    show knightloid hb26 with dissolve
    pause
    show knightloid hb27 with dissolve
    pause
    show knightloid hb28 with dissolve
    pause
    return


label akaname_cg:
    scene
    show bg 088
    show akaname st02 at xy(X=100)
    show akaname ct01 zorder 15 as ct
    with dissolve
    pause
    show akaname ct02 zorder 15 as ct with dissolve
    pause
    show akaname ct11 zorder 15 as ct with dissolve
    pause
    show akaname ct12 zorder 15 as ct with dissolve
    pause
    show akaname ct21 zorder 15 as ct with dissolve
    pause
    show akaname ct22 zorder 15 as ct with dissolve
    pause
    show akaname ct31 zorder 15 as ct with dissolve
    pause
    show akaname ct32 zorder 15 as ct with dissolve
    pause
    show akaname ct41 zorder 15 as ct with dissolve
    pause
    hide ct
    show akaname h1
    with dissolve
    pause
    show akaname h2 with dissolve
    pause
    show akaname h3 with dissolve
    pause
    show akaname h4 with dissolve
    pause
    show akaname h5 with dissolve
    pause
    show akaname h6 with dissolve
    pause
    show akaname h7 with dissolve
    pause
    show akaname h8 with dissolve
    pause
    show akaname h9 with dissolve
    pause
    return


label mikolamia_cg:
    scene
    show bg 191
    show mikolamia ha1
    with dissolve
    pause
    show mikolamia ha2 with dissolve
    pause
    show mikolamia ha3 with dissolve
    pause
    show mikolamia ha4 with dissolve
    pause
    show mikolamia hb1 with dissolve
    pause
    show mikolamia hb2 with dissolve
    pause
    show mikolamia hb3 with dissolve
    pause
    show mikolamia hb4 with dissolve
    pause
    return


label kezyorou_cg:
    scene
    show bg 159
    show kezyorou ha1
    with dissolve
    pause
    show kezyorou ha2 with dissolve
    pause
    show kezyorou ha3 with dissolve
    pause
    show kezyorou ha4 with dissolve
    pause
    show kezyorou ha5 with dissolve
    pause
    show kezyorou ha6 with dissolve
    pause
    show kezyorou ha7 with dissolve
    pause
    show kezyorou hb1 with dissolve
    pause
    show kezyorou hb2 with dissolve
    pause
    show kezyorou hb3 with dissolve
    pause
    show kezyorou hb4 with dissolve
    pause
    show kezyorou hb5 with dissolve
    pause
    show kezyorou hb6 with dissolve
    pause
    return


label sirohebisama_cg:
    scene
    show bg 090
    show sirohebisama ha1
    with dissolve
    pause
    show sirohebisama ha1s with dissolve
    pause
    show sirohebisama ha2 with dissolve
    pause
    show sirohebisama ha3 with dissolve
    pause
    show sirohebisama ha4 with dissolve
    pause
    show sirohebisama ha5 with dissolve
    pause
    show sirohebisama ha6 with dissolve
    pause
    show sirohebisama ha7 with dissolve
    pause
    show sirohebisama ha8 with dissolve
    pause
    show sirohebisama ha9 with dissolve
    pause
    show sirohebisama ct01 at topright zorder 15 as ct
    show sirohebisama hb1
    with dissolve
    pause
    show sirohebisama ct02 at topright zorder 15 as ct
    show sirohebisama hb2
    with dissolve
    pause
    show sirohebisama ct03 at topright zorder 15 as ct
    show sirohebisama hb3
    with dissolve
    pause
    show sirohebisama ct04 at topright zorder 15 as ct
    show sirohebisama hb4
    with dissolve
    pause
    show sirohebisama ct05 at topright zorder 15 as ct
    show sirohebisama hb5
    with dissolve
    pause
    show sirohebisama ct06 at topright zorder 15 as ct
    show sirohebisama hb6
    with dissolve
    pause
    show sirohebisama ct07 at topright zorder 15 as ct
    show sirohebisama hb7
    with dissolve
    pause
    show sirohebisama ct08 at topright zorder 15 as ct
    show sirohebisama hb8
    with dissolve
    pause
    show sirohebisama ct09 at topright zorder 15 as ct
    show sirohebisama hb9
    with dissolve
    pause
    return


label walraune_cg:
    scene
    show bg 093
    show walraune h1
    with dissolve
    pause
    show walraune h2 with dissolve
    pause
    show walraune h3 with dissolve
    pause
    show walraune h4 with dissolve
    pause
    return


label dryad_cg:
    scene
    show bg 094

    if renpy.seen_label("dryad_ha"):
        show dryad ha1
        with dissolve
        pause
        show dryad ha2 with dissolve
        pause
        show dryad ha3 with dissolve
        pause
        show dryad ha4 with dissolve
        pause
        show dryad ha5 with dissolve
        pause
        show dryad ha6 with dissolve
        pause

        if not renpy.seen_label("dryad_hb"):
            return

    show dryad hb1
    with dissolve
    pause
    show dryad hb2 with dissolve
    pause
    show dryad hb3 with dissolve
    pause
    show dryad hb4 with dissolve
    pause
    show dryad hb5 with dissolve
    pause
    show dryad hb6 with dissolve
    pause
    show dryad hb7 with dissolve
    pause
    show dryad hb8 with dissolve
    pause
    show dryad hc1 with dissolve
    pause
    show dryad hc2 with dissolve
    pause
    show dryad hc3 with dissolve
    pause
    show dryad hc4 with dissolve
    pause
    show dryad hc5 with dissolve
    pause
    show dryad hc6 with dissolve
    pause
    show dryad hc7 with dissolve
    pause
    return


label queenalraune_cg:
    scene
    show bg 095
    show queenalraune h1
    with dissolve
    pause
    show queenalraune h2 with dissolve
    pause
    show queenalraune h3 with dissolve
    pause
    show queenalraune h4 with dissolve
    pause
    show queenalraune h5 with dissolve
    pause
    show queenalraune h6 with dissolve
    pause
    show queenalraune h7 with dissolve
    pause
    show queenalraune h8 with dissolve
    pause
    return


label slimelord_cg:
    scene
    show bg 097
    show slimelord hb1
    with dissolve
    pause
    show slimelord hb2 with dissolve
    pause
    show slimelord hb3 with dissolve
    pause
    show slimelord hb4
    show slimelord ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show slimelord hb5
    show slimelord ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show slimelord hb6
    show slimelord ct03 at topright zorder 15 as ct
    with dissolve
    pause
    hide ct
    show slimelord ha1
    with dissolve
    pause
    show slimelord ha2 with dissolve
    pause
    show slimelord ha3 with dissolve
    pause
    return


label eggel_cg:
    scene
    show bg 097
    show eggel ha1
    with dissolve
    pause
    show eggel ha2 with dissolve
    pause
    show eggel ha3 with dissolve
    pause
    show eggel ha4 with dissolve
    pause
    show eggel ha5 with dissolve
    pause
    show eggel hb1 with dissolve
    pause
    show eggel hb2 with dissolve
    pause
    show eggel hb3 with dissolve
    pause
    show eggel hb4 with dissolve
    pause
    show eggel hb5 with dissolve
    pause
    show eggel hb6 with dissolve
    pause
    return


label salamander2_cg:
    scene
    show bg 097
    show salamander hb1
    with dissolve
    pause
    show salamander hb2 with dissolve
    pause
    show salamander hb3 with dissolve
    pause
    show salamander hb4 with dissolve
    pause
    return


label tutigumo_cg:
    scene
    show bg 092
    show tutigumo st02
    show tutigumo ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show tutigumo ct02 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct03 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct04 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct11 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct12 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct13 at topright zorder 15 as ct with dissolve
    pause
    show tutigumo ct21 zorder 15 as ct with dissolve
    pause
    show tutigumo ct22 zorder 15 as ct with dissolve
    pause
    show tutigumo ct23 zorder 15 as ct with dissolve
    pause
    show tutigumo ct24 zorder 15 as ct with dissolve
    pause
    show tutigumo ct25 zorder 15 as ct with dissolve
    pause
    hide ct
    show tutigumo h1
    with dissolve
    pause
    show tutigumo h2 with dissolve
    pause
    show tutigumo h3 with dissolve
    pause
    show tutigumo h4 with dissolve
    pause
    show tutigumo h5 with dissolve
    pause
    show tutigumo h6 with dissolve
    pause
    show tutigumo h7 with dissolve
    pause
    show tutigumo h8 with dissolve
    pause
    show tutigumo h9 with dissolve
    pause
    return


label alakneload_cg:
    scene
    show bg 092
    show alakneload ha1
    with dissolve
    pause
    show alakneload ha2 with dissolve
    pause
    show alakneload ha3 with dissolve
    pause
    show alakneload ha4 with dissolve
    pause
    show alakneload ha5 with dissolve
    pause
    show alakneload ha6 with dissolve
    pause
    show alakneload ha7 with dissolve
    pause
    show alakneload ha8 with dissolve
    pause

    if renpy.seen_label("alakneload_ha"):
        show alakneload hc1
        with dissolve
        pause
        show alakneload ct01 at topleft zorder 15 as ct with dissolve
        pause
        show alakneload ct02 at topleft zorder 15 as ct with dissolve
        pause
        show alakneload ct03 at topleft zorder 15 as ct with dissolve
        pause
        show alakneload ct04 at topleft zorder 15 as ct
        show alakneload hc2
        with dissolve
        pause
        show alakneload hc3 with dissolve
        pause

        if not renpy.seen_label("alakneload_hb"):
            return

    hide ct
    show alakneload hb1
    with dissolve
    pause
    show alakneload hb2 with dissolve
    pause
    show alakneload hb3 with dissolve
    pause
    show alakneload hb4 with dissolve
    pause
    show alakneload hb5 with dissolve
    pause
    return


label kumonomiko_cg:
    scene
    show bg 092
    show kumonomiko ha1
    with dissolve
    pause
    show kumonomiko ha2 with dissolve
    pause
    show kumonomiko ha3 with dissolve
    pause
    show kumonomiko ha4 with dissolve
    pause
    show kumonomiko ha5 with dissolve
    pause
    show kumonomiko ha6 with dissolve
    pause
    show kumonomiko ha7 with dissolve
    pause
    show kumonomiko ha8 with dissolve
    pause
    show kumonomiko hb01 with dissolve
    pause
    show kumonomiko ct01 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumonomiko ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show kumonomiko ct03 at topleft zorder 15 as ct
    show kumonomiko hb02
    with dissolve
    pause
    show kumonomiko ct04 at topleft zorder 15 as ct
    show kumonomiko hb03
    with dissolve
    pause
    show kumonomiko ct05 at topleft zorder 15 as ct
    show kumonomiko hb04
    with dissolve
    pause
    show kumonomiko ct06 at topleft zorder 15 as ct
    show kumonomiko hb05
    with dissolve
    pause
    show kumonomiko ct07 at topleft zorder 15 as ct
    show kumonomiko hb06
    with dissolve
    pause
    show kumonomiko ct08 at topleft zorder 15 as ct
    show kumonomiko hb07
    with dissolve
    pause
    hide ct
    show kumonomiko hb08
    with dissolve
    pause
    show kumonomiko hb09 with dissolve
    pause
    show kumonomiko hb10 with dissolve
    pause
    return


label narcubus_cg:
    scene
    show bg 103
    show narcubus hb1 with dissolve
    pause
    show narcubus hb2 with dissolve
    pause
    show narcubus hb3 with dissolve
    pause
    show narcubus hb4 with dissolve
    pause
    show narcubus hc1 with dissolve
    pause
    show narcubus hc2 with dissolve
    pause
    show narcubus hc3 with dissolve
    pause
    show narcubus hc4 with dissolve
    pause
    show narcubus hc5 with dissolve
    pause
    show narcubus hc6 with dissolve
    pause
    show narcubus ha1 with dissolve
    pause
    show narcubus ha2 with dissolve
    pause
    show narcubus ha3 with dissolve
    pause
    show narcubus hd1 with dissolve
    pause
    show narcubus hd2
    show narcubus bk07
    with dissolve
    pause
    return


label inps_cg:
    scene
    show bg 103
    show inp_c st22 at xy(X=280)
    show inp_b st12 at xy(X=-250)
    show inp_a st02
    show inp ct21 zorder 15 as ct
    with dissolve
    pause
    show inp ct22 zorder 15 as ct with dissolve
    pause
    hide inp_c
    show inp ct11 zorder 15 as ct
    with dissolve
    pause
    show inp ct12 zorder 15 as ct with dissolve
    pause
    hide inp_b
    show inp ct01 zorder 15 as ct
    with dissolve
    pause
    show inp ct02 zorder 15 as ct with dissolve
    pause
    hide ct

    if renpy.seen_label("inps_ha"):
        show inp_a he1
        with dissolve
        pause
        show inp_a he2 with dissolve
        pause
        show inp_a he3 with dissolve
        pause
        show inp_a he4 with dissolve
        pause
        show inp_a he5 with dissolve
        pause

        if not renpy.seen_label("inps_hb"):
            return

    show inp hf1
    with dissolve
    pause
    show inp hf2 with dissolve
    pause
    show inp hf3 with dissolve
    pause
    show inp hf4 with dissolve
    pause
    return


label eva_cg:
    scene
    show bg 106
    show eva st02
    show eva ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show eva ct02 at topright zorder 15 as ct with dissolve
    pause
    show eva ct03 at topright zorder 15 as ct with dissolve
    pause
    show eva ct04 at topright zorder 15 as ct with dissolve
    pause
    show eva ct11 as ct with dissolve
    pause
    show eva ct12 as ct with dissolve
    pause
    show eva ct13 as ct with dissolve
    pause
    show eva ct14 as ct with dissolve
    pause
    show eva ct21 at topright zorder 15 as ct with dissolve
    pause
    show eva ct22 at topright zorder 15 as ct with dissolve
    pause
    show eva ct23 at topright zorder 15 as ct with dissolve
    pause
    show eva ct24 at topright zorder 15 as ct with dissolve
    pause
    show eva ct31 at topright zorder 15 as ct with dissolve
    pause
    show eva ct32 at topright zorder 15 as ct with dissolve
    pause
    show eva ct33 at topright zorder 15 as ct with dissolve
    pause
    show eva ct34 at topright zorder 15 as ct with dissolve
    pause
    hide ct
    show eva ha1
    with dissolve
    pause
    show eva ha2 with dissolve
    pause
    show eva ha3 with dissolve
    pause
    show eva ha4 with dissolve
    pause
    show eva ha5 with dissolve
    pause
    show eva ha6 with dissolve
    pause
    show eva hb1 with dissolve
    pause
    show eva hb2 with dissolve
    pause
    show eva hb3 with dissolve
    pause
    show eva hb4 with dissolve
    pause
    show eva hb5 with dissolve
    pause
    show eva hb6 with dissolve
    pause
    return


label trooperloid_cg:
    scene
    show bg 122
    show trooperloid st01
    show trooperloid ct01 zorder 15 as ct
    with dissolve
    pause
    show trooperloid ct11 zorder 15 as ct with dissolve
    pause
    show trooperloid ct12 zorder 15 as ct with dissolve
    pause
    show trooperloid ct21 zorder 15 as ct with dissolve
    pause
    show trooperloid ct22 zorder 15 as ct with dissolve
    pause
    show trooperloid ct23 zorder 15 as ct with dissolve
    pause
    show trooperloid ct24 zorder 15 as ct with dissolve
    pause
    show trooperloid ct25 zorder 15 as ct with dissolve
    pause
    show trooperloid ct26 zorder 15 as ct with dissolve
    pause
    hide ct
    show trooperloid ha01
    with dissolve
    pause
    show trooperloid ha02 with dissolve
    pause
    show trooperloid ha03 with dissolve
    pause
    show trooperloid ha04 with dissolve
    pause
    show trooperloid ha05 with dissolve
    pause
    show trooperloid ha06 with dissolve
    pause
    show trooperloid ha07 with dissolve
    pause
    show trooperloid ha08 with dissolve
    pause
    show trooperloid ha09 with dissolve
    pause
    show trooperloid ha10 with dissolve
    pause
    show trooperloid ha11 with dissolve
    pause
    show trooperloid ha12 with dissolve
    pause
    show trooperloid ha13 with dissolve
    pause
    show trooperloid ha14 with dissolve
    pause
    show trooperloid ha15 with dissolve
    pause
    show trooperloid ha16 with dissolve
    pause
    show trooperloid ha17 with dissolve
    pause
    show trooperloid ha18 with dissolve
    pause
    show trooperloid ha19 with dissolve
    pause
    show trooperloid ha20 with dissolve
    pause
    show trooperloid ha21 with dissolve
    pause
    show trooperloid ha22 with dissolve
    pause
    show trooperloid ha23 with dissolve
    pause
    show trooperloid ha24 with dissolve
    pause
    show trooperloid ha25 with dissolve
    pause
    show trooperloid ha26 with dissolve
    pause
    show trooperloid ha27 with dissolve
    pause
    show trooperloid ha28 with dissolve
    pause
    show trooperloid ha29 with dissolve
    pause
    show trooperloid ha30 with dissolve
    pause
    show trooperloid ha31 with dissolve
    pause
    show trooperloid ha32 with dissolve
    pause
    show trooperloid ha33 with dissolve
    pause
    show trooperloid ha34 with dissolve
    pause
    show trooperloid ha35 with dissolve
    pause
    show trooperloid ha36 with dissolve
    pause
    show trooperloid ha37 with dissolve
    pause
    show trooperloid ha38 with dissolve
    pause
    show trooperloid ha39 with dissolve
    pause
    show trooperloid hb01 with dissolve
    pause
    show trooperloid hb02 with dissolve
    pause
    show trooperloid hb05 with dissolve
    pause
    return


label assassinloid_cg:
    scene
    show bg 122
    show assassinloid st02
    show assassinloid ct21 zorder 15 as ct
    with dissolve
    pause
    show assassinloid ct01 zorder 15 as ct with dissolve
    pause
    show assassinloid ct02 zorder 15 as ct with dissolve
    pause
    show assassinloid ct11 zorder 15 as ct with dissolve
    pause
    show assassinloid ct12 zorder 15 as ct with dissolve
    pause
    show assassinloid ct13 zorder 15 as ct with dissolve
    pause
    show assassinloid ct14 zorder 15 as ct with dissolve
    pause
    hide ct
    show assassinloid st24
    with dissolve
    pause
    show assassinloid st25 with dissolve
    pause
    show assassinloid st31 with dissolve
    pause
    show assassinloid st32 with dissolve
    pause
    show assassinloid hb01 with dissolve
    pause
    show assassinloid hb02 with dissolve
    pause
    show assassinloid hb03 with dissolve
    pause
    show assassinloid hb04 with dissolve
    pause
    show assassinloid hb05 with dissolve
    pause
    show assassinloid hb06 with dissolve
    pause
    show assassinloid hb07 with dissolve
    pause
    show assassinloid hb08 with dissolve
    pause
    show assassinloid hb09 with dissolve
    pause
    show assassinloid hb10 with dissolve
    pause
    show assassinloid hb11 with dissolve
    pause
    show assassinloid hb12 with dissolve
    pause
    show assassinloid hb13 with dissolve
    pause
    show assassinloid hb14 with dissolve
    pause
    show assassinloid hb15 with dissolve
    pause
    show assassinloid hb16 with dissolve
    pause
    show assassinloid hb17 with dissolve
    pause
    show assassinloid hb18 with dissolve
    pause
    show assassinloid hb19 with dissolve
    pause
    show assassinloid hb20 with dissolve
    pause
    show assassinloid hb21 with dissolve
    pause
    show assassinloid hb22 with dissolve
    pause
    show assassinloid hb23 with dissolve
    pause
    show assassinloid hb24 with dissolve
    pause
    show assassinloid hb25 with dissolve
    pause
    show assassinloid hb26 with dissolve
    pause
    show assassinloid hb27 with dissolve
    pause
    show assassinloid hb28 with dissolve
    pause
    show assassinloid hb29 with dissolve
    pause
    show assassinloid hb30 with dissolve
    pause
    show assassinloid hb31 with dissolve
    pause
    show assassinloid hb32 with dissolve
    pause
    show assassinloid hb33 with dissolve
    pause
    show assassinloid hb34 with dissolve
    pause
    show assassinloid hb35 with dissolve
    pause
    show assassinloid hb36 with dissolve
    pause
    show assassinloid hc21 with dissolve
    pause
    show assassinloid hc22 with dissolve
    pause
    show assassinloid hc27 with dissolve
    pause
    return


label yomotu_cg:
    scene
    show bg 115
    show yomotu h1
    with dissolve
    pause
    show yomotu h2 with dissolve
    pause
    show yomotu h3 with dissolve
    pause
    return


label wormiel_cg:
    scene
    show bg 115
    show wormiel hb1
    with dissolve
    pause
    show wormiel ct01 at topleft zorder 15 as ct
    show wormiel hb2
    with dissolve
    pause
    show wormiel ct02 at topleft zorder 15 as ct
    show wormiel hb3
    with dissolve
    pause
    show wormiel ct03 at topleft zorder 15 as ct
    show wormiel hb4
    with dissolve
    pause
    show wormiel ct04 at topleft zorder 15 as ct
    show wormiel hb5
    with dissolve
    pause
    show wormiel hb6 with dissolve
    pause
    show wormiel ct05 at topleft zorder 15 as ct
    show wormiel hb7
    with dissolve
    pause
    show wormiel ct06 at topleft zorder 15 as ct
    show wormiel hb8
    with dissolve
    pause
    hide ct
    show wormiel ha01
    with dissolve
    pause
    show wormiel ha02 with dissolve
    pause
    show wormiel ha03 with dissolve
    pause
    show wormiel ha04 with dissolve
    pause
    show wormiel ha05 with dissolve
    pause
    show wormiel ha06 with dissolve
    pause
    show wormiel ha07 with dissolve
    pause
    show wormiel ha08 with dissolve
    pause
    show wormiel ha09 with dissolve
    pause
    show wormiel ha10 with dissolve
    pause
    return


label undine2_cg:
    scene
    show bg 115
    show undine hb1
    with dissolve
    pause
    show undine ct11 at topleft zorder 15 as ct
    with dissolve
    pause
    show undine ct12 at topleft zorder 15 as ct
    show undine hb2
    with dissolve
    pause
    show undine hb3
    with dissolve
    pause
    show undine ct13 at topleft zorder 15 as ct
    show undine hb4
    with dissolve
    pause
    return


label alice4_cg:
    scene
    show bg 195
    show alice h10_1
    with dissolve
    pause
    show alice h10_2 with dissolve
    pause
    show alice h10_3 with dissolve
    pause
    show alice h10_4 with dissolve
    pause
    show alice h10_5 with dissolve
    pause
    show alice h10_6 with dissolve
    pause
    show alice h10_7 with dissolve
    pause
    show alice h10_8 with dissolve
    pause
    show alice h10_9 with dissolve
    pause
    return


label drainplant_cg:
    scene
    show bg 197
    show drainplant st11
    with dissolve
    pause
    show drainplant ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show drainplant ct02 at topright zorder 15 as ct
    show drainplant st12
    with dissolve
    pause
    show drainplant ct03 at topright zorder 15 as ct with dissolve
    pause
    show drainplant ct04 at topright zorder 15 as ct
    with dissolve
    pause
    show drainplant ct05 at topright zorder 15 as ct
    show drainplant st13
    with dissolve
    pause
    return


label drainloid_cg:
    scene
    show bg 199
    show drainloid st02
    show drainloid ct01 zorder 15 as ct
    with dissolve
    pause
    show drainloid ct02 zorder 15 as ct with dissolve
    pause
    show drainloid ct11 zorder 15 as ct with dissolve
    pause
    hide ct
    show drainloid st21
    with dissolve
    pause
    show drainloid st22 with dissolve
    pause
    show drainloid st31 with dissolve
    pause
    show drainloid st32 with dissolve
    pause
    show drainloid st11 with dissolve
    pause
    show drainloid st23 with dissolve
    pause
    show drainloid st24 with dissolve
    pause
    show drainloid st33 with dissolve
    pause
    show drainloid st34 with dissolve
    pause
    show drainloid ha01 with dissolve
    pause
    show drainloid ha02 with dissolve
    pause
    show drainloid ha03 with dissolve
    pause
    show drainloid ha04 with dissolve
    pause
    show drainloid ha05 with dissolve
    pause
    show drainloid ha06 with dissolve
    pause
    show drainloid ha07 with dissolve
    pause
    show drainloid ha08 with dissolve
    pause
    show drainloid ha09 with dissolve
    pause
    show drainloid ha10 with dissolve
    pause
    show drainloid ha11 with dissolve
    pause
    show drainloid ha12 with dissolve
    pause
    show drainloid ha13 with dissolve
    pause
    show drainloid ha14 with dissolve
    pause
    show drainloid ha15 with dissolve
    pause
    show drainloid ha16 with dissolve
    pause
    show drainloid ha17 with dissolve
    pause
    show drainloid ha18 with dissolve
    pause
    show drainloid ha19 with dissolve
    pause
    show drainloid ha20 with dissolve
    pause
    show drainloid ha21 with dissolve
    pause
    show drainloid ha22 with dissolve
    pause
    show drainloid ha23 with dissolve
    pause
    show drainloid ha24 with dissolve
    pause
    show drainloid ha25 with dissolve
    pause
    show drainloid ha26 with dissolve
    pause
    show drainloid ha27 with dissolve
    pause
    show drainloid ha28 with dissolve
    pause
    show drainloid ha29 with dissolve
    pause
    show drainloid ha30 with dissolve
    pause
    show drainloid ha31 with dissolve
    pause
    show drainloid ha32 with dissolve
    pause
    show drainloid ha33 with dissolve
    pause
    show drainloid ha34 with dissolve
    pause
    show drainloid ha35 with dissolve
    pause
    show drainloid ha36 with dissolve
    pause
    show drainloid ha37 with dissolve
    pause
    show drainloid ha38 with dissolve
    pause
    show drainloid ha39 with dissolve
    pause
    show drainloid ha40 with dissolve
    pause
    return


label replicant_cg:
    scene
    show bg 199
    show replicant h1
    with dissolve
    pause
    show replicant h2 with dissolve
    pause
    show replicant h3 with dissolve
    pause
    show replicant h4 with dissolve
    pause
    show replicant h5 with dissolve
    pause
    show replicant h6 with dissolve
    pause
    return


label laplace_cg:
    scene
    show bg 199
    show laplace st02
    show laplace ct01 zorder 15 as ct
    with dissolve
    pause
    show laplace ct02 zorder 15 as ct with dissolve
    pause
    show laplace ct11 zorder 15 as ct with dissolve
    pause
    show laplace ct12 zorder 15 as ct with dissolve
    pause
    show laplace ct21 zorder 15 as ct with dissolve
    pause
    show laplace ct22 zorder 15 as ct with dissolve
    pause
    show laplace ct31 zorder 15 as ct with dissolve
    pause
    hide ct
    show laplace ha11
    with dissolve
    pause
    show laplace ha12 with dissolve
    pause
    show laplace ha13 with dissolve
    pause
    show laplace ha14 with dissolve
    pause
    show laplace hb01 with dissolve
    pause
    show laplace hb02 with dissolve
    pause
    show laplace hb03 with dissolve
    pause
    show laplace hb04 with dissolve
    pause
    show laplace hb05 with dissolve
    pause
    show laplace hb06 with dissolve
    pause
    show laplace hb07 with dissolve
    pause
    show laplace hb08 with dissolve
    pause
    show laplace hb09 with dissolve
    pause
    show laplace hb10 with dissolve
    pause
    show laplace hb11 with dissolve
    pause
    show laplace hb12 with dissolve
    pause
    show laplace hb13 with dissolve
    pause
    show laplace hb14 with dissolve
    pause
    show laplace hb15 with dissolve
    pause
    show laplace hb16 with dissolve
    pause
    show laplace hb17 with dissolve
    pause
    show laplace hb18 with dissolve
    pause
    show laplace hb19 with dissolve
    pause
    show laplace hb20 with dissolve
    pause
    show laplace hb21 with dissolve
    pause
    show laplace hb22 with dissolve
    pause
    show laplace hb23 with dissolve
    pause
    show laplace hb24 with dissolve
    pause
    show laplace hb25 with dissolve
    pause
    show laplace hb26 with dissolve
    pause
    show laplace hb27 with dissolve
    pause
    return


label ad_5_cg:
    scene
    show bg 202
    show ad_5 h1
    with dissolve
    pause
    show ad_5 h2 with dissolve
    pause
    show ad_5 h3 with dissolve
    pause
    show ad_5 h4
    show ad_5 ct01 at topright zorder 15 as ct
    with dissolve
    pause
    show ad_5 h5
    show ad_5 ct02 at topright zorder 15 as ct
    with dissolve
    pause
    show ad_5 ct03 at topright zorder 15 as ct with dissolve
    pause
    show ad_5 ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label fermesara_cg:
    scene
    show bg 202
    show fermesara h1
    with dissolve
    pause
    show fermesara ct01 at right zorder 15 as ct with dissolve
    pause
    show fermesara ct02 at right zorder 15 as ct with dissolve
    pause
    show fermesara ct03 at right zorder 15 as ct with dissolve
    pause
    show fermesara ct11 at left zorder 15 as ct with dissolve
    pause
    show fermesara ct12 at left zorder 15 as ct
    show fermesara h2
    with dissolve
    pause
    show fermesara ct13 at left zorder 15 as ct
    show fermesara h3
    with dissolve
    pause
    return


label angelghoul_cg:
    scene
    show bg 204
    show angelghoul h1
    with dissolve
    pause
    show angelghoul ct01 at topright zorder 15 as ct with dissolve
    pause
    show angelghoul ct02 at topright zorder 15 as ct with dissolve
    pause
    show angelghoul ct03 at topright zorder 15 as ct with dissolve
    pause
    show angelghoul ct04 at topright zorder 15 as ct with dissolve
    pause
    return


label dragonzonbe_cg:
    scene
    show bg 205
    show dragonzonbe ha1
    with dissolve
    pause
    show dragonzonbe ha2 with dissolve
    pause
    show dragonzonbe ha3 with dissolve
    pause
    show dragonzonbe hb1 with dissolve
    pause
    show dragonzonbe hb2 with dissolve
    pause
    show dragonzonbe hb3 with dissolve
    pause
    show dragonzonbe hb4 with dissolve
    pause
    return


label cirque1_cg:
    scene
    show bg 206
    show cirque ct41 zorder 15 as ct
    with dissolve
    pause
    show cirque ct42 zorder 15 as ct with dissolve
    pause
    show cirque ct43 zorder 15 as ct with dissolve
    pause
    show cirque ct01 zorder 15 as ct with dissolve
    pause
    show cirque ct02 zorder 15 as ct with dissolve
    pause
    hide ct
    show cirque h01
    with dissolve
    pause
    show cirque h02 with dissolve
    pause
    show cirque h03 with dissolve
    pause
    show cirque h04 with dissolve
    pause
    show cirque h05 with dissolve
    pause
    show cirque h06 with dissolve
    pause
    show cirque h07 with dissolve
    pause
    show cirque h08 with dissolve
    pause
    show cirque h09 with dissolve
    pause
    show cirque h10 with dissolve
    pause
    show cirque h11 with dissolve
    pause
    show cirque h12 with dissolve
    pause
    show cirque h13 with dissolve
    pause
    show cirque h14 with dissolve
    pause
    show cirque h15 with dissolve
    pause
    show cirque h16 with dissolve
    pause
    show cirque h17 with dissolve
    pause
    show cirque h18 with dissolve
    pause
    return


label cirque2_cg:
    scene
    show bg 206
    show cirque ct51 zorder 15 as ct
    with dissolve
    pause
    show cirque ct52 zorder 15 as ct with dissolve
    pause
    show cirque ct11 zorder 15 as ct with dissolve
    pause
    show cirque ct12 zorder 15 as ct with dissolve
    pause
    hide ct
    show cirque h01
    with dissolve
    pause
    show cirque h02 with dissolve
    pause
    show cirque h03 with dissolve
    pause
    show cirque h04 with dissolve
    pause
    show cirque h05 with dissolve
    pause
    show cirque h06 with dissolve
    pause
    show cirque h07 with dissolve
    pause
    show cirque h08 with dissolve
    pause
    show cirque h09 with dissolve
    pause
    show cirque h10 with dissolve
    pause
    show cirque h11 with dissolve
    pause
    show cirque h12 with dissolve
    pause
    show cirque h13 with dissolve
    pause
    show cirque h14 with dissolve
    pause
    show cirque h15 with dissolve
    pause
    show cirque h16 with dissolve
    pause
    show cirque h17 with dissolve
    pause
    show cirque h18 with dissolve
    pause
    return


label cirque3_cg:
    scene
    show bg 206
    show cirque ct31 zorder 15 as ct
    with dissolve
    pause
    show cirque ct32 zorder 15 as ct with dissolve
    pause
    show cirque ct21 zorder 15 as ct with dissolve
    pause
    show cirque ct22 zorder 15 as ct with dissolve
    pause
    hide ct
    show cirque h01
    with dissolve
    pause
    show cirque h02 with dissolve
    pause
    show cirque h03 with dissolve
    pause
    show cirque h04 with dissolve
    pause
    show cirque h05 with dissolve
    pause
    show cirque h06 with dissolve
    pause
    show cirque h07 with dissolve
    pause
    show cirque h08 with dissolve
    pause
    show cirque h09 with dissolve
    pause
    show cirque h10 with dissolve
    pause
    show cirque h11 with dissolve
    pause
    show cirque h12 with dissolve
    pause
    show cirque h13 with dissolve
    pause
    show cirque h14 with dissolve
    pause
    show cirque h15 with dissolve
    pause
    show cirque h16 with dissolve
    pause
    show cirque h17 with dissolve
    pause
    show cirque h18 with dissolve
    pause
    return


label alice15th_cg:
    scene
    show bg 206
    show alice15th ha01
    with dissolve
    pause
    show alice15th ha02 with dissolve
    pause
    show alice15th ha03 with dissolve
    pause
    show alice15th ha04 with dissolve
    pause
    show alice15th ha05 with dissolve
    pause
    show alice15th ha06 with dissolve
    pause
    show alice15th ha07 with dissolve
    pause
    show alice15th ha08 with dissolve
    pause
    show alice15th ha09 with dissolve
    pause
    show alice15th ha10 with dissolve
    pause
    show alice15th hc0 with dissolve
    pause
    show alice15th hc1 with dissolve
    pause
    show alice15th hc2 with dissolve
    pause
    show alice15th hc3 with dissolve
    pause
    show alice15th hc4 with dissolve
    pause
    return


label shirom_cg:
    scene
    show bg 206
    show shirom h1
    with dissolve
    pause
    show shirom h2 with dissolve
    pause
    show shirom h3 with dissolve
    pause
    show shirom h4 with dissolve
    pause
    show shirom h5 with dissolve
    pause
    show shirom h6 with dissolve
    pause
    return


label alice8th1_cg:
    scene
    show bg 145

    if renpy.seen_label("alice8th1_ha"):
        show alice8th ha1
        with dissolve
        pause
        show alice8th ha2 with dissolve
        pause
        show alice8th ha3 with dissolve
        pause
        show alice8th ha4 with dissolve
        pause
        show alice8th ha5 with dissolve
        pause
        show alice8th ha6 with dissolve
        pause
        show alice8th ha7 with dissolve
        pause
        show alice8th ha8 with dissolve
        pause
        show alice8th ha9 with dissolve
        pause

        if not renpy.seen_label("alice8th1_hb"):
            return

    show alice8th hb1
    with dissolve
    pause
    show alice8th hb2 with dissolve
    pause
    show alice8th hb3 with dissolve
    pause
    show alice8th hb4 with dissolve
    pause
    show alice8th hb5 with dissolve
    pause
    show alice8th hb6 with dissolve
    pause
    return


label traptemis_cg:
    scene
    show bg 207
    show traptemis st02
    with dissolve
    pause
    show traptemis st03 with dissolve
    pause
    show traptemis st04 with dissolve
    pause
    return


label gargoyle_cg:
    scene
    show bg 208
    show gargoyle st31
    with dissolve
    pause
    show gargoyle st32 with dissolve
    pause
    show gargoyle st33 with dissolve
    pause
    show gargoyle st34 with dissolve
    pause
    show gargoyle st35 as ct with dissolve
    pause
    hide ct
    show gargoyle h1
    with dissolve
    pause
    show gargoyle h2 with dissolve
    pause
    show gargoyle h3 with dissolve
    pause
    show gargoyle h4 with dissolve
    pause
    return


label doppele_cg:
    scene
    show bg 208
    show doppele ha1
    with dissolve
    pause
    show doppele ha2 with dissolve
    pause
    show doppele ha3 with dissolve
    pause
    show doppele hb1 with dissolve
    pause
    show doppele hb2 with dissolve
    pause
    show doppele hb3 with dissolve
    pause
    show doppele hc1 with dissolve
    pause
    show doppele hc2 with dissolve
    pause
    show doppele hc3 with dissolve
    pause
    show doppele hd1 with dissolve
    pause
    show doppele hd2 with dissolve
    pause
    show doppele hd3 with dissolve
    pause
    show doppele he1 with dissolve
    pause
    show doppele he2 with dissolve
    pause
    show doppele he3 with dissolve
    pause
    return


label hainu_cg:
    scene
    show bg 208
    show hainu h1
    with dissolve
    pause
    show hainu h2 with dissolve
    pause
    show hainu h3 with dissolve
    pause
    show hainu h4 with dissolve
    pause
    return


label amphis_cg:
    scene
    show bg 208
    show amphis h1
    with dissolve
    pause
    show amphis h2 with dissolve
    pause
    show amphis h3 with dissolve
    pause
    show amphis h4 with dissolve
    pause
    return


label tukuyomi_cg:
    scene
    show bg 208
    show tukuyomi h1
    with dissolve
    pause
    show tukuyomi h2 with dissolve
    pause
    show tukuyomi h3 with dissolve
    pause
    show tukuyomi h4 with dissolve
    pause
    return


label arcen_cg:
    scene
    show bg 208
    show arcen h1
    with dissolve
    pause
    show arcen h2 with dissolve
    pause
    show arcen h3 with dissolve
    pause
    show arcen h4 with dissolve
    pause
    return


label rapun_cg:
    scene
    show bg 208
    show rapun h1
    with dissolve
    pause
    show rapun h2 with dissolve
    pause
    show rapun h3 with dissolve
    pause
    show rapun h4 with dissolve
    pause
    return


label heavensgate_cg:
    scene
    show bg 208
    show heavensgate h1
    with dissolve
    pause
    show heavensgate h2 with dissolve
    pause
    show heavensgate h3 with dissolve
    pause
    show heavensgate h4 with dissolve
    pause
    return


label eden_cg:
    scene
    show bg 210

    if renpy.seen_label("eden_ha"):
        show eden ha1
        with dissolve
        pause
        show eden ha2 with dissolve
        pause
        show eden ha3 with dissolve
        pause
        show eden ha4 with dissolve
        pause

        if not renpy.seen_label("eden_hb"):
            return

    show eden hb1
    with dissolve
    pause
    show eden hb2  with dissolve
    pause
    show eden hb3 with dissolve
    pause
    show eden hb4 with dissolve
    pause
    return


label stein2_cg:
    scene
    show bg 211
    show stein2 h1
    with dissolve
    pause
    show stein2 h2 with dissolve
    pause
    show stein2 h3 with dissolve
    pause
    show stein2 ct01 at right as ct with dissolve
    pause
    show stein2 ct02 at right as ct with dissolve
    pause
    show stein2 h4 with dissolve
    pause
    show stein2 ct03 at right as ct with dissolve
    pause
    show stein2 ct04 at right as ct
    show stein2 h5
    with dissolve
    pause
    show stein2 ct05 at right as ct with dissolve
    pause
    hide ct
    show stein2 h6
    with dissolve
    pause
    show stein2 ct11 at right as ct with dissolve
    pause
    show stein2 ct12 at right as ct with dissolve
    pause
    show stein2 ct13 at right as ct with dissolve
    pause
    show stein2 ct14 at right as ct with dissolve
    pause
    show stein2 ct15 at right as ct with dissolve
    pause
    show stein2 ct16 at right as ct with dissolve
    pause
    return


label alice8th2_cg:
    scene
    show bg 212
    show alice8th2 st21
    with dissolve
    pause
    show alice8th2 ct01 at topleft zorder 15 as ct with dissolve
    pause
    show alice8th2 st22
    show alice8th2 ct02 at topleft zorder 15 as ct
    with dissolve
    pause
    show alice8th2 ct03 at topleft zorder 15 as ct with dissolve
    pause
    hide ct
    show alice8th2 st23
    with dissolve
    pause
    show alice8th2 st24 with dissolve
    pause
    show alice8th2 st25 with dissolve
    pause
    show alice8th2 st26 with dissolve
    pause
    show alice8th2 h1 with dissolve
    pause
    show alice8th2 h2 with dissolve
    pause
    show alice8th2 h3 with dissolve
    pause
    show alice8th2 h4 with dissolve
    pause
    show alice8th2 h5 with dissolve
    pause
    return


label alice8th3_cg:
    scene
    show bg 212
    show alice8th3 st11
    with dissolve
    pause
    show alice8th3 st12 with dissolve
    pause
    show alice8th3 h1 with dissolve
    pause
    show alice8th3 h2 with dissolve
    pause
    show alice8th3 h3 with dissolve
    pause
    show alice8th3 h4 with dissolve
    pause
    return


label alice8th4_cg:
    scene
    show bg 232
    show alice8th4 h1
    with dissolve
    pause
    show alice8th4 ha1 with dissolve
    pause
    show alice8th4 ha2 with dissolve
    pause
    show alice8th4 ha3 with dissolve
    pause
    show alice8th4 ha4 with dissolve
    pause
    show alice8th4 hb1 with dissolve
    pause
    show alice8th4 ct21 at right zorder 15 as ct with dissolve
    pause
    hide ct
    show alice8th4 hb2
    with dissolve
    pause
    show alice8th4 ct22 at right zorder 15 as ct with dissolve
    pause
    show alice8th4 ct23 at right zorder 15 as ct
    show alice8th4 hb3
    with dissolve
    pause
    show alice8th4 ct24 at right zorder 15 as ct
    show alice8th4 hb4
    with dissolve
    pause
    show alice8th4 ct25 at right zorder 15 as ct
    show alice8th4 hb5
    with dissolve
    pause
    hide ct
    show alice8th4 hc1
    with dissolve
    pause
    show alice8th4 ct31 at left zorder 15 as ct with dissolve
    pause
    hide ct
    show alice8th4 hc2
    with dissolve
    pause
    show alice8th4 ct32 at right zorder 15 as ct with dissolve
    pause
    show alice8th4 ct33 at right zorder 15 as ct
    show alice8th4 hc3
    with dissolve
    pause
    show alice8th4 ct34 at right zorder 15 as ct
    show alice8th4 hc4
    with dissolve
    pause
    show alice8th4 ct35 at right zorder 15 as ct
    show alice8th4 hc5
    with dissolve
    pause
    hide ct
    show alice8th4 hd1
    with dissolve
    pause
    show alice8th4 ct41 at right zorder 15 as ct with dissolve
    pause
    hide ct
    show alice8th4 hd2
    with dissolve
    pause
    show alice8th4 hd3 with dissolve
    pause
    show alice8th4 hd4 with dissolve
    pause
    show alice8th4 hd5 with dissolve
    pause
    show alice8th4 hd6 with dissolve
    pause
    show alice8th4 he1 with dissolve
    pause
    show alice8th4 he2 with dissolve
    pause
    show alice8th4 ct51 at right zorder 15 as ct with dissolve
    pause
    show alice8th4 ct52 at right zorder 15 as ct
    show alice8th4 he3
    with dissolve
    pause
    show alice8th4 ct53 at right zorder 15 as ct
    show alice8th4 he4
    with dissolve
    pause
    show alice8th4 ct54 at right zorder 15 as ct
    show alice8th4 he5
    with dissolve
    pause
    hide ct
    show alice8th4 hf1
    with dissolve
    pause
    show alice8th4 hf2 with dissolve
    pause
    show alice8th4 hf3 with dissolve
    pause
    show alice8th4 hf4 with dissolve
    pause
    show alice8th4 hg1 with dissolve
    pause
    show alice8th4 hg2 with dissolve
    pause
    show alice8th4 hg3 with dissolve
    pause
    show alice8th4 hg4 with dissolve
    pause
    show alice8th4 hh1 with dissolve
    pause
    show alice8th4 hh2 with dissolve
    pause
    show alice8th4 hh3 with dissolve
    pause
    show alice8th4 hh4 with dissolve
    pause
    show alice8th4 hh5 with dissolve
    pause
    return


label ilias3_cg:
    scene
    show bg 001
    show ilias hb1
    with dissolve
    pause
    show ilias hb2 with dissolve
    pause
    show ilias hb3 with dissolve
    pause
    show ilias hb4 with dissolve
    pause
    return


label ilias4_cg:
    scene
    show bg 001
    show ilias2 st21
    with dissolve
    pause
    show ilias2 st22 with dissolve
    pause

    if renpy.seen_label("ilias2_ha"):
        show ilias2 ha1
        with dissolve
        pause
        show ilias2 ct01 at topright zorder 15 as ct with dissolve
        pause
        show ilias2 ha2
        show ilias2 ct02 at topright zorder 15 as ct
        with dissolve
        pause
        show ilias2 ha3
        show ilias2 ct03 at topright zorder 15 as ct
        with dissolve
        pause
        show ilias2 ha4 with dissolve
        pause

        if not renpy.seen_label("ilias2_hb"):
            return

    hide ct
    show ilias2 hb1
    with dissolve
    pause
    show ilias2 hb2 with dissolve
    pause
    show ilias2 hb3 with dissolve
    pause
    show ilias2 hb4 with dissolve
    pause
    return


label end_cg:
    scene
    show bg 179
    show end1 h1
    with dissolve
    pause
    show end1 h2 with dissolve
    pause
    show end1 h3 with dissolve
    pause
    show end1 h4 with dissolve
    pause
    show end1 h5 with dissolve
    pause
    show end1 h6 with dissolve
    pause
    show bg 180
    show end2 h1
    with dissolve
    pause
    show end2 h2 with dissolve
    pause
    show end2 h3 with dissolve
    pause
    show end2 h4 with dissolve
    pause
    show end2 h5 with dissolve
    pause
    show bg 060
    show end3 h1
    with dissolve
    pause
    show end3 h2 with dissolve
    pause
    show end3 h3 with dissolve
    pause
    show end3 h4 with dissolve
    pause
    show bg 049
    show end4 h1
    with dissolve
    pause
    show end4 h2 with dissolve
    pause
    show end4 h3 with dissolve
    pause
    show end4 h4 with dissolve
    pause
    show bg 067
    show end5 h1
    with dissolve
    pause
    show end5 h2 with dissolve
    pause
    show end5 h3 with dissolve
    pause
    show end5 h4 with dissolve
    pause
    show bg 181
    show end6 h1
    with dissolve
    pause
    show end6 h2 with dissolve
    pause
    show end6 h3 with dissolve
    pause
    show end6 h4 with dissolve
    pause
    show end6 h5 with dissolve
    pause
    show bg 123
    show end7 h1
    with dissolve
    pause
    show end7 h2 with dissolve
    pause
    show end7 h3 with dissolve
    pause
    return


label granberia5_cg:
    scene

    if persistent.hsean_shitenno5 != 0:
        show bg 218
        show granberia he1
        with dissolve
        pause
        show granberia he2 with dissolve
        pause
        show granberia he3 with dissolve
        pause

        if not persistent.hsean_shitenno1:
            return

    show bg 047
    show granberia hf1
    with dissolve
    pause
    show granberia ct11 at topleft zorder 15 as ct with dissolve
    pause
    show granberia hf2
    show granberia ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show granberia hf3
    show granberia ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    return


label alma_elma4_cg:
    scene
    show bg 084
    show alma_elma hg1
    with dissolve
    pause
    show alma_elma hg2 with dissolve
    pause
    show alma_elma hg3 with dissolve
    pause
    return


label tamamo3_cg:
    scene
    show bg 176
    show tamamo hd01
    with dissolve
    pause
    show tamamo hd02 with dissolve
    pause
    show tamamo hd03 with dissolve
    pause
    show tamamo hd04 with dissolve
    pause
    show tamamo hd05 with dissolve
    pause
    show tamamo hd06 with dissolve
    pause
    show tamamo hd07 with dissolve
    pause
    show tamamo hd08 with dissolve
    pause
    show tamamo hd09 with dissolve
    pause
    show tamamo hd10 with dissolve
    pause
    return


label erubetie3_cg:
    scene
    show bg 177
    show erubetie hc1
    with dissolve
    pause
    show erubetie hc2 with dissolve
    pause
    show erubetie hc3 with dissolve
    pause
    show erubetie hc4 with dissolve
    pause
    return


label alice5_cg:
    scene
    show bg 195
    show alice h11_1
    with dissolve
    pause
    show alice h11_2 with dissolve
    pause
    show alice h11_3 with dissolve
    pause
    show alice h11_4 with dissolve
    pause
    show alice h11_5 with dissolve
    pause
    show bg 047
    show alice h12_1
    with dissolve
    pause
    show alice h12_2 with dissolve
    pause
    show alice ct01 at topright zorder 15 as ct with dissolve
    pause
    show alice ct02 at topright zorder 15 as ct with dissolve
    pause
    show alice ct03 at topright zorder 15 as ct with dissolve
    pause
    show alice ct04 at topright zorder 15 as ct with dissolve
    pause
    show alice h12_3 with dissolve
    pause
    show bg 001
    hide ct
    show alice iv03
    with dissolve
    pause
    return

## NG+
label granberia_ng_cg:
    scene

    if persistent.hsean_shitenno5 != 0:
        show bg 218
        show granberia he1
        with dissolve
        pause
        show granberia he2 with dissolve
        pause
        show granberia he3 with dissolve
        pause

        if not persistent.hsean_shitenno1:
            return

    show bg 047
    show granberia hf1
    with dissolve
    pause
    show granberia ct11 at topleft zorder 15 as ct with dissolve
    pause
    show granberia hf2
    show granberia ct12 at topleft zorder 15 as ct
    with dissolve
    pause
    show granberia hf3
    show granberia ct13 at topleft zorder 15 as ct
    with dissolve
    pause
    return
