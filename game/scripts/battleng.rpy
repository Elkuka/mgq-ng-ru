label tamamo_pre_ng_start:
    $ monster_name = "Тамамо"
    $ lavel = "tamamo_pre_ng"
    $ img_tag = "tamamo"
    $ tatie1 = "tamamo st11"
    $ tatie2 = "tamamo st12"
    $ tatie3 = "tamamo st13"
    $ tatie4 = "tamamo st11"
    $ haikei = "bg 035"
    $ monster_pos = center
    $ enemy_earth = 0
    $ enemy_earthturn = 0
    call skillset(2)
    pause 0.05
    play sound "audio/se/escape.ogg"

    if Difficulty == 1:
        $ max_enemylife = 22000
        $ henkahp1 = max_enemylife / 2
        $ henkahp2 = 5000
    elif Difficulty == 2:
        $ max_enemylife = 27000
        $ henkahp1 = max_enemylife / 2
        $ henkahp2 = 5000
    elif Difficulty == 3:
        $ max_enemylife = 33000
        $ henkahp1 = max_enemylife / 2
        $ henkahp2 = 5000
    elif Difficulty == 0:
        $ max_enemylife = 1
        $ henkahp1 = 1
        $ henkahp2 = 1

    $ alice_skill = 0
    $ damage_keigen = 100
    $ earth_keigen = 50
    if Difficulty == 3:
        $ earth_keigen = 25
    $ kaihi = 95
    $ mus = 4
    $ ng = 2
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ end_n = 9
    $ zmonster_x = -280
    $ tukix = 320
    $ owaza_count1a = 4
    $ owaza_count2a = 4
    $ owaza_count3a = 5

    pause .5
    t "Тогда хорошо.{w}\nДавай посмотрим на что ты способен!"
    "Вот оно...{w}\nЕсли я сражу её, тогда останется лишь два Небесных Рыцаря!"
    l "Погнали!"

    $ sinkou = 1
    $ hanyo[2] = 0
    jump common_main


label tamamo_pre_ng_main:
    if enemylife < henkahp1:
        jump tamamo_pre_ng_v

    call cmd("attack")

    if result == 1:
        jump common_attack
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump tamamo_pre_ng_kousan
    elif result == 6:
        jump tamamo_pre_ng_onedari
    elif result == 7:
        jump common_skill

label tamamo_pre_ng_a:
    if enemylife < henkahp1:
        jump tamamo_pre_ng_v

    if kousoku == 2:
        call tamamo_pre_ng_a8
        jump common_main

    while True:
        $ ransu = rnd2(1,8)

        if ransu == 1:
            call tamamo_pre_ng_a1
            jump common_main
        if ransu == 2:
            call tamamo_pre_ng_a2
            jump common_main
        if ransu == 3:
            call tamamo_pre_ng_a3
            jump common_main
        if ransu == 4:
            call tamamo_pre_ng_a4
            jump common_main
        if ransu == 5 and status == 0:
            call tamamo_pre_ng_a5
            jump common_main
        if ransu == 6:
            call tamamo_pre_ng_a6
            jump common_main
        if ransu == 7:
            call tamamo_pre_ng_a7
            jump common_main
        if ransu == 8:
            call tamamo_pre_ng_a9
            jump common_main

label tamamo_pre_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты полюбишь мою руку."
    elif ransu == 2:
        enemy "Приятнее всего, когда я ласкаю головку, правда ведь?"
    elif ransu == 3:
        enemy "Я пощекочу твою головку."

    call show_skillname("Мастурбация Кицунэ")
    play sound "audio/se/ero_koki1.ogg"

    "Маленькие ручки Тамамо проворно и ловко ласкают член Луки"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 40 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 30 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 20 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3641)

    $ dmg = {0: (370, 400), 1: (370, 400), 2: (400, 430), 3: (430, 460)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я буду сосать его столько, сколько захочу.{image=note}"
    elif ransu == 2:
        enemy "Не хочешь кончить в мой ротик? Хи-хи..."
    elif ransu == 3:
        enemy "Хи-хи... Думаешь, что сможешь стерпеть то, как я ласкаю тебя своим ротиком, м?"

    call show_skillname("Минет Кицунэ")
    play sound "audio/se/ero_buchu3.ogg"

    if wind_guard_on == 0 and aqua == 0:
        show tamamo ha2 with dissolve #700
        show tamamo ha4 with dissolve #700

    "Тамамо с похотливой жадностью сосёт весь член Луки своим маленьким ртом!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 40 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 30 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 20 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3642)
    $ dmg = {0: (400, 420), 1: (400, 420), 2: (430, 450), 3: (460, 480)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    call face(param=1)

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "У меня маленькая грудь... Но я знаю, как ей пользоваться."
    elif ransu == 2:
        enemy "Я хотя бы могу прижимать твою чувствительную головку к моим сосочкам..."
    elif ransu == 3:
        enemy "В мире полно людей, предпочитающих маленькие груди.{image=note}"

    call show_skillname("Потирание Плоской Грудью")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо потирает член Луки о свою плоскую грудь!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 40 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 30 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 20 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3643)
    $ dmg = {0: (380, 400), 1: (380, 400), 2: (410, 430), 3: (440, 460)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Некоторые люди просто обожают, когда над ними доминируют.{image=note}"
    elif ransu == 2:
        enemy "Я буду думать о тебе намного хуже, если ты от этого кончишь..."
    elif ransu == 3:
        enemy "Я буду жёстко и приятно растаптывать весь твой член..."

    call show_skillname("Растирание Ступнёй Луноборца")
    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она потирает его член своей маленькой ножкой!{w}{nw}"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 70 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 60 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 50 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3644)
    $ dmg = {0: (180, 200), 1: (180, 200), 2: (190, 210), 3: (200, 220)}
    call damage(dmg[Difficulty])
    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она потирает его член своей маленькой ножкой!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    play sound "audio/se/ero_koki1.ogg"

    "Удерживая своим хвостом равновесие, она сдавливает член Луки между обеими своими ножками!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a5:
    $ owaza_count2a = 0

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Такое твоё лицо просто заставляет меня хотеть облизать его..."
    elif ransu == 2:
        enemy "Мой язык может любого мужчину превратить в мою собственность..."
    elif ransu == 3:
        enemy "Мой язык может овладеть любым мужчиной.{image=note}"

    call show_skillname("Обессиливающее Развлечение")
    play sound "audio/se/ero_pyu1.ogg"

    "Тамамо прыгает на Луку и крепко его обнимает!{w}\nИ на этом она начинает вылизывать всё его лицо!"

    $ ransu = rnd2(1,100)

    if aqua > 0:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 6
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3645)
    $ dmg = {0: (200, 220), 1: (200, 220), 2: (230, 250), 3: (260, 280)}
    call damage(dmg[Difficulty])

    if status == 0:
        $ status = 1
        call status_print

        "Лука впадает в транс!"
    elif status == 1:
        call status_print

        "Лука в трансе..."

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    l "Хаа..."
    enemy "Все люди сразу же становятся лишь игрушками, когда ощущают мой язычок...{w}\nПоиграться мне теперь с тобой, м-м-м, как думаешь?"

    $ status_turn = rnd2(2,3)

    if Difficulty == 2:
        $ status_turn = rnd2(3,4)
    elif Difficulty == 3:
        $ status_turn = rnd2(4,5)
    return

label tamamo_pre_ng_a6:
    $ owaza_count3a = 0

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Пришло время для лучшего приёма!"
    elif ransu == 2:
        enemy "Пришло время добивания.{image=note}"
    elif ransu == 3:
        enemy "Этот приём доведёт тебя до оргазма.{image=note}"

    call show_skillname("Девять лун")
    $ ransu = rnd2(1,2)

    if ransu == 1:
        $ hanyo[1] = 1
        jump tamamo_pre_ng_a6a
    elif ransu == 2:
        $ hanyo[1] = 2
        jump tamamo_pre_ng_a6b

label tamamo_pre_ng_a6a:
    show tamamo cta01 at xy(X=490) zorder 15 as ct
    with dissolve #690

    "Все девять хвостов Тамамо устремляются к Луке!"

    play sound "audio/se/umaru.ogg"
    show tamamo cta02 at xy(X=490) zorder 15 as ct
    with dissolve #690

    "Первый хвост обматывается вокруг его члена!"

    if aqua > 0:
        hide ct with dissolve
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
        #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Второй хвост обвивается вокруг основания члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
        #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Кончик третьего хвоста щекочет отверстие его уретры!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Четвёртый хвост мягко массажирует и потискивает головку члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Пятый хвост поигрывает яичками Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Шестой хвост пощекочивает вверх и вниз по уздечке члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Седьмой хвост нежно потирается по анусу Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Восьмой хвост поглаживает вверх и вниз по всей длине спинки члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (180, 220), 1: (180, 220), 2: (184, 224), 3: (188, 228)}
        call damage(dmg[Difficulty])
    call hide_skillname

    $ nine_moons = 1

    if mylife == 0:
        return_to badend_h

    hide ct with dissolve

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a6b:
    play sound "audio/se/umaru.ogg"
    show tamamo ctb01 at xy(X=440) zorder 15 as ct
    with dissolve #690

    if aqua > 0:
        hide ct with dissolve
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 40
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 35
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 20

    call wind_guard_kakuritu

    if wind_guard_on == 0 and aqua == 0:
        show tamamo ctb02 at Position(anchor=(0, 0), pos=(440, 0)) zorder 15 as ct with dissolve #690

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            hide ct with dissolve
            jump wind_guard

   #call skillcount2(3647)

    "Тамамо протискивает член Луки в созданную хвостами дырочку!{w}{nw}"

    $ dmg = {0: (540, 660), 1: (540, 660), 2: (552, 672), 3: (564, 684)}
    call damage(dmg[Difficulty], "last")
    play sound "audio/se/umaru.ogg"

    "Мягкие и пушистые хвосты Тамамо стискиваются вокруг члена Луки!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    play sound "audio/se/umaru.ogg"

    "Её пушистые хвосты доят весь его член во всю длину, как поршень, едва ли не быстрее, чем может уловить его взгляд!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    hide ct with dissolve

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a7:
    $ owaza_count1a = 0
    $ owaza_num1 = 1

    if kousan != 2:
        if first_sel3 == 1:
            enemy "Что ж, тогда я опять заверну тебя в мои хвосты!"
        else:
            enemy "Я заверну тебя в мои хвостики!"

    call show_skillname("Tail Entangle")
    play sound "audio/se/ero_makituki3.ogg"

    "Девять хвостов Тамамо обвиваются вокруг всего тела Луки!"

    if aqua > 0:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 7
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (250, 280), 1: (250, 280), 2: (310, 380), 3: (340, 410)}
    call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    "Хвосты Тамамо удерживают Луку!"

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    if first_sel3 == 1:
        enemy "В этот раз ты не выберешься.{image=note}"
    else:
        enemy "Я вдо-о-оволь тебя обласкаю моими пушистыми хвостиками.{image=note}"

    $ first_sel3 = 1

    if earth == 0:
        $ genmogaku = 100
    elif earth > 0 and Difficulty < 3:
        $ genmogaku = 2
    elif earth > 0 and Difficulty == 3:
        $ genmogaku = 3

    return

label tamamo_pre_ng_a8:
    "Хвосты Тамамо удерживают Луку..."

    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break


    if ransu == 1:
        enemy "Хи-хи, адская щекотка!"
    elif ransu == 2:
        enemy "Я люблю вот эту твою штучку.{image=note}"
    elif ransu == 3:
        enemy "Я всю твою уязвимость обласкаю моими хвостами..."
    elif ransu == 4:
        enemy "Знамя твоего мужества станет причиной твоего поражения..."
    elif ransu == 5:
        enemy "Всё в порядке, не будет ничего плохого в том, если ты обрызгаешь мой хвост спермой.{image=note}"

    call show_skillname("Moontail Entanglement")
    play sound "audio/se/umaru.ogg"

    if hanyo[4] == 0:
        show tamamo ctc01 at xy(X=440) zorder 15 as ct with dissolve #690

    show tamamo ctc02 at xy(X=440) zorder 15 as ct with dissolve #690
    $ hanyo[4] = 1

    if ransu == 1:
        "The tip of her fluffy tail tickles Luka's penis!"
    elif ransu == 2:
        "One of Tamamo's tails tenderly strokes Luka's penis!"
    elif ransu == 3:
        "Tamamo's nine tails run all over Luka's penis!"
    elif ransu == 4:
        "All nine tails take turns rubbing against Luka's penis!"
    elif ransu == 5:
        "The tip of one of Tamamo's tail runs up and down the back of Luka's penis!"

   #call skillcount2(3646)
    $ dmg = {0: (250, 280), 1: (250, 280), 2: (310, 380), 3: (340, 410)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_pre_ng_a9:
    if status == 1:
        jump tamamo_pre_ng_a

    $ owaza_count3b = 0

    enemy "Ха!"

    call show_skillname("Оседлание Кицунэ")

    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"

    $ ransu = rnd2(1,100)
    if aqua > 0:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    if daten > 0:
        call skill22x
        jump common_main

    $ kousoku = 1
    call status_print

    "Тамамо сидит верхом на Луке!"
    enemy "Теперь ты мой.{w}\nВремя поиграть~!{image=note}"

   #call skillcount2(3648)
    call show_skillname("Moonlight Rendezvous")
    play sound "audio/se/ero_pyu1.ogg"
    show tamamo hb2 with dissolve #700

    "Тамамо садится вниз, насильно вгоняя член Луки в свою вагину!{w}{nw}"

    $ damage_kotei = 1
    $ tamamo_rape = 1
    $ kousoku = 2
    call damage((400, 450))

    "Лука был изнасилован Тамамо!"

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    l "Ahhh...!"
    "I gasp in pleasure as Tamamo's wet, tiny vagina instantly clamps down on me.{w}\nIf she keeps going like this, I'll cum in an instant!"

    return

label tamamo_pre_ng_kousan:
    call kousan_syori

    "Luka stops resisting..."
    enemy "Is that your answer?"

    call face(param=2)

    enemy "Оу? Сдаёшься?{w}\nЧто ж, для человека ты зашёл довольно далеко."
    enemy "Наградить ли тебя за это...?"

    if sinkou < 10:
        call tamamo_pre_ng_onedarix
    jump tamamo_pre_ng_a


label tamamo_pre_ng_kousan2:
    t "............."

    "Тамамо истощена..."

    jump tamamo_pre_ng_main

label tamamo_pre_ng_onedarix:
    return

label tamamo_pre_ng_v:
    $ store._skipping = True
    show tamamo st15 with dissolve
    t "Уаа!"
    "Тамамо пятится назад, не выдержав моего напора."
    show tamamo st11 with dissolve
    t "... Ты что-то с чем-то, знаешь?"
    t "Но я думаю на сегодня всё.{w}\nТы выиграл этот раунд, малыш."
    l "...{w} Что?"
    "Она вот так просто сдаётся?"
    "Нет...{w}\nЧтобы заставить её следовать моим идеалам, я должен доказать свою силу!"
    l "Это ведь была твоя не полная сила, не так ли?"
    t "Ммм?"
    l "Ты знаешь, о чём я говорю."
    l "Моя победа над тобой ничего не значит, когда ты в запечатанной форме!{w}\nПокажи мне свою истинную силу, Тамамо!"
    show tamamo st16 with dissolve
    t "Ох...{w}\nТы хочешь увидеть ЭТУ силу?"
    "Тамамо тихо посмеивается, будто насмехается надо мной."
    t "Я не пощажу тебя, даже если ты об этом попросишь.{w}\nТы уверен, что это то, чего ты хочешь, Лука?"
    l "Именно так."
    "Я ещё никогда не побеждал её истинную форму.{w}\nНо я не хочу победы, которая ничего для неё не значит.{w}\nСитуация прямо как когда я дрался с Альмой в замке."
    stop music fadeout 1.0
    t "Окей. Погналииии~!"
    "....................."
    play sound "audio/se/power.ogg"
    "Тамамо концентрирует свою магию!"
    play sound "audio/se/flash.ogg"
    scene bg white with dissolve
    "..."
    show bg 035 with dissolve
    show tamamo st62 with dissolve

label tamamo_ng_start:
    if mylife < 1:
        $ mylife = max_mylife
    $ lavel = "tamamo_ng"
    $ img_tag = "tamamo"
    $ tatie1 = "tamamo st62"
    $ tatie2 = "tamamo st62"
    $ tatie3 = "tamamo st63"
    $ tatie4 = "tamamo st62"
    $ monster_pos = center
    if Difficulty == 1:
        $ max_enemylife = 48000
    elif Difficulty == 2:
        $ max_enemylife = 60000
    elif Difficulty == 3:
        $ max_enemylife = 72000
    elif Difficulty == 0:
        $ max_enemylife = 1

    $ henkahp1 = 11000
    $ henkahp2 = 5000

    $ enemy_earth = 0
    $ enemy_earthturn = 0
    $ damage_keigen = 60
    $ earth_keigen = 50
    $ granberia = 0
    if Difficulty == 3:
        $ earth_keigen = 25
    $ mus = 21
    if damage_nobr > 0:
        $ damage_nobr = 0
    $ end_n = 9
    $ zmonster_x = -280
    $ tukix = 320
    $ hanyo[8] = 1
    $ enemylife = max_enemylife
    $ exp_minus = 770025
    $ mus = 21
    call musicplay
    $ renpy.show(haikei)
    t "Ты наверное думаешь, что настолько силён, что для тебя не составит проблем победить меня в этой форме, не так ли?{w}\nЧто ж, похоже придётся немного приуменьшить твою самонадеяность."
    l "Угх...!"
    "Её аура невероятно подавляющая.{w}\nТакая мощь..."
    "Боюсь, даже Алиса не смогла бы противостоять истинной форме Тамамо."
    t "Я являюсь одной из шести первых монстров, что ступили на землю.{w}\nИ я была одной из подчинённых на службе у первородной Владыки Монстров, мощь которой превосходила даже силу Илиас."
    t "Неужели ты правда думаешь, что я проиграю какому-то юнцу?"
    l "Ну и кто из нас тут ещё самоуверенный?"
    t "Ну тогда докажи это, если считаешь себя достаточно крутым."
    l "Я с удовольствием поставлю тебя на место!"
    t "... Однако эта форма всё ещё слаба.{w}\nЯ не могу использовать всю свою силу."
    t "... Хотя капля спермы может решить эту проблему.{w}\nПоэтому, пожалуй, я буду использовать только атаки удовольствием."
    t "По рукам?"
    l "... Хорошо."
    "Полагаю, мне придётся немного подождать, чтобы сразиться с ней в полную силу.{w}\nНо я должен победить её хотя бы в этой форме, чтобы заставить принять сосуществование."
    show tamamo st61 at xy(X=200)
    show alice st03b at xy(X=-133)
    with dissolve
    a "Эй, идиоты. Вы уже закончи-"
    show alice st07b at xy(X=-133) with dissolve
    extend "\n.............................."
    "Алиса тупо уставилась на перевоплотившуюся Тамамо."
    play sound "audio/se/escape.ogg"
    hide alice with dissolve
    call face(param=1)
    extend "\nПосле чего молча уходит в дверь из которой пришла."
    t "Глупая девочка.{w}\nНеужели она правда думала, что я, прожившая много столетий, так молодо выгляжу?"
    t "Но сейчас не об этом.{w}\nПокажи мне всё, на что ты способен, Лука!"
    if Difficulty == 3:
        $ skill01 = 27

    $ sinkou = 1
    jump common_main

label tamamo_ng_main:
    $ hanyo[0] = 0
    $ hanyo[1] = 0
    if enemy_earth < 1:
        $ damage_keigen = 60

    $ sinkou += 1

    if kousoku > 0 and granberia > 0:
        show tamamo hd02 with dissolve

    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1:
        jump common_attack
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump tamamo_ng_kousan
    elif result == 6:
        jump tamamo_ng_onedari
    elif result == 7:
        jump common_skill


label tamamo_ng_v:
    if persistent.nine_moons > 0:
        $ persistent.nine_moons_won = 1
    if Difficulty == 3:
        $ persistent.tamamo_hell = 1
    show tamamo st61 with dissolve
    $ nine_moouns = 0
    call part_achi
    $ daten = 0
    if not _in_replay:
        $ tamamo_fight = 3
        $ tamamo_rep += 2
        $ alice_rep += 1


    t "Ах...!"
    l "Ну всё, попалась..."
    call show_skillname("Денница")
    "Ослепительная звезда обрушивается в ад!"
    call skill22a

    while count < 3:
        play sound "audio/se/damage2.ogg"
        $ damage = rnd2(3200,3600)
        if count == 0:
            window show
            show screen hp
            window auto
        $ damage_nobr = 2

        call enemylife2

        $ count += 1

    call hide_skillname

    t "Гах...!{w}\nТы действительно её сын..."
    "Похоже, что Тамамо больше не может сражаться.{w}\nЕсли я продолжу в том же духе, это сильно навредит ей."
    l "...{w}\nХорошо, на этом хватит."
    t "Ммм?"
    l "Если мы продолжим в том же духе, то ты сильно пострадаешь или даже будешь запечатана.{w}\nЭто не то, ради чего я сражаюсь с тобой."
    t "Как благородно..."
    show tamamo st62 with dissolve
    extend " Но!"
    call show_skillname("Оседлание Кицунэ")
    play sound "audio/se/down.ogg"
    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"
    $ kousoku = 1
    call status_print(face=1)
    show tamamo hd01 with dissolve
    "Тамамо сидит верхом на Луке!"
    call hide_skillname
    t "Ты слишком мягок."
    l "Ах!{w}\nЧто за!?"
    "Я потерял бдительность и попался в её ловушку!"
    t "Понимаешь, Лука.{w}\nПросто потому, что я добрая ещё не значит, что меня легко победить."
    t "Кроме того, я уже запечатана, идиот."
    t "Теперь ты мой..."
    play sound "audio/se/earth2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0
    l "Что за!?"
    a "Я ведь говорила тебе не использовать Денницу, идиота кусок!"
    "Кричит Алиса из соседней комнаты."
    l "Что...?"
    if used_daystar == 0:
        extend "\nНо я не использовал Денницу!"
    l "Что происходит!?"
    t "О, я уверена нам не о чём беспокоиться.{w}\nНу а теперь..."
    call show_skillname("Рандеву Лунного Света")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0
    pause 0.5
    play sound "audio/se/bom2.ogg"
    scene bg white with flash
    t "Уаа!?"
    "Опоры не выдерживают и падают на Луку с Тамамо!"
    play sound "audio/se/damage2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0
    $ damage = rnd2(10000,12000)
    "Тамамо получает [damage] урона!"
    $ a_effect1 = 1
    call damage((2800, 3200))
    call hide_skillname
    stop music fadeout 1.0
    hide screen hp
    hide screen turn_scorer
    "............................"
    window hide
    play sound "audio/se/counter.ogg"
    show expression Text(_("ДВОЙНОЙ НОКАУТ!"), size=90, bold=True, italic="True", color="#ff0000", outlines=[(4, "#000", 0, 0)], font="fonts/taurus.ttf") zorder 30 as txt:
        align (.5, .5)

    pause 1.0
    scene bg black with Dissolve(1.0)
    pause 1.0
    window show
    window auto
    "..............................{w}\n-диот...{w}\nЭй, идиот!{w}\nПросыпайся!"
    l "Хах?"
    scene bg 021 with Dissolve(1.0)
    show alice st03 with dissolve
    a "Ты чуть не похоронил всех нас в этой пещере!{w}\nПеред тем, как использовать свою Денницу в таких местах, может стоить сначала убедиться смогут ли выдержать опоры, идиот!?"

    if used_daystar < 1:
        l "Но я не использовал Денницу!{w}\nТы хоть представляешь, как было сложно сдержатсья?"
        a "В любом случае, ты устроил настоящий хаос, из-за чего пещера не выдержала и обрушилась."

    elif used_daystar > 0:
        l "Ч-что произошло...?"
        a "Пещера обрушилась, вот что произошло!"

    l "Что!?{w}\nС Тамамо всё в порядке!?"
    t "Ууу..."
    hide alice with dissolve
    show tamamo st21 with dissolve
    ".............................."
    $ persistent.count_ko += 1

    if mylife == max_mylife and nodamage == 0:
        $ persistent.count_hpfull = 1

    if Difficulty == 3:
        $ persistent.count_hellvic = 1

    if status == 7:
        $ persistent.count_vickonran = 1

    $ renpy.end_replay()
    $ persistent.tamamo_ng_unlock = 1
    $ skillset = 1
    call victory(viclose=1)
    $ getexp = 4000000
    call lvup

    jump tamamo_ng_victory

label tamamo_ng_stalemate:
    show tamamo st61 with dissolve
    t "Эта битва...{w}\nОна слишком уж долго длится, тебе так не кажется?"
    l "Ха?"
    t "Наши силы примерно одинаковы, не так ли, мальчик?{w}\nИ, честно говоря, мне уже наскучило пытаться заставить тебя кончить."
    t "Почему бы нам не объявить ничью?{w}\nИсход битвы мы можем решить и в другой раз."
    l "Ух..."
    menu:
        "Объявить ничью":
            jump tamamo_ng_x1
        "Продолжить битву":
            jump tamamo_ng_x2

label tamamo_ng_x1:
    $ tamamo_rep += 1
    if party_member[1] == 2:
        $ granberia_rep -= 1

    $ tamamo_fight = 1

    l "Ты права.{w}\nЭто бессмысленно."
    "Мне тоже не очень хочеться продолжать битву в таком духе.{w}\nОбъявление ничьи ещё не значит, что мы вновь не сразимся когда-нибудь."
    t "Отлично.{w}{nw}"
    scene bg white with dissolve
    "..."
    show tamamo st11 with dissolve
    show bg 035 with dissolve
    extend "\nТогда на сегодня всё."

    call victory(viclose=1)
    $ getexp = (max_enemylife - enemylife) * 50
    call lvup

    jump tamamo_ng_victory

label tamamo_ng_x2:
    l "Нет.{w}\nЯ знаю, что смогу победить!{w}\nПока я стою на ногах, я никогда не сдамся!"
    t "Хорошо.{w}{nw}"
    call face(param=1)
    extend "\nКак пожелаешь...~{image=note}"
    jump tamamo_ng_main

label tamamo_ng_v2:
    $ tamamo_rep += 1
    $ tamamo_fight = 2
    show tamamo st61 with dissolve
    t "Угх.{w}\nЯ не могу больше сражаться..."
    t "Моя сила...ы"

    scene bg white with dissolve
    "Комната озаряется ярким светом!"
    show tamamo st11 with dissolve
    show bg 035 with dissolve
    play sound "audio/se/down.ogg"
    show tamamo st21 with dissolve
    t "Ууууууу...{w}\nНе могу больше...{w}\nЯ сдаюсь..."
    "Тамамо выдохлась!"

    call victory(viclose=1)
    $ getexp = (max_enemylife - enemylife) * 70
    call lvup

    jump tamamo_ng_victory

label tamamo_ng_a:
    if enemy_earth < 1:
      $ damage_keigen = 60

    $ enemy_aqua = 0
    $ enemy_fire = 0
    $ enemy_wind = 0
    if Difficulty == 1 and sinkou == 70:
        jump tamamo_ng_stalemate
    if Difficulty == 1 and sinkou == 100:
        jump tamamo_ng_v2
    if Difficulty == 2 and sinkou == 85:
        jump tamamo_ng_stalemate
    if Difficulty == 2 and sinkou == 125:
        jump tamamo_ng_v2
    if status == 1:
        $ aqua_turn += 1
    if kousoku == 1 and granberia > 0:
        call tamamo_ng_a9
        jump common_main
    if kousoku == 1:
        call tamamo_ng_a8
        jump common_main

    $ granberia = 0

    if mylife < mylife / 2:
        call tamamo_ng_ab
        jump common_main
    elif mp < 3:
        call tamamo_ng_ab
        jump common_main

    if aqua < 1 and kousan == 0:
        call tamamo_ng_a6
        jump common_main

    while True:

        $ ransu = rnd2(1,12)

        if ransu == 1:
            call tamamo_ng_a1
            jump common_main
        if ransu == 2 and hanyo[1] == 0:
            call tamamo_ng_a2
            jump common_main
        if ransu == 3 and hanyo[0] == 0:
            call tamamo_ng_a3
            jump common_main
        if ransu == 4:
            call tamamo_ng_a4
            jump common_main
        if ransu == 5 and status == 0:
            call tamamo_ng_a5
            jump common_main
        if ransu == 6:
            call tamamo_ng_a6
            jump common_main
        if ransu == 7:
            call tamamo_ng_a7
            jump common_main
        if ransu == 8:
            call tamamo_ng_a9
            jump common_main
        if ransu > 8 and enemy_earth == 0:
            call tamamo_ng_a10
            jump common_main

label tamamo_ng_ab:
    while True:
        $ ransu = rnd2(1,7)

        if ransu == 1:
            call tamamo_ng_a2
            jump common_main
        if ransu == 2:
            call tamamo_ng_a3
            jump common_main
        if ransu == 3:
            call tamamo_ng_a5
            jump common_main
        if ransu == 4:
            call tamamo_ng_a6
            jump common_main
        if ransu == 5 and status == 0 and aqua < 1:
            call tamamo_ng_a5
            jump common_main
        if ransu == 6:
            call tamamo_ng_a7
            jump common_main
        if ransu == 7:
            call tamamo_ng_a9
            jump common_main

label tamamo_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты полюбишь мою руку."
    elif ransu == 2:
        enemy "Приятнее всего, когда я ласкаю головку, не так ли?"
    elif ransu == 3:
        enemy "Я пощекочу твою головку."

    call show_skillname("Мастурбация Кицунэ")
    play sound "audio/se/ero_koki1.ogg"

    "Руки Тамамо проворно и ловко ласкают член Луки"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 5 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 6
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3641)

    $ dmg = {0: (370, 400), 1: (420, 450), 2: (450, 480), 3: (520, 550)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я буду сосать его столько, сколько захочу.{image=note}"
    elif ransu == 2:
        enemy "Не хочешь кончить в мой ротик? Хи-хи..."
    elif ransu == 3:
        enemy "Хи-хи... Думаешь, что сможешь стерпеть то, как я ласкаю тебя своим ротиком, м?"

    call show_skillname("Минет Кицунэ")
    play sound "audio/se/ero_buchu3.ogg"

    if wind_guard_on == 0 and aqua == 0:
        show tamamo ha2 with dissolve #700
        show tamamo ha4 with dissolve #700

    "Тамамо с похотливой жадностью сосёт весь член Луки своим ртом!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 80 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 75 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 55 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3642)
    $ dmg = {0: (200, 250), 1: (200, 250), 2: (270, 280), 3: (390, 410)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_buchu3.ogg"
    "Она заглатывает всё основание члена Луки!"
    $ dmg = {0: (200, 250), 1: (450, 500), 2: (480, 530), 3: (490, 560)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_buchu3.ogg"
    "Тамамо начинает бегло сосать и вылизывать каждый сантиметр члена Луки!"
    $ dmg = {0: (200, 250), 1: (560, 600), 2: (580, 630), 3: (810, 860)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    call face(param=1)

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a3:
    if kousan != 2:
        $ hanyo[0] = 1
        $ ransu = rnd2(1,100)
        if mp > 2:
            if mylife > max_mylife / 2 and ransu < 96:
                call tamamo_ng_a

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Моя потрясающая грудь пленит кого угодно~!{image=heart}"
    elif ransu == 2:
        enemy "Тебе ведь нравятся такие большие. Не так ли, мальчик?"
    elif ransu == 3:
        enemy "Ты ведь предпочитаешь пайзури?"

    call show_skillname("Пайзури Лисьей Королевы")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо сжимает член Луки между своих мягких сисек!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 80 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 75 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 55 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3643)
    $ dmg = {0: (200, 250), 1: (420, 450), 2: (500, 550), 3: (610, 650)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_koki1.ogg"
    "Она зажимает член Луки между своими сиськами!{w}{nw}"
    $ dmg = {0: (200, 250), 1: (380, 420), 2: (450, 490), 3: (460, 500)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_koki1.ogg"
    "И начинает двигать ими, принося удовольствие!"
    $ dmg = {0: (200, 250), 1: (450, 490), 2: (520, 560), 3: (620, 670)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Некоторые люди просто обожают, когда над ними доминируют.{image=note}"
    elif ransu == 2:
        enemy "Я буду думать о тебе намного хуже, если ты от этого кончишь..."
    elif ransu == 3:
        enemy "Я буду жёстко и приятно растаптывать весь твой член..."

    call show_skillname("Растирание Ступнёй Луноборца")
    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она потирает его член своей маленькой ножкой!{w}{nw}"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 4 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3644)
    $ dmg = {0: (180, 200), 1: (130, 150), 2: (140, 160), 3: (180, 200)}
    call damage(dmg[Difficulty])
    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она потирает его член своей маленькой ножкой!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо начинает мастурбировать его член!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a5:
    $ ransu = rnd2(1,100)
    if aqua > 0 and ransu < 95:
        jump tamamo_ng_a
    $ owaza_count2a = 0

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Такое твоё лицо просто заставляет меня хотеть облизать его..."
    elif ransu == 2:
        enemy "Мой язык может любого мужчину превратить в мою собственность..."
    elif ransu == 3:
        enemy "Мой язык может овладеть любым мужчиной.{image=note}"

    call show_skillname("Обессиливающее Развлечение")

    play sound "audio/se/ero_pyu1.ogg"

    "Тамамо прыгает на Луку и крепко его обнимает!{w}\nИ на этом она начинает вылизывать всё его лицо!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 3:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 100 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    #call skillcount2(3645)
    $ dmg = {0: (200, 220), 1: (500, 520), 2: (530, 550), 3: (560, 580)}
    call damage(dmg[Difficulty])

    if status == 0:
        $ status = 1
        call status_print

        "Лука впадает в транс!"
    elif status == 1:
        call status_print

        "Лука в трансе..."

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    l "Хаа..."
    enemy "Все люди сразу же становятся лишь игрушками, когда ощущают мой язычок...{w}\nПоиграться мне теперь с тобой, м-м-м, как думаешь?"

    $ status_turn = rnd2(2,3)

    if Difficulty == 2:
        $ status_turn = rnd2(3,4)
    elif Difficulty == 3:
        $ status_turn = rnd2(4,5)
    return

label tamamo_ng_a6:
    $ ransu = rnd2(1,100)
    if aqua > 0 and ransu < 95:
        jump tamamo_ng_a
    $ owaza_count3a = 0

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "На этот раз ты не выдержишь..."
    elif ransu == 2:
        enemy "Время заканчивать~!{image=note}"
    elif ransu == 3:
        enemy "Этот приём доведёт тебя до оргазма~!{image=note}"

    call show_skillname("Девять Лун")
    $ ransu = rnd2(1,2)

    if ransu == 1:
        $ hanyo[1] = 1
        jump tamamo_ng_a6a
    elif ransu == 2:
        $ hanyo[1] = 2
        jump tamamo_ng_a6b

label tamamo_ng_a6a:
    show tamamo cta01 at xy(X=490) zorder 15 as ct
    with dissolve #690

    "Все девять хвостов Тамамо устремляются к Луке!"

    play sound "audio/se/umaru.ogg"
    show tamamo cta02 at xy(X=490) zorder 15 as ct
    with dissolve #690

    "Первый хвост обматывается вокруг его члена!"

    $ ransu = rnd2(1,1000)
    if aqua > 0 and Difficulty < 2 and ransu < 1001 + undine_buff:
        hide ct with dissolve
        jump aqua_guard
    elif aqua > 0 and Difficulty > 1 and ransu < 999 + undine_buff:
        hide ct with dissolve
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Второй хвост обвивается вокруг основания члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Кончик третьего хвоста щекочет отверстие его уретры!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Четвёртый хвост мягко массажирует и потискивает головку члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Пятый хвост поигрывает яичками Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Шестой хвост пощекочивает вверх и вниз по уздечке члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Седьмой хвост нежно потирается по анусу Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Восьмой хвост поглаживает вверх и вниз по всей длине спинки члена Луки!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])

    play sound "audio/se/umaru.ogg"

    "Девятый хвост обвивается вокруг головки члена Луки и нежно её стискивает!"

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Девять Лун")
    else:
       #call skillcount2(3647)

        $ dmg = {0: (380, 420), 1: (380, 420), 2: (384, 424), 3: (388, 428)}
        call damage(dmg[Difficulty])
    call hide_skillname
    $ nine_moons = 1

    if mylife == 0:
        return_to badend_h

    hide ct with dissolve

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a6b:
    play sound "audio/se/umaru.ogg"
    show tamamo ctb01 at xy(X=440) zorder 15 as ct
    with dissolve #690

    $ ransu = rnd2(1,1000)
    if aqua > 0 and Difficulty < 2 and ransu < 1001 + undine_buff:
        hide ct with dissolve
        jump aqua_guard
    elif aqua > 0 and Difficulty > 1 and ransu < 999 + undine_buff:
        hide ct with dissolve
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 40
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 35
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 20

    call wind_guard_kakuritu

    if wind_guard_on == 0 and aqua == 0:
        show tamamo ctb02 at Position(anchor=(0, 0), pos=(440, 0)) zorder 15 as ct with dissolve #690

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            hide ct with dissolve
            jump wind_guard

   #call skillcount2(3647)

    "Тамамо протискивает член Луки в созданную хвостами дырочку!{w}{nw}"

    $ dmg = {0: (540, 660), 1: (540, 660), 2: (552, 672), 3: (564, 684)}
    call damage(dmg[Difficulty], "last")
    play sound "audio/se/umaru.ogg"

    "Мягкие и пушистые хвосты Тамамо стискиваются вокруг члена Луки!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    play sound "audio/se/umaru.ogg"

    "Её пушистые хвосты доят весь его член во всю длину, как поршень, едва ли не быстрее, чем может уловить его взгляд!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    hide ct with dissolve

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a7:
    $ ransu = rnd2(1,100)
    if aqua > 0 and ransu < 95:
        jump tamamo_ng_a
    $ owaza_count1a = 0
    $ owaza_num1 = 1

    if kousan != 2:
        if first_sel3 == 1:
            enemy "Что ж, тогда я опять заверну тебя в мои хвосты!"
        else:
            enemy "Я заверну тебя в мои хвостики!"

    call show_skillname("Tail Entangle")
    play sound "audio/se/ero_makituki3.ogg"

    "Девять хвостов Тамамо обвиваются вокруг всего тела Луки!"

    if aqua > 0:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 7
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (250, 280), 1: (250, 280), 2: (380, 410), 3: (410, 440)}
    call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    "Хвосты Тамамо удерживают Луку!"

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    if first_sel3 == 1:
        enemy "В этот раз ты не выберешься.{image=note}"
    else:
        enemy "Я вдо-о-оволь тебя обласкаю моими пушистыми хвостиками.{image=note}"

    $ first_sel3 = 1

    if earth == 0:
        $ genmogaku = 100
    elif earth > 0 and Difficulty < 3:
        $ genmogaku = 2
    elif earth > 0 and Difficulty == 3:
        $ genmogaku = 3

    return

label tamamo_ng_a8:
    "Хвосты Тамамо удерживают Луку..."

    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break


    if ransu == 1:
        enemy "Хи-хи, адская щекотка!"
    elif ransu == 2:
        enemy "Я люблю вот эту твою штучку.{image=note}"
    elif ransu == 3:
        enemy "Я всю твою уязвимость обласкаю моими хвостами..."
    elif ransu == 4:
        enemy "Знамя твоего мужества станет причиной твоего поражения..."
    elif ransu == 5:
        enemy "Всё в порядке, не будет ничего плохого в том, если ты обрызгаешь мой хвост спермой.{image=note}"

    call show_skillname("Moontail Entanglement")
    play sound "audio/se/umaru.ogg"

    if hanyo[4] == 0:
        show tamamo ctc01 at xy(X=440) zorder 15 as ct with dissolve #690

    show tamamo ctc02 at xy(X=440) zorder 15 as ct with dissolve #690
    $ hanyo[4] = 1

    if ransu == 1:
        "The tip of her fluffy tail tickles Luka's penis!"
    elif ransu == 2:
        "One of Tamamo's tails tenderly strokes Luka's penis!"
    elif ransu == 3:
        "Tamamo's nine tails run all over Luka's penis!"
    elif ransu == 4:
        "All nine tails take turns rubbing against Luka's penis!"
    elif ransu == 5:
        "The tip of one of Tamamo's tail runs up and down the back of Luka's penis!"

   #call skillcount2(3646)
    $ dmg = {0: (250, 280), 1: (250, 280), 2: (380, 410), 3: (410, 440)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label tamamo_ng_a9:
    if kousan != 2:
        $ ransu = rnd2(1,100)
        if aqua > 0 and ransu < 90:
            jump tamamo_ng_a

    if granberia == 1:
        call tamamo_ng_a9a
    elif granberia == 2:
        call tamamo_ng_a9b
    elif granberia == 3:
        call tamamo_ng_a9c

    $ owaza_count3b = 0

    enemy "Ха!"

    call show_skillname("Оседлание Кицунэ")

    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"

    $ ransu = rnd2(1,100)
    if aqua > 0 and ransu < 91 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    if daten > 0:
        call skill22x
        jump common_main

    $ kousoku = 1
    call status_print

    "Тамамо сидит верхом на Луке!"
    enemy "Теперь ты мой.{w}\nВремя поиграть~!{image=note}"

   #call skillcount2(3648)
    call show_skillname("Рандеву Лунного Света")
    play sound "audio/se/ero_pyu1.ogg"
    show tamamo hd01 with dissolve #700

    "Тамамо садится вниз, насильно вгоняя член Луки в свою вагину!{w}{nw}"

    $ damage_kotei = 1
    $ tamamo_rape = 1
    $ kousoku = 2
    call damage((600, 650))

    "Лука был изнасилован Тамамо!"

    call hide_skillname
    $ granberia += 1

    if mylife == 0:
        return_to badend_h

    l "Ahhh...!"
    "I gasp in pleasure as Tamamo's wet, tiny vagina instantly clamps down on me.{w}\nIf she keeps going like this, I'll cum in an instant!"

    return

label tamamo_ng_a10:
    enemy "...................."
    call show_skillname("Дикие Земли")
    play sound "audio/se/earth2.ogg"
    "Тамамо наполняет свое тело силой земли!"
    $ enemy_earth = 1
    $ enemy_earthturn = 4
    $ damage_keigen = 40

    call hide_skillname

    return

label tamamo_ng_kousan:
    call kousan_syori

    "Luka stops resisting..."
    enemy "Is that your answer?"

    call face(param=2)

    enemy "Sheesh... There's no hope for you."

    call tamamo_ng_a10
    jump common_main


label tamamo_ng_kousan2:
    a "After coming this far, don't disappoint me.{w}\nAll that's left is for you to wield your sword..."

    jump tamamo_ng_main


label tamamo_ng_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Satanic Pressure"
    $ list2 = "Pleasure Drain"
    $ list3 = "Eyes of Temptation"

    if persistent.skills[3657][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3658][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3659][0] > 0:
        $ list3_unlock = 1

    call cmd("onedari")

    if result == 1:
        jump tamamo_ng_onedari1
    elif result == 2:
        jump tamamo_ng_onedari2
    elif result == 3:
        jump tamamo_ng_onedari3
    elif result == -1:
        jump tamamo_ng_main


label tamamo_ng_onedari1:
    call onedari_syori_k

    enemy "You want me to strangle you with my tail...?{w}\nSheesh... There's no hope for you."

    if kousoku == 0:
        call tamamo_ng_a7

    while True:
        call tamamo_ng_a8


label tamamo_ng_onedari2:
    call onedari_syori_k

    enemy "You want me to suck on you with my tail...?{w}\nSheesh... There's no hope for you."

    if kousoku == 0:
        call tamamo_ng_a7

    while True:
        call tamamo_ng_a9


label tamamo_ng_onedari3:
    call onedari_syori

    enemy "You want to give your heart to me?{w}\nSheesh... There's no hope for you."

    call tamamo_ng_a10
    jump tamamo_ng_yuwaku_a


label tamamo_ng_h1:
    l "Uwa...!"

    play sound "audio/se/down.ogg"

    "After taking Alice's attack, I fall to the ground in defeat."

    call lose(viclose=2)
    call count_defeat(monster_name, "alice3")
    call count(14, 3)
    $ bad1 = "Alipheese's attack brought Luka to his knees."
    call face(param=2)
    jump tamamo_ng_h


label tamamo_ng_h2:
    $ ikigoe = 1
    call ikigoe
    call syasei1
    show alice bk07 zorder 10 as bk #692
    call syasei2

    "As Alipheese's tail winds around Luka, he comes, spraying his semen all over her tail."

    call lose(viclose=1)
    call count_defeat(monster_name, "alice3")
    call count(16, 3)
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to come as Alipheese wrapped her tail around him."
    call face(param=2)
    jump tamamo_ng_h


label tamamo_ng_h3:
    $ ikigoe = 1
    call ikigoe
    call syasei1
    show alice bk07 zorder 10 as bk #692
    call syasei2

    "As Alipheese tightens her tail around Luka, he comes, spraying his semen all over her tail."

    call lose(viclose=1)
    call count_defeat(monster_name, "alice3")
    call count(16, 3)
    $ bad1 = "Luka was forced to come as Alipheese tightened her tail around him."
    call face(param=2)
    jump tamamo_ng_h


label tamamo_ng_h4:
    $ ikigoe = 1
    call ikigoe
    call syasei1
    show alice bk11 zorder 10 as bk #691
    call syasei2

    "As Alipheese sucks on Luka's penis with the tip of her tail, Luka comes, filling her with his semen."

    call lose(viclose=1)
    call count_defeat(monster_name, "alice3")
    call count(2, 3)
    $ bad1 = "Alipheese used her tail to suck on Luka and force him to come."
    call face(param=2)
    jump tamamo_ng_h


label tamamo_ng_h5:
    $ ikigoe = 1
    call ikigoe
    call syasei1
    show alice bk04 zorder 10 as bk #691
    call syasei2

    "As Luka lusts after Alipheese's breasts, he comes, covering her hands with his semen."

    call lose(viclose=1)
    call count_defeat(monster_name, "alice3")
    call count(31, 3)
    $ bad1 = "Luka was forced to come as he lusted after Alipheese's breasts."
    jump tamamo_ng_h


label tamamo_ng_h:
    $ end_go = "lb_0300a"
    $ bad2 = "Luka spends an eternity having sex with Alice in the closed off space."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    hide bk
    show alice st04
    with dissolve #700

    if kousan != 0:
        a "Sheesh, you really are pitiful..."
    else:
        a "Did you truly fight seriously...?{w}\nNo, it doesn't matter anymore."

    "Alice begins to mutter a strange incantation...{w}\nThe throne room fills with power, as a strange atmosphere descends around us."
    "I hold my breath as it feels like the outside world itself is being cut off from us."
    l "Wh...What are you doing...?"

    show alice st01b with dissolve #700

    a "I created an unbreakable barrier around this room.{w}\nIt's no longer possible to enter or leave."
    l "N...No way..."
    "She created an unbreakable barrier...?{w}\nAnd inside of it, just the two of us remain..."
    a "With this, the Monster Lord vanished from the world.{w}\nSadly, so did the brave Hero who killed her."
    a "...Just a slight modification of the original scenario.{w}\nThe world should easily accept that kind of story, too."
    a "However... You can never escape from here again.{w}\nTogether with me, you shall be in here for eternity."
    l "Wh...Why did you...?"
    a "............."

    play sound "audio/se/ero_makituki3.ogg"

    "Alice slowly winds her tail around me, calmly taking off my clothes.{w}\nAfter stripping me naked, she pulls me close against her body."

    voice "audio/voice/alice9_01.ogg"

    a "...Luka, have sex with me.{w}\nWe shall spend eternity lusting after each other's bodies..."
    l "B...But I..."
    "Alice uses her tail to press me up right against her."

    voice "audio/voice/alice9_02.ogg"

    a "Hoora... It's the Monster Lord's pussy that took your virginity...{w}\nI'll use this to show you heaven again..."
    l "N...No, Alice...!{w}\nAhh!"
    "Ignoring me, Alice violates me by force."

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    show alice h09_1r with dissolve #700

    "My penis slowly sinks into her, the hot, wet flesh rubbing against me as I enter.{w}\nHer seductive touch is much different from when I felt her in her human form."
    "Warm juices fill her vagina, dripping out the deeper I go.{w}\nAt the same time, her soft flesh presses against my penis, rubbing me all the way inside."
    "Her folds eagerly twitch at the feeling of my glans as I press into her, all while she gently squeezes down on me."

    voice "audio/voice/alice9_03.ogg"

    a "Hoora, you're all the way inside..."
    l "Ahhh..."
    "The moment I'm all the way inside her, my body shakes as I seemingly go into a trance of pure ecstasy."

    call syasei1
    show alice h09_2r #700
    call syasei2

    "Having an instant orgasm, I come inside Alice as soon as I'm all the way in her."
    "Alice looks down at me and smiles as I go red from humiliation."

    voice "audio/voice/alice9_04.ogg"

    a "Hehe... Couldn't endure for ten seconds again?{w}\nMy pussy is an instant finish for you, I see..."

    voice "audio/voice/alice9_05.ogg"

    a "Even if I don't use a skill or anything, just insertion is enough...{w}\nThat really is a weak penis you have there..."
    l "Ahh..."
    "Even though I feel humiliated, with her powerful tail around me, I can't escape...{w}\nFrustration wells up in me as I can't even struggle."
    "As a result, I just feebly squirm in pleasure.{w}\nJust how long is Alice going to coil around me and tease me like this...?"

    voice "audio/voice/alice9_06.ogg"

    a "Hehe... What a pitiful look. You don't look like a Hero.{w}\nYou betrayed my hopes, and couldn't fulfill your duty as a Hero..."

    voice "audio/voice/alice9_07.ogg"

    a "I'll need to torment a miserable guy like you a lot...{w}\nHoora... Writhe in the Monster Lord's vagina techniques!"
    "Alice's vagina suddenly squeezes down around my penis, as if it was a snake coiling around its prey."
    l "Ah!{w} Ahhh!"

    voice "audio/voice/alice9_08.ogg"

    a "Hehe... Unbearable tightness, isn't it?{w}\nI can control my pussy at will, and even change its shape..."

    voice "audio/voice/alice9_09.ogg"

    a "I've mastered many techniques to bring a man to tears...{w}\nHora... Horahora... Like this..."
    l "Ahhh!!"
    "As my body is attacked by the sensations from the intense tightness, Alice presses me closer against her body"

    voice "audio/voice/alice9_10.ogg"

    a "Hehe... It looks like it feels good.{w}\nThat skill is called \"Serpent Choking\"..."

    voice "audio/voice/alice9_11.ogg"

    a "I skillfully use my vaginal muscles to tighten and give you a feeling of pressure...{w}\nIf you take this, someone like you wouldn't last long..."
    l "Ahhh!!"
    "I feel her muscles pressing into me from every direction, clamping down around my penis."
    "Without being painful at all, the unique sensations flood me with pleasure Alice hugs me closer."
    l "Ahh... I... I'm going to come..."

    voice "audio/voice/alice9_12.ogg"

    a "Hehe... You sure are fast...{w}\nNow come as the serpent squeezes down on you!"
    "Suddenly, she squeezes down on me from the base of my penis right to the tip, just as if there really was a snake inside of her constricting my penis."
    l "Ahh...!"
    "The serpent inside of her squeezes and presses down on me, forcing me over the edge."
    l "Ah... No more...{w}\nAhh!"

    call syasei1
    show alice h09_3r #700
    call syasei2

    "I explode inside of Alice, defeated by the clamping sensations inside of her."

    voice "audio/voice/alice9_13.ogg"

    a "Hehe... Such a quick surrender.{w}\nWhat a shameful man, letting so much leak out..."

    voice "audio/voice/alice9_14.ogg"

    a "I'll train that premature ejaculating penis of yours a little with my pussy...{w}\nWe have an eternity of time, after all..."
    l "Eh...?{w} Ahh!"
    "Suddenly, she loosens her vagina around me as the walls of her vagina start to squeeze and rub against my penis."
    "It's almost as if I'm stuck in the digestive organ of some creature...{w}\nOvercome by a completely new sensation, I feel the strength drain from my body again."

    voice "audio/voice/alice9_15.ogg"

    a "This skill is called the Sea Slug Wave...{w}\nThe name is poor, but it was created by a Monster Lord generations back..."

    voice "audio/voice/alice9_16.ogg"

    a "As you're feeling now, I make my pussy slowly move in a wave motion...{w}\nA skill to drive a man to orgasm with this strange movement..."

    voice "audio/voice/alice9_17.ogg"

    a "Horehore... It's unbearable, isn't it?{w}\nYou can't endure it, can you? You want to come, don't you?"
    l "Ahhh...!"
    "The wave motion in her vagina gets more and more intense, even going so far as to slightly bend my erect penis a little bit inside of her."
    "The tip of my penis rubs hard against her soft flesh as her walls push it around inside her, bringing unbearable pleasure with it."

    voice "audio/voice/alice9_18.ogg"

    a "Hehe... I know your penis is rejoicing at being inside my pussy."

    voice "audio/voice/alice9_19.ogg"

    a "You want to come inside me like this, don't you?{w}\nGo ahead and come, without holding anything back..."
    l "Ahh... It feels so good..."
    "My penis twitches inside of her as my body rejoices in bliss at the soft feeling of Alice's vagina around me."
    l "Ahhhh!!"

    call syasei1
    call syasei2

    "I climax as her wave motion continues, knocking my penis around inside of her even as I come."
    "Surrendering myself to the pleasure of her vagina, I softly sigh as her vagina continues rubbing against my penis as I come inside her."
    l "Ahh..."

    voice "audio/voice/alice9_20.ogg"

    a "Hehe... You surrendered after the third time...{w}\nI'll torment that surrendered penis of yours even more..."

    voice "audio/voice/alice9_21.ogg"

    a "Taste the Monster Lord's vaginal skills with your body..."
    "Alice's vagina suddenly gets smaller, as a slow suction sensation starts to pull at me inside her."
    "At the same time her vagina starts seemingly sucking on me, the vaginal walls start to squirm and wriggle around my penis."
    "It feels like my body itself could melt in pleasure as the suction slowly gets stronger and stronger."
    l "Haa..."

    voice "audio/voice/alice9_22.ogg"

    a "This skill is called \"Leech Suck\".{w}\nLike a leech sucking blood, I can suck semen out of your penis..."

    voice "audio/voice/alice9_23.ogg"

    a "Hoora, I'm going to suck the semen right out of your penis...{w}\nHora, hora, hora..."
    l "Ahhh!!"
    "The suction slowly gets more and more intense, causing her soft vaginal walls to press down hard against my penis."
    "Almost pulling my penis deeper inside, it really is as if her vagina is a giant leech, trying to forcibly suck semen out of my penis."
    l "No... I'm going to come again..."
    "I clench my stomach, trying to stop the coming orgasm.{w}\nAs a man, I can't just let myself be played with like this..."

    voice "audio/voice/alice9_24.ogg"

    a "Hehe, it's pointless to resist...{w}\nThis is a skill to forcibly suck out semen, after all."

    voice "audio/voice/alice9_25.ogg"

    a "Give up to the suction sensations, and allow me to suck out your semen..."
    l "Ahhh..."
    "Her vagina sucks hard on me like a mouth even as she teases me for my resistance."
    "Despite my best effort to hold on, the unrelenting stimulation slowly breaks down my defenses.{w}\nBefore long, I feel my lower body start to go numb in pleasure."
    l "Ahh... It... It feels good..."

    voice "audio/voice/alice9_26.ogg"

    a "Hehe, what truly short endurance...{w}\nHoora, I'll suck out the proof of your surrender!"
    "In the next instant, her vagina starts pulling on me even harder.{w}\nAlmost as if my soul itself was being sucked out, I feel myself pulled deeper into her as I'm pushed over the edge."
    l "Ahhh!!"

    call syasei1
    call syasei2

    "As I explode inside her, the powerful suction pulls out my semen even as I come.{w}\nGreedily sucking on my penis, the semen is pulled deeper inside Alice as she sucks on me with her vagina."
    "I shake in ecstasy as the suction continues, until every drop of my semen has been forcibly sucked out."
    l "Haa...{w}\nStop this already..."

    voice "audio/voice/alice9_27.ogg"

    a "Hehe... Did you think you would be freed from my pussy after just that?{w}\nOnce your penis is inside me, I'm not going to let go until I drain every drop of semen..."

    voice "audio/voice/alice9_28.ogg"

    a "Hoora, how about this skill?"
    "Suddenly, her vaginal walls start to move as the thousands of folds inside her press hard against my penis."
    "Like that, they all start moving up and down, as if they were thousands of tongues licking every inch of me."
    l "Ahhh!!"

    voice "audio/voice/alice9_29.ogg"

    a "Hehe... This is called \"Fold Hell\".{w}\nI can move each and every fold as I want..."

    voice "audio/voice/alice9_30.ogg"

    a "If I focus on the tip..."
    "The folds all converge around the tip of my penis, as if hundreds of tiny individual tongues are all licking my glans."
    l "Ahhh!!{w}\nStop it!"
    "I tremble in Alice's tail, still tightly holding me next to her as the overwhelming stimulation attacks me."
    "With her coiled around me like this, there's nothing I can do...{w}\nIt's like I'm nothing but Alice's prey..."

    voice "audio/voice/alice9_31.ogg"

    a "Hehe, how unsightly...{w}\nWrithing like that in a woman's embrace... How embarrassing."

    voice "audio/voice/alice9_32.ogg"

    a "I'll lick you clean in my pussy, and make you look even more pathetic..."
    l "Ah...!{w} Ahhh!"
    "From the base to the tip, her tongue-like folds lick against me as her vaginal walls themselves squirm and wriggle."
    "Just like the name, it really is as if I'm stuck in a hell of folds..."

    voice "audio/voice/alice9_33.ogg"

    alipheese "Hoora... Finish as my folds rub against you...{w}\nHora, horahorahora..."
    l "N...No... I'm coming again... !{w}\nAhh!"

    call syasei1
    call syasei2

    "Unable to endure her vagina, I explode inside of Alice all while her folds continue rubbing against me."
    l "Ah...! Ah..."
    "My body shakes as I come inside her, suddenly overcome with intense weakness after the powerful orgasm."
    "Now even weaker, I'm left completely defenseless in Alice's embrace."

    voice "audio/voice/alice9_34.ogg"

    a "Hehe... It seems like your tip is quite sensitive...{w}\nNow then, I'll use a vaginal skill to play with just that tip..."
    l "S...Stop... Ahh!"
    "I feel the muscles in her vagina moving around again."
    "Something like a tiny tentacle starts to tickle my urethral opening.{w}\nSuddenly, more and more tiny tentacles start to surround my tip, focusing solely around my opening."
    l "Wh...What is this...?{w}\nN...Not just the tip... Ah..."

    voice "audio/voice/alice9_35.ogg"

    a "Hehe... This skill is called \"Playful Sea Anemone\".{w}\nA sea anemone hidden in my vagina will play with your penis..."
    l "That feels... Disgusting...{w} Ah!"
    "I feebly shake side to side as the tentacles play with my urethral opening."

    voice "audio/voice/alice9_36.ogg"

    a "At first, the \"Playful Sea Anemone\" will focus on your urethral opening...{w}\nSince you're a man, you can't endure an attack on the urethra."

    voice "audio/voice/alice9_37.ogg"

    a "Hoora, hora, hora hora..."
    l "Hyaaa!"
    "I yell out as the tiny tentacle crawls around over the tip of my penis, sometimes gently slipping inside and rubbing around right at the entrance."
    "My hands and feet flail around wildly at the sensations, but Alice just pins me back to the floor, keeping me still."

    voice "audio/voice/alice9_38.ogg"

    a "Hehe... It seems quite effective.{w}\nFor now, I'll finish you off with \"Umbrella Hole Kill\"."

    voice "audio/voice/alice9_39.ogg"

    a "Hoora, I'll slowly play with your urethral opening...{w}\nHorahora, hora... Hehe."
    l "Ahhh!!"
    "The most sensitive part on my body is gently being tortured by Alice."
    "As the tip of one of her tiny wet tentacles rubs around inside again, I feel myself about to come."
    l "Ahh... Alice... I'm going to come...{w}\nIt... It feels so good..."

    call syasei1
    call syasei2

    "I explode, shooting semen out as Alice's tentacle continues to rub against the hole it's shooting from."
    "My entire body feels like it could melt into pleasure itself as I come."

    voice "audio/voice/alice9_40.ogg"

    a "...What, already finished?{w}\nI knew it, your glans is quite sensitive..."

    voice "audio/voice/alice9_41.ogg"

    a "We have as much time as we need, so I'll thoroughly train it...{w}\nHoora, next is the frenulum..."
    l "Hyaa... Ah...!"
    "This time, the tiny tentacles start to rub against the back of my penis, right where my glans starts."
    "As she switches from sensitive spot to sensitive spot, I continue squirming in her embrace."
    l "Ahhh... N... No...{w}\nIf you do that... I'll come again...!"

    voice "audio/voice/alice9_42.ogg"

    a "Sheesh, you're weak here too?{w}\nYour penis is full of weak points, isn't it...?"

    voice "audio/voice/alice9_43.ogg"

    a "Horahora... I'm sure an ejaculation as I stimulate back here will feel amazing..."
    l "Ahhh...!"
    "The tiny tentacles focus solely on the back of my penis, not giving me even a moment's respite from the unending stimulation."
    l "Ahhh!"

    call syasei1
    call syasei2

    "As her tentacles massage the back of my penis, I come inside Alice again."

    voice "audio/voice/alice9_44.ogg"

    a "...Sheesh, an instant finish even with the back.{w}\nIt doesn't qualify as training like this..."

    voice "audio/voice/alice9_45.ogg"

    a "Next is going to be the neck of the penis, but I'm sure you'll come right away anyway...{w}\nSheesh, such a pitiful guy..."
    "The tentacles move from the back of my penis, this time slowly coiling around right under my glans."
    l "Ah...!"

    voice "audio/voice/alice9_46.ogg"

    a "Hehe, it feels good, doesn't it?"

    voice "audio/voice/alice9_47.ogg"

    a "This skill is particularly good at this kind of teasing...{w}\nI can make someone at your level come in no time..."
    l "Ah...ah...ah..."
    "The tentacles wrap tighter around the area right under the tip of my penis."
    "After they all latch on, they start to quickly move up and down, in very small movements."
    "I feebly press against Alice's body in an unconscious reaction, but there's still no getting away."
    l "Ahh!{w} Guh..."

    voice "audio/voice/alice9_48.ogg"

    a "Oh... It looks like you're enduring it for once.{w}\nBut in the end, you'll reach the same miserable result."

    voice "audio/voice/alice9_49.ogg"

    a "At this rate, training your penis is going to be quite difficult..."
    l "Ahh...!"
    "Just as she said, my ability to withstand this is already at its breaking point."
    "All I can feel are her tiny tentacles, rubbing quickly right underneath my glans.{w}\nUnable to take it any longer, I let my body relax."
    l "Ahh... Already... Coming..."

    voice "audio/voice/alice9_50.ogg"

    a "Sheesh... You can't even take this for a minute?{w}\nOh well, come as I play with your glans..."
    "Alice sighs as the tentacles move even faster, easily pushing me over the edge right on her command."
    l "Ahhh!!"

    call syasei1
    call syasei2

    "More of my semen flows into Alice as her tentacles keep massaging right under the tip of my penis."
    l "Haaa..."

    voice "audio/voice/alice9_51.ogg"

    a "...Now then, I'm going to torment your urethral opening, neck and frenulum all at the same time.{w}\nLet's see if you can withstand this \"Glans Killer\" for even a second."
    l "N...No... If you do it all at the same time..."

    voice "audio/voice/alice9_52.ogg"

    a "Hoora... Here I go...{w}\nHora, hora, hora..."
    l "Ahhhh!!"
    "Her tiny tentacles latch onto every part of the tip of my penis."
    "The tip of one slips into my urethral opening, as the others latch onto the back of my penis, and others right under the neck."
    "The moment all of them latch onto me, all the strength leaves my body as my vision goes white."
    l "Haaa..."

    voice "audio/voice/alice9_53.ogg"

    a "...Sheesh, broken in an instant.{w}\nSurrendering in seconds... Such a pitiful penis."
    l "Haa..."
    "As Alice looks at me with disgust in her eyes..."

    call syasei1
    show alice h09_4r #700
    call syasei2

    "I come inside her, shooting the proof of my submission into her."
    l "Ahh..."
    "Forced to yet another orgasm after only seconds, my face goes red in humiliation and ecstasy."

    voice "audio/voice/alice9_54.ogg"

    a "Hehe... Truly pitiful. But cute.{w}\nFrom now on, I'll carefully train you with my pussy..."

    voice "audio/voice/alice9_55.ogg"

    a "We have as much time as we want...{w}\nLet's continue having sex for eternity..."
    l "Ahh... Alice... Alice..."
    "I cling to Alice's warm body as I indulge in the ecstasy.{w}\nKept in her embrace, she fills me with blissful pleasure."

    voice "audio/voice/alice9_56.ogg"

    a "Hehe... I'll never let go...{w}\nYou'll have sex with me forever in this room..."

    voice "audio/voice/alice9_57.ogg"

    a "Just the two of us, forever...{w}\nForever... Forever..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus, the Hero and the Monster Lord continue having sex.{w}\nStuck in a closed off space for eternity, they never part again."
    "With them behind an unbreakable barrier, even Goddess Ilias cannot lay her hands on them.{w}\nWithout any interference, Ilias forcefully takes over the world and begins to re-create it..."
    "Thus, the world is brought to peace..."
    "................."

    jump badend

label granberia_ng_start:
    $ monster_name = "Гранберия"
    $ lavel = "granberia_ng"
    $ img_tag = "granberia"
    $ tatie1 = "granberia st41"
    $ tatie2 = "granberia st42"
    $ tatie3 = "granberia st07"
    $ tatie4 = "granberia st42"
    $ haikei = "bg 013"
    $ monster_pos = center
    $ earth_keigen = 50
    $ ng = 2
    $ granberia = 0

    if Difficulty == 1:
        $ max_enemylife = 32000
        $ henkahp1 = 8000
    elif Difficulty == 2:
        $ max_enemylife = 40000
        $ henkahp1 = 9000
    elif Difficulty == 3:
        $ max_enemylife = 52000
        $ henkahp1 = 10000
        $ earth_keigen = 25
    elif Difficulty == 0:
        $ max_enemylife = 1

    $ alice_skill = 0
    $ damage_keigen = 70
    $ kaihi = 80
    $ mus = 4
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "But Luka can't break free from her tail!"
    $ mogaku_anno2 = "Luka broke free!"
    $ mogaku_anno3 = "But Luka can't attack like this!"
    $ mogaku_sel1 = "It isn't that easy to escape from the Monster Lord..."
    $ mogaku_sel2 = "Will you be finished that easily...?"
    $ mogaku_sel3 = "You won't escape... I'll squeeze you more..."
    $ mogaku_sel4 = "What's wrong? Is this all you had...?"
    $ mogaku_sel5 = "What pitiful struggling... Hehe."
    $ mogaku_earth_dassyutu1 = "Gnome's power truly is strong..."
    $ mogaku_earth_dassyutu2 = "To think you could even break loose from me..."
    $ end_n = 9
    $ zmonster_x = -270
    $ tukix = 300
    $ owaza_count1a = 4
    $ owaza_count2a = 4
    $ owaza_count3a = 5
    call skillset(2)

    pause .5
    l "Тебе же лучше сдаться, Гранберия!{w}\nПрекрати нападать на невинных людей и захватывать города! В противном случае я буду вынужден применить силу!"
    g "Ты доволно самоуверен, не так ли?{w}\nЧто ж, посмотрим на что ты способен!"
    call show_skillname("Рассекающий удар")
    play sound "audio/se/karaburi.ogg"
    "Гранберия взмахивает своим мечом, кося всё на своём пути!"
    call show_skillname("Шаг в сторону")
    play sound "audio/se/miss.ogg"
    $ renpy.show(haikei, at_list=[miss2])
    "Но Лука легко уклоняется от удара просто отшагнув!"
    call hide_skillname
    show granberia st04 with dissolve
    g "Хмпф.{w}\nЭто сложнее, чем я думала..."
    l "Ты действительно надеялась, что это будет так легко?"
    show granberia st02 with dissolve
    g "Не зазнавайся!"
    show granberia st41 with dissolve
    g "Как тебе такое!?"
    call show_skillname("Удар Дракона-Мясника")
    play sound "audio/se/karaburi.ogg"
    "Меч Гранберии обрушивается на Луку!"
    pause .3
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.show(haikei, at_list=[miss2])
    "Но Лука уклоняется, словно поток воды, обегающий препятствие!"
    call hide_skillname
    show granberia st06 with dissolve
    g "Что...?{w} Это движение!"
    show granberia st07 with dissolve
    g "Где ты ему научился!?{w}{nw}"
    show granberia st41 with dissolve
    g "Не важно, хватит уже этих игр."
    show granberia st04 with dissolve
    call show_skillname("Безмятежный Демонический Клинок")
    play sound "audio/se/aqua2.ogg"
    "Гранберия вкладывает свой меч в ножны.{w}{nw}"
    show granberia st41
    call skill13a
    extend "\nИ резко вынимая из ножн, прорезает всё на своём пути!"
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.show(haikei, at_list=[miss2])
    "Но Лука едва уходит от атаки!"
    call hide_skillname
    show granberia st07 with dissolve
    g "!!!"
    l "Я не позволю тебе захватить Илиасбург!"
    call counter
    call show_skillname("Денница")

    "Ослепительная звезда обрушивается в ад!"

    call skill22a

    while count < 3:
        play sound "audio/se/damage2.ogg"
        $ damage = rnd2(3200,3600)
        if count == 0:
            window show
            show screen hp
            window auto
        $ damage_nobr = 2

        call enemylife2(count+1)

        $ count += 1

    $ count = 0

    call hide_skillname
    show granberia st06 with dissolve
    g "!!!{w}\nЭта сила...!"
    "На мгновение Гранберия застыла в неподвижной позе, шокированная моей атакой."
    show granberia st04 with dissolve
    stop music fadeout 1.0
    g "Хорошо.{w}\nЕсли ты так силён..."
    show granberia st41 with dissolve
    if Difficulty == 3:
        $ mus = 32
    else:
        $ mus = 20
    call musicplay
    g "Тогда время игр закончено, пацан!"
    call show_skillname("Ярость")
    play sound "audio/se/fire4.ogg"
    show fire with dissolve
    show granberia st42 with dissolve
    show effect 008 with dissolve
    $ tatie1 = "granberia st42"
    pause .5
    hide fire with dissolve
    hide effect 008 with dissolve
    "Языки пламени пробегают по мечу Гранберии, воспламеняя её дух!"
    $ damage = max_enemylife - enemylife
    call enemylife_kaihuku
    call hide_skillname
    g "Покажи свои лучшие приёмы, малыш!{w}\nТебе они понадобятся!"
    l "С удовольствием..."
    "Я до сих пор не совсем понимаю, что тут творится...{w}\nНо если мне снова придётся победить Гранберию, то так тому и быть!"
    if Difficulty == 3:
        $ nostar = 1
        $ skill01 = 26

    jump common_main


label granberia_ng_main:
    if result == 15:
        jump granberia_ng_edging

    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and aqua > 0:
        jump common_attack
    elif result == 1 and aqua < 1:
        jump granberia_ng_miss
    elif result == 2 and earth > 0 and genmogaku > 0:
        jump mogaku_earth1
    elif result == 2 and earth > 0 and genmogaku == 0:
        jump mogaku_earth2
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump granberia_ng_kousan
    elif result == 6:
        jump granberia_ng_onedari
    elif result == 7:
        jump common_skill

label granberia_ng_nostar:
    call granberia_ng_a10
    jump granberia_ng_main

label granberia_ng_edging:
    show granberia st02 with dissolve
    g "Что это, чёрт возьми, за приём такой!?"
    l "Ух-ох...{w}\nОбычно я получаю возможность снова атаковать после его использования..."
    "Возможно я как-то не продумал данный ход..."
    show granberia st04 with dissolve
    g "... Хорошо.{w}\nЕсли ты так хочешь этого..."
    show granberia st41 with dissolve
    g "Тогда я просто тебя убью!"
    play sound "audio/se/karaburi.ogg"
    show effect 006a zorder 30
    pause .3
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=15)
    $ mylife = 0
    pause .5
    stop music fadeout 1.0
    $ monocro("images/Backgrounds/bg 013.webp",204,0,0)
    $ monocro("images/Characters/granberia/granberia st41.webp",204,0,0)
    with dissolve
    "Я чувствую, как острый клинок пронзает насквозь моё тело. Моё сознание начинает постепенно угасать..."
    hide screen hp
    $ nobtn = 1
    scene bg black with Dissolve(2.0)
    $ bad1 = "Будучи полным придурком, коим он, собственно, и является, Лука попытался использовать крайность против Гранберии."
    $ bad2 = "Она просто убила его за это."
    jump badend

label granberia_ng_miss:
    $ before_action = 3
    $ ransu = rnd2(1,3)
    if ransu == 1:
        ori "Хаа!"
    elif ransu == 2:
        ori "Ораа!"
    elif ransu == 3:
        ori "Получай!"

    "Лука атакует!"
    call mp_kaihuku
    play sound "audio/se/karaburi.ogg"
    call kaihi

    "Но Гранберия легко уклоняется!"

    if first_sel3 == 1:
        jump granberia_ng_a

    l "Что за...?{w}\nЯ промахнулся!?"

    g "Ты действительно надеялся, что это будет так легко?{w}\nАтаки, подобно этой, сродни укусам комаров!"

    "Обычные атаки против неё бесползены...{w}\nМожет мне попробывать что-то ещё?"

    jump granberia_ng_a


label granberia_ng_miss2:
    $ before_action = 4
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori ".............."
    elif ransu == 2:
        ori "Вижу!"
    elif ransu == 3:
        ori "Вверяя свой меч потоку..."

    call show_skillname("Клинок Стоячей Воды")

    "Меч Луки, сверкая, направляется к противнику!"
    call mp_kaihuku
    play sound "audio/se/karaburi.ogg"
    call kaihi
    "Но Гранберия легко уклоняется!"

    jump granberia_ng_a

label granberia_ng_001:
    show granberia st07 with dissolve
    g "Это... просто нереально...!"
    g "Что ты за безумный демон такой!?"
    g "Кто-то с подобной силой сражается на стороне Илиас..."
    l "Я не сражаюсь за Илиас!"
    l "Просто сдайся, Гранберия!{w}\nЯ правда не хочу запечатывать тебя..."
    show granberia st02 with dissolve
    g "Ни за что... "
    show granberia st41 with dissolve
    extend "Я не могу оставить кого-то, подобно тебе, в живых!"
    show granberia st42 with dissolve
    call show_skillname("Ярость")
    play sound "audio/se/fire4.ogg"
    show fire with dissolve
    show granberia st42 with dissolve
    show effect 008 with dissolve
    pause .5
    "Языки пламени пробегают по мечу Гранберии, полностью восстанавливая её боевой дух!"
    hide fire with dissolve
    hide effect 008 with dissolve
    call hide_skillname
    l "!"
    "Она не собирается сдаваться...{w}\nОна всерьёз планирует драться до смерти одного из нас!{w}\nНеужели мне действительно придётся её запечатать...?"

    $ granberia = 1
    jump granberia_ng_a

label granberia_ng_v:
    if Difficulty == 3:
        $ persistent.granberia_hell = 1
    show granberia st07 with dissolve
    enemy "Нгх...{w}\nЧёрт бы тебя побрал...!"
    play sound "audio/se/down.ogg"
    "Гранберия обессиленно падает на колени."
    l "Прости меня, Гранберия, но я сумел защитить Илиасбург.{w}\nТвоя осада провалилась."
    l "Если ты согласна прекратить нападения на людей, я позволю тебе уйти."
    g "Никогда..."
    stop music fadeout 1.0
    a "Эй, идиоты."
    show alice st11b at xy(X=-133) #700
    show granberia st07 at xy(X=200)
    with dissolve
    a "Прекращайте."
    l "Алиса?"
    show alice st11b at xy(X=-133) #700
    show granberia st06 at xy(X=200)
    with dissolve
    g "!?{w} Т-ты!"

    $ persistent.count_ko += 1

    if mylife == max_mylife and nodamage == 0:
        $ persistent.count_hpfull = 1

    if Difficulty == 3:
        $ persistent.count_hellvic = 1

    if status == 7:
        $ persistent.count_vickonran = 1

    $ renpy.end_replay()
    $ persistent.granberia_ng_unlock = 1

    call victory(viclose=1)

    $ getexp = 1500000
    call lvup

    jump lb_0045


label granberia_ng_a:
    if enemylife < henkahp1 and granberia != 1:
        jump granberia_ng_001

    if enemylife == 0:
        jump granberia_ng_v

    if granberia != 1:

        while True:
            $ ransu = rnd2(1,9)

            if aqua == 0 and ransu < 3:
                call granberia_ng_a1
                jump common_main
            elif aqua == 0 and 2 < ransu < 5:
                call granberia_ng_a2
                jump common_main
            elif aqua == 0 and 4 < ransu < 7:
                call granberia_ng_a3
                jump common_main

            elif aqua > 0 and ransu < 2:
                call granberia_ng_a1
                jump common_main
            elif aqua > 0 and 3 < ransu < 5:
                call granberia_ng_a2
                jump common_main
            elif aqua > 0 and ransu < 7:
                call granberia_ng_a3
                jump common_main

            elif ransu == 7:
                call granberia_ng_a4
                jump common_main
            elif ransu == 8:
                call granberia_ng_a5
                jump common_main
            elif ransu == 9:
                call granberia_ng_a6
                jump common_main

    if granberia > 0:

        while True:
            $ ransu = rnd2(10,12)

            if aqua == 0 and ransu == 10:
                call granberia_ng_a7
                jump common_main
            elif aqua == 0 and ransu == 11:
                call granberia_ng_a8
                jump common_main
            elif aqua == 0 and ransu >= 12:
                call granberia_ng_a9
                jump common_main

            elif aqua > 0 and ransu == 10:
                call granberia_ng_a7
                jump common_main
            elif aqua > 0 and  ransu == 11:
                call granberia_ng_a9
                jump common_main
            elif aqua > 0 and ransu >= 12:
                call granberia_ng_a8
                jump common_main

label granberia_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ха!"
    elif ransu == 2:
        enemy "От атаки, подобно этой, легко увернуться!"
    elif ransu == 3:
        enemy "Просто лёгкий тычок..."

    call show_skillname("Рассекающий удар")
    play sound "audio/se/karaburi.ogg"
    "Гранберия взмахивает своим мечом, кося всё на своём пути!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 70+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 70+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 55+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (300, 350), 1: (300, 350), 2: (380, 410), 3: (600, 650)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как насчёт этого!?"
    elif ransu == 2:
        enemy "А вот и я!"
    elif ransu == 3:
        enemy "Сможешь ли ты увидеть эту атаку!?"

    call show_skillname("Техника Проклятого Меча: Обезглавливание")
    play sound "audio/se/tuki.ogg"

    "Гранберия быстро делает шаг вперёд и нацеливает мощный взмах мечом на шею Луки!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 75+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 75+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 55+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 15
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (500, 550), 1: (500, 550), 2: (600, 650), 3: (750, 800)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng_a3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Съешь это!"
    elif ransu == 2:
        enemy "Слишком медленно!"
    elif ransu == 3:
        enemy "Сможешь ли ты уследить за моими движениями!?"

    call show_skillname("Кровопускающий Громовой Выпад: Ураган")
    play sound "audio/se/karaburi.ogg"
    "Гранберия ступает вперёд, словно гром, и делает быстрый выпад!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 10+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 7+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 5+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (260, 320), 1: (260, 320), 2: (340, 400), 3: (400, 450)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хаа!"
    elif ransu == 2:
        enemy "Почувствуй всю мощь моего меча своим телом!"
    elif ransu == 3:
        enemy "Я не дам тебе ни единой поблажки!"

    call show_skillname("Атака Дракона-Мясника")
    play sound "audio/se/karaburi.ogg"
    "Меч Гранберии обрушивается на Луку!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 75+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 75+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 65+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ a_effect1 = 1

    $ dmg = {0: (700, 750), 1: (700, 750), 2: (800, 900), 3: (900, 1000)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng_a5:
    enemy "Со всей моей силой...!"

    call show_skillname("Демонический Крушитель Черепов: Очищение")
    window hide
    hide screen hp
    play sound "audio/se/karaburi.ogg"
    show effect 003 zorder 30 with dissolve
    pause 0.3
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    show screen hp
    window show
    play sound "audio/se/bom2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=25)
    window auto

    "Гранберия прыгает в воздух и обрушивается вниз!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 80+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 80+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 70+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Мощный удар обрушивается на голову Луки!"

    $ a_effect1 = 1
    $ dmg = {0: (800, 900), 1: (800, 900), 2: (950, 1050), 3: (1050, 1200)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng_a6:
    $ owaza_count3c = 0

    enemy "Я всверлю в тебя ужас от этого умения!"

    call show_skillname("Гибельный Клинок Звезды Хаоса")

    "Меч Гранберии блестит как хаотичная падающая звезда!"

    pause 0.5
    call skill6a
    $ ransu = rnd2(1,100)

    if aqua > 3 and Difficulty < 2 and ransu < 75:
        jump aqua_guard
    elif aqua > 3 and Difficulty == 2 and ransu < 75:
        jump aqua_guard
    elif aqua > 3 and Difficulty == 3 and ransu < 45:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Гибельный Клинок Звезды Хаоса")
    else:
        call damage((120, 150))

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Гибельный Клинок Звезды Хаоса")
    else:
        $ damage_nobr = 2
        call damage((120, 150))

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Гибельный Клинок Звезды Хаоса")
    else:
        call damage((120, 150))

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Гибельный Клинок Звезды Хаоса")
    else:
        call damage((120, 150))

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Гибельный Клинок Звезды Хаоса")
    else:
        $ damage_nobr = 2
        call damage((120, 150))

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng_a7:
    $ owaza_count3c = 0

    enemy "Мой наилучший приём... Неудержимый Испепеляющий Клинок!"

    call show_skillname("Неудержимый Испепеляющий Клинок")

    "Меч Гранберии замелькал, проводя бесчисленные атаки!"

    pause 0.5
    call skill16a

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 60+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 60+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 55+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        $ damage_kotei = 1
        call damage((210, 250), "first")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        $ damage_kotei = 1
        call damage((210, 250), "second")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        $ damage_kotei = 1
        call damage((210, 250), "last")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        $ damage_kotei = 1
        call damage((210, 250), "first")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 3

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "But Luka moves like the wind and dodges!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        $ damage_kotei = 1
        call damage((210, 250), "second")

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng_a8:
    $ ransu = rnd2(1,2)

    if ransu == 1:
        enemy "Следуя течению... Безмятежный Демонический Клинок!"
    elif ransu == 2:
        enemy "Я рассеку всё на своём пути... Безмятежный Демонический Клинок!"

    show granberia st04 with dissolve
    "Гранберия вкладывает свой меч в ножны.{w}{nw}"
    call show_skillname("Безмятежный Демонический Клинок")
    show granberia st42
    call skill13a

    extend "\nИ резко вытаскивая из ножн, прорезает всё на своём пути!"

    if aqua > 0:
        jump granberia_ng_a8a

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Атака Гранберии достигает Луку в мгновение ока!"

    play sound "audio/se/damage2.ogg"

    $ dmg = {0: (850, 1000), 1: (850, 1000), 2: (1050, 1200), 3: (1200, 1400)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng_a8a:
    hide ct with dissolve
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.show(haikei, at_list=[miss2])

    "Лука пытается уклониться!"
    extend "\nНо атака настигает его быстрее, чем он отреагировал!"

    $ dmg = {0: (450, 500), 1: (450, 500), 2: (550, 600), 3: (720, 750)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng_a9:
    enemy "Получай!"

    "Наполнив свой меч силой Земли, Гранберия обрушивает его на Луку!"

    call show_skillname("Сотрясающее Землю Обезглавливание")
    call skill12a

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 60+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 50+undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 45+undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Мощный удар обрушивается на голову Луки!"

    play sound "audio/se/damage2.ogg"

    $ dmg = {0: (1050, 1200), 1: (1050, 1200), 2: (1250, 1400), 3: (1800, 2200)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng_a10:
    window show
    show screen hp
    window auto

    call face(param=1)
    g "Против меня это не сработает дважды!"
    call counter
    "Гранберия подпрыгивает вверх к звезде!"
    play sound "audio/se/karaburi.ogg"
    hide granberia with dissolve
    pause .3
    play sound "audio/se/karaburi.ogg"
    show effect 006a zorder 30
    pause .3
    show effect 002 with ImageDissolve("images/System/mask01.webp", 0.5)
    pause .3
    play sound "audio/se/karaburi.ogg"
    hide effect with dissolve
    "Она ударяет своим пылающим лезвием прямо в звезду!"
    play sound "audio/se/bom2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "screens")
    play sound "audio/se/fire2.ogg"
    pause 3.0
    play sound "audio/se/power.ogg"
    call show_skillname("Звёздный Клинок")
    "Свет звезды резонирует с огнём на лезвиях!"
    "Сила света и огня сливаются воедино в мече Гранберии!"
    play sound "audio/se/power.ogg"
    pause 1.0
    play sound "audio/se/karaburi.ogg"
    "Наконец, она обрушивает его на Луку!"
    play sound "audio/se/bom2.ogg"
    pause .4
    play sound "audio/se/bom3.ogg"
    show bg white with flash
    pause .3
    $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "screens")
    $ a_effect1 = 1
    call damage((3200, 3600))
    call hide_skillname
    $ renpy.show(haikei)
    $ renpy.transition(Dissolve(1.5), layer="master")
    call face(param=1)
    play sound "audio/se/down.ogg"
    if mylife == 0:
        return_to badend_h
    if mylife > 0:
        $ persistent.starblade = 1
    play sound "audio/se/down.ogg"

    jump granberia_ng_main

label granberia_ng_kousan:
    "Если я сдамся, она сразу же меня прикончит..."
    jump granberia_ng_main


label granberia_ng_onedari:
    "Она не тот противник, кого я могу попросить об атаке!"
    jump granberia_ng_main


label granberia_ng_h1:
    play sound "audio/se/down.ogg"

    translator "А ты ожидал здесь увидеть хентай, да?{w}\nХентайные сцены я буду переводить в последнюю очередь!"

    $ monster_x = -200
    $ monster_y = 0
    $ end_n = 2
    $ bad1 = "sample text"
    $ bad2 = "sample text"
    jump badend

    # call lose(viclose=2)
    # call count_defeat(monster_name, "alice3")
    # call count(14, 3)
    # $ bad1 = "Alipheese's attack brought Luka to his knees."
    # call face(param=2)
    # jump granberia_ng_h


label granberia_ng_h:
    $ end_go = "lb_0300a"
    $ bad2 = "Luka spends an eternity having sex with Alice in the closed off space."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    hide bk
    show alice st04
    with dissolve #700

    if kousan != 0:
        a "Sheesh, you really are pitiful..."
    else:
        a "Did you truly fight seriously...?{w}\nNo, it doesn't matter anymore."

    "Alice begins to mutter a strange incantation...{w}\nThe throne room fills with power, as a strange atmosphere descends around us."
    "I hold my breath as it feels like the outside world itself is being cut off from us."
    l "Wh...What are you doing...?"

    show alice st01b with dissolve #700

    a "I created an unbreakable barrier around this room.{w}\nIt's no longer possible to enter or leave."
    l "N...No way..."
    "She created an unbreakable barrier...?{w}\nAnd inside of it, just the two of us remain..."
    a "With this, the Monster Lord vanished from the world.{w}\nSadly, so did the brave Hero who killed her."
    a "...Just a slight modification of the original scenario.{w}\nThe world should easily accept that kind of story, too."
    a "However... You can never escape from here again.{w}\nTogether with me, you shall be in here for eternity."
    l "Wh...Why did you...?"
    a "............."

    play sound "audio/se/ero_makituki3.ogg"

    "Alice slowly winds her tail around me, calmly taking off my clothes.{w}\nAfter stripping me naked, she pulls me close against her body."

    voice "audio/voice/alice9_01.ogg"

    a "...Luka, have sex with me.{w}\nWe shall spend eternity lusting after each other's bodies..."
    l "B...But I..."
    "Alice uses her tail to press me up right against her."

    voice "audio/voice/alice9_02.ogg"

    a "Hoora... It's the Monster Lord's pussy that took your virginity...{w}\nI'll use this to show you heaven again..."
    l "N...No, Alice...!{w}\nAhh!"
    "Ignoring me, Alice violates me by force."

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    show alice h09_1r with dissolve #700

    "My penis slowly sinks into her, the hot, wet flesh rubbing against me as I enter.{w}\nHer seductive touch is much different from when I felt her in her human form."
    "Warm juices fill her vagina, dripping out the deeper I go.{w}\nAt the same time, her soft flesh presses against my penis, rubbing me all the way inside."
    "Her folds eagerly twitch at the feeling of my glans as I press into her, all while she gently squeezes down on me."

    voice "audio/voice/alice9_03.ogg"

    a "Hoora, you're all the way inside..."
    l "Ahhh..."
    "The moment I'm all the way inside her, my body shakes as I seemingly go into a trance of pure ecstasy."

    call syasei1
    show alice h09_2r #700
    call syasei2

    "Having an instant orgasm, I come inside Alice as soon as I'm all the way in her."
    "Alice looks down at me and smiles as I go red from humiliation."

    voice "audio/voice/alice9_04.ogg"

    a "Hehe... Couldn't endure for ten seconds again?{w}\nMy pussy is an instant finish for you, I see..."

    voice "audio/voice/alice9_05.ogg"

    a "Even if I don't use a skill or anything, just insertion is enough...{w}\nThat really is a weak penis you have there..."
    l "Ahh..."
    "Even though I feel humiliated, with her powerful tail around me, I can't escape...{w}\nFrustration wells up in me as I can't even struggle."
    "As a result, I just feebly squirm in pleasure.{w}\nJust how long is Alice going to coil around me and tease me like this...?"

    voice "audio/voice/alice9_06.ogg"

    a "Hehe... What a pitiful look. You don't look like a Hero.{w}\nYou betrayed my hopes, and couldn't fulfill your duty as a Hero..."

    voice "audio/voice/alice9_07.ogg"

    a "I'll need to torment a miserable guy like you a lot...{w}\nHoora... Writhe in the Monster Lord's vagina techniques!"
    "Alice's vagina suddenly squeezes down around my penis, as if it was a snake coiling around its prey."
    l "Ah!{w} Ahhh!"

    voice "audio/voice/alice9_08.ogg"

    a "Hehe... Unbearable tightness, isn't it?{w}\nI can control my pussy at will, and even change its shape..."

    voice "audio/voice/alice9_09.ogg"

    a "I've mastered many techniques to bring a man to tears...{w}\nHora... Horahora... Like this..."
    l "Ahhh!!"
    "As my body is attacked by the sensations from the intense tightness, Alice presses me closer against her body"

    voice "audio/voice/alice9_10.ogg"

    a "Hehe... It looks like it feels good.{w}\nThat skill is called \"Serpent Choking\"..."

    voice "audio/voice/alice9_11.ogg"

    a "I skillfully use my vaginal muscles to tighten and give you a feeling of pressure...{w}\nIf you take this, someone like you wouldn't last long..."
    l "Ahhh!!"
    "I feel her muscles pressing into me from every direction, clamping down around my penis."
    "Without being painful at all, the unique sensations flood me with pleasure Alice hugs me closer."
    l "Ahh... I... I'm going to come..."

    voice "audio/voice/alice9_12.ogg"

    a "Hehe... You sure are fast...{w}\nNow come as the serpent squeezes down on you!"
    "Suddenly, she squeezes down on me from the base of my penis right to the tip, just as if there really was a snake inside of her constricting my penis."
    l "Ahh...!"
    "The serpent inside of her squeezes and presses down on me, forcing me over the edge."
    l "Ah... No more...{w}\nAhh!"

    call syasei1
    show alice h09_3r #700
    call syasei2

    "I explode inside of Alice, defeated by the clamping sensations inside of her."

    voice "audio/voice/alice9_13.ogg"

    a "Hehe... Such a quick surrender.{w}\nWhat a shameful man, letting so much leak out..."

    voice "audio/voice/alice9_14.ogg"

    a "I'll train that premature ejaculating penis of yours a little with my pussy...{w}\nWe have an eternity of time, after all..."
    l "Eh...?{w} Ahh!"
    "Suddenly, she loosens her vagina around me as the walls of her vagina start to squeeze and rub against my penis."
    "It's almost as if I'm stuck in the digestive organ of some creature...{w}\nOvercome by a completely new sensation, I feel the strength drain from my body again."

    voice "audio/voice/alice9_15.ogg"

    a "This skill is called the Sea Slug Wave...{w}\nThe name is poor, but it was created by a Monster Lord generations back..."

    voice "audio/voice/alice9_16.ogg"

    a "As you're feeling now, I make my pussy slowly move in a wave motion...{w}\nA skill to drive a man to orgasm with this strange movement..."

    voice "audio/voice/alice9_17.ogg"

    a "Horehore... It's unbearable, isn't it?{w}\nYou can't endure it, can you? You want to come, don't you?"
    l "Ahhh...!"
    "The wave motion in her vagina gets more and more intense, even going so far as to slightly bend my erect penis a little bit inside of her."
    "The tip of my penis rubs hard against her soft flesh as her walls push it around inside her, bringing unbearable pleasure with it."

    voice "audio/voice/alice9_18.ogg"

    a "Hehe... I know your penis is rejoicing at being inside my pussy."

    voice "audio/voice/alice9_19.ogg"

    a "You want to come inside me like this, don't you?{w}\nGo ahead and come, without holding anything back..."
    l "Ahh... It feels so good..."
    "My penis twitches inside of her as my body rejoices in bliss at the soft feeling of Alice's vagina around me."
    l "Ahhhh!!"

    call syasei1
    call syasei2

    "I climax as her wave motion continues, knocking my penis around inside of her even as I come."
    "Surrendering myself to the pleasure of her vagina, I softly sigh as her vagina continues rubbing against my penis as I come inside her."
    l "Ahh..."

    voice "audio/voice/alice9_20.ogg"

    a "Hehe... You surrendered after the third time...{w}\nI'll torment that surrendered penis of yours even more..."

    voice "audio/voice/alice9_21.ogg"

    a "Taste the Monster Lord's vaginal skills with your body..."
    "Alice's vagina suddenly gets smaller, as a slow suction sensation starts to pull at me inside her."
    "At the same time her vagina starts seemingly sucking on me, the vaginal walls start to squirm and wriggle around my penis."
    "It feels like my body itself could melt in pleasure as the suction slowly gets stronger and stronger."
    l "Haa..."

    voice "audio/voice/alice9_22.ogg"

    a "This skill is called \"Leech Suck\".{w}\nLike a leech sucking blood, I can suck semen out of your penis..."

    voice "audio/voice/alice9_23.ogg"

    a "Hoora, I'm going to suck the semen right out of your penis...{w}\nHora, hora, hora..."
    l "Ahhh!!"
    "The suction slowly gets more and more intense, causing her soft vaginal walls to press down hard against my penis."
    "Almost pulling my penis deeper inside, it really is as if her vagina is a giant leech, trying to forcibly suck semen out of my penis."
    l "No... I'm going to come again..."
    "I clench my stomach, trying to stop the coming orgasm.{w}\nAs a man, I can't just let myself be played with like this..."

    voice "audio/voice/alice9_24.ogg"

    a "Hehe, it's pointless to resist...{w}\nThis is a skill to forcibly suck out semen, after all."

    voice "audio/voice/alice9_25.ogg"

    a "Give up to the suction sensations, and allow me to suck out your semen..."
    l "Ahhh..."
    "Her vagina sucks hard on me like a mouth even as she teases me for my resistance."
    "Despite my best effort to hold on, the unrelenting stimulation slowly breaks down my defenses.{w}\nBefore long, I feel my lower body start to go numb in pleasure."
    l "Ahh... It... It feels good..."

    voice "audio/voice/alice9_26.ogg"

    a "Hehe, what truly short endurance...{w}\nHoora, I'll suck out the proof of your surrender!"
    "In the next instant, her vagina starts pulling on me even harder.{w}\nAlmost as if my soul itself was being sucked out, I feel myself pulled deeper into her as I'm pushed over the edge."
    l "Ahhh!!"

    call syasei1
    call syasei2

    "As I explode inside her, the powerful suction pulls out my semen even as I come.{w}\nGreedily sucking on my penis, the semen is pulled deeper inside Alice as she sucks on me with her vagina."
    "I shake in ecstasy as the suction continues, until every drop of my semen has been forcibly sucked out."
    l "Haa...{w}\nStop this already..."

    voice "audio/voice/alice9_27.ogg"

    a "Hehe... Did you think you would be freed from my pussy after just that?{w}\nOnce your penis is inside me, I'm not going to let go until I drain every drop of semen..."

    voice "audio/voice/alice9_28.ogg"

    a "Hoora, how about this skill?"
    "Suddenly, her vaginal walls start to move as the thousands of folds inside her press hard against my penis."
    "Like that, they all start moving up and down, as if they were thousands of tongues licking every inch of me."
    l "Ahhh!!"

    voice "audio/voice/alice9_29.ogg"

    a "Hehe... This is called \"Fold Hell\".{w}\nI can move each and every fold as I want..."

    voice "audio/voice/alice9_30.ogg"

    a "If I focus on the tip..."
    "The folds all converge around the tip of my penis, as if hundreds of tiny individual tongues are all licking my glans."
    l "Ahhh!!{w}\nStop it!"
    "I tremble in Alice's tail, still tightly holding me next to her as the overwhelming stimulation attacks me."
    "With her coiled around me like this, there's nothing I can do...{w}\nIt's like I'm nothing but Alice's prey..."

    voice "audio/voice/alice9_31.ogg"

    a "Hehe, how unsightly...{w}\nWrithing like that in a woman's embrace... How embarrassing."

    voice "audio/voice/alice9_32.ogg"

    a "I'll lick you clean in my pussy, and make you look even more pathetic..."
    l "Ah...!{w} Ahhh!"
    "From the base to the tip, her tongue-like folds lick against me as her vaginal walls themselves squirm and wriggle."
    "Just like the name, it really is as if I'm stuck in a hell of folds..."

    voice "audio/voice/alice9_33.ogg"

    alipheese "Hoora... Finish as my folds rub against you...{w}\nHora, horahorahora..."
    l "N...No... I'm coming again... !{w}\nAhh!"

    call syasei1
    call syasei2

    "Unable to endure her vagina, I explode inside of Alice all while her folds continue rubbing against me."
    l "Ah...! Ah..."
    "My body shakes as I come inside her, suddenly overcome with intense weakness after the powerful orgasm."
    "Now even weaker, I'm left completely defenseless in Alice's embrace."

    voice "audio/voice/alice9_34.ogg"

    a "Hehe... It seems like your tip is quite sensitive...{w}\nNow then, I'll use a vaginal skill to play with just that tip..."
    l "S...Stop... Ahh!"
    "I feel the muscles in her vagina moving around again."
    "Something like a tiny tentacle starts to tickle my urethral opening.{w}\nSuddenly, more and more tiny tentacles start to surround my tip, focusing solely around my opening."
    l "Wh...What is this...?{w}\nN...Not just the tip... Ah..."

    voice "audio/voice/alice9_35.ogg"

    a "Hehe... This skill is called \"Playful Sea Anemone\".{w}\nA sea anemone hidden in my vagina will play with your penis..."
    l "That feels... Disgusting...{w} Ah!"
    "I feebly shake side to side as the tentacles play with my urethral opening."

    voice "audio/voice/alice9_36.ogg"

    a "At first, the \"Playful Sea Anemone\" will focus on your urethral opening...{w}\nSince you're a man, you can't endure an attack on the urethra."

    voice "audio/voice/alice9_37.ogg"

    a "Hoora, hora, hora hora..."
    l "Hyaaa!"
    "I yell out as the tiny tentacle crawls around over the tip of my penis, sometimes gently slipping inside and rubbing around right at the entrance."
    "My hands and feet flail around wildly at the sensations, but Alice just pins me back to the floor, keeping me still."

    voice "audio/voice/alice9_38.ogg"

    a "Hehe... It seems quite effective.{w}\nFor now, I'll finish you off with \"Umbrella Hole Kill\"."

    voice "audio/voice/alice9_39.ogg"

    a "Hoora, I'll slowly play with your urethral opening...{w}\nHorahora, hora... Hehe."
    l "Ahhh!!"
    "The most sensitive part on my body is gently being tortured by Alice."
    "As the tip of one of her tiny wet tentacles rubs around inside again, I feel myself about to come."
    l "Ahh... Alice... I'm going to come...{w}\nIt... It feels so good..."

    call syasei1
    call syasei2

    "I explode, shooting semen out as Alice's tentacle continues to rub against the hole it's shooting from."
    "My entire body feels like it could melt into pleasure itself as I come."

    voice "audio/voice/alice9_40.ogg"

    a "...What, already finished?{w}\nI knew it, your glans is quite sensitive..."

    voice "audio/voice/alice9_41.ogg"

    a "We have as much time as we need, so I'll thoroughly train it...{w}\nHoora, next is the frenulum..."
    l "Hyaa... Ah...!"
    "This time, the tiny tentacles start to rub against the back of my penis, right where my glans starts."
    "As she switches from sensitive spot to sensitive spot, I continue squirming in her embrace."
    l "Ahhh... N... No...{w}\nIf you do that... I'll come again...!"

    voice "audio/voice/alice9_42.ogg"

    a "Sheesh, you're weak here too?{w}\nYour penis is full of weak points, isn't it...?"

    voice "audio/voice/alice9_43.ogg"

    a "Horahora... I'm sure an ejaculation as I stimulate back here will feel amazing..."
    l "Ahhh...!"
    "The tiny tentacles focus solely on the back of my penis, not giving me even a moment's respite from the unending stimulation."
    l "Ahhh!"

    call syasei1
    call syasei2

    "As her tentacles massage the back of my penis, I come inside Alice again."

    voice "audio/voice/alice9_44.ogg"

    a "...Sheesh, an instant finish even with the back.{w}\nIt doesn't qualify as training like this..."

    voice "audio/voice/alice9_45.ogg"

    a "Next is going to be the neck of the penis, but I'm sure you'll come right away anyway...{w}\nSheesh, such a pitiful guy..."
    "The tentacles move from the back of my penis, this time slowly coiling around right under my glans."
    l "Ah...!"

    voice "audio/voice/alice9_46.ogg"

    a "Hehe, it feels good, doesn't it?"

    voice "audio/voice/alice9_47.ogg"

    a "This skill is particularly good at this kind of teasing...{w}\nI can make someone at your level come in no time..."
    l "Ah...ah...ah..."
    "The tentacles wrap tighter around the area right under the tip of my penis."
    "After they all latch on, they start to quickly move up and down, in very small movements."
    "I feebly press against Alice's body in an unconscious reaction, but there's still no getting away."
    l "Ahh!{w} Guh..."

    voice "audio/voice/alice9_48.ogg"

    a "Oh... It looks like you're enduring it for once.{w}\nBut in the end, you'll reach the same miserable result."

    voice "audio/voice/alice9_49.ogg"

    a "At this rate, training your penis is going to be quite difficult..."
    l "Ahh...!"
    "Just as she said, my ability to withstand this is already at its breaking point."
    "All I can feel are her tiny tentacles, rubbing quickly right underneath my glans.{w}\nUnable to take it any longer, I let my body relax."
    l "Ahh... Already... Coming..."

    voice "audio/voice/alice9_50.ogg"

    a "Sheesh... You can't even take this for a minute?{w}\nOh well, come as I play with your glans..."
    "Alice sighs as the tentacles move even faster, easily pushing me over the edge right on her command."
    l "Ahhh!!"

    call syasei1
    call syasei2

    "More of my semen flows into Alice as her tentacles keep massaging right under the tip of my penis."
    l "Haaa..."

    voice "audio/voice/alice9_51.ogg"

    a "...Now then, I'm going to torment your urethral opening, neck and frenulum all at the same time.{w}\nLet's see if you can withstand this \"Glans Killer\" for even a second."
    l "N...No... If you do it all at the same time..."

    voice "audio/voice/alice9_52.ogg"

    a "Hoora... Here I go...{w}\nHora, hora, hora..."
    l "Ahhhh!!"
    "Her tiny tentacles latch onto every part of the tip of my penis."
    "The tip of one slips into my urethral opening, as the others latch onto the back of my penis, and others right under the neck."
    "The moment all of them latch onto me, all the strength leaves my body as my vision goes white."
    l "Haaa..."

    voice "audio/voice/alice9_53.ogg"

    a "...Sheesh, broken in an instant.{w}\nSurrendering in seconds... Such a pitiful penis."
    l "Haa..."
    "As Alice looks at me with disgust in her eyes..."

    call syasei1
    show alice h09_4r #700
    call syasei2

    "I come inside her, shooting the proof of my submission into her."
    l "Ahh..."
    "Forced to yet another orgasm after only seconds, my face goes red in humiliation and ecstasy."

    voice "audio/voice/alice9_54.ogg"

    a "Hehe... Truly pitiful. But cute.{w}\nFrom now on, I'll carefully train you with my pussy..."

    voice "audio/voice/alice9_55.ogg"

    a "We have as much time as we want...{w}\nLet's continue having sex for eternity..."
    l "Ahh... Alice... Alice..."
    "I cling to Alice's warm body as I indulge in the ecstasy.{w}\nKept in her embrace, she fills me with blissful pleasure."

    voice "audio/voice/alice9_56.ogg"

    a "Hehe... I'll never let go...{w}\nYou'll have sex with me forever in this room..."

    voice "audio/voice/alice9_57.ogg"

    a "Just the two of us, forever...{w}\nForever... Forever..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus, the Hero and the Monster Lord continue having sex.{w}\nStuck in a closed off space for eternity, they never part again."
    "With them behind an unbreakable barrier, even Goddess Ilias cannot lay her hands on them.{w}\nWithout any interference, Ilias forcefully takes over the world and begins to re-create it..."
    "Thus, the world is brought to peace..."
    "................."

    jump badend

label queenharpy_ng_start:
    $ monster_name = "Королева Гарпий"
    $ lavel = "queenharpy_ng"
    $ img_tag = "queenharpy"
    $ tatie1 = "queenharpy st13"
    $ tatie2 = "queenharpy st24"
    $ tatie3 = "queenharpy st22"
    $ tatie4 = "queenharpy st23"
    $ haikei = "bg 027"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 25000
    elif Difficulty == 2:
        $ max_enemylife = 30000
        $ henkahp1 = 11000
        $ henkahp2 = 5000
    elif Difficulty == 3:
        $ max_enemylife = 35000

    $ alice_skill = 0
    $ damage_keigen = 90
    $ kaihi = 100
    $ mus = 0
    $ ng = 2
    $ nocharge = 1
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ end_n = 3
    $ exp_minus = 150
    $ zmonster_x = -270
    $ tukix = 300

    play music "NGDATA/bgm/boss.ogg"
    queen_harpy "Я в самом деле хочу изнасиловать тебя."
    l "Подожди-подожди, что?"
    "Этого не было раньше..."
    $ tatie1 = "queenharpy st21"
    play sound "audio/se/escape.ogg"
    show queenharpy st23 with dissolve
    "... Этого определённо раньше не было!"
    queen_harpy "Пришла пора показать тебе истинную силу Королевы Гарпий!"
    play sound "audio/se/bird.ogg"
    "Королева Гарпий призывает силу ветра!"
    play sound "audio/se/wind2.ogg"
    show effect wind zorder 30 #1
    pause 1.0
    hide effect with dissolve
    if Difficulty < 3:
        $ enemy_wind = 2
        $ enemy_windturn = 6
    else:
        $ enemy_wind = 1
        $ enemy_windturn = 99
        $ skill01 = 26

    call face(param=1)
    l "Гах..."
    "Этот бой будет намного труднее, чем раньше.{w}\nПолагаю, у меня не остаётся выбора!"
    call skillset(2)
    "Я неохотно снимаю кольцо матери и встаю в боевую стойку."
    jump common_main

label queenharpy_ng_main:
    $ ng = 2
    if enemy_wind == 0 and enemy_windturn > 0:
        $ enemy_windturn = 0

    if result == 44 and enemy_wind > 0 and daten < 1 and hanyo[0] < 2:
        $ enemy_windturn = 0
        call enemy_element_sel
        $ enemy_windturn = 1
    elif result == 44 and enemy_wind > 0 and daten < 1 and hanyo[0] > 1:
        $ enemy_windturn = 0
        call enemy_element_sel
        $ enemy_windturn = 2

    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and enemy_wind > 0:
        jump queenharpy_ng_miss
    elif result == 1 and enemy_wind < 1:
        jump common_attack
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5 and sinkou == 1:
        jump queenharpy_ng_kousan
    elif result == 6 and sinkou == 1:
        jump queenharpy_ng_onedari
    elif result == 7:
        jump common_skill

label queenharpy_ng_miss:
    l "Хаа!"
    "Лука атакует!"

    call mp_kaihuku
    pause .5
    play sound "audio/se/bird.ogg"
    hide queenharpy
    pause .5
    call face(param=2)

    "Но Королева Гарпий двигается подобно ветру и уворачивается!"
    if sinkou == 0:
        call queenharpy_ng001

    call face(param=1)

    jump queenharpy_ng_a

label queenharpy_ng001:
    l "Что за!?"
    "Она просто взлетела в воздух!"
    "И эта скорость...!{w}\nДолжно быть она быстрее даже Альмы Эльмы!"
    show queenharpy st23
    queen_harpy "Хаха.{w}\nИ это все на что ты способен?"
    l "Дерьмо..."
    "У меня нет Сильфы или Гномы с собой, чтобы противостоять ей.{w}\nЧто же мне делать...?"
    return

label queenharpy_ng002:
    call face(param=1)

    queen_harpy "Ты довольно силён."
    "Королева Гарпий смотрит на меня удивлённым взглядом."
    queen_harpy "Однако, я не позволю тебе победить меня!"

    call show_skillname("Магическая концентрация")
    play sound "audio/se/power.ogg"
    "Королева Гарпий концентрирует вокруг себя магическую энергию!"
    call hide_skillname
    l "Гх...!"
    "Я улавливаю странную энергию. слабо исходящую от Королевы Гарпий."
    if aqua > 0:
        extend "\nБезмятежный разум не работает!"

    "Мне нужно что-то сделать!{w}\nНо что...?"

    $ owaza_count1a = 0
    $ hanyo[0] = 1

    jump common_main

label queenharpy_ng_v:
    if Difficulty == 3:
        $ persistent.queenhapy_hell = 1

    call part_achi
    call face(param=3)

    $ persistent.count_ko += 1

    if mylife == max_mylife and nodamage == 0:
        $ persistent.count_hpfull = 1

    if Difficulty == 3:
        $ persistent.count_hellvic = 1

    if status == 7:
        $ persistent.count_vickonran = 1

    $ renpy.end_replay()

    $ persistent.queenharpy_ng_unlock = 1

    queen_harpy "Ах!{w}\nОн слишком силён...!"
    "Нанеся последний удар, я заставил Королевы Гарпий пасть на землю.{w}\nНе похоже, что у ней осталось сил сопротивляться."
    call victory(viclose=1)
    $ getexp = 950000
    call lvup
    call skillset(1)
    jump lb_0064

label queenharpy_ng_a:
    if kousoku == 0:
        $ sinkou = 0

    if result == 17 and skill01 < 26 and enemy_wind > 1:
        $ enemy_windturn = 0
        call enemy_element_sel
        $ enemy_windturn = 1

    elif result == 17 and skill01 > 25 and Difficulty == 3 and enemy_wind > 1:
        $ enemy_windturn = 0
        call enemy_element_sel
        $ enemy_windturn = 1

    if hanyo[0] == 1:
        call queenharpy_ng_a12
        jump common_main

    if enemylife < max_enemylife/2 and hanyo[0] < 1:
        jump queenharpy_ng002

    if enemy_wind == 0:
        $ hanyo[2] -= 1

    if enemy_wind > 0:
        $ hanyo[2] = 3

    if kousoku > 0 and sinkou == 0:
        call queenharpy_ng_a6
        jump common_main

    elif kousoku > 0 and sinkou == 1:
        call queenharpy_ng_a8
        jump common_main

    elif kousoku > 0 and sinkou == 2:
        call queenharpy_ng_a9
        jump common_main

    elif kousoku > 0 and sinkou == 3:
        call queenharpy_ng_a10
        jump common_main

    while True:

        if Difficulty < 3:
            $ ransu = rnd2(1,9)

        elif Difficulty == 3:
            $ ransu = rnd2(1,11)

        if ransu == 1 and enemylife < max_enemylife/2 and hanyo[4] < 1:
            call queenharpy_ng_a5
            jump common_main

        elif ransu == 2:
            call queenharpy_ng_a1
            jump common_main

        elif ransu == 3:
            call queenharpy_ng_a2
            jump common_main

        elif ransu == 4:
            call queenharpy_ng_a3
            jump common_main

        elif ransu == 5:
            call queenharpy_ng_a4
            jump common_main

        elif Difficulty < 3 and ransu == 6 and hanyo[0] == 2:
            call queenharpy_ng_a12
            jump common_main

        elif Difficulty < 3 and ransu > 6 and enemy_wind < 1 and enemy_windturn < 1 and hanyo[2] < 1:
            call queenharpy_ng_a11
            jump common_main

        elif Difficulty == 3 and ransu > 9 and enemy_wind < 1 and enemy_windturn < 1 and hanyo[2] < 1:
            call queenharpy_ng_a11
            jump common_main

        elif Difficulty == 3 and ransu > 5 and hanyo[0] == 2:
            call queenharpy_ng_a12
            jump common_main

label queenharpy_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Некоторые человеческие мужчины такие странные..."
    elif ransu == 2:
        enemy "Не волнуйся, я не обижу тебя..."
    elif ransu == 3:
        enemy "Это не больно... возможно."

    call show_skillname("Футджоб гарпии")
    play sound "audio/se/ero_koki1.ogg"

    "Королева гарий наступает на член Луки!"

    $ dmg = {0: (260, 280), 1: (260, 280), 2: (300, 320), 3: (500, 550)}
    call damage(dmg[Difficulty])

    if enemy_wind > 0:

        $ dmg = {0: (260, 280), 1: (260, 280), 2: (300, 320), 3: (500, 550)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label queenharpy_ng_a2:
    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Некоторые человеческие мужчины такие странные..."
    elif ransu == 2:
        enemy "Не волнуйся, я не обижу тебя..."
    elif ransu == 3:
        enemy "Это не больно... возможно."

    call show_skillname("Минет гарпии")
    play sound "audio/se/ero_buchu3.ogg"

    "Королева гарий сосёт член Луки!"

    $ dmg = {0: (220, 240), 1: (220, 240), 2: (260, 280), 3: (450, 470)}
    call damage(dmg[Difficulty])

    if enemy_wind > 0:

        $ dmg = {0: (220, 240), 1: (220, 240), 2: (260, 280), 3: (450, 470)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label queenharpy_ng_a3:
   #call skillcount2(3027)

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты ведь не против моих крыльев...?"
    elif ransu == 2:
        enemy "Насладись моими перьями..."
    elif ransu == 3:
        enemy "Ощути наслаждение от моих перьев..."

    call show_skillname("Массаж перьями")
    play sound "audio/se/umaru.ogg"

    "Королева гарий трётся своими перьями о Луку!"

    $ dmg = {0: (250, 270), 1: (250, 270), 2: (290, 310), 3: (490, 560)}
    call damage(dmg[Difficulty])

    if enemy_wind > 0:

        $ dmg = {0: (250, 270), 1: (250, 270), 2: (290, 310), 3: (490, 560)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label queenharpy_ng_a4:
    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 2:
        enemy "Насладись моей грудью..."
    elif ransu == 3:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 4:
        enemy "Просто выплести всё на мою грудь..."
    elif ransu == 5:
        enemy "Предайся наслаждению..."

    call show_skillname("Королевское пайзури")
    play sound "audio/se/ero_koki1.ogg"

    "Королева Гарпий трётся своими сиськами о член Луки!"

    $ dmg = {0: (250, 290), 1: (250, 290), 2: (310, 330), 3: (520, 540)}
    call damage(dmg[Difficulty])

    if enemy_wind > 0:

        $ dmg = {0: (250, 290), 1: (250, 290), 2: (310, 330), 3: (520, 540)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label queenharpy_ng_a5:
    enemy "Может станем немного... ближе?"

    call show_skillname("Бросок наземь")
    play sound "audio/se/down.ogg"

    "Королева Гарпий прижимает Луку к земле!"

    if daten > 0:
        jump skill22x

    $ kousoku = 1
    call status_print(face=1)

    "Королева Гарпий сидит верхом на Луке!"

    call hide_skillname

    l "Гх..."
    "На этот раз она удерживает мои руки в захвате.{w}\nЯ не могу атаковать в таком положении!"
    call face(param=2)
    enemy "Ты удостоился чести спариться с Королевой.{w}\nРазве ты не счастлив?"
    enemy "Выплесни всю свою сперму в мою матку..."
    $ kousoku = 1
    $ genmogaku = 1

    jump common_main

label queenharpy_ng_a6:
    $ sinkou += 1
    if kousan != 2:
        enemy "Время спариваться..."

    $ queenhapy_rape += 1

    call show_skillname("Королевское изнасилование")
    play sound "audio/se/ero_pyu5.ogg"

    show queenharpy h1
    show queenharpy ct01 as ct
    with dissolve

    "Королева Гарпий садится на член Луки, насильно вставляя его внутрь!"

    $ kousoku = 2
    $ damage_kotei = 1

    $ dmg = {0: (600, 650), 1: (600, 650), 2: (600, 650), 3: (900, 950)}
    call damage(dmg[Difficulty])

   #call skillcount2(3029)

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    enemy "Хи-хи... ну как тебе киска Королевы?{w}\nПохоже твоему члену это нравится..."

    return

label queenharpy_ng_a8:
    $ sinkou += 1
   #call skillcount2(3029)
    "Лука был изнасилован Королевой Гарпий!"

    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 2:
        enemy "Насладись моей грудью..."
    elif ransu == 3:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 4:
        enemy "Просто выплести всё на мою грудь..."
    elif ransu == 5:
        enemy "Предайся наслаждению..."

    call show_skillname("Королевская киска")
    play sound "audio/se/ero_chupa4.ogg"

    "Королева Гарпий двигает своей талией!"

    $ damage_kotei = 1
    call damage((600, 650))

    call hide_skillname
    if mylife == 0:
        return_to badend_h

    return

label queenharpy_ng_a9:
    $ sinkou += 1
   #call skillcount2(3030)
    "Лука был изнасилован Королевой Гарпий!"

    python:
        while True:
            ransu = rnd2(1,5)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 2:
        enemy "Насладись моей грудью..."
    elif ransu == 3:
        enemy "Хи-хи... Как насчёт этого?"
    elif ransu == 4:
        enemy "Просто выплести всё на мою грудь..."
    elif ransu == 5:
        enemy "Предайся наслаждению..."

    call show_skillname("Королевская наездница")
    play sound "audio/se/ero_chupa3.ogg"

    "Королева Гарпий начинает быстро скакать на Луке!"

    $ damage_kotei = 1
    if Difficulty < 3:
        call damage((850, 900))
    else:
        call damage((1200, 1300))

    call hide_skillname
    if mylife == 0:
        return_to badend_h

    return

label queenharpy_ng_a10:
    enemy "Хм, пора бы уже заканчивать с этим."

    call show_skillname("Королевские объятия")
    play sound "audio/se/ero_chupa3.ogg"

    "Королева Гарпий прижимает Луку так сильно, как это вообще возможно, заставляя его кончить!"

    $ damage_kotei = 1
    call damage((1300, 1400))

    call hide_skillname
    if mylife == 0:
        return_to badend_h

    return


label queenharpy_ng_a11:
    enemy "..........................."

    call show_skillname("Ураган")
    play sound "audio/se/wind2.ogg"

    show effect wind zorder 30 #1
    pause 1.0
    hide effect with dissolve

    "Королева Гарпий призывает силу ветра!"

    if Difficulty == 3 or kousan > 0:
        $ enemy_wind = 1
        $ enemy_windturn = 99

    else:
        $ enemy_wind = 2
        $ enemy_windturn = 6

    call hide_skillname

    return

label queenharpy_ng_a12:
    $ hanyo[0] = 2
    enemy "Посмотри в мои глаза..."

    call show_skillname("Гипноз")
    play sound "audio/se/flash.ogg"

    show bg white with flash
    pause .3
    $ renpy.show(haikei)
    $ renpy.transition(Dissolve(1.5), layer="master")

    "Глаза Королевы Гарпий вспыхивают белым светом!"

    if daten > 0:
        jump skill22x

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 101
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 101
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 101

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ status = 4
    call status_print

    "Лука сдаётся Королеве Гарпий!"

    #call skillcount2(8500)

    l "Ах!{w}\nЧто за!?"
    enemy "Хи-хи...{w}\nТеперь ты подчинишься любому моему приказу.{w}\nЯ могу делать с тобой всё, что захочу."
    show queenharpy st23 with dissolve
    enemy "Отныне ты моя личная игрушка!"

    call show_skillname("Бросок наземь")
    play sound "audio/se/down.ogg"

    "Королева Гарпий прижимает Луку к земле!"

    $ kousoku = 1
    call status_print(face=1)

    "Королева Гарпий обвивает Луку своими мягкими перьями!"
    "После чего садиться своей промежностью на его рот."
    call hide_skillname

    enemy "А теперь... лижи."
    l "Хах..."
    "Несмотря на то, что мой разум пытается сопротивляться, тело покорно выполняет её команду."
    call show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"
    "Язык Луки проскальзывает сквозь перья Королевы Гарпий и входит в её киску!"

    $ dmg = {0: (450, 500), 1: (450, 500), 2: (450, 500), 3: (450, 500)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    l "Мммпхм..."
    enemy "Продолжай..."

    call show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa5.ogg"

    "Язык Луки лижет грубые стенки влагалища Королевы!"

    $ dmg = {0: (450, 500), 1: (450, 500), 2: (450, 500), 3: (450, 500)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    enemy "Ара, дай мне тебе помочь..."

    call show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_pyu1.ogg"
    "Королева Гарпий начинает тереться своей промежностью о рот Луки!"

    extend "\nПосле чего прижимает своими крыльями голову Луки к себе!"

    $ dmg = {0: (450, 500), 1: (450, 500), 2: (450, 500), 3: (450, 500)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    enemy "Глубже..."

    call show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"

    "Королева опускает бёдра ещё сильнее из-за чего язык Луки походит глубже в неё!{w}\nОн начинает лизать её клитор!"

    $ dmg = {0: (450, 500), 1: (450, 500), 2: (450, 500), 3: (450, 500)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    "Жидкость из киски Королевы Гарпий начала стекать Луке в рот!"
    play sound "audio/se/ero_chupa4.ogg"

    $ dmg = {0: (150, 200), 1: (150, 200), 2: (150, 200), 3: (150, 200)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    enemy "Ммм...{w}\nПришло время закончить это..."
    call show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"
    "Королева Гарпий прижимает лицо Луки так сильно, насколько это вообще возможно!"

    $ dmg = {0: (850, 900), 1: (850, 900), 2: (850, 900), 3: (850, 900)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    "Ещё больше смазки начинает стекать в рот Луки!"

    $ dmg = {0: (350, 400), 1: (350, 400), 2: (350, 400), 3: (350, 400)}
    call damage(dmg[Difficulty])

    if mylife < 1:
        $ mylife = 1
        return_to badend_h

    return_to badend_h

label queenharpy_ng_chargecancel:
    call face(param=3)

    enemy "Оу нет, не выйдет!"
    call counter
    call show_skillname("Отмена разгона")
    "Королева Гарпий резко бросается к Луке и начинает ласкать его!"

    $ dmg = {0: (250, 300), 1: (250, 300), 2: (350, 400), 3: (450, 500)}
    call damage(dmg[Difficulty])

    "Лука теряет концентрацию!"
    call hide_skillname
    call face(param=1)

    if hanyo[1] < 1:
        $ hanyo[1] = 1
        l "Ах!{w}\nЧто...?"
        enemy "Ты действительно думаешь, что я настолько глупа?{w}\nТы слишком медленный, чтобы проворачивать такие трюки со мной, мальчик."

    jump queenharpy_ng_a

label alma_ng_start:
    $ monster_name = "Альма Эльма"
    $ lavel = "alma_ng"
    $ img_tag = "alma_elma"
    $ tatie1 = "alma_elma st51"
    $ tatie2 = "alma_elma st52"
    $ tatie3 = "alma_elma st53"
    $ tatie4 = "alma_elma st54"
    $ haikei = "bg darkbeach"
    $ monster_pos = center

    if Difficulty == 1 and mylv > 81:
        $ max_enemylife = 28000
        $ henkahp1 = 14000
    elif Difficulty == 2 and mylv > 81:
        $ max_enemylife = 32000
        $ henkahp1 = 16000
    elif Difficulty == 3 and mylv > 81:
        $ max_enemylife = 50000
        $ henkahp1 = 25000

    elif Difficulty == 1 and mylv < 82:
        $ max_enemylife = 24000
        $ henkahp1 = 14000
    elif Difficulty == 2 and mylv < 82:
        $ max_enemylife = 26000
        $ henkahp1 = 16000
    elif Difficulty == 3 and mylv < 82:
        $ max_enemylife = 40000
        $ henkahp1 = 25000

    $ alice_skill = 0
    $ damage_keigen = 85
    $ kaihi = 80
    $ mus = 28
    call musicplay
    $ ng = 2
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ end_n = 3
    $ exp_minus = 150
    $ zmonster_x = -270
    $ tukix = 300

    alma "Уфу-фу-фу...{w}\nЯ собираюсь изнасиловать тебя, малыш Лука..."
    "На лице Альмы застыла непристойная ухмылка.{w}\nЕё явно забавляет вся эта ситуация..."
    alma "Ау...{w}\nНе смотри на меня так серьёзно, Лука..."
    alma "Если ты выиграешь, я позволю тебе пройти..{w}{nw}"
    call face(param=2)
    extend "\n... Но если ты проиграешь, будешь высушен досуха."
    call face(param=1)
    alma "Без разницы, что в итоге будет, все останутся в выигрыше."
    l "..................."
    "Ну, так и так, но мне пришлось бы драться с ней.{w}\nОна одна из Небесных Рыцарей, которых я должен победить!"
    call skillset(2)
    $ enemy_wind = 1
    $ enemy_windturn = 9999
    jump common_main

label alma_ng_main:
    if result == 15:
        jump alma_ng_edging

    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and aqua > 0:
        jump common_attack
    elif result == 1:
        jump alma_ng_miss
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5 and sinkou == 1:
        jump alma_ng_kousan
    elif result == 6 and sinkou == 1:
        jump alma_ng_onedari
    elif result == 7:
        jump common_skill

label alma_ng_edging:
    call face(param=2)
    enemy "Ох, так ты не хочешь сражаться?{w}\nХочешь просто сдаться?"
    enemy "Что ж, это тоже неплохо...{w}\n В таком случае я покажу тебе бонус..."
    if kousoku == 0:
        call alma_ng_a9
    call alma_ng_a10
    call alma_ng_vaginalhell
    $ bad1 = "Лука использовал крайность против Альмы, став её секс-рабом."
    $ bad2 = "... Чёртов изврат."
    jump badend

label alma_ng_miss:
    $ before_action = 3
    $ ransu = rnd2(1,3)
    if ransu == 1:
        l "Хаа!"
    elif ransu == 2:
        l "Хья!"
    elif ransu == 3:
        l "Ора!"
    "Лука атакует!"

    call mp_kaihuku
    pause .5
    play sound "audio/se/wind2.ogg"
    hide alma_elma
    pause .5
    call face(param=1)

    "Но Альма Эльма легко уворачивается!"

    alma "Слишком медленно, малыш Лука..."

    "Дерьмо...{w}\nНе думаю, что в таком духе я по ней попаду..."

    $ first_sel3 = 1

    jump alma_ng_a

label alma_ng_v:
    call face(param=3)

    $ persistent.count_ko += 1

    if mylife == max_mylife and nodamage == 0:
        $ persistent.count_hpfull = 1

    if Difficulty == 3:
        $ persistent.count_hellvic = 1

    if status == 7:
        $ persistent.count_vickonran = 1

    $ renpy.end_replay()

    $ persistent.alma_ng_unlock = 1

    alma "... Ауч."
    alma "Похоже что... ты победил, малыш Лука."
    play sound "audio/se/down.ogg"
    "Альма Эльма падает на землю без сознания!"
    hide alma_elma with dissolve
    call victory(viclose=1)
    $ getexp = 1750000
    call lvup
    call skillset(1)
    jump lb_0092

label alma_ng_a:
    $ hanyo[4] -= 1
    $ hanyo[3] += 1
    if enemylife < 1:
        jump alma_ng_v

    if kousoku > 0 and hanyo[4] < 1:
        call alma_ng_a10
        jump common_main
    elif kousoku > 0 and hanyo[4] > 0:
        call alma_ng_a11
        jump common_main

    if mp > 7:
        $ ransu = rnd2(1,100)

        if ransu < 31:
            call alma_ng_a1
            jump common_main

    if mp > 4 and enemylife < (max_enemylife / 2):
        $ ransu = rnd2(1,100)

        if ransu < 41:
            call alma_ng_a1
            jump common_main

    while True:
        $ ransu = rnd2(1,10)

        if ransu == 1 and mp > 2:
            call alma_ng_a1
            jump common_main

        elif ransu == 2:
            call alma_ng_a2
            jump common_main

        elif ransu == 3 and hanyo[3] > 1:
            call alma_ng_a3
            jump common_main

        elif ransu == 4:
            call alma_ng_a4
            jump common_main

        elif ransu == 5:
            call alma_ng_a5
            jump common_main

        elif ransu == 7:
            call alma_ng_a7
            jump common_main

        elif ransu == 8 and hanyo[4] < 1:
            call alma_ng_wind
            jump common_main

        elif ransu == 9 and enemylife < (max_enemylife / 2):
            call alma_ng_wind2
            jump common_main

        elif ransu == 10 and kousoku < 1 and status == 0:
            call alma_ng_a9
            jump common_main

label alma_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты будешь просто моей едой..."
    elif ransu == 2:
        enemy "Я могу использовать хвост..."
    elif ransu == 3:
        enemy "Это просто предварительная ласка..."

    call show_skillname("Хвост Суккуба: Поглощение Магии")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    if daten > 0:
        jump skill22x

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 3 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Её хвост засасывает член Луки внутрь!{w}\nТаинственная сила истощает его!"

    if Difficulty == 1:
        $ ransu = rnd2(2,4)

    elif Difficulty == 2:
        $ ransu = rnd2(3,5)

    elif Difficulty == 3:
        $ ransu = rnd2(6,8)

    play sound "audio/se/down2.ogg"
    $ mp -= ransu
    if mp < 0:
        $ mp = 0

    "Лука теряет [ransu] SP!"

    call damage((100,150))

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a2:
    if first_sel1 == 0:
        $ first_sel1 = 1
    python:
        while True:
            ransu = rnd2(1,2)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как насчёт этого?"
    elif ransu == 2:
        enemy "А вот и я!"

    call show_skillname("Хвост Суккуба: Поглощение Здоровья")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Её хвост начинает сильно сосать член Луки!"

    $ dmg = {0: (370, 400), 1: (370, 400), 2: (410, 450), 3: (460, 510)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_wind:
    $ hanyo[4] = 1

    alma "Я ослаблю тебя своим хвостом..."
    play sound "audio/se/tuki.ogg"

    call show_skillname("Хвост Суккуба: Поглощение Уровня")

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    if daten > 0:
        jump skill22x

    "Хвост начинает сосать член Луки!{w}\nУровень Луки был поглащён!"

    play sound "audio/se/down2.ogg"

    if Difficulty == 1:
        $ drain = 3
        call lvdrain

    elif Difficulty == 2:
        $ drain = 4
        call lvdrain

    elif Difficulty == 3:
        $ drain = 5
        call lvdrain

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if mylv == 1:
        return_to badend_h

    return

label alma_ng_wind2:
    $ hanyo[0] = 2

    alma "Не хочешь немного поиграть...?"

    call show_skillname("Феромоны Суккуба")
    play sound "audio/se/wind1.ogg"

    "Опьяняющий аромат воздействует на сознание Луки!"

    if daten > 0:
        jump skill22x

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 101
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 101
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 101

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ status = 1
    $ status_turn = 2

    "Лука поддаётся искушнию Альмы!"

    return

label alma_ng_a3:
   #call skillcount2(3027)

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хе-хе... Ты собираешься испачкать мою руку своей спермой?"
    elif ransu == 2:
        enemy "Я не могу ничего поделать с желанием подразнить его.{image=note}"
    elif ransu == 3:
        enemy "Хе-хе... Приятно, правда?"

    call show_skillname("Умелые ручки")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма дотрагивается своими руками до члена Луки!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    $ dmg = {0: (450, 470), 1: (450, 470), 2: (500, 520), 3: (550, 570)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Приёмы моих рук заставят тебя корчиться и биться в экстазе..."
    elif ransu == 2:
        enemy "Ни один человек не может стерпеть моих пальчиков..."
    elif ransu == 3:
        enemy "Что ты об этом думаешь, а? Это ручное мастерство Королевы Суккубов..."

    call show_skillname("Древние Техники Суккубов: Змея")
    play sound "audio/se/ero_koki1.ogg"

    "Все десять пальчиков Альмы пробегаются верх-вниз по члену Луки!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Её пальчики трогают ползают по члену Луки будто змея!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nЕё десять пальцев извиваются по всему члену!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nПосле чего она начинает выжимать его!"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a5:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хорошенько испробуй мастерство ротика самой Королевы Суккубов..."
    elif ransu == 2:
        enemy "Я высосу из тебя всё, хочешь ты этого или нет..."
    elif ransu == 3:
        enemy "Я усосу тебя в подчинение..."

    call show_skillname("Дух Романус Тери")
    play sound "audio/se/ero_buchu3.ogg"

    "Альма Эльма берёт член Луки в рот и начинает сосать!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Её язык начинает вылизывать весь член Луки!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nМягкий язык Альмы сдавливает член Луки!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nОбвив его своим мокрым языком, Альма Эльма начинает сильно сосать член Луки!"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a6:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Даже мои груди - оружие..."
    elif ransu == 2:
        enemy "Мои груди приносят сказочное наслаждение..."
    elif ransu == 3:
        enemy "Я утоплю тебя в моих грудях..."

    call show_skillname("Мело Софи Теллус")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма сдавливает член Луки между своими большими сиськами!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    "Зефирные прикосновения окружают Луку со всех сторон!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nАльма Эльма медленно сжимает свои сиськи вокруг члена Луки!{w}{nw}"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    extend "\nПосле чего она начинает двигать своей грудью вверх-вниз!"

    $ dmg = {0: (170, 190), 1: (170, 190), 2: (200, 230), 3: (260, 290)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a7:
    $ owaza_count3a = 0
    alma "Погрузись в агонию удовольствия от моего секретного навыка!"

    call show_skillname("Церемония Суккуба")

    "Альма Эльма хватает Луку сзади!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    # call skillcount2(3636)
    play sound "audio/se/ero_koki1.ogg"

    "Обхватив Луку сзади, она начинает поглаживать его член!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_paizuri.ogg"

    extend "\nБыстро сбивая Луку с ног, она обхватывает его член своими сиськами и начинает сдавливать его!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_sigoki1.ogg"

    extend "\nКогда Лука пытается встать, Альма Эльма быстро вскакивает и, наступая на его член, прижимает вновь к земле!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_koki1.ogg"

    extend "\nПосле чего, она нажимает сильнее своей ногой!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_simetuke2.ogg"

    extend "\nНаклонившись над ним, она сжимает член Луки тыльной стороной своего бедра!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    play sound "audio/se/ero_sigoki1.ogg"

    extend "\nТряся ими из стороны в сторону, она продолжает сжимать член Луки между своих бёдер!{w}{nw}"

    $ dmg = {0: (90, 110), 1: (90, 110), 2: (93, 113), 3: (96, 116)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng_a8:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Даже мои груди - оружие..."
    elif ransu == 2:
        enemy "Мои груди приносят сказочное наслаждение..."
    elif ransu == 3:
        enemy "Я утоплю тебя в моих грудях..."

    call show_skillname("Совращающий поцелуй")
    play sound "audio/se/ero_chupa2.ogg"

    "Альма Эльма соприкасается своими губами с губами Луки!{w}{nw}"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 3 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    extend "\nПосле чего, её влажный язык проскальзывает в рот Луки!{w}{nw}"

    if daten > 0:
        call skill22x
        jump common_main

    "После поцелуя развратное ощущение охватывает тело Луки!"

    #call skillcount2(3637)
    if status == 0:
        "Лука был загипнотизирован Альмой!"
    elif status == 1:
        "Лука загипнотизирован..."

    $ status = 5
    call status_print

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    l "Хаа..."
    enemy "Хорошие ощущение, не правда ли?{w}\nПозволь мне сделать тебе ещё лучше..."
    if Difficulty < 2:
        $ status_turn = rnd2(3,4)
    elif Difficulty == 2:
        $ status_turn = rnd2(4,5)
    elif Difficulty == 3:
        $ status_turn = rnd2(5,6)

    return

label alma_ng_a9:
    $ owaza_count1b = 0
    $ owaza_num1 = 2
    if kousan != 2:
        alma "Хочешь стать моей добычей...?"

    call show_skillname("Удержание Суккуба")

    "Альма Эльма опрокидывает Луку на землю и напрыгивает на него!"

    if daten > 0:
        jump skill22x

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 101 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 101 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 99 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 1
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "But Luka moves like the wind and dodges!"
            jump wind_guard

    # call skillcount2(3636)
    $ tatie4 = "alma_elma hd0"
    $ kousoku = 1
    $ genmogaku = 2

    "Альма Эльма крепко прижимается к Луке!"

    call hide_skillname

    if kousan == 2:
        return

    if first_sel2 != 1:
        if surrender < 1:
            alma "А теперь пора изнасиловать тебя...{w}{nw}"

        elif surrender > 0:
            alma "А теперь пора изнасиловать тебя..."
            call alma_ng_a10

        extend "\nЕсли ты не будешь сопротивляться, я покажу тебе рай удовольствий."
        alma "Ну а если будешь, я исушу тебя без капли жалости.{w}\nИтак... что же ты будешь делать?"

    else:
        alma "Ты вновь собираешься вырываться...?{w}\nИли решил погрузиться в удовольствие?"

    $ first_sel2 = 1
    if earth == 0:
        $ genmogaku = 1
    elif earth > 0:
        $ genmogaku = 0

    $ mogaku_sel1 = "Если таков твой выбор, то больше я тебя не отпущу..."
    $ mogaku_sel2 = "Если таков твой выбор, то больше я тебя не отпущу..."
    $ mogaku_sel3 = "Какой разочаровывающий выбор..."
    $ mogaku_sel4 = "Какой разочаровывающий выбор..."
    $ mogaku_sel5 = "Какой разочаровывающий выбор..."

    return

label alma_ng_a10:
    "Альма Эльма восседает верхом на Луке..."
    if kousan != 2:
        enemy "Так ты сдаёшься... в таком случае я погружу тебя в пучину наслаждений.{w}\nОщущения изменяться в зависимости от того, насколько глубоко ты войдёшь в мою киску."
        enemy "Чем глубже, тем быстрее ты сойдёшь с ума...{w}\nЭто ультимативная техника, называемая - Пять Кругов Вагинального Ада..."
        enemy "Ты будешь одним из немногих, кто ощутит это...{w}\nХе-хе... интересно, на каком из этапов ты сойдёшь с ума?"

    call show_skillname("Пять Кругов Вагинального Ада: Первая Тюрьма")
    play sound "audio/se/ero_pyu1.ogg"
    show alma_elma hd1 with dissolve
    "Альма Эльма медленно опускается на член Луки, вводя его кончик в свою вагину!{w}{nw}"
    $ damage_kotei = 1
    call damage((400,450))
    call hide_skillname
    "Лука был изнасилован Альмой!"
    if mylife == 0:
        return_to badend_h
    # l "Аххх!"
    # enemy "Хе-хе... это только начало.{w}\nЧем дальше, тем более сильное наслаждение тебя ждёт."
    # enemy ""
    call show_skillname("Пять Кругов Вагинального Ада: Вторая Тюрьма")
    play sound "audio/se/ero_slime1.ogg"
    $ damage_kotei = 1
    call damage((400,450))
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    call show_skillname("Пять Кругов Вагинального Ада: Третья Тюрьма")
    play sound "audio/se/ero_name2.ogg"
    if mylife == 0:
        return_to badend_h
    $ damage_kotei = 1
    call damage((400,450))
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    call show_skillname("Пять Кругов Вагинального Ада: Четвёртая Тюрьма")
    play sound "audio/se/ero_chupa5.ogg"
    $ damage_kotei = 1
    call damage((400,450))
    call hide_skillname
    if mylife == 0:
        return_to badend_h

label alma_ng_vaginalhell:
    call show_skillname("Пять Кругов Вагинального Ада: Пятая Тюрьма")
    play sound "audio/se/ero_makituki3.ogg"
    "Альма Эльма медленно опускается, полностью вводя член Луки в себя!{w}\nШейка её матки открывается и сжимается вокруг кончика члена Луки!"
    $ damage_kotei = 1
    call damage((400,450))
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    if surrender > 0:
        jump alma_ng_vaginalhell

    $ kousoku = 1
    $ genmogaku = 0
    $ mogaku_dassyutu1 = "...!?"
    $ mogaku_dassyutu2 = "Что за хуйня!?"
    jump common_main

label sylph_ng_start:
    if skill04 > 0:
        $ skill04 = 3
    $ ng = 2
    $ support = 0
    $ nostar = 0
    $ monster_name = "Сильфа"
    $ lavel = "sylph_ng"
    $ img_tag = "sylph"
    $ tatie1 = "sylph st51"
    $ tatie2 = "sylph st54"
    $ tatie3 = "sylph st53"
    $ tatie4 = "sylph st52"
    $ haikei = "bg 061"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 8000
    elif Difficulty == 2:
        $ max_enemylife = 10000
    elif Difficulty == 3:
        $ max_enemylife = 12000

    $ alice_skill = 0
    $ damage_keigen = 20
    $ earth_keigen = 40
    $ kaihi = 80
    $ mus = 27
    $ tikei = 2
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ half_s1 = "Хе-хе-хе..."
    $ half_s2 = "Ты уже почти проигра-а-ал, Лука~{image=note}!"
    $ kiki_s1 = "Ты уже всё!?"
    $ kiki_s2 = "Т-а-а-ак быстро~{image=note}!"
    $ end_n = 4
    $ exp_minus = 4150
    $ zmonster_x = -280

    l "Сильфа!?{w}\n... Что за херня!?"
    "Она выглядит как-то по другому... прямо как в пустыне, когда её сила была запечатана.{w}\n... Но в отличии от того раза, сейчас я чувствую куда большую силу, исходящую от неё."
    "Подождите-ка минутку...{w}\nНеужели...?"
    "Она уменьшила меня!?"
    l "Что за фигню ты творишь!?"
    show sylph st54 with dissolve
    sylph "Ха-ха...{w}\nЯ запечатала часть твоей магической энергии и передала её себе."
    l "... Ты...{w}\nЧТО!?"
    show sylph st51 with dissolve
    sylph "Не волнуйся! Это легко отменить!{w}\n... Если ты победишь меня, конечно же."
    l "Ты не можешь так делать!{w}\nЭто...{w} это читерство!"
    show sylph st54 with dissolve
    sylph "Хе-хе-хе...{w}\nТогда пойди и останови меня!"
    show sylph st51 with dissolve
    l ".............."
    "Дерьмово.{w}\nПолагаю это её способ проверить мои силы в этот раз."
    "... Хотя я более чем уверен, что она просто хочет изнасиловать меня."
    l "Хорошо...{w}\nТогда я сражусь с тобой!"
    sylph "Ну тогда погна-а-али~{image=note}!"
    play sound "audio/se/wind2.ogg"
    "Сильфа призывает силу ветра!"
    show effect wind with dissolve
    pause 1.0
    hide effect with dissolve
    $ enemy_wind = 1
    $ enemy_windturn = 9999
    l "Уоа..."
    "Вокруг нас задул сильный ветер!"
    call hanyo_null
    call skillset(2)
    jump common_main

label sylph_ng_main:
    call cmd("attack")

    if result == 1 and aqua > 0 and hanyo[0] > 0:
        jump common_attack
    elif result == 1 and aqua > 0 and hanyo[0] < 1:
        jump sylph_ng001
    elif result == 1 and aqua < 1:
        jump sylph_ng002
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump sylph_ng_kousan
    elif result == 6:
        jump sylph_ng_onedari
    elif result == 7:
        jump common_skill

label sylph_ng001:
    call attack
    l "Что...?{w}\nЧто это!?"
    l "Это весь урон, который я способен нанести!?"
    sylph "Хе-хе-хе...{w}\nТеперь ты не сможешь сильно атаковать!"
    sylph "Но так как я не лучший боец по сравнению с другими духами, мы уравнены в силах!"
    l "... Хмпф."
    "Какой же геморр."
    $ hanyo[0] = 1
    jump common_attack_end

label sylph_ng002:
    l "Хаа!"
    "Лука атакует!"
    call mp_kaihuku
    pause .5
    play sound "audio/se/wind2.ogg"
    hide alma_elma
    pause .1
    call face(param=2)
    "Но Сильфа двигается подобно ветру и уклоняется!"
    if hanyo[1] < 1:
        call sylph_ng003

    call face(param=1)
    jump common_attack_end

label sylph_ng003:
    l "Вот чёрт!"
    sylph "Ты правда думал, что сможешь таким ударом попасть по мне!?{w}\nЭто было та-а-ак медленно~{image=note}!"
    call face(param=1)
    sylph "... Но я чувствую, что ты владеешь техникой безмятежного разума.{w}\nПочему бы тебе не попробовать воспользоваться им?"
    $ hanyo[1] = 1
    return

label sylph_ng_v:
    "... Я победил её!?"
    enemy "............."
    call face(param=3)
    enemy "Уааааааааа!{w} Ты такой жестокий!"
    "Лука заставил Сильфу плакать!"
    l "Что!? Я!?{w}\nЭто ты всё это начала!"
    enemy "Но тебе не нужно было так сильно бить меня!{w}\nЯ просто хотела немного тебя изнасиловать!"
    l "!?"
    "\"Просто\"?{w}\n\"Немного\"?{w}\nЧто!?"
    l "Так теперь это моя вина!?{w}\nЯ даже не хочу сражаться с тобой!"
    enemy "{i}Сопит{/i}{w}\nУааааааа..."
    enemy "Я извиняюсь...{w}\nЯ не знала, что ещё делать..."
    enemy "Вот.{w}\nТы можешь забрать свою силу назад..."
    play sound "audio/se/power.ogg"
    "Сильфа начинает излучать слабую энергию!"
    play sound "audio/se/flash.ogg"
    scene color white with dissolve
    l "Ах!"
    "Внезапно, мой взор застилает пелена!"
    show bg 061 with dissolve
    "Сработало?{w}\nДа! Теперь я снова нормально размера!"
    call victory(viclose=1)
    $ getexp = 900000
    call lvup
    jump lb_0127

label sylph_ng_earth:
    $ hanyo[1] = 1
    if hanyo[4] == 0:
        sylph "Эй, ты уже получил Гноми!{w}\nХотя это тебе не поможе-е-ет~{imate=note}!"
    elif hanyo[4] == 1:
        sylph "Э-э-э..."
    elif hanyo[4] == 2:
        sylph "Г-Гноми...?"
    $ hanyo[4] += 1
    return

label sylph_ng_aqua:
    $ hanyo[2] = 1
    if hanyo[4] == 0:
        sylph "Эй, Дини вместе с тобой!{w}\nПриветики, Дини!"
    elif hanyo[4] == 1:
        sylph "Ты получил и Дини?{w}\nХмм..."
    elif hanyo[4] == 2:
        sylph "Д-Дини...?"
    $ hanyo[4] += 1
    return

label sylph_ng_fire:
    $ hanyo[3] = 1
    if hanyo[4] == 0:
        sylph "Ты уже получил Мэнди?{w}\nПриветики, Мэнди!"
    elif hanyo[4] == 1:
        sylph "Ещё и Мэнди?{w}\nСерьёзно...?"
    elif hanyo[4] == 2:
        sylph "М-Мэнди...?"
    $ hanyo[4] += 1
    return

label sylph_ng_v2:
    $ sylph_rep -= 2
    show sylph st55 with dissolve
    stop music fadeout 1.0
    enemy "............."
    show sylph st56 with dissolve
    enemy "В-а-а-а-а-а!"
    "............."
    play music "audio/bgm/comi1.ogg"
    "Лука заставил Сильфу плакать!"
    l "А, что?{w}\nЧто я сделал!?"
    enemy "Я ведь тебе даже не нужна!{w}\nТы сначала собрал остальных духов и только потом пришёл за мной!"
    l "Э-э-э!?{w}\nНо... это вовсе не так!"
    sylph "{i}сопит{/i}{w}\nУ-а-а-а..."
    "Сильфа потеряла боевой дух!"
    enemy "Вот.{w}\nТы можешь забрать свою силу назад..."
    play sound "audio/se/power.ogg"
    "Сильфа начинает излучать слабую энергию!"
    play sound "audio/se/flash.ogg"
    scene color white with dissolve
    l "Ах!"
    "Внезапно, мой взор застилает пелена!"
    show bg 061 with dissolve
    "Сработало?{w}\nДа! Теперь я снова нормально размера!"
    call victory(viclose=1)
    $ getexp = 900000
    call lvup
    jump lb_0127b

label sylph_ng_a:
    if hanyo[1] < 1 and earth > 0 and skill03 > 0:
        call sylph_ng_earth
    elif hanyo[2] < 1 and aqua > 0 and skill04 > 0:
        call sylph_ng_aqua
    elif hanyo[3] < 1 and fire > 0 and skill05 > 0:
        call sylph_ng_fire
    if hanyo[4] == 3 and skill05 > 0 and skill04 > 0 and skill03 > 0:
        jump sylph_ng_v2

    while True:
        if kousan == 1:
            jump sylph_ng_a5
        if kousoku < 1:
            $ sinkou = 0

        $ ransu = rnd2(1,5)

        if ransu == 1 and (enemylife < max_enemylife / 2) and hanyo[5] < 1:
            call sylph_ng_a5
            jump common_main
        if ransu == 2:
            call sylph_ng_a1
            jump common_main
        if ransu == 3:
            call sylph_ng_a2
            jump common_main
        if ransu == 4:
            call sylph_ng_a3
            jump common_main
        if ransu == 5:
            call sylph_ng_a4
            jump common_main

label sylph_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты серьёзно кончишь от этого!?"
    elif ransu == 2:
        enemy "Люди такие странные~{image=note}!"
    elif ransu == 3:
        enemy "Тебе нравиться, когда не тебя наступа-а-ают~{image=note}?"

    call show_skillname("Нога Ветряного Духа")
    play sound "audio/se/ero_koki1.ogg"
    "Сильфа наступает на пах Луки!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 2 and ransu < 76 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 36 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty < 2 and ransu < 21 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 11 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Нога Ветряного Духа")

    else:
        $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
        call damage(dmg[Difficulty])

    #call skillcount1(3025)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "Она хватает член Луки своими пальцами!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "После чего, сжимает его своими мягкими ступнями!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label sylph_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я буду сосать его~{image=note}!"
    elif ransu == 2:
        enemy "Я попробую твою смерму~{image=note}!"
    elif ransu == 3:
        enemy "Сосём-сосём-сосём~{image=note}!"

    call show_skillname("Минет Ветряного Духа")
    play sound "audio/se/ero_buchu3.ogg"
    "Сильфа берёт член Луки в свой рот и начинает сосать его!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 2 and ransu < 76 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 36 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty < 2 and ransu < 21 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 11 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Минет Ветряного Духа")

    else:
        $ dmg = {0: (180, 200), 1: (180, 200), 2: (220, 240), 3: (450, 470)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    "Она заглатывает его член глубже и начинает неистово сосать!"
    $ dmg = {0: (290, 310), 1: (290, 310), 2: (330, 350), 3: (550, 590)}
    call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    "Её влажный язык начинает лизать яички Луки!"
    $ dmg = {0: (240, 280), 1: (240, 280), 2: (280, 300), 3: (490, 530)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label sylph_ng_a3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Скачем, скачем, скачем~{image=note}!"
    elif ransu == 2:
        enemy "Мои бёдра очень мягкие..."
    elif ransu == 3:
        enemy "Разве теперь мои бёдра не восхитительны~{image=note}?"

    call show_skillname("Сжатие Ветряного Духа")
    play sound "audio/se/ero_paizuri.ogg"
    "Сильфа зажимает член Луки меж своих мягких бёдер!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 2 and ransu < 76 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 36 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty < 2 and ransu < 21 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 11 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Минет Ветряного Духа")

    else:
        $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    "После чего, она начинает двигать ими вверх и вниз!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    "Наконец, она сжимает свои бёдра так сильно, насколько может!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label sylph_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как насчёт моей груди~{image=note}?"
    elif ransu == 2:
        enemy "Погляди, они стали больше~{image=note}!"
    elif ransu == 3:
        enemy "Это заставит тебя кончить~{image=note}!"

    call show_skillname("Пайзури Ветряного Духа")
    play sound "audio/se/ero_koki1.ogg"
    "Сильфа зажимает член Луки меж своих сисек!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 2 and ransu < 76 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 36 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty < 2 and ransu < 21 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 11 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Минет Ветряного Духа")

    else:
        $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "После чего, она сдавливает свою грудь вокруг его члена!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "После чего начинает двигать своей грудью вверх-вниз вместе с его членом!"
    $ dmg = {0: (230, 250), 1: (230, 250), 2: (270, 290), 3: (520, 540)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label sylph_ng_a5:
    $ hanyo[5] = 1
    if kousan != 2:
        enemy "Я собираюсь покончить с этим, Лука~{image=note}!"
    
    call show_skillname("Удержание Ветрянного Духа")
    "Сильфа берёт Луку в захват!"
    if aqua > 0:
        jump aqua_guard 
    if daten > 0:
        call skill22x
        jump common_main
    $ kousoku = 1
    call status_print
    "Сильфа прижимает его к земле!"

    sylph "Попался~{image=note}!"
    l "Гах..."
    sylph "Я не позволю тебе вырваться~{image=note}!"
    $ kousoku = 1
    $ genmogaku = 1
    jump sylph_ng_a6

label sylph_ng_a6:
    $ sinkou += 1
    if kousan != 2:
        enemy "Я победила..."

    call show_skillname("Киска Ветрянного Духа")
    play sound "audio/se/ero_pyu5.ogg"
    show sylph hb1 with dissolve
    "Сильфа насильно вставляет член Луки в себя!"
    $ kousoku = 2
    $ damage_kotei = 1
    call damage((900,950))
    call hide_skillname
    if mylife == 0:
        jump badend_h
    "Лука был изнасилован Сильфой!"
    l "Аххх!"
    enemy "Мммм...{w}\nТак приятно..."
    enemy "Я хочу ещё..."
    jump sylph_ng_a8

label sylph_ng_a8:
    $ sinkou += 1

    call show_skillname("Киска Ветрянного Духа")
    play sound "audio/se/ero_chupa4.ogg"
    show sylph hb1 with dissolve
    "Сильфа двигает своими бёдрами ввер-вниз!"
    $ kousoku = 2
    $ damage_kotei = 1
    call damage((900,1000))
    call hide_skillname
    if mylife == 0:
        jump badend_h
    "Лука был изнасилован Сильфой!"
    enemy "Хе-хе-хе...{w}\nТы ведь уже скоро кончишь, так?"
    enemy "Последний рывок!"
    jump sylph_ng_a9

label sylph_ng_a9:
    $ sinkou += 1

    call show_skillname("Киска Ветрянного Духа")
    play sound "audio/se/ero_chupa3.ogg"
    show sylph hb1 with dissolve
    "Сильфа прыгает на члене Луки со всей силы!"
    $ kousoku = 2
    $ damage_kotei = 1
    call damage((850,900))
    call hide_skillname
    if mylife == 0:
        jump badend_h
    "Лука был изнасилован Сильфой!"
    enemy "Хмм?{w}\nТы всё ещё держишься?"
    enemy "Я не позволю тебе вырва-а-аться~{image=note}!"
    jump sylph_ng_a9

label gnome_ng_start:
    $ ng = 2
    $ skill01 = 27
    $ nostar = 0
    call skillset(2)
    if skill04 > 0:
        $ skill04 = 3
    $ monster_name = "Гнома"
    $ lavel = "gnome_ng"
    $ img_tag = "gnome"
    $ tatie1 = "gnome st11"
    $ tatie2 = "gnome st13"
    $ tatie3 = "gnome st12"
    $ tatie4 = "gnome st23"
    $ haikei = "bg 076"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 14000
        $ henkahp1 = 13500
    elif Difficulty == 2:
        $ max_enemylife = 14500
        $ henkahp1 = 13500
    elif Difficulty == 3:
        $ max_enemylife = 15500
        $ henkahp1 = 13500

    $ alice_skill = 0
    $ damage_keigen = 30
    $ earth_keigen = 0
    if Difficulty == 3:
        $ earth_keigen = 15
    $ kaihi = 90
    $ mus = 27
    $ tikei = 3
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = ".........................."
    $ mogaku_sel2 = ".........................."
    $ mogaku_sel3 = ".........................."
    $ mogaku_sel4 = ".........................."
    $ mogaku_sel5 = ".........................."
    $ half_s1 = ".........................."
    $ half_s2 = ".........................."
    $ kiki_s1 = "....?"
    $ kiki_s2 = ".........................."
    $ end_n = 6
    $ exp_minus = 57000
    $ zmonster_x = -320
    $ tukix = 350
    $ owaza_count1a = 4
    $ owaza_count1b = 5
    $ owaza_count1c = 5
    "Похоже, мне всё равно было не избежать этого боя."
    "Как и у Сильфа, у Гномы не должно быть реального боевого опыта.{w}{nw}"
    if skill02 > 0:
        "... Но Сильфа сыграла со мной злую шутку, запечатав силу. Что же планирует Гнома...?"
    else:
        "Это не должно быть слишком сложным!"
    gnome "............................"
    play sound "audio/se/earth2.ogg"
    $ enemy_earth = 1
    $ enemy_earthturn = 9999
    "Гнома взывает к силе земли!{w}\nЗащита Гномы сильно увеличена!"
    jump common_main

label gnome_ng_main:
    if kousoku < 1:
        hide c701
        hide c702
        hide c703
        hide c704
        with dissolve
    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and hanyo[0] > 0:
        jump common_attack
    elif result == 1 and hanyo[0] < 1:
        jump gnome_ng001
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump gnome_ng_kousan
    elif result == 6:
        jump gnome_ng_onedari
    elif result == 7:
        jump common_skill

label gnome_ng_kousan:
    if surrender < 1:
        call kousan_syori
    enemy "...?"
    call face(param=2)
    enemy ".............."
    $ daten = 0
    $ aqua = 0
    jump gnome_ng_a6

label gnome_ng001:
    call attack
    if enemylife < 1:
        jump gnome_ng_v
    l "... Чёрт."
    "Гнома использовала силу земли, поэтому её тело намного прочнее, чем обычно.{w}\nДолжен быть какой-то другой способ нанести ей урон..."
    $ hanyo[0] = 1
    jump gnome_ng_a

label gnome_ng_v:
    hide c701
    hide c702
    hide c703
    hide c704
    with dissolve
    show gnome st14 with dissolve
    gnome "!!!"
    hide gnome with dissolve
    play sound "audio/se/down.ogg"
    "Гнома падает на песок.{w}\nПохоже она всё ещё в сознании..."
    "Отлично!{w}\nДумаю, я победил!"
    call victory(viclose=1)
    $ getexp = 1100000
    call lvup
    jump lb_0159

label gnome_ng_a:
    if damage_keigen < 30:
        $ damage_keigen = 30
        call gnome_ng_earthguard2
    elif damage_keigen > 30:
        $ damage_keigen = 30

    if kousoku > 0:
        $ hanyo[1] += 1
    elif kousoku < 1:
        $ hanyo[1] = 0
    if hanyo[1] > 2:
        jump gnome_ng_a6

    if used_daystar > 0 and daten > 0:
        call gnome_ng_earthguard
        jump common_main
    if charge > 0:
        $ ransu = rnd2(1,2)
    if charge > 0 and kousoku > 0:
        call gnome_ng_a1
        $ charge = 0
        jump common_main
    elif charge > 0 and ransu == 1:
        call gnome_ng_a1
        $ charge = 0
        jump common_main
    elif charge > 0 and ransu == 2:
        call gnome_ng_a2
        $ charge = 0
        jump common_main

    $ ransu = rnd2(1,6)
    if ransu < 4:
        call gnome_ng_a0
        jump common_main
    elif ransu == 4:
        call gnome_ng_a3
        jump common_main
    elif ransu == 5:
        call gnome_ng_a4
        jump common_main
    elif ransu == 6 and kousoku < 1:
        call gnome_ng_a5
        jump common_main
    elif ransu == 6 and kousoku > 0:
        call gnome_ng_a4
        jump common_main

label gnome_ng_earthguard:
    enemy "................."
    call show_skillname("Титановая броня")
    play sound "audio/se/earth2.ogg"
    gnome "Гнома концентрирует силу земли и значительно усиливает свою защиту!"
    $ damage_keigen = 2
    if daten > 0:
        call skill22x
        jump common_main
    call hide_skillname
    return

label gnome_ng_earthguard2:
    "Титановая броня Гномы спадает!"
    return

label gnome_ng_a0:
    enemy "................."
    call show_skillname("Разгон")
    play sound "audio/se/power.ogg"
    "Гнома заряжает свою магию!"
    $ charge += 1
    call hide_skillname
    return

label gnome_ng_a1:
    $ charge = 0
    enemy "...................."
    call show_skillname("Удар Гномы")
    play sound "audio/se/earth2.ogg"
    "Гнома наделяет свой кулак силой земли!{w}{nw}"
    play sound "audio/se/dageki.ogg"
    extend "\nПосле чего, бьёт Луку!"
    $ ransu = rnd2(1,100)
    if kousoku < 1:
        if skill04 < 1:
            if aqua > 2 and Difficulty == 1 and ransu < 41+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 36+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 31+undine_buff:
                jump aqua_guard
        elif skill04 > 0:
            if aqua == 2 and Difficulty == 1 and ransu < 76+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 2 and ransu < 71+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 3 and ransu < 66+undine_buff:
                jump aqua_guard


            if aqua > 2 and Difficulty == 1 and ransu < 51+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 46+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 41+undine_buff:
                jump aqua_guard

        if wind != 4 and wind != 0 and Difficulty == 1:
            $ wind_kakuritu  = 85
        elif wind != 4 and wind != 0 and Difficulty == 2:
            $ wind_kakuritu  = 80
        elif wind != 4 and wind != 0 and Difficulty == 3:
            $ wind_kakuritu  = 75

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ guard_break = 1
        $ a_effect1 = 1
        $ dmg = {0: (950, 1000), 1: (950, 1000), 2: (1090, 1150), 3: (1230, 1300)}
        call damage(dmg[Difficulty])

    call hide_skillname
    $ guard_break = 0

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label gnome_ng_a2:
    $ charge = 0
    enemy "...................."
    call show_skillname("Разлом")
    play sound "audio/se/earth2.ogg"
    "Гнома наделяет свой кулак силой земли!{w}\nПосле чего, земля под ногами Луки раскалывается!"
    $ ransu = rnd2(1,100)
    if kousoku < 1:
        if skill04 < 1:
            if aqua > 2 and Difficulty == 1 and ransu < 41+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 36+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 31+undine_buff:
                jump aqua_guard
        elif skill04 > 0:
            if aqua == 2 and Difficulty == 1 and ransu < 61+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 2 and ransu < 56+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 3 and ransu < 51+undine_buff:
                jump aqua_guard


            if aqua > 2 and Difficulty == 1 and ransu < 41+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 36+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 31+undine_buff:
                jump aqua_guard

        if wind != 4 and wind != 0 and Difficulty == 1:
            $ wind_kakuritu  = 85
        elif wind != 4 and wind != 0 and Difficulty == 2:
            $ wind_kakuritu  = 80
        elif wind != 4 and wind != 0 and Difficulty == 3:
            $ wind_kakuritu  = 75

    $ dmg = {0: (220, 240), 1: (220, 240), 2: (260, 280), 3: (450, 470)}
    call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    $ mogaku_anno1 = "Но Лука не может выбраться из земли!"
    $ mogaku_anno2 = "Лука вырывается на свободу!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"

    $ kousoku = 1
    $ genmogaku = 2
    call status_print
    "Лука застрял в земле!"

    call hide_skillname

    return

label gnome_ng_a3:

    enemy "...................."
    call show_skillname("Грязевые ручки")
    play sound "audio/se/ero_koki1.ogg"
    "Гнома хватает член Луки неистово доит его!"
    $ ransu = rnd2(1,100)
    if kousoku < 1:
        if skill04 < 1:
            if aqua > 2 and Difficulty == 1 and ransu < 11+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 6+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 1+undine_buff:
                jump aqua_guard
        elif skill04 > 0:
            if aqua == 2 and Difficulty == 1 and ransu < 21+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 2 and ransu < 16+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 3 and ransu < 11+undine_buff:
                jump aqua_guard


            if aqua > 2 and Difficulty == 1 and ransu < 11+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 6+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 1+undine_buff:
                jump aqua_guard

        if wind != 4 and wind != 0 and Difficulty == 1:
            $ wind_kakuritu  = 40
        elif wind != 4 and wind != 0 and Difficulty == 2:
            $ wind_kakuritu  = 35
        elif wind != 4 and wind != 0 and Difficulty == 3:
            $ wind_kakuritu  = 30


    $ dmg = {0: (370, 430), 1: (370, 430), 2: (410, 460), 3: (490, 550)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label gnome_ng_a4:
    enemy "........"
    call show_skillname("Минет Земляного Духа")
    play sound "NGDATA/se/lewdsuck.ogg"
    "Гнома берёт член Луки в рот и начинает сильно сосать!"
    $ ransu = rnd2(1,100)
    if kousoku < 1:
        if skill04 < 1:
            if aqua > 2 and Difficulty == 1 and ransu < 16+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 11+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 6+undine_buff:
                jump aqua_guard
        elif skill04 > 0:
            if aqua == 2 and Difficulty == 1 and ransu < 26+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 2 and ransu < 21+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 3 and ransu < 16+undine_buff:
                jump aqua_guard


            if aqua > 2 and Difficulty == 1 and ransu < 11+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 6+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 1+undine_buff:
                jump aqua_guard

        if wind != 4 and wind != 0 and Difficulty == 1:
            $ wind_kakuritu  = 35
        elif wind != 4 and wind != 0 and Difficulty == 2:
            $ wind_kakuritu  = 30
        elif wind != 4 and wind != 0 and Difficulty == 3:
            $ wind_kakuritu  = 25

    $ dmg = {0: (410, 440), 1: (410, 440), 2: (450, 490), 3: (500, 550)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label gnome_ng_a5:
    $ hanyo[4] = 1
    if kousan != 2:
        enemy ".................."
    show doro st01 at xy(X=230) as c701
    show doro st02 at xy(X=-120) as c702
    show doro st03 at xy(X=-180) as c703
    show doro st03 at xy(X=240) as c704
    with dissolve
    call show_skillname("Заключение Грязевыми Куклами")
    "Грязевые куклы появляются вокруг Луки1"
    $ ransu = rnd2(1,100)
    if kousoku < 1:
        if skill04 < 1:
            if aqua > 2 and Difficulty == 1 and ransu < 46+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 41+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 36+undine_buff:
                jump aqua_guard
        elif skill04 > 0:
            if aqua == 2 and Difficulty == 1 and ransu < 76+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 2 and ransu < 71+undine_buff:
                jump aqua_guard
            elif aqua == 2 and Difficulty == 3 and ransu < 66+undine_buff:
                jump aqua_guard


            if aqua > 2 and Difficulty == 1 and ransu < 51+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 2 and ransu < 46+undine_buff:
                jump aqua_guard
            elif aqua > 2 and Difficulty == 3 and ransu < 41+undine_buff:
                jump aqua_guard

        if wind != 4 and wind != 0 and Difficulty == 1:
            $ wind_kakuritu  = 65
        elif wind != 4 and wind != 0 and Difficulty == 2:
            $ wind_kakuritu  = 60
        elif wind != 4 and wind != 0 and Difficulty == 3:
            $ wind_kakuritu  = 55

    if daten > 0:
        $ kousoku = 0
        call skill22x
        jump common_main
    $ kousoku = 1
    $ genmogaku = 1
    $ hanyo[2] = 1
    call status_print
    "Они хватают Луку и прижимают к земле!"
    gnome ".............."

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label gnome_ng_a6:
    if kousan == 2 and hanyo[2] < 1:
        call gnome_ng_a5
    if hanyo[2] < 1:
        call gnome_ng_a5
    if kousoku < 1:
        jump gnome_ng_a
    
    enemy "...!"
    play sound "audio/se/escape.ogg"
    hide c701
    hide c702
    hide c703
    hide c704
    with dissolve
    "Гнома срывает с себя всю одежду!"
    jump badend_h
    pause .5
    call show_skillname("Грязевая киска")
    play sound "audio/se/ero_pyu1.ogg"
    show gnome hb7 with dissolve
    "И насильно входит в Луку!"
    $ kousoku = 2
    $ damage_kotei = 1
    call damage((1000,1030))
    call hide_skillname
    if mylife == 0:
        jump badend_h
    "Лука был изнасилован Гномой!"
    enemy "................"
    jump gnome_ng_a8

label erubetie_ng_start:
    $ nostar = 0
    $ ng = 2
    $ monster_name = "Эрубети"
    $ lavel = "erubetie_ng"
    $ img_tag = "erubetie"
    $ tatie1 = "erubetie st01"
    $ tatie2 = "erubetie st01"
    $ tatie3 = "erubetie st01"
    $ tatie4 = "erubetie st01"
    $ haikei = "bg 097"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 36000
        $ henkahp1 = 27000
        $ henkahp2 = 18000
        $ henkahp3 = 10000
    elif Difficulty == 2:
        $ max_enemylife = 38000
        $ henkahp1 = 28500
        $ henkahp2 = 19000
        $ henkahp3 = 10000
    elif Difficulty == 3:
        $ max_enemylife = 40000
        $ henkahp1 = 0
        $ henkahp2 = 0
        $ henkahp3 = 0

    $ alice_skill = 0
    $ damage_keigen = 90
    $ earth_keigen = 30
    if Difficulty == 3:
        $ earth_keigen = 25
    $ kaihi = 80
    $ mus = 29
    $ tikei = 3
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_dassyutu1 = "Ты сбежал из моих объятий..."
    $ mogaku_dassyutu2 = "Но от своей судьбы тебе не убежать..."
    $ half_s1 = "Скоро я растворю тебя."
    $ half_s2 = "Я удостоверюсь, что ты будешь страдать за твоё появление здесь."
    $ kiki_s1 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ kiki_s2 = "I won't lose when it comes to power"
    $ end_n = 9
    $ exp_minus = 1370025
    $ zmonster_x = -270
    $ tukix = 320
    enemy "Хорошо.{w}\nЕсли ты так сильно хочешь умереть, тогда я с радостью исполню твою мольбу."
    l "Я здесь, чтобы повидаться с Ундиной, а не вредить источнику или играть в твои глупые игры!{w}\nПросто дай мне пройти!"
    "Я понимаю, что это бесполезно. Но я всё ещё не могу не попытаться переубедить её."
    enemy "... Как ты смеешь так разговаривать со мной...!?"
    "На мгноение я вижу, как на её вечно безразличном лице вспыхивает выражение гнева."
    enemy "... Очень хорошо.{w}\nТогда я сделаю твою смерть настолько мучительной, насколько это возможно."
    call show_skillname("Мор")
    play sound "audio/se/ero_makituki5.ogg"
    "Токсичная жидкость вцепляется в рот Луки!"
    l "Аргх!"
    "Прежде чем я успеваю среагировать, Эрубети насильно проталкивает её в моё горло.{w}\nЧтобы избежать удушья, я был вынужден проглотить это."
    $ ransu = rnd2(1,3)
    pause .5
    $ status = 9
    call status_print
    play sound "audio/se/aqua3.ogg"
    "Лука отравлен!"
    call hide_skillname
    $ poison = 999999
    l "Что ты сделала со мной!?"
    enemy "Загрязнённая слизь отравит твоё тело и медленно разрушит его.{w}\nКогда ты будешь достаточно ослаблен, что даже не сможешь продолжать сражаться, я поглощу тебя."
    l "Гах..."
    "У Эрубети очень извращённый ум...{w}\nНо я не могу так просто сдаться!"
    l "Если мне придётся победить тебя, чтобы продолжить путь, то так тому и быть!"
    call skillset(2)
    "Я вынимаю свой меч, приготовившись к битве."
    $ enemy_aqua = 1
    $ enemy_aquaturn = 9999
    $ hanyo[1] = 0
    jump common_main

label erubetie_ng_main:
    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and kousoku < 1:
        jump common_attack
    elif result == 2 and earth < 1 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and earth < 1 and genmogaku == 0:
        jump common_mogaku2
    elif result == 2 and earth > 0 and genmogaku > 0:
        jump mogaku_earth1
    elif result == 2 and earth > 0 and genmogaku == 0:
        jump mogaku_earth2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump undine_ng_kousan
    elif result == 6:
        jump erubetie_ng_onedari
    elif result == 7:
        jump common_skill

label erubetie_ng_v:
    call face(param=3)
    enemy "Аргх!{w}\nКак это возможно!?"
    enemy "Для обычного человека иметь такую силу..."
    call victory(viclose=1)
    $ getexp = 1750000
    call lvup
    jump lb_0196a

label erubetie_ng001:
    $ hanyo[1] = 1
    l "Эрубети, пожалуйста, послушай меня!{w}\nЯ не хочу сражаться с тобой или вредить девушкам-слизям! Эта не та причина, по которой я здесь!"
    enemy "Жалкие молитвы, которыми ты пытаешься искупить свои грехи...{w}\nНеужели ты ожидаешь, что я поверю в них, после того, как ты напал на моих подчинённых?{w}\nТвоя просьба оскорбительна!"
    l "........."
    return

label erubetie_ng002:
    $ hanyo[1] = 2
    l "Неужели ты думаешь, что я хотел с ними сражаться!?{w}\nЭти девушки-слизи первые меня атаковали! Я просто защищал себя!"
    enemy "Жалкое оправдание...{w}\nКак будто бы они угрожали твоей жизни."
    enemy "Ты дерёшься со мной, но, несмотря на это, тебе хватает смелости обвинять моих подчинённых!?"
    l "Но тогда что насчёт остальных людей, которые пропали в этом источнике?{w}\nЧто они такого сделали, чем заслужили смерть!?"
    enemy "Люди ступили на нашу священную землю.{w}\nОни заслужили свою смерть за это святотатство."
    enemy "И вскоре ты присоединишься к ним."
    return

label erubetie_ng003:
    $ hanyo[1] = 3
    l "Ты серьёзно думаешь, что беспричинная жестокость решит вашу проблему?{w}\nЧто дало убийство этих людей?{w}\nЭто не вернёт ваши места обитания..."
    enemy "Умолкни!"
    enemy "Если у нас не будет шанса выжить, тогда я самолично поглощу и растворю каждого живущего человека на этой планете.{w} Поэтому я уничтожу любого, кто встанет у меня на пути!"
    return

label erubetie_ng_a:
    if enemylife < henkahp3 and hanyo[1] == 2:
        call erubetie_ng003
    elif enemylife < henkahp2 and hanyo[1] == 1:
        call erubetie_ng002
    elif enemylife < henkahp1 and hanyo[1] == 0:
        call erubetie_ng001

    if kousoku < 1:
        hide c701
        hide c702
        hide c703
        hide c704
        with dissolve

    if kousoku == 1:
        call erubetie_ng_a7
        jump common_main
    elif kousoku == 2:
        call erubetie_ng_a10
        jump common_main
    elif kousoku == 3:
        call erubetie_ng_a11
        jump common_main
    elif kousoku == 4:
        call erubetie_ng_a12
        jump common_main

    while True:
        $ ransu = rnd2(1,7)

        if ransu == 1:
            call erubetie_ng_a1
            jump common_main
        elif ransu == 2:
            call erubetie_ng_a2
            jump common_main
        elif ransu == 3:
            call erubetie_ng_a3
            jump common_main
        elif ransu == 4:
            call erubetie_ng_a4
            jump common_main
        elif ransu == 5:
            call erubetie_ng_a5
            jump common_main
        elif ransu == 6:
            call erubetie_ng_a6
            jump common_main
        elif ransu == 7:
            call erubetie_ng_a9
            jump common_main

label erubetie_ng_ab:
    while True:
        $ ransu = rnd2(1,3)
        if ransu == 1:
            call erubetie_ng_a1
            jump common_main
        elif ransu == 2:
            call erubetie_ng_a2
            jump common_main
        elif ransu == 3:
            call erubetie_ng_a3
            jump common_main

label erubetie_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я использую свою слизь, чтобы поиграть с твоим членом..."
    elif ransu == 2:
        enemy "Как долго ты сможешь выдержать мою слизь?"
    elif ransu == 3:
        enemy "Наполни мою слизь своей спермой..."

    call show_skillname("Вытягивание Агарты")
    play sound "audio/se/ero_slime1.ogg"
    "Слизь Эрубети сжимает член Луки!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 51:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (540, 570), 1: (540, 570), 2: (560, 590), 3: (580, 610)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Окунись в мою слизь своим телом..."
    elif ransu == 2:
        enemy "Я обернусь вокруг твоего тела..."
    elif ransu == 3:
        enemy "Я буду ласкать тебя своей слизью..."

    call show_skillname("Распространение Аркадии")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети обволакивает тело Луки!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 51:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (560, 580), 1: (560, 580), 2: (580, 600), 3: (600, 620)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я поиграю с твоей задней дырочкой..."
    elif ransu == 2:
        enemy "Моя бесформенная слизь довольно легко может проскользнуть внутрь..."
    elif ransu == 3:
        enemy "Дай мне посмотреть на эту жалкую эякуляцию от такого метода..."

    call show_skillname("Поглаживание Ксанаду")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети проникает в анальное отверстие Луки, стимулируя его простату!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 51:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (540, 560), 1: (540, 560), 2: (560, 580), 3: (580, 600)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Скорчись от вихря наслаждения!"
    elif ransu == 2:
        enemy "Что-то, что может сделать только слизь..."
    elif ransu == 3:
        enemy "Я попытаю его в вихре наслаждения..."

    call show_skillname("Закручивание Шангри-Ла")
    play sound "audio/se/ero_slime1.ogg"
    "Слизь Эрубети стекает член Луки!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 51:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (180, 200), 1: (180, 200), 2: (187, 207), 3: (194, 214)}
        call damage(dmg[Difficulty])

    play sound "audio/se/ero_slime1.ogg"
    "Слизь принимает форму вокруг члена Луки и сжимает его!{w}{nw}"

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (180, 200), 1: (180, 200), 2: (187, 207), 3: (194, 214)}
        call damage(dmg[Difficulty])

    play sound "audio/se/ero_slime1.ogg"
    "После этого, она начинает вращаться вокруг него, словно водоворот!"

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu


    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (180, 200), 1: (180, 200), 2: (187, 207), 3: (194, 214)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a5:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Растворись в волне наслаждения!"
    elif ransu == 2:
        enemy "Расплавься в гигантской волне..."
    elif ransu == 3:
        enemy "Растворись в моём теле..."

    call show_skillname("Расплавляющий Шторм")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети создаёт массивное цунами!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 101:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 101:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 101:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 75
        elif Difficulty == 2:
            $ wind_kakuritu = 75
        elif Difficulty == 3:
            $ wind_kakuritu = 70
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        "Огромная массивная волна слизи обрушивается на тело Луки, захватывая его в вихрь слизи!"
        $ dmg = {0: (1850, 1950), 1: (1850, 1950), 2: (1950, 2050), 3: (2000, 2100)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a6:
    $ owaza_count1a = 0
    $ owaza_num1 = 1
    if kousan == 2:
        enemy "Утони в наслаждении моего тела!"

    call show_skillname("Райская Тюрьма")
    play sound "audio/se/ero_slime3.ogg"
    "Слизь Эрубети обволакивает тело Луки и насильно поглужает его в неё!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 101:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 85
        elif Difficulty == 2:
            $ wind_kakuritu = 85
        elif Difficulty == 3:
            $ wind_kakuritu = 80
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard
    else:
        $ dmg = {0: (330, 360), 1: (330, 360), 2: (350, 380), 3: (370, 400)}
        call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if kousan == 2:
        return

    if first_sel1 != 1:
        enemy "Теперь пришло время выжать твоё семя...{w}\nДай мне насладиться твоей агонией от наслаждения..."
    else:
        enemy "На этот раз тебе не вырваться.{w}\nДай мне насладиться твоей агонией от наслаждения..."

    $ first_sel1 = 1

    if earth < 1:
        $ genmogaku = 2
    elif earth > 0:
        $ genmogaku = 0

    return

label erubetie_ng_a7:
    "Лука застрял в слизи Эрубети..."
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Окунись в мою слизь своим телом..."
    elif ransu == 2:
        enemy "Я обернусь вокруг твоего тела..."
    elif ransu == 3:
        enemy "Я буду ласкать тебя своей слизью..."

    call show_skillname("Райская Тюрьма")
    play sound "audio/se/ero_slime3.ogg"
    "Слизь Эрубети проникает в анальное отверстие Луки, стимулируя его простату!"
    $ dmg = {0: (530, 560), 1: (530, 560), 2: (550, 580), 3: (570, 600)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label erubetie_ng_a9:
    $ owaza_count1a = 0
    $ owaza_num1 = 2
    if kousan == 2:
        enemy "Хе-хе... Дай мне немного приобнять тебя..."

    call show_skillname("Божественная Буря")
    play sound "audio/se/ero_makituki2.ogg"
    "Эрубети подбирается к Луке со спины, и обнимает его!"
    $ ransu = rnd2(1,100)
    if aqua > 2 and Difficulty == 1 and ransu < 101:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 85
        elif Difficulty == 2:
            $ wind_kakuritu = 85
        elif Difficulty == 3:
            $ wind_kakuritu = 80
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard
    else:
        $ dmg = {0: (400, 440), 1: (400, 440), 2: (420, 460), 3: (440, 480)}
        call damage(dmg[Difficulty])

    "Задняя часть тела Луки утопает в теле Эрубети!"

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    if kousan == 2:
        return

    enemy "Моё тело приятно, не правда ли?{w}\nПозволь мне сделать тебе ещё приятнее..."

    if earth < 1:
        $ genmogaku = 1
    elif earth > 0:
        $ genmogaku = 0

    return

label erubetie_ng_a10:
    if kousan != 2:
        enemy "Хе-хе... я могу делать и так..."
    call show_skillname("Божественный поток")
    play sound "audio/se/ero_slime3.ogg"
    show erubetie st01 at xy(X=50)
    show erubetie st01 at xy(X=400) as c702
    with dissolve
    "Эрубети разделяется ещё на одно тело, которое обнимает Луку спереди!{w}{nw}"
    $ dmg = {0: (400, 440), 1: (400, 440), 2: (420, 460), 3: (440, 480)}
    call damage(dmg[Difficulty])
    $ kousoku = 3
    extend "\nЛука зажат меж двух Эрубети!"
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    if kousan == 2:
        return

    e_a "Хе-хе... теперь стало в два раза лучше, ведь так?"
    e_b "Я собираюсь сделать тебе ещё приятнее..."

    return

label erubetie_ng_a11:
    if kousan == 2:
        enemy "Я дам тебе насладиться сексом, который могут дать только девушки-слизи..."

    call show_skillname("Божественное Обуздание")
    "Эрубети разделяется вновь!{w}\nНовое тело хватается за нижнюю часть тела Луки!"
    $ dmg = {0: (400, 440), 1: (400, 440), 2: (420, 460), 3: (440, 480)}
    call damage(dmg[Difficulty])
    $ kousoku = 4
    "Тело Луки удерживается тремя Эрубети!"
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    if kousan == 2:
        return

    e_a "Хе-хе... Подготовка завершена."
    e_b "Пришло время веселья..."
    e_c "Скорчись в агонии от адского наслаждения, которое может принести только слизь..."

    return

label erubetie_ng_a12:
    if kousan == 2:
        enemy "Теперь, сойди с ума от наслаждения..."

    call show_skillname("Божественная Судьба")
    "Все Эрубети, держащиеся за тело Луки, начинают одновременно двигаться!"

    jump badend_h


label undine_ng_start:
    $ poison = 1
    $ ng = 2
    if skill04 > 0:
        $ skill04 = 3
    $ monster_name = "Ундина"
    $ lavel = "undine_ng"
    $ img_tag = "undine"
    $ tatie1 = "undine st01"
    $ tatie2 = "undine st02"
    $ tatie3 = "undine st01"
    $ tatie4 = "undine st02"
    $ haikei = "bg 097"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 18000
    elif Difficulty == 2:
        $ max_enemylife = 18500
    elif Difficulty == 3:
        $ max_enemylife = 19500

    $ alice_skill = 0
    $ damage_keigen = 80
    $ earth_keigen = 20
    $ kaihi = 90
    $ mus = 27
    $ tikei = 3
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "Ты даже вырваться не можешь?"
    $ mogaku_sel2 = "Похоже, ты уже всё..."
    $ mogaku_sel3 = "Ты не можешь использовать силу духов..."
    $ mogaku_sel4 = "Сопротивляться физически бесполезно."
    $ mogaku_sel5 = "Я всё представляла, каков ты... Но проиграть этому..."
    $ mogaku_earth_dassyutu1 = "Похоже, ты можешь использовать силу земли."
    $ end_n = 4
    $ exp_minus = 4150
    $ zmonster_x = -280

    undine "Тогда хорошо.{w}\nЕсли ты собираешься атаковать всех подряд в полную силу...{w}\nТо я тоже не стану сдерживаться."
    "Ледяной голос Ундины пронзает меня."
    l "Да послушай же ты меня~!{w}\nЗнаю, я выгляжу как плохой парень, но я не твой враг!"
    show undine st04 with dissolve
    undine "Ты вторгся в мой источник и атаковал здешних девушек-слизей.{w}\nТы даже ранил Эрубети..."
    call face(param=1)
    undine "Ты заплатишь за это."
    l "......................"
    call hanyo_null
    $ nostar = 2
    jump common_main

label undine_ng001:
    $ hanyo[2] = 1
    show undine st04 with dissolve
    enemy "... Хмпф.{w}\nДолжна признать, я поражена..."
    call face(param=1)
    enemy "... Но я не позволю тебе победить."
    call undine_ng_serene2
    l "Подожди-ка...{w}\nТы только что использовала Безмятежный Разум!?"
    enemy "Ты серьёзно удивлён, что дух воды обладает такой способностью?{w}\nТеперь ты меня даже не каснёшься."
    l "........"
    "Похоже, что теперь попасть по ней будет сложно.{w}\nНо может быть если я воспользуюсь достаточно быстрой техникой..."
    return

label undine_ng002:
    $ hanyo[5] = 1
    enemy "Хм?{w}\nСаламандра с тобой...?"
    enemy "Как...{w} ностальгично."
    call show_skillname("Стена Воды")
    show undine st11 with dissolve
    pause .5
    play sound "audio/se/aqua.ogg"
    with flash
    pause .5
    play sound "audio/se/aqua2.ogg"
    "Толстая стенка воды окружает Ундины, защищая её от пламени!"
    $ damage_keigen = 50
    show undine st04 with dissolve
    enemy "Попробуй-ка пробиться через это. Если сможешь..."
    call hide_skillname
    return

label undine_ng_main:
    if result == 44 and fire > 0 and hanyo[4] < 1:
       jump undine_ng_minilixir

    call cmd("attack")

    if fire > 0 and nostar < 2:
        $ nostar = 1
    if fire == 0 and nostar < 2:
        $ nostar = 0

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and kousoku < 1:
        jump common_attack
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5 and sinkou == 1:
        jump undine_ng_kousan
    elif result == 7:
        jump common_skill

label undine_ng_kousan:
    if surrender < 1:
        call kousan_syori

    "Лука прекращает сопротивляться."
    enemy "Ох?{w}\nТы уже всё?{w}\nХорошо."
    $ daten = 0
    $ aqua = 0
    jump undine_ng_a3b

label undine_ng_v:
    show undine st04 with dissolve
    enemy ".............{w}\nПолагаю будет глупостью продолжать сражение."
    show undine st05 with dissolve
    enemy "Хорошо.{w}\nЯ достаточно увидела."
    enemy "Я признаю своё поражение.{w}\nТы победил."
    "Ундина сдаётся..."
    call victory(viclose=1)
    $ getexp = 1250000
    call lvup
    jump lb_0197

label undine_ng_a_aqua:
    $ hanyo[0] = 2
    $ ransu = rnd2(1,3)
    if fire > 0 and (enemylife < max_enemylife * 2/3) and enemy_aqua < 1:
        if ransu == 1:
            call undine_ng_serene2
            jump common_main
        if ransu > 1:
            call undine_ng_serene2
            jump common_main

    call undine_ng_serene2
    jump common_main

label undine_ng_a:
    while True:
        if fire > 0 and hanyo[5] < 1:
            call undine_ng002

        if hanyo[4] > 0:
            $ hanyo[4] -= 1

        if enemylife < (max_enemylife / 2) and hanyo[2] < 1:
            call undine_ng001

        if enemylife < (max_enemylife * 45 / 100)  and nostar > 1 and hanyo[4] > 0 and fire > 0:
            jump undine_ng_heal

        if enemylife < (max_enemylife * 37 / 100)  and nostar < 2 and hanyo[4] > 0 and fire > 0:
            jump undine_ng_heal

        if enemylife < (max_enemylife / 2) and salamander_rep > 19 and fire > 0:
            jump undine_ng_minilixir

        if enemylife < (max_enemylife / 2) and hanyo[1] < 1 and enemy_aqua < 1:
            jump undine_ng_a_aqua

        if kousoku > 0:
            jump undine_ng_a3b

        if fire > 0 and hanyo[2] < 1:
            jump undine_ng_sala

        if aqua > 0 and hanyo[1] < 1:
            jump undine_ng_serene

        if enemylife < (max_enemylife / 2) and mp < 8:
            $ sinkou = 0

        if hanyo[3] != 1 and enemylife < (max_enemylife / 2) and mp > 10:
            $ sinkou += 1
            $ hanyo[3] = 1

        if enemylife < (max_enemylife / 2) and mp > 10 and hanyo[4] < 1:
            jump undine_ng_heal

        if enemylife < (max_enemylife / 3) and hanyo[4] < 1:
            jump undine_ng_heal

        $ ransu = rnd2(1,7)

        if kousoku > 0 and enemylife > (max_enemylife / 2):
            call undine_ng_a4
            jump common_main

        if kousoku > 0 and enemylife < (max_enemylife / 2):
            call undine_ng_a3c
            jump common_main

        if ransu == 1:
            call undine_ng_a1
            jump common_main

        elif ransu == 2:
            call undine_ng_a2
            jump common_main

        elif ransu == 3 and enemylife > (max_enemylife * 2/3):
            call undine_ng_a3
            jump common_main

        elif ransu == 3 and enemylife < (max_enemylife * 2/3):
            call undine_ng_a3b
            jump common_main

        elif ransu == 4:
            call undine_ng_a5
            jump common_main

        elif ransu == 5 and enemylife < (max_enemylife * 2/3):
            call undine_ng_a6
            jump common_main

        elif ransu == 6:
            call undine_ng_a7
            jump common_main

        elif ransu == 7:
            call undine_ng_a8
            jump common_main

label undine_ng_nostar:
    show undine st07 with dissolve
    enemy "Я знала, что ты сделаешь так!"
    call counter
    call show_skillname("Слизневый Шторм")
    "Ундина создаёт массивное цунами из слизи над Денницей!{nw}{w}"
    play sound "audio/se/wind2.ogg"
    extend "\nПосле чего, это цунами поглощает Денницу, уничтожая её!"
    call hide_skillname
    if hanyo[3] < 1:
        call undine_ng_nostar2
    return

label undine_ng_nostar2:
    $ hanyo[3] = 1
    l "Что...!?{w}\nМоя атака!"
    call face(param=1)
    if hanyo[5] > 0:
        enemy "Думаешь такой умный, вызывая Саламандру после такой атаки?{w}\nЯ не настолько глупа, малыш."
    l "Гах...{w}\nЧёрт возьми..."

    return
label undine_ng_sala:
    show undine st06 with dissolve
    enemy "Саламандра...{w}{nw}"

    if salamander_rep > 4:
        extend "\nК тому же, их связь довольно сильна..."

    enemy "Похоже это будет тяжелее, чем я думала..."
    $ hanyo[2] = 1
    call face(param=1)
    jump undine_ng_a

label undine_ng_serene2:
    show undine st11 with dissolve
    enemy "..........................."
    call show_skillname("Безмятежный Разум: Уровень 2")
    play sound "audio/se/aqua.ogg"
    with flash
    pause .5
    "Ундина очищает свой разум и соединяется с потоками мира!"
    $ enemy_aqua = 2
    $ enemy_aquaturn = 9999
    call hide_skillname
    call face(param=1)
    return

label undine_ng_serene3:
    show undine st11 with dissolve
    enemy "..........................."
    call show_skillname("Безмятежный Разум: Уровень 3")
    play sound "audio/se/aqua.ogg"
    with flash
    pause .5
    "Ундина очищает свой разум и соединяется с потоками мира!"
    $ enemy_aqua = 3
    $ enemy_aquaturn = 12
    call hide_skillname
    call face(param=1)
    return

label undine_ng_minilixir:
    show undine st06 with dissolve
    enemy "Ах!{w}\nСаламандра тоже сильна...!"
    call show_skillname("Миниксир")
    play sound "audio/se/aqua.ogg"
    $ damage = 3000
    call enemylife_kaihuku
    call hide_skillname
    $ hanyo[4] = 1
    call face(param=1)
    return

label undine_ng_heal:
    call face(param=2)
    if hanyo[0] == 0:
        enemy "Я не позволю тебе так легко победить..."
    elif hanyo[0] == 1:
        enemy "Это бесполезно..."
    elif hanyo > 1:
        enemy "Ты не достоин моей силы..."

    call show_skillname("Эликсир")
    play sound "audio/se/aqua.ogg"
    "Ундина со всей своей силы поглощает влагу из воздуха..."
    play sound "audio/se/power.ogg"
    "Ундина накапливает свои силы!"
    if daten > 0:
        call skill22x
        jump common_main

    $ damage = max_enemylife / 3
    call enemylife_kaihuku
    call hide_skillname
    $ hanyo[4] = 2
    if hanyo[0] == 0:
        call undine_ng_heal2
    elif hanyo[0] == 1:
        call undine_ng_heal3
    call face(param=1)
    return

label undine_ng_heal2:
    l "Что за...!"
    $ hanyo[0] = 1
    "Она просто исцелилась!?"
    enemy "Глупое дитя...{w}\nТвоя вина, что ты дал мне шанс."
    l "Дерьмово..."
    "Ну, а теперь то что?{w}\nМне просто продолжать атаковать её?"
    return

label undine_ng_heal3:
    $ hanyo[0] = 2
    l "Снова!?"
    "Но если она продолжит так просто лечиться...!"
    enemy "Хмпф.{w}\nТы правда настолько беспомощен?"
    return

label undine_ng_serene:
    $ hanyo[1] = 1
    enemy "Безмятежный разум...{w}\nЭто... странно."
    "Ундина выглядит озадаченной."
    enemy "... Ты пытаешься на меня произвести впечатление или что?"
    l "Хах, что...?"
    enemy "Твоё умение обращаться с потоками - отстой.{w}\nЯ могу легко обойти это..."
    l "Что!?"
    "\"Отстой\"!?{w}\nЯ правда настолько плох...?"
    $ hanyo[1] = 1
    jump undine_ng_a

label undine_ng_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Если ты кончишь от этого — провалишь испытание..."
    elif ransu == 2:
        enemy "Терпи это удовольствие"
    elif ransu == 3:
        enemy "Приятно, правда...? Но ты должен терпеть."

    call show_skillname("Аква-змея")
    play sound "audio/se/ero_makituki1.ogg"
    "Ундина хватает член Луки своей рукой!"

    $ ransu = rnd2(1,100)

    if aqua > 2 and Difficulty < 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 5
        elif Difficulty == 2:
            $ wind_kakuritu = 3
        elif Difficulty == 3:
            $ wind_kakuritu = 1
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (540, 570), 1: (540, 570), 2: (650, 680), 3: (700, 740)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label undine_ng_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как долго ты сможешь терпеть это...?"
    elif ransu == 2:
        enemy "Кончай... Если ты сдаёшься."
    elif ransu == 3:
        enemy "Вытерпишь ли ты это?"

    call show_skillname("Аква-поглаживания")
    play sound "audio/se/ero_slime1.ogg"
    "Хвост Ундины хватает член Луки, поглаживая его!"

    $ ransu = rnd2(1,100)

    if aqua > 2 and Difficulty < 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4:
        $ wind_kakuritu = 5
    if wind == 4:
        $ wind_kakuritu = 1

    $ wind_kakuritu = 0
    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (540, 570), 1: (540, 570), 2: (650, 680), 3: (700, 740)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label undine_ng_a3:
    $ owaza_count1a = 0
    if kousan == 2:
        enemy "Я обездвижу твоё тело..."

    call show_skillname("Водная Тюрьма")
    play sound "audio/se/ero_slime3.ogg"
    "Тело Ундины поглощает Луку!"

    $ ransu = rnd2(1,100)

    if aqua > 2 and Difficulty < 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    # if wind > 0 and wind != 4:
    #     if Difficulty == 1:
    #         $ wind_kakuritu = 10
    #     if Difficulty == 2:
    #         $ wind_kakuritu = 5
    #     if Difficulty == 3:
    #         $ wind_kakuritu = 3
    # if wind == 4:
    #     $ wind_kakuritu = 1

    # call wind_guard_kakuritu

    # if wind_guard_on != 0:
    #     if wind > 0:
    #         $ sel1 = "Но Лука уклоняется словно торнадо!"
    #         jump wind_guard

    $ dmg = {0: (440, 480), 1: (440, 480), 2: (520, 560), 3: (570, 640)}
    call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    call hide_skillname

    "Лука застрял в теле Ундины!"

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    call face(param=2)
    if first_sel1 == 0:
        enemy "Если ты достаточно силён, то тебе не составит проблем вырваться, да...?"

    elif first_sel1 == 1:
        enemy "Выберишься ли ты в этот раз...?"

    $ first_sel1 = 1

    if earth == 0:
        $ genmogaku = 1

    elif earth > 0:
        $ genmogaku = 0

    return

label undine_ng_a3b:
    $ owaza_count1a = 0
    if kousan == 2:
        enemy "Твоя сила ошеломительна...{w}\nНо!"

    call show_skillname("Объятье Водного Духа")
    play sound "audio/se/ero_slime3.ogg"
    "Ундина захватывает Луку своим хвостом!"

    $ ransu = rnd2(1,100)

    if aqua > 1 and spirit_count == 0:
        jump aqua_guard

    if aqua > 2 and Difficulty < 2 and ransu < 11 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    # if wind > 0 and wind != 4:
    #     if Difficulty == 1:
    #         $ wind_kakuritu = 10
    #     if Difficulty == 2:
    #         $ wind_kakuritu = 5
    #     if Difficulty == 3:
    #         $ wind_kakuritu = 3
    # if wind == 4:
    #     $ wind_kakuritu = 1

    # call wind_guard_kakuritu

    # if wind_guard_on != 0:
    #     if wind > 0:
    #         $ sel1 = "Но Лука уклоняется словно торнадо!"
    #         jump wind_guard

    $ dmg = {0: (140, 180), 1: (140, 180), 2: (220, 260), 3: (370, 440)}
    call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    call hide_skillname

    "Лука схвачен хвостом Ундины!"

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    call face(param=2)
    enemy "Тебе не вырваться.{w}\nТеперь ты весь мой..."
    play sound "audio/se/ero_pyu1.ogg"
    #show undine hb1 with dissolve
    "Ундина проталкивает Луку внутрь себя!"
    $ dmg = {0: (640, 680), 1: (640, 680), 2: (720, 760), 3: (870, 940)}
    call damage(dmg[Difficulty])

    $ first_sel1 = 1

    if mylife == 0:
        return_to badend_h

    if earth == 0:
        $ genmogaku = 1

    elif earth > 0:
        $ genmogaku = 0

    if kousan > 0:
        jump undine_ng_a3c

    return

label undine_ng_a4:
    "Лука застрял в теле Ундины..."
    $ ransu = rnd2(1,5)
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Стань единым целым со мной..."
    elif ransu == 2:
        enemy "Пришла пора заканчивать..."
    elif ransu == 3:
        enemy "Я выжму твоё семя и растворю тебя..."
    elif ransu == 4:
        enemy "Если я простимулирую твой член, ты кончишь в меня?"
    elif ransu == 5:
        enemy "Просто расслабься..."

    call show_skillname("Аква-объятья")
    play sound "audio/se/ero_makituki3.ogg"
    if ransu == 1:
        "Тело Ундины поглощает Луку!"
    elif ransu == 2:
        "Тело Ундины сжимает член Луки!"
    elif ransu == 3:
        "Холодная слизь тела Ундины заставляет Луку расслабиться!"
    elif ransu == 4:
        "Слизистое тело Ундины дрожит, касаясь члена Луки!"
    elif ransu == 5:
        "Тело Ундины начинает вибрировать!"
    #call skillcount2(3394)
    $ dmg = {0: (840, 880), 1: (840, 880), 2: (920, 960), 3: (970, 1020)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if kousan == 3:
        return

    if mylife == 0:
        return_to badend_h

    return

label undine_ng_a5:
    #call skillcount1(3028)
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты кончишь от этого..."
    elif ransu == 2:
        enemy "Я очень хороша в этом..."
    elif ransu == 3:
        enemy "Ты не сможешь сопртивляться..."

    call show_skillname("Аква-минет")
    play sound "audio/se/ero_chupa4.ogg"
    "Ундина берёт член Луки в рот и начинает неистово сосать!"
    play sound "audio/se/ero_chupa5.ogg"

    $ ransu = rnd2(1,100)

    if aqua > 2 and Difficulty < 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 5
        elif Difficulty == 2:
            $ wind_kakuritu = 5
        elif Difficulty == 3:
            $ wind_kakuritu = 5
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard

    else:
        $ dmg = {0: (680, 740), 1: (680, 740), 2: (760, 820), 3: (830, 880)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label undine_ng_a6:
    jump undine_ng_a
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Повинуйся моей песни."
    elif ransu == 2:
        enemy "Ты не сможешь сопртивляться..."
    elif ransu == 3:
        enemy "Я легко загипнотизирую тебя..."

    call show_skillname("Песнь Одинокой Русалки")
    play sound "audio/se/ero_makituki5.ogg"
    "Ундина поёт прекрасную песню!"
    play sound "audio/se/ero_chupa5.ogg"

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 100
        elif Difficulty == 2:
            $ wind_kakuritu = 100
        elif Difficulty == 3:
            $ wind_kakuritu = 100
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра оглушает Луку!"
            jump wind_guard
    else:
        $ status = 1
        call status_print
        $ status_turn = 2
        "Лука загипнотизирован песней Ундины!"
    call hide_skillname
    call face(param=2)
    enemy "Ха-ха-ха...{w}\nТеперь ты в моей власти..."

    return

label undine_ng_a7:
    #call skillcount1(3026)
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как долго ты сможешь терпеть это...?"
    elif ransu == 2:
        enemy "Просто сдайся..."
    elif ransu == 3:
        enemy "Вытерпишь ли ты это?"

    call show_skillname("Липкое пайзури")
    play sound "audio/se/ero_slime3.ogg"
    pause .4
    play sound "audio/se/ero_koki1.ogg"
    "Ундина зажимает член Луки меж своей липкой груди!"

    $ ransu = rnd2(1,100)

    if aqua > 2 and Difficulty < 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 5
        elif Difficulty == 2:
            $ wind_kakuritu = 5
        elif Difficulty == 3:
            $ wind_kakuritu = 5
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Аква-змея")

    else:
        $ dmg = {0: (660, 720), 1: (660, 720), 2: (740, 780), 3: (820, 880)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label undine_ng_a8:
    $ ransu = rnd2(1,100)
    if ransu < 50:
        jump undine_ng_a

    $ owaza_count3b = 0
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Теперь у меня есть силы для этого..."
    elif ransu == 2:
        enemy "Прощай, Лука..."
    elif ransu == 3:
        enemy "Это конец..."

    call show_skillname("Слизневый Шторм")
    play sound "audio/se/ero_slime3.ogg"

    "Ундина создаёт массивное цунами из слизи!"

    if aqua > 2:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 20
        elif Difficulty == 2:
            $ wind_kakuritu = 15
        elif Difficulty == 3:
            $ wind_kakuritu = 10
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    "Массивная стена из слизи врезается в тело Луки, унося его в водоворот слизи!"
    #call skillcount2(3653)
    $ dmg = {0: (1650, 1750), 1: (1650, 1750), 2: (1650, 1750), 3: (1800, 1900)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label salamander_ng_start:
    $ monster_name = "Саламандра"
    $ lavel = "salamander_ng"
    $ img_tag = "salamander"
    $ tatie1 = "salamander st02"
    $ tatie2 = "salamander st02"
    $ tatie3 = "salamander st03"
    $ tatie4 = "salamander st02"
    $ haikei = "bg 115"
    $ monster_pos = center
    $ earth_keigen = 30
    $ ng = 2

    if Difficulty == 1:
        $ max_enemylife = 25000
        $ henkahp1 = 13500
    elif Difficulty == 2:
        $ max_enemylife = 25500
        $ henkahp1 = 13500
    elif Difficulty == 3:
        $ max_enemylife = 26500
        $ henkahp1 = 13500
        $ earth_keigen = 1
    elif Difficulty == 0:
        $ max_enemylife = 2
        $ henkahp1 = 1

    $ alice_skill = 0
    $ damage_keigen = 70
    $ kaihi = 90
    $ mus = 27
    $ tikei = 3
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "But Luka can't break free!"
    $ mogaku_anno2 = "Luka broke free!"
    $ mogaku_anno3 = "But Luka can't attack like this!"
    $ mogaku_sel1 = "What's wrong? That it? You'll lose in no time at this rate!"
    $ mogaku_sel2 = "Don't think you can break free from me so easily!"
    $ mogaku_sel3 = "Weak struggling like that won't ever break you free!"
    $ mogaku_sel4 = "If you don't try as hard as you can, you'll never break free..."
    $ mogaku_sel5 = "I can't lend my power to such weakness..."
    $ mogaku_dassyutu1 = "You finally broke away, eh?"
    $ mogaku_dassyutu2 = "It took you far too long... It seems like you're still too naive."
    $ mogaku_earth_dassyutu1 = "It seems as though you can use Gnome's power."
    $ mogaku_earth_dassyutu2 = ""
    $ half_s1 = "...И это всё?"
    $ half_s2 = "Покажи мне больше приёмов!"
    $ kiki_s1 = "Ты достиг своего предела?"
    $ kiki_s2 = "Кажется я ошибалась в тебе..."
    $ end_n = 7
    $ exp_minus = 80000
    $ zmonster_x = -330
    $ tukix = 370
    $ owaza_count1a = 5
    $ owaza_count1b = 5
    $ owaza_count1c = 5
    call face(param=2)
    call skillset(2)
    salamander "Хорошо...{w}\nПолучается, ты не трус."
    l "Я никогда не убегаю!{w}\nЕсли мне придётся победить тебя, чтобы ты присоединилась ко мне, то именно это я и собираюсь сделать!"
    salamander "Хм-м, ты мне уже нравишься.{w}\nДавай-ка посмотрим, действительно ли ты так силён, каким хочешь казаться."
    "Саламандра должна быть сильнейшим духом...{w}\nЕсли она действительно в этот раз будет сражаться всерьёз, то мне следует быть осторожным!"

    $ sinkou = 1
    $ charge = 0
    $ nostar = 2
    $ enemy_fire = 1
    $ enemy_fireturn = 9999
    jump common_main

label salamander_ng_nostar:
    call face(param=3)
    salamander "Я так не думаю!"
    call counter
    call show_skillname("Огненный шар")
    play sound "audio/se/fire1.ogg"
    "Саламандра быстро заряжает свою магию...{w}{nw}"
    play sound "audio/se/bom3.ogg"
    extend "\nПосле чего стреляет шаром огня прямо в звезду, уничтожая её!"
    jump salamander_ng_a

label salamander_ng_main:
    while True:
        call cmd("attack")

        if result == 1 and kousoku > 0:
            jump common_mogaku
        elif result == 1 and kousoku < 1:
            jump common_attack
        elif result == 2 and genmogaku > 0:
            jump common_mogaku
        elif result == 2 and genmogaku == 0:
            jump common_mogaku2
        elif result == 3:
            jump common_bougyo
        elif result == 4:
            jump common_nasugamama
        elif result == 5:
            jump salamander_ng_kousan
        elif result == 6:
            jump salamander_ng_onedari
        elif result == 7:
            jump common_skill

label salamander_ng_v:
    show salamander st13 with dissolve
    enemy "!?{w}\nКак это вообще возможно!?"
    enemy "Я проиграла..."
    show salamander st17 with dissolve
    enemy "......................"
    "Секунду Саламандра пристально смотрит на меня.{w}\nВ конце концов её взгляд смягчается."
    show salamander st16 with dissolve
    enemy "... Могу ли я доверить тебе использовать мою силу лишь во имя добра?{w}\nДействительно ли ты достоин этого?"
    show salamander st18 with dissolve
    l "Я никогда не использовал твою силу таким способом..."
    l "Мне нужна сила духов, чтобы защищать остальных, а не вредить им."
    "Эта единственная причина, по которой я вообще взял в руки меч..."
    show salamander st16 with dissolve
    salamander "...................{w}\nХорошо.{w} Я признаю своё поражение."
    show salamander st18 with dissolve
    call victory(viclose=1)
    $ getexp = 1500000
    call lvup
    $ persistent.salamander_unlock = 1
    $ skill04 = 2
    jump lb_0242

label salamander_ng001:
    call face(param=3)
    salamander "Чёрт возьми!{w}\nА ты и правда силён..."
    l "Видишь?{w}\nОчевидно же, что я достоин твоей силы."
    "Если уж я в первый раз заслужил её доверие, то нет никаких причин, чтобы она в этот раз отказалась! Ведь так?"
    call face(param=1)
    "И правда...{w}\nНе похоже, что я смогу победить этой тактикой..."
    l "Получается ты идёшь со мной?"
    salamander "...............{w}{nw}"
    call face(param=2)
    extend "\nУ меня есть более хорошая идея..."
    "Похотливая ухмылка расползается по её лицу."
    salamander "Лучше вместо грубой силы я перейду к атакам удовольствия..."
    l "Ой, да брось! Ты серьёзно!?"
    salamander "Если ты действительно силён, то это не должно составить никаких проблем. Правильно?"
    l "..............."
    "Почему.{w}\nНу почему всё должно быть так сложно?"
    "И похоже нет никакого способа избежать этой битвы..."
    $ earth_keigen = 5+gnome_buff
    $ hanyo[1] = 1

label salamander_ng_a:
    if enemylife < max_enemylife/2 and hanyo[1] < 1 and mylife > 1200:
        call salamander_ng001

    if kousoku < 1:
        $ sinkou = 0
    elif kousoku > 0:
        jump salamander_ng_a8

    if hanyo[1] < 1:
        $ ransu = rnd2(1,5)

        if ransu == 1:
            call salamander_ng_a1
            jump common_main
        elif ransu == 2:
            call salamander_ng_a2
            jump common_main
        elif ransu == 3:
            call salamander_ng_a3
            jump common_main
        elif ransu == 4:
            call salamander_ng_a4
            jump common_main
        elif ransu == 5:
            call salamander_ng_a5
            jump common_main
    
    elif hanyo[1] > 0:
        $ ransu = rnd2(1,4)

        if ransu == 1:
            call salamander_ng_a7
            jump common_main
        elif ransu == 2:
            call salamander_ng_a11
            jump common_main
        elif ransu == 3:
            call salamander_ng_a9
            jump common_main
        elif ransu == 4:
            call salamander_ng_a10
            jump common_main

label salamander_ng_a1:
    if charge > 0:
        jump salamander_ng_a
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Сможешь ли ты пережить это пламя?"
    elif ransu == 2:
        enemy "Надеюсь ты действительно настолько, иначе это будет больно..."
    elif ransu == 3:
        enemy "Будет немного больно..."

    call show_skillname("Сгори!")
    play sound "audio/se/fire1.ogg"
    "Воздух вокруг Луки воспламеняется!"
    $ ransu = rnd2(1,100)
    $ a_effect1 = 1
    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 26 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 46 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 21 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 21 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 41 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 16 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 16 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 36 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 11 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind != 4:
        $ wind_kakuritu = 10

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ dmg = {0: (600, 620), 1: (600, 620), 2: (750, 770), 3: (800, 820)}
    call damage(dmg[Difficulty])

    call hide_skillname
    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return


label salamander_ng_a2:
    if charge < 1:
        jump salamander_ng_a
    
    enemy "Тебе не пережить эту атаку!"

    call show_skillname("Метеорит")
    play sound "audio/se/dageki.ogg"

    "Саламандра высвобождает всю свою силу!"
    "Расплавленная глыба поднимается из лавы и падает на Луку!"

    pause 0.3
    window hide
    hide screen hp
    play sound "audio/se/bom3.ogg"
    show effect 002 ng zorder 30 with ImageDissolve("images/System/mask01.webp", 1.5, hard=True)
    show effect 002 ng zorder 30:
        align (0.5, 0.5)
        pause 1.0
        linear 1.5 zoom 3.0
    $ renpy.pause(1.6, hard=True)
    show color white
    hide effect with Dissolve(1.5)
    hide color with Dissolve(2.0)
    window show
    show screen hp
    window auto

    $ a_effect1 = 1

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 81 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 86 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 71 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 76 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 81 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 66 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 71 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 76 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 61 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ dmg = {0: (2000, 2200), 1: (2000, 2200), 2: (2600, 2800), 3: (3200, 3500)}
    call damage(dmg[Difficulty])

    call hide_skillname
    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return


label salamander_ng_a3:
    if charge > 0:
        jump salamander_ng_a

    enemy "Съешь это!"

    call show_skillname("Огненный кулак")
    play sound "audio/se/fire1.ogg"
    "Пламя пробегает вверх-вниз по руке Саламандры!{w}{nw}"
    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, апперкотом она бьёт Луку!"

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 101 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 81 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 76 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 86 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 71 + undine_buff:
            jump aqua_guard
    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 25
    elif wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    call skillcount2(3515)
    $ dmg = {0: (950, 1070), 1: (950, 1070), 2: (1150, 1310), 3: (1390, 1560)}
    call damage(dmg[Difficulty])

    call hide_skillname
    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label salamander_ng_a4:
    if charge > 0:
        jump salamander_ng_a

    enemy "Съешь это!"

    call show_skillname("Огненный пинок")
    play sound "audio/se/fire1.ogg"
    "Пламя пробегает вверх-вниз по ноге Саламандры!{w}{nw}"
    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, она пинает Луку!"

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 101 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 81 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 76 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 86 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 71 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 20
    elif wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 10

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    call skillcount2(3515)
    $ dmg = {0: (830, 960), 1: (830, 960), 2: (860, 990), 3: (910, 1060)}
    call damage(dmg[Difficulty])

    call hide_skillname
    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label salamander_ng_a5:
    if charge > 0:
        jump salamander_ng_a

    enemy "Этого ты не переживёшь!"

    call show_skillname("Огненный шар")
    play sound "audio/se/fire2.ogg"
    "Вспышка красного света вылетает из лавы в Луку!"

    pause 0.3
    window hide
    hide screen hp
    play sound "audio/se/bom3.ogg"
    show effect 002 ng zorder 30 with ImageDissolve("images/System/mask01.webp", 1.5, hard=True)
    show effect 002 ng zorder 30:
        align (0.5, 0.5)
        pause 1.0
        linear 1.5 zoom 3.0
    $ renpy.pause(1.6, hard=True)
    show color white
    hide effect with Dissolve(1.5)
    hide color with Dissolve(2.0)
    window show
    show screen hp
    window auto

    $ a_effect1 = 1

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 101 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 81 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 96 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 76 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 86 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 91 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 71 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 20
    elif wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 10

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ dmg = {0: (780, 890), 1: (780, 890), 2: (810, 930), 3: (870, 980)}
    call damage(dmg[Difficulty])

    call hide_skillname
    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return


label salamander_ng_a6:
    if charge > 0:
        jump salamander_ng_a

    enemy ".........................."

    call show_skillname("Разгон")
    play sound "audio/se/power.ogg"
    "Саламандра заряжает свою магию!"
    $ charge += 1
    $ serene = 0
    call hide_skillname

    return


label salamander_ng_a7:
    if charge > 0:
        jump salamander_ng_a

    $ owaza_count1b = 0
    $ owaza_num1 = 2

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Прикончат ли тебя мои объятья?"
    elif ransu == 2:
        enemy "Я хороша ещё и в обездвиживающих приёмах..."
    elif ransu == 3:
        enemy "Позволить мне подойти так близко... Ты всё ещё слишком неопытен!"

    call show_skillname("Объятия Огненного Духа")
    play sound "audio/se/ero_makituki5.ogg"

    "Саламандра рванулась вперёд и обняла Луку, втискивая его лицо в свою грудь!"

    $ ransu = rnd2(1,100)

    if aqua == 2 or (aqua > 2 and skill04 == 0):
        jump aqua_guard

    elif Difficulty < 2 and aqua > 2 and skill04 > 0 and ransu > 86:
        jump aqua_guard
        
    elif Difficulty == 2 and aqua > 2 and skill04 > 0 and ransu > 81:
        jump aqua_guard

    elif Difficulty == 3 and aqua > 2 and skill04 > 0 and ransu > 76:
        jump aqua_guard

    if wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 20
    elif wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 10

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    call skillcount2(3516)
    $ dmg = {0: (150, 250), 1: (150, 250), 2: (150, 250), 3: (250, 350)}
    call damage(dmg[Difficulty])
    $ kousoku = 2
    call status_print

    "Саламандра крепко обняла Луку!"

    call face(param=2)

    enemy "Хах!{w} Задыхайся в моих сиськах!"

    play sound "audio/se/ero_makituki5.ogg"

    "Саламандра зажала голову Луки между своей грудью!{w}{nw}"

    call damage(dmg[Difficulty], "second")
    play sound "audio/se/ero_simetuke1.ogg"

    "Она сильнее сжимает руки, обнимая Луку ещё крепче!{w}{nw}"

    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    if earth == 0 and Difficulty < 3:
        $ genmogaku = 3
    elif earth == 0 and Difficulty == 3:
        $ genmogaku = 4
    elif earth > 0 and Difficulty < 3:
        $ genmogaku = 0
    elif earth > 0 and Difficulty == 3:
        $ genmogaku = 1

    return

label salamander_ng_a8:
    if charge > 0:
        jump salamander_ng_a
    if charge > 0:
        jump salamander_ng_a
    enemy "Это конец..."

    call show_skillname("Киска Огненного Духа")
    show salamander hb1 with dissolve
    play sound "audio/se/ero_pyu5.ogg"

    "Саламанадра с силой садится на член Луки!{w}{nw}"
    $ kousoku = 2
    $ damage_kotei

    call skillcount2(3029)
    call damage(900, 950)

    play sound "audio/se/ero_simetuke1.ogg"

    "Она сильнее сжимает руки, обнимая Луку ещё крепче!{w}{nw}"

    $ dmg = {0: (45, 55), 1: (45, 55), 2: (35, 45), 3: (45, 55)}
    call damage(dmg[Difficulty], "last")
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    "Лука был изнасилован Саламандрой!"
    enemy "Хи-хи-хи...{w}\nТеперь тебе не сбежать..."
    $ sinkou += 1

    jump badend_h
    # jump salamander_ng_a8b

label salamander_ng_a9:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Мне хочется пососать его..."
    elif ransu == 2:
        enemy "Ммм...~"
    elif ransu == 3:
        enemy "Дай мне попробовать свою сперму..."

    call show_skillname("Минет Огненного Духа")
    play sound "audio/se/ero_makituki2.ogg"

    "Саламандра хватает член Луки!"

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 61 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 76 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 21 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 56 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 66 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 16 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 51 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 36 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 11 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    play sound "NGDATA/se/lewdsuck.ogg"

    call skillcount2(3516)
    $ dmg = {0: (800, 880), 1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
    call damage(dmg[Difficulty])
    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label salamander_ng_a10:
    enemy "Моя грудь очень хороша..."

    call show_skillname("Пайзури Огненного Духа")
    play sound "audio/se/ero_paizuri.ogg"

    "Саламандра с силой сжимает член Луки меж своих сисек!"

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 61 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 76 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 21 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 56 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 66 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 16 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 51 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 36 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 11 + undine_buff:
            jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    if wind != 4:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    play sound "audio/se/ero_paizuri.ogg"
    "После чего, она сдавливает свою грудь вокруг его члена!"
    play sound "audio/se/ero_pyu1.ogg"

    call skillcount2(3516)
    $ dmg = {0: (800, 880), 1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
    call damage(dmg[Difficulty])
    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label salamander_ng_a11:
    enemy "Моя грудь очень хороша..."

    call show_skillname("Бёдра Огненного Духа")
    play sound "audio/se/ero_paizuri.ogg"

    "Саламандра с силой сжимает член Луки меж своих бёдер!"

    $ ransu = rnd2(1,100)

    if Difficulty == 1:
        if skill04 == 0 and aqua > 2 and ransu < 21 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 46 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 11 + undine_buff:
            jump aqua_guard
    elif Difficulty == 2:
        if skill04 == 0 and aqua > 2 and ransu < 16 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 36 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 6 + undine_buff:
            jump aqua_guard
    elif Difficulty == 3:
        if skill04 == 0 and aqua > 2 and ransu < 11 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua == 2 and ransu < 26 + undine_buff:
            jump aqua_guard
        elif skill04 > 0 and aqua > 2 and ransu < 1 + undine_buff:
            jump aqua_guard

    if wind > 0:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind == 2:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    play sound "audio/se/ero_paizuri.ogg"
    "После чего, она сжимает ими его член со всей своей силой."

    call skillcount2(3516)
    $ dmg = {0: (800, 880), 1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
    call damage(dmg[Difficulty])
    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_start:
    call skillset(1)
    $ ng = 2
    $ monster_name = "Коакума"
    $ lavel = "koakuma_ng2"
    $ img_tag = "koakuma"
    $ tatie1 = "koakuma st02"
    $ tatie2 = "koakuma st02"
    $ tatie3 = "koakuma st01"
    $ tatie4 = "koakuma st02"
    $ haikei = "bg 084"
    $ monster_pos = center

    if Difficulty == 1:
        $ max_enemylife = 45000
    elif Difficulty == 2:
        $ max_enemylife = 50000
    elif Difficulty == 3:
        $ max_enemylife = 55000

    $ alice_skill = 0
    $ damage_keigen = 80
    $ earth_keigen = 20
    $ kaihi = 80
    $ mus = 30
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ tukix = 300
    $ zmonster_x = -270
    $ svillage_eventriri = 0
    $ knight_suc = 0

    if svillage_eventriri > 0:
        koakuma "Моя сестра рассказывала о тебе..."
        l "Э!?"
        koakuma "Она сказала, что твоё семя потрясающее..."
        koakuma "Теперь моя очередь её попробовать!"
    elif svillage_eventriri < 1:
        koakuma "В прошлый раз тебе просто повезло!{w}{nw}"
        if knight_suc == "Erubetie":
            extend "\nТогда тебя спасла эта сучка из соплей..."
        else:
            extend "\nМеня просто застали врасплох и я не знала, как использовать своих духов..."

        koakuma "Но теперь я закончу начатое дело!"

    koakuma "Зильфа! Одолжи мне свою силу!"
    play sound "audio/se/wind2.ogg"
    call show_skillname("Гниющий Ветер")
    syoukan zylphe st02
    $ enemy_wind = 1
    $ enemy_windturn = 16
    "Коакума призывает духа ветра!"
    call hide_skillname
    koakuma "Ты тоже, Гигамандер!"
    call show_skillname("Проклятое Пламя")
    play sound "NGDATA/se/fire2.ogg"
    syoukan gigamander st01
    $ enemy_fire = 1
    $ enemy_fireturn = 12
    "Коакума призывает духа огня!"
    call hide_skillname
    $ enemy_spiritcount = 2
    pause .5
    l "Гх..."
    "При ней до сих пор эти духи...{w}{nw}"
    if svillage_eventriri < 1:
        extend "\nЯ могу как и в прошлый раз просто держать оборону, пока они не пропадут."
    elif svillage_eventriri > 0:
        extend "\nВетер и огонь... она будет довольно быстрой и сильной.{w} Мне нужно быть осторожным!"

    $ nostar = 0
    jump common_main

label koakuma_ng2_main:
    if max_mylife < mylife * 5:
        call face(param=1)
    if max_mylife > mylife * 5:
        call face(param=2)
    $ enemy_noattack = 0
    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and aqua > 0:
        jump common_attack
    elif result == 1:
        jump alma_ng_miss
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5 and sinkou == 1:
        jump koakuma_ng2_kousan
    elif result == 7:
        jump common_skill
    elif result == 19:
        jump common_support

label koakuma_ng2_kousan:
    "Если я здесь сдамся, то никогда не получу зелёную сферу!"
    jump koakuma_ng2_main

label koakuma_ng2001:
    $ hanyo[8] = 1
    $ earth_keigen = 50
    call face(param=3)
    enemy "Ррррр..."
    enemy "Почему ты всё ещё сражаешься!?{w}\nТы хочешь, чтобы тебя трахнули или нет!?"
    l "Нет!{w}\nМне нужен этот приз! Я не проиграю!"
    enemy "... Хмпф.{w}\nТы не скажешь \"нет\" в этот раз..."
    enemy "Если ты не хочешь отдаваться мне...{w}\nТогда я просто разорву тебя в клочья!"
    enemy "Вперёд, Гигамандер!"
    call show_skillname("Проклятое Пламя")
    play sound "NGDATA/se/fire2.ogg"
    syoukan gigamander st01
    $ enemy_fire = 2
    $ enemy_fireturn = 9999
    "Коакума взывает к своему огненному духу, быстро востанавливая свои силы!"
    call hide_skillname
    l "Ээээ?"
    enemy "Больше никаких атак удовольствия.{w}\nТеперь я хочу просто выпотрошить тебя..."
    l "Ууу..."
    "Прекрасно.{w}\nПохоже я её серьёзно разозлил!"
    "Это будет больно...{w}\nЛучше держать Гному призванной."

    return

label koakuma_ng2002:
    $ hanyo[9] = 1
    show koakuma st01 with dissolve
    koakuma "Гхх...!"
    koakuma "Почему это происходит!?{w}\nПочему ты просто не можешь спокойно постоять на месте!?"
    l "Ты сама виновата в этом."
    l "Ты дала волю своей ярости и жажде крови...{w}\nДо тех пор, пока ты будешь так сражаться, я могу уклоняться от всего с помощью Безмятежного Разума."
    koakuma "Рррррр!"
    koakuma "Уклонись-ка от этого, маленький ублюдок!"
    pause .5
    play sound "audio/se/power.ogg"
    pause 1.0
    play sound "audio/se/fire2.ogg"
    "Коакума заряжает свой огонь магией!"
    l "Эээ!?"
    l "Что она делает!?{w}\nЧто МНЕ нужно делать!?"
    return

label koakuma_ng2003:
    call show_skillname("Извержение")
    play sound "audio/se/miss_wind.ogg"
    play sound "audio/se/miss.ogg"
    call face(param=3)
    "Коакума сокращает дистанцию и захватывает тело Луки!"
    koakuma "Если я проиграю, то хотя бы заберу тебя с собой..."
    "Тело Коакумы вспыхивает в мощном пламени!"
    pause 2.0
    play sound "audio/se/power.ogg"
    pause 2.0
    play sound "audio/se/bom3.ogg"
    hide koakuma
    show color white
    with Dissolve(3.0)
    pause 2.0
    show koakuma st02
    hide color
    with Dissolve(1.0)

    if aqua == 2:
        pause .5
        call show_skillname("Безмятежное движение")
        play sound "audio/se/miss_aqua.ogg"
        "Но Луке удаётся выскользнуть из её рук в последнюю секунду!"

        call hide_skillname

    if aqua != 2:
        $ a_effect1 = 1
        $ dmg = {0: (3500, 3800), 1: (3500, 3800), 2: (4200, 4500), 3: (5000, 5400)}
        call damage(dmg[Difficulty])

    play sound "audio/se/dageki.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=30)

    $ ransu = rnd2(12000,13500)

    $ enemylife -= ransu

    "Коакума получает [ransu] урона!"

    if mylife == 0:
        return_to badend_h
    jump koakuma_ng2_v

label koakuma_ng2_v:
    hide fire
    hide wind
    with dissolve
    call face(param=3)
    koakuma "Гах!{w}\nЯ не могу... больше... сражаться..."
    play sound "audio/se/down.ogg"
    hide koakuma with dissolve
    "Коакума падает на землю!"
    call victory(viclose=1)
    $ getexp = 3000000
    call lvup
    jump grandnoah_05

label koakuma_ng2_wind:
    $ enemy_noattack = 1
    koakuma "Зильфа, вернись сейчас же!"
    play sound "audio/se/wind2.ogg"
    call show_skillname("Гниющий Ветер")
    syoukan zylphe st02
    $ enemy_wind = 1
    $ enemy_windturn = 16
    $ enemy_spiritcount += 1
    "Сила ветра наполняет тело Коакумы, резко увеличивая её скорость!"
    call hide_skillname
    return

label koakuma_ng2_fire:
    $ enemy_noattack = 1
    koakuma "Грр...{w}\nПриди, Гигамандер!"
    call show_skillname("Проклятое Пламя")
    play sound "NGDATA/se/fire2.ogg"
    syoukan gigamander st01
    $ enemy_fire = 1
    $ enemy_fireturn = 12
    $ enemy_spiritcount += 1
    "Тело Коакумы нагревается, взрываясь в свирепом пламени!"
    call hide_skillname
    return

label koakuma_ng2_a:
    if hanyo[11] > 0:
        $ hanyo[11] -= 1
    if result == 19 and enemy_earth > 0:
        call koakuma_ab5b
    if enemylife < max_enemylife * 50 / 100 and hanyo[8] < 1:
        call koakuma_ng2001
        jump koakuma_ng2_ab

    if aqua < 1:
        jump koakuma_ng2_aa
    if hanyo[8] < 1:
        jump koakuma_ng2_aa
    elif hanyo[8] > 0:
        jump koakuma_ng2_ab

label koakuma_ng2_aa:
    if kousoku > 0:
        call koakuma_ng2_aa6
        jump common_main
    if enemy_wind < 1 and enemy_fire > 0:
        call koakuma_ng2_wind
        jump common_main
    while True:
        if enemy_wind > 0:
            $ ransu = rnd2(1,5)
        if enemy_wind < 1 and enemy_fire < 1:
            $ ransu = rnd2(1,20)
        if enemy_wind > 0 and enemy_fire < 1:
            $ ransu = rnd2(1,11)

        if ransu == 1:
            call koakuma_ng2_aa1
            jump common_main

        elif ransu == 2:
            call koakuma_ng2_aa2
            jump common_main

        elif ransu == 3 and hanyo[3] > 1:
            call koakuma_ng2_aa3
            jump common_main

        elif ransu == 4:
            call koakuma_ng2_aa4
            jump common_main

        elif ransu == 5:
            call koakuma_ng2_aa5
            jump common_main

        elif ransu < 17 and enemy_wind < 1:
            call koakuma_ng2_wind
            jump common_main

        elif 16 < ransu < 21 and enemy_fire < 1:
            call koakuma_ng2_fire
            jump common_main

label koakuma_ng2_ab:
    if is_damaged > 0 and hanyo[10] == 1:
        call koakuma_ng2_ab6c
    if hanyo[10] == 1:
        call koakuma_ng2_ab6b
        jump common_main
    if hanyo[9] > 0:
        call koakuma_ng2003
        jump koakuma_ng2_v
    if enemylife < 10000 and hanyo[9] < 1:
        call koakuma_ng2002
        jump common_main
    if kousoku > 0:
        call koakuma_ng2_aa6
        jump common_main
    if enemy_wind < 1 and enemy_fire > 0:
        call koakuma_ng2_wind
        jump common_main
    while True:
        if enemy_wind > 0:
            $ ransu = rnd2(1,6)
        if enemy_wind < 1 and enemy_fire < 1:
            $ ransu = rnd2(1,20)
        if enemy_wind > 0 and enemy_fire < 1:
            $ ransu = rnd2(1,11)

        if ransu == 1:
            call koakuma_ng2_ab1
            jump common_main

        elif ransu == 2:
            call koakuma_ng2_ab2
            jump common_main

        elif ransu == 3 and hanyo[3] > 1:
            call koakuma_ng2_ab3
            jump common_main

        elif ransu == 4:
            call koakuma_ng2_ab4
            jump common_main

        elif ransu == 5:
            call koakuma_ng2_ab5
            jump common_main

        elif ransu == 6 and enemylife < max_enemylife * 40 / 100 and hanyo[11] != 3:
            call koakuma_ng2_ab6a
            jump common_main

        elif ransu < 17 and enemy_wind < 1:
            call koakuma_ng2_wind
            jump common_main

        elif 16 < ransu < 21 and enemy_fire < 1:
            call koakuma_ng2_fire
            jump common_main

label koakuma_ng2_aa1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Можешь поиграть с моей ногой..."
    elif ransu == 2:
        enemy "Это всё, чего ты достоин...~{image=note}"
    elif ransu == 3:
        enemy "Надеюсь ты не кончишь от такого..."

    call show_skillname("Ножка Дьяволёнка")
    play sound "audio/se/ero_koki1.ogg"
    "Коакума сильно наступает на пах Луки!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 3 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 11 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Ножка Дьяволёнка")

    else:
        $ dmg = {0: (380, 400), 1: (380, 400), 2: (420, 440), 3: (470, 490)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "Опуская свою ногу чуть ниже, она массирует мошонку Луки!"

    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Ножка Дьяволёнка")
    else:
        $ dmg = {0: (380, 400), 1: (380, 400), 2: (420, 440), 3: (470, 490)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_aa2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Я очень хорошо владею своими ручками..."
    elif ransu == 2:
        enemy "Кончишь ли ты от этого?"
    elif ransu == 3:
        enemy "Всё, что мне нужно - это мои пальцы..."

    call show_skillname("Ручка Дьяволёнка")
    play sound "audio/se/ero_koki1.ogg"
    "Коакума быстро сокращает дистанцию до Луки!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and ransu < 11 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Ручка Дьяволёнка")

    else:
        "Она наклоняет руку к его паху и хватает член Луки!"

        $ dmg = {0: (380, 400), 1: (380, 400), 2: (420, 440), 3: (470, 490)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_koki1.ogg"
    "Коакума плотно обхватывают член Луки и начинает невероятно быстро двигать руками!"

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Ручка Дьяволёнка")

    else:
        $ dmg = {0: (380, 400), 1: (380, 400), 2: (420, 440), 3: (470, 490)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_aa3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Мужчины пускают слюни от одного вида моих ног..."
    elif ransu == 2:
        enemy "Просто кончи уже."
    elif ransu == 3:
        enemy "Мои бёдра довольно мягкие..."

    call show_skillname("Бёдра Дьяволёнка")
    play sound "audio/se/ero_paizuri.ogg"
    "Коакума насильно сжимает член Луки меж своих обнажённых бёдер и начинает сжимать их!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 3 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 11 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Бёдра Дьяволёнка")

    else:
        $ dmg = {0: (400, 450), 1: (400, 450), 2: (450, 480), 3: (800, 850)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_paizuri.ogg"
    "Она начинает быстро тереть своими тёплыми, пухлыми бёдрами о член Луки!"

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Бёдра Дьяволёнка")

    else:
        $ dmg = {0: (400, 450), 1: (400, 450), 2: (450, 480), 3: (800, 850)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_aa4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Мои сиськи довольно упругие..."
    elif ransu == 2:
        enemy "Возможно они немного маленькие, но..."
    elif ransu == 3:
        enemy "Моя грудь очень мягкая..."

    call show_skillname("Поглаживание плоской грудью")
    play sound "audio/se/ero_paizuri.ogg"
    "Коакума сжимает член Луки меж своих сисек!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty < 3 and ransu < 16 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 11 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Поглаживание плоской грудью")

    else:
        $ dmg = {0: (400, 450), 1: (400, 450), 2: (450, 480), 3: (800, 850)}
        call damage(dmg[Difficulty])

    if mylife == 0:
        return_to badend_h

    if enemy_wind < 1:
        call hide_skillname
        return

    play sound "audio/se/ero_paizuri.ogg"
    "После чего, она поглаживает его лицо своими сосками!"

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Поглаживание плоской грудью")
    else:
        $ dmg = {0: (400, 450), 1: (400, 450), 2: (450, 480), 3: (800, 850)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_aa5:
    if not hanyo[4] < 1:
        $ ransu = rnd2(1,100)
        if ransu < 51:
            jump koakuma_ng2_a

    $ hanyo[4] = 1

    if not kousan == 2:
        enemy "Хватит уже уклоняться!"

    call show_skillname("Сковывание")
    play sound "audio/se/down.ogg"
    "Коакума толкает Луку на землю!{w}{nw}"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 96 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 86 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 76 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 71 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 61 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 51 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Сковывание")
    else:
        $ kousoku = 1
        if earth > 0:
            $ genmogaku = 0
        if earth < 1:
            $ genmogaku = 3

        call status_print
        call face(param=2)

        "После чего она садится на его лицо!"
        l "Ммпфм!?"
        "Маленькая суккуба сидит на моём лице.{w}\nЕё мягкая попка, едва прикрытая чёрными трусиками, прямо перед моими глазами."
        if surrender > 0:
            call koakuma_ng2_aa6
        enemy "Бьюсь об заклад, что ты получаешь от такого унижения удовольствие, да?"
        enemy "... Что ж, тогда не сиди как статю.{w}\nБудет намного веселее, если ты будешь барахтаться~!{image=note}"

    call hide_skillname

    return

label koakuma_ng2_aa6:
    $ sinkou += 1
    if not kousan == 2:
        enemy "А теперь...{w}\nПостони ради меня..."
    call show_skillname("Унижение от Дьяволёнка")
    play sound "audio/se/ero_koki1.ogg"
    "Коакума обхватывает Луку своими ногами и нажимает на его пах!"
    call damage((300,350))
    if mylife == 0:
        return_to badend_h
    "Она сжимает его член меж своих пальцев на ногах!"
    call damage((300,350))
    if mylife == 0:
        return_to badend_h
    "Она насильно захватывает член Луки меж своих мягких ступней и начинает их сжимать!"
    call damage((300,350))
    if mylife == 0:
        return_to badend_h

    call hide_skillname
    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "Отведай этого!"

    call show_skillname("Удар Дракона")
    play sound "audio/se/fire1.ogg"
    "Пламя начинает пробегаться вверх и вниз по руке Коакумы!{w}{nw}"
    play sound "audio/se/dageki.ogg"
    "После чего она наносит апперкот Луке!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 96 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 91 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 86 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 71 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 61 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Удар Дракона")
    else:
        $ a_effect1 = 1
        call damage((1150,1310))

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "Подавись этим!"

    call show_skillname("Пинок Дракона")
    play sound "audio/se/fire1.ogg"
    "Пламя начинает пробегаться вверх и вниз по ноге Коакумы!{w}{nw}"
    play sound "audio/se/dageki.ogg"
    "После чего она наносит пинок Луке!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 96 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 91 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 86 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 71 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 66 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 61 + undine_buff:
        jump aqua_guard

    if wind == 4:
        $ wind_kakuritu = 1
    elif wind > 0 and wind != 4 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Пинок Дракона")
    else:
        $ a_effect1 = 1
        call damage((1150,1310))

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "Ветер разорвёт твоё тело на клочки!"

    call show_skillname("Шамшир")
    play sound "audio/se/wind3.ogg"
    "Коакума призывает шквал ветрянных клинков!"
    pause 2.0
    play sound "audio/se/slash.ogg"
    show effect 015 zorder 30
    $ renpy.pause(0.2, hard=True)
    play sound "audio/se/slash.ogg"
    show effect 017 zorder 30

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 6 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется от всех лезвий словно торнадо!"
            call wind_guard
            call show_skillname("Шамшир")
    else:
        $ a_effect1 = 1
        call damage((950,1000))

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "После такого ты сам будешь умолять меня изнасиловать тебя..."

    call show_skillname("Дикий огонь")
    play sound "audio/se/fire2.ogg"
    "Коакума накапливает энергию..."
    play sound "audio/se/bom3.ogg"
    with flash
    "Всё её тело вспыхивает в белом пламени, покрывая весь Коллизей в мощном огне!"
    enemy "А теперь...{w}\nСгори!"
    play sound "audio/se/power.ogg"
    pause 2.0
    play sound "audio/se/karaburi.ogg"
    "Она делает выпад вперёд, выставляя свою руку...{w}{nw}"
    play sound "audio/se/bom2.ogg"
    "После чего, из неё вылетает мощный огненный шар!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 96 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 91 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 86 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 56 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 51 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 46 + undine_buff:
        jump aqua_guard

    if wind > 0:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но ветер сдувает огненный шар!"
            call wind_guard
            call show_skillname("Дикий огонь")
    else:
        $ a_effect1 = 1
        call damage((1550,1710))

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab5:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "Какое прелестное личико..."

    call show_skillname("Когти Дьявола")
    play sound "audio/se/karaburi.ogg"
    "Коакума делает выпад вперёд, готовая нанести удар!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 96 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 2 and ransu < 91 + undine_buff:
        jump aqua_guard
    elif aqua == 2 and Difficulty == 3 and ransu < 86 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 1 and ransu < 61 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 56 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 51 + undine_buff:
        jump aqua_guard

    if wind > 0:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветра отталкивает её назад!"
            call wind_guard
            call show_skillname("Когти Дьявола")
    else:
        pause .5
        play sound "audio/se/slash.ogg"
        pause .5
        play sound "audio/se/slash.ogg"
        pause .5
        play sound "audio/se/slash.ogg"
        "Она безжалостно царапает лицо и тело Луки!"

        $ a_effect1 = 1
        call damage((1550/3,1650/3))
        $ a_effect1 = 1
        call damage((1550/3,1650/3))
        $ a_effect1 = 1
        call damage((1550/3,1650/3))

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab6a:
    $ hanyo[10] = 1
    call face(param=3)
    enemy "Гигамандер, Зильфа!{w}\nДаруйте мне свою силу!"
    call show_skillname("Гниющий Ветер")
    syoukan zylphe st02
    $ enemy_wind = 1
    $ enemy_windturn = 16
    pause .5
    call show_skillname("Проклятое Пламя")
    syoukan gigamander st01
    $ enemy_fire = 1
    $ enemy_fireturn = 12
    "Коакума призывает обоих духов разом!"
    play sound "NGDATA/se/fire2.ogg"
    play sound2 "audio/se/wind2.ogg"
    show effect 008 as fire zorder 30
    show effect wind as wind zorder 30
    pause 2.0
    "Коакума заряжает все свои силы, глядя на Луку!"
    enemy "Ты должен был просто сдаться и принять поражение...{w}\nА теперь ты сгоришь.."
    call hide_skillname
    return

label koakuma_ng2_ab6b:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    call face(param=3)
    enemy "Отведай-ка этого!"

    call show_skillname("Блицкриг Адского Пламени")
    hide koakuma
    play sound "audio/se/karaburi.ogg"
    call face(param=3)
    "С силой, заряженной на полную катушку, Коакума делает рывок в сторону Луки!"

    $ guard_break = 1
    $ a_effect1 = 1
    $ dmg = {0: (2850, 3050), 1: (2850, 3050), 2: (3350, 3550), 3: (3850, 4050)}
    call damage(dmg[Difficulty])

    call hide_skillname
    hide fire
    hide wind
    with dissolve

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label koakuma_ng2_ab6c:
    $ hanyo[10] = 0
    $ hanyo[11] = 3
    hide fire
    hide wind
    with dissolve
    call face(param=3)
    enemy "Гррр!"
    "Моя атака отбрасывает Коакуму назад в связи с чем она теряет концентрацию!"
    enemy "Рррр...{w}\nЧёрт тебя побери!"

    return

label granberia_ng2_start:
    $ monster_name = "Гранберия"
    $ lavel = "granberia_ng2"
    $ img_tag = "granberia"
    $ tatie1 = "granberia st41"
    $ tatie2 = "granberia st42"
    $ tatie3 = "granberia st07"
    $ tatie4 = "granberia st42"
    $ haikei = "bg 084"
    $ monster_pos = center
    $ earth_keigen = 57
    $ ng = 2
    $ nocharge = 1
    call skillset(1)

    if Difficulty == 1:
        $ max_enemylife = 45000
        $ henkahp1 = 8000
    elif Difficulty == 2:
        $ max_enemylife = 50000
        $ henkahp1 = 9000
    elif Difficulty == 3:
        $ max_enemylife = 55000
        $ henkahp1 = 10000
        $ earth_keigen = 25
    elif Difficulty == 0:
        $ max_enemylife = 1

    $ alice_skill = 0
    $ damage_keigen = 70
    $ kaihi = 80
    $ mus = 25
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "But Luka can't break free from her tail!"
    $ mogaku_anno2 = "Luka broke free!"
    $ mogaku_anno3 = "But Luka can't attack like this!"
    $ mogaku_sel1 = "It isn't that easy to escape from the Monster Lord..."
    $ mogaku_sel2 = "Will you be finished that easily...?"
    $ mogaku_sel3 = "You won't escape... I'll squeeze you more..."
    $ mogaku_sel4 = "What's wrong? Is this all you had...?"
    $ mogaku_sel5 = "What pitiful struggling... Hehe."
    $ mogaku_earth_dassyutu1 = "Gnome's power truly is strong..."
    $ mogaku_earth_dassyutu2 = "To think you could even break loose from me..."
    $ end_n = 9
    $ zmonster_x = -270
    $ tukix = 320
    $ exp_minus = 1370025

    g "Пришло время закончить то, с чего мы начали в Илиасбурге...{w}\nПриготовься, Лука!"
    call show_skillname("Ярость")
    play sound "audio/se/fire4.ogg"
    show fire with dissolve
    show granberia st42 with dissolve
    show effect 008 with dissolve
    pause .5
    hide fire
    hide effect 008 with dissolve
    call hide_skillname
    l "Гах...!"
    "Эта битва не будет такой же, как и в прошлый раз...{w}\nОна уже улучшила свои навыки по сравнению с первой битвой!"
    g "Никаких святых сил!{w}\nТолько твоё умение обращаться с мечом!"
    l "... Хорошо.{w}\nЯ принимаю твой вызов, Гранберия!"
    "Если я смог победить её в прошлый раз, то и в этот смогу!"
    jump common_main


label granberia_ng2_main:
    call cmd("attack")

    if result == 1 and aqua > 0:
        jump common_attack
    elif result == 1 and aqua < 1:
        jump granberia_ng2_miss
    elif result == 2 and earth > 0 and genmogaku > 0:
        jump mogaku_earth1
    elif result == 2 and earth > 0 and genmogaku == 0:
        jump mogaku_earth2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump granberia_ng2_kousan
    elif result == 6:
        jump granberia_ng2_onedari
    elif result == 7:
        jump common_skill

label granberia_ng2_nostar:
    call granberia_ng2_a10
    jump granberia_ng2_main

label granberia_ng2_edging:
    show granberia st02 with dissolve
    g "Что это, чёрт возьми, за приём такой!?"
    l "Ух-ох...{w}\nОбычно я получаю возможность снова атаковать после его использования..."
    "Возможно я как-то не продумал данный ход..."
    show granberia st04 with dissolve
    g "... Хорошо.{w}\nЕсли ты так хочешь этого..."
    show granberia st41 with dissolve
    g "Тогда я просто тебя убью!"
    play sound "audio/se/karaburi.ogg"
    show effect 006a zorder 30
    pause .3
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=15)
    $ mylife = 0
    pause .5
    stop music fadeout 1.0
    $ renpy.show(monocro("images/Backgrounds/bg 084.webp",204,0,0))
    $ renpy.show(monocro("images/Characters/granberia/granberia st41.webp",204,0,0))
    with dissolve
    "Я чувствую, как острый клинок пронзает насквозь моё тело. Моё сознание начинает постепенно угасать..."
    hide screen hp
    $ nobtn = 1
    scene bg black with Dissolve(2.0)
    $ bad1 = "Будучи полным придурком, коим он, собственно, и является, Лука попытался использовать крайность против Гранберии."
    $ bad2 = "Она просто убила его за это."
    jump badend

label granberia_ng2_miss:
    $ before_action = 3
    $ ransu = rnd2(1,3)
    if ransu == 1:
        ori "Хаа!"
    elif ransu == 2:
        ori "Ораа!"
    elif ransu == 3:
        ori "Получай!"

    "Лука атакует!"
    call mp_kaihuku
    play sound "audio/se/karaburi.ogg"
    call kaihi

    "Но Гранберия уклоняется!"

    if wind == 3:
        "Лука атакует!"
        play sound "audio/se/karaburi.ogg"
        call kaihi
        "Но Гранберия уклоняется!"

    if first_sel3 == 1:
        jump granberia_ng2_a

    l "Ах!{w}\nЯ промахнулся!"

    g "Ты действительно надеялся, что это будет так легко?{w}\nАтаки, подобно этой, сродни укусам комаров!"

    "Обычные атаки против неё бесползены...{w}\nМожет быть для начала стоит успокоить свой разум..."

    jump granberia_ng2_a


label granberia_ng2_miss2:
    $ before_action = 4
    $ ransu = rnd2(1,3)

    if ransu == 1:
        ori ".............."
    elif ransu == 2:
        ori "Вижу!"
    elif ransu == 3:
        ori "Вверяя свой меч потоку..."

    call show_skillname("Клинок Стоячей Воды")

    "Меч Луки, сверкая, направляется к противнику!"
    call mp_kaihuku
    play sound "audio/se/karaburi.ogg"
    call kaihi
    "Но Гранберия легко уклоняется!"

    jump granberia_ng2_a

label granberia_ng2001:
    show granberia st07 with dissolve
    g "Даже сейчас... я всё ещё проигрываю!"
    g "Хорошо...{w}{nw}"
    show granberia st41 with dissolve
    extend "\nТогда попробуем несколько другую тактику!"
    show granberia st42 with dissolve
    call show_skillname("Ярость")
    play sound "audio/se/fire4.ogg"
    show fire with dissolve
    show granberia st42 with dissolve
    show effect 008 with dissolve
    pause .5
    "Языки пламени пробегают по мечу Гранберии, полностью восстанавливая её боевой дух!"
    hide fire with dissolve
    hide effect 008 with dissolve
    $ damage = max_enemylife/3
    call enemylife_kaihuku
    call hide_skillname
    l "Э?"
    "\"Несколько другую тактику\"?{w}\nЧто она имеет в виду?"

    $ hanyo[3] = 1
    return

label granberia_ng2_chargecancel:
    enemy "Это не сработает против меня!"
    call counter
    call show_skillname("Сбитие разгона")
    play sound "audio/se/karaburi.ogg"
    "Гранберия делает быстрый выпад вперёд и наносит удар по Луке!"
    $ dmg = {0: (350, 400), 1: (350, 400), 2: (450, 500), 3: (550, 600)}
    call damage(dmg[Difficulty])
    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if hanyo[1] < 1:
        jump granberia_ng2_chargecancel2

    jump granberia_ng2_a

label granberia_ng2_chargecancel2:
    $ hanyo[1] = 1
    l "Гхх!{w}\nЧто за...!?"
    enemy "Я видела, как ты используешь эту технику бесчисленное число раз.{w}\nЯ не позволю тебе победить такими трусливыми методами!"
    l "Аххх..."
    "Она не позволяет разгонять мою энергию...{w}\nЧто же тогда мне делать?"
    jump granberia_ng2_a

label granberia_ng2_v:
    show granberia st07 with dissolve
    enemy "Да!{w}\nНевероятно!"
    hide granberia with dissolve
    play sound "audio/se/down.ogg"
    "Гранберия падает лицом на землю!"
    stop music fadeout 1.0
    l "!?"
    l "Это значит...{w}\nЯ выиграл?"

    call victory(viclose=1)

    $ getexp = 1500000
    call lvup

    jump grandnoah_06


label granberia_ng2_a:
    if enemylife < max_enemylife/2 and hanyo[3] != 1:
        call granberia_ng2001

    if enemylife == 0:
        jump granberia_ng2_v

    if hanyo[3] != 1:

        while True:
            $ ransu = rnd2(1,9)

            if aqua == 0 and ransu < 3:
                call granberia_ng2_a1
                jump common_main
            elif aqua == 0 and 2 < ransu < 5:
                call granberia_ng2_a2
                jump common_main
            elif aqua == 0 and 4 < ransu < 7:
                call granberia_ng2_a3
                jump common_main

            elif aqua > 0 and ransu < 2:
                call granberia_ng2_a1
                jump common_main
            elif aqua > 0 and 3 < ransu < 5:
                call granberia_ng2_a2
                jump common_main
            elif aqua > 0 and ransu < 7:
                call granberia_ng2_a3
                jump common_main

            elif ransu == 7:
                call granberia_ng2_a4
                jump common_main
            elif ransu == 8:
                call granberia_ng2_a5
                jump common_main
            elif ransu == 9:
                call granberia_ng2_a6
                jump common_main

    if hanyo[3] > 0:

        while True:
            if aqua == 0:
                $ ransu = rnd2(10,13)
            else:
                $ ransu = rnd2(10,18)

            if aqua > 0 and ransu == 10:
                call granberia_ng2_a7
                jump common_main

            elif aqua == 0 and ransu == 10:
                call granberia_ng2_a8
                jump common_main
            elif aqua == 0 and ransu == 11:
                call granberia_ng2_a8
                jump common_main
            elif aqua == 0 and ransu == 12:
                call granberia_ng2_a9
                jump common_main
            elif aqua == 0 and ransu == 13:
                call granberia_ng2_a10
                jump common_main
            elif aqua > 0 and ransu == 11:
                call granberia_ng2_a8
                jump common_main
            elif aqua == 0 and ransu == 11:
                call granberia_ng2_a9
                jump common_main
            elif hanyo[0] < 1 and ransu == 11:
                call granberia_ng2_a9
                jump common_main

            elif hanyo[0] > 0 and wind > 0 and ransu == 11:
                call granberia_ng2_a10
                jump common_main

            elif aqua > 0 and ransu == 12:
                call granberia_ng2_a10
                jump common_main
            elif aqua > 0 and ransu > 12:
                call granberia_ng2_a7
                jump common_main

label granberia_ng2_a1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как насчёт этого!?"
    elif ransu == 2:
        enemy "А вот и я!"
    elif ransu == 3:
        enemy "Сможешь ли ты увидеть эту атаку!?"

    call show_skillname("Техника Проклятого Меча: Обезглавливание")
    play sound "audio/se/tuki.ogg"

    "Гранберия быстро делает шаг вперёд и нацеливает мощный взмах мечом на шею Луки!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 75 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 75 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 55 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ a_effect1 = 1
    $ dmg = {0: (910, 1050), 1: (910, 1050), 2: (1100, 1160), 3: (1300, 1390)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng2_a2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хаа!"
    elif ransu == 2:
        enemy "Почувствуй всю мощь моего меча своим телом!"
    elif ransu == 3:
        enemy "Я не дам тебе ни единой поблажки!"

    call show_skillname("Атака Дракона-Мясника")
    play sound "audio/se/karaburi.ogg"
    "Меч Гранберии обрушивается на Луку!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 35 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 35 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 25 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 10
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 5

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ guard_break = 1
    $ a_effect1 = 1
    $ dmg = {0: (980, 1100), 1: (980, 1100), 2: (1120, 1150), 3: (1240, 1290)}
    call damage(dmg[Difficulty])
    call hide_skillname

    $ guard_break = 0

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng2_a3:
    enemy "Со всей моей силой...!"

    call show_skillname("Демонический Крушитель Черепов: Очищение")
    window hide
    hide screen hp
    play sound "audio/se/karaburi.ogg"
    show effect 003 zorder 30 with dissolve
    pause 0.3
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)
    show screen hp
    window show
    play sound "audio/se/bom2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=25)
    window auto

    "Гранберия прыгает в воздух и обрушивается вниз!"

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 45 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 35 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 25 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4 and Difficulty < 2:
        $ wind_kakuritu = 20
    elif wind > 0 and wind != 4 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind > 0 and wind != 4 and Difficulty == 3:
        $ wind_kakuritu = 10

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    "Мощный удар обрушивается на голову Луки!"

    play sound "audio/se/damage2.ogg"

    $ guard_break = 1
    $ a_effect1 = 1
    $ dmg = {0: (1120, 1260), 1: (1120, 1260), 2: (1290, 1420), 3: (1450, 1600)}
    call damage(dmg[Difficulty])
    call hide_skillname

    $ guard_break = 0

    if mylife == 0:
        return_to badend_h

    return


label granberia_ng2_a4:
    $ owaza_count3c = 0

    enemy "Мой наилучший приём... Неудержимый Испепеляющий Клинок!"

    call show_skillname("Неудержимый Испепеляющий Клинок")

    "Меч Гранберии замелькал, проводя бесчисленные атаки!"

    pause 0.5
    call skill16a

    $ ransu = rnd2(1,100)

    if hanyo[2] < 1:
        if aqua > 0 and Difficulty < 2 and ransu < 60 + undine_buff:
            jump aqua_guard
        elif aqua > 0 and Difficulty == 2 and ransu < 60 + undine_buff:
            jump aqua_guard
        elif aqua > 0 and Difficulty == 3 and ransu < 55 + undine_buff:
            jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "Но Лука уклоняется словно торнадо!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        #$ damage_kotei = 1
        $ a_effect1 = 1
        $ dmg = {0: (435, 490), 1: (435, 490), 2: (500, 570), 3: (600, 690)}
        call damage(dmg[Difficulty], "first")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "Но Лука уклоняется словно торнадо!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        #$ damage_kotei = 1
        $ a_effect1 = 1
        $ dmg = {0: (435, 490), 1: (435, 490), 2: (500, 570), 3: (600, 690)}
        call damage(dmg[Difficulty], "second")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "Но Лука уклоняется словно торнадо!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        #$ damage_kotei = 1
        $ a_effect1 = 1
        $ dmg = {0: (435, 490), 1: (435, 490), 2: (500, 570), 3: (600, 690)}
        call damage(dmg[Difficulty], "last")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "Но Лука уклоняется словно торнадо!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        #$ damage_kotei = 1
        $ a_effect1 = 1
        $ dmg = {0: (435, 490), 1: (435, 490), 2: (500, 570), 3: (600, 690)}
        call damage(dmg[Difficulty], "first")

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 5
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind > 0:
        $ sel1 = "Но Лука уклоняется словно торнадо!"
        call wind_guard
        call show_skillname("Неудержимый Испепеляющий Клинок")
    else:
        play sound "audio/se/slash.ogg"
        #$ damage_kotei = 1
        $ a_effect1 = 1
        $ dmg = {0: (435, 490), 1: (435, 490), 2: (500, 570), 3: (600, 690)}
        call damage(dmg[Difficulty], "second")

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng2_a5:
    $ ransu = rnd2(1,2)

    if ransu == 1:
        enemy "Следуя течению... Безмятежный Демонический Клинок!"
    elif ransu == 2:
        enemy "Я рассеку всё на своём пути... Безмятежный Демонический Клинок!"

    call show_skillname("Безмятежный Демонический Клинок")
    play sound "audio/se/aqua2.ogg"

    show granberia st04 with dissolve
    "Гранберия вкладывает свой меч в ножны.{w}{nw}"
    show granberia st42
    call skill13a

    extend "\nИ резко вытаскивая из ножн, прорезает всё на своём пути!"

    if hanyo[2] < 1:
        if aqua > 0:
            jump granberia_ng2_a5a

        if wind > 0 and Difficulty < 2:
            $ wind_kakuritu = 3
        elif wind > 0 and Difficulty == 2:
            $ wind_kakuritu = 2
        elif wind > 0 and Difficulty == 3:
            $ wind_kakuritu = 1

        call wind_guard_kakuritu

        if wind_guard_on != 0:
            if wind > 0:
                $ sel1 = "Но Лука уклоняется словно торнадо!"
                jump wind_guard

    "Атака Гранберии рассекает голову Луки в мгновение ока!"

    play sound "audio/se/damage2.ogg"

    $ a_effect1 = 1
    $ dmg = {0: (1330, 1540), 1: (1330, 1540), 2: (1480, 1700), 3: (1690, 1830)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng2_a5a:
    hide ct with dissolve
    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.show(haikei, at_list=[miss2])

    "Лука пытается уклониться!"
    extend "\nНо атака настигает его быстрее, чем он отреагировал!"

    $ a_effect1 = 1
    $ dmg = {0: (770, 840), 1: (770, 840), 2: (850, 930), 3: (940, 1050)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng2_a6:
    enemy "Получай!"

    "Наполнив свой меч силой Земли, Гранберия обрушивает его на Луку!"

    call show_skillname("Сотрясающее Землю Обезглавливание")
    call skill12a

    if hanyo[2] < 1:
        $ ransu = rnd2(1,100)

        if aqua > 0 and Difficulty < 2 and ransu < 60 + undine_buff:
            jump aqua_guard
        elif aqua > 0 and Difficulty == 2 and ransu < 50 + undine_buff:
            jump aqua_guard
        elif aqua > 0 and Difficulty == 3 and ransu < 45 + undine_buff:
            jump aqua_guard

        if wind > 0 and Difficulty < 2:
            $ wind_kakuritu = 3
        elif wind > 0 and Difficulty == 2:
            $ wind_kakuritu = 2
        elif wind > 0 and Difficulty == 3:
            $ wind_kakuritu = 1

        call wind_guard_kakuritu

        if wind_guard_on != 0:
            if wind > 0:
                $ sel1 = "Но Лука уклоняется словно торнадо!"
                jump wind_guard

    "Мощный удар обрушивается на голову Луки!"

    play sound "audio/se/damage2.ogg"
    $ a_effect1 = 1
    $ dmg = {0: (1750, 1960), 1: (1750, 1960), 2: (1950, 2170), 3: (2150, 2400)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng2_a7:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    enemy "Этого будет достаточно, чтобы сокрушить тебя!"

    call show_skillname("Волновая мясорубка")
    play sound "audio/se/aqua.ogg"
    "Гранберия запечатывает силу воды, отрезая Луку от потока!"
    play sound "audio/se/slash.ogg"
    show effect 005_02 zorder 30
    hide effect with Dissolve(0.4)
    play sound "audio/se/slash.ogg"
    show effect 005_03 zorder 30
    hide effect with Dissolve(0.4)
    play sound "audio/se/slash.ogg"
    show effect 003 zorder 30
    hide effect with Dissolve(0.4)

    $ a_effect1 = 1
    $ dmg = {0: (660, 770), 1: (660, 770), 2: (780, 900), 3: (920, 1050)}
    call damage(dmg[Difficulty])

    $ a_effect1 = 1
    $ dmg = {0: (660, 770), 1: (660, 770), 2: (780, 900), 3: (920, 1050)}
    call damage(dmg[Difficulty])

    $ a_effect1 = 1
    $ dmg = {0: (660, 770), 1: (660, 770), 2: (780, 900), 3: (920, 1050)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if aqua > 0:
        $ aqua_turn -= 1
    elif aqua == 2:
        $ aqua_end = 1

    return

label granberia_ng2_a8:
    $ ransu = rnd2(1,3)

    if ransu == 1:
        enemy "Сила грозы в моей руке!"
    elif ransu == 2:
        enemy "О, дух воды... наполни мой меч силой самого грома!"
    elif ransu == 3:
        enemy "Выдержишь ли ты этот удар!?"

    call show_skillname("Клинок Молний")
    play sound "NGDATA/se/thunderstrike.ogg"
    with flash
    "Гранберия поднимает свой меч на головой, после чего в него ударяет молния!"
    play sound "NGDATA/se/electric.ogg" loop
    show granberia elec with dissolve

    "Меч Гранберии наделяется силой молнии!{w}\nПосле чего она наносит удар, усиленный огнём и молнией, по Луке!"
    play sound "audio/se/karaburi.ogg"
    show effect thunder zorder 30
    pause 1.0
    hide effect with ImageDissolve("images/System/mask01.webp", 0.5)

    pause .3

    if hanyo[2] < 1:
        $ ransu = rnd2(1,100)

        if aqua > 0 and Difficulty == 1 and ransu < 25 + undine_buff:
            call face(param=1)
            jump aqua_guard
        elif aqua > 0 and Difficulty == 1 and ransu < 15 + undine_buff:
            call face(param=1)
            jump aqua_guard
        elif aqua > 0 and Difficulty == 1 and ransu < 10 + undine_buff:
            call face(param=1)
            jump aqua_guard

        if wind > 0 and Difficulty < 2:
            $ wind_kakuritu = 35
        elif wind > 0 and Difficulty == 2:
            $ wind_kakuritu = 30
        elif wind > 0 and Difficulty == 3:
            $ wind_kakuritu = 25

        call wind_guard_kakuritu

        if wind_guard_on != 0:
            if wind > 0:
                $ sel1 = "Но Лука, используя силу Сильфы, уходит от едва задевающей его атаки!"
                jump wind_guard

    $ a_effect1 = 1
    $ dmg = {0: (2520, 2800), 1: (2520, 2800), 2: (2900, 3300), 3: (3400, 3900)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        call face(param=2)
        return_to badend_h

    call face(param=1)

    return

label granberia_ng2_a9:
    enemy "Тебе не пережить эту атаку!"

    call show_skillname("Раскалывающий удар")
    play sound "audio/se/karaburi.ogg"
    play sound "audio/se/damage2.ogg"
    with Quake((0, 0, 0, 0), 0.1, dist=30)

    "Наполнив свой меч силой Земли, Гранберия вонзает его в землю!"
    play sound "audio/se/earth2.ogg"
    with Quake((0, 0, 0, 0), 1.5, dist=30)
    "Земля раскалывается под Лукой!"
    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty < 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 2 and ransu < 3 + undine_buff:
        jump aqua_guard
    elif aqua > 0 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty < 2:
        $ wind_kakuritu = 3
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 2
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука с помощью силы ветра отпрыгивает от раскола!"
            jump wind_guard

    play sound "audio/se/down.ogg"
    "Лука срывается в расщелину!"
    g "Попался..."
    play sound "audio/se/power.ogg"
    "Гранберия полностью заряжает свой меч силой земли!"
    play sound "audio/se/karaburi.ogg"
    call skill13a
    play sound "audio/se/bom3.ogg"
    with Quake((0, 0, 0, 0), 2.5, dist=30)
    "После чего, наносит удар прямо по голове Луки!"

    $ a_effect1 = 1

    $ dmg = {0: (2870, 3100), 1: (2870, 3100), 2: (3130, 3500), 3: (3540, 3900)}
    call damage(dmg[Difficulty])
    call hide_skillname

    if mylife == 0:
        return_to badend_h

    return

label granberia_ng2_a10:
    $ hanyo[2] = 1
    show granberia st04 with dissolve
    enemy "................."
    call show_skillname("Концентрация")
    play sound "audio/se/aqua2.ogg"
    pause 1.5
    play sound "audio/se/power.ogg"
    $ enemy_aqua = 1
    "Гранберия концентрирует всю свою магию воды для достижения абсолютной безмятежности!"
    $ ransu = rnd2(1,3)
    call face(param=1)
    if ransu == 1:
        call granberia_ng2_a4
    elif ransu == 2:
        call granberia_ng2_a5
    elif ransu == 3:
        call granberia_ng2_a6

    $ hanyo[2] = 0
    $ enemy_aqua = 0
    call hide_skillname
    if mylife == 0:
        return_to badend_h
    return

label granberia_ng2_kousan:
    "Если я сдамся, она сразу же меня прикончит..."
    jump granberia_ng2_main


label granberia_ng2_onedari:
    "Она не тот противник, кого я могу попросить об атаке!"
    jump granberia_ng2_main

label alma_ng2_start:
    $ ng = 2
    $ monster_name = "Альма Эльма"
    $ lavel = "alma_ng2"
    $ img_tag = "alma_elma"
    $ tatie1 = "alma_elma st21"
    $ tatie2 = "alma_elma st22"
    $ tatie3 = "alma_elma st23"
    $ tatie4 = "alma_elma st24"
    $ haikei = "bg 225"
    $ monster_pos = center

    $ earth_keigen  = 50
    if Difficulty == 1:
        $ max_enemylife = 42000
        $ henkahp1 = 14000
    elif Difficulty == 2 :
        $ max_enemylife = 46000
        $ henkahp1 = 16000
    elif Difficulty == 3:
        $ max_enemylife = 50000
        $ henkahp1 = 25000
        $ earth_keigen  = 25
    $ alice_skill = 0
    $ damage_keigen = 85
    $ kaihi = 80
    $ mus = 28
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ end_n = 9
    $ exp_minus = 1370025
    $ zmonster_x = -270
    $ tukix = 320

    "Хорошо, просто расслабься, Лука...{w}\nТы уже побеждал её до этого. Поэтому в этот раз не должно возникнуть никаких проблем! Ведь так?"
    enemy "Фу-фу-фу...{w}\nА вот и я, малыш Лука~{image=note}!"
    enemy "Хмм, ты не собираешься снимать кольцо?{w}\nТогда хорошо. Мне же будет легче тебя победить...~{image=note}"
    $ enemy_wind = 2
    $ enemy_windturn = 9999
    jump common_main

label alma_ng2_main:
    if result == 15:
        jump alma_ng2_edging

    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and aqua > 0:
        jump common_attack
    elif result == 1:
        jump alma_ng2_miss
    elif result == 2 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and genmogaku == 0:
        jump common_mogaku2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5 and sinkou == 1:
        jump alma_ng2_kousan
    elif result == 6 and sinkou == 1:
        jump alma_ng2_onedari
    elif result == 7:
        jump common_skill

label alma_ng2_chargecancel:
    alma "Не в этот ра-а-з~{image=note}!"
    call counter
    play sound "audio/se/miss.ogg"
    play sound2 "audio/miss_wind.ogg"
    hide alma_elma
    pause .1
    call face(param=2)
    "Альма Эльма стремительно приближается к Луке, пока он беззащитен!"
    play sound "audio/se/down.ogg"
    pause .7
    play sound "audio/se/ero_paizuri.ogg"
    "Прежде чем Лука успел отреагировать, она уже успела повалить его на землю и зажать его член меж своих сисек!"
    alma "А теперь... насладить моей грудью~{image=note}."
    play sound "audio/se/ero_paizuri.ogg"
    show alma_elma hh1 with dissolve
    "Альма начинает быстро двигать своей грудью верх-вниз!{w}{nw}"
    $ dmg = {0: (500, 550), 1: (500, 550), 2: (600, 650), 3: (700, 750)}
    call damage(dmg[Difficulty])
    play sound "audio/se/ero_paizuri.ogg"
    "Она начинает двигать ими верх-вниз поочерёдно!{w}{nw}"
    $ dmg = {0: (500, 550), 1: (500, 550), 2: (600, 650), 3: (700, 750)}
    call damage(dmg[Difficulty])
    play sound "audio/se/ero_paizuri.ogg"
    "Альма сдавливает член Луки и начинает трясти его!"
    $ dmg = {0: (500, 550), 1: (500, 550), 2: (600, 650), 3: (700, 750)}
    call damage(dmg[Difficulty])
    call hide_skillname
    call face(param=1)
    if mylife == 0:
        return_to badend_h
    if aqua != 2 and hanyo[0] > 0 and hanyo[2] < 1:
        $ hanyo[2] = 1
        call alma_ng2003

    jump alma_ng2_a

label alma_ng2_miss:
    $ before_action = 3
    $ ransu = rnd2(1,3)
    if ransu == 1:
        l "Хаа!"
    elif ransu == 2:
        l "Хья!"
    elif ransu == 3:
        l "Ора!"
    "Лука атакует!"

    call mp_kaihuku
    pause .5
    play sound "audio/se/wind2.ogg"
    hide alma_elma
    pause .5
    call face(param=1)

    "Но Альма Эльма легко уворачивается!"

    if wind == 3:
        "Лука атакует!"
        call mp_kaihuku
        pause .5
        play sound "audio/se/wind2.ogg"
        hide alma_elma
        pause .5
        call face(param=1)

        "Но Альма Эльма легко уворачивается!"

    if first_sel3 == 1:
        jump alma_ng2_a

    $ first_sel3 = 1
    alma "Слишком медленно, малыш Лука..."

    "Дерьмо...{w}\nНе думаю, что в таком духе я по ней попаду..."

    $ first_sel3 = 1

    jump alma_ng2_a

label alma_ng2_v:
    call face(param=3)

    alma "... Ауч."
    play sound "audio/se/down.ogg"
    "Альма Эльма падает на колени."
    alma "... Что ж.{w}\nВидимо я больше не смогу продолжать битву."
    call face(param=1)
    alma "Полагаю - это твоя победа."
    l "Э!?{w}{nw}"
    if aqua == 2:
        stop music fadeout 1.0
        show bg 225 with Dissolve(1.5)
    extend "Я выиграл!?"
    alma "Ага~{image=note}!"
    alma "Поздравляю, малыш Лука. Ты превзошёл даже мои ожидания."
    alma "Конечно немного обидно быть побеждённой в таком месте...{w}\nНо... думаю не стоит надрывать позвоночник ради этого."
    l ".............."
    "Не нравится мне эта метафора..."
    call victory(viclose=1)
    $ getexp = 3500000
    call lvup
    jump grandnoah_07

label alma_ng2001:
    call face(param=3)
    alma "... Хмпф."
    "Альма смотрит на меня с немного раздражённым выражением лица."
    alma "Знаешь...{w}\nНе очень весело сражаться с тобой, когда у тебя есть Сильфа с Ундиной."
    l "Просто сдайся, Альма!{w}\nТы ведь знаешь, что тебе не победить меня!"
    "Я уже давным давно превзошёл её по силе!{w}\nНет ни единого способа, которым она могла бы победить меня!"
    "... По крайней мере, пока со мной мои духи."
    alma "... Ммм.{w}{nw}"
    $ tatie1 = "alma_elma st51"
    $ tatie2 = "alma_elma st52"
    $ tatie3 = "alma_elma st53"
    $ tatie4 = "alma_elma st54"
    play sound "audio/se/escape.ogg"
    call face(param=2)
    extend "\nТы в этом уверен?"
    l "Уууу."
    "... Ух-ох."
    call face(param=1)
    alma "Фу-фу-фу..."
    call show_skillname("Экстремальный Ветер")
    play sound "audio/se/wind2.ogg"
    show effect wind as wind_699 with Dissolve(1.0)
    pause 1.0
    "Свирепый ветер обволакивает всё тело Альмы!"
    $ enemy_wind = 4
    $ enemy_windturn = 9999
    call hide_skillname
    l "Гах...!"
    "Что это!?"
    "Бушующий ветер наполнил всю арену.{w}\nЯ никогда такого раньше ещё не видел..."
    alma "Ты не должен быть таким высокомерным, малыш Лука..."
    alma "Я хотела лишь немного с тобой поиграть, но теперь я хочу преподать тебе урок...~{image=note}"
    l "!!!"
    "Преподать мне урок?{w}\nЧто она собирается делать...?"

    return

label alma_ng2002:
    l ".............."
    pause .5
    call show_skillname("Безмятежный Разум")
    show color white with dissolve
    pause 1.5
    "Но Лука реагирует в мгновение ока и без всяких усилий уклоняется."
    hide color with Dissolve(1.5)
    if hanyo[1] < 1:
        $ hanyo[1] = 1
        $ hanyo[2] = 1
        call alma_ng2002b

    return

label alma_ng2002b:
    l "................."
    "Нежное, знакомое чувство обволакивает меня."
    "Такое ощущение, будто само время замедлило свой ход вокруг меня.{w}\nДаже несмотря на высокую скорость Альмы, в этом состоянии я смог прочувствовать её и уклониться."
    "От неё, пусть и слабо, но ощущается небольшая жажда крови..."
    syoukan undine st01
    undine "Похоже тебе всё ещё не достаёт практики.{w}\nХотя, думаю, это не удивительно..."
    undine "Достигнуть совершенного Безмятежного Разума очень тяжело...{w}\nНо тебе придётся поддерживать это состояние, если ты хочешь победить против неё."
    undine "Для того, чтобы мы были так близки, наши души должны быть соединены очень прочно... это означает, что остальные духи отрезаны от нас.{w}\nБудь очень осторожен с тем, как ты используешь эту силу."
    "Да, я понимаю...{w}\nСпасибо, Ундина."
    "Таким образом, даже мои самые сокровенные мысли могут быть услышаны ею.{w}\nНаш ум и тело сейчас полностью синхронизированны в единое целое."
    alma "О боже.{w}\nЭто..."
    "Голос Альма для моих ушей - просто неразборчивый шум, пока битва будет продолжаться."

    return

label alma_ng2003:
    call hide_skillname
    l "Гах...!"
    "Что это было!?{w}\nКак будто она просто телепортировалась!"
    "Она всегда была такой быстрой...?{w}\nНеужели всё это время она скрывала свою силу?"
    syoukan salamander st13
    salamander "Ты не должен был задевать её гордость.{w}\nТы вёл себя с ней надмено, поэтому теперь она хочет проучить тебя."
    l "Да что я такого сказал?{w}\nЯ ничего такого не имел в виду..."
    salamander "В любом случае, если ты хочешь победить, тебе нужно как-то уклоняться от её атак."

    if aqua > 0:
        l "Но сила Ундины не помогает!"
        salamander "Ну это не удивительно."

    if svillage > 0:
        salamander "Альма Эльма - не Коакума.{w}\nОна намного лучше скрывает свою ярость и жажду крови..."

    else:
        salamander "Альма Эльма очень хороша в сокрытии своей жажды крови, даже когда она в ярости..."

    l "Но тогда...{w}\nУ меня нет других способов!"
    salamander "Да неужели?{w}\nЕсть ещё одна техника, которую ты не попробовал..."
    l "Ещё одна техника...?"
    if aqua < 1:
        l "Для уклонения от атак...?"

    pause 1.0
    l "!!!"
    "Кажется я понял, о чём она!"
    l "Ясно! Спасибо, Саламандра!"

    return

label alma_ng2004:
    l "................."
    "Нежное, знакомое чувство обволакивает меня."
    "Такое ощущение, будто само время замедлило свой ход вокруг меня.{w}\nДаже несмотря на высокую скорость Альмы, в этом состоянии я могу чувствовать исходящие потоки от её сердца и предсказывать движения."
    "От неё, пусть и слабо, но ощущается небольшая жажда крови..."
    enemy "Мммм, ясненько...{w}\nЭто твоё лучшее состояние безмятежности, да...?"
    enemy "Интересно, что же быстрее...{w}\nМой ветер или твоя вода...?"
    enemy "Хе-хе, давай-ка проверим это~{image=note}."
    call show_skillname("Мело Софи Теллус: Тёмная техника")
    play sound "audio/se/miss.ogg"
    hide alma_elma
    pause .1
    call face(param=2)

    "Альма Эльма настолько быстро двигается к Луки, что её не видно!"

    "................."

    call show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    "Лука следует исходящим потокам от Альмы Эльмы...{w}{p=1.0}... Но он не успевает уклониться вовремя!"
    call hide_skillname
    play sound "audio/se/down.ogg"
    $ a_effect1 = 1
    call damage((100,130))
    "Прежде чем Лука успевает отреагировать, она толкает его на землю и сажимает его член меж своих сисек!"

    alma "А теперь... насладись моей грудью~{image=note}."

    call show_skillname("Мело Софи Теллус: Тёмная техника")

    play sound "audio/se/ero_koki1.ogg"

    show alma_elma hh1 with dissolve

    "Альма начинает быстро двигать своей грудью верх-вниз!{w}{nw}"

    $ dmg = {0: (250, 275), 1: (250, 275), 2: (300, 325), 3: (350, 375)}
    call damage(dmg[Difficulty])

    "Она начинает двигать ими верх-вниз поочерёдно!{w}{nw}"

    $ dmg = {0: (250, 275), 1: (250, 275), 2: (300, 325), 3: (350, 375)}
    call damage(dmg[Difficulty])

    "Альма сдавливает член Луки и начинает трясти его!"

    $ dmg = {0: (250, 275), 1: (250, 275), 2: (300, 325), 3: (350, 375)}
    call damage(dmg[Difficulty])

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    stop music fadeout 1.0
    $ renpy.show(haikei)
    with Dissolve(1.5)
    $ aqua = 0
    $ aqua_turn = 0
    $ aqua_end = 0
    play music "NGDATA/bgm/alma.ogg"
    l "Ах...!"
    "Нет...{w}\nЯ смог увидеть эту атаку в своём разуме перед тем, как Альма начала двигаться...{w}\nНо даже двигаясь вместе с потоком, я всё ещё недостаточно быстр..."
    enemy "Ха-ха...{w}\nТы прямо как Гранберия, малыш Лука~{image=note}..."
    enemy "Не важно, умеешь ты предсказывать мои движения заранее или нет, пока твоё тело не будет двигаться с той же скоростью, что и моё."
    enemy "Как бы печально это не звучало, но это твой предел.{w}\nА теперь я хочу унизить тебя перед всей этой толпой..."
    l "Гах..."
    "Теперь даже скорости Сильфы не хватает, чтобы угнаться за Альмой...{w}\nОна начинает двигаться раньше, чем я..."
    "Она так быстра, что я даже не могу читать её движения, пока не сфокусируюсь на достижении абсолютной безмятежности..."
    "... Но даже если я его достигаю, то я уже не могу угнать за её скоростью!{w}\nДолжен быть какой-то другой выход..."
    syoukan sylph st01
    sylph "Лука!{w}\nТебе нужна моя сила, чтобы победить её!"
    l "Я знаю!{w}\nНо этого всё ещё недостаточно..."
    "Мне нужно достигнуть абсолютной безмятежности, чтобы у меня был хоть какой-то шанс против Альмы...{w}\n... Но при его достижении я отрезаю себя от силы Сильфы и уже не смогу двигаться достаточно быстро!"
    "Но я не могу призвать их обеих!{w}\nИли могу...?"
    syoukan undine st01
    undine "Я чувствую решимость в твоём сердце, Лука.{w}\nЯ уверена, с этой решительностью ты сможешь использовать всю мою силу без потери скорости."
    l "Но как...!?"
    "Как я могу поддерживать настолько глубокую связь с Ундиной, не теряя чувства ветра?"
    syoukan sylph st01
    sylph "Ты сможешь, Лука!{w}\nВетер, и всё живое не планете...{w}\nВсе элементы природы... они связаны с потоками!"
    sylph "Даже ветер является его частью!"
    sylph "Разве ты не знаешь, как выглядит поток ветра, Лука?{w}\nКогда ты входишь в состояние безмятежности, просто не теряй мой образ и пусть сам поток ветра унесёт нас вдоль течения!"
    sylph "Просто войди в своё состояние безмятежности, используя мою силу.{w}\nЯ помогу тебе!{w}\nОбещаю!"
    l "Понял..."
    "Мне просто нужно иметь при себе Сильфы, когда я вхожу в абсолютное безмятежное состояние...{w}\nЕсли она поможет мне, тогда я смогу изменить исход этой битвы!"
    enemy "Ох...{w}\nМне нравится этот решительный блеск в твоих глазах, малыш Лука..."
    enemy "Хе-хе-хе... хочу увидеть их снова, когда я сокрушу тебя~{image=note}."
    $ skill_serenegale = 1
    $ hanyo[12] = 3

    return

label alma_ng2005:
    $ hanyo[11] = 1
    ".................."
    "... Смогу ли я почувствовать поток ветра?{w}\nДухи сказали, что я смогу..."
    "Чтобы у меня был хоть какой-то шанс против Альмы, мне нужно использовать одновременно силы ветра и воды на максимуме их возможностей.{w}\nЯ не могу здесь проиграть!"
    l "Принять течение ветра в моё сердце...!"
    call show_skillname("Безмятежный Разум: Буря")
    stop music fadeout 1.0
    play sound "audio/se/aqua.ogg"
    show color white as wind_698 with dissolve
    show bg black
    show effect wind as wind_698 with Dissolve(1.5)
    pause 1.0
    play music "audio/bgm/aqua.ogg"
    play sound2 "audio/se/wind2.ogg" loop
    $ aqua = 2
    $ wind = 5
    $ earth = 0
    $ fire = 0
    $ fire_turn = 0
    $ earth_turn = 0
    $ aqua_turn = mp
    $ wind_turn = mp
    call hide_skillname
    l "Э-это..."
    "Как и до этого, я могу чувствовать все потоки вокруг меня...{w}\n... Но теперь, вдобавок к этому, я могу чувствовать течение ветра, окружающее и быстро несущие меня вперёд..."
    "Я целиком и полностью вверил своё сердце течению, не разрывая связь с ветром...{w}\nНет...{w} Я не просто не разорвал его, но и усилил."
    "Такое чувство, будто само моё сердце соединено с потоками ветра...{w}\nБудто бы я стал полностью един с ветром..."
    "Вместо того, чтобы время замедлялось вокруг меня, такое ощущение, будто мой разум всё быстрее и быстрее разгоняется..."
    syoukan undine st01
    undine "Используя безмятежное состояние, ты преобразовал своё сердце и разум в сам ветер."
    undine "Прямо сейчас, ты перешагнул предел человеческого тела.{w}\nДаже Генриху не удалось достигнуть такого состояния..."
    undine "Теперь скорость каждой клетки твоего тела и разума резко возросла, достигнув скорости ветра.{w}\nДаже твоё сердцебиение ускорилось до опасного уровня..."
    undine "Думаю очевидно, что это создаёт огромную нагрузку на твоё тело. Поэтому твоё SP теряет в два раза быстрее по сравнению с предыдущим состоянием."
    undine "Тебе нужно закончить эту битву как можно быстрее..."
    undine "С другой стороны, ты получаешь огромные преимущества в обмен на риск.{w}\nТеперь ты не только способен уклоняться от всех движений Альмы, но и все твои навыки теперь будут выполняться дважды."
    undine "Как и раньше, ты сможешь выйти из этого состояния через Безмятежный Демонический Клинок, который будет усилен также силой ветра."
    undine "Ты также сможешь в любое время перейти в 3 уровень моей силы, но тогда твоя связь с ветром будет разорвана."
    syoukan sylph st01
    sylph "Я могу чувствовать течение твоего сердца, Лука!{w}\nДавай вместе победим Альму и покажем ей, кто здесь чемпионы!"
    l "Спасибо вам..."
    "Прямо сейчас, все три сердца соединены воедино...{w}\nДаже невероятная скорость Альма Эльмы теперь не остановит нас!"
    enemy "О боже.{w}\nЭто..."
    "Голос Альмы едва доходит до крошечного пятнышка сознания, поскольку мой разум всё быстрее ускоряется."
    "Пока мой разум и тело будут двигаться со скоростью ветра, битва будет продолжаться!"

    return

label alma_ng2006:
    l ".............."
    call show_skillname("Безмятежное Движение: Буря")
    stop music fadeout 1.0
    play sound "audio/se/aqua.ogg"
    show color white with dissolve
    pause 1.5
    play sound "audio/se/miss_wind.ogg"
    "Но Лука читает движения Альмы и, следуя потокам ветра, без труда уклоняется!"
    hide color white with Dissolve(1.5)
    l "...!"
    "Когда Альма промахивает мимо Луки, его меч, сверкая, инстиктивно направляется в её сторону!"
    play sound "audio/se/slash4.ogg"
    $ abairitu = 440 + mylv * 4
    call enemylife
    call hide_skillname
    if enemylife < 1:
        jump alma_ng2_v

    return

label alma_ng2_a:
    if hanyo[0] == 0:
        jump alma_ng2_aa

    elif hanyo[0] == 1:
        jump alma_ng2_ab

label alma_ng2_aa:
    $ hanyo[4] -= 1
    $ hanyo[3] += 1
    if hanyo[13] > 0:
        $ hanyo[13] -= 1

label alma_ng2_aax:
    if enemylife < 1:
        jump alma_ng2_v

    if hanyo[0] < 1 and enemylife < (max_enemylife * 66 / 100) and (aqua > 0 or wind > 0):
        $ hanyo[0] = 1
        call alma_ng2001
        jump common_main

    if kousoku > 0 and hanyo[4] < 1:
        call alma_ng2_aa10
        jump common_main
    elif kousoku > 0 and hanyo[4] > 0:
        call alma_ng2_aa11
        jump common_main

    if mp > 7:
        $ ransu = rnd2(1,100)

        if ransu < 31:
            call alma_ng2_aa1
            jump common_main

    if mp > 4 and enemylife < (max_enemylife / 2):
        $ ransu = rnd2(1,100)

        if ransu < 41:
            call alma_ng2_aa1
            jump common_main

    while True:
        $ ransu = rnd2(1,10)

        if ransu == 1 and mp > 2:
            call alma_ng2_aa1
            jump common_main

        elif ransu == 2:
            call alma_ng2_aa2
            jump common_main

        elif ransu == 3 and hanyo[3] > 1:
            call alma_ng2_aa3
            jump common_main

        elif ransu == 4:
            call alma_ng2_aa4
            jump common_main

        elif ransu == 5:
            call alma_ng2_aa5
            jump common_main

        elif ransu == 7:
            call alma_ng2_aa7
            jump common_main

        elif ransu == 8 and hanyo[4] < 1:
            call alma_ng2_wind
            jump common_main

        elif ransu == 9 and status == 0:
            call alma_ng2_wind2
            jump common_main

        elif ransu == 10 and kousoku < 1:
            call alma_ng2_aa9
            jump common_main

label alma_ng2_ab:
    if aqua == 2 and hanyo[10] < 1:
        $ hanyo[10] = 1
        $ hanyo[2] = 1
        call alma_ng2004
        jump common_main

    if hanyo[9] > 0:
        $ hanyo[9] -= 1

    if hanyo[12] > 0:
        $ hanyo[12] -= 1

    if hanyo[14] > 0:
        $ hanyo[14] -= 1

    if kousoku > 0 and hanyo[8] > 0:
        call alma_ng2_ab4a
        jump common_main
    elif kousoku > 0 and hanyo[8] > 1:
        call alma_ng2_ab4b
        jump common_main

    $ ransu = rnd2(1,100)

    if ransu < 60 and mp > 4:
        call alma_ng2_ab5
        jump alma_ng2_ac

    if ransu > 59 and hanyo[9] < 1:
        call alma_ng2_ab6
        jump alma_ng2_ac

    jump alma_ng2_ac

label alma_ng2_ac:
    $ hanyo[7] = 0
    $ hanyo[8] = 0

    while True:
        $ ransu = rnd2(1,5)

        if ransu == 1 and mp > 2:
            call alma_ng2_ab1
            jump common_main

        elif ransu == 2:
            call alma_ng2_ab2
            jump common_main

        elif ransu == 3 and aqua == 0:
            call alma_ng2_ab3
            jump common_main

        elif ransu == 3 and hanyo[12] < 1 and hanyo[5] < 1:
            call alma_ng2_ab3
            jump common_main

        elif ransu == 4 and hanyo[12] < 1 and aqua != 2:
            call alma_ng2_ab4
            jump common_main

        elif ransu == 4 and hanyo[12] < 1 and hanyo[6] < 1:
            $ hanyo[6] = 1
            call alma_ng2_ab4
            jump common_main

        elif ransu == 5 and kousoku < 1:
            call alma_ng2_aa9
            jump common_main

label alma_ng2_aa1:
    if hanyo[13] > 0:
        jump alma_ng2_aax

    $ hanyo[13] = 3

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты будешь просто моей едой..."
    elif ransu == 2:
        enemy "Я могу использовать хвост..."
    elif ransu == 3:
        enemy "Это просто предварительная ласка..."

    call show_skillname("Хвост Суккуба: Поглощение Магии")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    if daten > 0:
        jump skill22x

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 35
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 30
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 25

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    "Её хвост засасывает член Луки внутрь!{w}\nТаинственная сила истощает его!"

    if Difficulty == 1:
        $ ransu = rnd2(3,4)

    elif Difficulty == 2:
        $ ransu = rnd2(5,6)

    elif Difficulty == 3:
        $ ransu = rnd2(8,10)

    play sound "audio/se/down2.ogg"
    $ mp -= ransu
    if mp < 0:
        $ mp = 0

    "Лука теряет [ransu] SP!"

    call damage((200,250))

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa2:
    if first_sel1 == 0:
        $ first_sel1 = 1
    python:
        while True:
            ransu = rnd2(1,2)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Как насчёт этого?"
    elif ransu == 2:
        enemy "А вот и я!"

    call show_skillname("Хвост Суккуба: Поглощение Здоровья")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 35
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 30
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 25

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    "Её хвост начинает сильно сосать член Луки!"

    $ dmg = {0: (1110, 1150), 1: (1110, 1150), 2: (1230, 1290), 3: (1350, 1420)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_wind:
    $ hanyo[4] = 1

    alma "Я ослаблю тебя своим хвостом..."
    play sound "audio/se/tuki.ogg"

    call show_skillname("Хвост Суккуба: Поглощение Уровня")

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 35
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 30
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 25

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    if daten > 0:
        jump skill22x

    "Хвост начинает сосать член Луки!{w}\nУровень Луки был поглащён!"

    play sound "audio/se/down2.ogg"

    if Difficulty == 1:
        $ drain = 3
        call lvdrain

    elif Difficulty == 2:
        $ drain = 4
        call lvdrain

    elif Difficulty == 3:
        $ drain = 5
        call lvdrain

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if mylv == 1:
        return_to badend_h

    return

label alma_ng2_wind2:
    $ hanyo[0] = 2

    alma "Не хочешь немного поиграть...?"

    call show_skillname("Феромоны Суккуба")
    play sound "audio/se/wind1.ogg"

    "Опьяняющий аромат воздействует на сознание Луки!"

    if daten > 0:
        jump skill22x

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 50
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 50
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 50

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но ветер сдувает аромат в сторону!"
            jump wind_guard

    $ status = 1
    $ status_turn = 2

    "Лука поддаётся искушнию Альмы!"

    return

label alma_ng2_aa3:
   #call skillcount2(3027)

    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хе-хе... Ты собираешься испачкать мою руку своей спермой?"
    elif ransu == 2:
        enemy "Я не могу ничего поделать с желанием подразнить его.{image=note}"
    elif ransu == 3:
        enemy "Хе-хе... Приятно, правда?"

    call show_skillname("Умелые ручки")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма дотрагивается своими руками до члена Луки!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    $ dmg = {0: (1100, 1120), 1: (1100, 1120), 2: (1210, 1240), 3: (1320, 1360)}
    call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa4:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Приёмы моих рук заставят тебя корчиться и биться в экстазе..."
    elif ransu == 2:
        enemy "Ни один человек не может стерпеть моих пальчиков..."
    elif ransu == 3:
        enemy "Что ты об этом думаешь, а? Это ручное мастерство Королевы Суккубов..."

    call show_skillname("Древние Техники Суккубов: Змея")
    play sound "audio/se/ero_koki1.ogg"

    "Все десять пальчиков Альмы пробегаются верх-вниз по члену Луки!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Древние Техники Суккубов: Змея")
    else:
        "Её пальчики трогают ползают по члену Луки будто змея!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Древние Техники Суккубов: Змея")
    else:
        extend "\nВсе её десять пальцев извиваются по всему члену!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Древние Техники Суккубов: Змея")
    else:
        extend "\nПосле чего она начинает выжимать его!"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa5:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Хорошенько испробуй мастерство ротика самой Королевы Суккубов..."
    elif ransu == 2:
        enemy "Я высосу из тебя всё, хочешь ты этого или нет..."
    elif ransu == 3:
        enemy "Я усосу тебя в подчинение..."

    call show_skillname("Дух Романус Тери")
    play sound "audio/se/ero_buchu3.ogg"

    "Альма Эльма берёт член Луки в рот и начинает сосать!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 20 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 15 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Дух Романус Тери")
    else:

        "Её язык начинает вылизывать весь член Луки!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Дух Романус Тери")
    else:

        extend "\nМягкий язык Альмы сдавливает член Луки!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Дух Романус Тери")
    else:

        extend "\nОбвив его своим мокрым языком, Альма Эльма начинает сильно сосать член Луки!"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa6:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Даже мои груди - оружие..."
    elif ransu == 2:
        enemy "Мои груди приносят сказочное наслаждение..."
    elif ransu == 3:
        enemy "Я утоплю тебя в моих грудях..."

    call show_skillname("Мело Софи Теллус")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма сдавливает член Луки между своими большими сиськами!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 2 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 1 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Мело Софи Теллус")
    else:

        "Зефирные прикосновения окружают Луку со всех сторон!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Мело Софи Теллус")
    else:

        extend "\nАльма Эльма медленно сжимает свои сиськи вокруг члена Луки!{w}{nw}"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Мело Софи Теллус")
    else:

        extend "\nПосле чего она начинает двигать своей грудью вверх-вниз!"

        $ dmg = {0: (420, 450), 1: (420, 450), 2: (470, 510), 3: (530, 580)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa7:
    $ owaza_count3a = 0
    alma "Погрузись в агонию удовольствия от моего секретного навыка!"

    call show_skillname("Церемония Суккуба")

    "Альма Эльма хватает Луку сзади!"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 10 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 5 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 2 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 1 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 1 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 1 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:

        # call skillcount2(3636)
        play sound "audio/se/ero_koki1.ogg"

        "Обхватив Луку сзади, она начинает поглаживать его член!{w}{nw}"

        $ dmg = {0: (180, 230), 1: (180, 230), 2: (210, 235), 3: (260, 300)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:
        play sound "audio/se/ero_paizuri.ogg"

        extend "\nБыстро сбивая Луку с ног, она обхватывает его член своими сиськами и начинает сдавливать его!{w}{nw}"

        $ dmg = {0: (190, 205), 1: (190, 205), 2: (220, 245), 3: (270, 310)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:
        play sound "audio/se/ero_sigoki1.ogg"

        extend "\nКогда Лука пытается встать, Альма Эльма быстро вскакивает и, наступая на его член, прижимает вновь к земле!{w}{nw}"

        $ dmg = {0: (190, 205), 1: (190, 205), 2: (220, 245), 3: (270, 310)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:
        play sound "audio/se/ero_koki1.ogg"

        extend "\nПосле чего, она нажимает сильнее своей ногой!{w}{nw}"

        $ dmg = {0: (190, 205), 1: (190, 205), 2: (220, 245), 3: (270, 310)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:
        play sound "audio/se/ero_simetuke2.ogg"

        extend "\nНаклонившись над ним, она сжимает член Луки тыльной стороной своего бедра!{w}{nw}"

        $ dmg = {0: (190, 205), 1: (190, 205), 2: (220, 245), 3: (270, 310)}
        call damage(dmg[Difficulty])

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 25
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 20
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 15

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
            call show_skillname("Церемония Суккуба")
    else:
        play sound "audio/se/ero_sigoki1.ogg"

        extend "\nТряся ими из стороны в сторону, она продолжает сжимать член Луки между своих бёдер!{w}{nw}"

        $ dmg = {0: (190, 205), 1: (190, 205), 2: (220, 245), 3: (270, 310)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife*2 and sel_hphalf == 0:
        call common_s1
    elif max_mylife > mylife*5 and sel_kiki == 0:
        call common_s2

    return

label alma_ng2_aa8:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Растай от моего поцелуя..."
    elif ransu == 2:
        enemy "Я украду твоё сердце..."
    elif ransu == 3:
        enemy "Твоё сердце принадлежит мне..."

    call show_skillname("Совращающий поцелуй")
    play sound "audio/se/ero_chupa2.ogg"

    "Альма Эльма соприкасается своими губами с губами Луки!{w}{nw}"

    $ ransu = rnd2(1,100)

    if aqua == 2 and Difficulty == 1 and ransu < 80 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 2 and ransu < 75 + undine_buff:
        jump aqua_guard
    if aqua == 2 and Difficulty == 3 and ransu < 70 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 50 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 2 and ransu < 40 + undine_buff:
        jump aqua_guard
    if aqua > 2 and Difficulty == 3 and ransu < 30 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 100
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 100
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 100

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но порыв ветера отталкивает Альму!"
            jump wind_guard

    extend "\nПосле чего, её влажный язык проскальзывает в рот Луки!{w}{nw}"

    if daten > 0:
        call skill22x
        jump common_main

    "После поцелуя развратное ощущение охватывает тело Луки!"

    #call skillcount2(3637)
    if status == 0:
        "Лука был загипнотизирован Альмой!"
    elif status == 1:
        "Лука загипнотизирован..."

    $ status = 5
    call status_print

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    l "Хаа..."
    enemy "Хорошие ощущение, не правда ли?{w}\nПозволь мне сделать тебе ещё лучше..."
    if Difficulty < 2:
        $ status_turn = rnd2(3,4)
    elif Difficulty == 2:
        $ status_turn = rnd2(4,5)
    elif Difficulty == 3:
        $ status_turn = rnd2(5,6)

    return

label alma_ng2_aa9:
    $ owaza_count1b = 0
    $ owaza_num1 = 2
    if kousan == 2:
        alma "Хочешь стать моей добычей...?"

    call show_skillname("Удержание Суккуба")

    "Альма Эльма опрокидывает Луку на землю и напрыгивает на него!"

    if daten > 0:
        jump skill22x

    $ ransu = rnd2(1,100)

    if aqua > 0 and Difficulty == 1 and ransu < 101 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 2 and ransu < 101 + undine_buff:
        jump aqua_guard
    if aqua > 0 and Difficulty == 3 and ransu < 99 + undine_buff:
        jump aqua_guard

    if wind > 0 and Difficulty == 1:
        $ wind_kakuritu = 100
    elif wind > 0 and Difficulty == 2:
        $ wind_kakuritu = 100
    elif wind > 0 and Difficulty == 3:
        $ wind_kakuritu = 100

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            jump wind_guard

    # call skillcount2(3636)
    $ tatie4 = "alma_elma hd0"
    $ kousoku = 1
    $ genmogaku = 2

    "Альма Эльма крепко прижимается к Луке!"

    call hide_skillname

    if kousan == 2:
        return

    if first_sel2 != 1:
        if surrender < 1:
            alma "А теперь пора изнасиловать тебя...{w}{nw}"

        elif surrender > 0:
            alma "А теперь пора изнасиловать тебя..."
            call alma_ng2_aa10

        extend "\nЕсли ты не будешь сопротивляться, я покажу тебе рай удовольствий."
        alma "Ну а если будешь, я исушу тебя без капли жалости.{w}\nИтак... что же ты будешь делать?"

    else:
        alma "Ты вновь собираешься вырываться...?{w}\nИли решил погрузиться в удовольствие?"

    $ first_sel2 = 1
    if earth == 0:
        $ genmogaku = 1
    elif earth > 0:
        $ genmogaku = 0

    $ mogaku_sel1 = "Если таков твой выбор, то больше я тебя не отпущу..."
    $ mogaku_sel2 = "Если таков твой выбор, то больше я тебя не отпущу..."
    $ mogaku_sel3 = "Какой разочаровывающий выбор..."
    $ mogaku_sel4 = "Какой разочаровывающий выбор..."
    $ mogaku_sel5 = "Какой разочаровывающий выбор..."

    return

label alma_ng2_ab1:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Слишком медленно..."
    elif ransu == 2:
        enemy "Я изнасилую тебя, малыш Лука...~{image=note}"
    elif ransu == 3:
        enemy "В этот раз Сильфи не спасёт тебя..."

    call show_skillname("Мело Софи Теллус: Тёмная техника")
    play sound "audio/se/miss.ogg"
    hide alma_elma
    pause .1
    call face(param=2)
    "Альма Эльма настолько быстро двигается к Луки, что её не видно!"
    if aqua == 2 and wind=5:
        call alma_ng2006
        return

    play sound "audio/se/down.ogg"

    "Прежде чем Лука успевает отреагировать, она толкает его на землю и сажимает его член меж своих сисек!"

    alma "А теперь... насладись моей грудью~{image=note}."

    call show_skillname("Мело Софи Теллус: Тёмная техника")

    play sound "audio/se/ero_koki1.ogg"

    show alma_elma hh1 with dissolve

    "Альма начинает быстро двигать своей грудью верх-вниз!{w}{nw}"

    $ dmg = {0: (210, 260), 1: (210, 260), 2: (310, 360), 3: (410, 460)}
    call damage(dmg[Difficulty])

    "Она начинает двигать ими верх-вниз поочерёдно!{w}{nw}"

    $ dmg = {0: (210, 260), 1: (210, 260), 2: (310, 360), 3: (410, 460)}
    call damage(dmg[Difficulty])

    "Альма сдавливает член Луки и начинает трясти его!"

    $ dmg = {0: (210, 260), 1: (210, 260), 2: (310, 360), 3: (410, 460)}
    call damage(dmg[Difficulty])

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if aqua != 2 and hanyo[2] < 1:
        $ hanyo[2] = 1
        call alma_ng2003

    return

label alma_ng2_ab2:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты выглядишь таким аппетитным...!"
    elif ransu == 2:
        enemy "Я высосу из тебя всю сперму..."
    elif ransu == 3:
        enemy "Ветер не поможет тебе..."

    call show_skillname("Дух Романус Тери: Венера")
    play sound "audio/se/miss.ogg"
    hide alma_elma
    pause .1
    call face(param=2)
    "Альма Эльма настолько быстро двигается к Луки, что её не видно!"
    if aqua == 2 and wind=5:
        call alma_ng2006
        return

    "Она захватывает Луку и быстро проносит через всю арену!{w}{nw}"

    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, она впечатывает его в одну из стен Коллизея!"

    $ a_effect1 = 1
    $ dmg = {0: (100, 130), 1: (100, 130), 2: (150, 180), 3: (200, 230)}
    call damage(dmg[Difficulty])

    "Прежде чем Лука успевает отреагировать, она толкает его на землю и сажимает его член меж своих сисек!"

    alma "Ммм... мммф..."

    "Её язык начинает вылизывать весь член Луки!{w}{nw}"

    $ dmg = {0: (120, 170), 1: (120, 170), 2: (220, 270), 3: (320, 370)}
    call damage(dmg[Difficulty])

    extend "\nМягкий язык Альмы сдавливает член Луки!{w}{nw}"

    $ dmg = {0: (120, 170), 1: (120, 170), 2: (220, 270), 3: (320, 370)}
    call damage(dmg[Difficulty])

    extend "\nОбвив его своим мокрым языком, Альма Эльма начинает сильно сосать член Луки!"

    $ dmg = {0: (120, 170), 1: (120, 170), 2: (220, 270), 3: (320, 370)}
    call damage(dmg[Difficulty])

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if aqua != 2 and hanyo[2] < 1:
        $ hanyo[2] = 1
        call alma_ng2003

    return

label alma_ng2_ab3:
    python:
        while True:
            ransu = rnd2(1,3)

            if ransu != ren:
                ren = ransu
                break

    if ransu == 1:
        enemy "Ты выглядишь таким аппетитным...!"
    elif ransu == 2:
        enemy "Я высосу из тебя всю сперму..."
    elif ransu == 3:
        enemy "Ветер не поможет тебе..."

    call show_skillname("Искушение Дьявола: Фера Вентус")
    play sound "audio/se/miss.ogg"
    hide alma_elma
    pause .1
    call face(param=2)
    pause .5
    if aqua == 2:
        "Альма приближается к Луке!"
    if aqua == 2 and wind=5:
        call alma_ng2006
        return

    "Прежде чем Лука успевает отреагировать, Альма прижимает его голым телом!"

    alma "А теперь... растай~{image=note}."

    if daten > 0:
        call skill22x
        jump common_main

    pause .5
    play sound "audio/se/ero_chupa4.ogg"
    pause .4

    "Её язык начинает вылизывать весь член Луки!{w}{nw}"

    $ dmg = {0: (210, 250), 1: (210, 250), 2: (310, 350), 3: (410, 450)}
    call damage(dmg[Difficulty])

    extend "\nМягкий язык Альмы сдавливает член Луки!{w}{nw}"

    $ dmg = {0: (210, 250), 1: (210, 250), 2: (310, 350), 3: (410, 450)}
    call damage(dmg[Difficulty])

    extend "\nОбвив его своим мокрым языком, Альма Эльма начинает сильно сосать член Луки!"

    $ dmg = {0: (210, 250), 1: (210, 250), 2: (310, 350), 3: (410, 450)}
    call damage(dmg[Difficulty])

    call hide_skillname

    call face(param=1)

    if mylife == 0:
        return_to badend_h

    if not aqua < 2:
        $ hanyo[5] = 1
        play sound "audio/se/yami.ogg"
        l "Гах..."
        "Альма гипнотизирует Луку своим поцелуем!"
        l "..............."
        call show_skillname("Безмятежный разум")
        play sound "audio/se/aqua.ogg"
        show color white with Dissolve(1.5)
        pause 1.5
        "Но Лука, используя силу Ундины, еле как сопротивляется её искушению!"
        hide color with Dissolve(1.5)
        call face(param=3)
        enemy "Ауу, так не честно..."
        if aqua != 2 and hanyo[2] < 1:
            $ hanyo[2] = 1
            call alma_ng2003

        call face(param=1)

    else:
        l "Нннн..."
        $ status = 5
        call status_print
        $ status_turn = 999
        "Лука был загипнотизирован поцелум Альмы!"
        call face(param=2)
        enemy "Мммм...{w}\nЗапечательно~{image=note}."
        enemy "Победа моя...{w}\nКак же печально для тебя, малыш Лука...~{image=note}"

    call hide_skillname
    return

label alma_ng2_yuwaku_a:
    while True:
        if hanyo[7] < 1:
            call alma_ng2_ab3a
            jump common_main

        elif hanyo[7] > 0:
            call alma_ng2_ab3b
            jump common_main

label alma_ng2_ab3a:
    $ hanyo[7] = 1

label valkyrie_ng_start:
    $ monster_name = "Валькирия"
    $ lavel = "valkyrie_ng"
    $ img_tag = "valkyrie"
    $ tatie1 = "valkyrie st01"
    $ tatie2 = "valkyrie st01"
    $ tatie3 = "valkyrie st01"
    $ tatie4 = "valkyrie st01"
    $ haikei = "bg 225"
    $ monster_pos = center

    $ earth_keigen  = 40
    if Difficulty == 1:
        $ max_enemylife = 32000
        $ henkahp1 = 14000
    elif Difficulty == 2 :
        $ max_enemylife = 36000
        $ henkahp1 = 16000
    elif Difficulty == 3:
        $ max_enemylife = 40000
        $ henkahp1 = 25000
        $ earth_keigen  = 25

    $ luka_weapon = 0
    $ player = 0
    $ ng = 0
    $ knightsup = 0
    $ party1sup = 0
    $ party2sup = 0
    $ alicesup = 0
    $ alice_skill = 0
    $ damage_keigen = 85
    $ kaihi = 80
    $ mus = 36
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_sel1 = "I'm not done with you yet?"
    $ mogaku_sel2 = "You can't escape that easily."
    $ mogaku_sel3 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ mogaku_sel4 = "I won't lose when it comes to power"
    $ mogaku_sel5 = "Even if you struggle, you won't break free!"
    $ mogaku_earth_dassyutu1 = "Hum... That's some power."
    $ mogaku_earth_dassyutu2 = "As expected from someone filled with Gnome's power."
    $ end_n = 2
    $ zmonster_x = -270
    $ tukix = 300

    l "Гах...!"
    "Нехорошо!{w}\nАнгелы уже здесь!?"
    "Неужели Илиас... готова атаковать!?"
    syoukan undine st01
    undine "Я так не думаю..."
    undine "Похоже эти двое были отправлены сюда в одиночку, дабы не вызвать слишком много шума."
    l "Хмм..."
    "Тогда это не имеет значения.{w}\nОна — единственное препятствие, отделяющее меня от зелёной сферы."
    "Я не могу позволить Валькирии забрать её!"
    "Я тянусь к рукоятке своего меча...{w}\n...!"
    l "С-Сияние Ангела!"
    "Оно пропало!"
    "Та девица украла его!?"
    "У меня до сих пор есть свой железный меч...{w}\nНо я не могу им атаковать ангелов! Правильно?"
    "Нет...{w} У меня нет выбора. Этот меч всё, что я сейчас имею."
    "Я неохотно обнажаю свой железный меч и встаю в стойку."
    "Мне правда не хочется прибегать к этому...{w}\nНо Купидон забрала моё Сияние Ангела, поэтому у меня нет иного выбора!"
    valkyrie "Хватит тянуть кота за хвост.{w}\nТеперь тебя ничто не спасёт от моего клинка..."
    $ nostar = 2
    $ enemy_holy = 1
    $ enemy_holyturn = 999999
    $ hanyo[0] = -1

    jump common_main

label valkyrie_ng_main:
    while True:
        $ enemy_noattack = 0
        if used_daystar > 0 and hanyo[10] < 1:
            $ hanyo[10] = 1
            call valkyrie_ng_daystar
        if wind != 5 and hanyo[8] == 1:
            $ hanyo[8] = 2
            call valkyrie_ng002
        
        call cmd("attack") 

        if result == 1:
            jump common_attack
        elif result == 2 and genmogaku > 0 and earth < 1:
            jump common_mogaku
        elif result == 2 and genmogaku == 0 and earth < 1:
            jump common_mogaku2
        elif result == 2 and genmogaku > 0 and earth > 0:
            jump mogaku_earth1
        elif result == 2 and genmogaku == 0 and earth > 0:
            jump mogaku_earth2
        elif result == 3:
            jump common_bougyo
        elif result == 4:
            jump common_nasugamama
        elif result == 5:
            jump valkyrie_ng_kousan
        elif result == 7:
            jump common_skill
        elif result == 9:
            jump common_support

label valkyrie_ng_kousan:
    "Если я сдамся здесь, мне никогда не заполучить зелёную сферу!"
    jump valkyrie_ng_main


label valkyrie_ng_syori:
    "Я не могу попросить противника об атаке!"
    jump valkyrie_ng_main

label valkyrie_ng001:
    enemy "Гах..."
    enemy "Как и ожидалось, ты довольно силён."
    enemy "Но я не собираюсь проигрывать тебе!"

    return

label valkyrie_ng002:
    l "Ах...!"
    "Даже со своей новой силой, я не могу даже прочувствовать атаку Валькирии...{w}\nЧто не так!?"
    syoukan undine st01
    undine "Похоже святая энергия, окружающая эту девушку, мешает тебе читать её сердце.{w}\nСомневаюсь, что таким образом ты сможешь заметить хоть капельку жажды крови от неё..."
    l "Серьёзно?{w}\nЗначит ли это, что я даже не смогу уворачиваться от её атак?"
    undine "Ты ведь уже сражался с ангелами до того, как был отправлен назад во времени, так?{w}\nРазве у тебя нет других способностей против них?"
    l "Других способностей...?"
    pause 1.0
    l "...!"
    "А ведь правда!{w}\nТак как мой противник — ангел, мне нужно воспользоваться..."   
    return

label valkyrie_ng_v:
    call face(param=3)
    valkyrie "Нет!{w}\nЯ должна... продолжать... сражаться...!"
    call show_skillname("Медитация")
    "Валькирия пытается медитировать!"
    pause .5
    play sound "audio/se/tamamo2.ogg"
    "Но у ней не осталось энергии!"
    l "Похоже ты достигла своего предела..."
    valkyrie "Рррр...!"
    valkyrie "У меня всё ещё остались силы для ещё одной атаки...{w}\nИ я не собираюсь тратить их попусту!"
    play sound "audio/se/bird.ogg"
    "Валькирия делает выпад в мою сторону!"
    call show_skillname("Девятиликий Ракшаса")
    $ renpy.pause(0.5, hard=True)
    call skill20a
    call hide_skillname
    l "Ах!"
    "Реагируя на рефлексах, я атакую в ответ!"
    call show_skillname("Девятиликий Ракшаса")
    $ renpy.pause(0.5, hard=True)
    call skill20a
    "Два святых клинка ударяются друг о друга!"
    call hide_skillname
    $ renpy.pause(0.5, hard=True)
    play sound "audio/se/slash.ogg"
    $ renpy.pause(0.7, hard=True)
    play sound "audio/se/slash3.ogg"
    $ renpy.pause(0.25, hard=True)
    play sound "audio/se/slash3.ogg"
    $ renpy.pause(0.4, hard=True)
    play sound "audio/se/break.ogg"
    $ renpy.pause(0.4, hard=True)
    play sound "audio/se/light.ogg"
    scene color white with dissolve
    "Вспышка ослепительного света озаряет всю арену!"
    pause 1.0
    "................."
    pause .5
    stop music
    pause .5
    hide color with Dissolve(1.0)
    l "Гах..."
    "... Что произошло?{w}\nЯ выиграл?"
    "Я не чувствую никакой боли...{w}\nИ Валькирия исчезла."
    "Она была запечатана?{w}\nПолагаю, это не имеет значения.{w}\nЯ выиграю!{w} ...Надеюсь."
    play sound "audio/se/tamamo.ogg"
    show cupid st06 with dissolve
    cupid "О не-е-ет...{w}\nОна проиграла!?"
    l ".............."
    "Я бросаю взгляд на Купидона, всё ещё держащего Сияние Ангела."
    cupid "А...?{w}{nw}"
    show cupid st03 with dissolve
    cupid "Хья!"
    play sound "audio/se/bird.ogg"
    hide cupid with dissolve
    "Купидон стремительно улетает прочь!{w}\n... Вместе с Сиянием Ангела."
    l "Подожди!{w}\nВернись!"
    "Она улизнула...{w}\nПрекрасно."
    call victory(viclose=1)
    $ getexp = 3000000
    call lvup
    jump grandnoah_08

label valkyrie_ng_daystar:
    valkyrie "Нгх...!{w}\nДенница!"
    valkyrie "Приём Люцифины...{w}\nЯ не ожидала такого от тебя... даже несмотря на то, что ты её дитя.{w}\nХмпф..."

label valkyrie_ng_nostar:
    call face(param=3)
    valkyrie "Ты держишь меня за идиотку?"
    call counter
    call show_skillname("Танец Ангела")
    play sound "audio/se/bun.ogg"
    hide valkyrie with dissolve
    "Польностью заряжая свою магию, Валькирия превращается в чистую святую энергию в тот момент, когда звезда настигает её!"
    pause .5
    play sound "audio/se/bom2.ogg"
    pause 1.0
    $ damage = rnd2(3000, 3500)
    call enemylife_kaihuku
    pause .5
    play sound "audio/se/bun.ogg"
    call face(param=1)
    "Валькирия рематериализируется."
    jump valkyrie_ng_a

label valkyrie_ng_a:
    while True:
        if hanyo[6] < 1 and result != 23 and result != 19 and result != 4 and result != 3 and result != 26 and before_action != 3:
            $ hanyo[6] = 1
            call valkyrie_ng_angeldance
        if hanyo[6] > 0 and hanyo[7] < 6:
            $ hanyo[7] += 1
        if hanyo[7]:
            call valkyrie_ng_angelhalo
            jump common_main

        if hanyo[5] < 2 and Difficulty == 1 and (enemylife < max_enemylife*35/100):
            $ hanyo[5] = 2
            call valkyrie_ng_heal2
            jump common_main
        if hanyo[5] < 2 and Difficulty > 1 and (enemylife < max_enemylife*45/100):
            $ hanyo[5] = 2
            call valkyrie_ng_heal2
            jump common_main
        if hanyo[5] < 1 and enemylife < max_enemylife*666/1000:
            $ hanyo[5] = 1
            call valkyrie_ng001

        if hanyo[0] > 0:
            call valkyrie_ng_healc
            $ hanyo[0] -= 1
        if hanyo[0] == 0:
            call valkyrie_ng_healb

        if hanyo[2] < 1 and hanyo[3] < 3:
            $ hanyo[3] += 1
        if hanyo[2] < 1 and hanyo[3] == 3:
            $ hanyo[2] += 1
            $ hanyo[3] = 0

        if (enemylife < max_enemylife*20/100) and hanyo[2] > 0:
            call valkyrie_ng_heal2
            jump common_main

        if hanyo[5] == 0 and hanyo[7] < 6:
            $ ransu = rnd2(1,3)
        if hanyo[5] == 0 and hanyo[7] > 5:
            $ ransu = rnd2(1,4)
        if hanyo[5] == 1:
            $ ransu = rnd2(1,6)
        if hanyo[5] == 2:
            $ ransu = rnd2(1,8)

        if ransu == 1 and hanyo[5] < 2:
            call valkyrie_ng_a1
            jump common_main

        elif ransu == 2 and hanyo[5] < 2:
            call valkyrie_ng_a2
            jump common_main

        elif ransu == 1 and hanyo[5] > 1:
            call valkyrie_ng_a1b
            jump common_main
        elif ransu == 2 and hanyo[5] > 1:
            call valkyrie_ng_a2b
            jump common_main
        elif ransu == 3:
            call valkyrie_ng_a3
            jump common_main
        elif ransu == 4 and hanyo[0] < 0:
            call valkyrie_ng_heal
            jump common_main
        elif ransu == 4 and hanyo[0] > -1:
            call valkyrie_ng_a3
            jump common_main
        elif ransu == 5:
            call valkyrie_ng_a5
            jump common_main
        elif ransu == 6:
            call valkyrie_ng_a6
            jump common_main
        elif ransu > 6 and (enemylife < max_enemylife*50/100) and hanyo[2] > 0:
            call valkyrie_ng_heal2
            jump common_main
        elif ransu > 6:
            call valkyrie_ng_a5
            jump common_main

label valkyrie_ng_angelhalo:
    $ enemy_wind = 0
    $ enemy_windturn = 0
    $ hanyo[7] += 1
    l "Гхх...!"
    "Это плохо...{w}\nЯ не могу по ней даже попасть!"
    valkyrie "Ха-ха-ха-ха...{w}\nИлиас будет очень рада, если я уберу тебя с её пути..."
    l "Н-нет..."
    "Что же мне делать?{w}\nЯ не могу сдаваться..."
    pause 1.0
    "Подождите!{w}\nМожет быть если я сниму своё кольцо..."
    pause .5
    q "Лука!{w}\nИдиот!"
    l "А!?"
    hide valkyrie with dissolve
    "С трибун доносится знакомый голос."
    "Бросив взгляд в их сторону, я замечаю..."
    show alice st03 as c700 with dissolve
    l "Алиса!"
    a "Используй это!"
    show alice st01 as c700 with dissolve
    play sound "audio/se/karaburi.ogg"
    "Алиса кидает знакомый меч на арену."
    l "Ах!"
    show angelsword as c700 with dissolve
    "Это Сияние Ангела!"
    "Правильно...{w}\nПохоже это меч Алисы."
    show alice st03 as c700 with dissolve
    a "Держись!{w}\nЯ помогу тебе."
    show valkyrie st01 at xy(X=150)
    show alice st01 at xy(X=-28) as c700
    with dissolve
    valkyrie "Хмпф.{w}\nНет, не поможешь."
    play sound "audio/se/light.ogg"
    "Валькирия возносит свой меч вверх и луч света из его острия создаёт прозрачную стену вокруг арены!"
    valkyrie "Этот барьер должен сдержать этих надоедливых глупцов."
    a "Что за..."
    pause .5
    play sound "audio/se/dageki.ogg"
    "Алисы пытается сломать барьер, но её попытки тщётны."
    show alice st03 as c700 with dissolve
    a "Чёрт возьми!{w}\nТебе лучше выиграть этот бой!"
    show alice st01 as c700 with dissolve
    l "Само собой!"
    hide c700
    show valkyrie st01 at center
    with dissolve
    "Спасибо, Алиса..."
    valkyrie "Хмпф.{}w\nНе имеет значения..."
    valkyrie "Я всё равно хотела честной битвы."
    $ luka_weapon = 1
    call skillset(2)
    "Быстро поднимая Сияние Ангела и снимая кольцо, я вновь встаю в боевую стойку."
    $ enemy_holy = 2
    $ enemy_holyturn = 999999

    return

label test_ng_start:
    $ _game_menu_screen = "custom_gm"
    $ in_game = 1
    $ storyon = 1
    $ ng = 1
    $ manuoff = 0
    $ mylv = 1000
    $ max_mylife = 4000
    $ max_mp = 23
    $ exp = 79999000
    $ mon_labo_on = 0
    $ skillset = 1
    $ ngitem = 1
    $ item01 = 1
    $ item02 = 0
    $ item04 = 0
    $ item05 = 0
    $ item06 = 0
    $ item07 = 0
    $ item08 = 0
    $ item09 = 0
    $ item21 = 0
    $ skill01 = 27
    $ skill02 = 3
    $ skill03 = 3
    $ skill04 = 3
    $ skill05 = 3
    $ ivent01 = 0
    $ ivent02 = 0
    $ ivent03 = 0
    $ ivent04 = 0
    $ ivent05 = 0
    $ ivent06 = 0
    $ ivent07 = 0
    $ ivent08 = 0
    $ ivent09 = 0
    $ ivent10 = 0
    $ ivent11 = 0
    $ ivent12 = 0
    $ ivent13 = 0
    $ ivent14 = 0
    $ ivent15 = 0
    $ ivent16 = 0
    $ ivent17 = 0
    $ ivent18 = 0
    $ check01 = 0
    $ check02 = 0
    $ check03 = 0
    $ check04 = 0
    $ check05 = 0
    $ check06 = 0
    $ check07 = 0
    $ check08 = 0
    $ check08 = 0
    $ check10 = 0
    $ check11 = 0
    $ ruka_tati = 0
    $ enemy_wind = 0
    $ enemy_earth = 0
    $ enemy_aqua = 0
    $ enemy_fire =0
    $ enemy_windturn = 0
    $ enemy_earthturn = 0
    $ enemy_aquaturn = 0
    $ enemy_fireturn = 0
    $ queenharpy_sex = 0
    $ alice_sex = 0
    $ granberia_sex = 0
    $ tamamo_sex = 0
    $ alma_sex = 0
    $ erubetie_sex = 0
    $ sylph_sex = 0
    $ salamander_sex = 0
    $ undine_sex = 0
    $ gnome_sex = 0
    $ hapy_sex = 0
    $ kitsune_sex = 0
    $ chrome_sex = 0
    $ vamploli_sex = 0
    $ dragonpup_sex = 0
    $ dragonpup_sex = 0
    $ tinylamia_sex = 0
    $ goblingirl_sex = 0
    $ tamamo_rape = 0
    $ queenhapy_rape = 0
    $ queenhapy_nomate = 0
    $ banditcave = 0
    $ harpyvillage = 0
    $ iliasport = 0
    $ portnatalia = 0
    $ chromemansion = 0
    $ saniliacity = 0
    $ sabasacity = 0
    $ pyramid = 0
    $ lilymansion = 0
    $ grandnoahcity = 0
    $ yamatai = 0
    $ plansect = 0
    $ grangoldcity = 0
    $ svillage = 0
    $ ladyvillage = 0
    $ goldport = 0
    $ fairyisland = 0
    $ poseidoncave = 0
    $ pirateship = 0
    $ garuda = 0
    $ sylph_get = 0
    $ salamander_get = 0
    $ undine_get = 0
    $ gnome_get = 0
    $ spirit1 = ""
    $ spirit2 = ""
    $ spirit3 = ""
    $ spirit4 = ""
    $ knight_enrika1 = ""
    $ knight_harpyvillage = ""
    $ knight_banditcave = ""
    $ knight_iliasport = ""
    $ party1_harpyvillage = ""
    $ party1_banditcave = ""
    $ party1_iliasport = ""
    $ party2_harpyvillage = ""
    $ party2_banditcave = ""
    $ party2_iliasport = ""
    $ knight_enrika2 = ""
    $ party1_enrika2 = ""
    $ party2_enrika2 = ""
    $ knight_enrika3 = ""
    $ party1_enrika3 = ""
    $ party2_enrika3 = ""
    $ knight_enrika4 = ""
    $ party1_enrika4 = ""
    $ party2_enrika4 = ""
    $ spirit_count = 0
    $ luka_weapon = 1
    $ party_member = ["", "", "", "", ""]
    # [0] - Alice
    # [1] - Knights
    # [2], [3] - Continent Ilias members
    # [4] - Sentora members
    $ granberia_rep = 0
    $ tamamo_rep = 0
    $ alma_rep = 0
    $ erubetie_rep = 0
    $ alice_rep = 0
    $ salamander_rep = 0
    $ sylph_rep = 0
    $ undine_rep = 0
    $ gnome_rep = 0
    $ harpy_rep = 0
    $ kitsune_rep = 0
    $ alice_fight = 0
    $ tamamo_fight = 0
    $ nanabi_fight = 0

    call lvup_check
    window auto
    $ Difficulty = 1

    $ nostar = 0
    $ ng = 2
    $ monster_name = "Амира"
    $ lavel = "test_ng"
    $ img_tag = "amira"
    $ tatie1 = "amira st01"
    $ tatie2 = "amira st01"
    $ tatie3 = "amira st02"
    $ tatie4 = "amira st01"
    $ haikei = "bg 019"
    $ monster_pos = center

    $ max_enemylife = 1*10**6
    $ alice_skill = 0
    $ damage_keigen = 90
    $ earth_keigen = 30
    if Difficulty == 3:
        $ earth_keigen = 25
    $ kaihi = 80
    $ mus = 37
    $ tikei = 3
    call musicplay
    call maxmp
    $ mp = max_mp
    call syutugen2
    $ mogaku_anno1 = "Но Лука не может вырваться из её объятий!"
    $ mogaku_anno2 = "Лука вырывается!"
    $ mogaku_anno3 = "Но Лука не может атаковать в таком положении!"
    $ mogaku_dassyutu1 = "Ты сбежал из моих объятий..."
    $ mogaku_dassyutu2 = "Но от своей судьбы тебе не убежать..."
    $ half_s1 = "Скоро я растворю тебя."
    $ half_s2 = "Я удостоверюсь, что ты будешь страдать за твоё появление здесь."
    $ kiki_s1 = "Hmmhmmhmm... You have a long way to go before competing with me in strength!"
    $ kiki_s2 = "I won't lose when it comes to power"
    $ end_n = 9
    $ exp_minus = 1370025
    $ zmonster_x = -270
    $ tukix = 320
    enemy "Наконец-то я тебя нашла, мой дорогой~{image=note}!"
    l "Ох, дерьмово..."
    menu:
        "Снять кольцо":
            call skillset(2)
        "Нет":
            call skillset(1)
    
    "Я вынимаю свой меч, приготовившись к битве."
    $ hanyo[1] = 0
    jump common_main

label test_ng_main:
    call cmd("attack")

    if result == 1 and kousoku > 0:
        jump common_mogaku
    elif result == 1 and kousoku < 1:
        jump common_attack
    elif result == 2 and earth < 1 and genmogaku > 0:
        jump common_mogaku
    elif result == 2 and earth < 1 and genmogaku == 0:
        jump common_mogaku2
    elif result == 2 and earth > 0 and genmogaku > 0:
        jump mogaku_earth1
    elif result == 2 and earth > 0 and genmogaku == 0:
        jump mogaku_earth2
    elif result == 3:
        jump common_bougyo
    elif result == 4:
        jump common_nasugamama
    elif result == 5:
        jump undine_ng_kousan
    elif result == 6:
        jump test_ng_onedari
    elif result == 7:
        jump common_skill

label test_ng_v:
    return

label test_ng_a:
    $ ransu = rnd2(1,3)
    if ransu == 1:
        call test_ng_a1
        jump common_main
    elif ransu == 2:
        call test_ng_a2
        jump common_main
    elif ransu == 3:
        call test_ng_a3
        jump common_main

label test_ng_a1:
    $ ransu = rnd2(1,2)
    if ransu == 1:
        enemy "Я училась у Чака Норриса!"
    elif ransu == 2:
        enemy "Я училась у Брюса Ли!"

    call show_skillname("Удар")
    play sound "audio/se/dageki.ogg"
    "Амира бьёт с вертухи в щи Луки!"
    $ ransu = rnd2(1,100)
    if aqua == 2 and ransu < 91:
        jump aqua_guard
    if aqua > 2 and Difficulty == 1 and ransu < 51:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty < 2:
            $ wind_kakuritu = 25
        elif Difficulty == 2:
            $ wind_kakuritu = 25
        elif Difficulty == 3:
            $ wind_kakuritu = 20
    elif wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но Лука уклоняется словно торнадо!"
            call wind_guard
    else:
        $ dmg = {0: (540, 570), 1: (540, 570), 2: (560, 590), 3: (580, 610)}
        call damage(dmg[Difficulty])

    call hide_skillname

    if mylife == 0:
        return_to badend_h

    if max_mylife > mylife * 2 and sel_hphalf == 0:
        call common_s1

    if max_mylife > mylife * 5 and sel_kiki == 0:
        call common_s2

    return

label test_ng_a2:
    enemy "Ора-ра!"

    call show_skillname("Зов Стихий")
    $ ransu = rnd2(1,4)
    if ransu == 1:
        play sound "audio/se/wind2.ogg"
        $ enemy_wind = 2
        $ enemy_windturn = 6
        "Амира призывает силу ветра!"
    if ransu == 2:
        play sound "audio/se/earth1.ogg"
        $ enemy_earth = 2
        $ enemy_earthturn = 6
        "Амира призывает силу земли!"
    if ransu == 3:
        play sound "audio/se/aqua2.ogg"
        $ enemy_aqua = 3
        $ enemy_aquaturn = 6
        "Амира призывает силу воды!"
    if ransu == 4:
        play sound "audio/se/fire2.ogg"
        $ enemy_fire = 2
        $ enemy_fireturn = 6
        "Амира призывает силу огня!"

    call hide_skillname

    return

label test_ng_a3:
    if kousan == 2:
        enemy "Пришло время рейп-тайма, дорогой~!"

    call show_skillname("Захват")
    play sound "audio/se/karaburi.ogg"
    "Амира хватает Луку!"

    $ ransu = rnd2(1,100)
    if aqua == 2 and ransu < 91:
        jump aqua_guard
    if aqua > 2 and Difficulty < 2 and ransu < 51 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 2 and ransu < 46 + undine_buff:
        jump aqua_guard
    elif aqua > 2 and Difficulty == 3 and ransu < 41 + undine_buff:
        jump aqua_guard

    if wind > 0 and wind != 4:
        if Difficulty == 1:
            $ wind_kakuritu = 25
        if Difficulty == 2:
            $ wind_kakuritu = 20
        if Difficulty == 3:
            $ wind_kakuritu = 15
    if wind == 4:
        $ wind_kakuritu = 1

    call wind_guard_kakuritu

    if wind_guard_on != 0:
        if wind > 0:
            $ sel1 = "Но мощный порыв ветра отталкивает Амиру назад!"
            jump wind_guard

    $ dmg = {0: (440, 480), 1: (440, 480), 2: (520, 560), 3: (570, 640)}
    call damage(dmg[Difficulty])

    $ kousoku = 1
    call status_print

    call hide_skillname

    "Лука схвачен!"

    if mylife == 0:
        return_to badend_h

    if kousan == 2:
        return

    call face(param=2)
    if first_sel1 == 0:
        enemy "Если ты достаточно силён, то тебе не составит проблем вырваться, да...?"

    elif first_sel1 == 1:
        enemy "Выберишься ли ты в этот раз...?"

    $ first_sel1 = 1

    if earth == 0:
        $ genmogaku = 1

    elif earth > 0:
        $ genmogaku = 0

    return
    