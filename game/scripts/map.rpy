init -2:
    style map_button:
        is button
        xpadding 0
        xmargin 0
        background None

    style map_button_text:
        is default
        font "fonts/archiform.otf"
        size 22
        idle_color "#fff"
        hover_color "#000fff"
        insensitive_color "#8888"
        idle_outlines [(2, "#000c", 0, 0)]
        hover_outlines [(2, "#000c", 0, 0)]
        insensitive_outlines [(2, "#303030cc", 0, 0)]

    style map_text:
        is default
        font "fonts/archiform.otf"
        size 16
        line_leading 10
        # bold True
        color "#fff"
        outlines [(1, "#000c", 0, 0)]


# part 1
label map1:
    scene bg map1 with blinds


label map1_2:
    $ ui.textbutton("Илиасбург", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(1), style="map_button") #601
    $ ui.textbutton("Илиаспорт", anchor=(0, 0), pos=(250, 40), clicked=ui.returns(2), style="map_button") #602
    $ ui.textbutton("Горы Ирины", anchor=(0, 0), pos=(180, 240), clicked=ui.returns(3), style="map_button") #603
    $ ui.textbutton("Деревня\nСчастья", anchor=(0, 0), pos=(630, 180), clicked=ui.returns(4), style="map_button") #604
    $ ui.textbutton("Энрика", anchor=(0, 0), pos=(220, 390), clicked=ui.returns(5), style="map_button") #605
    $ ui.textbutton("Илиасвилль", anchor=(0, 0), pos=(360, 470), clicked=ui.returns(6), style="map_button") #606
    if iliasport < 1:
        $ ui.text("Рек. уровень: 81", style="map_text", anchor=(0, 0), pos=(250, 60)) #607
    elif iliasport > 0:
        $ ui.text("(На Сентору)", style="map_text", anchor=(0, 0), pos=(250, 60)) #607

    if ivent01 == 0:
        $ ui.text("Рек. уровень: N/A", style="map_text", anchor=(0, 0), pos=(160, 260))  #608
    elif ivent01 == 1:
        $ ui.text("Выполнено", style="map_text", anchor=(0, 0), pos=(190, 260))  #608

    if ivent02 == 0:
        $ ui.text("Рек. уровень: N/A", style="map_text", anchor=(0, 0), pos=(630, 220))  #609
    elif ivent02 == 1:
        $ ui.text("Выполнено", style="map_text", anchor=(0, 0), pos=(660, 220))  #609

    if ivent03 == 0:
        $ ui.text("Рек. уровень: N/A", style="map_text", anchor=(0, 0), pos=(220, 410))  #610
    elif ivent03 == 1:
        $ ui.text("Выполнено", style="map_text", anchor=(0, 0), pos=(250, 410))  #610
        # Обработка нажатий кнопок на карте

    $ result = ui.interact()

    if result == 1:
        call city03_main0
        play sound "audio/se/asioto2.ogg"
        scene bg black with blinds
        play music "audio/bgm/field1.ogg"
        jump map1
    elif result == 2 and iliasport < 1:
        jump lb_0071
    elif result == 2 and iliasport == 1:
        jump iliascontinent_end
    elif result == 3 and ivent01 == 0:
        jump lb_0054
    elif result == 3 and ivent01 == 1:
        jump lb_0059
    elif result == 4 and ivent02 == 0:
        jump lb_0060
    elif result == 4 and ivent02 == 1:
        call city04_main0
        play sound "audio/se/asioto2.ogg"
        scene bg black with blinds
        play music "audio/bgm/field1.ogg"
        jump map1
    elif result == 5 and ivent03 == 0:
        jump lb_0067
    elif result == 5 and ivent03 == 1:
        jump lb_0070
    elif result == 6:
        jump lb_0053


label map2:
    if region != 6:
        stop music fadeout 1.0

    scene bg map2 with Dissolve(1.0)
    if region != 6:
        play music "NGDATA/bgm/field6.ogg"

    $ region = 6
    $ region_name = "Наталия"

label map2_2:
    # $ ui.textbutton("San Ilia", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(1), style="map_button") #601
    # $ ui.textbutton("Port Natalia", anchor=(0, 0), pos=(630, 270), clicked=ui.returns(2), style="map_button") #602
    # $ ui.textbutton("Haunted Mansion", anchor=(0, 0), pos=(420, 170), clicked=ui.returns(3), style="map_button") #603
    if skill02 < 1:
        $ ui.textbutton("Лес Духов", anchor=(0, 0), pos=(260, 60), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("Рек. уровень: 84", style="map_text", anchor=(0, 0), pos=(220, 80)) #607
    else:
        $ ui.textbutton("{color=00C957}Лес Духов{/color}", anchor=(0, 0), pos=(260, 60), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("{color=00C957}ЗАВЕРШЕНО{/color}", style="map_text", anchor=(0, 0), pos=(220, 80)) #607

    $ ui.textbutton("Карта мира", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(9), style="map_button") #604

    # if ivent04 == 0:
    #     $ ui.text("Rec. Level: 16", style="map_text", anchor=(0, 0), pos=(640, 290))  #605
    # elif ivent04 == 1:
    #     $ ui.text("Clear", style="map_text", anchor=(0, 0), pos=(680, 290))  #605
    #
    # if ivent05 == 0:
    #     $ ui.text("Rec. Level: 17", style="map_text", anchor=(0, 0), pos=(410, 190))  #606
    # elif ivent05 == 1:
    #     $ ui.text("Clear", style="map_text", anchor=(0, 0), pos=(450, 190))  #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        if skill02 < 1:
            jump lb_0119
        else:
            jump sylph_complete

    elif result == 9:
        jump map0

label sylph_complete:
    $ renpy.show(BG)
    show sylph st21 with dissolve
    sylph "А?{w}\nЗачем ты хочешь вернуться в Лес Духов, Лука?"
    sylph "Ты уже вернул меня!"
    l "Ну..."
    "Я в любом случае ничего не смогу сделать с Лабораторией Сирому без Курому.{w}\nДумаю Сильфа права."
    "Я вновь открываю свою карту."

    jump map0

label map3:
    scene bg map3 with Dissolve(1.0)


label map3_2:
    # $ ui.textbutton("Sabasa Castle", anchor=(0, 0), pos=(300, 230), clicked=ui.returns(1), style="map_button") #601
    # $ ui.textbutton("Pyramid", anchor=(0, 0), pos=(130, 300), clicked=ui.returns(2), style="map_button") #602
    # $ ui.textbutton("Witch Hunt Village", anchor=(0, 0), pos=(270, 500), clicked=ui.returns(3), style="map_button") #603
    # $ ui.textbutton("Safaru Ruins", anchor=(0, 0), pos=(350, 60), clicked=ui.returns(4), style="map_button") #604
    # $ ui.text("Rec. Level: 24", style="map_text", anchor=(0, 0), pos=(330, 80)) #607
    if skill03 < 1:
        $ ui.textbutton("Руины Сафару", anchor=(0, 0), pos=(260, 60), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("Рек. уровень: 83", style="map_text", anchor=(0, 0), pos=(220, 80)) #607
    else:
        $ ui.textbutton("{color=F4A460}Руины Сафару{/color}", anchor=(0, 0), pos=(260, 60), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("{color=F4A460}ЗАВЕРШЕНО{/color}", style="map_text", anchor=(0, 0), pos=(220, 80)) #607

    $ ui.textbutton("Карта мира", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(9), style="map_button") #604

    # if ivent06 == 0:
    #     $ ui.text("Rec. Level: 21", style="map_text", anchor=(0, 0), pos=(100, 320))  #605
    # elif ivent06 == 1:
    #     $ ui.text("Done", style="map_text", anchor=(0, 0), pos=(140, 320))  #605

    # if ivent07 == 0:
    #     $ ui.text("Rec. Level: 22", style="map_text", anchor=(0, 0), pos=(250, 520))  #606
    # elif ivent07 == 1:
    #     $ ui.text("Done", style="map_text", anchor=(0, 0), pos=(290, 520))  #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        if skill03 < 1:
            jump safina_first
        else:
            jump gnome_complete

    elif result == 9:
        jump map0


# вторая часть
label map4:
    scene bg map4 with Dissolve(1.0)


label map4_2:
    # $ ui.textbutton("Grand Noah Castle", anchor=(0, 0), pos=(80, 500), clicked=ui.returns(1), style="map_button") #601
    # $ ui.textbutton("Yamatai Village", anchor=(0, 0), pos=(600, 140), clicked=ui.returns(2), style="map_button") #602
    # $ ui.textbutton("Plansect Village", anchor=(0, 0), pos=(240, 310), clicked=ui.returns(3), style="map_button") #603
    if skill04 < 1:
        $ ui.textbutton("Источник Ундины", anchor=(0, 0), pos=(360, 490), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("Рек. уровень: 84", style="map_text", anchor=(0, 0), pos=(360, 510)) #607
    else:
        $ ui.textbutton("{color=00BFFF}Источник Ундины{/color}", anchor=(0, 0), pos=(360, 490), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("{color=00BFFF}ЗАВЕРШЕНО{/color}", style="map_text", anchor=(0, 0), pos=(360, 510)) #607

    $ ui.textbutton("Карта мира", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(9), style="map_button") #604


    # if ivent09 == 0:
    #     $ ui.text("Rec. Level: 28", style="map_text", anchor=(0, 0), pos=(570, 165))  #605
    # elif ivent09 == 1:
    #     $ ui.text("Complete", style="map_text", anchor=(0, 0), pos=(610, 165))  #605

    # if ivent10 == 0:
    #     $ ui.text("Rec. Level: 30", style="map_text", anchor=(0, 0), pos=(230, 335))  #606
    # elif ivent10 == 1:
    #     $ ui.text("Complete", style="map_text", anchor=(0, 0), pos=(270, 335))  #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        if skill04 < 1:
            jump grandnoah_first
        else:
            jump undine_complete

    elif result == 9:
        jump map0


label map5:
    scene bg map5 with Dissolve(1.0)


label map5_2:
    # $ ui.textbutton("Grangold Castle", anchor=(0, 0), pos=(640, 320), clicked=ui.returns(1), style="map_button") #601
    # $ ui.textbutton("Succubus Village", anchor=(0, 0), pos=(400, 270), clicked=ui.returns(2), style="map_button") #602
    # $ ui.textbutton("Lady's Village", anchor=(0, 0), pos=(180, 320), clicked=ui.returns(3), style="map_button") #603
    if skill05 < 1:
        $ ui.textbutton("Золотой вулкан", anchor=(0, 0), pos=(380, 410), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("Рек. уровень: 85", style="map_text", anchor=(0, 0), pos=(380, 430)) #607
    else:
        $ ui.textbutton("{color=FF6347}Золотой вулкан{/color}", anchor=(0, 0), pos=(380, 410), clicked=ui.returns(1), style="map_button") #604
        $ ui.text("{color=FF6347}ЗАВЕРШЕНО{/color}", style="map_text", anchor=(0, 0), pos=(380, 430)) #607
    # $ ui.textbutton("Gold Port", anchor=(0, 0), pos=(280, 120), clicked=ui.returns(5), style="map_button") #605
    # $ ui.text("Rec. Level: 44", style="map_text", anchor=(0, 0), pos=(350, 430)) #608

    $ ui.textbutton("Карта мира", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(9), style="map_button") #604
    
    # if ivent12 == 0:
    #     $ ui.text("Rec. Level: 40", style="map_text", anchor=(0, 0), pos=(390, 290))  #606
    # elif ivent12 == 1:
    #     $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(430, 290))  #606

    # if ivent13 == 0:
    #     $ ui.text("Rec. Level: 42", style="map_text", anchor=(0, 0), pos=(150, 340))  #607
    # elif ivent13 == 1:
    #     $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(190, 340))  #607

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        if skill05 < 1:
            jump gold_first
        else:
            jump salamander_complete

    elif result == 9:
        jump map0

label map6:
    scene bg map0 with Dissolve(1.0)


label map6_2:
    if item07 == 0:
        $ ui.textbutton("Port Natalia", anchor=(0, 0), pos=(480, 250), clicked=ui.returns(4), style="map_button") #604
        $ ui.text("Rec. Level: 46", style="map_text", anchor=(0, 0), pos=(470, 270))  #608

    if item09 == 0:
        $ ui.textbutton("Gold Port", anchor=(0, 0), pos=(350, 110), clicked=ui.returns(5), style="map_button") #605
        $ ui.text("Rec. Level: 48", style="map_text", anchor=(0, 0), pos=(330, 130))  #609

    if ivent06 == 1 and ivent14 == 0:
        $ ui.textbutton("Sabasa Castle", anchor=(0, 0), pos=(200, 230), clicked=ui.returns(6), style="map_button") #606
        $ ui.text("Rec. Level: 49", style="map_text", anchor=(0, 0), pos=(160, 250))  #610

    $ ui.textbutton("Shrine", anchor=(0, 0), pos=(200, 120), clicked=ui.returns(7), style="map_button") #607

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 4 and ivent04 == 0:
        jump lb_0252
    elif result == 4 and ivent04 == 1:
        jump lb_0253
    elif result == 5:
        call city26_main0
        play sound "audio/se/asioto2.ogg"
        scene bg black with blinds
        play music "audio/bgm/field3.ogg"
        jump map6
    elif result == 6:
        jump lb_0267
    elif result == 7 and item02+item05+item06+item08+item09 < 5:
        jump lb_0275
    elif result == 7 and item02+item05+item06+item08+item09 == 5:
        jump lb_0276


label map7:
    scene bg map6 with Dissolve(1.0)


label map7_2:
    $ ui.textbutton("Remina", anchor=(0, 0), pos=(200, 290), clicked=ui.returns(1), style="map_button") #601
    $ ui.textbutton("Monster Lord's Castle", anchor=(0, 0), pos=(380, 260), clicked=ui.returns(3), style="map_button") #603
    $ ui.text("Rec. Level: 57", style="map_text", anchor=(0, 0), pos=(330, 280)) #606

    if ivent15 == 0:
        $ ui.text("Rec. Level: 54", style="map_text", anchor=(0, 0), pos=(150, 310))  #604
    elif ivent15 == 1:
        $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(190, 310))  #604

    $ ui.textbutton("Sealed Sinner's Prison", anchor=(0, 0), pos=(455, 300), clicked=ui.returns(2), style="map_button") #602

    if ivent16 == 0:
        $ ui.text("Rec. Level: 56", style="map_text", anchor=(0, 0), pos=(520, 320))  #605
    elif ivent16 == 1:
        $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(560, 320))  #605

        # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1 and ivent15 == 0:
        jump lb_0283
    elif result == 1 and ivent15 == 1:
        jump lb_0287
    elif result == 2 and ivent16 == 0:
        jump lb_0288
    elif result == 2 and ivent16 == 1:
        jump lb_0290
    elif result == 3:
        jump lb_0291


# третья часть
label map8:
    scene bg map1 with Dissolve(1.0)


label map8_2:
    $ ui.textbutton("Enrika", anchor=(0, 0), pos=(220, 390), clicked=ui.returns(5), style="map_button") #605
    $ ui.textbutton("North To Sentora", anchor=(0, 0), pos=(250, 40), clicked=ui.returns(2), style="map_button") #602
    $ ui.text("Rec. Lvl: 69", style="map_text", anchor=(0, 0), pos=(280, 60)) #608

    if ivent01 > 0:
        $ ui.textbutton("Iliasburg", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(1), style="map_button") #601

        if ivent01 == 1:
            $ ui.text("Rec. Lvl: 67", style="map_text", anchor=(0, 0), pos=(360, 300))  #607
        elif ivent01 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(400, 300))  #607

    if ivent02 > 0:
        $ ui.textbutton("Happiness Village", anchor=(0, 0), pos=(630, 180), clicked=ui.returns(4), style="map_button") #604

        if ivent02 == 1:
            $ ui.text("Rec. Lvl: 68", style="map_text", anchor=(0, 0), pos=(620, 200))  #609
        elif ivent02 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(660, 200))  #609


    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 5:
        call city31_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map8
    elif result == 1 and ivent01 == 1:
        jump lb_0316
    elif result == 1 and ivent01 == 2:
        call city32_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map8
    elif result == 4 and ivent02 == 1:
        jump lb_0319
    elif result == 4 and ivent02 == 2:
        call city33_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map8
    elif result == 2:
        jump lb_0322


label map9:
    scene bg map2 with Dissolve(1.0)


label map9_2:
    if ivent05 > 0:
        $ ui.textbutton("San Ilia Castle", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(1), style="map_button") #601
        $ ui.textbutton("Haunted Mansion", anchor=(0, 0), pos=(420, 170), clicked=ui.returns(3), style="map_button") #603

        if ivent05 == 1:
            $ ui.text("Rec. Lvl: 71", style="map_text", anchor=(0, 0), pos=(410, 190))  #606
        elif ivent05 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(450, 190))  #606

    if ivent04 > 0:
        $ ui.textbutton("Port Natalia", anchor=(0, 0), pos=(630, 270), clicked=ui.returns(2), style="map_button") #602

        if ivent04 == 1:
            $ ui.text("Rec. Lvl: 70", style="map_text", anchor=(0, 0), pos=(640, 290))  #605
        elif ivent04 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(680, 290))  #605

    $ ui.textbutton("Forest of Spirits", anchor=(0, 0), pos=(260, 60), clicked=ui.returns(4), style="map_button") #604
    $ ui.text("Rec. Lvl: 72", style="map_text", anchor=(0, 0), pos=(220, 80)) #607

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city36a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map9
    elif result == 2 and ivent04 == 1:
        jump lb_0326
    elif result == 2 and ivent04 == 2:
        call city35_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map9
    elif result == 3 and ivent05 == 1:
        jump lb_0333
    elif result == 3 and ivent05 == 2:
        call lb_0336x
        jump map9
    elif result == 4:
        jump lb_0337


label map10:
    scene bg map3 with Dissolve(1.0)


label map10_2:
    if ivent14 > 0:
        $ ui.textbutton("Sabasa Castle", anchor=(0, 0), pos=(300, 230), clicked=ui.returns(1), style="map_button") #601

    if ivent07 > 0:
        $ ui.textbutton("Witch Hunt Village", anchor=(0, 0), pos=(270, 500), clicked=ui.returns(3), style="map_button") #603

        if ivent07 == 1:
            $ ui.text("Rec. Lvl: 72", style="map_text", anchor=(0, 0), pos=(250, 520))  #606
        elif ivent07 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(290, 520))  #606

    $ ui.textbutton("Safaru Ruins", anchor=(0, 0), pos=(350, 60), clicked=ui.returns(4), style="map_button") #604
    $ ui.text("Rec. Level: 73", style="map_text", anchor=(0, 0), pos=(330, 80)) #607

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city37a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map10
    elif result == 3 and ivent07 == 1:
        jump lb_0345
    elif result == 3 and ivent07 == 2:
        call city38_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map10
    elif result == 4:
        jump lb_0349


label map11:
    scene bg map4 with Dissolve(1.0)


label map11_2:
    if ivent08 > 0:
        $ ui.textbutton("Grand Noah Castle", anchor=(0, 0), pos=(80, 500), clicked=ui.returns(1), style="map_button") #601

    if ivent09 > 0:
        $ ui.textbutton("Yamatai Village", anchor=(0, 0), pos=(600, 140), clicked=ui.returns(2), style="map_button") #602

        if ivent09 == 1:
            $ ui.text("Rec. Level: 73", style="map_text", anchor=(0, 0), pos=(570, 160))  #605
        elif ivent09 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(610, 160))  #605

    if ivent10 > 0:
        $ ui.textbutton("Plansect Village", anchor=(0, 0), pos=(240, 310), clicked=ui.returns(3), style="map_button") #603

        if ivent10 == 1:
            $ ui.text("Rec. Level: 74", style="map_text", anchor=(0, 0), pos=(230, 330))  #606
        elif ivent10 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(270, 330))  #606

    $ ui.textbutton("Undine's Spring", anchor=(0, 0), pos=(360, 490), clicked=ui.returns(4), style="map_button") #604
    $ ui.text("Rec. Level: 75", style="map_text", anchor=(0, 0), pos=(360, 510)) #607

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city39a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map11
    elif result == 2 and ivent09 == 1:
        jump lb_0356
    elif result == 2 and ivent09 == 2:
        call city40_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map11
    elif result == 3 and ivent10 == 1:
        jump lb_0360
    elif result == 3 and ivent10 == 2:
        call city41_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map11
    elif result == 4:
        jump lb_0364


label map12:
    scene bg map5 with Dissolve(1.0)


label map12_2:
    if ivent11 > 0:
        $ ui.textbutton("Grangold Castle", anchor=(0, 0), pos=(640, 320), clicked=ui.returns(1), style="map_button") #601

    if ivent12 > 0:
        $ ui.textbutton("Succubus Village", anchor=(0, 0), pos=(400, 270), clicked=ui.returns(2), style="map_button") #602

        if ivent12 == 1:
            $ ui.text("Rec. Level: 74", style="map_text", anchor=(0, 0), pos=(390, 290))  #606
        elif ivent12 == 2:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(430, 290))  #606

    if ivent11 > 0:
        $ ui.textbutton("Gold Port", anchor=(0, 0), pos=(280, 120), clicked=ui.returns(3), style="map_button") #605

        if ivent17 == 0:
            $ ui.text("Rec. Level: 75", style="map_text", anchor=(0, 0), pos=(260, 140))  #607

    if ivent17 == 2:
        $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(300, 140))  #607

    $ ui.textbutton("Gold Volcano", anchor=(0, 0), pos=(380, 410), clicked=ui.returns(4), style="map_button") #604
    $ ui.text("Rec. Level: 76", style="map_text", anchor=(0, 0), pos=(350, 430)) #608

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city42_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map12
    elif result == 2 and ivent12 == 1:
        jump lb_0372
    elif result == 2 and ivent12 == 2:
        call city43_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map12
    elif result == 3 and ivent17 == 0:
        jump lb_0376
    elif result == 3 and ivent17 == 2:
        call city44_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/field4.ogg"
        jump map12
    elif result == 4:
        jump lb_0379


label map13:
    scene bg map0 with Dissolve(1.0)


label map13_2:
    if ivent14 > 0:
        $ ui.textbutton("Drain Lab", anchor=(0, 0), pos=(10, 340), clicked=ui.returns(1), style="map_button") #601

        if ivent14 == 1:
            $ ui.text("Rec. Level: 76", style="map_text", anchor=(0, 0), pos=(0, 360))  #604
        elif ivent14 == 3:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(40, 360))  #604

    if ivent05 > 1:
        $ ui.textbutton("Biolab", anchor=(0, 0), pos=(400, 200), clicked=ui.returns(2), style="map_button") #602

        if ivent05 == 2:
            $ ui.text("Rec. Level: 77", style="map_text", anchor=(0, 0), pos=(370, 220))  #605
        elif ivent05 == 3:
            $ ui.text("Completed", style="map_text", anchor=(0, 0), pos=(410, 220))  #605

    $ ui.textbutton("Sealed Sinner's Prison", anchor=(0, 0), pos=(400, 30), clicked=ui.returns(3), style="map_button") #603
    $ ui.text("Rec. Level: 78", style="map_text", anchor=(0, 0), pos=(370, 50)) #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1 and ivent14 < 3:
        jump lb_0384
    elif result == 1 and ivent14 == 3:
        jump lb_0388x
    elif result == 2 and ivent05 < 3:
        jump lb_0389
    elif result == 2 and ivent05 == 3:
        jump lb_0393x
    elif result == 3:
        jump lb_0394


label maped:
    scene bg map0 with Dissolve(1.0)


label maped_2:
    $ ui.textbutton("Ilias Continent", anchor=(0, 0), pos=(525, 335), clicked=ui.returns(1), style="map_button") #601
    $ ui.textbutton("San Ilia Region", anchor=(0, 0), pos=(400, 260), clicked=ui.returns(2), style="map_button") #602
    $ ui.textbutton("Sabasa Region", anchor=(0, 0), pos=(180, 270), clicked=ui.returns(3), style="map_button") #603
    $ ui.textbutton("Noah Region", anchor=(0, 0), pos=(530, 150), clicked=ui.returns(4), style="map_button") #604
    $ ui.textbutton("Gold Region", anchor=(0, 0), pos=(340, 130), clicked=ui.returns(5), style="map_button") #605
    $ ui.textbutton("Monster Lord's Castle", anchor=(0, 0), pos=(365, 40), clicked=ui.returns(6), style="map_button") #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        jump map14
    elif result == 2:
        jump map15
    elif result == 3:
        jump map16
    elif result == 4:
        jump map17
    elif result == 5:
        jump map18
    elif result == 6:
        jump lb_0413


label map14:
    scene bg map1 with Dissolve(1.0)


label map14_2:
    $ ui.textbutton("Ilias Village", anchor=(0, 0), pos=(360, 470), clicked=ui.returns(1), style="map_button") #601

    if ivent01 == 2:
        $ ui.textbutton("Iliasburg", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(2), style="map_button") #602

    if ivent02 == 2:
        $ ui.textbutton("Happiness Village", anchor=(0, 0), pos=(630, 180), clicked=ui.returns(3), style="map_button") #603

    $ ui.textbutton("World Map", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(4), style="map_button") #604

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city51_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map14
    elif result == 2:
        call city52_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map14
    elif result == 3:
        call city53_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map14
    elif result == 4:
        jump maped


label map15:
    scene bg map2 with Dissolve(1.0)


label map15_2:
    if ivent18 == 2:
        $ ui.textbutton("San Ilia Castle", anchor=(0, 0), pos=(370, 280), clicked=ui.returns(1), style="map_button") #601

    if ivent04 == 2:
        $ ui.textbutton("Port Natalia", anchor=(0, 0), pos=(630, 270), clicked=ui.returns(2), style="map_button") #602

    $ ui.textbutton("World Map", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(4), style="map_button") #604

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city56a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map15
    elif result == 2:
        call city55_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map15
    elif result == 4:
        jump maped


label map16:
    scene bg map3 with Dissolve(1.0)


label map16_2:
    if ivent06 == 2:
        $ ui.textbutton("Sabasa Castle", anchor=(0, 0), pos=(300, 230), clicked=ui.returns(1), style="map_button") #601

    if ivent07 == 2:
        $ ui.textbutton("Witch Hunt Village", anchor=(0, 0), pos=(270, 500), clicked=ui.returns(3), style="map_button") #603

    $ ui.textbutton("World Map", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(4), style="map_button") #604

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city57a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map16
    elif result == 3:
        call city58_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map16
    elif result == 4:
        jump maped


label map17:
    scene bg map4 with Dissolve(1.0)


label map17_2:
    if ivent08 == 2:
        $ ui.textbutton("Grand Noah Castle", anchor=(0, 0), pos=(80, 500), clicked=ui.returns(1), style="map_button") #601

    if ivent09 == 2:
        $ ui.textbutton("Yamatai Village", anchor=(0, 0), pos=(600, 140), clicked=ui.returns(2), style="map_button") #602

    if ivent10 == 2:
        $ ui.textbutton("Plansect Village", anchor=(0, 0), pos=(240, 310), clicked=ui.returns(3), style="map_button") #603

    $ ui.textbutton("World Map", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(4), style="map_button") #604

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city59a_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map17
    elif result == 2:
        call city60_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map17
    elif result == 3:
        call city61_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map17
    elif result == 4:
        jump maped


label map18:
    scene bg map5 with Dissolve(1.0)


label map18_2:
    if ivent11 == 2:
        $ ui.textbutton("Grangold Castle", anchor=(0, 0), pos=(640, 320), clicked=ui.returns(1), style="map_button") #601

    if ivent12 == 2:
        $ ui.textbutton("Succubus Village", anchor=(0, 0), pos=(400, 270), clicked=ui.returns(2), style="map_button") #602

    if ivent17 == 2:
        $ ui.textbutton("Gold Port", anchor=(0, 0), pos=(280, 120), clicked=ui.returns(3), style="map_button") #605

    $ ui.textbutton("World Map", anchor=(0, 0), pos=(600, 550), clicked=ui.returns(4), style="map_button") #604

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        call city62_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map18
    elif result == 2:
        call city63_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map18
    elif result == 3:
        call city64_main0
        play sound "audio/se/habataki.ogg"
        scene bg black with blinds
        play music "audio/bgm/epilogue.ogg"
        jump map18
    elif result == 4:
        jump maped

label map0:
    scene bg map0 with Dissolve(1.0)

    if item09 > 0 and purple_orb < 1:
        $ purple_orb = 1
        $ orb_count += 1

label map0_2:
    $ ui.textbutton("Регион Наталия", anchor=(0, 0), pos=(450, 250), clicked=ui.returns(1), style="map_button") #601
    $ ui.textbutton("Регион Сафина", anchor=(0, 0), pos=(200, 300), clicked=ui.returns(2), style="map_button") #606
    $ ui.textbutton("Регион Ноа", anchor=(0, 0), pos=(530, 160), clicked=ui.returns(3), style="map_button") #606
    $ ui.textbutton("Золотой регион", anchor=(0, 0), pos=(320, 130), clicked=ui.returns(4), style="map_button") #606

    # Обработка нажатий кнопок на карте
    $ result = ui.interact()

    if result == 1:
        play music "NGDATA/bgm/field6.ogg"
        jump map2
    elif result == 2:
        play music "NGDATA/bgm/field8.ogg"
        jump map3
    elif result == 3:
        play music "NGDATA/bgm/field9.ogg"
        jump map4
    elif result == 4:
        play music "NGDATA/bgm/field10.ogg"
        jump map5
